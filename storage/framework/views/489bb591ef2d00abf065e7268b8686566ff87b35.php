<?php $__env->startSection('custom-css'); ?>
<style type="text/css">
	#itr {
		padding: 2rem;
	}
	.itr-leg {
		background: #ffffff;
		margin-bottom: 2rem;
	}
	.itr-leg .heading {
		border: 1px solid #B2B2B2;
		border-bottom: 1px solid white;
		position: relative;
		bottom: -1px;
		display: inline-block;
		font-weight: 400;
		margin: 0;
		padding: 2rem;
	}
	.itr-leg .heading i:before {
		font-size: 25px;
		color: #2AA9DF;
	}
	.itr-leg .body {
		border: 1px solid #B2B2B2;
		padding: 2rem;
	}
	.itr-leg .body section {
		margin-bottom: 2rem;
	}
	.itr-leg .body .title {
		font-size: 17px;
		text-transform: uppercase;
	}
	.itr-leg .body .desc {
		margin-left: 8.8rem;
	}
	.itr-leg .body .desc p {
		line-height: 1em;
	}
	.itr-leg .body .desc p i.status-icon {
		margin-left: 1.5rem;
		margin-right: .2rem;
		color: #009688;
		font-weight: 700;
	}
	span.not-available,
	span.not-available i {
		color: #d32f2f !important;
	}
	.itr-leg .body .desc p span.price {
		font-weight: 700;
		font-size: 18px;
		color: #009688;
	}
	.itr-leg .body .title i:before {
		position: relative;
		top: 5px;
		border-radius: 100px;
		padding: 1.5rem;
		color: #ffffff;
		font-size: 25px;
		margin-right: 2.8rem;
	}
	.itr-leg .body .accomodation .title i:before {
		background: #00bcd4;
	}
	.itr-leg .body .activities .title i:before {
		background: #2196f3;
	}
	.itr-leg .body .transport .title i:before {
		background: #f44336;
	}
	.change-btn i {
		font-size: 1.6rem;
		margin-left: 3rem;
		color: #f8f8f8 !important;
		background: #2AA9DF;
		padding: 8px;
		display: inline-block;
		transition: all 100ms ease-in-out 0s;
	}
	.change-btn i:hover {
		text-decoration: none;
		background: #2792BF;
	}
	.old-price {
		text-decoration: line-through;
		color: #ABABAB;
		font-weight: 400;
		margin-left: 1rem;
	}
	#review-summary {
		border: 1px solid #B2B2B2;
	    padding: 5px;
	    margin-top: 11px;
	}
	#review-summary h3 {
		margin-top: 0;
	}
	#total {
		font-size: 16px;
	}
	.white-bg{min-height: initial;}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

					<div class="tabs-container">
						<div class="panel-inner">
							<div class="panel-icon">

								<svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
									<desc>Created with Sketch.</desc>
									<defs>
										<polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>
									</defs>
									<g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-503.000000, -69.000000)">
											<g id="placeholder" transform="translate(503.000000, 69.000000)">
												<g id="Group">
													<mask id="mask-2" fill="white">
														<use xlink:href="#path-1"></use>
													</mask>
													<g id="Clip-2"></g>
													<path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>
												</g>
											</g>
										</g>
									</g>
								</svg>
							</div>
							<div class="panel-container">
								<h4><?php echo e(session()->get( 'search' )['itinerary'][0]['city']['name']); ?> to  <?php echo e(session()->get( 'search' )['itinerary'][count(session()->get( 'search' )['itinerary']) - 1]['city']['name']); ?></h4>
								<div>
									<p><?php echo e(session()->get( 'search' )['total_number_of_days']); ?> - <strong><?php echo e(session()->get( 'search' )['currency'].' '.$totalCost); ?></strong> (Per Person)</p>
									<div class="row">
										<div class="col-lg-2 col-md-3 col-sm-3">
											<svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
												<!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
												<desc>Created with Sketch.</desc>
												<defs></defs>
												<g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<g id="49---EROA007-V4.1-Generic-(Edit-Itinerary)-01" transform="translate(-66.000000, -241.000000)" fill="#212121">
														<g id="(Generic)-Icon---Calendar" transform="translate(66.000000, 241.000000)">
															<g id="(eRoam)-Icon---Calendar">
																<path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
																<path d="M10.1326667,8.000625 L12,8.000625 L12,6.25 L10.1326667,6.25 L10.1326667,8.000625 Z M10.1326667,10.2503125 L12,10.2503125 L12,8.4996875 L10.1326667,8.4996875 L10.1326667,10.2503125 Z M10.1326667,12.5 L12,12.5 L12,10.749375 L10.1326667,10.749375 L10.1326667,12.5 Z M7.733,12.5 L9.60033333,12.5 L9.60033333,10.749375 L7.733,10.749375 L7.733,12.5 Z M5.33333333,12.5 L7.20066667,12.5 L7.20066667,10.749375 L5.33333333,10.749375 L5.33333333,12.5 Z M2.93366667,12.5 L4.79933333,12.5 L4.79933333,10.749375 L2.93366667,10.749375 L2.93366667,12.5 Z M2.93366667,10.2503125 L4.79933333,10.2503125 L4.79933333,8.4996875 L2.93366667,8.4996875 L2.93366667,10.2503125 Z M2.93366667,8.000625 L4.79933333,8.000625 L4.79933333,6.25 L2.93366667,6.25 L2.93366667,8.000625 Z M5.33333333,8.000625 L7.20066667,8.000625 L7.20066667,6.25 L5.33333333,6.25 L5.33333333,8.000625 Z M5.33333333,10.2503125 L7.20066667,10.2503125 L7.20066667,8.4996875 L5.33333333,8.4996875 L5.33333333,10.2503125 Z M7.733,10.2503125 L9.60033333,10.2503125 L9.60033333,8.4996875 L7.733,8.4996875 L7.733,10.2503125 Z M7.733,8.000625 L9.60033333,8.000625 L9.60033333,6.25 L7.733,6.25 L7.733,8.000625 Z M9.60033333,5.749375 L2.39966667,5.749375 L2.39966667,13.000625 L12.534,13.000625 L12.534,5.749375 L9.60033333,5.749375 Z" id="Fill-3"></path>
															</g>
														</g>
													</g>
												</g>
											</svg> Date:
										</div>
										<div class="col-lg-10 col-md-9 col-sm-9">
											<?php echo e(date( 'j M Y', strtotime( session()->get( 'search' )['itinerary'][0]['city']['date_from'] ) )); ?> - <?php echo e(date( 'j M Y', strtotime( session()->get( 'search' )['itinerary'][count(session()->get( 'search' )['itinerary']) - 1]['city']['date_to'] ) )); ?>

										</div>
									</div>
									<div class="row">
										<div class="col-lg-2 col-md-3 col-sm-3">
											<svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
												<!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
												<desc>Created with Sketch.</desc>
												<defs>
													<polygon id="path-1" points="0.0734846939 0.0301886111 8.05731122 0.0301886111 8.05731122 7.93047228 0.0734846939 7.93047228"></polygon>
													<polygon id="path-3" points="0 5.61288199 0 0.127353462 14.9144388 0.127353462 14.9144388 5.61288199 1.12675642e-15 5.61288199"></polygon>
												</defs>
												<g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<g id="49---EROA007-V4.1-Generic-(Edit-Itinerary)-01" transform="translate(-66.000000, -260.000000)">
														<g id="(Generic)-Icon---User" transform="translate(66.000000, 260.000000)">
															<g id="(eRoam)-Icon---User">
																<g id="Group-3" transform="translate(3.367347, 0.121513)">
																	<mask id="mask-2" fill="white">
																		<use xlink:href="#path-1"></use>
																	</mask>
																	<g id="Clip-2"></g>
																	<path d="M4.06516837,1.0618806 C5.68180102,1.0618806 7.00440306,2.37258211 7.00440306,3.97439892 C7.00440306,5.57621572 5.68180102,6.88691723 4.06516837,6.88691723 C2.4489949,6.88691723 1.12639286,5.57621572 1.12639286,3.97439892 C1.12639286,2.37258211 2.4489949,1.0618806 4.06516837,1.0618806 M4.06516837,7.93047228 C6.26955612,7.93047228 8.05731122,6.15859802 8.05731122,3.97439892 C8.05731122,1.79019981 6.26955612,0.0301582708 4.06516837,0.0301582708 C1.8612398,0.0301582708 0.0734846939,1.80203253 0.0734846939,3.97439892 C0.0734846939,6.1467653 1.8612398,7.93047228 4.06516837,7.93047228" id="Fill-1" fill="#212121" mask="url(#mask-2)"></path>
																</g>
																<g id="Group-6" transform="translate(0.000000, 8.616800)">
																	<mask id="mask-4" fill="white">
																		<use xlink:href="#path-3"></use>
																	</mask>
																	<g id="Clip-5"></g>
																	<path d="M5.00797959,1.17090852 L9.90593878,1.17090852 C11.9142551,1.17090852 13.5674694,2.65181918 13.8246122,4.56932694 L1.08991837,4.56932694 C1.34706122,2.6636519 3.00027551,1.17090852 5.00797959,1.17090852 M0.526653061,5.61288199 L14.3877245,5.61288199 C14.6819082,5.61288199 14.9145612,5.38244732 14.9145612,5.09087691 C14.9145612,2.36024877 12.6735918,0.127353462 9.90593878,0.127353462 L5.00797959,0.127353462 C2.25287755,0.127353462 -3.06122449e-05,2.34841605 -3.06122449e-05,5.09087691 C-3.06122449e-05,5.38244732 0.232622449,5.61288199 0.526653061,5.61288199" id="Fill-4" fill="#212121" mask="url(#mask-4)"></path>
																</g>
															</g>
														</g>
													</g>
												</g>
											</svg> Travellers:
										</div>
										<div class="col-lg-10 col-md-9 col-sm-9">
											<?php echo e($travellers); ?>

										</div>

										<?php if(session()->has('error')): ?>
											<p style="text-align: center; color: red;padding-top: 10px;" id="error-msg"><?php echo e(session()->get('error')); ?></p>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="tabs-content-container tabs-content-wrapper1">
						<div class="white-bg payment-page">
							<div class="row">
								<div class="col-sm-4">
									<a href="<?php echo e(url( 'proposed-itinerary' )); ?>" name="" class="btn btn-primary btn-block">VIEW ITINERARY</a>
								</div>
								<div class="col-sm-4">
									<button type="submit" name="" class="btn btn-block btn-primary">Share Itinerary</button>
								</div>
								<div class="col-sm-4">
									<button type="submit" name="" class="btn btn-block btn-primary">Print Itinerary</button>
								</div>
							</div>
							<!-- <div class="m-t-30">
								<h4>PAX Details</h4>
							</div> -->
		<form class="horizontal-form" action="payment" method="post" name="signup_form" id="signup_form_id">
			<?php echo e(csrf_field()); ?>					
				<section class="m-t-40">
                <h4>Lead Person Information</h4>
                 <?php 
                	for($i=0;$i<$travellers;$i++){
                		if($i == 0){
                			?>
                			<div class="m-t-10">
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="input-field select-box-new">
                        <select class="form-control passenger_title" name="passenger_title[<?php echo e($i); ?>]">
                          <option value="Mr">Mr</option>
                          <option value="Ms">Ms</option>
                          <option value="Mrs">Mrs</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="input-field">
                        <input id="pax-fname<?php echo e($i); ?>" type="text" name="passenger_first_name[<?php echo e($i); ?>]" class="passenger_first_name">
                        <label for="pax-fname<?php echo e($i); ?>" class="">First Name</label>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="input-field">
                        <input id="pax-lname<?php echo e($i); ?>" type="text" name="passenger_last_name[<?php echo e($i); ?>]" class="passenger_last_name">
                        <label for="pax-lname<?php echo e($i); ?>" class="">Last Name</label>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="m-t-30 radio-blackgroup">
                        <label class="radio-checkbox label_radio m-r-10" for="radio-m<?php echo e($i); ?>"><input type="radio" name="passenger_gender[<?php echo e($i); ?>]" id="radio-m<?php echo e($i); ?>" value="Male" checked="">Male</label>
                        <label class="radio-checkbox label_radio" for="radio-f<?php echo e($i); ?>"><input type="radio" name="passenger_gender[<?php echo e($i); ?>]" id="radio-f<?php echo e($i); ?>" value="Female">Female</label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="input-field">
                        <input id="pax-dob<?php echo e($i); ?>" type="text" name="passenger_dob[<?php echo e($i); ?>]" class="passenger_dob">
                        <label for="pax-dob<?php echo e($i); ?>" class="" id="dob">Date of Birth</label>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="input-field">
                        <input id="pax-contact<?php echo e($i); ?>" type="text" name="passenger_contact_no[<?php echo e($i); ?>]" class="passenger_contact_no">
                        <label for="pax-contact<?php echo e($i); ?>">Contact No.</label>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="input-field">
                        <input id="pax-email<?php echo e($i); ?>" type="text" name="passenger_email[<?php echo e($i); ?>]" class="passenger_email">
                        <label for="pax-email<?php echo e($i); ?>">Email</label>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="input-field select-box-new">
                        <select class="form-control passenger_country" name="passenger_country[<?php echo e($i); ?>]">
                          <option value="">Select</option>
                         <?php 
						    $j=0;
						    $allCountry = array();
						    foreach($countries as $country){
						      foreach ($country['countries'] as $country_data){
						          $allCountry[$j]['name'] = $country_data['name'];
						          $allCountry[$j]['id'] = $country_data['id'];
						          $allCountry[$j]['region'] = $country['id'];
						          $allCountry[$j]['regionName'] = $country['name'];
						          $j++;
						      } 
						    }
						    usort($allCountry, 'sort_by_name');
						  ?>	

						 <?php if( count($allCountry) > 0 ): ?>
							<?php foreach($allCountry as $Country): ?>
								<option value="<?php echo e($Country['id']); ?>"><?php echo e($Country['name']); ?></option>
							<?php endforeach; ?>
						  <?php endif; ?>
                        </select>
                      </div>
                      <label for="passenger_country[0]" generated="true" class="error"></label>
                    </div>
                  </div>
                </div>
                			<?php
                		}else{
                			?>
                			 <div class="m-t-20">
                  <h5>PAX <?php echo e(($i)); ?> Update Details</h5>
                  <div class="row">
                    <div class="col-sm-1">
                      <div class="input-field select-box-new">
                        <select class="form-control passenger_title" name="passenger_title[<?php echo e($i); ?>]">
                          <option value="Mr">Mr</option>
                          <option value="Ms">Ms</option>
                          <option value="Mrs">Mrs</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-11">
                      <div class="row">
                        <div class="col-sm-3">
                          <div class="input-field">
                            <input id="pax-fname<?php echo e($i); ?>" type="text" name="passenger_first_name[<?php echo e($i); ?>]" class="passenger_first_name">
                            <label for="pax-fname<?php echo e($i); ?>" class="">First Name</label>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="input-field">
                            <input id="pax-lname<?php echo e($i); ?>" type="text" name="passenger_last_name[<?php echo e($i); ?>]" class="passenger_last_name">
                            <label for="pax-lname<?php echo e($i); ?>" class="">Last Name</label>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="m-t-30 radio-blackgroup">
                            <label class="radio-checkbox label_radio m-r-10" for="radio-m<?php echo e($i); ?>"><input type="radio" name="passenger_gender[<?php echo e($i); ?>]" id="radio-m<?php echo e($i); ?>" value="Male" checked="">Male</label>
                            <label class="radio-checkbox label_radio" for="radio-f<?php echo e($i); ?>"><input type="radio" name="passenger_gender[<?php echo e($i); ?>]" id="radio-f<?php echo e($i); ?>" value="Female">Female</label>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="input-field">
                            <input id="pax-dob<?php echo e($i); ?>" type="text" name="passenger_dob[<?php echo e($i); ?>]" class="passenger_dob">
                            <label for="pax-dob<?php echo e($i); ?>" class="" id="dob1">Date of Birth</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                			<?php
                		}
                	}
                ?>
                
              </section>
							<h4 class="m-t-30">Payment Confirmation</h4>
							<div class="table-responsive m-t-20">
								<table class="table">
									<tbody>
                                    <?php

                                    $total = $totalCost*$travellers;
                                    $gst = $total*0.025;
                                    $finalTotal = number_format($total + $gst, 2, '.', ',');

                                    ?>
									<tr>
										<td width="80%">Total Per Person </td>
										<td><?php echo e($currency); ?></td>
										<td><?php echo e(number_format($totalCost, 2, '.', ',')); ?></td>
									</tr>
									<tr>
										<td width="80%">Sub Total Amount </td>
										<td><?php echo e($currency); ?></td>
										<td><?php echo e(number_format($total, 2, '.', ',')); ?></td>
									</tr>
									<tr>
										<td>Credit Card Fee's 2.5%</td>
										<td><?php echo e($currency); ?></td>
										<td><?php echo e(number_format($gst, 2, '.', ',')); ?></td>
									</tr>
									<tr>
										<td><strong>Final Amount</strong></td>
										<td><strong><?php echo e($currency); ?></strong></td>
										<td><strong><?php echo e($finalTotal); ?></strong></td>
									</tr>
									</tbody>
								</table>
							</div>
							
								<div class="m-t-20">
									<h4>Credit Card Details</h4>
									
									<input type="hidden" name="totalAmount" id="totalAmount" value="<?php echo e($finalTotal); ?>">
									<input type="hidden" name="currency" id="currency" value="<?php echo e($currency); ?>">
									<div class="row">
										<div class="col-sm-6">
											<div class="input-field">
												<input type="text" name="first_name" id="first_name" class="form-control">
												<label for="first_name" class="">First Name</label>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input-field">
												<input type="text" name="last_name" id="last_name" class="form-control">
												<label for="last_name" class="">Last Name</label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="input-field">
												<input type="text" name="card_number" id="card_number" data-stripe="number" class="form-control">
												<label for="card_number" class="">Credit Card Number</label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-9">
											<div class="row">
												<div class="col-sm-3">
													<label class="expiry-label">Expiration Date:</label>
												</div>
												<div class="col-sm-4">
													<div class="input-field select-box-new" style="margin-bottom: 5px;">
														<select class="form-control" name="month" id="month" data-stripe="exp_month">
															<option value="">MM</option>
                                                            <?php $expiry_month = date('m');?>
                                                            <?php for($i = 1; $i <= 12; $i++) {
                                                            $s = sprintf('%02d', $i);?>
															<option value="<?php echo $s;?>" <?php //if ( $expiry_month == $i ) { ?>  <?php //} ?>><?php echo $s;?></option>
                                                            <?php } ?>
														</select>
													</div>
													<label for="month" generated="true" class="error"></label>
												</div>
												<div class="col-sm-4">
													<div class="input-field select-box-new" style="margin-bottom: 5px;">
														<select class="form-control" name="year" id="year" data-stripe="exp_year">
															<option value="">YYYY</option>
                                                            <?php
                                                            $lastyear = date('Y')+21;
                                                            $curryear = date('Y');
                                                            for($k=$curryear;$k<$lastyear;$k++){ ?>
															<option value="<?php echo substr($k, -2);?>"><?php echo $k;?></option>
                                                            <?php } ?>
														</select>
													</div>
													
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="input-field">
												<input type="text" maxlength="3" name="cvv" id="cvv" class="form-control" data-stripe="cvc">
												<label for="cvv" class="">CVV</label>
												<span class="input-icon">
                      <a href="#" class="help-icon" data-toggle="modal" data-target="#helpModal"><i class="fa fa-question-circle"></i></a>
                      </span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-8">
											<div class="input-field">
												<input id="country" type="text" name="country">
												<label for="country" class="">Country</label>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="input-field">
												<input id="postalcode" type="text" name="postalcode">
												<label for="postalcode" class="">Postal Code</label>
											</div>
										</div>
									</div>
									<!--<div class="row">
                                      <div class="col-sm-8">
                                        <div class="input-field">
                                          <input id="country" type="text">
                                          <label for="country" class="">Country</label>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="input-field">
                                          <input id="pcode" type="text">
                                          <label for="pcode" class="">Postal Code</label>
                                        </div>
                                      </div>
                                    </div>-->
									<div class="form-group m-t-40">
										<div class="row">
											<div class="col-sm-5">
												<ul class="payment-type">
													<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
													<li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
													<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
												</ul>
											</div>
											<div class="col-sm-7">
												<button type="submit" name="" class="btn btn-block btn-primary">Pay Now</button>
											</div>
										</div>
									</div>
									<div class="text-center m-t-40">
										<p>Your credit card will be charged <?php echo e($currency); ?> <?php echo e($finalTotal); ?> By clicking "Pay Now" below, you agree to <a href="#">these terms</a></p>
									</div>
							</form>
					</div>

					</div>

					</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script src="<?php echo e(url( 'assets/js/theme/bootstrap-datepicker.js' )); ?>"></script>
<script type="text/javascript">
	
	var searchSession = JSON.parse($('#search-session').val());

	// REVIEW MODULE
	var review = (function() {

		function init() {

			// var transportObject = {
			// 	fare_source_code : 'ODAxMDE5JlRGJlRGJjRkYzRmMjExLTllYzgtNDlkZC04MjcxLTg0MjgwMDY0MzZmMyZURiY=',
			// 	price : '180.00',
			// 	currency : globalCurrency,
			// 	fare_type : 'Public'
			// };
			var leg = 0;

			// var formatTransport = eroam.formatTransport( searchSession.itinerary[ leg ].transport, leg );
			// validateMystifly( formatTransport, leg );

			//var formattedHotel = eroam.formatHotel( searchSession.itinerary[ leg ].hotel, leg );
			//validateAEHotel( formattedHotel, leg );

			buildItinerary();
		}


		function buildItinerary() {
			var itinerary = searchSession.itinerary;
			for (var i = 0; i < itinerary.length; i++) {
				var leg = itinerary[i];

				// HOTEL
				var hotel = eroam.formatHotel(leg.hotel, leg.city);
				buildHotel(hotel, i);

				// ACTIVITIES
				var activities = eroam.formatActivities(leg.activities);
				buildActivities(activities, i);

				// TRANSPORT
				if (i != itinerary.length - 1) {
					console.log( 'transport: ',  leg.transport );
					var nextLeg = itinerary[ i + 1 ];
					var transport = eroam.formatTransport(leg.transport, leg.city, nextLeg.city);
					buildTransport(transport, i);
				}
			}
		}


		function buildHotel(hotel, index) {
			$('.hotel-row').eq(index).html(
				'<div class="col-md-7">' +
					'<b><i class="fa fa-hotel"></i> Accomodation:</b>' +
					'<br>' +
					'<span>'+ hotel.name +' ('+hotel.room_name+')</span>' +
				'</div>' +
				'<div class="col-md-5 text-right">' +
					'<br>' +
					'<span class="bold-txt">$279.89</span>' +
					'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
					'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
				'</div>'
			);
		}


		function buildActivities(activities, index) {
			var activityList = '';
			if (activities.length > 0) {
				for (var i = 0; i < activities.length; i++) {
					var activity = activities[i];
					activityList += '<li>'+ moment(activity.date_selected).format('dddd, MMM D') +' - '+ activity.name +'</li>';
				}

				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b>' +
						'<ul>' +
							activityList +
						'</ul>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="not-available"><i class="fa fa-times-circle"></i> Unavailable</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			} else {
				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b><br>' +
						'<span>No activities included.</span>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			}
		}


		function buildTransport(transport, index) {
			if (transport.provider != 'own_arrangement') {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ transport.name +'</span><br>' +
						'<span><b>Depart:</b> '+ transport.departure +'</span><br>' +
						'<span><b>Arrive:</b> '+ transport.arrival +'</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<span class="old-price">$279.89</span>' +
						'<span class="bold-txt">$280.89</span>' +
						'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			} else {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ searchSession.itinerary[index].city.name +' to '+ searchSession.itinerary[index + 1].city.name +' (Own Arrangement)</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			}
		}


		function validateMystifly( t, leg )
		{
			var unvalidated = isNotUndefined( t.availability_checked_at ) ? isExpired( t.availability_checked_at ) : true;
			console.log('Mystifly transport should be validated:', unvalidated);
			if( unvalidated )
			{
				MystiflyController.revalidate(
					t.fare_source_code,
					function( revalidateSuccessRS ) {

						var newPrice = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount;
						var newCurrency = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
						var convertedNewPrice = eroam.convertCurrency( newPrice, newCurrency );
						if( convertedNewPrice != t.price ) {
							// NEW PRICE DOES NOT MATCH THE OLD PRICE
							// NEW PRICE OF FLIGHT IS STORE IN VARIABLE "convertedNewPrice"
							// UPDATE THE PRICE UI WITH THE NEW PRICE & USE "globalCurrency" VARIABLE AS THE CURRENCY
							searchSession.itinerary[ leg ].transport.price = convertedNewPrice;
							searchSession.itinerary[ leg ].transport.old_price = t.price;
						}
						searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();


						console.log('revalidate success', revalidateSuccessRS);

						MystiflyController.flightRules(

							t.fare_source_code,

							function( flightRulesSuccessRS ) {

								console.log('fare-rules success', flightRulesSuccessRS);

								var rules = FareRules.FareRule.RuleDetails.RuleDetail;

								if( t.fare_type != 'Public' ) {
									// THIS WILL SHOW A LINK AS THE FLIGHT RULES
									console.log( 'flight rules is a link', rules );
								}else{
									console.log('flight rules is NOT a link', rules);
									if( Array.isArray( rules ) ) {
										rules.forEach(function( elem, key ) {
											
										});
										console.log('rules is an array', rules);
									}else{
										console.log('rules is not an array', rules);
									}
								}

								searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();
								bookingSummary.update( searchSession );
							},

							function( flightRulesFailureRS ) {
								// MYSTIFLY TRANSPORT NOT AVAILABLE
								console.log('fare-rules fail', flightRulesFailureRS);
							}
						);
					},

					function( revalidateFailureRS ) {
						// MYSTIFLY TRANSPORT NOT AVAILABLE
						console.log('revalidate fail', revalidateFailureRS);
					}

				);
			}
		}


		function createAERequest( searchSession )
		{
			// var rq = {
			// 	Title : '',
			// 	FirstName : '',
			// 	LastName : '',
			// 	Address : null,
			// 	Email : '',
			// 	MobileNumber : '',
			// 	HotelRequests : []
			// };
			// searchSession.itinerary.forEach(function( leg, i ){
			// 	if( i.hotel.provider == 'ae' )
			// 	{
			// 		rq.HotelRequests.push({
			// 			HotelId : i.hotel_id,
			// 			RoomId : i.room_id,
			// 			MealId : i.meal_id,
			// 			ProviderId : i.provider_id,
			// 			GroupIdentifier : i.group_identifier,
			// 			ExpectedBookingPrice : i.price, // e.g. 1155.00
			// 			GuestName : '', // e.g. John Doe
			// 			Occupancy : {
			// 				RoomIndex : '',
			// 				Adults : ''
			// 			},
			// 			FromDate : '', // e.g. 2014-10-01T00:00:00
			// 			ToDate : '' // e.g. 2014-10-01T00:00:00
			// 		});
			// 	}
			// });	
			// return rq;
		}

		
		function validateAEHotel( h, leg )
		{
			var unvalidated = isNotUndefined( h.availability_checked_at ) ? isExpired( h.availability_checked_at ) : true;
			console.log('AE hotel should be validated:', unvalidated);
			if( unvalidated )
			{
				var rq = {
					FromDate: h.checkin,
					ToDate : h.checkout,
					Adults : searchSession.travellers,
					HotelId : h.hotel_id
				};
				AEController.checkHotelAvailability(
					rq,
					function( availabilitySuccessRS )
					{
						if( availabilitySuccessRS )
						{
							console.log('AE hotel:', h, 'AE hotel RQ:', rq, 'availabilitySuccessRS', availabilitySuccessRS);

							availabilitySuccessRS[0].rooms.forEach(function( room ){
								if( room.room_id == h.room_id )
								{
									// AN ERROR OCCURED HERE. CANNOT ACCESS SEARCHSESSION VARIABLE HERE. CONTINUE HERE
									var roomPrice = eroam.convertCurrency( h.price, h.currency );
									console.log('roomPrice', roomPrice);
									console.log('room.price', room.price);
									if( room.price != roomPrice )
									{
										searchSession.itinerary[ leg ].hotel.price = room.price;
										searchSession.itinerary[ leg ].hotel.old_price = h.price;
									}
									searchSession.itinerary[ leg ].hotel.provider_id = room.provider_id;
									searchSession.itinerary[ leg ].hotel.availability_checked_at = moment().format();
									bookingSummary.update( JSON.stringify( searchSession ) );
								}
							});
						}					
						else
						{
							console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'No data returned from validateAEHotel');
							// AE HOTEL NOT AVAILABLE
						}
					},
					function( availabilityFailureRS )
					{
						console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'availabilityFailureRS', availabilityFailureRS);
						// AE HOTEL NOT AVAILABLE
					}
				);
			}
		}


		function isExpired( availability_checked_at )
		{
			var difference = moment().diff( moment( availability_checked_at ), 'seconds' );
			console.log('difference', difference);
			return ( difference > 600 ) ? true : false ;
		}


		return {
			init: init,
		};


	})( MystiflyController, HBController );


	$(document).ready(function() {

		$('[data-toggle="tooltip"]').tooltip();
		review.init();
	}); // END OF DOCUMENT READY




</script>
<script src="<?php echo e(url( 'assets/js/theme/prettify.js' )); ?>"></script>
<script src="<?php echo e(url( 'assets/js/theme/jquery.slimscroll.js' )); ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {

    	$.validator.addMethod("lettersonly", function(value, element) {
		  return this.optional(element) || /^[a-z," "]+$/i.test(value);
		}, "Letters only please."); 
		$.validator.addMethod("numbersonly", function(value, element) {
		  return this.optional(element) || /^[0-9," "]+$/i.test(value);
		}, "Numbers only please."); 
    	$(".passenger_dob").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
        });
        $("#signup_form_id").validate({
        	ignore: [],
            rules: {
            	
                first_name: {
                    required: true,
                    lettersonly:true
                },
                last_name: {
                    required: true,
                    lettersonly:true
                },
                card_number: {
                    required: true,
                    number:true
                },
                year: {
                    required: true
                },
                cvv: {
                    required: true,
                    maxlength: 3,
                    number:true
                },
                month: {
                    required: true
                },
				country: {
				  required: true
				},
				 postalcode: {
				  required: true
				 }
            },

            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
                //alert('hi');
                //	payment();
				form.submit();

            }
        });

        $('.passenger_first_name').each(function () {
		    $(this).rules('add', {
		        required: true,
                lettersonly:true
		    });
		});
		$('.passenger_last_name').each(function () {
		    $(this).rules('add', {
		        required: true,
                lettersonly:true
		    });
		});
		$('.passenger_dob').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});
		$('.passenger_contact_no').each(function () {
		    $(this).rules('add', {
		        required: true,
		        numbersonly:true,
		        maxlength: 15,
		    });
		});
		$('.passenger_email').each(function () {
		    $(this).rules('add', {
		        required: true,
		        email:true
		    });
		});
		$('.passenger_country').each(function () {
		    $(this).rules('add', {
		        required: true
		    });
		});

        
        
        
        
        
    });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>