<?php $__env->startSection('content'); ?>

    <section class="banner-container">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class="first-slide" src="<?php echo e(url( 'assets/images/banner1.jpg' )); ?>" alt="First slide">
                </div>
                <div class="item">
                    <img class="third-slide" src="<?php echo e(url( 'assets/images/banner2.jpg' )); ?>" alt="Third slide">
                </div>
            </div>
            <div class="banner-search">
                <div class="banner-inner">
                    <p><img src="<?php echo e(url( 'assets/images/eRoam_Logo.png' )); ?>" alt="eroam" class="carousel-logo"></p>
                    <p>Over 500,000 hotels, 1,000 airlines, plus 100,000’s events, activities &amp; restaurants.</p>

                 <!--    <form method="post" action="search" id="search-form" class="form-horizontal clearfix">


                        <div class="col-md-10 col-md-offset-1" >
                            <div class="searchbox-new search-box1" style="display:none;">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 padding-right-0 banner-searchbox-new">
                                        <input type="text" name="project" id="project" class="form-control" placeholder="Where do you want to go?">

                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12 padding-left-0">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="searchbox-new search-box2">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="text" value="" name="start_location1" id="start_location1" class="form-control" placeholder="Start Location">
                                    </div>
                                    <div class="col-sm-3">
                                        <input id="travellers1" value="" type="number" name="travellers1" class="form-control" placeholder="No. of Travellers" min="1" max="10" />
                                    </div>
                                    <div class="col-sm-3">
                                        <input id="start_date1" value="" name="start_date1" type="text" class="form-control" placeholder="Start Date" >
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="searchbox-new search-box3" style="display:none;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <input type="text" value="" name="start_location2" id="start_location2" class="form-control" placeholder="Start Location">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" value="" name="end_location2" id="end_location2" class="form-control" placeholder="End Location">
                                    </div>
                                    <div class="col-sm-2">
                                        <input id="travellers2" value="" type="number" name="travellers2" class="form-control" placeholder="No. of Travellers" min="1" max="10" />
                                    </div>
                                    <div class="col-sm-2">
                                        <input id="start_date2" value="" name="start_date2" type="text" class="form-control" placeholder="Start Date" >
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-9 col-xs-12">
                                    <div class="checkbox-group">
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-01"><input type="radio" name="option" id="checkbox-01" value="packages">Tour Packages</label>
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-02"><input type="radio" name="option" id="checkbox-02" value="manual" checked>Tailormade Manual</label>
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-03"><input type="radio" name="option" id="checkbox-03" value="auto">Tailormade Auto</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-12 padding-0" style="display: none;">
                                    <p class="advance-right"><a href="#" class="advance-text">More Options <b class="caret"></b></a></p>
                                </div>
                            </div>
                        </div>
                    </form> -->
                     <form method="post" action="search" id="search-form" class="form-horizontal clearfix">
              <div class="col-md-10 col-md-offset-1">
                <input type="hidden" id="search_option_id" name="option" value="packages">
                    <div class="row" id="tour_package_id">
                      <div class="col-sm-3 padding-right-0">
                        <div class="dropdown packages search_dropdown_menu">
                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <span id="packages-option" class="packages-option" data-value="packages">Tour Packages</span> <span class="fa fa-angle-down"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="packages-option">
                            <li><a href="javascript://" value="packages">Tour Packages</a></li>
                              <li><a href="javascript://" value="manual">Tailormade Manual</a></li>
                              <li><a href="javascript://" value="auto">Tailormade Auto</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-sm-7 padding-0">
                        <input type="text" value="" name="project" id="project" class="form-control ui-autocomplete-input" placeholder="Where do you want to go?" autocomplete="off">
                      </div>
                      <div class="col-sm-2 padding-left-0">
                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                      </div>
                    </div>
                  </div>
          </form>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
        </div>
    </section>

   <!--  <section class="content-wrapper">
        <h1 class="hide"></h1>
        <article class="places-section eroam-trending">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2>TRENDING HOLIDAYS</h2>
                        <div class="places-list">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img1.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img2.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img3.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img4.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img5.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img1.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img3.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img5.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section> -->
    <section class="content-wrapper">
    <h1 class="hide"></h1>
    <article class="places-section eroam-trending">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
           <!--  <h2>TRENDING HOLIDAYS</h2> -->
            <h2>Top Tours</h2>
            <div class="places-list">

              <div id="tours-loader">
                <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
              </div>
              <div class="row" id="tourList"></div>
            </div>
          </div>
        </div>
      </div>
    </article>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>


<script>
var countryName   = '<?php echo e($countryName); ?>';
 var default_currency   = '<?php echo e($default_currency); ?>';
    var siteUrl = $( '#site-url' ).val();
	$(document).ready(function(){
         tourList();
        


		$('#onLoadLoginModal').click();

		var siteUrl = $('#site-url').val();
		
		$('input[class="form-control"]').focus(function(){
			$('.notification').html('');
		});

        $("#auth-form").validate({
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },

            },
            messages: {

                username: {
                    required: "Please enter Username."
                },
                password: {
                    required: "Please enter Password."
                },

            },
            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {

                authentication();

            }
        });
		//$('#authenticate-btn').click(function(e){
		function authentication() {
            //e.preventDefault();

            var data = $('#auth-form').serializeArray();

            if (validateData(data)) {
              
                $.ajax({
                    url: siteUrl + '/validate-auth-credentials',
                    method: 'POST',
                    data: validateData(data),
                    beforeSend: function () {
                        $('.error-message').hide();
                        $('.notification').css({color: 'rgb(0, 0, 0)'}).html('Checking').show();
                    },
                    success: function (response) {
                        if (response == 1) {
                            window.location.href = siteUrl;
                        }
                        else {
                            $('input[name="username"]').val('');
                            $('input[name="password"]').val('');
                            $('.notification').hide();
                            $('.error-message').css({color: 'red'}).html('Invalid Username or Password').show();
                        }
                    },
                    error: function () {
                        $('input[name="username"]').val('');
                        $('input[name="password"]').val('');
                        $('.notification').hide();
                        $('.error-message').css({color: 'red'}).html('An error has occured. Please try again.').show();
                    }
                });
            }
            else {
                $('.notification').hide();
                $('.error-message').css({color: 'red'}).html('Invalid Username or Password').show();
            }
        }
	});

	function validateData( data ) {
		var result = true;
		var values = {};
		try {
			data.forEach(function( d )
			{
				if( typeof d.value !== 'undefined' )
				{
					if( d.value.trim() == '' )
					{
						result = false;
					}
				}
				else
				{
					result = false;
				}
				values[d.name] = d.value;
			});

			if( result === true )
			{
				result = values;
			}
		} 
		catch( err )
		{
			console.log( 'An error has occured while validating the input fields', err );
			result = false;
		}
		return result;
	}
     function tourList(next){
        tour_data = {
          countryName: countryName,
          provider: 'getTours'
        };

        // CACHING HOTEL BEDS
        var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'getTours', true);

        eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
         console.log('tourResponse', tourResponse);
          if( tourResponse.length > 0 ){
            $.each( tourResponse, function( key, value ) {
              //console.log('value : '+ key + value);
              appendTours( value );
            })

          }
        })
      }
    function getStars(count, half = false){
        var stars = '';
        if( parseInt( count ) ){
          for( star = 1; star <= count; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
          }
          var emptyStars = 5 - parseInt( count );
          if(half){
            stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
            emptyStars = emptyStars - 1;
          }
          for( empty = 1; empty <= emptyStars; empty ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }else{
          for( star = 1; star <= 5; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }
         var stars = '';
        return stars;
      }  
    function appendTours(tour){

        $("#tours-loader").hide();
        var price = parseFloat(tour.price).toFixed(2);
        var star;
        var rating = tour.rating;

        if(rating % 1 === 0){
          stars = getStars( rating);
        }else {
          stars = getStars( rating, true );
        }

        var imgurl = 'http://www.adventuretravel.com.au'+tour.DiscountPercentage+'245x169/'+tour.OriginalPrice;

        $("#overlay").hide();
          if(parseInt(tour.no_of_days) == 1) {
              var day = 'Day';
          }else{
              var day = 'Days';
          }
        var html = '<div class="col-md-3 col-sm-4 m-t-20"><a href="/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'">'+
                  '<div class="place-wrapper">'+
                    '<div class="place-image">'+
                      '<img src="'+imgurl+'" alt="" class="img-responsive" id="tourImage_'+tour.tour_id+'">'+
                    '</div>'+
                    '<div class="place-inner">'+
                      '<div class="row">'+
                        '<div class="col-xs-6">'+

                            '<p><strong>' + parseInt(tour.no_of_days) + day +'</strong></p>' +
                        '</div>'+
                        '<div class="col-xs-6 text-right">'+
                          '<ul class="rating">'+stars+'</ul>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                  '<div class="place-details">'+
                      '<p> '+tour.tour_title+' - From '+default_currency+' '+price+' Per Person</p>'+
                  '</div>'+
                '</a></div>';
        $('#tourList').append(html);

        imageUrl = checkImageUrl(tour.tour_id, imgurl);
      }
      function checkImageUrl(id, url){
        eroam.ajax('post', 'existsImage', {url : url}, function(response){
          if(response == 200){
            //var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
            //$("#tourImage_"+id).attr('src', image);
          } else if(response == 400){
            //var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
            //$("#tourImage_"+id).attr('src', image);
          } else {
              var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
              $("#tourImage_"+id).attr('src', image);
          }
        });  
      }
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.home2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>