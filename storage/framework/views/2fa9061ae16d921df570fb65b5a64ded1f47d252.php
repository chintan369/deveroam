<?php
  $str  = json_encode($return_response);
  //echo "<pre>";print_r($return_response);
?>



<?php $__env->startSection('custom-css'); ?>
<style>
  .wrapper {
    background-image: url( <?php echo e(url( 'assets/img/bg1.jpg' )); ?> ) !important;
  }
  .red{
    color: red;
  }
  .top-margin{
    margin-top: 5px;
  }
  .container-padding{
    padding-left: 35px;
    padding-right: 35px;
  }
  .top-bottom-paddings{
    padding-top: 20px;
    padding-bottom: 20px;
  }
  .white-box{
    background: rgba(255, 255, 255, 0.9);
    border: solid thin #9c9b9b !important;
  }

  .wrapper{
    background-attachment: fixed;
  }

.padding-20{
    padding-left: 20px;
    padding-right: 20px;
  }
  
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>




    <div id="about-us">
    <!-- <div class="top-section-image">
      <img src="<?php echo e(asset('assets/images/bg-image.jpg')); ?>" alt="" class="img-responsive">
    </div> -->

    <section class="content-wrapper">
      <article class="tankyou-page">
        <div class="container-fluid">
          <div class="col-md-10 col-md-offset-1">
            <div class="m-t-50">
              <h1 class="text-center">Thank you for booking your trip with us !</h1>
              <div class="m-t-80">
                <div class="row">
                  <div class="col-sm-8">
                    <h3 class="booking-title">Booking Information</h3>
                  </div>
                  <div class="col-sm-4">
                    <form method="post" action="view-order-pdf">
                      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                      
                      <input type="hidden" name="return_response" value="<?php echo e($str); ?>">
                    
                    <button type="submit" name="" formtarget="_blank" class="btn btn-primary btn-block">View PDF</button>
                    </form>
                  </div>
                </div>
                <?php
                  if(isset($return_response) && !empty($return_response)){
                      $name = $return_response['passenger_info'][0]['passenger_first_name'].' '.$return_response['passenger_info'][0]['passenger_last_name'];
                      $email = $return_response['passenger_info'][0]['passenger_email'];
                      $contact = $return_response['passenger_info'][0]['passenger_contact_no'];
                      $gender = $return_response['passenger_info'][0]['passenger_gender'];
                      $dob = date('d-m-Y',strtotime($return_response['passenger_info'][0]['passenger_dob']));
                      $country = $return_response['passenger_info'][0]['passenger_country_name'];
                ?>    
                  <div class="box-wrapper m-t-30">
                    <h4>Personal Details</h4>
                    <p class="m-t-20"><strong>Booking Id:</strong> <?php echo e($return_response['invoiceNumber']); ?></p>
                    <div class="row">
                      <div class="col-sm-6">
                        <p><strong>Lead Person Name:</strong> <?php echo e($name); ?></p>
                      </div>
                      <div class="col-sm-6">
                        <p><strong>Lead Person Email:</strong> <?php echo e($email); ?></p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <p><strong>Lead Person Contact:</strong> <?php echo e($contact); ?></p>
                      </div>
                      <div class="col-sm-6">
                        <p><strong>Lead Person Gender:</strong> <?php echo e($gender); ?></p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <p><strong>Lead Person Dob:</strong> <?php echo e($dob); ?></p>
                      </div>
                      <div class="col-sm-6">
                        <p><strong>Lead Person Country:</strong> <?php echo e($country); ?></p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <p><strong>Days:</strong> <?php echo e($return_response['total_days']); ?></p>
                      </div>
                      <div class="col-sm-6">
                        <p><strong>Total Amount:</strong> <?php echo e($return_response['currency']); ?> <?php echo e($return_response['totalAmount']); ?></p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <p><strong>Date:</strong> <?php echo e(date( 'j M Y', strtotime($return_response['from_date']))); ?> - <?php echo e(date( 'j M Y',strtotime($return_response['to_date']))); ?></p>
                      </div>
                      <div class="col-sm-6">
                        <p><strong>Travellers:</strong> <?php echo e($return_response['travellers']); ?></p>
                      </div>
                    </div>
                  </div>

                  <?php 
                
                $last_key = count($return_response['leg']) -1;
                foreach ($return_response['leg'] as $key => $value) {

                    ?>
          <div class="box-wrapper">
            <h4><?php echo e($value['from_city_name']); ?>, <?php echo e($value['country_code']); ?></h4>
            <?php 
                if(isset($value['hotel']) && !empty($value['hotel'])){
            ?>
            <div class="m-t-20">
              <h5><i class="fa fa-hotel"></i> Accommodation</h5>
                
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Accommodation Name:</strong> <?php echo e($value['hotel'][0]['leg_name']); ?></p>
                  </div>
                  <div class="col-sm-6">
                    <p><strong>Duration:</strong> <?php echo e($value['hotel'][0]['nights']); ?> Nights</p>
                  </div>
                </div>  
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Check-in:</strong> <?php echo e(date( 'j M Y', strtotime($value['hotel'][0]['checkin']))); ?></p>
                  </div>
                  <div class="col-sm-6">
                    <p><strong>Check-out:</strong> <?php echo e(date( 'j M Y', strtotime($value['hotel'][0]['checkout']))); ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Price:</strong> <?php echo e($value['hotel'][0]['currency']); ?> <?php echo e(ceil($value['hotel'][0]['price'])); ?></p>
                  </div>
                  <div class="col-sm-6">
                     <?php  
                      $confirm = 'NOT CONFIRMED';
                      if($value['hotel'][0]['provider_booking_status'] == 'CF'){
                          $confirm = 'CONFIRMED';
                      }
                     ?>
                    <p><strong>Booking Status:</strong> <?php echo e($confirm); ?>

                    <?php
                    if(empty($value['hotel'][0]['provider_booking_status'])){
                      ?>
                       <span class="red"> *</span>
                      <?php
                    }
                    ?>
                    </p> 
                  </div>
                </div>
                <?php
                    if(!empty($value['hotel'][0]['provider_booking_status'])){
                      ?>
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Booking Id:</strong> <?php echo e($value['hotel'][0]['booking_id']); ?>

                    </p> 
                  </div>
                </div>
                <?php
                    }
                ?>
          </div>
              <?php 
                }else{
                  ?>
                  <div class="m-t-20">
                    <h5><i class="fa fa-hotel"></i> Accommodation</h5>
                    <div class="row">
                      <div class="col-sm-6">
                        <p>Own Arrangement</p>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                 if(isset($value['activities']) && !empty($value['activities'])){
                  foreach ($value['activities'] as $key1 => $value1) {
                    
              ?>

              <div class="m-t-20">
                <h5><i class="fa fa-map-o"></i> Activity Summary</h5>
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Activity Name:</strong> <?php echo e($value1['leg_name']); ?></p>
                  </div>
                  <div class="col-sm-6">
                    <p><strong>Date:</strong> <?php echo e(date( 'j M Y', strtotime($value1['from_date']))); ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Price:</strong> <?php echo e($value1['currency']); ?> <?php echo e(ceil($value1['price'])); ?></p>
                  </div>
                  <div class="col-sm-6">
                    <p><strong>Booking Status:</strong> <?php echo e(($value1['provider_booking_status'] == 'CONFIRMED')? 'CONFIRMED':'NOT CONFIRMED'); ?>

                    <?php
                    if($value1['provider_booking_status'] != 'CONFIRMED'){  
                      ?>
                       <span class="red"> *</span>
                      <?php
                    }
                    ?>
                    </p> 
                  </div>
                </div>
                <?php
                    if($value1['provider_booking_status'] == 'CONFIRMED'){  
                      ?>
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Booking Id:</strong> <?php echo e($value1['booking_id']); ?></p> 
                  </div>
                </div>
              <?php 
                }
              ?>    
                 
              </div>
              <?php 
              }
                }else{
                  ?>
                  <div class="m-t-20">
                    <h5><i class="fa fa-hotel"></i> Activity Summary</h5>
                    <div class="row">
                      <div class="col-sm-6">
                        <p>Own Arrangement</p>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                if(isset($value['transport']) && !empty($value['transport'])){
              ?>
             
                <?php 
                  $depart = $value['transport'][0]['departure_text'];
                  $arrive = $value['transport'][0]['arrival_text'];
                  if($value['transport'][0]['leg_name'] == 'Flight'){
                      if(!empty($value['transport'][0]['booking_summary_text'])){
                        $leg_depart = explode("Depart:",$value['transport'][0]['booking_summary_text'],2);
                        $leg_arrive = explode("Arrive:",$value['transport'][0]['booking_summary_text'],2);
                        
                        if(isset($leg_depart[1])){
                          $depart = explode("</small>",$leg_depart[1],2);
                          $depart = $depart[0];
                        }else{
                          $depart = '';
                        }
                        if(isset($leg_arrive[1])){
                          $arrive = explode("</small>",$leg_arrive[1],2);
                          $arrive = $arrive[0];
                        }else{
                          $arrive = '';
                        }
                      }else{
                         $depart = '';
                         $arrive = '';
                      }
                  }
              if($value['transport'][0]['provider'] == 'busbud'){


                      $depart = $value['transport'][0]['departure_text'];
                      $arrive = $value['transport'][0]['arrival_text'];

              }

                ?>
              <div class="m-t-20">
                <h5><i class="fa fa-bus"></i> Transport</h5>  
                <div class="row">
                  <div class="col-sm-12">
                    <?php 
                      $leg_name = explode('Depart:', $value['transport'][0]['booking_summary_text']);
                      if(isset($value['transport'][0]['booking_summary_text']) && !empty($value['transport'][0]['booking_summary_text'])){
                        $leg_name = trim(strip_tags($leg_name[0]));
                      }else{
                        $leg_name = strip_tags($value['transport'][0]['leg_name']);
                      }
                    ?>
                    <p><strong>Service:</strong> <?php echo e($leg_name); ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <p><strong>Departure:</strong> <?php echo e(strip_tags($depart)); ?></p>
                  </div>
                  <div class="col-sm-6 col-sm-offset-1">
                    <p><strong>Arrival:</strong> <?php echo e(strip_tags($arrive)); ?></p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Price:</strong> <?php echo e($value['transport'][0]['currency']); ?> <?php echo e(ceil($value['transport'][0]['price'])); ?></p>
                  </div>
                  <div class="col-sm-6">
                    <p><strong>Booking Status:</strong> <?php echo e(($value['transport'][0]['provider_booking_status'] == 'CONFIRMED')? 'CONFIRMED':'NOT CONFIRMED'); ?>

                    <?php
                    if($value['transport'][0]['provider_booking_status'] != 'CONFIRMED'){
                      ?>
                       <span class="red"> *</span>
                      <?php
                    }
                    ?>
                    </p> 
                  </div>
                </div>
                <?php
                    if($value['transport'][0]['provider_booking_status'] == 'CONFIRMED'){
                      ?>
                 <div class="row">  
                    <div class="col-sm-6">
                      <p><strong>Booking Id:</strong> <?php echo e($value['transport'][0]['booking_id']); ?>

                      </p> 
                    </div>
                </div>
                <?php 
                }
                ?>
              </div>  
               <?php
            }else{
              if($key != $last_key){
                ?>
              <div class="m-t-20">
                <h5><i class="fa fa-hotel"></i> Transport 
                  
                  </h5>
                <div class="row">
                  <div class="col-sm-6">
                    <p>Self-Drive / Own Arrangement</p>
                  </div>
                </div>
              </div>
                  <?php
              }
                  
          }
          ?>
            
          </div>
              <?php
                  }
                }
                ?>

            </div>
          </div>
          <p><span class="red">*</span> For NOT CONFIRMED Booking Please contact to eRoam Administrator <a href="<?php echo e(URL('contact-us')); ?>" target="_blank">Click Here</a></p>      
        </div>
      </article>
    </section>
    </div>
  

<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.static', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>