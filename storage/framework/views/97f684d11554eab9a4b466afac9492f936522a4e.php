<?php $__env->startSection('custom-css'); ?>
  <style type="text/css">
   .tabs-wrapper {
        /*margin-left: 350px;*/
        padding-top: 48px;
       /* height: 995px;*/
    }
    .modelCountryList label.radio-checkbox span{
        color: rgba(0,0,0, .54);
    }

    .jcarousel-wrapper {
        margin: 20px auto 55px auto;
    }

    .jcarousel li {
        padding-left: 5px;
        padding-right: 5px;
    }

    .detailDesc h1{
      font-size: 13px;
      /*position: absolute;
      left: 90px;*/
    }

    .reviewComment {
      padding-left:  25px;
      text-align: justify;
      padding-top: 5px;
    }

    .detailDesc ul{
      padding-left: 25px;
    }

    .tour-itinerary, .tour-viewItinerary{
      display:none;
    }

    .has-js .dateDiv label.r_on {
       background: url("<?php echo url('assets/images/check-on-black1.png'); ?>") no-repeat; 
    }

    .has-js .dateDiv .label_radio {
        background: url("<?php echo url('assets/images/check-off-black1.png'); ?>") no-repeat; 
    }

    .location-wrapper {
          height: 900px;
    }

    .panel-group { margin-bottom: 0px;}
  </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <?php 
    //echo '<pre>'; print_r($data['detail']);  print_r($data['dates']);  echo '</pre>';  die;
    //echo '<pre>'; print_r($data);  echo '</pre>';  die;

    /** Review List & Review Star Start **/
        $i=0; $rating = 0; 
        $reviewsDetails = '<h5>No Review Found.</h5>';
        $end = '';
  
        foreach ($data['reviews'] as $review) {

          if($i == 0){
            $reviewsDetails = '<ul class="tour-list">';
            $end = '</ul>';
          } 

          if($review['comments'] != ''){
              $comment = $review['comments'];
          }
          elseif($review['review_detail'] != ''){
            $comment = $review['review_detail'];
          } else {
            $comment = 'N/A';
          }

          if(is_float($review['rating'])){
            $stars = get_stars((int)$review['rating'], true);
          }else {
            $stars = get_stars((int)$review['rating']);
          }

          $reviewsDetails .= '<div>
            <div class="row">
              <div class="col-sm-10"><i class="fa fa-circle" aria-hidden="true" style="font-size: 7px;"></i> &nbsp;&nbsp;&nbsp;&nbsp;<strong>'.$review['email'].'</strong></div>
              <div class="col-sm-2"><ul class="rating pull-right">'.$stars.'</ul></div>
              <div class="clearfix"></div>
            </div>
            <p class="reviewComment">'.$comment.'</p>
          </div>';

          $i++;
          $rating += $review['rating'];
        }
        $reviewsDetails .=$end;
        $reviewCount = $i;

        if($rating > 0){ $starRating = $rating/$i; } 
        else { $starRating = 0; }
    /** Review List & Review Star End **/
  ?>


      <div class="tabs-content-container">
      <div class="tab-content" id="myTabContent"> 

        <div class="tab-pane fade in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab"> 
          <div class="tour-wrapper">
            <div class="list-box list-box-new">
              <div class="tabs-content-container-new">
              <div class="tour-top-content">
              <div class="row">
                <div class="col-sm-7">
                  <h4><?php echo e($data['detail']['tour_title']); ?></h4>
                  <p>
                    <svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">
                                <g id="Group" transform="translate(501.000000, 542.000000)">
                                    <g id="Group-16">
                                        <path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                    Tour Code: <?php echo e($data['detail']['tour_code']); ?>   |   Location: <?php echo e($data['detail']['destination'].', '.$data['detail']['tripRegion']); ?>

                  </p>
                </div>
                <div class="col-sm-5">
                  <div class="row">
                    <div class="col-xs-9 m-t-10 text-right border-right">
                      <p>From <strong><?php echo e($default_currency.' '.number_format($data['detail']['price'],2)); ?></strong> Per Person</p>
                      <ul class="rating">
                        <?php 
                          if(is_float($starRating)){
                            $stars = get_stars((int)$starRating, true);
                          }else {
                            $stars = get_stars((int)$starRating);
                          }
                         // echo $stars; 
                        ?>
                      </ul>
                    </div>
                    <div class="col-xs-3 text-center">
                      <h3 class="transport-price"><strong><?php echo e($data['detail']['no_of_days_text']); ?></strong></h3>
                      Days
                    </div>
                  </div>
                </div>
              </div>
              <hr/>

                <div class="clearfix">
                  <div class="logo-img">
                    <img src="<?php echo e(url( 'assets/images/tour-logo.png' )); ?>" alt="" class="img-responsive" />
                  </div>
                  <div class="logo-details">
                    <div class="row">
                      <div class="col-md-2"><strong>Countries:</strong></div>
                      <div class="col-md-9">
                        <?php 
                          $allCountry = '';
                          foreach ($data['countries'] as $country) {
                            $allCountry .= ucwords(strtolower($country['country_name'])).', ';
                          }
                          echo substr($allCountry, 0,-2);
                        ?> 
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2"><strong>Route:</strong></div>
                      <div class="col-md-9"><?php echo e($data['detail']['destination'].' - '.$data['detail']['departure']); ?></div>
                    </div>
                  </div>
                </div>

                <div class="jcarousel-wrapper">
                    <div class="jcarousel">
                      <ul>
                        <?php 
                          $i=0;
                          foreach($data['images'] as $key => $image){
                            $i++
                        ?>
                              <li><img src="http://www.adventuretravel.com.au/<?php echo e($data['detail']['folder_path']); ?>245x169/<?php echo e($image['image_thumb']); ?>" alt="Image <?php echo e($i); ?>"></li>
                        <?php } ?>
                      </ul>
                    </div>
                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                    <p class="jcarousel-pagination"></p>
                </div>

                <div class="m-t-20 row">
                  <div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block bookItinerary">BOOK ITINERARY</button>
                  </div>
                  <!--<div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block disable">CUSTOMISE ITINERARY</button>
                  </div>-->

                  <div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block viewItinerary">VIEW ITINERARY</button>
                  </div>

                  <div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block saveItinerary">SAVE ITINERARY</button>
                  </div>

                  <div class="col-sm-4 m-t-10" style="display:none;">
                    <button type="submit" name="" class="btn btn-primary btn-block backToDetail">BACK TO DETAILS</button>
                  </div>
                </div>

              </div>

                <!-- Tour Details Start -->
                <div class="tour-detail tour-bottom-content">
                    <div class="tour-tabs custom-tabs m-t-10" data-example-id="togglable-tabs">
                      <ul class="nav nav-tabs" id="myTabs1" role="tablist">
                        <li role="presentation" class="active"><a href="#overview" id="overview-tab" role="tab" data-toggle="tab" aria-controls="overview" aria-expanded="true">OVERVIEW</a></li>
                      <!--<li role="presentation"><a href="#review" id="review-tab" role="tab" data-toggle="tab" aria-controls="review" aria-expanded="true">REVIEWS (<?php echo e($reviewCount); ?>)</a></li> -->
                        <li role="presentation"><a href="#details" id="details-tab" role="tab" data-toggle="tab" aria-controls="details" aria-expanded="true">DETAILS</a></li>
                        <!--<li role="presentation"><a href="#location1" id="location1-tab" role="tab" data-toggle="tab" aria-controls="location1" aria-expanded="true">LOCATION</a></li>-->
                      </ul>
                    </div>
                  <div class="tourDetailDiv">
                    
                    <div class="tabs-content-container">
                      <div class="tab-content" id="myTabContent1"> 
                        <div class="tab-pane fade in active" role="tabpanel" id="overview" aria-labelledby="overview-tab"> 
                          <div class="m-t-10 detailDesc">
                            
                            <p><?php echo $data['detail']['long_description']; ?></p>
                            <div class="m-t-20 row">
                              <div class="col-sm-6">
                                <div class="row">
                                  <div class="col-xs-4"><strong>Start:</strong></div>
                                  <div class="col-xs-8"><?php echo e($data['detail']['departure']); ?> </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-4"><strong>Finish:</strong></div>
                                  <div class="col-xs-8"> <?php echo e($data['detail']['destination']); ?> </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-4"><strong>Countries:</strong></div>
                                  <div class="col-xs-8"><?php echo substr($allCountry, 0,-2);?></div>
                                </div>
                              </div>

                              <div class="col-sm-6">
                                <div class="row">
                                  <div class="col-xs-4"><strong>Theme:</strong></div>
                                  <div class="col-xs-8">Explorer</div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-4"><strong>Ages:</strong></div>
                                  <div class="col-xs-8">Min 12</div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-4"><strong>Group Size:</strong></div>
                                  <div class="col-xs-8"><?php echo e('Min '.$data['detail']['groupsize_min'].', Max '.$data['detail']['groupsize_max']); ?></div>
                                </div>
                              </div>
                            </div>

                            <?php if($data['detail']['supplier_desc']): ?>
                              <div class="m-t-20">
                                <div class="m-t-20">
                                  <?php echo $data['detail']['supplier_desc']; ?>
                                </div>
                              </div>
                            <?php endif; ?>
                          </div>
                        </div>

                        <div class="tab-pane fade" role="tabpanel" id="review" aria-labelledby="review-tab"> 
                          <div class="m-t-10 detailDesc">
                            <?php echo $reviewsDetails; ?>
                          </div>
                        </div> 

                        <div class="tab-pane fade" role="tabpanel" id="details" aria-labelledby="details-tab"> 
                          <?php echo $data['detail']['xml_PracticalDetail']; ?>
                        </div> 

                        <div class="tab-pane fade" role="tabpanel" id="location1" aria-labelledby="location1-tab"> 
                          <?php 
                            $locations = explode(',', $data['detail']['tripCountries']);
                            if(!empty($locations)){
                              $locationText = '<ul class="tour-list">';
                              foreach ($locations as $key => $value) {
                                $locationText .= '<li>'.$value.'</li>';
                              }
                              $locationText .= '</ul>';
                              echo $locationText;
                            }
                          ?>
                        </div>
                       </div> 
                    </div>
                  </div>
                </div>
                <!-- Tour Details End -->

                <!-- Itinerary Book Start -->
                  <div class="panel-group tour-accordian tour-itinerary" id="accordion1" role="tablist" aria-multiselectable="true">
                    <div class=" m-t-20">
                      <div class="bookingDates tour-bottom-content">
                        <div class="row text-center desktop-block">
                          <div class="col-md-3 col-sm-3 col-xs-6 text-left">
                            <p class="depart-text">DEPARTING <a href="#"><i class="fa fa-caret-down"></i></a></p>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-6 text-left">
                            <p class="finish-text">FINISHING <a href="#"><i class="fa fa-caret-down"></i></a></p>
                          </div>
                          <!--<div class="col-md-2 col-sm-2 col-xs-3 black-box">
                            <p>AVAILABLE</p>
                          </div>-->
                          <div class="col-md-5 col-sm-3 col-xs-7">
                            <p>TOTAL FROM $AUD <a href="#"><i class="fa fa-caret-down"></i></a></p>
                          </div>
                          <div class="col-md-1 col-sm-1 col-xs-2 text-right"></div>
                        </div>
                        <div class="tourItineraryDiv">
                        <?php 

                            $i=0; $totalCost = 0;
                            $startDate  = '';
                            $finishDate ='';
                            //echo '<pre>'; print_r($data['dates']);  echo '</pre>'; 
                            if(!empty($data['dates'])){
                              foreach ($data['dates'] as $date) {
                                //echo '<pre>'; print_r($date);  echo '</pre>'; 
                                $i++;
                                if($i == 1){ 
                                  $totalCost = $date['total']; 
                                  $startDate = date('Y-m-d H:i:s', strtotime($data['dates'][0]['startDate']));
                                  $finishDate = date('Y-m-d H:i:s', strtotime($data['dates'][0]['finishDate']));

                                }
                        ?>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingPicks<?php echo e($i); ?>" data-toggle="collapse" data-parent="#accordion1" data-target="#collapsePicks<?php echo e($i); ?>"  aria-expanded="true" <?php /*href="#collapsePicks{{ $i }}" aria-controls="collapsePicks{{ $i }}"*/ ?> >
                              <div class="panel-title row text-center">
                                <div class="col-md-3 col-sm-3 col-xs-4 text-left">
                                  <p class="mobile-block">DEPARTING <a href="#"><i class="fa fa-caret-down"></i></a></p>
                                  <span><?php echo e(convert_date($date['startDate'], 'new_eroam_format')); ?></span>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-4 text-left">
                                  <p class="mobile-block">FINISHING <a href="#"><i class="fa fa-caret-down"></i></a></p>
                                  <span><?php echo e(convert_date($date['finishDate'], 'new_eroam_format')); ?></span>
                                </div>
                                <!--<div class="col-md-2 col-sm-2 col-xs-4 black-box dateDiv">
                                  <p class="mobile-block">AVAILABLE</p>
                                  <span><label class="radio-checkbox label_check" for="checkbox-d<?php echo e($i); ?>" ><input disabled type="checkbox" id="checkbox-d<?php echo e($i); ?>" checked >&nbsp;</label></span>
                                  <input type="hidden" id="startDate<?php echo e($i); ?>" value="<?php echo e(date('Y-m-d H:i:s', strtotime($date['startDate']))); ?>" >
                                  <input type="hidden" id="finishDate<?php echo e($i); ?>" value="<?php echo e(date('Y-m-d H:i:s', strtotime($date['finishDate']))); ?>" >
                                </div>-->
                                <div class="col-md-5 col-sm-3 col-xs-10 person-price">
                                  <p class="mobile-block">TOTAL FROM <?php echo e($default_currency); ?> <a href="#"><i class="fa fa-caret-down"></i></a></p>
                                  <span><?php echo e(number_format($date['total'],2)); ?> Per Person</span>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-2 collapse-icon">
                                  <a role="button"  <?php echo ($i!=1)?'class="collapsed"':''; ?>><i class="fa fa-minus-circle"></i></a>
                                </div>
                              </div>
                            </div>
                            <div id="collapsePicks<?php echo e($i); ?>" class="panel-collapse collapse <?php echo ($i==1)?'in':''; ?>" role="tabpanel" aria-labelledby="headingPicks<?php echo e($i); ?>">
                              <div class="panel-body">
                                <h4><?php echo e($date['departureName']); ?></h4>
                                <div class="row">
                                  <div class="col-sm-4">
                                    <p>
                                      <svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                          <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                                          <desc>Created with Sketch.</desc>
                                          <defs></defs>
                                          <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                              <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">
                                                  <g id="Group" transform="translate(501.000000, 542.000000)">
                                                      <g id="Group-16">
                                                          <path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>
                                                      </g>
                                                  </g>
                                              </g>
                                          </g>
                                      </svg>
                                      Tour Code: <?php echo e($date['departureCode']); ?>

                                    </p>
                                  </div>
                                  <div class="col-sm-8">
                                    <div class="row">
                                      <div class="col-md-5 col-sm-6 text-right">
                                        <div class="price-block">
                                          <div class="price-inner">
                                            <div><?php echo e($default_currency); ?> <span class="transport-price"><strong><?php echo e(number_format($date['total'],2)); ?></strong></span></div>
                                            per person in a twin share room
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-7 col-sm-6">
                                        <input type="hidden" class="dateRedio" name="date" id="radio-d<?php echo e($i); ?>" value="<?php echo e($date['total']); ?>" >
                                        <button type="button" name="" class="btn btn-primary btn-block active bookButton" id="bookButton_<?php echo e($i); ?>" >BOOK ITINERARY</button>
                                        <?php /* <a href="#Book1" class="btn btn-primary btn-block active bookButto" >BOOK ITINERARY</a>
                                        <button type="button" name="" class="btn btn-primary btn-block">CUSTOMISE ITINERARY</button>*/ ?>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <p class="notes m-t-30">All prices include GST, Government taxes and are one way per person in Australian dollars. Payment fees may apply depending on your payment method. Additional Servicing Fee and / or Booking Price Guarantee can include multiple passengers and products. Total price for all passengers will be shown after you select your flight(s). To view the fare rules, <a href="#"><strong>click here</strong></a>.</p>
                              </div>
                            </div>
                          </div>
                        <?php } } else {  echo '<div><center>No records found.</center></div>'; }  ?>
                      </div>
                      </div>

                      <!-- Save Itinerary Start -->
                        <a id="Book"></a>
                        <div class="bookingFrom" style="display:none;">
                          <div class="tour-bottom-content">
                            <div class="bookScroll">
                          <form class="horizontal-form" action="<?php echo e(url('tourPayment')); ?>" method="post" name="tour_form" id="tour_form_id">
                            <?php echo e(csrf_field()); ?>

                            <section>
                              <div>
                                <div class="row">
                                  <div class="col-sm-4">
                                    <div class="input-field select-box-new">
                                      <select class="form-control" id="NoOfPass" name="NoOfPass">
                                        <option value="">No of Passenger</option>
                                        <?php
                                            for($i=1; $i<=10; $i++) {
                                              if($i==1){$select = 'selected';} else { $select = '';}
                                              echo '<option value="'.$i.'" '.$select.'>'.$i.'</option>';
                                            }
                                        ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="display-field">
                                      <span id="pricePP"><?php echo e($default_currency.' '.number_format($totalCost,2)); ?></span> / Per Adult Passenger
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="display-field">
                                      <strong>Total Amount</strong>   <span id="TotalPricePP"><?php echo e($default_currency.' '.number_format($totalCost,2)); ?></span>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <h4 class="m-t-30">Lead Person Information</h4>
                              <div class="m-t-10">
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="input-field select-box-new">
                                      <select class="form-control passenger_title" name="passenger_title[]">
                                          <option value="Mr">Mr</option>
                                          <option value="Ms">Ms</option>
                                          <option value="Mrs">Mrs</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="input-field">
                                      <input id="pax-fname" type="text" name="passenger_first_name[1]" class="passenger_first_name">
                                      <label for="pax-fname" class="">First Name</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="input-field">
                                      <input id="pax-lname" type="text" name="passenger_last_name[1]" class="passenger_last_name">
                                      <label for="pax-lname" class="">Last Name</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="m-t-30 radio-blackgroup">
                                      <label class="radio-checkbox label_radio m-r-10" for="radio-m11"><input type="radio" name="passenger_gender[1]" id="radio-m11" value="male" checked="">Male</label>
                                      <label class="radio-checkbox label_radio" for="radio-m12"><input type="radio" name="passenger_gender[1]" id="radio-m12" value="female">Female</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-3">
                                    <div class="input-field">
                                      <input id="pax-dob" type="text" name="passenger_dob[1]" class="passenger_dob">
                                      <label for="pax-dob" class="" id="dob">Date of Birth</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="input-field">
                                      <input id="pax-contact" type="text" name="passenger_contact_no">
                                      <label for="pax-contact">Contact No.</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="input-field">
                                      <input id="pax-email" type="text" name="passenger_email">
                                      <label for="pax-email">Email</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="input-field select-box-new">
                                      <select class="form-control" name="passenger_country">
                                        <?php 
                                          $allCountry = array();
                                          $j=0;
                                          foreach($countries as $country){
                                            foreach ($country['countries'] as $country_data){
                                                $allCountry[$j]['name'] = $country_data['name'];
                                                $allCountry[$j]['id'] = $country_data['id'];
                                                $j++;
                                            } 
                                          }
                                          usort($allCountry, 'sort_by_name');
                                        ?>

                                        <?php if( count($allCountry) > 0 ): ?>
                                          <?php foreach($allCountry as $Country): ?>
                                            <option value="<?php echo e($Country['name']); ?>"><?php echo e($Country['name']); ?></option>
                                          <?php endforeach; ?>
                                        <?php endif; ?>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div id="OtherPersons"> </div>
                            </section>

                            <h4 class="m-t-30">Payment Confirmation</h4>
                            <div class="table-responsive m-t-20">
                              <?php
                                $travellers = 1;
                                $total = $totalCost*$travellers;
                                $gst = $total*0.025;
                                $finaltotal1 = $total + $gst;
                                $finalTotal = number_format($total + $gst, 2, '.', ',');
                              ?>

                              <table class="table">
                                <tbody>
                                  <tr>
                                    <td width="80%">Total Per Person</td>
                                    <td><?php echo e($default_currency); ?></td>
                                    <td id="total"><?php echo e(number_format($totalCost, 2, '.', ',')); ?></td>
                                  </tr>
                                  <tr>
                                    <td>Sub Total Amount</td>
                                    <td><?php echo e($default_currency); ?></td>
                                    <td id="subtotal"><?php echo e(number_format($total, 2, '.', ',')); ?></td>
                                  </tr>
                                  <tr>
                                    <td>Credit Card Fee's 2.5%</td>
                                    <td><?php echo e($default_currency); ?></td>
                                    <td id="ccfee"><?php echo e(number_format($gst, 2, '.', ',')); ?></td>
                                  </tr>
                                  <tr>
                                    <td><strong>Total Amount Due</strong></td>
                                    <td><strong><?php echo e($default_currency); ?></strong></td>
                                    <td id="finaltotal"><strong><?php echo e($finalTotal); ?></strong></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <div class="m-t-20">
                              <h4>Credit Card Details</h4>

                              <input type="hidden" name="tour_id" value="<?php echo e($data['detail']['tour_id']); ?>">
                              <input type="hidden" name="Title" value="<?php echo e($data['detail']['tour_title']); ?>">
                              <input type="hidden" name="code" value="<?php echo e($data['detail']['code']); ?>">
                              <input type="hidden" name="provider" value="<?php echo e($data['detail']['provider']); ?>">
                              <input type="hidden" name="departure_date" id="departure_date" value="<?php echo e($startDate); ?>">
                              <input type="hidden" name="return_date" id="return_date" value="<?php echo e($finishDate); ?>">
                              <input type="hidden" name="singleAmount" id="singleAmount" value="<?php echo e($totalCost); ?>">
                              <input type="hidden" name="creditCardFee" id="creditCardFee" value="<?php echo e($gst); ?>">
                              <input type="hidden" name="totalAmount" id="totalAmount" value="<?php echo e($finaltotal1); ?>">
                              <input type="hidden" name="currency" id="currency" value="<?php echo e($default_currency); ?>">
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="input-field">
                                    <input name="first_name" id="first_name" type="text" class="form-control">
                                    <label for="first_name" class="">First Name</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="input-field">
                                    <input name="last_name" id="last_name" type="text" class="form-control">
                                    <label for="last_name" class="">Last Name</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="input-field">
                                    <input name="card_number" id="card_number" data-stripe="number" class="form-control" type="text">
                                    <label for="card_number" class="">Credit Card Number</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-9">
                                  <div class="row">
                                    <div class="col-sm-3">
                                      <label class="expiry-label">Expiration Date:</label>
                                    </div>
                                    <div class="col-sm-4">
                                      <div class="input-field select-box-new" style="margin-bottom: 5px;">
                                        <select class="form-control" name="month" id="month" data-stripe="exp_month">
                                          <option value="">MM</option>
                                            <?php $expiry_month = date('m');?>
                                            <?php for($i = 1; $i <= 12; $i++) {
                                            $s = sprintf('%02d', $i);?>
                                              <option value="<?php echo $s;?>" <?php //if ( $expiry_month == $i ) { ?>  <?php //} ?>><?php echo $s;?></option>
                                            <?php } ?>
                                        </select>
                                      </div>
                                      <label for="month" generated="true" class="error"></label>
                                    </div>
                                    <div class="col-sm-4">
                                      <div class="input-field select-box-new" style="margin-bottom: 5px;">
                                        <select class="form-control" name="year" id="year" data-stripe="exp_year">
                                          <option value="">YYYY</option>
                                            <?php
                                            $lastyear = date('Y')+21;
                                            $curryear = date('Y');
                                            for($k=$curryear;$k<$lastyear;$k++){ ?>
                                              <option value="<?php echo substr($k, -2);?>"><?php echo $k;?></option>
                                            <?php } ?>
                                        </select>
                                      </div>
                                      <label for="year" generated="true" class="error"></label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="input-field">
                                    <input type="text" maxlength="3" name="cvv" id="cvv" class="form-control" data-stripe="cvc">
                                    <label for="cvv" class="">CVV</label>
                                    <span class="input-icon">
                                    <a href="#" class="help-icon" data-toggle="modal" data-target="#helpModal"><i class="fa fa-question-circle"></i></a>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-8">
                                  <div class="input-field">
                                    <input id="country" type="text" name="country">
                                    <label for="country" class="">Country</label>
                                  </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="input-field">
                                    <input id="postalcode" type="text" name="postalcode">
                                    <label for="postalcode" class="">Postal Code</label>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group m-t-40">
                                <div class="row">
                                  <div class="col-sm-5">
                                    <ul class="payment-type">
                                      <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                                      <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                                      <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                                    </ul>
                                  </div>
                                  <div class="col-sm-7">
                                    <button type="submit" name="" class="btn btn-block btn-primary">Pay Now</button>
                                  </div>
                                </div>
                              </div>
                              <div class="text-center m-t-40">
                                <p>Your credit card will be charged <?php echo e($default_currency); ?> <span id="chargeAmount"> <?php echo e($finalTotal); ?></span>. By clicking "Pay Now" below, you agree to <a href="#">these terms</a></p>
                              </div>
                            </div>
                          </form>
                        </div>
                        </div>
                        </div>
                      <!-- Save Itinerary End -->
                    </div>
                  </div>
                <!-- Itinerary Book End -->

                <!-- View Itinerary Start -->
              <div class="tour-viewItinerary m-t-30">
                  <div class="tour-bottom-content">
                    <div class="tourViewItineraryDiv">
                      <h3 class="itinarery-title">Tour Itinerary</h3>
                      <?php echo $data['detail']['xml_itinerary']; ?>

                      <?php if($data['detail']['accommodation'] != '' && $data['detail']['accommodation'] != 'N/A'): ?>
                      <div class="m-t-20">
                        <h3 class="itinarery-title">Included</h3>
                          <ul class="tour-list">
                            <?php 
                              $accommodations = explode(',', $data['detail']['accommodation']);
                                foreach ($accommodations as $accommodation) {
                                  echo  '<li>'.$accommodation.'</li>';
                                }  
                            ?>
                          </ul>
                      </div>
                      <?php endif; ?>
                    </div>
                  </div>
              </div>
                <!-- View Itinerary End -->

            </div>
          </div>
          </div>
        </div>
      </div>
      </div>



  <input type="hidden" value="" id="DateRadioIdValue"> 
          
<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>
<script type="text/javascript">
    $(document).ready(function(){

        var jcarousel = $('.jcarousel'); 
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                width = carousel.innerWidth();

                if (width >= 1280) {
                    width = width / 5;
                } else if (width >= 992) {
                    width = width / 4;
                }
                else if (width >= 768) {
                    width = width / 4
                }
                else if (width >= 640) {
                    width = width / 4;
                } else {
                    width = width / 3;
                }
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });
    
        //tour-detail
        $('body').on('click', '.bookItinerary', function() {
          $(".tour-detail, .tour-viewItinerary").hide();
          $(".tour-itinerary, .bookingDates").show();

          //$(".bookingDates").hide();

          $(".saveItinerary").parent().hide();
          $(".backToDetail").parent().show();
            $(".bookingFrom").hide();
          $(this).addClass('active');
          $('.viewItinerary').removeClass('active');
            calculateHeight();
        });

        $('body').on('click', '.viewItinerary', function() {
          $(".tour-viewItinerary, .bookingDates").show();
          $(".tour-itinerary, .tour-detail").hide();
            $(".bookingFrom").hide();
          $(".saveItinerary").parent().hide();
          $(".backToDetail").parent().show();

          $(this).addClass('active');
          $('.bookItinerary').removeClass('active');
            calculateHeight();
        });

        $('body').on('click', '.backToDetail', function() {
          $(".tour-detail, .bookingDates").show();
          $(".tour-itinerary, .tour-viewItinerary").hide();

          $(".saveItinerary").parent().show();
          $(".backToDetail").parent().hide();
            $(".bookingFrom").hide();
          $('.bookItinerary, .viewItinerary').removeClass('active');
            calculateHeight();
        });

        $('body').on('click', '.dateRadio', function() {
            ChangePriceValues(); 
        });

        $('body').on('click', '.bookButton', function() {

            //alert($('#accordion1').position().top);
            //$('html, body').animate({scrollTop:$('#accordion1').position().top}, 'slow');

            $(".bookingDates").hide();
            $(".bookingFrom").show();

            var id = $(this).attr('id');
            id = id.split('_');
            $("#DateRadioIdValue").val(id[1]);

            ChangePriceValues();
        });

        $('body').on('click', '.panel-heading', function() {
          //var id = $(this).attr('id');
          //id = id.replace('headingPicks', '');

          //$("input[name='date']:checked").each(function(){
            //$(this).attr('checked', false);
            //$(this).parent().removeClass('r_on');
          //});

         // $("#radio-d"+id).attr('checked', true);
          //$("#radio-d"+id).parent().addClass('r_on');

          //ChangePriceValues();
          
        });
        /***** From Validation Start *****/
          $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z," "]+$/i.test(value);
          }, "Letters only please."); 

          $.validator.addMethod("numbersonly", function(value, element) {
            return this.optional(element) || /^[0-9," "]+$/i.test(value);
          }, "Numbers only please."); 

          $(".passenger_dob").datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
          });

          $('#tour_form_id').on('submit', function(event) {
              // adding rules for inputs with class 'comment'
              $('.passenger_first_name').each(function () {
                  $(this).rules('add', {
                      required: true,
                      lettersonly:true
                  });
              });   

              $('.passenger_last_name').each(function () {
                  $(this).rules('add', {
                      required: true,
                      lettersonly:true
                  });
              });

              $('.passenger_dob').each(function () {
                  $(this).rules('add', {
                      required: true
                  });
              });        

              // prevent default submit action         
              event.preventDefault();

              // test if form is valid 
              if($('#tour_form_id').validate().form()) {
                  console.log("validates");
              } else {
                  console.log("does not validate");
              }
          })

          $("#tour_form_id").validate({
            ignore: [],
            rules: {

                passenger_contact_no:{
                    required: true,
                    numbersonly:true,
                    maxlength: 15,
                },
                passenger_email: {
                    required: true,
                    email:true
                },
                passenger_country:{
                    required: true
                },

                first_name: {
                    required: true,
                    lettersonly:true
                },
                last_name: {
                    required: true,
                    lettersonly:true
                },
                card_number: {
                    required: true,
                    number:true
                },
                year: {
                    required: true
                },
                cvv: {
                    required: true,
                    maxlength: 3,
                    number:true
                },
                month: {
                    required: true
                },
                country: {
                  required: true
                },
                postalcode: {
                  required: true
                }
            },

            errorPlacement: function (label, element) {
              label.insertAfter(element);
            },
            submitHandler: function (form) {
              //alert('hi');
              //payment();
              form.submit();
            }
          });
        /***** From Validation End *****/

        $('#NoOfPass').on('change', function () {
            var num = $(this).val();
            AddPersons(num);
            ChangePriceValues();
        });
    });

    function ChangePriceValues(){

          var amount = $("#radio-d"+$("#DateRadioIdValue").val()+"").val();//$("input[name='date']:checked").val(); //$("#singleAmount").val();
          var NoOfPass = $('#NoOfPass option:selected').val();

          var id = $("#DateRadioIdValue").val(); //$("input[name='date']:checked").attr('id');
          //var id1 = id.replace('radio-d', 'startDate');
          //var id2 = id.replace('radio-d', 'finishDate');
          var startDate = $('#startDate'+id).val();
          var finishDate = $('#finishDate'+id).val();

          //console.log(startDate +'//'+ finishDate);
          //departure_date return_date startDate1 finishDate1

          var subtotal = amount * NoOfPass;
          var ccfee = subtotal * 0.025;
          var finaltotal = subtotal + ccfee;

          //console.log(parseFloat(amount).toFixed(2)+'//'+NoOfPass+'//'+parseFloat(subtotal).toFixed(2)+'//'+parseFloat(ccfee).toFixed(2)+'//'+parseFloat(finaltotal).toFixed(2));

          $('#total').text(parseFloat(amount).toFixed(2));
          $('#subtotal').text(parseFloat(subtotal).toFixed(2));
          $('#creditCardFee').val(parseFloat(ccfee).toFixed(2));
          $('#ccfee').text(parseFloat(ccfee).toFixed(2));
          $('#finaltotal, #chargeAmount').text(parseFloat(finaltotal).toFixed(2));
          $('#totalAmount').val(parseFloat(finaltotal).toFixed(2));
          $('#singleAmount').val(parseFloat(amount).toFixed(2));

          $('#pricePP').text("<?php echo e($default_currency); ?> " + parseFloat(amount).toFixed(2));
          $('#TotalPricePP').text("<?php echo e($default_currency); ?> " + parseFloat(subtotal).toFixed(2));

          $('#departure_date').val(startDate);
          $('#return_date').val(finishDate);
    }

    function AddPersons(num) {
      if (num == 1) {
          $('#OtherPersons').html("");
          return;
      } 

      $('#OtherPersons').html("");
      var data = ""; var key = '';
      for (var i = 1; i < num; i++) {

        key = i + 1;
          var html2 = '<div class="m-t-20">'+
                          '<h5>Person ' + key + '</h5>'+
                          '<div class="row">'+
                            '<div class="col-sm-1">'+
                              '<div class="input-field select-box-new passenger_title" name="passenger_title[' + key + ']">'+
                                '<select class="form-control">'+
                                    '<option value="Mr">Mr</option>'+
                                    '<option value="Ms">Ms</option>'+
                                    '<option value="Mrs">Mrs</option>'+
                                '</select>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-sm-11">'+
                              '<div class="row">'+
                                '<div class="col-sm-3">'+
                                  '<div class="input-field">'+
                                    '<input id="pax-fname' + i + '" type="text" name="passenger_first_name[' + key + ']" class="passenger_first_name">'+
                                    '<label for="pax-fname' + i + '" class="">First Name</label>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-sm-3">'+
                                  '<div class="input-field">'+
                                    '<input id="pax-lname' + i + '" type="text" name="passenger_last_name[' + key + ']" class="passenger_last_name">'+
                                    '<label for="pax-lname' + i + '" class="">Last Name</label>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-sm-3">'+
                                  '<div class="m-t-30 radio-blackgroup">'+
                                    '<label class="radio-checkbox label_radio m-r-10 r_on" for="radio-m' + key + '1"><input type="radio" name="passenger_gender[' + key + ']" id="radio-m' + key + '1" value="male" checked="checked">Male</label>'+
                                    '<label class="radio-checkbox label_radio" for="radio-m' + key + '2"><input type="radio" name="passenger_gender[' + key + ']" id="radio-m' + key + '2" value="female">Female</label>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-sm-3">'+
                                  '<div class="input-field">'+
                                    '<input id="pax-dob' + i + '" type="text" name="passenger_dob[' + key + ']" class="passenger_dob">'+
                                    '<label for="pax-dob' + i + '" class="" id="dob' + i + '">Date of Birth</label>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                      '</div>';
          data += html2;
      }
      $('#OtherPersons').html(data);

      $(".passenger_dob").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
      });

      $('.label_check, .label_radio').click(function(){
              setupLabel();
          });
  }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.tour', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>