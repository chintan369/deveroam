<?php $__env->startSection( 'custom-css' ); ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
	#map-loader {
		background: rgba(255, 255, 255, 0.85);
		height: 100%;
		width: 100%;
		position: absolute;
		top: 0;
		left: 0;
		z-index: 1;
	}
	#map-loader h4 {
		text-align: center;
		margin-top: 28%;
	}
	.marker-label {
		font-size: 9px;
		font-weight: bold;
		width: 15px;
		text-align: center;
		color: #000000;
	}
	.info-window {
		padding: 0;
		background: #fff;
	}
	/*.info-window img {
		width: 60%;
		height: 100px;
		margin-right: 1rem;
		box-shadow: 0 1px 6px #5B5B5B;
	}
	.iw-title {
		display: block;
		width: 100%;
		background: #2AA9DF;
		padding: 1.5rem;
	}
	.iw-title p {
		text-transform: uppercase;
		color: #ffffff;
		margin: 0;
		font-size: 11px;
		font-weight: 700;
		letter-spacing: 0.11rem;
	}
	.iw-description {
		margin: 25px 0;
		text-align: justify;
		width: 100%;
		padding: 0 1rem;
		font-size: 11px;
		max-height: 150px;
		overflow-y: auto;
		overflow-x: hidden;
	}
	.iw-controls {
		display: block;
		margin: 0;
	}
	.iw-controls a {
		display: inline-block;
		text-decoration: none;
		text-align: center;
		text-transform: uppercase;
		font-size: 11px;
		color: #ffffff;
		padding: 11px 35px;
		transition: .23s;
	}
	.iw-controls a:hover {
		opacity: 0.8;
	}
	.iw-controls i.icon:before {
		font-size: 11px;
		margin-left: 0;
	}
	.iw-controls .add-destination-btn {
		width: 50%;
		background: #2AA9DF;
	}
	.iw-controls .cancel-btn {
		width: 50%;
		background: #515151;
	}
	.iw-controls .number-of-nights {
		display: block;
		font-size: 15px;
		color: #2AA9DF;
		padding: 11px 35px;
		text-transform: uppercase;
	}
	.iw-controls .change-nights-btn[data-type="add"] {
		width: 50%;
		font-weight: 700;
		background: #E4E4E4;
		color: #052634;
	}
	.iw-controls .change-nights-btn[data-type="subtract"] {
		width: 50%;
		font-weight: 700;
		background: #b7b7b7;
		color: #5d5d5d;
	}
	.iw-controls .remove-btn,
	.iw-controls .remove-round-trip-btn {
		background: #515151;
		width: 100%;
	}
	.iw-controls .round-trip-btn {
		background: #2AA9DF;
		width: 100%;
	}*/
	.info-window-outer {
		background: #ffffff;
		width: 650px !important;
		top: 25px !important;
		left: 25px !important;
		box-shadow: 0 1px 6px #474747;
	}
	.info-window-outer > div {
		display: block !important;
	}

	.gm-style-iw {
		top: 25px !important;
	}
	.city-label-outer-parent {
		width: 100px !important;
		height: auto !important;
	}
	.city-label-outer {
		top: 18px !important;
		left: 25px !important;
		width: auto !important;
		background: transparent !important;
		box-shadow: none !important;
	}
	.city-label-outer > div > div {
		overflow: hidden !important;
	}
	.city-label {
		white-space: nowrap;
		cursor: url("https://maps.gstatic.com/mapfiles/openhand_8_8.cur") 8 8, default;
		font-size: 12px;
		font-weight: 700;
		color: #2AA9DF;
	}
	.gmnoprint[title]:after {
		color: red;
	}
	/*#edit-map-btn {
		background: #2AA9DF;
		color: #ffffff;
		position: absolute;
		top: 15px;
		/*left: 15px;*//*
		left: -380px;
		padding: 1rem 2rem;
		display: inline-block;
		font-size: 14px;
		text-decoration: none;
		border: 1px solid #2AA9DF;
		border-radius: 2px;
		transition: .23s;
	}
	#edit-map-btn:hover,
	a.switch-manual-btn:hover {
		opacity: 0.8;
	}*/
	#edit-map-container {
		background: #ffffff;
		position: absolute;
		top: 0px;
		left: 48px;
		display: none;
		/*height:953px;
		
		overflow-y: auto;
		overflow-x: hidden;*/
		width: 250px;
		padding: 10px;
		padding-bottom: 25px;
		/*z-index: 99;*/
	}
	#edit-map-container img {
		width: 100%;
	}
	#edit-map-overlay {
		display: none;
		position: absolute;
		left: 0;
		top: 0;
		background: rgba(0, 0, 0, 0.65);
		width: 100%;
		height: 100%;
	}
	a.close-edit-map-btn {
		color: #B5B5B5;
		text-align: right;
		display: block;
		text-decoration: none;
		transition: .23s;
	}
	a.close-edit-map-btn:hover {
		color: #373737;
	}
	a.close-edit-map-btn i:before {
		font-size: 15px;
	}
	.flaticon:before {
		margin-left: 0;
	}
	.route-container {
		border-radius: 2px;
		border: 2px solid #918F8F;
		padding: 5px;
		cursor: pointer;
		margin-bottom: 1rem;
		transition: .23s;
	}
	.route-container:hover {
		color: #2AA9DF;
		border-color: #2AA9DF;
	}
	/*a.switch-manual-btn {
		background: #2AA9DF;
		text-decoration: none;
		padding: 11px;
		color: #ffffff;
		transition: .23s;
	}*/
	#auto-sort {
		position: absolute;
		right: 0;
		top: 25px;
		padding: 3px 11px;
		font-weight: 700;
	}
	#auto-sort a {
		position: relative;
		top: 5px;
		/*color: #2AA9DF;*/
	}


	.content-body{
		position: absolute;
	    height: 100%;
	    top: 25px;
	    left: 0px; 
	    /*width: 68.5%;
    	padding-left: 0px;*/
    	/*margin-left: 31.5% !important;*/
	}




	.iteRight {
		/*position: relative;*/
	}

	.modal-image {
    	height: 160px !important;
	}

	#bodyContent {
		background-color: #fff;
	}


    .blue-txt {
	    /*color: #2AA9DF;*/
	    margin-top: 10px;
    	margin-bottom: 10px;
	}

	.location-wrapper{
		padding-bottom: 0px;
	}
	/*.mapWrapper {
		position: absolute;
		top: 0;
		left: 0;
		height: 100%;
		width: 100%;
	}*/

	.ui-state-default{
		list-style: none;
		padding: 10px !important;
		margin-top: 12px;
	}

	.ui-state-default:hover{
		list-style: none;
	}	
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div id="map-loader">
		<div class="loader-block">
	      <img src="<?php echo e(url( 'assets/images/launch.png' )); ?>" alt="" />
	      <h3 id="itenary_message_id"></h3>
	      <p class="loader-msg"><i class="fa fa-circle-o-notch fa-spin"></i> <span>Your itinerary will be displayed shortly…</span></p>
    	</div>
		<!-- <h4><i class="fa fa-circle-o-notch fa-spin"></i> LOADING MAP <br><small>We are currently creating your itinerary, this could take up to 20 seconds.</small></h4> -->
		<!-- <h4><i class="fa fa-clock-o fa-spin"></i> <small id="itenary_message_id"></small></h4> -->
		
	</div>

	

	<input type="hidden" id="map-data" value="<?php echo e(json_encode( Session::get( 'map_data' ) )); ?>">
	<input type="hidden" id="all-cities" value="<?php echo e(json_encode(Cache::get('cities'))); ?>">
	<div id="edit-map-overlay"></div>
	<a name="helpBoxDiv" style="position:absolute;top:0px;left:0px;"></a>
	<?php if( session()->get( 'map_data' )['type'] == 'auto' ): ?>
		<a href="#" id="edit-map-btn" style="display:none;"><i class="fa fa-sliders"></i> Edit Map<?php echo e(( $alternative_routes ) ? ' & Alternative Routes' : ''); ?></a>
		<div id="edit-map-container" class="helpBox">
			<a href="#" class="close-edit-map-btn"><i class="fa fa-times flaticon-multiply"></i></a>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="blue-txt"><i class="fa fa-map"></i> Current Route</h4>
					<div id="current-route-container">
						<?php foreach( $current_route as $city ): ?>
							<span><?php echo e(is_object($city) ? $city->name : $city['name']); ?></span>
							<?php if( last( $current_route ) != $city ): ?>
								<i class="fa fa-long-arrow-right"></i>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
					<?php if( $alternative_routes ): ?>
						<h4 class="blue-txt margin-top"><i class="flaticon flaticon-map"></i> Alternative <?php echo e(sing( count( $alternative_routes ), 'Routes', false )); ?></h4>
						<?php foreach( $alternative_routes as $key => $value ): ?>
							<div class="route-container" data-key="<?php echo e($key); ?>">
								<p><?php echo e($value['duration']); ?> <?php echo e($value['price_per_person']); ?> (Per Person)</p>
								<?php foreach( $value['cities'] as $city ): ?>
									<span><?php echo e($city['name']); ?></span>
									<?php if( last( $value['cities'] ) != $city ): ?>
										<i class="fa fa-long-arrow-right"></i>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
					<h4 class="blue-txt margin-top"><i class="fa fa-gear"></i> Custom Route</h4>
					<p>If you want to set your own route, you can do this by switching to manual mode.</p>
					<p><small>Note: Once you have switched to manual mode, you can't switch back to auto mode.</small></p>
					<a href="javascript://" class="switch-manual-btn btn btn-primary"><i class="fa fa-exchange"></i> Switch to Manual Mode</a>
				</div>
			</div>
		</div>
	<?php else: ?>
		<a href="#" id="edit-map-btn" style="display:none;"><i class="fa fa-question-circle"></i> Help</a>
		<span id="auto-sort" class="text-center"><i style="cursor: pointer;" class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="If enabled, selected locations will be sorted based on the shortest route when adding a new location."></i> Auto Sort <a href="#"><i class="<?php echo e((!empty(session()->get( 'map_data' )['auto_sort']) && session()->get( 'map_data' )['auto_sort'] == 'on') ? 'fa fa-toggle-on fa-2x' : 'fa fa-toggle-off fa-2x'); ?>"></i></a></span>
		<div id="edit-map-container" class="helpBox" >
			<a href="#" class="close-edit-map-btn"><i class="fa fa-times flaticon-multiply"></i></a>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="blue-txt"><i class="fa fa-map-marker"></i> Adding destinations</h4>
					<p>To add a destination to your itinerary, simple click on the blue dots found on the map.</p>
					<img src="<?php echo e(url( 'assets/img/map-dot.png' )); ?>">
					<h4 class="blue-txt margin-top"><i class="fa fa-hotel"></i> Removing a destination / Adding nights</h4>
					<p>To remove a destination, click on the blue marker of the destination you want to remove and click "REMOVE DESTINATION" button. Click on the "+" and "-" buttons to adjust the number of nights you want for that destination.</p>
					<img src="<?php echo e(url( 'assets/img/map-nights.png' )); ?>">
					<h4 class="blue-txt"><i class="flaticon flaticon-map"></i> Round Trips</h4>
					<p>If you want to include a round-trip to your itinerary, simple click on the blue marker of the starting location and click the "Round-trip" button.</p>
					<img src="<?php echo e(url( 'assets/img/map-roundtrip.png' )); ?>">
				</div>
			</div>
		</div>
	<?php endif; ?>
	<input type="hidden" value="<?php echo e(json_encode( session()->get( 'search_input' ) )); ?>" id="search-input">

<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>
<script type="text/javascript" src="<?php echo e(url('assets/js/eroam-map.js')); ?>"></script>

<script>

$( window ).load( eMap.init );

$(document).ready( function() {

	
	var searchInput = JSON.parse( $( '#search-input' ).val() ),
		countryId = searchInput.country,
		toCountryId = searchInput.to_country,
		tomorrow = moment().add( 1, 'days' ).format( 'YYYY/MM/DD' ),
		nextWeek = moment().add( 1, 'weeks' ).format( 'YYYY/MM/DD' );

	$('#date-of-travel').datetimepicker( {
		timepicker: false,
		format: 'Y-m-d',
		minDate: tomorrow,
		startDate: nextWeek
	} );

	eroam.ajax( 'post', 'get-cities-by-country-id', { country_id: countryId }, function( response ) {
		var city = $( '#starting-point' );
		city.html( '<option value="">Starting Point</option>' );
		$.each( response, function( k, v ) {
			var selected = v.id == searchInput.city ? 'selected' : '';
			city.append( '<option value="' + v.id + '" '+ selected +'>' + v.name + '</option>' );
		} );
	} );
	$( '#country option[value="' + countryId + '"]' ).attr( 'selected', true );

	// auto
	if ( searchInput.option == 'auto' ) {
		eroam.ajax( 'post', 'get-cities-by-country-id', { country_id: toCountryId }, function( response ) {
			var city = $( '#to-city' );
			city.html( '<option value="">Destination</option>' );
			$.each( response, function( k, v ) {
				var selected = v.id == searchInput.to_city ? 'selected' : '';
				city.append( '<option value="' + v.id + '" '+ selected +'>' + v.name + '</option>' );
			} );
		} );
		$( '#to-country option[value="' + countryId + '"]' ).attr( 'selected', true );
	}
	// Map Search Header
	$( '.search-option-btn' ).click( function( event ) {
		event.preventDefault();
		$( this ).next( '.search-option' ).prop( 'checked', true );
		$( '.search-option-btn' ).removeClass( 'active' );
		$( this ).addClass( 'active' );
		if ( $( this ).data( 'type' ) == 'manual' ) {
			$( '#auto-container' ).addClass( 'hide' );
		} else {
			$( '#auto-container' ).removeClass( 'hide' );
		}
	} );

	$( '.select-country' ).change( function() {
		var city = $( this ).parent().next().find( '.select-city' );
		if ( $( this ).val().trim() != '' ) {
			eroam.ajax( 'post', 'get-cities-by-country-id', { country_id: $( this ).val() }, function( response ) {
				city.html( '<option value="">Starting Point</option>' );
				$.each( response, function( k, v ) {
					city.append( '<option value="' + v.id + '">' + v.name + '</option>' );
				} );
			} );
		}
	} );

	$( '.update-search-btn' ).click( function( event ) {
		event.preventDefault();
		$( '#submit-btn' ).click();
	} );

	$( '#edit-map-btn' ).click( function( event ) {
		event.preventDefault();
		$( '#edit-map-container' ).show( 'slide', {direction: 'left'}, 300 );
		$( '#edit-map-overlay' ).fadeIn( 300 );
	} );

	$( '#edit-map-overlay, .close-edit-map-btn' ).click( function( event ) {
		event.preventDefault();
		$( '#edit-map-container' ).hide();/* 'slide', {direction: 'left'}, 200 );*/
		$( '#edit-map-overlay' ).fadeOut( 200 );
	} );


	$( '.route-container' ).click( function() {
		var key = $( this ).data( 'key' );
		eMap.switchRoute( key );
	} );

	$( '.switch-manual-btn' ).click( function( event ) {
		event.preventDefault();
		eMap.switchToManual();
	} );

	$('#auto-sort a').click(function(e) {
		e.preventDefault();
		$(this).find('i').toggleClass('fa-toggle-off fa-toggle-on');
		var autoSort = $(this).find('i').attr('class') == 'fa fa-2x fa-toggle-off' ? 'off' : 'on';

		eroam.ajax('post', 'map/update', { auto_sort: autoSort }, function() {
			window.location.reload();
		});
	});

	// TOOLTIP
	$('[data-toggle="tooltip"]').tooltip();

	/**
	 * ==================================================================================================
	 * REORDER LOCATIONS SCRIPT
	 * CREATED BY RGR
	 * ==================================================================================================
	 */
	/*$('body').on('click', '#reorder-locations-btn', function(e) {
		e.preventDefault();
		$('.itinerary-container, #reorder-locations-btn').hide();
		$('#reorder-locations-container').show();
		$('#reorder-locations').sortable({
			items: 'li:not(li:first-child)',
			placeholder: 'ui-state-highlight',
			opacity: 0.7,
			scrollSensitivity: 50,
			forcePlaceholderSize: true,
				over: function(e, ui){ 
					ui.placeholder.height(ui.item.height()-30);
				}
		});
	});*/

	$('body').on('click', '#cancel-reorder-btn', function(e) {
		e.preventDefault();
		$('#reorder-locations-container').hide();
		$('.itinerary-container, #reorder-locations-btn').show();
	});


	// START FUNCTION TO POSSIBLY REPLACE ALL UNCESSARY OWN ARRANGEMENT WITH API DATA
	// CREATED BY MIGUEL ON 2017-03-09
	var initialLoad = false; // THIS SHOULD BE A VARIABLE IN THE SEARCH_SESSION; SET AS FALSE AFTER INITIAL LOAD
	var searchSession = JSON.parse($('#search-session').val());
	if( initialLoad )
	{
		// searchSession.initialLoad = false;
		var travelDate    = searchSession.travel_date;
		var travellers    = searchSession.travellers;
		var itinerary     = searchSession.itinerary;

		var ownArragementHotels     = []; // VARIABLE TO STORE THE FOUND OWN ARRANGEMENT HOTELS
		var ownArragementActivities = []; // VARIABLE TO STORE THE FOUND OWN ARRANGEMENT ACTIVITIES
		var ownArragementTransports = []; // VARIABLE TO STORE THE FOUND OWN ARRANGEMENT TRANSPORTS
		var transportDuration  = '';
	
		itinerary.forEach(function( leg, legKey ){ // START LOOP THROUGH ITNERARY

			// JAYSON SHOULD WORK HERE
			// CHECK IF HOTEL IS OWN ARRANGEMENT
			if( leg.hotel == null )
			{
				// eroam.apiDeferred();
				// eroam.apiPromiseHandler();
			}
			// END HOTEL CHECK

			// CHECK IF ACTIVITY IS OWN ARRANGEMENT
			if( leg.activities == null )
			{
				var destinationReq = {
					destinationName: leg.city.name,
					country: leg.city.country.name
				};
				
				var destinationApiCall = eroam.apiDeferred('service/location', 'POST', destinationReq, 'destination', true);
				eroam.apiPromiseHandler( destinationApiCall, function( destinationResponse ){
					
					if( destinationResponse != null ){

						if( Object.keys(destinationResponse).length > 0){
							vRQ = {
								destId: destinationResponse.destinationId,
								startDate: formatDate(leg.city.date_from),
								endDate: formatDate(leg.city.date_to),
								currencyCode: 'AUD',
								sortOrder: 'TOP_SELLERS',
								topX: '1-15',
								provider:'viator'
							};
			
							var viatorApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', vRQ, 'viator_activity', true);
							eroam.apiPromiseHandler( viatorApiCall, function( viatorResponse ){
								
								if( viatorResponse != null )
								{
									if( viatorResponse.length > 0 )
									{
										var viatorAct = viatorResponse[0];
										
										data ={
											'date_from':formatDate(leg.city.date_from),
											'date_to':formatDate(leg.city.date_to),
											'id':viatorAct.code,
											'city_id':leg.city.id,
											'name':	viatorAct.title,
											'city':{
												'id':leg.city.id
											},
											'activity_price':[{
												'price':viatorAct.price,
												'currency':{'id':1,'code':'AUD'}
											}], 
											'price':[{
												'price':viatorAct.price,
												'currency':{'id':1,'code':'AUD'}
											}], 
											'currency_id':1,
											'currency': 'AUD',
											'provider': 'viator',
											'description': viatorAct.shortDescription
										};
					
										searchSession.itinerary[legKey].activities = [data];

										bookingSummary.update( JSON.stringify( searchSession ) );
					
									}
								}
							});
						}
					}
				});	
			}
			// END ACTIVITY CHECK

			// CHECK IF TRANSPORT IS OWN ARRANGEMENT IF LEG IS NOT THE LAST LEG
			if( legKey != (itinerary.length - 1) )
			{
				if( leg.transport == null || !leg.transport )
				{
					var mystiflyRQ;
					var mystiflyApiCalls            = [];
					var originIatas                 = leg.city.airport_codes;
					var destinationIatas            = itinerary[ legKey + 1 ].city.airport_codes;
					var arrayOfOriginIataCodes      = splitString( originIatas, "," );
					var arrayOfDestinationIataCodes = splitString( destinationIatas, "," );

					if( arrayOfOriginIataCodes.length > 0 && arrayOfDestinationIataCodes.length > 0 )
					{
						arrayOfOriginIataCodes.forEach(function( originIataCode ){
							arrayOfDestinationIataCodes.forEach(function( destinationIataCode ){
								mystiflyRQ = {
									DepartureDate : leg.city.date_to,
									OriginLocationCode : originIataCode,
									DestinationLocationCode : destinationIataCode,
									CabinPreference : 'Y',
									Code : ['ADT'],
									Quantity : [travellers],
									IsRefundable : false,
									IsResidentFare : false,
									NearByAirports : false,
									provider: 'mystifly'
								};

								try{
									console.log('mystiflyRQ Leg '+legKey, mystiflyRQ);
									mystiflyApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', mystiflyRQ, 'mystifly', true ) );
								}catch(e){
									console.log('An error has occured while sending the request to the mystifly API for leg'.legKey, e.message);
								}
							});
						});

						try{

							eroam.apiArrayOfPromisesHandler( mystiflyApiCalls, function( mystiflyArrayOfResponses ){
								mystiflyArrayOfResponses.forEach( function( mystiflyResponse ){
									console.log('mystiflyRS Leg '+legKey, mystiflyResponse);
									if( mystiflyResponse != null )
									{
										try{

											var transport;
											if( Array.isArray( mystiflyResponse.PricedItineraries.PricedItinerary ) ){
												if( mystiflyResponse.PricedItineraries.PricedItinerary.length > 0 ){
													transport = mystiflyResponse.PricedItineraries.PricedItinerary[0];
												}
											}else{
												transport = mystiflyResponse.PricedItineraries.PricedItinerary;
											}

											var fromCity           = leg.city.name;
											var toCity             = itinerary[ legKey + 1 ].city.name;
											var t                  = transport.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment;
											var fareSourceCode     = transport.AirItineraryPricingInfo.FareSourceCode;
											var airlineName        = ( isNotUndefined( t.OperatingAirline.Name ) ) ? t.OperatingAirline.Name : 'N/A';
											var airlineCode        = ( isNotUndefined( t.OperatingAirline.Code ) ) ? t.OperatingAirline.Code : 'N/A';
											var etd                = t.DepartureDateTime;
											var eta                = t.ArrivalDateTime;
											var transportClass     = (t.CabinClassText).trim() != '' ? t.CabinClassText : 'Cabin Class Not Specified';
											var departure          = moment( etd, moment.ISO_8601 );
											var arrival            = moment( eta, moment.ISO_8601 );
											var difference         = moment.duration( arrival.diff( departure ) );
											var duration           = difference.asHours();
											var currency           = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
											var price              = eroam.convertCurrency( transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount, currency );
											
											var transportName      = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+' / Depart - '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure.format('hh:mm A')+'. Arrive - '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+arrival.format('hh:mm A');
											
											var bookingSummaryText = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+'<br/><small>Depart: '+t.DepartureData+' '+departure.format('hh:mm A')+'</small><br/><small>Arrive: '+t.ArrivalData+' '+arrival.format('hh:mm A')+'</small>';

											var data = {
												airline_code: airlineCode,
												arrival_data: t.ArrivalData,
												arrival_location: t.ArrivalAirportLocationCode,
												booking_summary_text: bookingSummaryText,
												currency: globalCurrency,
												departure_data: t.DepartureData,
												departure_location: t.DepartureAirportLocationCode,
												duration: duration,
												eta: eta,
												etd: etd,
												fare_source_code: fareSourceCode,
												fare_type: transport.AirItineraryPricingInfo.FareType,
												flight_number: t.FlightNumber,
												from_city_id: leg.city.id,
												id: fareSourceCode,
												is_selected: true,
												operating_airline: airlineName,
												passenger_type_code: "ADT",
												passenger_type_quantity: travellers,
												price: { 
													0:{
														price:price,
														currency:{
															code:currency
														}
													}
												},
												provider: "mystifly",
												to_city_id: itinerary[ legKey + 1 ].city.id,
												transport_id: fareSourceCode,
												transport_type: {
													id: 1,
													name: "Flight"
												},
												transport_type_id: 1,
												transport_type_name: "Flight",
												transporttype: {
													id: 1,
													name: "Flight"
												}
											};

											searchSession.itinerary[ legKey ].transport = data;
											bookingSummary.update( JSON.stringify(searchSession) );
											//bookingSummary.hideMiniLoader( transportElement ); // hide mini loader
											console.log('mystifly successful call Leg '+legKey, data);
										}
										catch(e)
										{
											console.log('An error has occured while formatting the response of mystifly API call for leg'.legKey, e.message);
										}
									}
									else
									{
										//bookingSummary.hideMiniLoader( transportElement ); // hide mini loader
									}
								});

							});
						
						}
						catch(e)
						{
							console.log('An error has occured while processing the response of mystifly API call for leg'.legKey, e.message);
						}

					}
					else
					{
						//bookingSummary.hideMiniLoader( transportElement ); // hide mini loader
					}
				}
			}
			// END TRANSPORT CHECK
			




		}); // END LOOP THROUGH ITNERARY
		


	}

}); // end document ready


/*
| Update by junfel
| Added separator parameter
*/
function formatDate(date, separator='-') {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join( separator );
}

function calculateHeight(){
    var winHeight = $(window).height();

    var oheight = $('.page-sidebar').outerHeight();// -22
    var oheight1 = $('.page-sidebar').outerHeight();
    //alert(oheight);
    var elem = $('.page-content .tabs-container').outerHeight();
    var elemHeight = oheight - elem;
    $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);

    var elemHeight1 = oheight1 - elem;
    var winelemHeight = winHeight - elem;

    if(winHeight < oheight){
        $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);
        $(".page-content .tabs-content-wrapper1").outerHeight(elemHeight);
        $(".page-content .tabs-content-wrapper2").outerHeight(oheight1);
    } else{
        $(".page-content .tabs-content-wrapper").outerHeight(winelemHeight);
        $(".page-content .tabs-content-wrapper1").outerHeight(winelemHeight);
        $(".page-content .tabs-content-wrapper2").outerHeight(winelemHeight);
    }
    // $(".page-content .tabs-content-wrapper1").outerHeight(elemHeight1);
    // $(".page-content .tabs-content-wrapper2").outerHeight(oheight1);

	/*---- hotel content ------*/
    //var hotelContentHeight = oheight - elem;
    //alert(hotelContentHeight);
    $(".page-content .hotel-container").outerHeight(elemHeight);
    var hotelTop = $('.page-content .hotel-top-content').outerHeight();
    var hotelTabs = $('.page-content .custom-tabs').outerHeight() + 120;
    //alert(hotelTop);
    //alert(hotelTabs);
    var hotelHeight = (elemHeight - hotelTop) - hotelTabs;
    var winHotelHeight = (winelemHeight - hotelTop) - hotelTabs;
    //alert(hotelHeight);
    if(winHeight < oheight1){
        $(".page-content .hotel-container").outerHeight(elemHeight1);
        $(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
    } else{
        $(".page-content .hotel-container").outerHeight(winelemHeight);
        $(".page-content .hotel-bottom-content").outerHeight(winHotelHeight);
    }
    //$(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
}

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'layouts.search' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>