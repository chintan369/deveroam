<div class="m-t-20 profile-steps">
    <ul>
        <!-- <li class="profile-fill"> -->
        <li <?php if($user->customer->step_1 == 1 ) { ?> class="profile-fill" <?php }?>>
            <a href="<?php echo e(url('profile/step1')); ?>">
                <span class="circle"></span>
                <p>Complete Profile<br/>Details</p>
            </a>
        </li>
        <li <?php if($user->customer->step_2 == 1 ) { ?> class="profile-fill" <?php }?>>
            <a href="<?php echo e(url('profile/step2')); ?>">
                <span class="circle"></span>
                <p>Add Address<br/>Details</p>
            </a>
        </li>
        <li <?php if($user->customer->step_3 == 1 ) { ?> class="profile-fill" <?php }?>>
            <a href="<?php echo e(url('profile/step3')); ?>">
                <span class="circle"></span>
                <p>Update Personal<br/>Preferences</p>
            </a>
        </li>
        <li <?php if($user->customer->step_4 == 1 ) { ?> class="profile-fill" <?php }?>>
            <a href="<?php echo e(url('profile/step4')); ?>">
                <span class="circle"></span>
                <p>Update Contact<br/>Preferences</p>
            </a>
        </li>
        <li>
            <a href="#">
                <span class="circle"></span>
                <p>Book Your<br/>First Trip!</p>
            </a>
        </li>
    </ul>
</div>
<div class="row m-t-30">
    <div class="col-sm-9">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e($percent); ?>%;">
                <!-- 40% -->
            </div>
        </div>
    </div>
    <div class="col-sm-3"><strong>Profile <?php echo e($percent); ?>% Complete</strong></div>
</div>
<div class="m-t-30 row">
    <div class="col-lg-4 col-lg-offset-8 col-sm-6 col-sm-offset-6">
        <div class="row">
            <div class="col-xs-5">
                <button type="submit" name="" class="btn btn-black btn-block">UPDATE</button>
            </div>
            <div class="col-xs-7">Update your personal travel preferences.</div>
        </div>
    </div>
</div>