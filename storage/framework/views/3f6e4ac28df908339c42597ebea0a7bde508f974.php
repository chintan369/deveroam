<?php 
  
  $selectedPrice = 0;
  $hotelID = 0;
  $rateCode = 0;
  $search_session = json_decode(json_encode( session()->get( 'search' ) ) , FALSE );
  $travellers = $search_session->travellers;
  $searchRooms = $search_session->rooms;
  if($data->provider == 'expedia'){
    $total = '@total';
    if($search_session->itinerary[$data->leg]->hotel){
      $selectedPrice = ceil($search_session->itinerary[$data->leg]->hotel->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->ChargeableRateInfo->$total);
      $hotelID = $search_session->itinerary[$data->leg]->hotel->hotelId;
      $rateCode = ceil($search_session->itinerary[$data->leg]->hotel->RoomRateDetailsList->RoomRateDetails->rateCode);
    }
  }
  
  $surcharge_components = array(
      'TaxAndServiceFee'=>'Tax & Service Fee',
      'ExtraPersonFee'=>'Extra Person Fee',
      'Tax'=>'Tax',
      'ServiceFee'=>'Service Fee',
      'SalesTax'=>'Sales Tax',
      'HotelOccupancyTax'=>'Hotel Occupancy Tax',
      'PropertyFee'=>'Property Fee'
    );
  $i = 0;
  //echo '<pre>';print_r($search_session->itinerary[$data->leg]->hotel);exit;
  //echo '<pre>';print_r($data->HotelRoomResponse);exit;
  if(!isset($data->HotelRoomResponse->rateCode)){
    $temp_array = $data->HotelRoomResponse;
     foreach ($data->HotelRoomResponse as $key => $value) {
      if($value->rateCode == $rateCode){
        if($key != 0){
          $temp_array[$i] = $data->HotelRoomResponse[$key];
          $temp_array[$key] = $data->HotelRoomResponse[$i];
        }
      }
    }
    $data->HotelRoomResponse = $temp_array;
  }else{
    $temp_array[0] = $data->HotelRoomResponse;
    $data->HotelRoomResponse = $temp_array;
  }
?>
<?php 
  $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
  $nights = 0;
  
 ?>
<?php if(session()->get('search')): ?>
  <?php 
    $nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
   ?>
<?php endif; ?>



 
<?php $__env->startSection('content'); ?>
   
      <?php 
        
        $travellers   = session()->get( 'search' )['travellers'];   
        $total_childs = session()->get( 'search' )['child_total']; 
        $rooms        = session()->get( 'search' )['rooms']; 
        $room_text = "Room";
        if($rooms > 1)
        {
          $room_text = "Rooms";
        }
      ?>
          <div class="tabs-container">
            <div class="tabs-container-inner">
              <div class="row">
                <div class="col-sm-4">
                  <a href="<?php echo e(url( $data->searchCity.'/hotels?leg='.$data->leg )); ?>"><i class="fa fa-angle-left back-arrow"></i> <strong>Back to all Accommodation</strong></a>
                </div>
                <div class="col-sm-8">
                  <div class="panel-icon loc-icon">
                    <i class="icon icon-location"></i>
                  </div>
                  <div class="panel-container"> 
                    <?php echo e($data->searchCity); ?>, <?php echo e($data->country_name); ?>: <?php echo e(date('l d M Y',strtotime($data->arrivalDate))); ?> - <?php echo e(date('l d M Y',strtotime($data->departureDate))); ?> (<?php echo e($rooms); ?> <?php echo e($room_text); ?>)
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tabs-content-container">
            <div class="accomodation-top-content">
              <div class="row">
                <div class="col-sm-7">
                  <h4 class="hotel-title"><a href="#"><?php echo e($data->hotelName); ?></a></h4>
                  <p>
                    <i class="icon icon-location accomomdation-icon"></i>
                    Location: <?php echo e($data->country_name); ?>, <?php echo e($data->searchCity); ?> / <?php echo e($data->hotelCity); ?>

                  </p>
                </div>
                <div class="col-sm-5">
                  <div class="row">
                    <div class="col-xs-9 price-right border-right">
                      <p>From <strong>$AUD <?php echo e($data->hotelPrice); ?></strong><br/>Per Person<br/>Per Night<br/>
                      </p>
                    </div>
                    <div class="col-xs-3 text-center">
                      <h3 class="transport-price"><strong><?php echo e($nights); ?></strong></h3>
                      <span class="transport-nights"><?php echo e(($nights > 1 ? 'Nights':'Night')); ?></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="jcarousel-wrapper">
                <div class="jcarousel">  
                  <ul class="accomodationImg-list">
                    <?php if($data->HotelImages->HotelImage): ?>
                      <?php foreach($data->HotelImages->HotelImage as $key => $value): ?>
                        <?php if($key <= 30): ?>
                          <li><img src="<?php echo e($value->url); ?>" alt=""></li>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </ul>
                </div>
                <p class="jcarousel-pagination"></p>
              </div>
              <div class="m-t-50 row">
                <div class="col-sm-4">
                  <a href="javascript://" name="" class="btn btn-primary active accomodation-tab btn-block" data-id="roomRates">ROOMS &amp; RATES</a>
                </div>
                <div class="col-sm-4">
                  <a href="javascript://" class="btn btn-primary btn-block accomodation-tab" data-id="overview">OVERVIEW</a>
                </div>
                <div class="col-sm-4">
                  <a href="javascript://" name="" class="btn btn-primary btn-block accomodation-tab" data-id="importantInfo">IMPORTANT INFO</a>
                </div>
              </div>
            </div>
            <div class="roomRates-filter">
              <div class="m-t-20 row">
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-sm-3 date-control">
                      <div class="panel-form-group form-group">
                        <label class="label-control">Check-in Date:</label>
                        <div class="input-group datepicker">
                          <input id="checkin-date" type="text" placeholder="16 May 2017" class="form-control disabled" value="<?php echo e(date('d M Y',strtotime($data->arrivalDate))); ?>" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                          <span class="input-group-addon"><i class="icon-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3 date-control">
                      <div class="panel-form-group form-group">
                        <label class="label-control">Check-out Date:</label>
                        <div class="input-group datepicker">
                          <input id="checkout-date" type="text" placeholder="16 May 2018" class="form-control disabled" value="<?php echo e(date('d M Y',strtotime($data->departureDate))); ?>" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                          <span class="input-group-addon"><i class="icon-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="panel-form-group form-group">
                        <label class="label-control">Rooms</label>
                        <input type="text" class="form-control disabled" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" value="<?php echo e($rooms); ?>">
                        <!-- <select class="form-control disabled" data-toggle="tooltip" title="Not Available in Pilot">
                          <option>1</option>
                          <option>2</option>
                        </select> -->
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="panel-form-group form-group">
                        <label class="label-control">Adults (18+)</label>
                        <input type="text" value="<?php echo e($travellers); ?>" class="form-control disabled" readonly data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">
                        <!-- <select class="form-control disabled" data-toggle="tooltip" title="Not Available in Pilot">
                          <option>1</option>
                          <option>2</option>
                        </select> -->
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="panel-form-group form-group">
                        <label class="label-control">Children (0-17)</label>
                       <input type="text" class="form-control disabled" readonly data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" value="<?php echo e($total_childs); ?>">
                        <!-- <select class="form-control disabled" data-toggle="tooltip" title="Not Available in Pilot">
                          <option>0</option>
                          <option>1</option>
                          <option>2</option>
                        </select> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <button type="submit" name="" class="btn btn-secondary active btn-block">UPDATE</button>
                </div>
              </div>
            </div>
            <div class="accomodation-bottom-content" id="roomRates">
              <div class="accomodation-bottom-inner">
                <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                  <?php if($data->provider=="expedia"): ?>
                    <?php if($data->HotelRoomResponse): ?>
                      <?php foreach($data->HotelRoomResponse as $key => $RoomResponse): ?>

                       <?php 
                          $singleRate = '@nightlyRateTotal';
                          $singleRate = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$singleRate;
                          $singleRate = round(($singleRate * $eroamPercentage) / 100 + $singleRate,2); 
                          $taxes = $RoomResponse->RateInfos->RateInfo->taxRate;
                          $total = $singleRate + $taxes;
                          $displaySymbol = '';
                          $disabled_status = '';

                          if($rateCode == $RoomResponse->rateCode){
                            $displaySymbol = '+';
                            $differencePrice =0;
                            $disabled_status = 'disabled';
                          }else{
                            $differencePrice = abs($total - $selectedPrice);
                            if($total < $selectedPrice){
                              $displaySymbol ='-';
                            }else{
                              $displaySymbol = '+';
                            }
                          }
                          
                          $currencyCode = '@currencyCode';
                          $currencyCode = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$currencyCode;
                         ?>

                        <div class="panel panel-default">
                          <div class="panel-heading" id="headingPicks<?php echo e($key); ?>" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapsePicks<?php echo e($key); ?>" aria-expanded="true" aria-controls="collapsePicks<?php echo e($key); ?>">
                            <div class="panel-title row">
                              <div class="col-sm-2 col-xs-12 transport-img padding-right-0">
                                <div class="transport-img-center">
                                  <img src="<?php echo e(url( 'assets/images/default-transport.jpg' )); ?>" alt="" class="img-responsive" />
                                </div>
                              </div>
                              <div class="col-sm-6 col-xs-6">
                                  <div class="transport-inner">
                                    <div class="transport-border">
                                    <strong><?php echo e($RoomResponse->rateDescription); ?></strong>
                                    <br/>Room sleeps <?php echo e($RoomResponse->rateOccupancyPerRoom); ?> Guests. <?php echo e(($RoomResponse->RateInfos->RateInfo->nonRefundable == 0 ? 'Refundable':'Non-Refundable')); ?>

                                    </div>
                                  </div>
                              </div>
                              <div class="col-sm-4 col-xs-6 accomodationPrice">
                                <div class="transport-inner">
                                  <h3><strong>$AUD <?php echo e(number_format($total / $travellers,2)); ?></strong></h3>
                                  Per Person For <?php echo e($nights); ?> <?php echo e(($nights > 1 ? 'Nights':'Night')); ?>

                                </div>
                              </div>
                            </div>
                          </div>

                          <div id="collapsePicks<?php echo e($key); ?>" class="panel-collapse collapse <?php echo e(($key == 0 ? 'in':'')); ?>" role="tabpanel" aria-labelledby="headingPicks1">
                            <div class="panel-body">
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="m-t-20">
                                    <?php 
                                      $nights = 0;
                                     ?>
                                    <?php if(session()->get('search')): ?>
                                      <?php 
                                        $nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
                                       ?>
                                    <?php endif; ?>
                                    <p><strong>Check-in: </strong> <?php echo e(date('d M Y',strtotime($data->arrivalDate))); ?><br/>
                                    <strong>Check-out:</strong> <?php echo e(date('d M Y',strtotime($data->departureDate))); ?><br/>
                                    <strong>Duration:</strong> <?php echo e($nights); ?> <?php echo e(($nights > 1 ? 'Night(s)':'Night')); ?>

                                    </p>
                                  </div>
                                  <div class="m-t-20">
                                    <p><?php echo html_entity_decode($RoomResponse->RoomType->descriptionLong); ?></p>

                                  </div>
                                  <div class="m-t-20">
                                    <p>Room sleeps <?php echo e($RoomResponse->rateOccupancyPerRoom); ?> Guests.<br/><?php echo e(($RoomResponse->RateInfos->RateInfo->nonRefundable == 0 ? 'Refundable':'Non-Refundable')); ?></p>
                                  </div>
                                  <div class="m-t-30"><a href="#" data-toggle="modal" data-target="#roomTypeModal<?php echo e($key); ?>" class="more-info moreInfo"><strong><i class="fa fa-info-circle"></i> More Details</strong></a>
                                  </div>
                                                              <!-- Modal -->
                                  <div class="modal fade" class="roomTypeModal" id="roomTypeModal<?php echo e($key); ?>" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-md" role="document">
                                      <div class="modal-content">
                                        <a href="#" class="modal-close" data-dismiss="modal">x</a>
                                        <!-- <div class="roomType-img">
                                          <img src="<?php echo e(url( 'assets/images/modal-default.jpg' )); ?>" class="img-responsive" />
                                        </div> -->
                                           <div class="connected-carousels">
                                              <div class="stage">
                                                  <div class="carousel carousel-stage">
                                                      <ul>
                                                          <?php if($data->HotelImages->HotelImage): ?>
                                                            <?php foreach($data->HotelImages->HotelImage as $key => $value): ?>
                                                              <?php if($key <= 30): ?>
                                                                <li><img src="<?php echo e($value->url); ?>" alt="" width="600" height="400"></li>
                                                              <?php endif; ?>
                                                            <?php endforeach; ?>
                                                          <?php endif; ?>
                                                      </ul>
                                                  </div>
                                                  <!-- <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
                                                  <a href="#" class="next next-stage"><span>&rsaquo;</span></a> -->
                                              </div>
                                              <div class="navigation">
                                                  <a href="#" class="prev prev-navigation">&lsaquo;</a>
                                                  <a href="#" class="next next-navigation">&rsaquo;</a>
                                                  <div class="carousel carousel-navigation">
                                                      <ul>
                                                          <?php if($data->HotelImages->HotelImage): ?>
                                                            <?php foreach($data->HotelImages->HotelImage as $key => $value): ?>
                                                              <?php if($key <= 30): ?>
                                                                <li><img src="<?php echo e($value->url); ?>" alt="" width="80" height="80"></li>
                                                              <?php endif; ?>
                                                            <?php endforeach; ?>
                                                          <?php endif; ?>
                                                      </ul>
                                                  </div>
                                              </div>
                                            </div>
                                        <div class="modal-body">
                                          <div class="roomType-inner m-t-20">
                                            <div class="m-t-20">
                                              <p><?php echo html_entity_decode($RoomResponse->RoomType->descriptionLong); ?></p>
                                            </div>
                                            
                                            <div class="m-t-30 text-right">
                                              <a href="#" data-dismiss="modal" class="modalLink-blue">CLOSE</a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <!---No Modal -->

                                </div>
                                <div class="col-sm-5 col-sm-offset-1">
                                  <div class="row">
                                    <div class="col-sm-6">
                                      <strong>Guests: </strong>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                      <strong><?php echo e($travellers); ?> x Adult</strong>
                                    </div>
                                  </div>
                                  <table class="table m-t-20">
                                    <tbody>
                                      <?php 
                                      $NightlyRatesPerRoom = json_decode(json_encode($RoomResponse),true);
                                     
                                        if(isset($NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'])){
                                            if(isset($NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'][0])){
                                              $temp_rate = $NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'];
                                            }else{
                                              $temp_rate[0] = $NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'];
                                            }
                                            foreach($temp_rate as $chargeKey => $charges){
                                              $rate = '@rate';
                                              $chargesRate = 0;
                                              if(isset($charges['ChargeableNightlyRates'][0])){
                                                 foreach($charges['ChargeableNightlyRates'] as $c){
                                                    $chargesRate = $chargesRate + $c[$rate];
                                                  }
                                              }else{
                                                 $chargesRate = $charges['ChargeableNightlyRates'][$rate];
                                              }
                                             
                                              $chargesRate = round(($chargesRate * $eroamPercentage) / 100 + $chargesRate,2); 
                                               ?>
                                              <tr>
                                                <td><strong>Room - <?php echo e($chargeKey+1); ?>:</strong></td>
                                                <td class="text-right">$AUD</td>
                                                <td class="text-right"><?php echo e($chargesRate); ?></td>
                                              </tr>
                                              <?php  
                                            }
                                        }
                                       ?>
                                      <tr>
                                        <td><strong>Total:</strong></td>
                                        <td class="text-right">$AUD</td>
                                       
                                        <td class="text-right"><?php echo e($singleRate); ?></td>
                                      </tr>
                                      <?php 
                                      $sur_charges = json_decode(json_encode($RoomResponse),true);
                                     
                                        if(isset($sur_charges['RateInfos']['RateInfo']['ChargeableRateInfo']['Surcharges'])){
                                            $type = '@type';
                                            $amount = '@amount';
                                            $size = '@size';
                                            
                                            $temp_surcharge = $sur_charges['RateInfos']['RateInfo']['ChargeableRateInfo']['Surcharges']['Surcharge'];
                                            if(!isset($temp_surcharge[0])){
                                              $tempSurcharge[0] = $temp_surcharge;
                                              unset($temp_surcharge);
                                              $temp_surcharge = $tempSurcharge;
                                            }
                                            foreach($temp_surcharge as $charges){
                                               ?>
                                              <tr>
                                                <td><strong><?php echo e((isset($surcharge_components[$charges[$type]]) ? $surcharge_components[$charges[$type]]:'')); ?>:</strong></td>
                                                <td class="text-right">$AUD</td>
                                                <td class="text-right"><?php echo e($charges[$amount]); ?></td>
                                              </tr>
                                              <?php  
                                            }
                                        }else{
                                          ?>
                                          <tr>
                                              <td><strong>Taxes:</strong></td>
                                              <td class="text-right">$AUD</td>
                                              <td class="text-right"><?php echo e($taxes); ?></td>
                                          </tr> 
                                          <?php 
                                        }
                                       ?>
                                      
                                    </tbody>
                                  </table>
                                  <div class="accomodationPrice m-t-20">
                                    <h3><strong>$AUD <?php echo e($total); ?></strong></h3>
                                    <p><?php echo e($nights); ?> <?php echo e(($nights > 1 ? 'Nights':'Night')); ?><br/>Rates are quoted in Australian Dollars.</p>
                                  </div>
                                  <?php 
                                   
                                   ?>
                                  <button <?php echo e($disabled_status); ?> type="button" name="" class="btn btn-secondary active btn-block m-t-20 selected-room" data-provider="<?php echo e($data->provider); ?>" data-hotel="<?php echo e($data->hotelId); ?>" data-room="<?php echo e(json_encode($RoomResponse)); ?>">ADD TO ITINERARY</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
           
            <div class="accomodation-bottom-content" id="overview">
              <div class="accomodation-bottom-inner">
                <div class="accomodationMap">
                  <div id="map" style="height: 400px;width:100%"></div>
                    <input id="pac-input" class="form-control full-width" type="text" readonly data-lat="<?php echo e($data->latitude != '' ? $data->latitude : ''); ?>" data-lng="<?php echo e($data->longitude != '' ? $data->longitude : ''); ?>" value="<?php echo e($data->google_map_location != '' ? $data->google_map_location : ''); ?>" placeholder="Search Box" style="display:none;"> 
                      <div id="infowindow-content">
                      <img src="" width="16" height="16" id="place-icon">
                      <span id="place-name" class="place-title"></span><br>
                      <span id="place-address"></span>
                      </div>
                </div>
                <div class="m-t-30">
                  
                  <div class="m-t-20">
                    
                    <?php if(isset($data->HotelDetails->locationDescription)): ?>
                    <strong>Location</strong><br/>
                      <?php echo e($data->HotelDetails->locationDescription); ?>

                    <?php else: ?>
                      <p align="center" style="color:gray;font-size:30px">No data Available.<br/>
                    <?php endif; ?>
                  </div>
                
                </div>
              </div>
            </div>
            
             <?php if($data->provider=="expedia"): ?>
            <div class="accomodation-bottom-content" id="importantInfo">
              <div class="accomodation-bottom-inner">
                <div class="m-t-20">
                  <h4 class="hotel-title"><a href="#"><?php echo e($data->hotelName); ?></a></h4>
                  <p><?php echo e($data->country_name); ?>, <?php echo e($data->searchCity); ?> / <?php echo e($data->hotelCity); ?></p>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <p><strong>Hotel Amenities</strong></p>
                    <div class="row">
                      <div class="col-sm-6">
                        <ul>
                           <?php if($data->PropertyAmenities->PropertyAmenity): ?>
                            <?php foreach($data->PropertyAmenities->PropertyAmenity as $key => $value): ?>
                               <?php if($key % 2 == 0): ?>
                                <li><?php echo e($value->amenity); ?></li>
                               <?php endif; ?>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </ul>
                      </div>
                      <div class="col-sm-6">
                        <ul>
                           <?php if($data->PropertyAmenities->PropertyAmenity): ?>
                            <?php foreach($data->PropertyAmenities->PropertyAmenity as $key => $value): ?>
                               <?php if($key % 2 != 0): ?>
                                <li><?php echo e($value->amenity); ?></li>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </ul>
                      </div>
                    </div>
                  
                    <div class="m-t-30">
                      
                      <?php if(isset($data->HotelDetails->diningDescription)): ?>
                      <p><strong>Where To Eat</strong></p>
                      <p><?php echo e($data->HotelDetails->diningDescription); ?></p>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-sm-6">
                   

                    <div class="m-t-20">
                      <?php if($data->checkInInstructions): ?>
                      <?php 
                        $data->checkInInstructions = strstr($data->checkInInstructions, '<p><b>Fees</b>');
                        $data->checkInInstructions = str_replace('<p><b>Fees</b>','<p><strong>Fees / Optional Extras</strong>',$data->checkInInstructions);
                       ?>
                      <?php echo html_entity_decode($data->checkInInstructions); ?>

                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
             <?php endif; ?>
              
      </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>
 

    <script type="text/javascript" src="<?php echo e(url( 'assets/js/theme/jquery.slimscroll.js' )); ?>"></script>
    <script type="text/javascript">
      
      function calculateHeight(){

        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        if(winHeight < oheight){ 
          $(".page-content .tabs-content-container").outerHeight(elemHeight);
        } else{
          $(".page-content .tabs-content-container").outerHeight(winelemHeight);
        }
      }

      function accomodationHideShow(){  
        $('.accomodation-bottom-content').eq(0).show();
        $('.accomodation-tab').click(function(){
            var href_value = $(this).attr('data-id');
            if(href_value == 'roomRates'){
              $('.roomRates-filter').show();
              calculateHeight();
            }else{
              $('.roomRates-filter').hide();
              calculateHeight();
            }
            $('#' + href_value).show();
            $('#' + href_value).addClass("accomodation-open");
            $('#' + href_value).siblings('.accomodation-bottom-content').hide();
            $('.accomodation-tab').removeClass('active');
            $(this).addClass("active");
            calculateHeight();
            init_map();
        });
      }

      
      $(document).ready(function(){

        var map, marker, infowindow, infowindowContent;
          //map();
          calculateHeight();
          accomodationHideShow();

          var tran = $('.accomodationPrice').outerHeight();
          $(".transport-img").outerHeight(tran);

          $('.accomodation-bottom-inner').slimScroll({
              height: '100%',
              color: '#212121',
              opacity: '0.7',
              size: '5px',
              allowPageScroll: true
          }); 
      });

      var search_session     = JSON.parse( $('#search-session').val() );
      var city_id     = "<?php echo e($data->city_id); ?>";
      var leg     = "<?php echo e($data->leg); ?>";
      $('body').on('click', '.selected-room', function(){
      // UPDATED BY RGR
      $('.selected-room').prop('disabled', false);
        $(this).prop('disabled', true);
        var data         = $(this).data();

        
        data.leg = leg;
        if(data.provider == 'expedia'){
          default_provider   = data.provider;
        }
        
      

        if(data.provider == 'eroam' && data.roomName.includes('Dorm')){
          var perPersonPrice = Math.ceil( eroam.convertCurrency( data.price, data.currency ) ).toFixed(2);
        }else{
          var perPersonPrice =  Math.ceil( eroam.convertCurrency( data.price, data.currency )).toFixed(2);
        }

        $('#hotel-price-'+data.hotelId).html('From Price <strong>'+globalCurrency+' '+Math.ceil(perPersonPrice).toFixed(2)+'</strong> (Per Night Per Person)');
        $('.hotel-list').removeClass('selected-hotel').attr('data-is-selected', 'false');
        $('.hotel-list1-'+data.id).addClass('selected-hotel').attr('data-is-selected', 'true').attr('data-price', data.price).attr('data-price-id', data.priceId);
        $('.room-types').modal('hide');
      
        var session = search_session.itinerary;

        if(session.length > 0){
          if(session[leg].city.id == city_id ){

            
            eroam.ajax('post', 'room-details', data, function(response) {
                data = response;
                var data = JSON.parse(data);
                search_session.itinerary[leg].hotel = data;
                //search_session.itinerary[0].hotel.expedia_rooms = JSON.parse($('#expedia_rooms').val());
                if(leg == 0){
                  search_session.itinerary[0].city.accommodation = true;
                }
                bookingSummary.update( JSON.stringify( search_session ) );
            });
          }
        }

    });
     
        function init_map() {

    map = new google.maps.Map(document.getElementById('map'), { zoom: 11 });
    // INFO WINDOW
    infowindow = new google.maps.InfoWindow();
    infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);

    // MARKER
    marker = new google.maps.Marker({
      map: map,
      anchorPoint: new google.maps.Point(0, -29)
    });

    var searchBox = $('#pac-input');
    var geocoder = new google.maps.Geocoder;

    if (searchBox.data('lat') != '' && searchBox.data('lng')) { // REVERSE GEOCODING
      var latlng = {lat: searchBox.data('lat'), lng: searchBox.data('lng')};
      geocoder.geocode({'location': latlng}, function(results, status) {
        if (status.toLowerCase() == 'ok') {
          var service = new google.maps.places.PlacesService(map);
          service.getDetails({
            placeId: results[0].place_id
          }, function(place, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              showPlaceOnMap(place);
            }
          });
        }
      });

    } else {
      geocoder.geocode({
        address: searchBox.val(),
        region: 'no',
      }, function(results, status) {
        if (status.toLowerCase() == 'ok') {
          var service = new google.maps.places.PlacesService(map);
          service.getDetails({
            placeId: results[0].place_id
          }, function(place, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
              showPlaceOnMap(place);
            }
          });
        }
      });
    }
  }

  function showPlaceOnMap(place) {
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
    }
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindowContent.children['place-icon'].src = place.icon;
    infowindowContent.children['place-name'].textContent = place.name;
    infowindowContent.children['place-address'].textContent = address;
    infowindow.open(map, marker);
  }
      function map(){
        // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'eRoam!'
                });
            }
      }

      /*------------- Jcarousel slider function Starts -------------- */
      (function($) {
        $(function() {
            var jcarousel = $('.jcarousel');

            jcarousel
                .on('jcarousel:reload jcarousel:create', function () {
                    var carousel = $(this),
                        width = carousel.innerWidth();

                    if (width >= 1280) {
                        width = width / 4;
                    } else if (width >= 992) {
                        width = width / 4;
                    }
                    else if (width >= 768) {
                        width = width / 4;
                    }
                    else if (width >= 640) {
                        width = width / 4;
                    }

                    carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                })
                .jcarousel({
                    wrap: 'circular'
                });

            $('.jcarousel-control-prev')
                .jcarouselControl({
                    target: '-=1'
                });

            $('.jcarousel-control-next')
                .jcarouselControl({
                    target: '+=1'
                });

            $('.jcarousel-pagination')
                .on('jcarouselpagination:active', 'a', function() {
                    $(this).addClass('active');
                })
                .on('jcarouselpagination:inactive', 'a', function() {
                    $(this).removeClass('active');
                })
                .on('click', function(e) {
                    e.preventDefault();
                })
                .jcarouselPagination({
                    perPage: 1,
                    item: function(page) {
                        return '<a href="#' + page + '">' + page + '</a>';
                    }
                });
        });
    })(jQuery);
    $('.moreInfo').on('click', function () {
         (function($) {
    // This is the connector function.
    // It connects one item from the navigation carousel to one item from the
    // stage carousel.
    // The default behaviour is, to connect items with the same index from both
    // carousels. This might _not_ work with circular carousels!
    var connector = function(itemNavigation, carouselStage) {
        return carouselStage.jcarousel('items').eq(itemNavigation.index());
    };

    $(function() {
        // Setup the carousels. Adjust the options for both carousels here.
        var carouselStage      = $('.carousel-stage').jcarousel();
        var carouselNavigation = $('.carousel-navigation').jcarousel();

        // We loop through the items of the navigation carousel and set it up
        // as a control for an item from the stage carousel.
        carouselNavigation.jcarousel('items').each(function() {
            var item = $(this);

            // This is where we actually connect to items.
            var target = connector(item, carouselStage);

            item
                .on('jcarouselcontrol:active', function() {
                    carouselNavigation.jcarousel('scrollIntoView', this);
                    item.addClass('active');
                })
                .on('jcarouselcontrol:inactive', function() {
                    item.removeClass('active');
                })
                .jcarouselControl({
                    target: target,
                    carousel: carouselStage
                });
        });

        // Setup controls for the stage carousel
        $('.prev-stage')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.next-stage')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        // Setup controls for the navigation carousel
        $('.prev-navigation')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.next-navigation')
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });
    });
})(jQuery);
    });  
    function calculateHeight(){
        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var roomDatesFilter = $('.roomRates-filter').outerHeight();
        //alert(roomDatesFilter);
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        var accomodationTop = $('.page-content .accomodation-top-content').outerHeight();
        var accomodationBottom = (elemHeight - accomodationTop) - roomDatesFilter;
        var winaccomodationBottom = (winelemHeight - accomodationTop) - roomDatesFilter;
        if(winHeight < oheight){ 
          $(".page-content .tabs-content-container").outerHeight(elemHeight);
          $(".page-content .accomodation-bottom-content").outerHeight(accomodationBottom);
        } else{
          $(".page-content .tabs-content-container").outerHeight(winelemHeight);
          $(".page-content .accomodation-bottom-content").outerHeight(winaccomodationBottom);
        }
      }
      function sortByDate(activities){
        var sorted = activities.sort(function (itemA, itemB) {
        var A = itemA.date_selected;
        var B = itemB.date_selected;
          return A.localeCompare(B);
        });
       return sorted;
    }

    
</script>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>