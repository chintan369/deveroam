<?php echo $__env->make( 'layouts.header' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<section class="inner-page content-wrapper">
      	<h1 class="hide"></h1>
      	<div class="map-wrapper">
	      	<div class="mapIcons-wrapper">
					<?php echo $__env->yieldContent( 'content' ); ?>
			</div>
      		<div id="map"></div>
		</div>
	</section>
<?php echo $__env->make( 'layouts.footer' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>