<?php echo $__env->make( 'layouts.header' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<section class="inner-page content-wrapper">

				<?php echo $__env->yieldContent( 'content' ); ?>

	</section>
<?php echo $__env->make( 'layouts.footer' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>