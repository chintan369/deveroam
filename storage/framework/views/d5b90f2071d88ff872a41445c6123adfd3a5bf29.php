<?php echo $__env->make('user.search_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $__env->startSection('content'); ?>
<section>
    <?php echo $__env->make('user.profile_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    <?php echo $__env->make('user.profile_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    <?php if(session()->has('profile_step3_success')): ?>
                        <p class="success-box m-t-30">
                            <?php echo e(session()->get('profile_step3_success')); ?>

                        </p>
                    <?php endif; ?>
                    <?php if(session()->has('profile_step3_error')): ?>
                        <p class="danger-box m-t-30">
                            <?php echo e(session()->get('profile_step3_error')); ?>

                        </p>
                    <?php endif; ?>
                    <div class="m-t-30 profile-prefrence">
                <h2 class="profile-title">Personal Preferences</h2>
                <form class="form-horizontal m-t-20" method="post" id="profile_step3_form"> 
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Age Group</label>
                    <select id="age-group" name="age_group" class="form-control age_group">
                        <option value="">Select Age Group</option>
                        <?php foreach($travel_preferences['age_groups'] as $age_group): ?>
                            <option value="<?php echo e($age_group['id']); ?>" <?php echo e($user->customer->pref_age_group_id == $age_group['id'] ? 'selected' : ''); ?>><?php echo e($age_group['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Transport Type</label>    
                      <select class="form-control transport_type_options" name="transport_types[]" id="dropdown-transport">
                      <option value="">Select Transport Type</option>                
                        <?php echo $transport_type_options; ?>

                      </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Gender</label>
                    <select class="form-control gender" name="gender">
                        <option value="">Select Gender</option>
                        <option value="male" <?php echo e($user->customer->pref_gender == 'male' ? 'selected' : ''); ?>>Male</option>
                        <option value="female" <?php echo e($user->customer->pref_gender == 'female' ? 'selected' : ''); ?>>Female</option>
                    </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Accommodation Type</label>
                    <select class="form-control hotel_category"  name="hotel_category_id[]">
                        <option>Select Accommodation Type</option>
                        <option value="1" <?php echo e($user->customer->pref_hotel_categories && in_array(1, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : ''); ?>> 1 Star</option>
                        <option value="5" <?php echo e($user->customer->pref_hotel_categories && in_array(5, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : ''); ?>> 2 Star</option>
                        <option value="2" <?php echo e($user->customer->pref_hotel_categories && in_array(2, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : ''); ?>> 3 Star</option>
                        <option value="3" <?php echo e($user->customer->pref_hotel_categories && in_array(3, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : ''); ?>> 4 Star</option>
                        <option value="9" <?php echo e($user->customer->pref_hotel_categories && in_array(9, explode(',', $user->customer->pref_hotel_categories)) ? 'selected' : ''); ?>> 5 Star</option>
                    </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Nationality</label>                    
                    <select id="nationality"  name="nationality" class="form-control nationality" >
                        <option value="">Select Nationality</option>
                        <?php foreach($travel_preferences['nationalities']['featured'] as $nationality): ?>
                            <option value="<?php echo e($nationality['id']); ?>" <?php echo e($user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : ''); ?>><?php echo e($nationality['name']); ?></option>
                        <?php endforeach; ?>
                            <option disabled> ======================================== </option>
                        <?php foreach($travel_preferences['nationalities']['not_featured'] as $nationality): ?>
                            <option value="<?php echo e($nationality['id']); ?>" <?php echo e($user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : ''); ?>><?php echo e($nationality['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="panel-form-group form-group">
                    <label class="label-control">Select Room Type</label>
                    <select class="form-control room_type_options" name="room_type">
                        <option>Select Room Type</option>
                        <option value="2" <?php echo e($user->customer->pref_hotel_room_types && in_array(2, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : ''); ?>>Single</option>
                        <option value="1" <?php echo e($user->customer->pref_hotel_room_types && in_array(1, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : ''); ?>>Twin</option>
                        <option value="8" <?php echo e($user->customer->pref_hotel_room_types && in_array(8, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : ''); ?>>Triple</option>
                        <option value="22" <?php echo e($user->customer->pref_hotel_room_types && in_array(22, explode(',', $user->customer->pref_hotel_room_types)) ? 'selected' : ''); ?>>Quad</option>
                    </select>
                  </div>                  
                  <div class="m-t-20">
                <h4 class="modal-title" id="myModalLabel">Activity Preferences</h4>
                <p>Please drag your preferred activities into the boxes below. Highest priority begins on the left.</p> 

                <div class="m-t-10">
                <ul class="drop-list">

                  <?php 
                      $imgArr = array(
                        15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                        5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                        13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                        29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                        11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                        20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                         //'Expeditions','Astronomy','Food','Short-Breaks'
                        );

                      $ij = 0;
                      if( count($interest_ids) > 0 ){
                        foreach ($interest_ids as $j => $interest_id){
                          $key = array_search($interest_id, array_column($labels, 'id'));
                          $label = $labels[$key];
                          $ij++;
                  ?>
                            <li ondrop="drop(event)" ondragover="allowDrop(event)" class="interest-button-active" id="interest-<?php echo e($label['id']); ?>" data-sequence="<?php echo e($ij); ?>" data-name="<?php echo e($label['name']); ?>" style="width:81px !important;height: 81px !important">
                              <img src="<?php echo url( 'assets/images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_<?php echo e($label['id']); ?>" class="img-responsive" alt="<?php echo e($j); ?> - <?php echo e($label['name']); ?>" data-value="<?php echo e($label['id']); ?>" data-name="<?php echo e($label['name']); ?>" title="<?php echo e($label['name']); ?>">
                              <input type="hidden" id="inputValue_<?php echo e($label['id']); ?>" class="input-interest" name="interests[<?php echo e($j + 1); ?>]" value="<?php echo e($label['id']); ?>">
                            </li>
                  <?php  } } ?>

                  <?php 
                    for($k = $ij; $k < 8; $k++){
                      echo '<li ondrop="drop(event)" ondragover="allowDrop(event)" class="blankInterest" data-sequence="'.($k + 1).'"  style="width:81
                      px !important;height: 81px !important"></li>';
                    }
                  ?>
                </ul>
                <div class="clearfix"></div>
              </div>

              <hr/>
              <div class="m-t-10">

                <ul class="drag-list interest-lists first">
                  <?php if( count($labels) > 0 ): ?>
                    <?php foreach($labels as $i => $label): ?>

                      <?php if( empty($label['name']) ): ?>
                        <?php continue; ?>
                      <?php endif; ?>
                      <li ondrop="drop(event)" ondragover="allowDrop(event)" data-value="<?php echo e($label['id']); ?>" class="<?php echo e(in_array($label['id'], $interest_ids ) ? ' interestMove' : ''); ?>" id="interest-<?php echo e($label['id']); ?>">
                        <?php
                          $namedis = 'block';
                          if(!in_array($label['id'], $interest_ids )){
                            $namedis = 'none';
                        ?>
                          <img src="<?php echo url( 'assets/images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_<?php echo e($label['id']); ?>" class="img-responsive" alt="<?php echo e($i); ?> - <?php echo e($label['name']); ?>" data-value="<?php echo e($label['id']); ?>"  data-name="<?php echo e($label['name']); ?>" title="<?php echo e($label['name']); ?>">
                          <input type="hidden" id="inputValue_<?php echo e($label['id']); ?>" class="input-interest" name="interests[]" value="">
                        <?php } ?>
                          <span style="display:<?php echo e($namedis); ?>;" id="name_<?php echo e($label['id']); ?>"><?php echo e($label['name']); ?></span>
                          
                      </li>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </ul>
                <div class="clearfix"></div>
              </div>
              </div>
                <div class="m-t-20">
                    <button type="submit" name="" id="save_travel_preferences" class="btn btn-black btn-block">UPDATE PROFILE</button>
                </div>
                <input type="hidden" name="user_id" id="user_id" value="<?php echo e($user->id); ?>">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                </form>
              </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<script type="text/javascript">
  var activitySequence = [<?php echo e(join(', ', $interest_ids)); ?>];
</script>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_leftmenu.js?v='.$version.'')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_step3.js?v='.$version.'')); ?>"></script>
<?php echo $__env->make('layouts.profileLayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>