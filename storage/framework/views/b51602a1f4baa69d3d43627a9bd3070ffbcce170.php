
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>:: eroam ::</title>
    <style type="text/css">
       body{font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; font-size: 14px; color: #212121;}
       .content-container{width: 700px; margin: 0 auto;}
      .box-wrapper {
          padding: 20px 30px 20px 30px;
          position: relative;
          margin-bottom: 20px;
          background: #FFFFFF;
          border: 1px solid #F8F8F8;
          box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
          -webkit-box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
          -moz-box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
          -ms-box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
          border-radius: 2px;
      }
      .row::after{clear: both; display: block; content: "";}
      .box-wrapper p{margin: 0 0 10px 0;}
      .m-t-20{margin-top: 20px;}
      strong{font-weight: bold;}
      table tr td{vertical-align: top;}
    </style>

  </head>
  <body style="font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; font-size: 14px; color: #212121;">
    <div class="content-container" style="width: 700px; margin: 0 auto;">
      <h1 style="font-size: 20px; color: #212121; letter-spacing: 0.71px; line-height: 18px; margin-top: 10px; margin-bottom: 25px;">Booking Information</h1> 
      <div class="box-wrapper" style="padding: 20px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef;">
        <h4 style="font-size: 20px; line-height: 22px; font-weight: 500; margin-top: 0px; margin-bottom: 20px; margin-left: 7px;">Personal Details</h4>
        <?php
          if(isset($return_response) && !empty($return_response)){
              $name = $return_response['passenger_info'][0]['passenger_first_name'].' '.$return_response['passenger_info'][0]['passenger_last_name'];
              $email = $return_response['passenger_info'][0]['passenger_email'];
              $contact = $return_response['passenger_info'][0]['passenger_contact_no'];
              $gender = $return_response['passenger_info'][0]['passenger_gender'];
              $dob = date('d-m-Y',strtotime($return_response['passenger_info'][0]['passenger_dob']));
              $country = $return_response['passenger_info'][0]['passenger_country_name'];
        ?>
        <table border="0" width="100%" cellspacing="4" cellpadding="4">
        <tr>
          <td colspan="2"><strong>Booking Id:</strong> <?php echo e($return_response['invoiceNumber']); ?></td>
        </tr>
        <tr>
          <td width="50%"><strong>Lead Person Name:</strong> <?php echo e($name); ?></td>
          <td width="50%"><strong>Lead Person Email:</strong> <?php echo e($email); ?></td>
        </tr>
        <tr>
          <td width="50%"><strong>Lead Person Contact:</strong> <?php echo e($contact); ?></td>
          <td width="50%"><strong>Lead Person Gender:</strong> <?php echo e($gender); ?></td>
        </tr>
        <tr>
          <td width="50%"><strong>Lead Person Dob:</strong> <?php echo e($dob); ?></td>
          <td width="50%"><strong>Lead Person Country:</strong> <?php echo e($country); ?></td>
        </tr>
        <tr>
          <td width="50%"><strong>Days:</strong> <?php echo e($return_response['total_days']); ?></td>
          <td width="50%"><strong>Total Amount:</strong> <?php echo e($return_response['currency']); ?> <?php echo e($return_response['totalAmount']); ?></td>
        </tr>
        <tr>
          <td width="50%"><strong>Date:</strong> <?php echo e(date( 'j M Y', strtotime($return_response['from_date']))); ?> - <?php echo e(date( 'j M Y',strtotime($return_response['to_date']))); ?></td>
          <td width="50%"><strong>Travellers:</strong> <?php echo e($return_response['travellers']); ?></td>
        </tr>
        </table>
      </div>
       <?php 
                
	    $last_key = count($return_response['leg']) -1;
	    foreach ($return_response['leg'] as $key => $value) {
    	?>
      <div class="box-wrapper" style="padding: 20px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef; border-radius: 2px;">
        <h4 style="font-size: 20px; line-height: 22px; margin-top: 0px; margin-bottom: 20px; margin-left: 7px;"><?php echo e($value['from_city_name']); ?>, <?php echo e($value['country_code']); ?></h4>
        <table border="0" width="100%" cellspacing="4" cellpadding="4">
        	<?php 
                if(isset($value['hotel']) && !empty($value['hotel'])){
            ?>
		          <tr>
		            <td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="images/hotel-icon.png" alt="" style="color: #000;"/> Accommodation</h5></td>
		          </tr>
		          <tr>
		            <td width="50%"><strong>Accommodation Name:</strong> <?php echo e($value['hotel'][0]['leg_name']); ?></td>
		            <td width="50%"><strong>Duration:</strong> <?php echo e($value['hotel'][0]['nights']); ?> Nights</td>
		          </tr>
		          <tr>
		            <td width="50%"><strong>Check-in:</strong> <?php echo e(date( 'j M Y', strtotime($value['hotel'][0]['checkin']))); ?></td>
		            <td width="50%"><strong>Check-out:</strong> <?php echo e(date( 'j M Y', strtotime($value['hotel'][0]['checkout']))); ?></td>
		          </tr>
		          <tr>
		            <td width="50%"><strong>Price:</strong> <?php echo e($value['hotel'][0]['currency']); ?> <?php echo e(ceil($value['hotel'][0]['price'])); ?></td>
		             <?php  
                      $confirm = 'NOT CONFIRMED';
                      if($value['hotel'][0]['provider_booking_status'] == 'CF'){
                          $confirm = 'CONFIRMED';
                      }
                     ?>
                <td width="50%"><strong>Booking Status:</strong> <?php echo e($confirm); ?>

		            <?php
                    if(empty($value['hotel'][0]['provider_booking_status'])){
                      ?>
                       <span style="color:red;"> *</span>
                      <?php
                    }
                    ?>	
		            </td>
		          </tr>
		          <?php
                    if(!empty($value['hotel'][0]['provider_booking_status'])){
                      ?>
	                  <tr>	
	                    <td width="50%"><strong>Booking Id:</strong> <?php echo e($value['hotel'][0]['booking_id']); ?>

	                    </td> 
	                  </tr>  
	              <?php
	                }
	               ?>
		          <tr>
		            <td colspan="2"></td>
		          </tr>
		          <tr>
		            <td colspan="2"></td>
		          </tr>
		        <?php
                }else{
	            ?>
		         	<tr>
			            <td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="images/hotel-icon.png" alt="" style="color: #000;"/> Accommodation</h5></td>
			          </tr>
			           <tr>
			                <td width="50%">Own Arrangement</td>
              			</tr>
			          <tr>
			            <td colspan="2"></td>
			          </tr>
			          <tr>
			            <td colspan="2"></td>
			         </tr>
	            <?php
                }
        if(isset($value['activities']) && !empty($value['activities'])){
                  foreach ($value['activities'] as $key1 => $value1) {        
        ?>          	
          <tr>
            <td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="images/activity-icon.png" alt="" style="color: #000;"/> Activity Summary</h5></td>
          </tr>
          <tr>
            <td width="50%"><strong>Activity Name:</strong> <?php echo e($value1['leg_name']); ?></td>
            <td width="50%"><strong>Date:</strong> <?php echo e(date( 'j M Y', strtotime($value1['from_date']))); ?></td>
          </tr>
          <tr>
            <td width="50%"><strong>Price:</strong> <?php echo e($value1['currency']); ?> <?php echo e(ceil($value1['price'])); ?></td>
            <td width="50%"><strong>Booking Status:</strong>  <?php echo e(($value1['provider_booking_status'] == 'CONFIRMED')? 'CONFIRMED':'NOT CONFIRMED'); ?>

            <?php
                if($value1['provider_booking_status'] != 'CONFIRMED'){
                  ?>
                   <span style="color:red;"> *</span>
                  <?php
                }
            ?>	
            </td>
          </tr>
          
          <?php
	         if($value1['provider_booking_status'] == 'CONFIRMED'){  
	      ?>
	        <tr>
	          <td width="50%"><strong>Booking Id:</strong> <?php echo e($value1['booking_id']); ?></td>
	        </tr>
           <tr>
            <td colspan="2"></td>
          </tr>
          <tr>
            <td colspan="2"></td>
          </tr>
	      <?php 
	        }
           ?>
          <tr>
            <td colspan="2"></td>
          </tr>
          <tr>
            <td colspan="2"></td>
          </tr>
  
        <?php 
    	}
    	}else{
    		?>
    		<tr>
	          <td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="images/hotel-icon.png" alt="" style="color: #000;"/> Activity Summary</h5></td>
	          </tr>
	           <tr>
	                <td width="50%">Own Arrangement</td>
      			</tr>
	          <tr>
	            <td colspan="2"></td>
	          </tr>
	          <tr>
	            <td colspan="2"></td>
	         </tr>
    		<?php
    	}
    	if(isset($value['transport']) && !empty($value['transport'])){
                  $depart = $value['transport'][0]['departure_text'];
                  $arrive = $value['transport'][0]['arrival_text'];
                  if($value['transport'][0]['leg_name'] == 'Flight'){
                      if(!empty($value['transport'][0]['booking_summary_text'])){
                        $leg_depart = explode("Depart:",$value['transport'][0]['booking_summary_text'],2);
                        $leg_arrive = explode("Arrive:",$value['transport'][0]['booking_summary_text'],2);
                        
                        if(isset($leg_depart[1])){
                          $depart = explode("</small>",$leg_depart[1],2);
                          $depart = $depart[0];
                        }else{
                          $depart = '';
                        }
                        if(isset($leg_arrive[1])){
                          $arrive = explode("</small>",$leg_arrive[1],2);
                          $arrive = $arrive[0];
                        }else{
                          $arrive = '';
                        }
                      }else{
                         $depart = '';
                         $arrive = '';
                      }
                  }
    	?>  
    	 <tr>
            <td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="images/transport-icon.png" alt="" style="color: #000;"/> Transport</h5></td>
          </tr>  
          <tr>
          	<?php 
              $leg_name = explode('Depart:', $value['transport'][0]['booking_summary_text']);
              if(isset($value['transport'][0]['booking_summary_text']) && !empty($value['transport'][0]['booking_summary_text'])){
                $leg_name = trim(strip_tags($leg_name[0]));
              }else{
                $leg_name = strip_tags($value['transport'][0]['leg_name']);
              }
            ?>
            <td colspan="2"><strong>Service:</strong> <?php echo e($leg_name); ?></td>
          </tr>  
          <tr>
            <td width="50%"><strong>Departure:</strong> <?php echo e(strip_tags($depart)); ?></td>
            <td width="50%"><strong>Arrival:</strong> <?php echo e(strip_tags($arrive)); ?></td>
          </tr>
          <tr>
            <td width="50%"><strong>Price:</strong> <?php echo e($value['transport'][0]['currency']); ?> <?php echo e(ceil($value['transport'][0]['price'])); ?></td>
            <td width="50%"><strong>Booking Status:</strong> <?php echo e(($value['transport'][0]['provider_booking_status'] == 'CONFIRMED')? 'CONFIRMED':'NOT CONFIRMED'); ?>

            <?php
                if($value['transport'][0]['provider_booking_status'] != 'CONFIRMED'){
                  ?>
                   <span style="color:red;"> *</span>
                  <?php
                }
            ?>	
            </td>
          </tr>
          <?php
            if($value['transport'][0]['provider_booking_status'] == 'CONFIRMED'){
          ?>
             <tr>  
                <td width="50%"><strong>Booking Id:</strong> <?php echo e($value['transport'][0]['booking_id']); ?>

                  </td> 
            </tr>
            <?php 
            }
            ?> 
        <?php
        }else{
        	if($key != $last_key){
        	?>
	            <tr>
		         <td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="images/transport-icon.png" alt="" style="color: #000;"/> Transport</h5></td>
		          </tr>
		         <tr>
		             <td width="50%">Self-Drive / Own Arrangement</td>
	      		</tr>
            <?php 
         	}
     	}
        ?>	
         
         
        </table>

      </div>
      <?php 
  		}
  	}
  	?>	
    <p><span style="color:red;"> *</span> For NOT CONFIRMED Booking Please contact to eRoam Administrator.</p>  
    </div>
  </body>
</html>