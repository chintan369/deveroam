<?php echo $__env->make( 'layouts.search_header' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="clearfix"> </div>
	<div class="page-container">
		<?php /* <?php if( request()->segment( 1 ) == 'map' ): ?>
			<form action="<?php echo e(url( 'search' )); ?>" method="post">
				<div class="white-box row margin-btm">
					<div class="col-xs-12">
						<div class="row content-header no-padding-btm">
							<div class="option-container col-xs-6">
								<a href="#" data-type="manual" class="search-option-btn padding <?php echo e(session()->get( 'search_input' )['option'] == 'manual' ? 'active' : ''); ?>">Multi-City Manual</a>
								<input class="search-option" type="radio" id="option1" name="option" value="manual" <?php echo e(session()->get( 'search_input' )['option'] == 'manual' ? 'checked' : ''); ?>>
							</div>
							<div class="option-container col-xs-6">
								<a href="#" data-type="auto" class="search-option-btn padding <?php echo e(session()->get( 'search_input' )['option'] == 'auto' ? 'active' : ''); ?>">Multi-City Auto</a>
								<input class="search-option" type="radio" id="option2" name="option" value="auto" <?php echo e(session()->get( 'search_input' )['option'] == 'auto' ? 'checked' : ''); ?>>
							</div>
						</div>
						<div class="row content-header">
							<div class="col-md-2">
								<select id="country" name="country" class="select-country">
									<option value="#">Starting Country</option>
									<?php foreach( $countries as $country ): ?>
										<?php if( count( $country['city'] ) > 0 ): ?>
											<option value="<?php echo e($country['id']); ?>"><?php echo e($country['name']); ?></option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-md-2">
								<select id="starting-point" name="city" class="select-city">
									<option value="#">Starting Point</option>
								</select>
							</div>
							<div class="col-md-2">
								<select id="number-of-travellers" name="travellers">
									<option value="">Travellers</option>
									<option value="1" <?php echo e(session()->get( 'search_input' )['travellers'] == "1" ? 'selected' : ''); ?>>1</option>
									<option value="2" <?php echo e(session()->get( 'search_input' )['travellers'] == "2" ? 'selected' : ''); ?>>2</option>
									<option value="3" <?php echo e(session()->get( 'search_input' )['travellers'] == "3" ? 'selected' : ''); ?>>3</option>
									<option value="4" <?php echo e(session()->get( 'search_input' )['travellers'] == "4" ? 'selected' : ''); ?>>4</option>
									<option value="5" <?php echo e(session()->get( 'search_input' )['travellers'] == "5" ? 'selected' : ''); ?>>5</option>
									<option value="6" <?php echo e(session()->get( 'search_input' )['travellers'] == "6" ? 'selected' : ''); ?>>6</option>
									<option value="7" <?php echo e(session()->get( 'search_input' )['travellers'] == "7" ? 'selected' : ''); ?>>7</option>
									<option value="8" <?php echo e(session()->get( 'search_input' )['travellers'] == "8" ? 'selected' : ''); ?>>8</option>
									<option value="9" <?php echo e(session()->get( 'search_input' )['travellers'] == "9" ? 'selected' : ''); ?>>9</option>
									<option value="10" <?php echo e(session()->get( 'search_input' )['travellers'] == "10" ? 'selected' : ''); ?>>10</option>
								</select>
							</div>
							<div class="col-md-2">
								<input type="text" name="start_date" id="date-of-travel" placeholder="Travel Date" value="<?php echo e(session()->get( 'search_input' )['start_date']); ?>">
							</div>
							<div class="col-md-4">
								<a href="#" class="update-search-btn padding"><i class="fa fa-search"></i> update</a>
							</div>
						</div>
						<div class="row content-header <?php echo e(session()->get( 'search_input' )['option'] == 'manual' ? 'hide' : ''); ?>" id="auto-container">
							<div class="col-md-2">
								<select id="to-country" name="to_country" class="select-country">
									<option value="#">Destination Country</option>
									<?php foreach( $countries as $country ): ?>
										<?php if( count( $country['city'] ) > 0 ): ?>
											<option value="<?php echo e($country['id']); ?>"><?php echo e($country['name']); ?></option>
										<?php endif; ?>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-md-2">
								<select id="to-city" name="to_city" class="select-city">
									<option value="#">Destination</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
				<input type="submit" id="submit-btn" class="hide">
			</form>
		<?php endif; ?> */ ?>

		<!-- booking summary -->	
			<div class="page-sidebar-wrapper">
      		<div class="page-sidebar top-margin">
      			<div class="pageSidebar-inner">
					<div class="booking-summary" style="">
						<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>
					</div>
				</div>
			</div>
		</div>
		<!-- booking summary -->

		<div class="page-content-wrapper">
      		<div class="page-content top-margin">

				<div class="left-strip accomodationleftstrip">
					 <div class="leftStrip-icons">
						<?php 
							$function = Request::segment(1);
						    if($function == "map"){ 
						?>
							    <?php if( session()->get( 'map_data' )['type'] == 'auto' ): ?>
							      	<p class="map-icon">
								        <a href="#helpBoxDiv" id="edit-map-btn">
									        <i class="icon icon-edit" ></i>
								        </a>
							      	</p>
							      	
								<?php else: ?>
								    <p class="map-icon">
								        <a href="#helpBoxDiv" id="edit-map-btn">
									        <i class="fa fa-question-circle" id="edit-map-btn"></i>
								        </a>
								    </p>
								    
								<?php endif; ?>
						<?php } else { ?>
						    <p class="map-icon">
						        <a href="/map">
						            <i class="icon icon-map">
						           </i>
						        </a>
						    </p>
						    
						<?php } ?>  
					</div>
					<a href="#" class="arrow-btn-new open"><i class="fa fa-angle-left"></i></a>
				</div>
				
	          	
	            	<?php if($function == "map"){?>
	            		<div class="map-main-container">
			            	<div id="map" style="height: 1000px;"></div>
				            <div class="mapWrapper">
				            	<?php echo $__env->yieldContent( 'content' ); ?>
				            </div>
			            </div>
					<?php } else { ?>
						<div class="tabs-main-container accomodation-tabs-main-container">
				            <!-- <div id="map1" style="height: 1500px;"> -->
				            <div id="map1">
				            	<?php echo $__env->yieldContent( 'content' ); ?>
				            </div>
			        	</div>
					<?php } ?>
	          	
	        </div>
		</div>
	</div>
	<div class="clearfix"> </div>
<?php echo $__env->make( 'layouts.search_footer' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>