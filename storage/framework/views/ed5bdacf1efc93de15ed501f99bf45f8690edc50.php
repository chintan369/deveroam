<?php 

  // $season = array();
  // $t_return = array();
  //   if(isset($data['Seasons']) && !empty($data['Seasons'])){
  //     foreach ($data['Seasons'] as $key => $value) {
  //       if($value['flightPrice'] > 0){
  //         $season[$value['season_id']] = $value['flightPrice'];
  //         $t_return[$value['season_id']] = $value['flightReturn'];
  //       }
  //     }
  //   }
  $season = array();
  $t_return = array();
  $default_selected_city = session()->get( 'default_selected_city' );
    if(isset($data['tourFlightResult']) && !empty($data['tourFlightResult'])){
      foreach ($data['tourFlightResult'] as $key => $value) {
        if($default_selected_city == $value['flight_depart_city'] && $value['flightPrice'] > 0){
           $season[$value['season_id']] = $value['flightPrice'];
           $t_return[$value['season_id']] = $value['flightReturn'];
        }
        
      }
    }
    $tourDate = 0;
    if(session()->has('tourDate') && session()->get('tourDate') == 'Yes') {  
      $tourDate = 1;
    }

    $tourSession = 0;
    if(session()->has('tourSession') && session()->get('tourSession') == 'Yes') {  
      $tourSession = 1;
    }

?>


<?php $__env->startSection('custom-css'); ?>
  <style type="text/css">
   .tabs-wrapper {
        /*margin-left: 350px;*/
        padding-top: 48px;
       /* height: 995px;*/
    }
    .modelCountryList label.radio-checkbox span{
        color: rgba(0,0,0, .54);
    }

    .jcarousel-wrapper {
        margin: 20px auto 55px auto;
    }

    .jcarousel li {
        padding-left: 5px;
        padding-right: 5px;
    }

    .detailDesc h1{
      font-size: 13px;
      /*position: absolute;
      left: 90px;*/
    }
    #details ul{
      padding-left: 25px;
    }
    #details ol{
      padding-left: 25px;
    }
    #overview ul{
      padding-left: 25px;
    }
    #overview ol{
      padding-left: 25px;
    }
    .tourViewItineraryDiv  ul{
      padding-left: 25px;
    }
     .tourViewItineraryDiv  ol{
      padding-left: 25px;
    }

    .reviewComment {
      padding-left:  25px;
      text-align: justify;
      padding-top: 5px;
    }

    .detailDesc ul{
      padding-left: 25px;
    }

    .tour-itinerary, .tour-viewItinerary, #makeAnEnquiry{
      display:none;
    }

    .has-js .dateDiv label.r_on {
       background: url("<?php echo url('assets/images/check-on-black1.png'); ?>") no-repeat; 
    }

    .has-js .dateDiv .label_radio {
        background: url("<?php echo url('assets/images/check-off-black1.png'); ?>") no-repeat; 
    }

    .location-wrapper {
          height: 900px;
    }

    .panel-group { margin-bottom: 0px;}
    .page-content{
      margin-left:0px
    }
    .page-sidebar { display: none; }
    .left-strip { display: none; }

    .makeAnEnquiry1:focus{
      background: #FFFFFF;
      border: 1px solid rgba(223,223,223,0.30);
      color: #212121;
    }
  </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <?php 
    function existsImage($uri){
        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_https_CODE);
        curl_close($ch);

        return $code;// == 200;
    }
    //echo'<pre>';
    //print_r(Session::all());

    $payError = 0; 
    if(Session::has('error')){
      $payError = 1;
    }
    $is_DateRadioIdValue = 0;
    if(session()->has('DateRadioIdValue')){ 
      $is_DateRadioIdValue = session()->get('DateRadioIdValue');
    }
    //exit;

    //echo '<pre>';print_r($data['detail']);exit;
    //echo '<pre>'; print_r($data['detail']);  print_r($data['dates']);  echo '</pre>';  die;
    //echo '<pre>'; print_r($data);  echo '</pre>';  die;

    /** Review List & Review Star Start **/
        $i=0; $rating = 0; 
        $reviewsDetails = '<h5>No Review Found.</h5>';
        $end = '';
  
        foreach ($data['reviews'] as $review) {

          if($i == 0){
            $reviewsDetails = '<ul class="tour-list">';
            $end = '</ul>';
          } 

          if($review['comments'] != ''){
              $comment = $review['comments'];
          }
          elseif($review['review_detail'] != ''){
            $comment = $review['review_detail'];
          } else {
            $comment = 'N/A';
          }

          if(is_float($review['rating'])){
            $stars = get_stars((int)$review['rating'], true);
          }else {
            $stars = get_stars((int)$review['rating']);
          }

          $reviewsDetails .= '<div>
            <div class="row">
              <div class="col-sm-10"><i class="fa fa-circle" aria-hidden="true" style="font-size: 7px;"></i> &nbsp;&nbsp;&nbsp;&nbsp;<strong>'.$review['email'].'</strong></div>
              <div class="col-sm-2"><ul class="rating pull-right">'.$stars.'</ul></div>
              <div class="clearfix"></div>
            </div>
            <p class="reviewComment">'.$comment.'</p>
          </div>';

          $i++;
          $rating += $review['rating'];
        }
        $reviewsDetails .=$end;
        $reviewCount = $i;

        if($rating > 0){ $starRating = $rating/$i; } 
        else { $starRating = 0; }

        $total_duration = ''; 
        if($data['detail']['durationType'] == 'd'){
          $total_duration = 'Day';
          if(ceil($data['detail']['no_of_days']) > 1){
            $total_duration = 'Days';
          }
        }else if($data['detail']['durationType'] == 'h'){
          $total_duration = 'Hour';
          if(ceil($data['detail']['no_of_days']) > 1){
            $total_duration = 'Hours';
          }
        } 

        if(empty($data['detail']['discount']) || $data['detail']['discount'] == '.00'){
            $data['detail']['discount'] = 0;
        }
        if(empty($data['detail']['saving_per_person']) || $data['detail']['saving_per_person'] == '.00'){
            $data['detail']['saving_per_person'] = 0;
        }
       
    /** Review List & Review Star End **/
  ?>

      <div class="loader1"><img src="<?php echo e(url( 'assets/images/loader.gif' )); ?>" alt="Loader" /></div>  
      <div class="tabs-content-container">
      <div class="tab-content" id="myTabContent"> 

        <div class="tab-pane fade in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab"> 
          <div class="tour-wrapper">
            <div class="list-box list-box-new">
              <div class="tabs-content-container-new">
              <div class="tour-top-content">
                <div class="row">
                  <div class="col-sm-7">
                    <h4><?php echo e($data['detail']['tour_title']); ?></h4>
                    <p>
                      <svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
                          <!-- Generator: Sketch 46.2 (44496) - https://www.bohemiancoding.com/sketch -->
                          <desc>Created with Sketch.</desc>
                          <defs></defs>
                          <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">
                                  <g id="Group" transform="translate(501.000000, 542.000000)">
                                      <g id="Group-16">
                                          <path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>
                                      </g>
                                  </g>
                              </g>
                          </g>
                      </svg>
                      <?php 
                              $allCountry = '';
                              foreach ($data['countries'] as $country) {
                                $allCountry .= ucwords(strtolower($country['country_name'])).', ';
                              }
                              $allCountry = substr($allCountry, 0,-2);
                            ?> 
                      Tour Code: <?php echo e($data['detail']['tour_code']); ?>   |   Countries: <?php echo e($allCountry); ?> 
                    </p>
                  </div>
                  <div class="col-sm-5">
                    <div class="row">
                      <div class="col-xs-9 text-right border-right">
                        <?php 
                          if($data['detail']['flightPrice'] > 0){
                            ?>
                              <p>From <strong>$<?php echo e($default_currency.' '.number_format($data['detail']['price'] + $data['detail']['flightPrice'] ,2)); ?></strong> P.P (Inclusive Flights)</p>
                            <?php
                          }else{
                            ?>
                              <p>From <strong>$<?php echo e($default_currency.' '.number_format($data['detail']['price'],2)); ?></strong> P.P (Land Only)</p>
                            <?php
                          }
                          if($data['detail']['discount'] > 0){
                            ?>
                              <p class="red-text">eRoam Discount<strong> <?php echo e($data['detail']['discount']); ?>%</strong></p>
                            <?php
                          }
                          if($data['detail']['saving_per_person'] > 0){
                            ?>
                              <p class="red-text">Savings From<strong> $<?php echo e($default_currency); ?> <?php echo e(number_format($data['detail']['saving_per_person'],2)); ?></strong> P.P</p>

                            <?php
                          }
                        ?>
                        
                        <ul class="rating">
                          <?php 
                            if(is_float($starRating)){
                              $stars = get_stars((int)$starRating, true);
                            }else {
                              $stars = get_stars((int)$starRating);
                            }
                           // echo $stars; 
                          ?>
                        </ul>
                      </div>
                      <div class="col-xs-3 text-center">
                        <h3 class="transport-price"><strong><?php echo e($data['detail']['no_of_days']); ?></strong></h3>
                        <?php echo e($total_duration); ?>

                      </div>
                    </div>
                  </div>
                </div>
                <hr/>
                
                <div class="clearfix">
                  <div class="logo-img">
                    <?php 
                      $tourlogo = 'http://dev.cms.eroam.com/'.$data['detail']['logo_path'];
                    ?>
                    <img src="<?php echo e($tourlogo); ?>" alt="" class="img-responsive" />
                  </div>
                  <div class="logo-details">
                    <div class="row">
                      <div class="col-sm-8 col-md-8">
                        <div class="row">
                          <div class="col-md-2 col-sm-3"><strong>Countries:</strong></div>
                          <div class="col-md-10 col-sm-9">
                            <?php 
                              $allCountry = '';
                              foreach ($data['countries'] as $country) {
                                $allCountry .= ucwords(strtolower($country['country_name'])).', ';
                              }
                              echo substr($allCountry, 0,-2);
                            ?> 
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-2 col-sm-3"><strong>Route:</strong></div>
                          <div class="col-md-10 col-sm-9"><?php echo e($data['detail']['departure'].' - '.$data['detail']['destination']); ?></div>
                        </div>
                        <?php if(!empty($data['detail']['tripActivities'])){ ?>
                        <div class="row">
                          <div class="col-md-2 col-sm-3"><strong>Tour Theme:</strong></div>
                           <?php 
                              $tripActivities = '';
                              if(!empty($data['detail']['tripActivities'])){
                                  $tripActivities = explode(',',$data['detail']['tripActivities']);
                                  $tripActivities = $tripActivities[0];
                              }
                          ?>
                          <div class="col-md-10 col-sm-9"><?php echo e($tripActivities); ?></div>
                        </div>
                        <?php } ?>
                        <div class="row">
                        <div class="col-sm-12">
                           <div class="row">
                              <div class="col-md-2 col-sm-3"><strong>Accommodation:</strong></div>

                              <div class="col-md-10 col-sm-9"><?php echo e($data['detail']['accommodation']); ?></div>
                            </div>
                            <div class="row">
                              <div class="col-md-2 col-sm-3"><strong>Transport:</strong></div>
                              <div class="col-md-10 col-sm-9"><?php echo e($data['detail']['transport']); ?></div>
                            </div>
                          </div>  
                        </div>
                      </div>  
                      <div class="col-sm-4 col-md-4">
                        <div class="row">
                          <div class="col-md-4 col-sm-3"><strong>Group Size:</strong></div>
                          <div class="col-md-8 col-sm-9">Min <?php echo e($data['detail']['groupsize_min']); ?> <?php if($data['detail']['groupsize_max'] != 0): ?> - Max <?php echo e($data['detail']['groupsize_max']); ?> <?php endif; ?></div>
                        </div>  
                        <div class="row">
                          <div class="col-md-4 col-sm-3"><strong>Children:</strong></div>
                          <div class="col-md-8 col-sm-9"><?php echo e(($data['detail']['is_childAllowed'] == 1 ? "Yes":"No")); ?></div>
                        </div>
                        <?php 
                        if($data['detail']['children_age'] != '' && $data['detail']['is_childAllowed'] == 1){
                          ?>

                        <div class="row">
                          <div class="col-md-4 col-sm-3"><strong>Children Age:</strong></div>
                          <div class="col-md-8 col-sm-9"><?php echo e($data['detail']['children_age']); ?></div>
                        </div>
                        <?php
                        }
                        ?>
                      </div>    
                    </div>
                      
                  </div>
                </div>

                <?php if($data['images']){ ?> 
                  <div class="jcarousel-wrapper">
                      <div class="jcarousel">
                        <ul>
                          <?php 
                            $i=0;
                            foreach($data['images'] as $key => $image){
                              $i++;
                              //$imageUrl1 = "http://www.adventuretravel.com.au/".$data['detail']['folder_path']."245x169/".$image['image_thumb'];
                              $image['image_thumb'] = str_replace("thumbnail","small",$image['image_thumb']);
                              if($image['image_thumb'] != '' && $image['image_small'] != ''){
                                   $imageUrl2 = "http://dev.cms.eroam.com/".$image['image_thumb'];
                              }else{
                                   $imageUrl2 = "http://www.adventuretravel.com.au/".$data['detail']['folder_path']."245x169/".$image['image_thumb'];
                              }

                             // if(existsImage($imageUrl1) == 200){
                                //echo '<li><img src="'.$imageUrl1.'" alt="Image '.$i.'"></li>';
                             //}else if(existsImage($imageUrl2) == 200){
                                echo '<li><img src="'.$imageUrl2.'" alt="Image '.$i.'"></li>';
                              //}else { }
                             
                              /*<li><img src="http://dev.dev.cms.eroam.com/{{ $image['image_thumb'] }}" alt="Image {{$i}}"></li>
                                <li><img src="http://www.adventuretravel.com.au/{{ $data['detail']['folder_path'] }}245x169/{{ $image['image_thumb'] }}" alt="Image {{$i}}"></li>*/
                            } 
                          ?>
                        </ul>
                      </div>
                      <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                      <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                      <p class="jcarousel-pagination"></p>
                  </div>
                <?php } ?>

                <div class="m-t-20 row">
                 
                  <!--<div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block disable">CUSTOMISE ITINERARY</button>
                  </div>-->

                  <div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block viewItinerary active">VIEW ITINERARY</button>
                  </div>

                  <div class="col-sm-4 m-t-10">

                    <!-- <button type="button" class="btn btn-primary btn-block makeAnEnquiry1" onclick="window.open('<?php echo e(url('contact-us')); ?>', '_blank')">MAKE AN ENQUIRY</button> -->
                    <button type="button" class="btn btn-primary btn-block makeAnEnquiry1" data-toggle="modal" data-target="#enquiryModal" id="openModel" >MAKE AN ENQUIRY</button>
                  </div>

                  <div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block bookItinerary">DATES & RATES</button>
                  </div>

                  <?php /*<div class="col-sm-4 m-t-10">
                    <button type="button" name="" class="btn btn-primary btn-block saveItinerary">SAVE ITINERARY</button>
                  </div>

                  <div class="col-sm-4 m-t-10" style="display:none;">
                    <button type="submit" name="" class="btn btn-primary btn-block backToDetail">BACK TO DETAILS</button>
                  </div>*/ ?>
                </div>
                <div class="row flight-details m-t-20 flight_cls" style="display:none;">
                    <div class="col-sm-3">
                      <button type="submit" id="with_flights" name="" class="btn btn-block btn-orange">
                        <div class="flights-icon">
                          <svg width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                              <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->
                              <desc>Created with Sketch.</desc>
                              <defs></defs>
                              <g id="Additional-eRoam-UI-/-UX-Updates" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <g id="Group-22" transform="translate(1.000000, 1.000000)">
                                      <circle id="Oval-4" stroke="#FFFFFF" cx="10" cy="10" r="10"></circle>
                                      <path d="M16,11.4 L16,10.2 L10.9473684,7.2 L10.9473684,3.9 C10.9473684,3.402 10.5242105,3 10,3 C9.47578947,3 9.05263158,3.402 9.05263158,3.9 L9.05263158,7.2 L4,10.2 L4,11.4 L9.05263158,9.9 L9.05263158,13.2 L7.78947368,14.1 L7.78947368,15 L10,14.4 L12.2105263,15 L12.2105263,14.1 L10.9473684,13.2 L10.9473684,9.9 L16,11.4 Z" id="Shape" fill="#FFFFFF" fill-rule="nonzero"></path>
                                  </g>
                              </g>
                          </svg>
                        </div>
                       <span>With Flights</span></button>
                    </div>
                    <div class="col-sm-3">
                      <button type="submit" id="without_flights" name="" class="btn btn-block btn-orange">
                        <div class="flights-icon">                
                          <svg width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                              <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->
                              <desc>Created with Sketch.</desc>
                              <defs></defs>
                              <g id="Additional-eRoam-UI-/-UX-Updates" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                  <g id="Group-23" transform="translate(1.000000, 1.000000)">
                                      <circle id="Oval-4" stroke="#212121" fill-opacity="0.2" fill="#FFFFFF" cx="10" cy="10" r="10"></circle>
                                      <path d="M16,11.4 L16,10.2 L10.9473684,7.2 L10.9473684,3.9 C10.9473684,3.402 10.5242105,3 10,3 C9.47578947,3 9.05263158,3.402 9.05263158,3.9 L9.05263158,7.2 L4,10.2 L4,11.4 L9.05263158,9.9 L9.05263158,13.2 L7.78947368,14.1 L7.78947368,15 L10,14.4 L12.2105263,15 L12.2105263,14.1 L10.9473684,13.2 L10.9473684,9.9 L16,11.4 Z" id="Shape" fill="#212121" fill-rule="nonzero"></path>
                                      <path d="M1.5,3.5 L17.5,16.5" id="Line-3" stroke="#212121" stroke-linecap="square"></path>
                                  </g>
                              </g>
                          </svg>
                        </div>
                        <span>Without Flights</span></button>
                    </div>
                    <div class="col-sm-3 date-control">
                      <div class="panel-form-group input-picker">
                        <label class="label-control">Jump to a Departure Month</label>
                        <div class="input-group datepicker">
                          <input id="start_date5" type="text" placeholder="Select Month" class="form-control" value="">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                      <label class="red-text" id="red-text" style="display:none;">Please select month</label>
                    </div>

                    <div class="col-sm-3">
                      <button type="button" name="" class="btn btn-block btn-primary update_date_search">Update</button>
                    </div>
                  </div>
              </div>

                <!-- Tour Details Start -->
                <div class="tour-detail tour-bottom-content" style="display: none;">
                    <div class="tour-tabs custom-tabs m-t-10" data-example-id="togglable-tabs">
                      <ul class="nav nav-tabs" id="myTabs1" role="tablist">
                        <li role="presentation" class="active"><a href="#overview" id="overview-tab" role="tab" data-toggle="tab" aria-controls="overview" aria-expanded="true">OVERVIEW</a></li>
                      <!--<li role="presentation"><a href="#review" id="review-tab" role="tab" data-toggle="tab" aria-controls="review" aria-expanded="true">REVIEWS (<?php echo e($reviewCount); ?>)</a></li> -->
                        <li role="presentation"><a href="#details" id="details-tab" role="tab" data-toggle="tab" aria-controls="details" aria-expanded="true">DETAILS</a></li>
                        <!--<li role="presentation"><a href="#location1" id="location1-tab" role="tab" data-toggle="tab" aria-controls="location1" aria-expanded="true">LOCATION</a></li>-->
                      </ul>
                    </div>
                        <div class="tourDetailDiv">
                      
                      <div class="tabs-content-container">
                        <div class="tab-content" id="myTabContent1"> 
                          <div class="tab-pane fade in active" role="tabpanel" id="overview" aria-labelledby="overview-tab"> 

                          </div>

                          <div class="tab-pane fade" role="tabpanel" id="review" aria-labelledby="review-tab"> 
                            <div class="m-t-10 detailDesc">
                              <?php echo $reviewsDetails; ?>
                            </div>
                          </div> 

                          <div class="tab-pane fade" role="tabpanel" id="details" aria-labelledby="details-tab"> 
                            <?php echo $data['detail']['xml_PracticalDetail']; ?>
                          </div> 

                          <div class="tab-pane fade" role="tabpanel" id="location1" aria-labelledby="location1-tab"> 
                            <?php 
                              $locations = explode(',', $data['detail']['tripCountries']);
                              if(!empty($locations)){
                                $locationText = '<ul class="tour-list">';
                                foreach ($locations as $key => $value) {
                                  $locationText .= '<li>'.$value.'</li>';
                                }
                                $locationText .= '</ul>';
                                echo $locationText;
                              }
                            ?>
                          </div>
                         </div> 
                      </div>
                    </div>
                </div>
                <!-- Tour Details End -->

                <!-- Itinerary Book Start -->
                <div class="panel-group tour-accordian tour-itinerary" id="accordion1" role="tablist" aria-multiselectable="true">
                  <div class=" m-t-20">
                    <div class="bookingDates tour-bottom-content">
                      <div class="row text-center desktop-block">
                        <div class="col-md-3 col-sm-3 col-xs-6 text-left">
                          <p class="depart-text">DEPARTING <a href="javascript://"><!-- <i class="fa fa-caret-down date_sorting"></i> --></a></p>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-left">
                          <p class="finish-text">FINISHING <a href="javascript://"><!-- <i class="fa fa-caret-down date_sorting"></i> --></a></p>
                        </div>
                        <!--<div class="col-md-2 col-sm-2 col-xs-3 black-box">
                          <p>AVAILABLE</p>
                        </div>-->
                        <div class="col-md-5 col-sm-3 col-xs-7">
                          <p>TOTAL FROM $AUD <a href="#"><!-- <i class="fa fa-caret-down"></i> --></a></p>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-2 text-right"></div>
                      </div>
                      <div class="tourItineraryDiv">
                      <?php 

                          $i=0; $totalCost = 0; $singalCost = 0;
                          $startDate  = '';
                          $finishDate ='';
                          //echo '<pre>'; print_r($data['dates']);  echo '</pre>'; 
                          if(!empty($data['dates'])){
                            foreach ($data['dates'] as $date) {
                              //echo '<pre>'; print_r($date);  echo '</pre>'; 
                              $i++;
                              if($i == 1){ 
                                $totalCost  = $date['total']; 
                                $singalCost = $date['singlePrice']; 
                                $startDate  = date('Y-m-d H:i:s', strtotime($data['dates'][0]['startDate']));
                                $finishDate = date('Y-m-d H:i:s', strtotime($data['dates'][0]['finishDate']));

                              }
                             
                                  
                                $start_date = date('F-Y',strtotime($date['startDate']));
                                
                                $price = number_format($date['total'],2); 
                                $price_single = $date['singlePrice']; 
                                $price_total = $date['total'];
                                $flag_var = 'flightNoCls'; 
                                if(!empty($season)){
                                  if(array_key_exists($date['season_id'],$season)){
                                    $price = number_format($date['total'] + $season[$date['season_id']],2);
                                    $totalCost  = $date['total'] + $season[$date['season_id']]; 
                                    $singalCost = $date['singlePrice'] + $season[$date['season_id']];
                                    $price_single =$date['singlePrice'] + $season[$date['season_id']];
                                    $price_total = $date['total'] + $season[$date['season_id']];  
                                    $flag_var = 'flightYesCls';
                                  }
                                }
                            ?>     
                           <div class="panel panel-default <?php echo e($flag_var); ?> <?php echo $start_date;?>">             
                          <div class="panel-heading <?php echo ($i==1)?'panel-open':''; ?>" role="tab" id="headingPicks<?php echo e($i); ?>" data-toggle="collapse" data-parent="#accordion<?php echo e($i); ?>" data-target="#collapsePicks<?php echo e($i); ?>"  data-sort="<?php echo e($i); ?>" aria-expanded="true" <?php /*href="#collapsePicks{{ $i }}" aria-controls="collapsePicks{{ $i }}"*/ ?> >
                            <div class="panel-title row text-center">
                              <div class="col-md-3 col-sm-3 col-xs-4 text-left">
                                <p class="mobile-block">DEPARTING <a href="#"><i class="fa fa-caret-down"></i></a></p>
                                <span><?php echo e(date('l d F Y',strtotime($date['startDate']))); ?></span>
                              </div>
                              <div class="col-md-3 col-sm-3 col-xs-4 text-left">
                                <p class="mobile-block">FINISHING <a href="#"><i class="fa fa-caret-down"></i></a></p>
                                <span><?php echo e(date('l d F Y',strtotime($date['finishDate']))); ?></span>
                              </div>
                             
                              <input type="hidden" id="startDate<?php echo e($i); ?>" value="<?php echo e(date('Y-m-d H:i:s', strtotime($date['startDate']))); ?>" >
                              <input type="hidden" id="finishDate<?php echo e($i); ?>" value="<?php echo e(date('Y-m-d H:i:s', strtotime($date['finishDate']))); ?>" >

                              <div class="col-md-5 col-sm-3 col-xs-10 person-price">
                                <p class="mobile-block">TOTAL FROM $<?php echo e($default_currency); ?> <a href="#"><i class="fa fa-caret-down"></i></a></p>
                                <span class="withOutFlights flightPriceCls">$<?php echo e($default_currency); ?> <?php echo e(number_format($date['total'],2)); ?> Per Person (Land Only)</span>
                                <span class="withFlights">$<?php echo e($default_currency); ?> <?php echo e($price); ?> Per Person (Inclusive Flights)</span>
                                <span class="<?php echo $start_date;?>-withOutFlights" style="display:none;"><?php echo e(number_format($date['total'],2)); ?> (Land Only)</span>
                                <span class="<?php echo $start_date;?>-withFlights" style="display:none;"><?php echo e($price); ?> (Inclusive Flights)</span>
                                <!-- + $season[$date['season_id']] -->
                              </div> 
                              <div class="col-md-1 col-sm-1 col-xs-2 collapse-icon">
                                <a role="button" id="collapsedBtn<?php echo e($i); ?>" class="collapsedBtn <?php echo ($i!=1)?'collapsed':''; ?>" ><i class="fa fa-minus-circle"></i></a>
                              </div>
                            </div>
                          </div>
                          <div id="collapsePicks<?php echo e($i); ?>" class="panel-collapse collapse <?php /*if(session()->has('DateRadioIdValue')){ echo 'in'; } else { */ echo ($i==1)?'in':''; /*}*/ ?>" role="tabpanel" aria-labelledby="headingPicks<?php echo e($i); ?>">
                            <div class="panel-body panel-body-new">
                              <h4><?php echo e($date['departureName']); ?></h4>
                              <h4><?php echo e($data['detail']['tour_title']); ?></h4>
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="m-t-10">
                                    <div class="tour_detail_icon">
                                      <svg width="20px" height="16px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                          <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
                                          <desc>Created with Sketch.</desc>
                                          <defs></defs>
                                          <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                              <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">
                                                  <g id="Group" transform="translate(501.000000, 542.000000)">
                                                      <g id="Group-16">
                                                          <path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>
                                                      </g>
                                                  </g>
                                              </g>
                                          </g>
                                      </svg>
                                    </div>
                                    <div class="tour_detail_data"><span>Tour Code:</span> <?php echo e($date['departureCode']); ?></div>
                                  </div>

                                <?php 
                                  $allCountry = '';
                                  foreach ($data['countries'] as $country) {
                                    $allCountry .= ucwords(strtolower($country['country_name'])).', ';
                                  }
                                  $allCountry = substr($allCountry, 0,-2);
                                ?> 
                                <div class="tour_detail_data"><span>Countries:</span> <?php echo e($allCountry); ?></div>
                                <div class="tour_detail_data"><span>Duration:</span> <?php echo e($data['detail']['no_of_days']); ?> <?php echo e($total_duration); ?></div>
                                <div class="tour_detail_data"><span>Start:</span> <?php echo e($data['detail']['departure']); ?>, <?php echo e($data['detail']['departure_country']); ?></div>
                                <div class="tour_detail_data"><span>Finish:</span> <?php echo e($data['detail']['destination']); ?>, <?php echo e($data['detail']['destination_country']); ?></div>
                                <?php 
                                  $Flight = ''; 
                                  if(!empty($t_return)){
                                    if(array_key_exists($date['season_id'],$t_return)){
                                      $default_selected_city = session()->get( 'default_selected_city' );
                                      $city_name = 'MEL';
                                      if($default_selected_city == 7){
                                        $city_name = 'SYD';
                                      }elseif($default_selected_city == 15){
                                        $city_name = 'BNE';
                                      }elseif($default_selected_city == 30){
                                        $city_name = 'MEL';
                                      }
                                      $Flight = $city_name;  
                                    }
                                  }
                                  if(!empty($Flight)){
                                    ?>
                                    <div class="tour_detail_data return_flights_from"><span>Flight:</span> Return Flights From (<?php echo e($Flight); ?>)</div>
                                    <?php
                                    $Flight = '';
                                  } 

                                ?>                                  
                                </div>
                                <div class="col-sm-8 text-right">
                                      <div class="price-block">
                                        <div class="price-inner">
                                          <div>$<?php echo e($default_currency); ?> <span class="transport-price innerWithOutFlights"><strong><?php echo e(number_format($date['total'],2)); ?> </strong><small style="font-size: 14px;">(Land Only)</small></span><span class="transport-price innerWithFlights"><strong><?php echo e($price); ?> </strong><small style="font-size: 14px;">(Inclusive Flights)</small></span></div>
                                          Per Person 
                                          <?php 
                                            if($date['supplement'] > 0){
                                              ?>
                                              in a Twin Share Room
                                              <p class="m-t-10"><strong>Want your own single Room?</strong><br > <i>From an extra</i> $<?php echo e($default_currency.' '.number_format($date['supplement'],2)); ?> </p>
                                              <?php
                                            }
                                          ?>
                                          
                                        </div>
                                      </div>
                                </div>
                              </div>
                              
                              <div class="row">
                                <div class="col-sm-8">
                                  <p class="notes m-t-25">All prices are inclusive of all relevant Government taxes &amp; GST and are per person in Australian dollars.</p>
                                </div>
                                <div class="col-sm-4">
                                      <input type="hidden"  name="singlePrice" id="singlePrice<?php echo e($i); ?>" value="<?php echo e($date['singlePrice']); ?>" >
                                      <input type="hidden" class="dateRedio" name="date" id="radio-d<?php echo e($i); ?>" value="<?php echo e($date['total']); ?>" >
                                       <input type="hidden"  name="singlePrice" id="singlePrice<?php echo e($i); ?>two" value="<?php echo e($price_single); ?>" >
                                      <input type="hidden" class="dateRedio" name="date" id="radio-d<?php echo e($i); ?>two" value="<?php echo e($price_total); ?>" >
                                      <button type="button" name="" class="btn btn-primary btn-block bookButton" id="bookButton_<?php echo e($i); ?>" >BOOK TOUR</button>
                                </div>  
                              </div>
                            </div>
                          </div>
                        </div>
                                
                       
                      <?php } } else {  echo '<div><center>No records found.</center></div>'; }  ?>
                      <div id="no_records" style="display:none;"><center>No departure date found.</center></div>
                    </div>
                    </div>

                    <!-- Save Itinerary Start -->
                      <a id="Book"></a>
                      <div class="bookingFrom" style="display:;">
                        <div class="tour-bottom-content">
                          <div class="bookScroll">

                            <?php 
                              if($payError == 1){
                                echo ' <p style="text-align: center; color: red;padding-top: 10px;" id="error-msg">'.Session::get('error').'</p>';
                              }
                            ?>
                        <form class="horizontal-form" data-eway-encrypt-key="kzi7D33f7vCQQuKaWDulraHWAUzmm8As0j/IZw9I+e2vScjLFN5JUtgNWDd332eFFPAF4JN8txghHXjAvOpI3KFs7ELu3M2z+J7aKKO7aiLayBS+VV9WJBZ93mt3geV3Qsgk0qg/JvBuFGlZVQZjBYFQTbmyRoKl3s3wmEGZaLkiUIE4qmj1H7p9RocJd5OH4AASbSxeeo+PN27jkz+6W+MrwSzU8zMtedQD/TY0EPkOjOJYcQJ8k5o5gWrEcCJzEGg9L+y66d2ysqOhKaqfmaaYSOvVBgZMOM//uJv1JK4F3XPwUaXomCKtUNn7sUfVhTOLzjBUpGcepKF1M6nllw==" action="<?php echo e(url('tourPayment')); ?>" method="post" name="tour_form" id="tour_form_id">

                          <?php echo e(csrf_field()); ?>

                          <section>
                            <div>
                              <div class="row">
                                <div class="col-sm-3">
                                  <label for="NoOfPass">No of Passenger</label>
                                  <div class="select-box-new">
                                    <select class="form-control" id="NoOfPass" name="NoOfPass">
                                      <option value="">No of Passenger</option>
                                      <?php
                                          for($i=1; $i<=9; $i++) {
                                            if($i==1){$select = 'selected';} else { $select = '';}
                                            echo '<option value="'.$i.'" '.$select.'>'.$i.'</option>';
                                          }
                                      ?>
                                    </select>
                                  </div>
                                </div>

                                <div class="col-sm-3">
                                    <label for="NoOfSingleRoom">Single Room</label>
                                    <div class="select-box-new select-boxNew">
                                       <select class="form-control" id="NoOfSingleRoom" name="NoOfSingleRoom" <?php if($data['detail']['no_of_days'] == 1 || $data['detail']['durationType'] == 'h'){ ?>disabled<?php } ?>>
                                        <option value="">Single Room</option>
                                        <option value="1" selected>1</option>
                                        <?php
                                          /*for($i=0; $i<=1; $i++) {
                                            if($i==0){$select = '';} else { $select = 'selected';}
                                            echo '<option value="'.$i.'" '.$select.'>'.$i.'</option>';
                                          }*/
                                        ?>
                                      </select>
                                    </div>
                                    <label for="NoOfSingleRoom" generated="true" class="error"></label>
                                    <p class="select-note" id="SinglePricePP">($<?php echo e($default_currency.' '.number_format($singalCost,2)); ?>) Per Person</p>
                                </div>

                                <div class="col-sm-3">
                                    <label for="NoOfTwinRoom">Twin Room</label>
                                    <div class="select-box-new select-boxNew">
                                      <select class="form-control" id="NoOfTwinRoom" name="NoOfTwinRoom" readonly disabled>
                                        <option value="">Twin Room</option>
                                        <option value="0" selected>0</option>
                                      </select>
                                    </div>
                                    <label for="NoOfTwinRoom" generated="true" class="error"></label>
                                    <p class="select-note" id="pricePP">($<?php echo e($default_currency.' '.number_format($totalCost,2)); ?>) Per Person</p>
                                </div>
                               
                                <div class="col-sm-3">
                                  <div class="display-field">
                                    <strong>Total Amount</strong>   <span id="TotalPricePP">$<?php echo e($default_currency.' '.number_format($totalCost,2)); ?></span>
                                  </div>
                                </div>
                              </div>

                            </div>

                            <h4 class="m-t-30">Lead Person Information</h4>
                            <div class="m-t-10">
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="input-field select-box-new">
                                    <select class="form-control passenger_title" name="passenger_title[1]">
                                        <option value="Mr">Mr</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Mrs">Mrs</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="input-field">
                                    <input id="pax-fname" type="text" name="passenger_first_name[1]" class="passenger_first_name">
                                    <label for="pax-fname" class="">First Name</label>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="input-field">
                                    <input id="pax-lname" type="text" name="passenger_last_name[1]" class="passenger_last_name">
                                    <label for="pax-lname" class="">Last Name</label>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="m-t-30 radio-blackgroup">
                                    <label class="radio-checkbox label_radio m-r-10" for="radio-m11"><input type="radio" name="passenger_gender[1]" id="radio-m11" value="male" checked="">Male</label>
                                    <label class="radio-checkbox label_radio" for="radio-m12"><input type="radio" name="passenger_gender[1]" id="radio-m12" value="female">Female</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-3">
                                  <div class="input-field">
                                    <input id="pax-dob" type="text" name="passenger_dob[1]" class="passenger_dob">
                                    <label for="pax-dob" class="" id="dob">Date of Birth</label>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="input-field">
                                    <input id="pax-contact" type="text" name="passenger_contact_no">
                                    <label for="pax-contact">Contact No.</label>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="input-field">
                                    <input id="pax-email" type="text" name="passenger_email">
                                    <label for="pax-email">Email</label>
                                  </div>
                                </div>
                                <div class="col-sm-3">
                                  <div class="input-field select-box-new">
                                    <select class="form-control" name="passenger_country">
                                      <option value="">Select Country</option>
                                      <?php 
                                        $allCountry = array();
                                        $j=0;
                                        foreach($countries as $country){
                                          foreach ($country['countries'] as $country_data){
                                              $allCountry[$j]['name'] = $country_data['name'];
                                              $allCountry[$j]['id'] = $country_data['id'];
                                              $j++;
                                          } 
                                        }
                                        usort($allCountry, 'sort_by_name');
                                      ?>

                                      <?php if( count($allCountry) > 0 ): ?>

                                        <?php foreach($allCountry as $Country): ?>
                                          <option value="<?php echo e($Country['name']); ?>"><?php echo e($Country['name']); ?></option>
                                        <?php endforeach; ?>
                                      <?php endif; ?>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div id="OtherPersons"> </div>
                          </section>

                          <h4 class="m-t-30">Payment Confirmation</h4>
                          <div class="table-responsive m-t-20">
                            <?php
                              $travellers = 1;
                              $total = $totalCost*$travellers;
                              //$gst = $total*0.025;
                              $gst = 0;
                              $finaltotal1 = $total + $gst;
                              $finalTotal = number_format($total + $gst, 2, '.', ',');
                            ?>

                            <table class="table">
                              <tbody>
                                <?php /*<tr>
                                  <td width="80%">Total Per Person</td>
                                  <td>{{ $default_currency }}</td>
                                  <td id="total">{{ number_format($totalCost, 2, '.', ',') }}</td>
                                </tr>*/ ?>
                                <?php if(($data['detail']['no_of_days'] != 1 && $data['detail']['durationType'] == 'd') || ($data['detail']['durationType'] != 'h' && $data['detail']['no_of_days'] != 1)){?>
                                <tr class="single_room_total">
                                  <td width="80%">Single Room Total</td>
                                  <td>$<?php echo e($default_currency); ?></td>
                                  <td id="singaltotal"><?php echo e(number_format(0, 2, '.', ',')); ?></td>
                                </tr><?php } ?>
                            <?php if(($data['detail']['no_of_days'] != 1 && $data['detail']['durationType'] == 'd') || ($data['detail']['durationType'] != 'h' && $data['detail']['no_of_days'] != 1)){?>
                                <tr class="twin_room_total">
                                  <td width="80%">Twin Room Total</td>
                                  <td>$<?php echo e($default_currency); ?></td>
                                  <td id="total"><?php echo e(number_format($totalCost, 2, '.', ',')); ?></td>
                                </tr>
                                <?php } ?>

                               <!-- <tr>
                                  <td>Sub Total Amount</td>
                                  <td><?php echo e($default_currency); ?></td>
                                  <td id="subtotal"><?php echo e(number_format($total, 2, '.', ',')); ?></td>
                                </tr>
                                <tr>
                                  <td>Credit Card Fee's 2.5%</td>
                                  <td><?php echo e($default_currency); ?></td>
                                  <td id="ccfee"><?php echo e(number_format($gst, 2, '.', ',')); ?></td>
                                </tr>-->
                                <tr>
                                  <td><strong>Total Amount Due</strong></td>
                                  <td><strong>$<?php echo e($default_currency); ?></strong></td>
                                  <td id="finaltotal"><strong>$<?php echo e($finalTotal); ?></strong></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="m-t-20">
                            <h4>Credit Card Details</h4>
                            <input type="hidden" name="tour_id" value="<?php echo e($data['detail']['tour_id']); ?>">
                            <input type="hidden" name="Title" value="<?php echo e($data['detail']['tour_title']); ?>">
                            <input type="hidden" name="code" value="<?php echo e($data['detail']['code']); ?>">
                            <input type="hidden" name="provider" value="<?php echo e($data['detail']['provider']); ?>">
                            <input type="hidden" name="departure_date" id="departure_date" value="<?php echo e($startDate); ?>">
                            <input type="hidden" name="return_date" id="return_date" value="<?php echo e($finishDate); ?>">
                            <input type="hidden" name="singleAmount" id="singleAmount" value="0">
                            <input type="hidden" name="twinAmount" id="twinAmount" value="<?php echo e($totalCost); ?>">
                            <input type="hidden" name="creditCardFee" id="creditCardFee" value="<?php echo e($gst); ?>">
                            <input type="hidden" name="totalAmount" id="totalAmount" value="<?php echo e($finaltotal1); ?>">
                            <input type="hidden" name="currency" id="currency" value="$<?php echo e($default_currency); ?>">
                            <input type="hidden" value="<?php if(session()->has('DateRadioIdValue')){ echo session()->get('DateRadioIdValue'); }?>" name="DateRadioIdValue" id="DateRadioIdValue">  

                            <div class="row">
                              <div class="col-sm-6">
                                <div class="input-field">
                                  <input name="first_name" id="first_name" type="text" class="form-control">
                                  <label for="first_name" class="">First Name</label>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="input-field">
                                  <input name="last_name" id="last_name" type="text" class="form-control">
                                  <label for="last_name" class="">Last Name</label>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12">
                                <div class="input-field">
                                  <input name="card_number" id="card_number" data-stripe="number" class="form-control" type="text" data-eway-encrypt-name="EWAY_CARDNUMBER">
                                  <label for="card_number" class="">Credit Card Number</label>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-9">
                                <div class="row">
                                  <div class="col-sm-3">
                                    <label class="expiry-label">Expiration Date:</label>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="input-field select-box-new" style="margin-bottom: 5px;">
                                      <select class="form-control" name="month" id="month" data-stripe="exp_month">
                                        <option value="">MM</option>
                                          <?php $expiry_month = date('m');?>
                                          <?php for($i = 1; $i <= 12; $i++) {
                                          $s = sprintf('%02d', $i);?>
                                            <option value="<?php echo $s;?>" <?php //if ( $expiry_month == $i ) { ?>  <?php //} ?>><?php echo $s;?></option>
                                          <?php } ?>
                                      </select>
                                    </div>
                                    <label for="month" generated="true" class="error"></label>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="input-field select-box-new" style="margin-bottom: 5px;">
                                      <select class="form-control" name="year" id="year" data-stripe="exp_year">
                                        <option value="">YYYY</option>
                                          <?php
                                          $lastyear = date('Y')+21;
                                          $curryear = date('Y');
                                          for($k=$curryear;$k<$lastyear;$k++){ ?>
                                            <option value="<?php echo substr($k, -2);?>"><?php echo $k;?></option>
                                          <?php } ?>
                                      </select>
                                    </div>
                                    <label for="year" generated="true" class="error"></label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="input-field">
                                  <input type="text" maxlength="3" name="cvv" id="cvv" class="form-control" data-stripe="cvc"  data-eway-encrypt-name="EWAY_CARDCVN">
                                  <label for="cvv" class="">CVV</label>
                                  <span class="input-icon">
                                  <a href="#" class="help-icon" data-toggle="modal" data-target="#helpModal"><i class="fa fa-question-circle"></i></a>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-8">
                                <div class="input-field">
                                  <input id="country" type="text" name="country">
                                  <label for="country" class="">Country</label>
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <div class="input-field">
                                  <input id="postalcode" type="text" name="postalcode">
                                  <label for="postalcode" class="">Postal Code</label>
                                </div>
                              </div>
                            </div>
                            <div class="form-group m-t-40">
                              <div class="row">
                                <div class="col-sm-5">
                                  <ul class="payment-type">
                                    <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                                    <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                                    <!-- <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li> -->
                                  </ul>
                                </div>
                                <div class="col-sm-7">
                                  <button type="submit" name="" class="btn btn-block btn-primary">Pay Now</button>
                                </div>
                              </div>
                            </div>
                            <div class="text-center m-t-40">
                              <p>Your credit card will be charged $<?php echo e($default_currency); ?> <span id="chargeAmount"> <?php echo e($finalTotal); ?></span>. By clicking "Pay Now" below, you agree to <a href="#">these terms</a></p>
                            </div>
                          </div>
                        </form>
                      </div>
                      </div>
                      </div>
                    <!-- Save Itinerary End -->
                  </div>
                </div>
                <!-- Itinerary Book End -->

                <!-- View Itinerary Start -->
                <div class="tour-viewItinerary m-t-30">
                    <div class="tour-bottom-content">
                      <div class="tourViewItineraryDiv">
                          <div class="m-t-10 detailDesc">
                              <h3 class="itinarery-title" style="margin-bottom:10px; margin-top: 15px;">Overview</h3>
                              <p><?php echo $data['detail']['long_description']; ?></p>
                             <!--  <div class="m-t-20 row">
                                  <div class="col-sm-3">
                                      <div class="row">
                                          <div class="col-xs-4 col-sm-3 col-md-2"><strong>Start:</strong></div>
                                          <div class="col-xs-8 col-sm-9 col-md-10"><?php echo e($data['detail']['departure']); ?>,<?php echo e($data['detail']['departure_country']); ?> </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-xs-4 col-sm-3 col-md-2"><strong>Finish:</strong></div>
                                          <div class="col-xs-8 col-sm-9 col-md-10"> <?php echo e($data['detail']['destination']); ?>,<?php echo e($data['detail']['destination_country']); ?> </div>
                                      </div>

                                  </div>
                                  <?php
                                  $allCountry1 = '';
                                  foreach ($data['countries'] as $country) {
                                  $allCountry1 .= ucwords(strtolower($country['country_name'])).', ';
                                  }

                                  //echo substr($allCountry1, 0,-2); ?>
                                  <div class="col-sm-6">
                                      <div class="row">
                                          <div class="col-xs-4 col-sm-3 col-md-2"><strong>Countries:</strong></div>
                                          <div class="col-xs-8 col-sm-9 col-md-10"><?php echo substr($allCountry1, 0,-2);?></div>
                                      </div>
                                      <?php /*<div class="row">
                                    <div class="col-xs-4"><strong>Theme:</strong></div>
                                    <div class="col-xs-8">Explorer</div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-4"><strong>Ages:</strong></div>
                                    <div class="col-xs-8">Min 12</div>
                                  </div>*/ ?>
                                      <div class="row">
                                          <div class="col-xs-4 col-sm-3 col-md-2"><strong>Group Size:</strong></div>
                                          <div class="col-xs-8 col-sm-9 col-md-10"><?php echo e('Min '.$data['detail']['groupsize_min'].', Max '.$data['detail']['groupsize_max']); ?></div>
                                      </div>
                                  </div>
                              </div> -->

                          <!--<?php if($data['detail']['supplier_desc']): ?>
                              <div class="m-t-20">
                                <div class="m-t-20">
                                  <?php //echo $data['detail']['supplier_desc']; ?>
                                  </div>
                                </div>
                              <?php endif; ?>-->
                          </div>
                        <h3 class="itinarery-title" style="margin-bottom:10px; margin-top: 40px;">Tour Itinerary</h3>
                        <?php echo $data['detail']['xml_itinerary']; ?>

                        <?php if($data['detail']['accommodation'] != '' && $data['detail']['accommodation'] != 'N/A'): ?>
                       <!--  <div class="m-t-20">
                          <h3 class="itinarery-title">Included</h3>
                            <ul class="tour-list"> -->
                              <?php 
                                // $accommodations = explode(',', $data['detail']['accommodation']);
                                //   foreach ($accommodations as $accommodation) {
                                //     echo  '<li>'.$accommodation.'</li>';
                                //   }  
                              ?>
                           <!--  </ul>
                        </div> -->
                        <?php endif; ?>
                      </div>
                    </div>
                </div>
                <!-- View Itinerary End -->

                <!--<div id="makeAnEnquiry12" class="m-t-30">
                  <div class="tour-bottom-content">
                    <div class="tourmakeAnEnquiry">
                      <form name="enquiryForm" id="enquiryForm" action="tour-enquiry" method="post">
                        <?php echo e(csrf_field()); ?>

                        <div id="mailMessage" style="dispaly:none;"></div>
                        <div class="m-t-30">
                          <div class="input-field">
                            <label for="firstname">First Name</label>
                            <input type="text" class="text-field" id="firstname" name="firstname" >
                          </div>

                          <div class="input-field">
                            <label for="lastname">Last Name</label>
                            <input type="text" class="text-field" id="lastname" name="lastname" >
                          </div>

                          <div class="input-field">
                            <label for="email">Email Address</label>
                            <input type="text" class="text-field" id="email" name="email" >
                          </div>

                          <div class="input-field">
                            <label for="phone">Phone Number</label>
                            <input type="text" class="text-field" id="phone" name="phone" >
                          </div>

                          <div class="input-field">
                            <label for="tourname">Tour Interested In</label>
                            <input type="text" class="text-field" id="tourname" name="tourname" readonly value="<?php echo e($data['detail']['tour_title']); ?>">
                          </div>

                          <div class="input-field">
                            <label for="dates">Interested Travel Dates</label>
                            <input type="text" class="text-field" id="dates" name="dates" >
                          </div>

                          <div>
                            <button type="submit" name="myForm" class="btn btn-primary btn-block">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                </div>-->
          </div>
          </div>
        </div>
      </div>
      </div>
  
    <!-- Modal -->
    <div class="modal fade" id="enquiryModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-container">
              <h4 class="modal-title" id="myModalLabel">Make Enquiry</h4>
            </div>
          </div>
          <hr>
          <div class="modal-body">
            <div class="modal-container">
                <form id="myForm" method="post" class="form-horizontal">
                  <?php echo e(csrf_field()); ?>

                  <div id="success-box" style="dispaly:none;"></div>
                <div class="m-t-30">
                  <div class="input-field">
                    <input type="text" class="text-field" id="product" name="product" value="<?php echo e($data['detail']['tour_title']); ?>" readonly>
                    <label for="product">Product</label>
                  </div>
                  <div class="input-field">
                        <input type="text" class="text-field" id="product1" name="product1" value="<?php echo e($data['detail']['tour_code']); ?>" readonly>
                        <label for="product">Tour Code</label>
                    </div>
                  <div class="input-field">
                    <input type="text" class="text-field" id="first_name" name="first_name" >
                    <label for="first_name">First Name</label>
                  </div>
                  <div class="input-field">
                    <input type="text" class="text-field" id="family_name" name="family_name" >
                    <label for="family_name">Family Name</label>
                  </div>
                  <div class="input-field">
                    <input type="email" class="text-field" id="email" name="email" >
                    <label for="email">Email Address</label>
                  </div>
                  <div class="input-field">
                    <input type="text" class="text-field" id="phone" name="phone" >
                    <label for="phone">Phone Number</label>
                  </div>
                  <div class="input-field">
                    <label for="postcode">Postcode</label>
                    <input type="text" class="text-field" id="postcode" name="postcode" >
                  </div>
                  <div class="input-field">
                    <input type="text" class="text-field" id="travel_date" name="travel_date" >
                    <label for="travel_date">Travel Date</label>
                  </div>
                  <div class="input-field">
                    <textarea name="comments" class="text-field" rows="20" style="height: 150px;"></textarea>
                    <label for="comments">Comments</label>
                  </div>
                  <div class="notification text-center row m-t-25"></div>

                  <div>
                    <button type="submit" name="myForm" class="btn btn-primary btn-block btn_enquiry">Submit</button>
                  </div>
                </div>
                </form>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>
<script type="text/javascript">
      $(window).load(function(){
        $('.loader1').hide(); 
      });
     function flightPriceCls(){
          $('.flightPriceCls').each(function(){
              if($('#with_flights').hasClass('active')){
                $('.withFlights').show();
                $('.withOutFlights').hide();
                $('.innerWithFlights').show();
                $('.innerWithOutFlights').hide();
                $('.return_flights_from').show();
              }else{
                $('.withFlights').hide();
                $('.withOutFlights').show();
                $('.innerWithFlights').hide();
                $('.innerWithOutFlights').show();
                $('.return_flights_from').hide();
              }
          });
      }
    function displayFlightPrice(){
      var global_flag = 0; 
      if($('.panel-default').length < 1){
        $('#no_records').show();
      }else{
        $('#no_records').hide();
      }
      
      if($('.flightYesCls').length > 1){
        $('#with_flights').addClass('active');
        $('#without_flights').removeClass('active');
        $('.withFlights').show();
        $('.withOutFlights').hide();
        $('.innerWithFlights').show();
        $('.innerWithOutFlights').hide();
        $('.return_flights_from').show();
      }else{
        $('#with_flights').removeClass('active');
        $('#without_flights').addClass('active');
        $('.withFlights').hide();
        $('.withOutFlights').show();
        $('.innerWithFlights').hide();
        $('.innerWithOutFlights').show();
        $('.return_flights_from').hide();
      }
      $('.panel-default').hide();
      var n_flag = 0;
      $('.panel-default').each(function(i){
         if($(this).hasClass('flightYesCls')){
            n_flag = 1;
            $(this).show();
            $('#no_records').hide();
            $('#with_flights').addClass('active');
            $('#without_flights').removeClass('active'); 
         }
      });
      if(n_flag == 0){
        $('.panel-default').each(function(){
         if(!$(this).hasClass('flightYesCls')){
            $(this).show();
            $('#no_records').hide();
         }
        });
        $('#with_flights').removeClass('active');
        $('#without_flights').addClass('active');
      }
      flightPriceCls();
    }

    var siteUrl = $('#site-url').val();
    $(document).ready(function(){

      <?php 
        if(!empty($season)){
          ?>
            displayFlightPrice();
          <?php
        }else{
          ?>
          $('#with_flights').hide();
          $('#without_flights').hide();
          $('.withFlights').hide();
          $('.withOutFlights').show();
          $('.innerWithFlights').hide();
          $('.innerWithOutFlights').show();
          $('.return_flights_from').hide();
          <?php
        }
      ?>
      
      $('#with_flights').click(function(){
          $('#no_records').show();
          $('.return_flights_from').show();
          $(this).addClass('active');
          $('#without_flights').removeClass('active');
          var select_date = $('#start_date5').val();
          if(select_date){
            select_date = $('#start_date5').val().replace(' ','-');
            $('.panel-default').hide();
            $('.'+select_date+'').each(function(){
               if($(this).hasClass('flightYesCls')){
                  $(this).closest('.flightYesCls').show();
                  $('#no_records').hide();
                  flightPriceCls(); 
               }
            });
          }else{
            $('.panel-default').hide();
            $('.panel-default').each(function(){
               if($(this).hasClass('flightYesCls')){
                  $(this).show();
                  $('#no_records').hide();
                  flightPriceCls(); 
               }
            });
          }
      });
      $('#without_flights').click(function(){
          $('.return_flights_from').hide();
          $(this).addClass('active');
          $('#with_flights').removeClass('active');
          var select_date = $('#start_date5').val();
          if(select_date){
            select_date = $('#start_date5').val().replace(' ','-');
            $('.panel-default').hide();
            $('#no_records').show();
            $('.'+select_date+'').each(function(){
                    $('.'+select_date+'').show();
                    $('#no_records').hide();
            });
            flightPriceCls();
          }else{
            $('.panel-default').hide();
            $('#no_records').show();
            $('.panel-default').each(function(){
              $(this).show();
              $('#no_records').hide();
            });
            flightPriceCls(); 
          } 
      });

      $('.update_date_search').click(function(){
          var select_date = $('#start_date5').val().replace(' ','-');
          if(!select_date){
             $('#red-text').show();
             return true; 
          }else{
            $('#red-text').hide(); 
          }
          $('.panel-default').hide();
          $('#no_records').show();
          
          <?php 
            if(!empty($season)){
              ?>
                  if($('#with_flights').hasClass('active')){
                    $('.'+select_date+'').each(function(index, element){
                        if($(this).hasClass('flightYesCls')){
                          $(this).closest('.flightYesCls').show();
                           $('#no_records').hide();
                        }
                    });
                     
                  }else{
                    $('.'+select_date+'').each(function(index, element){
                          $('.'+select_date+'').show();
                          $('#no_records').hide();
                    });
                  }
                  flightPriceCls();
              <?php
            }else{
              ?>
               $('.'+select_date+'').each(function(index, element){
                  $('.'+select_date+'').show();
                  $('#no_records').hide();
              });
              flightPriceCls();
              <?php
            }
      ?>
      });
      
      $("#travel_date").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true
      });

      $("#myForm").validate({
          rules: {
              email: {
              required: true,
              email:true
            },
            first_name: {
              required: true
            },
            family_name: {
                required: true
            },
            phone: {
                number: true
            },
            postcode: {
                required: true
            }
          },
          errorPlacement: function (label, element) {
            label.insertAfter(element);
          },
          submitHandler: function (form) {
                enquiry();
          }
      });

      function enquiry() {

            var data = $('#myForm').serializeArray();
              
                $.ajax({
                    url: siteUrl +'/enquiry-form',
                    method: 'POST',
                    data: data,
                    beforeSend: function () {
                        $('.btn_enquiry').attr('disabled',true);
                        $('.notification').css({color: 'rgb(0, 0, 0)'}).html('Sending request...').show();
                    },
                    success: function (response) {
                        if (response.trim() == 1) {
                            $("#success-box").show().html('<p class="success-box">Enquiry sent successfully.</p>');
                            $('.btn_enquiry').attr('disabled',false);
                            $('.notification').hide();
                        }else{
                            $('#success-box').css({color: 'red'}).html('<p class="success-box">Enquiry not sent please try again.</p>').show();
                            $('.btn_enquiry').attr('disabled',false);
                            $('.notification').hide();
                        }
                    },
                    error: function () {
                    }
                });
      }

      $(".tour-viewItinerary, .bookingDates").show();
      $('.twin_room_total').hide();
      // $('.page-content').css("margin-left","0px");
      // $('.page-content').css("tabs-main-container","0px");
      // $('.page-sidebar').hide();
      // $('.left-strip').hide();

      $(".collapsedBtn").click(function(evt){
          //alert($(this).attr("id"));
          //alert($(this).attr("class"));

          if($(this).hasClass('collapsed')){//alert(1);
          
            $(".panel-heading").attr("aria-expanded",false);
            $(".collapse").removeClass('in');
            $(".collapsedBtn").addClass('collapsed');
            $('#'+$(this).attr("id")).removeClass('collapsed');
          }else{
            $('#'+$(this).attr("id")).addClass('collapsed');
          }

          //$(".panel-heading").attr("aria-expanded",false);
          //$(".collapse").removeClass('in');
         // $(".collapsedBtn").addClass('collapsed');
         // $('#'+$(this).attr("id")).removeClass('collapsed');
      });
     
      var is_DateRadioIdValue =  "<?php echo e($is_DateRadioIdValue); ?>";
      if(is_DateRadioIdValue > 0){
          $(".panel-heading").attr("aria-expanded",false);
          $("#headingPicks"+is_DateRadioIdValue).attr("aria-expanded",true);

          $(".panel-heading").removeClass("panel-open");
          $("#headingPicks"+is_DateRadioIdValue).addClass("panel-open");

          $(".collapsedBtn").addClass('collapsed');
          $("#collapsedBtn"+is_DateRadioIdValue).removeClass('collapsed');

          $(".collapse").removeClass('in');
          $("#collapsePicks"+is_DateRadioIdValue).addClass('in');

         var single_amount = $("#singlePrice"+is_DateRadioIdValue).val();
         var amount = $("#radio-d"+is_DateRadioIdValue).val();
         $('#SinglePricePP').text("$<?php echo e($default_currency); ?> " + parseFloat(single_amount).toFixed(2)+" (Per Person)");
         $('#pricePP').text("$<?php echo e($default_currency); ?> " + parseFloat(amount).toFixed(2)+" (Per Person)");
         $('#TotalPricePP').text("$<?php echo e($default_currency); ?> " + parseFloat(amount).toFixed(2));
      }

      var payError = "<?php echo e($payError); ?>";
      if(payError == 1){
        $(".tour-detail, .tour-viewItinerary, .bookingDates").hide();
        $(".tour-itinerary, .bookingFrom").show();
        ChangePriceValues();
      }

      var tourDate = "<?php echo e($tourDate); ?>";
      var tourSession = "<?php echo e($tourSession); ?>";
      if(tourDate  == 1 && tourSession == 1){
          $('.flight_cls').show();
          $(".tour-detail, .tour-viewItinerary").hide();
          $(".tour-itinerary, .bookingDates").show();
          //$(".bookingDates").hide();
          $(".saveItinerary").parent().hide();
          $(".backToDetail").parent().show();
          $(".bookingFrom").hide();
          $("#makeAnEnquiry").hide();
          $(this).addClass('active');
          $('.viewItinerary, .makeAnEnquiry1').removeClass('active');
          $('.bookItinerary').addClass('active');
      }
      document.onload = function() {
        inactivityTime();
      };

      document.onmousemove = function() {
          inactivityTime();
      };
      document.onmousedown = function() {
          inactivityTime();
      };
      document.onkeypress = function() {
          inactivityTime();
      };
      document.ontouchstart = function() {
          inactivityTime();
      };

      var inactivityTime = function() {
        if( '<?php echo e(isset($page_will_expire) ? 1 : 0); ?>' == 1 )
        {
          var t;
          var timer
          document.onload = resetTimer;
          window.onload = resetTimer;
          document.onmousemove = resetTimer;
          document.onkeypress = resetTimer;

          function showInactiveNotification() {
            if(! ($("#session-expire").data('bs.modal') || {isShown: false}).isShown ){
              var counter = 60;
              
              //$('.session-home-btn').html('Home('+counter+')');
              $('#session-expire').modal('show');

              var  data = { tourSession: 'Yes'};
              eroam.ajax('post', 'session/set_tourSession', data, function(response){ });

              timer = setInterval(function(){
                //$('.session-home-btn').html('Home('+counter+')');
                // if( counter == 0 ){
                //   //$('.session-home-btn').html('Home');
                //   $('.session-home-btn').prop('disabled', true);
                //   $('.session-continue-btn').prop('disabled', true);
                //   clearInterval(timer);
                //   window.location = '<?php echo e(url("")); ?>';
                // }
                counter--;
              }, 1000);
            }
          }

          function resetTimer() {
            clearTimeout(t);
            clearInterval(timer);
            t = setTimeout(showInactiveNotification, 600000);
          }

          $('.session-home-btn').click(function(){
            location.reload();
          });

          $('.session-continue-btn').click(function(){
            clearTimeout(t);
            clearInterval(timer);
            $('#session-expire').modal('hide');
            
          });
          $('.session-continue-btn').click(function(){
            window.location = '<?php echo e(url("/")); ?>';
            //clearTimeout(t);
            // clearInterval(timer);
            // $('#session-expire').modal('hide');
          });
        }
      };

        var jcarousel = $('.jcarousel'); 
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                width = carousel.innerWidth();

                if (width >= 1280) {
                    width = width / 5;
                } else if (width >= 992) {
                    width = width / 4;
                }
                else if (width >= 768) {
                    width = width / 4
                }
                else if (width >= 640) {
                    width = width / 4;
                } else {
                    width = width / 3;
                }
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });
    
        //tour-detail
        $('body').on('click', '.bookItinerary', function() {
          $('.flight_cls').show();
          $(".tour-detail, .tour-viewItinerary").hide();
          $(".tour-itinerary, .bookingDates").show();
          //$(".bookingDates").hide();
          $(".saveItinerary").parent().hide();
          $(".backToDetail").parent().show();
            $(".bookingFrom").hide();
            $("#makeAnEnquiry").hide();
          $(this).addClass('active');
          $('.viewItinerary, .makeAnEnquiry1').removeClass('active');
            //calculateHeight();

            var  data = { tourDate: 'Yes'};
            eroam.ajax('post', 'session/set_tourDate', data, function(response){ });
        });

        $('body').on('click', '.viewItinerary', function() {
          $('.flight_cls').hide();
          $(".tour-viewItinerary, .bookingDates").show();
          $(".tour-itinerary, .tour-detail").hide();
            $(".bookingFrom").hide();
          $(".saveItinerary").parent().hide();
          $(".backToDetail").parent().show();
          $("#makeAnEnquiry").hide();

          $(this).addClass('active');
          $('.bookItinerary, .makeAnEnquiry1').removeClass('active');
            //calculateHeight();

          var  data = { tourDate: 'No'};
          eroam.ajax('post', 'session/set_tourDate', data, function(response){ });
        });

        $('body').on('click', '.backToDetail', function() {
          $(".tour-detail, .bookingDates").show();
          $(".tour-itinerary, .tour-viewItinerary").hide();

          $(".saveItinerary").parent().show();
          $(".backToDetail").parent().hide();
          $(".bookingFrom").hide();
          $('.bookItinerary, .viewItinerary').removeClass('active');

          $("#makeAnEnquiry").hide();

            //calculateHeight();
        });
        // $("#start_date5").datepicker({
        //   format: 'mm yyyy',
        //   autoclose: true,
        //   todayHighlight: true
        // });
        $("#start_date5").datepicker({
               format: "MM yyyy",
               autoclose: true,
               viewMode: "months", 
               minViewMode: "months"
          });
        $('body').on('click', '.makeAnEnquiry1', function() {
            $('.flight_cls').hide();
            $(".tour-viewItinerary, .bookingDates").show();
            $(".tour-itinerary, .tour-detail").hide();
              $(".bookingFrom").hide();
            $(".saveItinerary").parent().hide();
            $(".backToDetail").parent().show();
            $("#makeAnEnquiry").hide();

            $(this).addClass('active');
            $('.viewItinerary, .bookItinerary').removeClass('active');

            var  data = { tourDate: 'No'};
            eroam.ajax('post', 'session/set_tourDate', data, function(response){ });
        });  

        $('#enquiryModal').on('hidden.bs.modal', function () {
            $('.viewItinerary').addClass('active');
            $('.bookItinerary, .makeAnEnquiry1').removeClass('active');
        })

        $('body').on('click', '.makeAnEnquiry', function() {
          $('.flight_cls').hide();
          $("#makeAnEnquiry").show();
          $(".tour-detail, .bookingDates").hide();
          $(".tour-itinerary, .tour-viewItinerary").hide();

          $(".saveItinerary").parent().show();
          $(".backToDetail").parent().hide();
          $(".bookingFrom").hide();
          $('.bookItinerary, .viewItinerary').removeClass('active');
            //calculateHeight();
        });
        

        $('body').on('click', '.dateRadio', function() {
            ChangePriceValues(); 
        });

        $('body').on('click', '.bookButton', function() {
          $('.flight_cls').hide();
            //alert($('#accordion1').position().top);
            //$('html, body').animate({scrollTop:$('#accordion1').position().top}, 'slow');
            $(".bookingDates").hide();
            $(".bookingFrom").show();

            var id = $(this).attr('id');
            id = id.split('_');
            $("#DateRadioIdValue").val(id[1]);

            id1 = id[1];
            if($('#with_flights').hasClass('active')){
              var twinprice = $("#radio-d"+id1+"two").val();
              var singalprice = $("#singlePrice"+id1+"two").val();
            }else{
              var twinprice = $("#radio-d"+id1).val();
              var singalprice = $("#singlePrice"+id1).val();
            }
            $("#radio-d"+id1).val(twinprice)
            $("#singlePrice"+id1).val(singalprice);
          
            $('#total').text(parseFloat(twinprice).toFixed(2));  
            $('#total').text(parseFloat(twinprice).toFixed(2));  
            $('#finaltotal').text(parseFloat(twinprice).toFixed(2));  
            $('#pricePP').text("$<?php echo e($default_currency); ?> " + parseFloat(twinprice).toFixed(2)+" (Per Person)");
            $('#SinglePricePP').text("$<?php echo e($default_currency); ?> " + parseFloat(singalprice).toFixed(2)+" (Per Person)");

            ChangePriceValues();
        });

        $('body').on('click', '.panel-heading', function() {

          var currentId = $(this).attr('data-sort');
          $(".panel-heading").removeClass("panel-open");
          $("#headingPicks"+currentId).addClass("panel-open");

          $(".panel-collapse").removeClass(' in ');

          /*if($("#headingPicks"+currentId).hasClass('panel-open')){
            //$("#headingPicks"+currentId).removeClass('panel-open');
            //$("#headingPicks"+currentId).attr("aria-expanded",false);
            $("#collapsePicks"+currentId).find('.panel-collapse').removeClass(' in ');
            $("#headingPicks"+currentId).removeClass(' panel-open ');
          } else {
            //$("#headingPicks"+currentId).addClass('panel-open');
            //$("#headingPicks"+currentId).attr("aria-expanded",true);
            $("#collapsePicks"+currentId).find('.panel-collapse').addClass(' in ');
            $("#headingPicks"+currentId).addClass(' panel-open ');
          }*/
          
          //$("#collapsePicks"+currentId).find('.panel-collapse').addClass(' in ');
          /*if($("#collapsePicks"+currentId).find('.panel-collapse').hasClass('in')){
            $("#collapsePicks"+currentId).find('.panel-collapse').removeClass(' in ');
          } else {
            $("#collapsePicks"+currentId).find('.panel-collapse').addClass(' in ');
          }*/

          $(".collapsedBtn").addClass(' collapsed ');
          //$("#collapsedBtn"+currentId).removeClass(' collapsed ');
          if($("#collapsedBtn"+currentId).hasClass('collapsed')){
            $("#collapsedBtn"+currentId).removeClass('collapsed');
          } else {
            $("#collapsedBtn"+currentId).addClass('collapsed');
          }

          //$("#collapsedBtn"+$(this).attr('data-sort')).addClass('collapsed');
          //$("#collapsedBtn"+$(this).attr('data-sort')).removeClass('collapsed');

          
          //var id = $(this).attr('id');
          //id = id.replace('headingPicks', '');

          //$("input[name='date']:checked").each(function(){
            //$(this).attr('checked', false);
            //$(this).parent().removeClass('r_on');
          //});

         // $("#radio-d"+id).attr('checked', true);
          //$("#radio-d"+id).parent().addClass('r_on');
          //ChangePriceValues();
        });

        /***** From Validation Start *****/
          $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z," "]+$/i.test(value);
          }, "Letters only please."); 

          $.validator.addMethod("numbersonly", function(value, element) {
            return this.optional(element) || /^[0-9," "]+$/i.test(value);
          }, "Numbers only please."); 

          $.validator.addMethod('ge', function(value, element, param) {

            //var total = parseInt($("#NoOfSingleRoom").val()) + parseInt($("#NoOfTwinRoom").val()) 
             var total = parseInt($("#NoOfSingleRoom").val());  
              return this.optional(element) || parseInt(total) <= parseInt($(param).val());
          }, 'Select less than or equal to value of No of Passenger.');


          $(".passenger_dob").datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
          });

          $('#tour_form_id').on('submit', function(event) {
              // adding rules for inputs with class 'comment'
              $('.passenger_first_name').each(function () {
                  $(this).rules('add', {
                      required: true,
                      lettersonly:true
                  });
              });   

              $('.passenger_last_name').each(function () {
                  $(this).rules('add', {
                      required: true,
                      lettersonly:true
                  });
              });

              $('.passenger_dob').each(function () {
                  $(this).rules('add', {
                      required: true
                  });
              });        

            // event.preventDefault();

              // test if form is valid 
              if($('#tour_form_id').validate().form()) {
                  console.log("validates");
              } else {
                   // prevent default submit action         
                  event.preventDefault();
                  console.log("does not validate");
              }
          })

          $("#tour_form_id").validate({
            ignore: [],
            rules: {
                NoOfSingleRoom :{  
                  required: true ,
                  ge: '#NoOfPass'
                },
                NoOfTwinRoom :{  
                  required: true ,
                  ge: '#NoOfPass'
                },
                passenger_contact_no:{
                    required: true,
                    numbersonly:true,
                    maxlength: 15,
                },
                passenger_email: {
                    required: true,
                    email:true
                },
                passenger_country:{
                    required: true
                },

                first_name: {
                    required: true,
                    lettersonly:true
                },
                last_name: {
                    required: true,
                    lettersonly:true
                },
                card_number: {
                    required: true,
                    number:true
                },
                year: {
                    required: true
                },
                cvv: {
                    required: true,
                    maxlength: 3,
                    number:true
                },
                month: {
                    required: true
                },
                country: {
                  required: true
                },
                postalcode: {
                  required: true
                }
            },
            errorPlacement: function (label, element) {
              label.insertAfter(element);
            }
          });

          $("#enquiryForm").validate({
            rules: {
                firstname: {
                  required: true
                },
                lastname: {
                  required: true
                },
                email: {
                  required: true,
                  email:true
                },
                phone: {
                  required: true
                },
                tourname: {
                  required: true
                },
                dates: {
                    required: true
                }
            },
            messages: {
              email: {
                required: "Please enter Email Address."
              }
            },
            submitHandler: function (form) {
              //form.submit();
              var site_url = "<?php echo e(url('/')); ?>/";
              $.ajax({
                  type:'POST',
                  url:site_url+'sendTourEnquiry',
                  data:{
                      firstname :$("#firstname").val(), 
                      lastname  :$("#lastname").val(),
                      email     :$("#email").val(),
                      phone     :$("#phone").val(), 
                      tourname  :$("#tourname").val(),
                      dates     : $("#dates").val()
                  },
                  success:function (response) { 
                      $("#firstname").val('');
                      $("#lastname").val('');
                      $("#email").val('');
                      $("#phone").val('');
                      $("#tourname").val('');
                      $("#dates").val('');
                      $("#mailMessage").show().html('<p class="success-box">Enquiry sent successfully.</div>');
                      return false;
                  }
              });
            }
          });
        /***** From Validation End *****/
        // set html for single and twin room
        
        $('#NoOfPass').on('change', function () {

          var num = $(this).val();
          var return_val = num % 2;
          <?php if(($data['detail']['no_of_days'] != 1 && $data['detail']['durationType'] == 'd') || ($data['detail']['durationType'] != 'h' && $data['detail']['no_of_days'] != 1)){?>
          if(return_val == 0){
              var t_html ='<option value="">Twin Room</option>';
              var new_num = num / 2;
              t_html +='<option value="'+new_num+'" selected>'+new_num+'</option>';
              var s_html ='<option value="" disabled>Single Room</option>'; 
              s_html +='<option value="0" selected>0</option>';
              for (var i = 2; i <= num; i++) {
                  s_html +='<option value="'+i+'">'+i+'</option>';
                  if(i!=num){
                    i = i+1; 
                  }
              }
            }else{
              if(num > 1){
                var new_num = num - 1;
                var new_num = new_num / 2;
                var t_html ='<option value="">Twin Room</option>'; 
                t_html +='<option value="'+new_num+'" selected>'+new_num+'</option>';

                for (var i = 1; i <= num; i++) {
                  s_html +='<option value="'+i+'">'+i+'</option>';
                  if(i!=num){
                    i = i+1; 
                  }
                }
              }else{
                var s_html ='<option value="" disabled>Single Room</option>'; 
                s_html +='<option value="1" selected>1</option>';
                var t_html ='<option value="">Twin Room</option>'; 
                t_html +='<option value="0" selected>0</option>';
              }

            }
           
            
            $('#NoOfSingleRoom').html(s_html);
            $('#NoOfTwinRoom').html(t_html);
<?php } ?>
            var singal = $('#NoOfSingleRoom option:selected').val();
            var twin = $('#NoOfTwinRoom option:selected').val();
            
            AddPersons(num);
            ChangePriceValues();
             if(singal == 0){
              $('.single_room_total').hide();
             }else{
               $('.single_room_total').show();
             }

             if(twin == 0){
              $('.twin_room_total').hide();
             }else{
              $('.twin_room_total').show();
             }
        });

        $('#NoOfSingleRoom').on('change', function () {
          var num = $('#NoOfPass option:selected').val();
          var new_num = $(this).val();
          var return_val = new_num % 2;
          var s_html ='<option value="" disabled>Single Room</option>';
          var t_html ='<option value="">Twin Room</option>';
          if(return_val == 0){

              var s_html ='<option value="" disabled>Single Room</option>'; 
              s_html +='<option value="0" selected>0</option>';
              for (var i = 2; i <= num; i++) {
                if(i == new_num){
                  s_html +='<option value="'+i+'" selected>'+i+'</option>'; 
                }else{
                  s_html +='<option value="'+i+'">'+i+'</option>'; 
                }
                if(i!=num){
                  i = i+1; 
                }
              }
          }else{
              for (var i = 1; i <= num; i++) {
                if(i == new_num){
                  s_html +='<option value="'+i+'" selected>'+i+'</option>'; 
                }else{
                  s_html +='<option value="'+i+'">'+i+'</option>'; 
                }
                if(i!=num){
                  i = i+1; 
                }
              }
          }
          
          
          new_num = num - new_num;
          if(new_num > 1){
            new_num = new_num / 2;
            t_html +='<option value="'+new_num+'" selected>'+new_num+'</option>';
          }else{
            var t_html ='<option value="">Twin Room</option>'; 
            t_html +='<option value="0" selected>0</option>';
          }
            
          $('#NoOfSingleRoom').html(s_html);
          $('#NoOfTwinRoom').html(t_html);

          var singal = $('#NoOfSingleRoom option:selected').val();
          var twin = $('#NoOfTwinRoom option:selected').val();
            
          AddPersons(num);
          ChangePriceValues();
           if(singal == 0){
            $('.single_room_total').hide();
           }else{
             $('.single_room_total').show();
           }

           if(twin == 0){
            $('.twin_room_total').hide();
           }else{
            $('.twin_room_total').show();
           }
          
        });

        $('#NoOfTwinRoom').on('change', function () {

            var numOfPass = $('#NoOfPass option:selected').val();
            var singal = $('#NoOfSingleRoom option:selected').val();
            var twin = $(this).val();
            if(parseInt(twin) <= parseInt(numOfPass)){
              //if(parseInt(twin + singal)  <= parseInt(numOfPass)){
              var diff = parseInt(numOfPass) - parseInt(twin);
              $('#NoOfSingleRoom').val(diff);
              ChangePriceValues();
            } 
        });
    });

    function ChangePriceValues(){
          var amount            = $("#radio-d"+$("#DateRadioIdValue").val()+"").val();
          var NoOfPass          = $('#NoOfPass option:selected').val();
          var NoOfSingleRoom    = $('#NoOfSingleRoom option:selected').val();
          var NoOfTwinRoom      = $('#NoOfTwinRoom option:selected').val();
          var id                = $("#DateRadioIdValue").val();
          var startDate         = $('#startDate'+id).val();
          var finishDate        = $('#finishDate'+id).val();
          var singlePrice       = $('#singlePrice'+id).val();

          // if(NoOfSingleRoom != '' && NoOfTwinRoom!= ''){
          //     var singalTotal   = singlePrice * NoOfSingleRoom;
          //     var twinTotal     = amount * NoOfPass;
          //     var subtotal      = parseFloat(singalTotal) + parseFloat(twinTotal);
          //     var amount        = twinTotal;
          //     var singaltotal   = singalTotal;
          // } else {
          //     var subtotal      = amount * NoOfPass;
          //     var singaltotal   = 0;
          // }
          var singalTotal   = NoOfSingleRoom * singlePrice;
          var twinTotal     = amount * NoOfTwinRoom;
          twinTotal     = twinTotal * 2;
          var subtotal      = parseFloat(singalTotal) + parseFloat(twinTotal);
          var amount        = twinTotal;
          var singaltotal   = singalTotal;

          var ccfee             = 0;
          var finaltotal        = subtotal + ccfee;
          //console.log(parseFloat(amount).toFixed(2)+'//'+NoOfPass+'//'+parseFloat(subtotal).toFixed(2)+'//'+parseFloat(ccfee).toFixed(2)+'//'+parseFloat(finaltotal).toFixed(2));

          <?php if(($data['detail']['no_of_days'] != 1 && $data['detail']['durationType'] == 'd') || ($data['detail']['durationType'] != 'h' && $data['detail']['no_of_days'] != 1)){?>

          $('#total').text(parseFloat(amount).toFixed(2));
          $('#singaltotal').text(parseFloat(singaltotal).toFixed(2));
          $('#subtotal').text(parseFloat(subtotal).toFixed(2));
          $('#creditCardFee').val(parseFloat(ccfee).toFixed(2));
          $('#ccfee').text(parseFloat(ccfee).toFixed(2));
          $('#finaltotal, #chargeAmount').text(parseFloat(finaltotal).toFixed(2));

          $('#totalAmount').val(parseFloat(finaltotal).toFixed(2));
          $('#singleAmount').val(parseFloat(singaltotal).toFixed(2));
          $('#twinAmount').val(parseFloat(amount).toFixed(2));

          $('#pricePP1').text("$<?php echo e($default_currency); ?> " + parseFloat(amount).toFixed(2));
          $('#TotalPricePP').text("$<?php echo e($default_currency); ?> " + parseFloat(subtotal).toFixed(2));

          $('#departure_date').val(startDate);
          $('#return_date').val(finishDate);
          <?php }else{ ?>
            var finaltotal   = NoOfPass * singlePrice;
            $('#totalAmount').val(parseFloat(finaltotal).toFixed(2));
            $('#finaltotal, #chargeAmount').text(parseFloat(finaltotal).toFixed(2));
            $('#TotalPricePP').text("$<?php echo e($default_currency); ?> " + parseFloat(finaltotal).toFixed(2));
            <?php } ?>
    }

    function AddPersons(num) {
      if (num == 1) {
          $('#OtherPersons').html("");
          return;
      } 

      $('#OtherPersons').html("");
      var data = ""; var key = '';
      for (var i = 1; i < num; i++) {

        key = i + 1;
          var html2 = '<div class="m-t-20">'+
                          '<h5>Person ' + key + '</h5>'+
                          '<div class="row">'+
                            '<div class="col-sm-1">'+
                              '<div class="input-field select-box-new passenger_title" name="passenger_title[' + key + ']">'+
                                '<select class="form-control">'+
                                    '<option value="Mr">Mr</option>'+
                                    '<option value="Ms">Ms</option>'+
                                    '<option value="Mrs">Mrs</option>'+
                                '</select>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-sm-11">'+
                              '<div class="row">'+
                                '<div class="col-sm-3">'+
                                  '<div class="input-field">'+
                                    '<input id="pax-fname' + i + '" type="text" name="passenger_first_name[' + key + ']" class="passenger_first_name">'+
                                    '<label for="pax-fname' + i + '" class="">First Name</label>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-sm-3">'+
                                  '<div class="input-field">'+
                                    '<input id="pax-lname' + i + '" type="text" name="passenger_last_name[' + key + ']" class="passenger_last_name">'+
                                    '<label for="pax-lname' + i + '" class="">Last Name</label>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-sm-2">'+
                                  '<div class="m-t-30 radio-blackgroup">'+
                                    '<label class="radio-checkbox label_radio m-r-10 r_on" for="radio-m' + key + '1"><input type="radio" name="passenger_gender[' + key + ']" id="radio-m' + key + '1" value="male" checked="checked">Male</label>'+
                                    '<label class="radio-checkbox label_radio" for="radio-m' + key + '2"><input type="radio" name="passenger_gender[' + key + ']" id="radio-m' + key + '2" value="female">Female</label>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-sm-4">'+
                                  '<div class="input-field">'+
                                    '<input id="pax-dob' + i + '" type="text" name="passenger_dob[' + key + ']" class="passenger_dob">'+
                                    '<label for="pax-dob' + i + '" class="" id="dob' + i + '">Date of Birth</label>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                      '</div>';
          data += html2;
      }
      $('#OtherPersons').html(data);

      $(".passenger_dob").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
      });

      $('.label_check, .label_radio').click(function(){
              setupLabel();
          });
  }
</script>
 <!--<script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js"></script> -->
<?php
       session()->forget('error');
       session()->forget('DateRadioIdValue');
       session()->forget('tourDate');
       session()->forget('tourSession');
?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.tour', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>