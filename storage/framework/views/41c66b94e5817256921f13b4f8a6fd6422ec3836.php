 
<?php $__env->startSection( 'custom-css' ); ?>
<style type="text/css">
	.hotels-content{
		padding-top: 10px;
	}
	.hotels-content select{
		width: 100%;
		padding: 11px;
	}
	.no-padding-border-margin{
		padding: 0px;
		border: none;
		margin: 0px;
	}
	/*.hotel-list{
		padding: 10px;
		border: 1px solid #ccc;
		margin-bottom: 10px
	}*/
	.hotel-list button{
		padding: 11px;
	}
	.blue-button{
		background: #2AA9DF;
		padding: 11px;
		width: 100%;
		color: #ffffff;
		border-radius: 2px;
	}
	.hotel-header{
		margin-bottom: 10px;
	}
	.hotel-price > small{
		font-size: 60%;
	}
	/*.selected-hotel{
		background: rgba(39, 168, 223, 0.44);
		border: 1px solid #2AA9DF;
	}*/
	.selected-hotel .hotel-details{background-color: #F1F1F1;}
	.modal-content{
		border-radius: 3px;
	}
	.blue-button:hover, .blue-button:focus{
		color: #ffffff;
	}
	/*.icon-style{
		border: 2px solid #918F8F;
		border-radius: 4px;
		padding: 8px 0;
		text-align: center;
		width: 38px;
		font-size: 21px;
		top: -4px;
	}*/
	.white{
		color: #fff;
	}
	.black-background{
		background: #000000;
	}
	.btn > .caret{
		position: absolute;
		top: 50%;
		right: 9px;
	}

	.view-more{
		margin-top: 10px;
	}

	#number_of_nights, #sort-ad, #search-toggler{
		border-radius: 2px;
		border: none; /*1px solid rgb(169, 169, 169);*/
		position: relative;
		text-align: left;
		width: 100%;
		/*padding: 11px;*/
		padding: 13px 11px 13px 11px;
		box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
	}

	#number_of_nights:hover{background-color:  #fff;}
	/*.popover{
		border-radius: 15px;
		border: 1px solid #999;
		background-color: #fff;
		/* background-color: rgba(255, 255, 255, 0.44); *//*
	}

	.popover-title{
		padding: 4px 6px;
		border-radius: 14px 14px 0 0;
		border-bottom: 1px solid #999;
		background-color: #fff;
		/* background-color: rgba(255, 255, 255, 0.7); *//*
	}*/
	.popover-title {
		margin: 0px;
		padding-left: 16px;
		padding-top: 6px;
		padding-bottom: 6px;
	}
	.popover-title > strong{
		padding-left: 20px;
	}
	.popover.bottom {
		top: 44px;
		margin-top: 5px;
		left: -58.25px;
		width: 255px;

	}
	.popover.bottom ul{
		list-style: none;
		padding-left: 0px;
	}
	 /*#number_of_nights_list.popover.bottom ul{
		margin-bottom: 0px;
		height: 292px;
		overflow: scroll;
		width: 100%;
	}*/
	.popover.bottom ul > li {
		margin-bottom: 5px;
		padding-left: 10px;
	}
	.popover.bottom ul > li > span{
		padding-left: 15px;
	}
	.show{
		display: block;
	}
	.popover-content {
		padding: 6px;
		/*  background-color: rgba(255, 255, 255, 0.7); */
		background-color: #fff;
		border-radius: 0 0 14px 14px;
	}
	.toggle-check{
		cursor: pointer;
	}
	#hotel-list{
		margin-bottom: 20px;
	}
	.hotel-desc-more{
		display:none;
	}
	.thumb {
		width: 100%;
		height: 200px;
		background-size: contain !important;
		background-position: center !important;
		background-repeat: no-repeat !important;
		/* background-color: #2B2B2B !important; */
		background-color: transparent;
	}
	.sort-button{
		width: 100%;
		padding: 11px;
		border-radius: 2px;
		text-align: left;
	}
	.sort-asc-desc{
		cursor: pointer;
	}
	#search.popover.bottom .popover-content{
		padding: 17px;
	}
	#search.popover.bottom .arrow {
		right: 4% !important;
	}
	#search.popover.bottom {
		top: 44px;
		margin-top: 5px;
		left: -530.25px;
		width: 600px !important;
		max-width: 600px !important;
	}
	#search.popover.bottom .popover-content{
		/* min-height: 150px; */
	}
	.popover > .arrow2,
	.popover > .arrow2:after {
		position: absolute;
		display: block;
		width: 0;
		height: 0;
		border-color: transparent;
		border-style: solid;
	}
	.popover > .arrow2 {
		border-width: 11px;
	}
	.popover > .arrow2:after {
		content: "";
		border-width: 10px;
	}
	.popover.bottom > .arrow2 {
		top: -11px;
		margin-left: -11px;
		border-top-width: 0;
		border-bottom-color: #999;
		border-bottom-color: rgba(0, 0, 0, .25);
	}
	.popover.bottom > .arrow2:after {
		top: 1px;
		margin-left: -10px;
		content: " ";
		border-top-width: 0;
		border-bottom-color: #fff;
	}
	.search-field{
		border: 1px solid #000;
		padding: 10px;
		width: 100%;
	}
	.search-button-container{
		padding-left: 0;
	}
	#suggestion-container{
		width: 100%;
		margin-top: 5px;
		min-height: 100px;
		max-height: 300px;
		overflow-x: hidden;
		overflow-y: scroll;
	}
	.suggestions{
		cursor: pointer;
	}
	.selected-suggestion, .suggestions:hover{
		background: rgba(39, 168, 223, 0.44);
		border: 1px solid #2AA9DF;
	}
	.gray-button{
		background-color: #e0e0e0;
		background-position: 0 -15px;
		padding: 10px;
		width: 100%;
		border: 1px solid rgb(169, 169, 169);
		border-radius: 2px;
	}
	/*#hotel-loader {
		position: absolute;
		top: 50%;
		width: 100%;
		transform: translateY( -50% );
		text-align: center;
	}*/
	#hotel-loader div {
		/*background: rgba(255, 255, 255, 0.81);*/
		text-align: center;
		padding: 8px;
		border-radius: 2px;
		font-size: 18px;
		width: 100%;
	}

	/* VIEW MORE STYLES */
	#view-more-loader {
		text-align: center;
		font-size: 18px;
		padding-top: 304px;
		text-transform: uppercase;
		position: absolute;
		top: 0;
		left: 0px;
		width: 100%;
		height: 100%;
		display: none;
		/*background: #ffffff;*/
	}
	#suburbs-list {
		margin: 1rem 0;
		padding: 5px 18px;
		background: #484848;
	}
	#suburbs-list li {
		text-align: center;
		cursor: pointer;
		color: #B6B6B6;
		display: inline-block;
		list-style: none;
		margin-right: 5px;
		transition: all 200ms ease-in-out 0s;
	}
	#suburbs-list li.active {
		color: #f5f5f5;
	}
	#suburbs-list li.hidden-suburb {
		display: none;
	}
	#load-more-suburbs-btn {
		text-decoration: none;
		border: 1px solid #f6f6f6;
		padding: 1px 5px;
		display: inline-block;
		color: #f6f6f6;
	}
	.display-room-type{
		display: block;
		position: relative;
		margin-bottom: 20px;
	}
	.display-room-type > span {
		position: absolute;
		right: 0;
		bottom: -20px;
	}
	.ui-state-active, .ui-slider-handle {
		background: #2AA9DF !important;
	}
	.range-field{
		border-top:0;
		border-left: 0; 
		border-right: 0;
		font-weight: bold;
		background: transparent;
		border-bottom: 1px solid #999;
	}
	.range-field:focus{
		outline: none;
	}
	.fa-star-o, .fa-star-half-o, .fa-star {
		font-size: 1.2em;
	}

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- <div id="hotels-container1"> -->
<div class="tabs-container">
            <div class="row">
              <div class="col-sm-7">
                <div class="panel-inner">
                  <div class="panel-icon">    
                    
                    <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                        <desc>Created with Sketch.</desc>
                        <defs>
                            <polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>
                        </defs>
                        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-503.000000, -69.000000)">
                                <g id="placeholder" transform="translate(503.000000, 69.000000)">
                                    <g id="Group">
                                        <mask id="mask-2" fill="white">
                                            <use xlink:href="#path-1"></use>
                                        </mask>
                                        <g id="Clip-2"></g>
                                        <path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                  </div>
                  <div class="panel-container"> 
                    <h4 id="city-name">
                      <?php echo e($data['city_name']); ?>, <?php echo e($data['city']['country_name']); ?><span class="hotel-count"></span>
                    </h4>
                    <p class="m-t-5">
                      <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                          <desc>Created with Sketch.</desc>
                          <defs></defs>
                          <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-535.000000, -95.000000)" fill="#212121">
                                  <g id="Page-1" transform="translate(535.000000, 95.000000)">
                                      <path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
                                      <path d="M10.1326667,8.000625 L12,8.000625 L12,6.25 L10.1326667,6.25 L10.1326667,8.000625 Z M10.1326667,10.2503125 L12,10.2503125 L12,8.4996875 L10.1326667,8.4996875 L10.1326667,10.2503125 Z M10.1326667,12.5 L12,12.5 L12,10.749375 L10.1326667,10.749375 L10.1326667,12.5 Z M7.733,12.5 L9.60033333,12.5 L9.60033333,10.749375 L7.733,10.749375 L7.733,12.5 Z M5.33333333,12.5 L7.20066667,12.5 L7.20066667,10.749375 L5.33333333,10.749375 L5.33333333,12.5 Z M2.93366667,12.5 L4.79933333,12.5 L4.79933333,10.749375 L2.93366667,10.749375 L2.93366667,12.5 Z M2.93366667,10.2503125 L4.79933333,10.2503125 L4.79933333,8.4996875 L2.93366667,8.4996875 L2.93366667,10.2503125 Z M2.93366667,8.000625 L4.79933333,8.000625 L4.79933333,6.25 L2.93366667,6.25 L2.93366667,8.000625 Z M5.33333333,8.000625 L7.20066667,8.000625 L7.20066667,6.25 L5.33333333,6.25 L5.33333333,8.000625 Z M5.33333333,10.2503125 L7.20066667,10.2503125 L7.20066667,8.4996875 L5.33333333,8.4996875 L5.33333333,10.2503125 Z M7.733,10.2503125 L9.60033333,10.2503125 L9.60033333,8.4996875 L7.733,8.4996875 L7.733,10.2503125 Z M7.733,8.000625 L9.60033333,8.000625 L9.60033333,6.25 L7.733,6.25 L7.733,8.000625 Z M9.60033333,5.749375 L2.39966667,5.749375 L2.39966667,13.000625 L12.534,13.000625 L12.534,5.749375 L9.60033333,5.749375 Z" id="Fill-3"></path>
                                  </g>
                              </g>
                          </g>
                      </svg> 
                     <?php echo e(convert_date($data['checkin'], 'new_eroam_format')); ?> - <?php echo e(convert_date($data['checkout'], 'new_eroam_format')); ?></p>
                  </div>
                </div>
              </div>

              <div class="col-md-2 m-t-10">
					<!-- <select name="duration" id="">
						<option value=""> 3 nights </option>
					</select> -->

					<button type="button"  class="btn btn-default filter-buttons" id="number_of_nights"> <?php echo e($data['default_nights'].' '.str_plural('Night', $data['default_nights'])); ?>

						<span class="caret"></span>
					</button>
					<div class="popover fade bottom in" role="tooltip" id="number_of_nights_list">
						<div class="arrow" style="left: 50%;"></div>

						<h3 class="popover-title">
							<i class="icon-style fa fa-moon-o fa-2x"></i> <strong>Number of Nights </strong>
						</h3>
						<div class="popover-content">
							<ul >

								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 1 ? 'black-background' : ''); ?>" data-value="1"></i>
									<span> 1 Night </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 2 ? 'black-background' : ''); ?>" data-value="2"></i>
									<span> 2 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 3 ? 'black-background' : ''); ?>" data-value="3"></i>
									<span> 3 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 4 ? 'black-background' : ''); ?>" data-value="4"></i>
									<span> 4 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 5 ? 'black-background' : ''); ?>" data-value="5"></i>
									<span> 5 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 6 ? 'black-background' : ''); ?>" data-value="6"></i>
									<span> 6 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 7 ? 'black-background' : ''); ?>" data-value="7"></i>
									<span> 7 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 8 ? 'black-background' : ''); ?>" data-value="8"></i>
									<span> 8 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 9 ? 'black-background' : ''); ?>" data-value="9"></i>
									<span> 9 Nights </span>
								</li>
								<li class="toggle-check">
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 10 ? 'black-background' : ''); ?>" data-value="10"></i>
									<span> 10  Nights </span>
								</li>

							</ul>
						</div>
					</div>
				</div>
              <div class="col-sm-3 m-t-10">
                <div class="input-group search-control">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                  </span>
                  <input type="text" class="form-control" placeholder="">
                </div>
              </div>
            </div>
            <!--<div class="custom-tabs" data-example-id="togglable-tabs">
                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                  <li role="presentation" class="active"><a href="#top-picks" id="top-picks-tab" role="tab" data-toggle="tab" aria-controls="top-picks" aria-expanded="true">TOP PICKS</a>
                  </li> 

                  <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop3" data-toggle="dropdown" aria-controls="myTabDrop3-contents">LOWEST PRICE <span class="caret"></span></a> 
                    <ul class="dropdown-menu" aria-labelledby="myTabDrop3" id="myTabDrop3-contents"> 

                    </ul> 
                  </li>
                  
                  <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents">RATING <span class="caret"></span></a> 
                    <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 

                       </ul> 
                  </li>

                  <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop2" data-toggle="dropdown" aria-controls="myTabDrop2-contents">STARS <span class="caret"></span></a> 
                    <ul class="dropdown-menu" aria-labelledby="myTabDrop2" id="myTabDrop2-contents"> 

                      </ul>
                  </li> 

                  <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop4" data-toggle="dropdown" aria-controls="myTabDrop4-contents">DISTANCE FROM CITY <span class="caret"></span></a> 
                    <ul class="dropdown-menu" aria-labelledby="myTabDrop4" id="myTabDrop4-contents"> 

                    </ul> 
                  </li>

               
                  </ul> 
            </div>-->
</div>

<div class="tabs-content-container">
	
        <div class="tab-content" id="myTabContent"> 
	    	
          <div class="tab-pane fade in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab"> 
			  <div id="hotels-container1" class="tabs-content-wrapper">				                
	                 
				<div id="hotel-loader">
						<div><i class="fa fa-circle-o-notch fa-spin"></i> Loading Hotels...</div>
				</div>
					 
	             <div class="row hotelList-wrapper" id="hotel-list">
	                  
	             </div>
	                

	          </div>
	          
                <div id="view-more-details">
				</div>

				<div id="view-more-loader">
					<i class="fa fa-circle-o-notch fa-spin"></i> loading hotel data...
				</div>
          </div> 
          <div class="tab-pane fade" role="tabpanel" id="provider1" aria-labelledby="provider1-tab"> 
	            <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p> 
	          </div> 
		        <div class="tab-pane fade" role="tabpanel" id="transport1" aria-labelledby="transport1-tab"> 
		          <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p> 
		        </div> 
	            <div class="tab-pane fade" role="tabpanel" id="lowest-price1" aria-labelledby="lowest-price1-tab"> 
	              <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p> 
	            </div> 
	            <div class="tab-pane fade" role="tabpanel" id="duration1" aria-labelledby="duration1-tab"> 
	              <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p> 
	            </div> 
      </div>   
</div>         
<!-- <div id="hotels-container">
	<div class="col-md-12">
		<div class="col-md-6 hotel-header">

			 <h4 class="blue-txt bold-txt " id="city-name"> <?php echo e($data['city_name']); ?> Hotels <small class="blue-txt hotel-count"> </small></h4>

			<p>
				<span>
				<?php if($data['checkin']): ?>
					<strong>Check-in</strong> <?php echo e(convert_date($data['checkin'], 'eroam_format')); ?>

				<?php endif; ?>
				</span>
				<span style="margin-left:10px;">
				<?php if($data['checkout']): ?>
					<strong>Check-out</strong> <?php echo e(convert_date($data['checkout'], 'eroam_format')); ?>

				<?php endif; ?>
				</span>
			</p>
		</div>
		<div class="col-md-6 hotels-content">
			<div class="row">
				<div class="col-md-5" >
				
					<button type="button"  class="btn btn-default sort-button filter-buttons" id="sort-ad">
						Sort 
						 <span class="caret"></span> 
					</button>
					<div class="popover fade bottom in" role="tooltip" id="sort">
						<div class="arrow" style="left: 50%;"></div>
						<h3 class="popover-title">
							<i class="icon-style fa fa-sort fa-2x"></i> <strong> Search Filters </strong> 
						</h3>
						<div class="popover-content">
							<ul >
								
								<li class="sort-asc-desc"> 
									<i class="fa icon-style fa-sort-asc " data-sort="asc"></i> 
									<span> Ascending </span> 
								</li>
								<li class="sort-asc-desc"> 
									<i class="fa icon-style fa-sort-desc" data-sort="desc"></i> 
									<span> Descending </span> 
								</li>

							</ul>
						</div>
					</div>

				</div>
				<div class="col-md-5">
					

					<button type="button"  class="btn btn-default filter-buttons" id="number_of_nights"> <?php echo e($data['default_nights'].' '.str_plural('Night', $data['default_nights'])); ?>

						<span class="caret"></span>
					</button>
					<div class="popover fade bottom in" role="tooltip" id="number_of_nights_list">		 	
						<div class="arrow" style="left: 50%;"></div>

						<h3 class="popover-title">
							<i class="icon-style fa fa-moon-o fa-2x"></i> <strong>Number of Nights </strong> 
						</h3>
						<div class="popover-content">
							<ul >
								
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 1 ? 'black-background' : ''); ?>" data-value="1"></i> 
									<span> 1 Night </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 2 ? 'black-background' : ''); ?>" data-value="2"></i> 
									<span> 2 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 3 ? 'black-background' : ''); ?>" data-value="3"></i> 
									<span> 3 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 4 ? 'black-background' : ''); ?>" data-value="4"></i> 
									<span> 4 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 5 ? 'black-background' : ''); ?>" data-value="5"></i> 
									<span> 5 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 6 ? 'black-background' : ''); ?>" data-value="6"></i> 
									<span> 6 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 7 ? 'black-background' : ''); ?>" data-value="7"></i> 
									<span> 7 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 8 ? 'black-background' : ''); ?>" data-value="8"></i> 
									<span> 8 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 9 ? 'black-background' : ''); ?>" data-value="9"></i> 
									<span> 9 Nights </span> 
								</li>
								<li class="toggle-check"> 
									<i class="fa icon-style fa-check white <?php echo e($data['default_nights'] == 10 ? 'black-background' : ''); ?>" data-value="10"></i> 
									<span> 10  Nights </span> 
								</li>
								
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-2" style="padding: 0">
					<button class=" btn bold-txt filter-buttons btn-default" id='search-toggler'> 
						<i class="fa fa-search"> </i> 
						<span class="caret"></span>
					</button>
					
					<div class="popover fade bottom in" role="tooltip" id="search">
						<div class="arrow2" style="right: 4% !important;"></div>
						<h3 class="popover-title">
							<i class="icon-style fa fa-bed fa-2x"></i> <strong> Hotel Search </strong> 
						</h3>
						<div class="popover-content">
							<form id="search-form">
								<div class="row">
									<div class="col-md-12">
										<p>
											<label for="">Price range:</label> 
											<span class="curr bold-txt"></span>
											<input type="text" size="9" name="price_from" id="price-from" class="range-field" > 
											<span class="bold-txt">.00  - </span>
											<span class="curr bold-txt"></span>
											<input type="text bold-txt" size="9" name="price_to" id="price-to" class="range-field">
											<span class="bold-txt">.00 </span>
											<span id="error_range" style="color: red;font-weight: bold;"></span>
											
										</p>
										<div id="slider-range"></div><br />

									</div>
									
									<div class="clearfix"></div>
									<div class="col-md-8"> 
										<input type="text" class="search-field" id="search-field" placeholder="Search for hotels here" autocomplete="off" autofocus > 
										<div id="suggestion-container">
											<ul>
												
											</ul>
										</div> 
										
									</div>
							
									<div class="col-md-2 search-button-container" > 
										<button type="submit" class="btn blue-button bold-txt" id="search-hotels"> Search </button> 
									</div>
									<div class="col-md-2 search-button-container" > 
										<button type="button" class="btn gray-button bold-txt" id="clear-search"> Clear </button> 
										
									</div>
								</div>
							</form>
							<div class="clearfix"></div>
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>
	<div class="col-md-12 suburbs">
			<ul id="suburbs-list" style="display:none;">
				<li class="active" data-suburb="all" data-suburb-id="0" data-hb-zone-id="0"><i class="fa fa-check-circle-o"></i> All</li>
				
			<?php if(count($data['suburb']) > 0): ?>
				<?php foreach($data['suburb'] as $suburb): ?>
				<li class="active" data-suburb="<?php echo e($suburb['name']); ?>"  data-suburb-id="<?php echo e($suburb['id']); ?>" data-hb-zone-id="<?php echo e($suburb['hb_zone']['zone_number']); ?>"><i class="fa fa-check-circle-o"></i><?php echo e($suburb['name']); ?></li>
				<?php endforeach; ?>
			<?php endif; ?>
				<a href="#" id="load-more-suburbs-btn">Load More</a>
			</ul>

	</div>

	<div class="col-md-12" id="hotel-list">
		
	</div>
	<div id="hotel-loader">
		<span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Hotels...</span>
	</div>
</div> -->



<?php /* MODALS */ ?>
<div class="modal fade room-types" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title selected-hotel-title blue-txt bold-txt">Hotel</h4>
				</div>
				<div class="modal-body table-container">
					
				</div>

		</div>
	</div>
</div>


<div class="modal fade no-hotel" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header ">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<p>Sorry, you can't add additional hotel nights because there's no hotel selected.</p>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-md-6 pull-right">
						<button type="button" class="btn confirm-cancel-button eroam-confirm-cancel-btn" data-dismiss="modal">Close</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<input type="hidden" class="the-city-id" value="<?php echo e(json_encode( get_city_by_id( $data['city_id'] ) )); ?>">



<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>

<script charset="utf-8">
	
	var default_provider   = '<?php echo e($data['default_provider']); ?>';
	var default_hotel      = '<?php echo e($data['default_hotel_id']); ?>';
	var default_room_price_raw = '<?php echo e(( number_format($data['default_room_price'], 2 ) )); ?>';
	var default_room_price = '<?php echo e(( number_format($data['default_room_price'], 2 ) / $data["travellers"] )); ?>';
	var default_room_name  = '<?php echo e($data['default_room_name']); ?>';
	var default_price_id   = '<?php echo e($data['default_price_id']); ?>';
	var default_room_id   = '<?php echo e($data['default_room_id']); ?>';
	var default_currency   = '<?php echo e($data['default_currency']); ?>';
	var from_date          = "<?php echo e($data['date_from']); ?>";
	var to_date            = "<?php echo e($data['date_to']); ?>";
	var city_id            = '<?php echo e($data['city_id']); ?>'; 
	var search_session     = JSON.parse( $('#search-session').val() );
	var search_input       = JSON.parse( $('#search-input').val() );
	var totalHotels        = 0;
	var maxPrice           = 0;
	var hotel_filter       = [<?php echo e(join( ', ', $data['hotel_filter'] )); ?>];
	var cityData           = JSON.parse($('.the-city-id').val());
	var room_type_id	   = [<?php echo e(join( ', ', $data['room_type_id'] )); ?>];
	var hotel_stars	   = [<?php echo e(join( ', ', $data['hotel_stars'] )); ?>];
	var default_hotel_category_id	   = '<?php echo e($data['default_hotel_category_id']); ?>';
	var number_of_travellers = <?php echo e($data["travellers"]); ?>;
	var leg = '<?php echo e($data['leg']); ?>';
	
	var hotels_appended    = [];
	var hotels_from_price  = {
		eroam : [],
		hb : [],
		aot : [],
		ae : [],
	}
	$(document).ready(function(){

		var selected_attributes,
			is_selected, 
			is_active;

		console.log("cityData", cityData);

		buildHotels();

		// UPDATED BY RGR
		$('body').on('click', '.room-type-button', function() {
			buildRoomTypes(false, $(this).attr('data-index'), $(this).attr('data-season'), $(this).attr('data-hotel'), $(this).attr('data-provider'), $(this).attr('data-hotel-name'), $(this).attr('data-hotel-location'), $(this).attr('data-rooms'));
		});
		$('body').on('click', '.select-room-btn', function() {
			$('.select-room-btn').prop('disabled', false);
			$(this).prop('disabled', true);
		});

		/*
		|Added by Junfel
		|Sorting the display by Hotel names
		*/
		$('.sort-asc-desc').click(function(){
			$('.sort-asc-desc').find('i').removeClass('black-background white');
			$(this).find('i').toggleClass('black-background white');
			var asc_desc = $(this).find('i').attr('data-sort');
			var asc = asc_desc == 'asc' ? true : false;
			hotelSort(asc);
		});
		/*
		| Added by Junfel
		| Update booking summary when room is selected
		*/
		$('body').on('click', '.selected-room', function(){
			// UPDATED BY RGR
			$('.selected-room').prop('disabled', false);
			$(this).prop('disabled', true);
			var data         = $(this).data();

			if(data.provider == 'hb'){
				data.images = [data.images];
			}
			
			default_provider   = data.provider;
			default_hotel      = data.hotelId;
			default_room_name  = data.roomName;
			default_room_id    = data.roomId;

			if(data.provider == 'eroam' && data.roomName.includes('Dorm')){
				var perPersonPrice = Math.ceil( eroam.convertCurrency( data.price, data.currency ) ).toFixed(2);
			}else{
				var perPersonPrice =  Math.ceil( eroam.convertCurrency( data.price, data.currency ) / number_of_travellers ).toFixed(2);
			}

			//$('#hotel-price-'+data.hotelId).html('<small> From Price Per Night <br />( Per Person ) </small> <br /> '+globalCurrency+' '+Math.ceil(perPersonPrice).toFixed(2));
			$('#hotel-price-'+data.hotelId).html('From Price <strong>'+globalCurrency+' '+Math.ceil(perPersonPrice).toFixed(2)+'</strong> (Per Night Per Person)');
			$('.hotel-list').removeClass('selected-hotel').attr('data-is-selected', 'false');
			$('#hotel-list-'+data.id).addClass('selected-hotel').attr('data-is-selected', 'true').attr('data-price', data.price).attr('data-price-id', data.priceId);
			$('.room-types').modal('hide');
			
			var session = search_session.itinerary;

			if(session.length > 0){

				if(session[leg].city.id == city_id ){
					search_session.itinerary[leg].hotel = convertToSnakeCase( data );
					if(leg == 0){
						search_session.itinerary[0].city.accommodation = true;
					}
					bookingSummary.update( JSON.stringify( search_session ) );
				}

			}

		});
		/*
		| Added by Junfel
		| popover toggler
		*/
		$('.filter-buttons').click(function(){
			var id = $(this).attr('id');
			switch(id){
				case 'sort-ad':
					$('#number_of_nights_list').removeClass('show');
					$('#search').removeClass('show');
					$('#sort').toggleClass('show');
				break;
				case 'search-toggler':
					$('#number_of_nights_list').removeClass('show');
					$('#sort').removeClass('show');
					$('#search').toggleClass('show');
					$('#search-field').focus();
					return false;
				break;
				case 'number_of_nights':
					$('#sort').removeClass('show');
					$('#search').removeClass('show');
					$('#number_of_nights_list').toggleClass('show');
				break;
			}
		});
		$('body').click(function(e){
			var target = e.target;
			if ( $(target).hasClass('filter-buttons') === false && $('.popover').has(e.target).length === 0 ) {
				$('.filter-buttons').removeClass('show');
				$('.popover.bottom').removeClass('show');
			}
		});
		/*
		| added by Junfel
		| displaying suggestions
		| arrow up and down selection
		*/
		arrowSelect = function(event) {
			var numOfSuggestions = $('.suggestions').length-1;
			var selected = -1;
			var items = $('.suggestions');
			var current = $('.selected-suggestion').index();

			if(event.which == $.ui.keyCode.UP) {
				if(current == -1){
					$(items[numOfSuggestions]).addClass('selected-suggestion');
					var value = $(items[numOfSuggestions]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}else{
					var next = current - 1;
					next = next < 0 ? numOfSuggestions : next;
					$(items[current]).removeClass('selected-suggestion');
					$(items[next]).addClass('selected-suggestion');
					var value = $(items[next]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}
			}
			else if(event.which == $.ui.keyCode.DOWN){
				if(current == -1){
					$(items[0]).addClass('selected-suggestion');
					var value = $(items[0]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}else{
					var next = current + 1;
					next = next > numOfSuggestions ? 0 : next;
					$(items[current]).removeClass('selected-suggestion');
					$(items[next]).addClass('selected-suggestion');
					var value = $(items[next]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}
			}
			else{
				$('#suggestion-container').find('ul').html('');
				var pattern = new RegExp($('.search-field').val().toString(), 'gi');
				var priceTo = parseFloat($('#price-to').val());
				var priceFrom  = parseFloat($('#price-from').val());

				$('.hotel-list').filter(function() {
					var hotPrice = parseFloat($(this).attr('data-from-price'));
			
					if( hotPrice >= priceFrom && hotPrice <= priceTo ){
						if($(this).find('h4').text().match(pattern) != null){
							var list = '<li class="suggestions">'+$(this).find('h4').text()+'</li>'; 
							$('#suggestion-container').find('ul').append(list);
						}
					}else{
						if($(this).find('h4').text().match(pattern) != null){
							var list = '<li class="suggestions">'+$(this).find('h4').text()+'</li>'; 
							$('#suggestion-container').find('ul').append(list);
						}
					}
				});
			}
		}
		/*
		| Added by junfel 
		| function for showing suggestion on keydown and selecting suggestion through arrow down and arrow up
		*/
		$('.search-field').bind('keydown', arrowSelect);
		/*
		| Added by Junfel
		| function for clearing search and display all hotels
		*/
		$('#clear-search').click(function(){
			var min = parseInt(eroam.convertCurrency(0, 'AUD'));
			//var max = parseInt(eroam.convertCurrency(50000, 'AUD'));
			var max =  parseInt(maxPrice);
			$( '#slider-range' ).slider({
				range: true,
				min: min,
				max: max,
				values: [ min, max ]
			});
			$('#price-from').val(min);
			$('#price-to').val(max);

			$('.hotel-list').show();
			$('.search-field').val('');
			$('#search-form').submit();
		});
		/*
		| Added by Junfel
		| function for selecting suggestion through mouse click
		*/
		$('body').on('click', '.suggestions', function(){
			var value = $(this).text();
			$('.search-field').val(value);
			$('#search-form').submit();
		});
		/*
		| Added by Junfel 
		| Function for searching hotels in the selected City
		*/
		$('#search-form').submit(function(){
			
			/*
			| search pattern
			| check all possible matches and case insensitive
			*/
			$('#suggestion-container').find('ul').html('');
			var pattern = new RegExp($('.search-field').val().toString(), 'gi');
			var priceTo = parseFloat($('#price-to').val());
			var priceFrom  = parseFloat($('#price-from').val());

			$('.hotel-list').filter(function() {
				
				var hotPrice = parseFloat($(this).attr('data-from-price'));
				if( hotPrice >= priceFrom && hotPrice <= priceTo ){
					if($(this).find('h4').text().match(pattern) == null){
						 $(this).hide();
					}else{
						$(this).show();
					}
				}else{
					$(this).hide();
				}
			});

			return false;
			
		});
		/*
		| Added by Junfel
		| function for updating the default nights
		*/
		$('.toggle-check').click(function(){
			
			var checkbox = $(this).find('i');
			var value = checkbox.attr('data-value');
			
			eroam.ajax( 'get', 'latest-search', { }, function( response ){

				if(response){
					search_session = JSON.parse(response);
					var session = search_session.itinerary;

					if(session.length > 0){

						if(session[leg].city.id == city_id ){

							var nights = search_session.itinerary[leg].city.default_nights;
							//var nights = 0;
							//if( search_session.itinerary[leg].hotel != null && !isNaN(search_session.itinerary[leg].hotel.nights) ){
							if( (search_session.itinerary[leg].hotel != null) &&  typeof search_session.itinerary[leg].hotel.nights != 'undefined' ){
								
								nights = search_session.itinerary[leg].hotel.nights;
								search_session.itinerary[leg].hotel.nights = value;
							
								var days_to_add = parseInt(value) - parseInt(nights);

								if( days_to_add > 0 ){
									
									search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) + days_to_add;
									search_session.itinerary[leg].city.days_to_add = days_to_add;
									search_session.itinerary[leg].city.add_after_date = from_date;
									bookingSummary.update( JSON.stringify( search_session ) );

									checked(checkbox, value);

								}else{
									
									var deduct = Math.abs(days_to_add);
									var val = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

									search_session.itinerary[leg].city.days_to_deduct = deduct;
									search_session.itinerary[leg].city.deduct_after_date = from_date;
									search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

									if( search_session.itinerary[leg].activities != null ){
										
										var act = sortByDate(search_session.itinerary[leg].activities);
										var departureDate = getDeparture(leg);
										departureDate = departureDate.split(' ');

										/*if( parseInt(departureDate[1]) < 18 ){
											deduct = parseInt(deduct) - 1;
										}*/

										var activityDates = getActivityToBeCancelledByDates(  search_session.itinerary[leg].city.date_to, parseInt(deduct) );
										activityCancellation(activityDates, leg, act, checkbox, value );
									}else{
										bookingSummary.update( JSON.stringify( search_session ) );
										checked(checkbox, value);
									}
									return;
								}
							}else{
								$('.no-hotel').modal();
							}
						}

					}
				}
			});
			
		});
		//jayson added for filtering of hotels by suburbs
		$( 'body' ).on( "click", '#suburbs-list li', function(  ){
			var selected_suburbs = [];
			var selected_zones = [];

			$('#suburbs-list li').each(function()
			{
				if($(this).hasClass("active")) { 
					var data = [];
					selected_suburbs.push($(this).data("suburb-id"));
					selected_zones.push($(this).data("hb-zone-id"));
				}
			
			});	

			$('.hotel-list').each(function(){
				provider = $(this).data('provider');
				div_id = $(this).attr('id');
				is_selected = $(this).data('isSelected');

				switch(provider){
					case "eroam" :

						hotel_suburb_id = $(this).data('hotelSuburbId');
						console.log( hotel_suburb_id,selected_suburbs[0]);
						
						if(selected_suburbs.length > 0 && $.inArray( hotel_suburb_id,selected_suburbs) == -1 && is_selected != true && selected_suburbs[0] !== 0){
							
							$('#'+div_id).hide();
						}else{
							$('#'+div_id).show();
						}
					
					break;
					case "hb":

						hotel_hb_zone = $(this).data('hotelZoneId');
						console.log( hotel_hb_zone,selected_zones);
						if(selected_zones.length > 0 && $.inArray( hotel_hb_zone,selected_zones) == -1 && is_selected != true){
				
							$('#'+div_id).hide();
						}else{
							
							$('#'+div_id).show();
						}
					
						
					break;	
				}
			});

		});
		//jayson added end

		$( 'body' ).on( "click", '.hotel-name', function(  ){
			var hotel_id = $( this ).attr( 'data-hotel-id' );
			$( '.hotel-list[data-hotel-id="'+hotel_id+'"]' ).find( ".hotel-desc-less" ).toggle();
			$( '.hotel-list[data-hotel-id="'+hotel_id+'"]' ).find( ".hotel-desc-more" ).toggle();
		} );

		$('body').on('click', '.view-more-btn', showHotelDetails);
		$('body').on('click', '.act-btn-less', function() {
			var provider = $(this).data('provider');
			var code = $(this).data('code');
			$('#view-more-details').html('').hide();
			$('#hotels-container1').show();
			var hotel = $('#hotels-container1').find('.view-more-btn[data-provider="'+provider+'"][data-code="'+code+'"]').offset().top;
			$('.content-body').scrollTop(hotel - 250);
			setEqualHeight_CommonClass(".hotel-details-block .hotel-details .panel-container");
		});

		$('#suburbs-list li').click(function() {
			var suburb = $(this).data('suburb');

			if (suburb == 'all') {
				if ($(this).hasClass('active')) {
					$('#suburbs-list li').removeClass('active');
					$('#suburbs-list li i').attr('class', 'fa fa-circle-o');
				} else {
					$('#suburbs-list li').addClass('active');
					$('#suburbs-list li i').attr('class', 'fa fa-check-circle-o');
				}
			} else {
				$('#suburbs-list li[data-suburb="all"]').removeClass('active').find('i').attr('class', 'fa fa-circle-o');
				$('#suburbs-list li[data-suburb="all"]').removeClass('active');
				$(this).find('i').toggleClass('fa-circle-o fa-check-circle-o');
				$(this).toggleClass('active');
			}
		});

		// toggle more suburds
		$('#suburbs-list li').each(function(k, v) {
			if (k > 7) {
				$(this).addClass('hidden-suburb')
			}
		});

		if ($('#suburbs-list li').length > 7) {
			$('#load-more-suburbs-btn').show();
		}else{
			$('#load-more-suburbs-btn').hide();
		}

		$('#load-more-suburbs-btn').click(function(e) {
			e.preventDefault();
			var hidden = $('#suburbs-list li.hidden-suburb');

			hidden.each(function(k, v) {
				if (k < 7) {
					$(this).removeClass('hidden-suburb');
				}
				// check if no more hidden suburbs
				if (hidden.length <= 7) {
					$('#load-more-suburbs-btn').hide();
				}
			});
		});

	}); // end of document ready

	/* ADDED BY RGR */
	/* BUILD ROOM TYPES */
	function buildRoomTypes(viewMore = false, dataIndex, dataSeason, dataHotel, dataProvider, dataHotelName, dataHotelLocation, dataRooms) {
		var hotel_index    = dataIndex;
		var season         = dataSeason;
		var hotel_id       = dataHotel;
		var html           = '', table = '';
		var rooms;
		var hotel_provider = dataProvider;

		if (viewMore == true) {
			var container = $('#view-more-rooms');
		} else {
			var container = $('.table-container');
			$('.room-types').modal();
			container.html('<p class="text-center" style="padding: 20px"> <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>  </p>');
		}

		var table ='<table class="table table-striped ">';
		table +='<thead>';
		table +='<th> Room Type </th>';
		table +='<th> Price Per Person </th>';
		table +='<th> &nbsp; </th>';
		table +='</thead>';
		table +='<tbody class="room_prices">';
		table +='</tbody>';
		table +='</table>';

		switch(hotel_provider){
			
			case "eroam":

				var hotelName = dataHotelName;
				rooms = jQuery.parseJSON( eroamRooms[hotel_id] );
				if(rooms.length > 0){
					$('.selected-hotel-title').text(hotelName);
					container.html(table);

					
					for( counter = 0; counter  < rooms.length; counter ++ ){
						if( rooms[counter].hotel_season_id == season ){
							
							rooms.hotel_name      = hotelName;
							rooms.price_id        = rooms[counter].id;
							rooms.each_room_price = price( parseFloat(rooms[counter].price).toFixed(2), rooms[counter].season['currency']['code']);

							rooms.price           = rooms[counter].price;
							rooms.currency        = rooms[counter].season.currency.code;
							rooms.room_type       = rooms[counter].room_type.name;
							rooms.hotel_id        = hotel_id;
							rooms.room_type_id    = rooms[counter].room_type.id;
							rooms.room_id   	  = rooms[counter].room_type.id;
							rooms.season_id       = rooms[counter].hotel_season_id;
							rooms.provider  = 'eroam';
							appendRoomtypes( rooms );

						}else{
							continue;
						}
					}
				}else{
					container.html('<p class="text-center"> No Room Type Available. </p>');
				}
				break;

			case "hb":
				rooms = jQuery.parseJSON( hbRooms[hotel_id] );

				var hotel_name = dataHotelName;
				var address = dataHotelLocation;
				//console.log(rooms);
			
				if(rooms.length > 0){
					$('.selected-hotel-title').text( hotel_name );
					container.html(table);
					$.each(rooms, function( key, value ) {
						if (typeof value.rates[0].net == 'undefined'){
							return;
						}
						rooms.hotel_name = hotel_name;
						rooms.price      = parseFloat(value.rates[0].net).toFixed(2);
						rooms.rate_key   = value.rates[0].rateKey;
						rooms.currency   = 'AUD';
						rooms.room_type  = value.name;
						rooms.room_id    = value.code;
						rooms.hotel_id   = hotel_id;
						rooms.provider   = 'hb';
						rooms.location   = address;
						rooms.images 	 = rooms[0].images;
						rooms.description = rooms[0].description;
						rooms.cancellation_policy = '';
						appendRoomtypes(rooms);
					});
				}else{
					container.html('<p class="text-center"> No Room Type Available. </p>');
				}
				break;

			case "expedia":
				rooms = jQuery.parseJSON( expediaRooms[hotel_id] );

				var hotel_name = dataHotelName;
				var address = dataHotelLocation;
				//console.log(rooms);
			
				if(rooms.length > 0){
					$('.selected-hotel-title').text( hotel_name );
					container.html(table);
					$.each(rooms, function( key, value ) {
						//console.log(value);
						if (typeof value.rate == 'undefined'){
							return;
						}
						rooms.hotel_name = hotel_name;
						rooms.price      = parseFloat(value.rate).toFixed(2);
						rooms.rate_key   = value.rateKey;
						rooms.rate_code  = value.rateCode;
						rooms.currency   = 'AUD';
						rooms.room_type  = value.roomDescription;
						rooms.room_id    = value.roomTypeCode;
						rooms.hotel_id   = hotel_id;
						rooms.provider   = 'expedia';
						rooms.location   = address;
						rooms.images 	 = rooms[0].images;
						rooms.description = rooms[0].roomDescription;
						rooms.cancellation_policy = '';
						//console.log(rooms);
						appendRoomtypes(rooms);
					});
				}else{
					container.html('<p class="text-center"> No Room Type Available. </p>');
				}
				break;

			case 'ae':
				rooms = jQuery.parseJSON( aeRooms[hotel_id] );
				var hotel_name = dataHotelName;
				var address = dataHotelLocation;
				console.log(rooms);

				if(rooms.length > 0){
					$('.selected-hotel-title').text( hotel_name );
					container.html(table);
					$.each(rooms, function( key, value ) {
						if (typeof value.price == 'undefined'){
							return;
						}
						rooms.hotel_name          = hotel_name;
						rooms.price               = parseFloat(value.price).toFixed(2);
						rooms.currency            = 'AUD';
						rooms.room_type           = value.name;
						rooms.room_id             = value.room_id;
						rooms.meal_id             = value.meal_id;
						rooms.hotel_id            = hotel_id;
						rooms.provider            = 'ae';
						rooms.location            = address;
						rooms.images              = rooms[0].images;
						rooms.description         = rooms[0].description;
						rooms.cancellation_policy = '';
						rooms.group_identifier    = value.group_identifier;
						appendRoomtypes(rooms);
					});
				}else{
					container.html('<p class="text-center"> No Room Type Available. </p>');
				}

				break;
					
			case "aot":

				var room_opts =  dataRooms;
				var hotel_name = dataHotelName;
				var address = dataHotelLocation;
				room_opts = room_opts.split( "|" );

				var aot_room_data = {
					type : 'orr_multi',
					opt : room_opts,
					date_from : formatDate(from_date),
					date_to : formatDate(to_date)
				};

				$('.selected-hotel-title').text( hotel_name );

				var eroamApiCallAotRoomTypes = eroam.apiDeferred('api_aot', 'POST', aot_room_data, 'aot', true);

				eroam.apiPromiseHandler( eroamApiCallAotRoomTypes, function( aotReponseRoomTypes ){

					
					if( aotReponseRoomTypes.length > 0 ){
						container.html(table);

						aotHotelsArray = aotHotelsArray.map(function(val){
							val.rooms = [];
							return val;
						});

						var aotHotelsArrayMerged =  aot.clean( 'supp_rooms_merge', aotHotelsArray , {
							array :  aotReponseRoomTypes ,
							room_type: 'double'
						} )

						var hotel_code = hotel_id;
						aotHotelsArray = aot.separateRoomTypesPrices( aotHotelsArray, aotHotelsArrayMerged,hotel_code);	
						var aotHotelSelected = aotHotels[hotel_id];
						if( aotHotelSelected.rooms.length > 0)
						{
							var rooms = {};
							for( counter = 0; counter  < aotHotelSelected.rooms.length; counter ++ )
							{
								var currentRoom = aotHotelSelected.rooms[counter];
								currentRoom.hotel_name = hotel_name;
								currentRoom.currency = 'AUD';
								currentRoom.provider = 'aot';
								currentRoom.price = parseFloat(currentRoom.price).toFixed(2);
								currentRoom.hotel_id = hotel_id;
								currentRoom.price_id = currentRoom.code;
								currentRoom.location = address;
								appendRoomtypes(currentRoom);
							}
						}
					}else{
						container.html('<p class="text-center"> No Room Type Available. </p>');
					}
				});

				break;
		}
	}

	function removeAllAct(){

	}
	/* ADDED BY RGR */
	/* SHOW HOTEL DETAILS */
	function showHotelDetails(event) {
		event.preventDefault();
		var provider = $(this).data('provider');
		var code = $(this).data('code');
		var star_rating = $(this).data('star-rating')
		var dataAttr = {
			index: $(this).attr('data-index'),
			season: $(this).attr('data-season'),
			hotel: $(this).attr('data-hotel'),
			provider: $(this).attr('data-provider'),
			hotelName: $(this).attr('data-hotel-name'),
			hotelLocation: $(this).attr('data-hotel-location'),
			rooms: $(this).attr('data-rooms')
		};

		eroam.ajax('get', 'hotels/view-more', {provider: provider, code: code, dataAttr: dataAttr, star_rating: star_rating }, function(response) {
			$('#view-more-details').html(response).show();

				$(function() {
					calculateHeight();
					$('.activityDesc').slimScroll({
				      height: '100%',
				      color: '#212121',
				      opacity: '0.7',
				      size: '5px',
				      allowPageScroll: true
		    		});
		        var jcarousel = $('.jcarousel'); 

		        jcarousel
		            .on('jcarousel:reload jcarousel:create', function () {
		                var carousel = $(this),
		                width = carousel.innerWidth();

		                if (width >= 1280) {
		                    width = width / 5;
		                } else if (width >= 992) {
		                    width = width / 4;
		                }
		                else if (width >= 768) {
		                    width = width / 4
		                }
		                else if (width >= 640) {
		                    width = width / 4;
		                } else {
		                    width = width / 3;
		                }

		                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
		            })
		            .jcarousel({
		                wrap: 'circular'
		            });

		        $('.jcarousel-control-prev')
		            .jcarouselControl({
		                target: '-=1'
		            });

		        $('.jcarousel-control-next')
		            .jcarouselControl({
		                target: '+=1'
		            });

		        $('.jcarousel-pagination')
		            .on('jcarouselpagination:active', 'a', function() {
		                $(this).addClass('active');
		            })
		            .on('jcarouselpagination:inactive', 'a', function() {
		                $(this).removeClass('active');
		            })
		            .on('click', function(e) {
		                e.preventDefault();
		            })
		            .jcarouselPagination({
		                perPage: 1,
		                item: function(page) {
		                    return '<a href="#' + page + '">' + page + '</a>';
		                }
		            });
		    

		    });
		}, function() {
			$('#hotels-container1').hide();
			$('#view-more-loader').show();
		}, function() {
			$('#view-more-loader').hide();
		});
	}

	/*
	| Added by Junfel
	| function for sorting hotels
	*/
	function hotelSort(asc = true){
		var hotelList = $('.hotel-list');
	    var alphabeticallyOrderedHotels = hotelList.sort(function (itemA, itemB) {
	    	if(asc){
	    		return $(itemA).find('h4.hotel-name').text() > $(itemB).find('h4.hotel-name').text();
	    	}else{
	    		return $(itemB).find('h4.hotel-name').text() > $(itemA).find('h4.hotel-name').text();
	    	}
	        
	    });
	    $('#hotel-list').html(alphabeticallyOrderedHotels);
	}

	function price(price, code){
		var new_price = Math.ceil(price).toFixed(2);
		var new_code = code.replace('D', '$');
		return new_code+' '+new_price;
	}

	function date_between(from, price_from, price_to){
		var result     = false;
		var from       = new Date(from);
		var price_from = new Date(price_from);
		var price_to   = new Date(price_to);
		if( from >= price_from && from <= price_to ){
			result = true;
		}
		return result;
	}

	function str_limit(str){
		var result = str;
		if(str.length > 200){
			result = str.substring(0, 200)+'.....';
		}
		return result;
	}

	function appendHotels( hotel ){

		var  name, id, image, index, description, season_id, html, rooms,attributes, is_active, hidden, roomData, roomName, stars, star_rating;
		var location = '';

		switch( hotel.provider )
		{
			case 'eroam':
				name        = hotel.name;
				id          = hotel.id;
				image       = "<?php echo e(config('env.DEV_CMS_URL')); ?>" + hotel.image;
				index       = hotel.index;
				description = hotel.description;
				season_id   = hotel.season_id;
				attributes  = hotel.attributes;
				provider    = "eroam";
				is_active   = hotel.is_active;
				location    = hotel.address_1;
				category    = hotel.hotel_category_id;
				suburb      = hotel.suburb;
				rooms       = '';
				//url is needed for view button
				url         = window.location.origin+'/eroam/hotel/'+id;
				//end
				hidden 		= hotel.hidden;
				if( window.eroamRooms === undefined ) window.eroamRooms = {};
				window.eroamRooms[id] = (hotel.prices) ? JSON.stringify(hotel.prices) : "";
				roomData = getHotelFromPrice( hotel );
				fromPriceString = roomData.fromPrice;
				roomName = roomData.roomName;
				stars = getHotelCategory(hotel.star_rating, 'eroam');
				star_rating = hotel.star_rating;
				break;
				
			case 'aot':
			
				provider    = hotel.provider;
				name        = hotel.name;
				id          = hotel.code;
				image       = hotel.image;
				category    = Math.floor(hotel.StarRating);
				index       = hotel.index;
				description = (hotel.description) ? hotel.description:"";
				season_id   = hotel.season_id;
				provider    = hotel.provider;
				rooms       = hotel.room_codes.join( "|" );
				attributes  = hotel.attributes;
				is_active   = hotel.is_active;
				location    = hotel.location;
				url 		= window.location.origin+'/aot/hotel/'+hotel.code;
				hidden 		= [];
				//fromPriceString = getHotelFromPrice( hotel );
				roomData = getHotelFromPrice( hotel );
				fromPriceString = roomData.fromPrice;
				roomName = roomData.roomName;
				stars = '';
				star_rating = '';

				break;

			//added by jayson for Hotel Beds API
			case 'hb':
				// console.log( "incubus : " , hotel );
				name               = hotel.name;
				id                 = hotel.code;
				image              = hotel.image;
				index              = hotel.code;
				provider           = "hb";
				category           = hotel.category;
				season_id          = '';
				rooms              = '';

				if( window.hbRooms === undefined ) window.hbRooms = {};
				hotel.rooms[0].images = [image];
				hotel.rooms[0].description = hotel.description;
				hotel.rooms[0].cancellation_policy = '';
				
				window.hbRooms[id] = (hotel.rooms) ? JSON.stringify(hotel.rooms) : "";

				description        = (hotel.description) ? hotel.description:"";
				attributes         = hotel.attributes;
				is_active          = hotel.is_active;
				location           = hotel.address;
				url 			   = window.location.origin+'/hb/hotel/'+hotel.code;
				hidden 		       =  [];
				//fromPriceString    = getHotelFromPrice( hotel );
				roomData = getHotelFromPrice( hotel );
				fromPriceString = roomData.fromPrice;
				roomName = roomData.roomName;
				
				stars = getHotelCategory(hotel.categoryCode, 'hb');
				star_rating = hotel.categoryCode;
				break;

			case 'expedia':
				// console.log( "incubus : " , hotel );
				name               = hotel.name;
				id                 = hotel.hotelId;
				image              = hotel.image;
				index              = hotel.hotelId;
				provider           = "expedia";
				category           = hotel.propertyCategory;
				season_id          = '';
				rooms              = '';

				//console.log(name);
				if( window.expediaRooms === undefined ) window.expediaRooms = {};
				hotel.rooms[0].images = [image];
				//hotel.rooms[0].description = hotel.description;
				hotel.rooms[0].cancellation_policy = '';
				//console.log(hotel.rooms);
				
				window.expediaRooms[id] = (hotel.rooms) ? JSON.stringify(hotel.rooms) : "";
				//console.log(window.expediaRooms[id]);

				description        = (hotel.description) ? hotel.description:"";
				attributes         = hotel.attributes;
				is_active          = hotel.is_active;
				location           = hotel.address;
				url 			   = window.location.origin+'/expedia/hotel/'+hotel.hotelId;
				hidden 		       =  [];
				//fromPriceString    = getHotelFromPrice( hotel );
				//console.log(url);

				roomData = getHotelFromPrice( hotel );
				fromPriceString = roomData.fromPrice;
				roomName = roomData.roomName;

				stars = getHotelCategory(hotel.hotelRating, 'expedia');
				star_rating = hotel.hotelRating;
				break;

			case 'ae':
				// console.log( "incubus : " , hotel );
				name               = hotel.name;
				id                 = hotel.hotel_id;
				image              = (hotel.image) ? hotel.image : 'http://cms.testeroam.com/images/no-image.png';
				index              = hotel.hotel_id;
				provider           = "ae";
				category           = hotel.category;
				season_id          = '';
				rooms              = '';
				if( window.aeRooms === undefined ) window.aeRooms = {};
				hotel.rooms[0].images = [image];
				hotel.rooms[0].description = hotel.description;
				hotel.rooms[0].cancellation_policy = '';
				
				window.aeRooms[id] = (hotel.rooms) ? JSON.stringify(hotel.rooms) : "";

				description        = (hotel.description) ? hotel.description:"";
				attributes         = hotel.attributes;
				is_active          = hotel.is_active;
				location           = hotel.city_name;
				url 			   = '';
				hidden 		       =  [];
				//fromPriceString    = getHotelFromPrice( hotel );
				roomData = getHotelFromPrice( hotel );
				fromPriceString = roomData.fromPrice;
				roomName = roomData.roomName;
				stars = '';
				star_rating = '';
				break;
		}

		var fromPriceSplit = fromPriceString.split(' ');


		console.log(provider+'==>'+name+'==>'+fromPriceSplit);

		maxPrice =  parseInt(fromPriceSplit[1]) > maxPrice ? parseInt(fromPriceSplit[1]) : maxPrice;
		attributes = attributes + ' ' + ' data-from-price="'+fromPriceSplit[1]+'" ';
		html = '<div class="col-md-6 hotel-list '+is_active+'" id="hotel-list-'+id+'" '+attributes+' >';
		html += hidden.join('');
		html += '<div class="hotel-details-block"><div class="hotel-img">';
		html += '<img src="'+image+'" alt="" class="img-responsive" />';

		html += '<div class="hotel-img-details">';
		if(is_active){
			html += '<div class="hotel-bg hotel-price" id="hotel-price-'+id+'">From Price <strong>'+hotel.priceString +'</strong> (Per Night Per Person)</div>';
			html += ' <div class="hotel-block hotel-margin"><div class="hotel-bg"><span>'+hotel.roomName+ '</span></div></div>';		
		}else{
			if( fromPriceString != '' ){
					html += '<div class="hotel-bg hotel-price" id="hotel-price-'+id+'">From Price <strong>'+fromPriceString +'</strong> (Per Night Per Person)</div>';
					html += ' <div class="hotel-block hotel-margin"><div class="hotel-bg"><span>'+roomName+ '</span></div></div>';
				}
			}
		html += '<div class="hotel-block"><div class="hotel-bg"><span><ul class="rating">'+stars+'</ul></span></div></div>';
		html += '</div>';
		html += '</div>';
		html += '<div class="hotel-details">';

		//html += '<a href="#" data-id="hotel_'+id+'">'+name+'</a></h4>';
		html += '<h4 class="blue-txt bold-txt hotel-name hotel-title" data-hotel-id="'+id+'"><a href="'+url+'" data-star-rating="'+star_rating+'" data-hotel-name="'+name+'"  data-hotel-category="'+category+'" data-index="'+index+'" data-hotel="'+id+'" data-season="'+season_id+'" data-hotel-name="'+name+'" data-provider="'+provider+'" data-rooms="'+rooms+'" data-hotel-location="'+location+'" data-code="'+id+'" class="view-more-btn">'+name+'</a></h4>'; 
		
		html += '<div class="panel-icon">';
		html += '<svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">';
                              html += '<desc>Created with Sketch.</desc>';
                              html += '<defs>';
                                  html += '<polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>';
                              html += '</defs>';
                              html += '<g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">';
                                  html += '<g id="53---EROA007-V4.1-Generic-(Edit-Hotel)-01" transform="translate(-500.000000, -402.000000)">';
                                      html += '<g id="Group-22" transform="translate(490.000000, 168.000000)">';
                                          html += '<g id="Group-44">';
                                              html += '<g id="Group-41">';
                                                  html += '<g id="placeholder" transform="translate(10.000000, 234.000000)">';
                                                      html += '<g id="Group">';
                                                          html += '<mask id="mask-2" fill="white">';
                                                              html +='<use xlink:href="#path-1"></use>';
                                                          html +='</mask>';
                                                          html +='<g id="Clip-2"></g>';
                                                          html +='<path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>';
                                                      html +='</g>';
                                                  html +='</g>';
                                              html +='</g>';
                                          html +='</g>';
                                      html +='</g>';
                                  html +='</g>';
                              html +='</g>';
                          html +='</svg>';
		html += '</div>';
		 html += '<div class="panel-container">';
		 if( location != '' && location != null ){
			html += '<p>'+location+'</p>';
		}
	     html += '<p class="m-t-10">';
	     html += (description) ? str_limit(description):"";
	     html += '</p>';
	     
       	html +='</div>';
       	html += '<div><button class="btn btn-primary btn-block m-t-20 room-type-button" data-hotel-name="'+name+'"  data-hotel-category="'+category+'" data-index="'+index+'" data-hotel="'+id+'" data-season="'+season_id+'" data-hotel-name="'+name+'" data-provider="'+provider+'" data-rooms="'+rooms+'" data-hotel-location="'+location+'">'; 
		html +='Room Types</button></div>';
		html += '</div></div>';
		html += '</div>';

		clean_name = name.toLowerCase().replace(/\s+/g, '').replace(/[^a-z0-9]/ig, '');
		clean_data = {
			id: id,
			name: clean_name,
			price: fromPriceString,
		};
		var appendedKey = isHotelAppended( clean_name );
		if( appendedKey === false && fromPriceSplit[1] > 0){
			
			if( is_active ){

				$('#hotel-list').prepend(html);

			}else{
				
				//this will filter hotel
				if( hotel.provider == 'eroam' && hotel.category == default_hotel_category_id ){//filter if provider is eroam
					
					if($('#hotel-list > div:first-child').hasClass( "selected-hotel" )){
						$('#hotel-list > div:first-child').after(html);
					}else{
						$('#hotel-list').prepend(html);
					}
				}else if( hotel.provider != 'eroam' && jQuery.inArray(hotel.category,hotel_stars) != -1 ){
					if($('#hotel-list > div:first-child').hasClass( "selected-hotel" )){
						$('#hotel-list > div:first-child').after(html);
					}else{
						$('#hotel-list').prepend(html);
					}
				}else{
					$('#hotel-list').append(html);
				}
			}
			totalHotels += 1;
			hotels_appended.push( clean_data );
			
		}else{

			var appendedData = hotels_appended[parseInt(appendedKey)];
			var appendedId = appendedData.id;

			if( !$('#hotel-list-'+appendedId).hasClass('selected-hotel') ){

				var appendedPrice = appendedData.price.split(' ');
				appendedPrice = parseInt(appendedPrice[1]);
				var currentPrice = is_active ? hotel.priceString.split(' ') : fromPriceString.split(' ');
				currentPrice = parseInt(currentPrice[1]);


				if(currentPrice < appendedPrice && currentPrice > 0){

					
					//$('#hotel-price-'+appendedId).html('<small> From Price Per Night <br> ( Per Person ) </small> <br>'+fromPriceString);
					$('#hotel-price-'+appendedId).html('From Price <strong>'+fromPriceString+'</strong> (Per Night Per Person)');
					$('#hotel-list-'+appendedId).attr('data-hotel-id', id)
												.attr('data-index', index)
												.attr('data-provider', provider);

					if(is_active){
						$('#hotel-list-'+appendedId).addClass('selectedHotel')
													.attr('data-price'. hotel.priceString)
													.attr('data-price-id', hotel.defaultPriceId)
													.attr('data-selected', 'true');

						//$('#hotel-price-'+appendedId).html('<small> From Price Per Night <br> ( Per Person ) </small> <br>'+hotel.priceString);
						$('#hotel-price-'+appendedId).html('From Price <strong>'+hotel.priceString+'</strong> (Per Night Per Person)');
					}
					$('#hotel-list-'+appendedId).find('button.room-type-button')
												.attr('data-hotel-name', name)
												.attr('data-hotel-category', category)
												.attr('data-index', index)
												.attr('data-hotel', id)
												.attr('data-season', season_id)
												.attr('data-provider', provider)
												.attr('data-rooms', rooms)
												.attr('data-hotel-location', location);

					$('#hotel-list-'+appendedId).find('a.view-more-btn')
												.attr('data-provider', provider)
												.attr('data-code', id);
					$('#hotel-list-'+appendedId).find('.hotel-name')
												.attr('data-hotel-id', id);

					$('#hotel-list-'+appendedId).attr('id', 'hotel-list-'+id);
					hotels_appended[parseInt(appendedKey)].id = id;
					hotels_appended[parseInt(appendedKey)].price = is_active ? hotel.priceString : fromPriceString;
				}
			}
		}
		setEqualHeight_CommonClass(".hotel-details-block .hotel-details .panel-container");
		setEqualHeight_CommonClass(".hotel-details-block .hotel-details");
		setEqualHeight_CommonClass(".hotel-block .hotel-bg");

	}

	// edited by miguel on 2017-01-18; cleaned data, stored in session and passed to function create_itinerary() 
	function appendRoomtypes(rooms)
	{	
		var price, roomName, html = '';
		switch( rooms.provider )
		{
			case 'eroam':
				price = rooms.price;
				convertedPrice = eroam.convertCurrency( price, rooms.currency );
				
				roomName = ucfirst( rooms.room_type );
				btnDataAttrHtml = [
					'data-provider="eroam"', 
					'data-id="'+rooms.hotel_id+'"',
					'data-hotel-id="'+rooms.hotel_id+'"', 
					'data-name="'+rooms.hotel_name+'"',
					'data-price="'+price+'"',
					'data-currency="'+rooms.currency+'"',
					'data-price-id="'+rooms.price_id+'"',
					'data-hotel-price-id="'+rooms.price_id+'"',
					'data-room-id="'+rooms.room_type_id+'"',
					'data-room-type-id="'+rooms.room_type_id+'"',
					'data-room-name="'+rooms.room_type+'"',
					'data-checkin="'+from_date+'"',
					'data-checkout="'+to_date+'"'
				].join('');
				break;

			case 'aot':
				price          = (rooms.price / 100);
				convertedPrice = eroam.convertCurrency( price, 'AUD' );
				roomName       = ucfirst( rooms.name );
				btnDataAttrHtml = [
					'data-provider="aot"', 
					'data-id="'+rooms.hotel_id+'"',
					'data-hotel-id="'+rooms.hotel_id+'"', 
					'data-name="'+rooms.hotel_name+'"',
					'data-price="'+price+'"',
					'data-currency="'+'AUD'+'"',
					'data-price-id="'+rooms.price_id+'"',
					'data-room-id="'+rooms.price_id+'"',
					'data-room-name="'+rooms.name+'"',
					'data-checkin="'+from_date+'"',
					'data-checkout="'+to_date+'"',
					'data-location="'+rooms.location+'"'
				].join('');
				break;

			case 'hb':
				price          = rooms.price;
				convertedPrice = eroam.convertCurrency( price, rooms.currency );
				roomName       = ucfirst( rooms.room_type.toLowerCase() );
				
				btnDataAttrHtml = [
					'data-provider="hb"', 
					'data-id="'+rooms.hotel_id+'"',
					'data-hotel-id="'+rooms.hotel_id+'"', 
					'data-name="'+rooms.hotel_name+'"',
					'data-price="'+price+'"',
					'data-currency="'+rooms.currency+'"',
					'data-price-id="'+rooms.room_id+'"',
					'data-room-id="'+rooms.room_id+'"',
					'data-room-name="'+roomName+'"',
					'data-room-type="'+rooms.room_type+'"',
					'data-checkin="'+from_date+'"',
					'data-checkout="'+to_date+'"',
					'data-location="'+rooms.location+'"',
					'data-description="'+rooms.description+'"',
					'data-images="'+rooms.images+'"',
					'data-cancellation_policy=""'
				].join('');
				break;	
			
			case 'expedia':
				price          = rooms.price;
				convertedPrice = eroam.convertCurrency( price, rooms.currency );

				//console.log('expediaRoomType '+rooms);
				roomName       = ucfirst( rooms.room_type.toLowerCase() );
				
				btnDataAttrHtml = [
					'data-provider="expedia"', 
					'data-id="'+rooms.hotel_id+'"',
					'data-hotel-id="'+rooms.hotel_id+'"', 
					'data-name="'+rooms.hotel_name+'"',
					'data-price="'+price+'"',
					'data-currency="'+rooms.currency+'"',
					'data-price-id="'+rooms.rate_code+'"',
					'data-room-id="'+rooms.room_id+'"',
					'data-room-name="'+roomName+'"',
					'data-room-type="'+rooms.room_type+'"',
					'data-checkin="'+from_date+'"',
					'data-checkout="'+to_date+'"',
					'data-location="'+rooms.location+'"',
					'data-description="'+rooms.description+'"',
					'data-images="'+rooms.images+'"',
					'data-cancellation_policy=""'
				].join('');
				break;	
			case 'ae':
				price          = rooms.price;
				convertedPrice = eroam.convertCurrency( price, rooms.currency );
				roomName       = ucfirst( rooms.room_type.toLowerCase() );
				console.log('ae room data', rooms);
				btnDataAttrHtml = [
					'data-provider="ae"', 
					'data-id="'+rooms.hotel_id+'"',
					'data-hotel-id="'+rooms.hotel_id+'"', 
					'data-name="'+rooms.hotel_name+'"',
					'data-price="'+price+'"',
					'data-currency="'+rooms.currency+'"',
					'data-price-id="'+rooms.room_id+'"',
					'data-room-id="'+rooms.room_id+'"',
					'data-room-name="'+roomName+'"',
					'data-room-type="'+rooms.room_type+'"',
					'data-checkin="'+from_date+'"',
					'data-checkout="'+to_date+'"',
					'data-location="'+rooms.location+'"',
					'data-description="'+rooms.description+'"',
					'data-images="'+rooms.images+'"',
					'data-group-identifier="'+rooms.group_identifier+'"',
					'data-meal-id="'+rooms.meal_id+'"',
					'data-cancellation_policy=""'
				].join('');
				break;	

		}
		if(rooms.provider == 'eroam' && rooms.room_type.includes("Dorm")){
			var convertedPricePerPerson =  parseFloat( convertedPrice );
		}else{
			var convertedPricePerPerson =  parseFloat( convertedPrice ) / parseInt('<?php echo e($data["travellers"]); ?>') ;
		}
		
		var priceString = globalCurrency + ' ' + Math.ceil( parseFloat(convertedPricePerPerson) ).toFixed(2);
		
		html += '<tr>';
		html += '<td>'+roomName+'</td>';
		html += '<td class="room-type-price" data-value="'+convertedPricePerPerson+'">'+priceString+'</td>';
		room_id = (rooms.room_id) ? rooms.room_id : rooms.room_type_id;
		
		if(rooms.hotel_id == default_hotel && rooms.provider == default_provider && room_id == default_room_id ){
			html += '<td><button class="btn btn-secondary btn-block bold-txt selected-room" '+btnDataAttrHtml+' disabled>Selected</button></td>';
		}else{
			html += '<td><button class="btn btn-primary btn-block bold-txt selected-room" '+btnDataAttrHtml+'>Select</button></td>';
		}
		
		html += '</tr>';

		$('.room_prices').append(html);
	}


	/* ADDED BY CALOY - UTILITIES  added by jayson for HOTELBEDS*/
	function validate_hotels(array, data, type)
	{
		// edited by miguel on 2016-11-15 to accommodate AE api results for validation; 
		var found  = false;
		var result = false;
		if( array.length > 0 )
		{
			found = array.some(function (value, key) {
				var hotel_name_in_array = value.name.toLowerCase().replace(/s+/g, '').replace(/[^a-z0-9]/ig, '');
				var hotel_name_to_add   = '';
				var validate_latlng     = false;
				var validate_name       = false;
				switch( type )
				{
					case 'hb':
						hotel_name_to_add = data.name.toLowerCase().replace(/s+/g, '').replace(/[^a-z0-9]/ig, '');
						validate_latlng   = (data.lat.toString().indexOf(value.lat.toString()) !== -1 && data.lng.toString().indexOf(value.lng.toString()) !== -1) || (value.lat.toString().indexOf(data.lat.toString()) !== -1 && value.lng.toString().indexOf(data.lng.toString()) !== -1) ? true : false;
						validate_name     = (hotel_name_in_array.toString().indexOf(hotel_name_to_add.toString()) !== -1 || hotel_name_to_add.toString().indexOf(hotel_name_in_array.toString()) !== -1) ? true : false;
						break;
					case 'ae':
						try
						{
							hotel_name_to_add = data.name.toLowerCase().replace(/s+/g, '').replace(/[^a-z0-9]/ig, '');

							// check if latitude and longitude already exists in hotel_array
							if( ( data.lat.toString().indexOf(value.lat.toString()) !== -1 && data.lng.toString().indexOf(value.lng.toString()) !== -1 ) ||
								( value.lat.toString().indexOf(data.lat.toString()) !== -1 && value.lng.toString().indexOf(data.lng.toString()) !== -1 )
							){ 
								validate_latlng = true;
							}
							// check if name already exists in hotel_array
							if( hotel_name_in_array.toString().indexOf(hotel_name_to_add.toString()) !== -1 || hotel_name_to_add.toString().indexOf(hotel_name_in_array.toString()) !== -1 ){
								validate_name = true;
							}					
						}
						catch( err )
						{
							// console.log('!--- Hotel validation error on '+type+' api with hotel_id:'+value.hotel_id+'; error_essage:'+err.message);
						}
						break;
				}			

				if( validate_name === true || validate_latlng === true )
					return true;
				else
					return false;
			});
		}

		if(found)
			result = true;
		return result;
	}

	function formatDate(date, time = false) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	    if(time){
	    	day += ' '+ d.getHours();
	    }
	    return [year, month, day].join( '-' );
	}

	/*	Number.prototype.between  = function (a, b) {
	    var min = Math.min.apply(Math, [a,b]),
	        max = Math.max.apply(Math, [a,b]);
	    return this > min && this < max;
	};*/

	function buildHotels(){
		var tasks = [
			buildEroamHotels,
			<?php if(api_is_active('hotelbeds')): ?>
				buildHotelBedsHotels,
			<?php endif; ?>
			<?php if(api_is_active('aot')): ?>
				buildAotHotels,
			<?php endif; ?>

			/* Added By Rekha Patel - 3rd Oct 2017 - Get Hotels list using Expedia API */
			<?php if(api_is_active('expedia')): ?>
                //buildExpediaHotels,
			<?php endif; ?>

			checkHotelsCount,
			<?php if(api_is_active('Arabian Explorers')): ?>
				buildAEHotels,
			<?php endif; ?>
			buildSuburb,
			rangeSlider
		];

		$.each(tasks, function(index, value) {
			$(document).queue('tasks', processTask(value));
		});

		$(document).queue('tasks');
		$(document).dequeue('tasks');
	}

	function processTask(fn){
		return function(next){
			doTask(fn, next);
		}
	}

	function doTask(fn, next){
		fn(next);
	}

	function buildSuburb(next){	
		$('#suburbs-list').show();
		next();
	}

	function buildEroamHotels(next){
		eroam_data = {
			city_ids: [city_id],
			date_from: formatDate(from_date),
			date_to: formatDate(to_date),
			traveller_number:<?php echo e($data['travellers']); ?>,
			hotel_category_id: hotel_filter,
			//hotel_category_id: [<?php echo e(join(', ', $data['hotel_category_id'])); ?> ]
			room_type_id:room_type_id,
		};
		console.log('eroam_data', eroam_data);
		var eroamApiCall = eroam.apiDeferred('city/hotel/v2', 'POST', eroam_data, 'eroam', true);
		eroam.apiPromiseHandler( eroamApiCall, function( eroamResponse ){
			console.log('eroamResponse', eroamResponse);
			
			if( eroamResponse.length > 0 ){
				/*
				| Loop Eroam hotel response
				*/
				var hidden = [];
				eroamResponse.forEach(function( eroamHot, eroamHotIndex ){

				    console.log(eroamHot);
					var price_season_id;
					eroamHot.prices.forEach(function(price, index){
						/*
						| filter price by season
						*/
						
						if( date_between(eroam_data.date_from, price.season.from, price.season.to) ){
							price_season_id = price.season.id;
							var convertedPrice = eroam.convertCurrency( price.price, price.season.currency.code)
							//maxPrice = parseInt(convertedPrice) > maxPrice ? parseInt(convertedPrice) : maxPrice;
							//hidden.push('<input type="hidden" class="hidden-room-price" value="'+convertedPrice+'">');
						}else{
							return;
						}
					});
					/*
					| filter hotels without room types
					| Some hotels displayed without room types because room prices are filtered by season
					*/

					/*if ( typeof price_season_id == 'undefined') {
						//return;
					}else{*/

						eroamHot.provider   = 'eroam';
						eroamHot.season_id  = price_season_id;
						eroamHot.image      = eroamHot.image.length > 0 ? eroamHot.image[0].small : 'uploads/hotels/no-image.png';
						eroamHot.index      = eroamHotIndex;
						is_selected         = 'false';
						is_active           = '';
						selected_attributes = '';
						if( eroamHot.provider == default_provider )
						{   
							if( eroamHot.id == default_hotel )
							{	
								is_selected          = 'true';
								is_active            = ' selected-hotel';

								if( default_room_name.includes('Dorm') ){
									var converted_price  = eroam.convertCurrency( default_room_price_raw, eroamHot.currency.code);
									eroamHot.priceString = globalCurrency + ' ' + Math.ceil( converted_price ).toFixed(2); 
								}else{
									var converted_price  = eroam.convertCurrency( default_room_price_raw, eroamHot.currency.code) / number_of_travellers;
									eroamHot.priceString = globalCurrency + ' ' + Math.ceil( converted_price ).toFixed(2); 
								}
								
								selected_attributes  = 'data-price="'+eroamHot.priceString+'" data-price-id="'+default_price_id+'"';
								eroamHot.defaultPriceId = default_price_id;
								eroamHot.roomName = default_room_name;
							} 
						}
					
						eroamHot.hidden = hidden;
						eroamHot.is_active = is_active;
						eroamHot.is_selected = is_selected;
						eroamHot.attributes = ' data-provider="eroam" data-index="'+eroamHotIndex+'" data-city-id="'+eroamHot.city_id+'" data-hotel-id="'+eroamHot.id+'" data-hotel-suburb-id="'+eroamHot.suburb_id+'" data-is-selected="'+eroamHot.is_selected+'" '+selected_attributes;
						try{
							appendHotels( eroamHot );
						}catch( e ){
							console.log( "error eraom hotel append : ", eroamHot.id , e.message );
						}
						
					//}
				});	
			}

			next();
		});
	}

	function buildHotelBedsHotels(next){
		var destinationCodes = getHBDestinationCodes( search_session.itinerary[ leg ].city.hb_destinations_mapped );
		if( destinationCodes.length > 0 ) // check if hb_destination_code exists for this city
		{
			destinationCodes.forEach(function(code, codeKey){
				hb_data = {
					city_ids: city_id,
					check_in: from_date,
					check_out: to_date,
					room:'1',
					adult:'<?php echo e($data["travellers"]); ?>',//number of traveller
					code:code,
					child:'0',
					provider: 'hb'
				};
				console.log('hbRequest', hb_data);

				// CACHING HOTEL BEDS
				var hbApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', hb_data, 'hb', true);

				eroam.apiPromiseHandler( hbApiCall, function( hbResponse ){
					console.log('hbResponse', hbResponse);
					if( hbResponse.array.length > 0 ){
					
						$.each( hbResponse.array, function( key, value ) {
							var roomsAreAvailable = false; // added by miguel to filter out hotels with no rooms
							if(value.rooms.length > 0)
							{
								$.each(value.rooms, function(roomIndex, roomValue){
									if (typeof roomValue.rates[0].net != 'undefined')
									{
										roomPrice = parseInt(eroam.convertCurrency(roomValue.rates[0].net, 'AUD'));
										//maxPrice =  roomPrice > maxPrice ? roomPrice : maxPrice;
										roomsAreAvailable = true;
									}
								});
							}
							if( roomsAreAvailable ) // added by miguel to filter out hotels with no rooms
							{
								value.provider      = 'hb';
								is_selected         = 'false';
								is_active           = '';
								selected_attributes = '';
								if( value.provider == default_provider )
								{
									if( value.code == default_hotel )
									{
										is_selected         = 'true';
										is_active           = 'selected-hotel';
										var converted_price = eroam.convertCurrency( default_room_price, default_currency);
										value.priceString   = globalCurrency + ' ' + Math.ceil( converted_price ).toFixed(2); 
										selected_attributes = 'data-price="'+value.priceString+'" data-price-id="'+default_price_id+'"';
										value.defaultPriceId = default_price_id;
										value.roomName = default_room_name;
									}
								}
						
								value.is_active   = is_active;
								value.is_selected = is_selected;
								value.attributes  = ' data-provider="hb" data-index="'+key+'" data-city-id="'+city_id+'"  data-hotel-zone-id="'+value.zoneCode+'" data-is-selected="'+value.is_selected+'" '+selected_attributes;
								

								try{
									appendHotels( value );
								}catch( e ){
									console.log("hb append error : ", e.message);
								}
							}

						});
						next();
					}else{
						next();
					}
				});

			});
		}
		else
		{
			next();
		}
		
	}

    function buildExpediaHotels(next){
    	alert();
        var location = ( search_session.itinerary[ leg ].city);

		expedia_data = {
            city: location.name,
			countryCode: location.country.code,
            arrivalDate: from_date,
            departureDate: to_date,
            numberOfAdults:'<?php echo e($data["travellers"]); ?>',//number of traveller
            provider: 'expedia'
        };
        console.log('expediaRequest', expedia_data);

        /*eroam.ajax('post', 'expedia/gethotels', expedia_data, function(response) {
        	console.log('expediaResponse', response);
        });*/

        // CACHING HOTEL BEDS
        var expediaApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', expedia_data, 'expedia', true);
        eroam.apiPromiseHandler( expediaApiCall, function( expediaResponse ){
            console.log('expediaResponse', expediaResponse);

            if( expediaResponse.data.length > 0 ){
                $.each( expediaResponse.data, function( key, value ) { 

                    var roomsAreAvailable = false; // added by miguel to filter out hotels with no rooms
                    if(value.rooms.length > 0)
                    { 
                        $.each(value.rooms, function(roomIndex, roomValue){
                            if (typeof roomValue.rate != 'undefined')
                            {
                                roomPrice = parseInt(eroam.convertCurrency(roomValue.rate, 'AUD'));
                                //maxPrice =  roomPrice > maxPrice ? roomPrice : maxPrice;
                                roomsAreAvailable = true;
                            }
                        });
                    }

                    if( roomsAreAvailable ) // added by miguel to filter out hotels with no rooms
                    {

                        value.provider      = 'expedia';
                        is_selected         = 'false';
                        is_active           = '';
                        selected_attributes = '';
                        if( value.provider == default_provider )
                        {
                            if( value.code == default_hotel )
                            {
                                is_selected         = 'true';
                                is_active           = 'selected-hotel';
                                var converted_price = eroam.convertCurrency( default_room_price, default_currency);
                                value.priceString   = globalCurrency + ' ' + Math.ceil( converted_price ).toFixed(2);
                                selected_attributes = 'data-price="'+value.priceString+'" data-price-id="'+default_price_id+'"';
                                value.defaultPriceId = default_price_id;
                                value.roomName = default_room_name;
                            }
                        }

                        value.is_active   = is_active;
                        value.is_selected = is_selected;
                        value.attributes  = ' data-provider="expedia" data-index="'+key+'" data-city-id="'+city_id+'"  data-hotel-id="'+value.hotelId+'" data-is-selected="'+value.is_selected+'" '+selected_attributes;

                        try{
                            appendHotels( value );
                        }catch( e ){
                            console.log("expedia append error : ", e.message);
                        }
                    }

                });
                next();
            }else{
                next();
            }
        });
    }

	function buildAEHotels(next){

		search_session.itinerary[ leg ].city.ae_city_mapped.forEach(function( mapped )
		{

			var ae_data = {
				FromDate : from_date,
				ToDate   : to_date,
				Adults   : '<?php echo e($data["travellers"]); ?>',
				RegionId : mapped.ae_city.RegionId,
				provider : 'ae',
			};	
			var aeApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', ae_data, 'ae', true);
			eroam.apiPromiseHandler( aeApiCall, function( aeResponse ){
				console.log('aeResponse', aeResponse);

				if(aeResponse!== null && aeResponse.length > 0 ){
						
					$.each( aeResponse, function( key, value ) {
				
						if(value.rooms.length > 0)
						{
							$.each(value.rooms, function(roomIndex, roomValue){
								if (typeof roomValue.price != 'undefined')
								{
									roomPrice = parseInt(eroam.convertCurrency(roomValue.price, roomValue.currency));
									//maxPrice =  roomPrice > maxPrice ? roomPrice : maxPrice;
									roomsAreAvailable = true;
								}
								
							});

						}
						if( roomsAreAvailable ) // 
						{
							value.provider      = 'ae';
							is_selected         = 'false';
							is_active           = '';
							selected_attributes = '';

							if( value.provider == default_provider )
							{
								if( value.hotel_id == default_hotel )
								{
									is_selected         = 'true';
									is_active           = 'selected-hotel';
									var converted_price = eroam.convertCurrency( default_room_price, default_currency);
									value.priceString   = globalCurrency + ' ' + converted_price; 
									selected_attributes = 'data-price="'+value.priceString+'" data-price-id="'+default_price_id+'"';
									value.defaultPriceId = default_price_id;
									value.roomName = default_room_name;
								}
							}
							
							value.is_active   = is_active;
							value.is_selected = is_selected;
							value.attributes  = ' data-provider="ae" data-index="'+key+'" data-city-id="'+city_id+'" data-hotel-id="'+value.hotel_id+'" data-is-selected="'+value.is_selected+'" '+selected_attributes;
							try{
								appendHotels( value );
							}catch( e ){
								console.log("ae append error : ", e.message);
							}
						}

					});
					next();
				}else{
					next();
				}
			});

		});

	}

	function buildAotHotels(next){
		var traveller_number = "<?php echo e($data['travellers']); ?>";
		var aot_room_type = 'single';
		switch( traveller_number ){
			case '1':
				aot_room_type = 'single';
				break;
			case '2':
				aot_room_type = 'double';
				break;
			case  '3':
				aot_room_type = 'triple';
				break;
		}
		aot_data = {
			type : 'q_supplier',
			city_id : city_id,
			room_type : aot_room_type,
			provider: 'aot'
		};
		//jayson  update this after demo
		if (search_session.itinerary[leg].hotel != null) {
			var aotApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', aot_data, 'aot', true);
			eroam.apiPromiseHandler( aotApiCall, function( aotReponse ){
				console.log('aotReponse', aotReponse);
				if( aotReponse.array !== undefined && aotReponse.array.length > 0 ){
					
					/*
					| Loop hotel response
					*/
					if( window.aotHotelsArray === undefined ) window.aotHotelsArray = [];
					window.aotHotelsArray = aotReponse.array;
					if( window.aotHotels === undefined ) window.aotHotels = {};
					aotReponse.array.forEach(function( aotHotel, aotHotelIndex ){

						aotHotel.provider = 'aot';
						aotHotel.season_id = 0; //since there's no need for seasons in AOT
						aotHotel.index = aotHotelIndex;
						
						var code = aotHotel.code;
						if( window.aotHotels === undefined ) window.aotHotels = {};
						window.aotHotels[code] = aotHotel;
						is_selected = 'false';
						is_active = '';
						selected_attributes = '';
						if( aotHotel.provider == default_provider )
						{
							if( aotHotel.code == default_hotel )
							{
								is_selected          = 'true';
								is_active            = ' selected-hotel';
								var converted_price  = eroam.convertCurrency( default_room_price, 'AUD');
								aotHotel.priceString = globalCurrency + ' ' + converted_price; 
								selected_attributes  = 'data-price="'+aotHotel.priceString+'" data-price-id="'+default_price_id+'"';
								aotHotel.defaultPriceId = default_price_id;
								aotHotel.roomName = default_room_name;
							}
						}

						aotHotel.is_active = is_active;
						aotHotel.is_selected = is_selected;
						aotHotel.attributes = ' data-provider="aot" data-index="'+aotHotelIndex+'" data-city-id="'+city_id+'" data-hotel-id="'+aotHotel.code+'" data-is-selected="'+aotHotel.is_selected+'" '+selected_attributes;
						
console.log(aotHotel);
						try{
							appendHotels( aotHotel );
						}catch( e ){
							console.log("aot append error : ", e.message);
						}
					});
					next();
				}else{
					next();
				}

			});
		}
		else
		{
			next();
		}

		
	}

	function checked(checkbox, value){
		$('.toggle-check').find('i').removeClass('black-background');
		checkbox.toggleClass('black-background');
		var text = value > 1 ? value+' Nights' : value+' Night';
		$('#number_of_nights').html(text +'<span class="caret"></span>'); 
	}

	function checkHotelsCount( next ){
		$( '#hotel-loader' ).fadeOut(300);
		totalHotels = parseInt(totalHotels);
		$('.hotel-count').html(': ' + totalHotels + ( ( totalHotels == 1 ) ? ' Hotel' : ' Hotels' )+ ' Found');
		if( totalHotels == 0 ){
			$('#suburbs-list').hide();
			var html = '<h4 class="text-center blue-txt bold-txt">No Hotels Found.</h4>';
			$('#hotel-list').append( html );
		}
		next();
	}

	function isHotelAppended( name ){
		var keyContainer;
		found = hotels_appended.some(function (value, key) {
			
			if( value.name == name ){
				keyContainer = key;
				return key;
			}else{
				return false;
			}
		});
		return found ? keyContainer : found ;
	}


	function getHotelFromPrice( hotel ){
		var stringFromPrice = '';
		var lowest_price_room_type = '';
		var converted_price = 0.00;
		switch( hotel.provider ){
			case 'eroam' :
					
					rooms = jQuery.parseJSON( eroamRooms[hotel.id] );
					if(rooms.length > 0){
						var lowest_price = 0;
						rooms.map( function( value,key ){
								var value_price = parseFloat( value.price );
								// lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
								// lowest_price_room_type = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value.room_type.name : lowest_price_room_type;
								console.log('eroam names', value);
								if(value.hotel_season_id == hotel.season_id ){
									if( lowest_price == 0 ){
										lowest_price = value_price;
										lowest_price_room_type = value.room_type.name;
									}else{
										if( lowest_price > value_price ){
											lowest_price = value_price;
											lowest_price_room_type = value.room_type.name;
										}
									}
								}
								
						});
					}
				if( lowest_price > 0 ){
					
					if(! lowest_price_room_type.includes('Dorm') ){
						lowest_price = lowest_price / number_of_travellers;
					}
					
					converted_price  = eroam.convertCurrency( lowest_price, hotel.currency.code);
				}
				break;
			case 'aot' :
				break;
			case 'hb' :
				if( hotel.rooms.length > 0 ){
					var lowest_price = 0;
					hotel.rooms.map( function( value,key ){
							if (typeof value.rates[0].net != 'undefined'){
								console.log('hbroom', value);
								var value_price = parseFloat( value.rates[0].net );
								//lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
								//lowest_price_room_type = 
								if( lowest_price == 0 ){
									lowest_price = value_price;
									lowest_price_room_type = value.name;
								}else{
									if( lowest_price > value_price ){
										lowest_price = value_price;
										lowest_price_room_type = value.name;
									}else{
										lowest_price = lowest_price;
										lowest_price_room_type = lowest_price_room_type;
									}
								}

							}
					} )					
				}
				if( lowest_price > 0 ){
					lowest_price = lowest_price / number_of_travellers;
					converted_price  = eroam.convertCurrency( lowest_price, 'AUD');
				}
				break;

            case 'expedia' :
                if( hotel.rooms.length > 0 ){
                    var lowest_price = 0;
                    hotel.rooms.map( function( value,key ){

                        if (typeof value.rate != 'undefined'){
                            console.log('expediaroom', value);
                            var value_price = parseFloat( value.rate );
                            //lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
                            //lowest_price_room_type =
                            if( lowest_price == 0 ){
                                lowest_price = value_price;
                                lowest_price_room_type = value.roomDescription;
                            }else{
                                if( lowest_price > value_price ){
                                    lowest_price = value_price;
                                    lowest_price_room_type = value.roomDescription;
                                }else{
                                    lowest_price = lowest_price;
                                    lowest_price_room_type = lowest_price_room_type;
                                }
                            }

                        }
                    } )
                }
                if( lowest_price > 0 ){
                    lowest_price = lowest_price / number_of_travellers;
                    converted_price  = eroam.convertCurrency( lowest_price, 'AUD');
                }
                break;

			case 'ae':
				if( hotel.rooms.length > 0 ){
					var lowest_price = 0;
					hotel.rooms.map( function( value,key ){
							if (typeof value.price != 'undefined'){
								var value_price = parseFloat( value.price );
								//lowest_price = (lowest_price == 0) ? value_price : (lowest_price > value_price) ? value_price : lowest_price;
								if( lowest_price == 0 ){
									lowest_price = value_price;
									lowest_price_room_type = value.name;
								}else{
									if( lowest_price > value_price ){
										lowest_price = value_price;
										lowest_price_room_type = value.name;
									}else{
										lowest_price = lowest_price;
										lowest_price_room_type = lowest_price_room_type;
									}
								}
							}
					} );
				}
				if( lowest_price > 0 ){
					lowest_price = lowest_price / number_of_travellers;
					converted_price  = eroam.convertCurrency( lowest_price, 'AUD');
				}
				break;
		}

		stringFromPrice = globalCurrency + ' ' + Math.ceil( converted_price ).toFixed(2); 
		var roomData = {
			fromPrice: stringFromPrice,
			roomName: lowest_price_room_type
		}
		//return stringFromPrice;
		return roomData;
	}
	function sortByDate(activities){
	    var sorted = activities.sort(function (itemA, itemB) {
			var A = itemA.date_selected;
			var B = itemB.date_selected;
		    return A.localeCompare(B);
	    });
	   return sorted;
	}
	function numberRange(start, count) {
		return Array.apply(0, Array(count))
		.map(function (value, key) { 
			return key + start;
		});
	}
	function addDays(date, days) {
		var result = new Date(date);
		result.setDate(result.getDate() + parseInt(days) );
		return formatDate(result);
	}
	function deductDays(date, days) {
		var result = new Date(date);
		result.setDate(result.getDate() - parseInt(days) );
		return formatDate(result);
	}
	function formatTime(time){
		return time.replace(/hr\(s\)|min\(s\)|hours|minutes /gi, function(x){
			return x == 'hr(s)' || 'hours' ? ':' : '';
		});
	}

	function removePlus(time){
		return time.replace( /\+/g, '' );
	}
	function get_hours_min(hour_min){
		var hours_mins = hour_min.toString().split(".");
		var hours = parseInt(hours_mins[0]);
		var mins = parseFloat('0.'+hours_mins[1]) * 60;

		return hours+' hr(s) '+Math.ceil(mins)+' min(s)';
	}
	function arrival_am_pm(eta){

		var result;
		var arrival_split = eta.split("+"); 
		var arrival_split_am_pm = arrival_split[0].split(" ");
		var arrival_split_hour_min = arrival_split[0].split(":");
		var arrival_hour = parseInt(arrival_split_hour_min[0]);
		var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


		if(typeof arrival_split_am_pm[1] !== 'undefined'){

			result = padd_zero(arrival_hour)+':'+arrival_min+' '+arrival_split_am_pm[1];
		}else{
			if( arrival_hour < 12 ){
				result = padd_zero(arrival_hour)+':'+arrival_min+' AM';
			}else{
				hour = arrival_hour - 12;
				result = padd_zero(hour)+':'+arrival_min+' PM';
			}
		}
		
		return result;
	}
	function padd_zero(number) {
		if(parseInt(number) == 0){
			number = 12;
		}
	    return (number < 10) ? ("0" + number) : number;
	}
	function getDayTime(arrivalDate, duration) {
		var hours_mins = duration.split(':');
		var result = new Date(arrivalDate);
		result.setHours(result.getHours() - parseInt(hours_mins[0]));
		result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));

		return formatDate(result, true);
	}

	function activityCancellation( activityDates, itineraryIndex, act, checkbox, value ){
		
		if( activityDates.length > 0 ){

			eroam.confirm( null, 'Decreasing the number of nights can cause schedule conflicts for the activities on this city. Would you like to cancel some activities for this city?', function(e){
				
				if( act.length > 0){
					for( index = act.length-1; index >= 0; index-- ){
						var actDuration = parseInt(act[index].duration);
						var multiDayRemoved = false;
						if(actDuration > 1){
							actDuration = actDuration - 1;
							var activityDateSelected = act[index].date_selected;
							for( var counter = 1;counter <= actDuration; counter ++ ){
								var multiDayActivityDate = addDays( activityDateSelected, counter );

								if( activityDates.indexOf( multiDayActivityDate ) > -1 && !multiDayRemoved ){
									act.splice(index, 1);
									multiDayRemoved = true;
								}
							}
						}else{
							if( activityDates.indexOf( act[index].date_selected ) > -1 ){
								act.splice(index, 1);
							}
						}
						
					}
				}
				
				search_session.itinerary[itineraryIndex].activities = act;
				bookingSummary.update( JSON.stringify( search_session ) );
				checked(checkbox, value);
				
			} );

		}else{
			bookingSummary.update( JSON.stringify( search_session ) );
			checked(checkbox, value);
		}

	}

	function getDeparture(itineraryIndex){

		var newDateTo = addDays(from_date, parseInt(search_session.itinerary[itineraryIndex].city.default_nights)); 
		var transport = search_session.itinerary[itineraryIndex].transport;

		var departureDate = to_date+' 24';
		if(transport != null){
			var eta = transport.eta;
			var  travelDuration =  transport.duration;
			if(transport.provider == 'mystifly'){
			
				travelDuration = formatTime(get_hours_min(travelDuration));
				eta = moment( eta, moment.ISO_8601 );
				eta = eta.format('hh:mm A');

			}else{
				travelDuration = removePlus(travelDuration);
				travelDuration = formatTime(travelDuration);
			}

			var arrivalTime = arrival_am_pm(eta);
			departureDate = getDayTime(newDateTo+' '+arrivalTime, travelDuration);
		}

		return departureDate;
	}

	function getActivityToBeCancelledByDates( dateTo, numberOfDays ){
		var dateArray = [];
		if( numberOfDays ){
			dateArray.push(dateTo);
			for( var count = 1;count <= numberOfDays; count ++ ){
				dateArray.push( deductDays( dateTo, count) )
			}
		}
		return dateArray;
	}

	function getHBDestinationCodes( hb_destinations_mapped )
	{
		result = [];
		if( hb_destinations_mapped != null )
		{
			hb_destinations_mapped.forEach(function( value, key ){
				result.push( value.hb_destination.destination_code.trim() );
			});
		}
		return result;
	}

	/*
	| Added by Junfel
	| jquery Ui range slider
	*/
	function rangeSlider(next){
		var min = parseInt( eroam.convertCurrency(0, 'AUD') );
		var max = parseInt( maxPrice );

		$( '#slider-range' ).slider({
			range: true,
			min: min,
			max: max,
			values: [ min, max ],
			slide: function( event, ui ) {
				$( '#price-from' ).val(ui.values[ 0 ]);
				$( '#price-to' ).val(ui.values[ 1 ]);
			}
		});
		//$( '#price-from' ).val($( '#slider-range' ).slider( 'values', 0 ));
		//$( '#price-to' ).val($( '#slider-range' ).slider( 'values', 1 ));

		$( '#price-from' ).val(0);
		$( '#price-to' ).val(5000);

		$('.curr').text(globalCurrency);

		next();
	}

	function getHotelCategory(code, provider = ''){
		var stars;
		switch( provider ){
			case 'hb':
				switch( code ){
					case '5EST':
					case 'HS5':
					case 'APTH5':
					case '5LUX':
					case '5LL':
						stars = getStars( 5 );
					break;
					case '4EST':
					case 'HS4':
					case 'APTH4':
					case '4LUX':
					case '4LL':
						stars = getStars( 4 );
					break;
					case '3EST':
					case 'HS3':
					case 'APTH3':
					case '3LUX':
					case '3LL':
						stars = getStars( 3 );
					break;
					case '2EST':
					case 'HS2':
					case 'APTH2':
					case '2LUX':
					case '2LL':
						stars = getStars( 2 );
					break;
					case '1EST':
					case 'HS1':
					case 'APTH1':
					case '1LUX':
					case '1LL':
						stars = getStars( 1 );
					break;
					case 'H4_5':
						stars = getStars( 4, true );
					break;
					case 'H3_5':
						stars = getStars( 3, true );
					break;
					case 'H2_5':
						stars = getStars( 2, true );
					break;
					case 'H1_5':
						stars = getStars( 1, true );
					break;
					default:
						stars = getStars( 0 ); 
					break;
					
				}
			break;
			case 'eroam':
				stars = getStars(code);
			break;
			case 'ae':
			break;
			case 'aot':
			break;
			case 'expedia':
				if(code % 1 === 0){
					stars = getStars( code);
				}else {
					stars = getStars( code, true );
				}
			break;
		}
		return stars;
	}

	function getStars(count, half = false){
		var stars = '';
		if( parseInt( count ) ){
			for( star = 1; star <= count; star ++ ){
				stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
			}
			var emptyStars = 5 - parseInt( count );
			if(half){
				stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
				emptyStars = emptyStars - 1;
			}
			for( empty = 1; empty <= emptyStars; empty ++ ){
				stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
			}
		}else{
			//for( star = 1; star <= 5; star ++ ){
				stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
			//}
		}
		return stars;
	}

	function getAERegionIds( AECitiesMapped )
	{
		var results = [];
		AECitiesMapped.forEach(function( AEMappedCity ){
			results.push( AEMappedCity.ae_city.RegionId );
		});
		return results;
	}


</script>
	
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>