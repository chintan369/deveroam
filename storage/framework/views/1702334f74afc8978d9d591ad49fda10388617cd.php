<?php $__env->startSection('custom-css'); ?>
<style>
	.wrapper {
		background-image: url( <?php echo e(url( 'assets/img/bg1.jpg' )); ?> ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

	.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	.text-field{
	    border: 1px solid #000;
	    padding: 11px;
	    width: 100%;
	    background: #fff;
	    border-radius: 2px;
	}
	.blue-button{
		transition: all 0.5s;
		background: #2AA9DF;
		border-color: #2AA9DF;
		color: #fff;
		padding: 11px;
		font-weight: bold;
		border-radius: 2px;
		width: 100%;
	}
	.blue-button:hover{
		background: #BEE5F6;
		color: #2AA9DF;
		border-color: #2AA9DF;
	}
	.has-error .text-field, .has-error .select-style {
	    border-color: #a94442;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	}
	.select-style {
	  
	    border: 1px solid #000;
	    padding: 11px;
	    width: 100%;
	    background: #fff;
	}
	.radius {
	    border-radius: 2px !important;
	}
	.form-control{
		border-radius: 0;
		/* padding-left: 35px; */
	}
	#user-register{
		padding: 5rem;
	}
	.error-msg {
		display: block;
		margin-bottom: 28px;
		color: #FFFFFF;
		background: #FF3D28;
		padding: 7px;
		transition: all 0.3s;
	}

	/* FB LOGIN */
	#fb-login-btn,
	#sign-up-fb-btn {

		outline: 0;
		border: 0;
		background: #3b5998;
		color: #f5f5f5 !important;
		display:none;
		/*display: block;*/
		width: 100%;
		padding: 15px;
		margin: 0 auto;
		text-decoration: none !important;
	}
	#fb-login-btn:disabled {
		opacity: 0.6;
	}
	#fb-login-btn img,
	#sign-up-fb-btn img {
		width: 30px;
		height: 30px;
		margin-left: 10px;
	}
	.form-group {
		margin-bottom: 20px;
		position: relative;
	}
	.form-group i  {
		position: absolute;
		top: 10px;
		left: 13px;
		font-size: 15px;
		color: #5c5c5c;
	}
	.form-control {
		border-radius: 0;
		padding-left: 35px;
	}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <section class="banner-container">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class="first-slide" src="<?php echo e(url( 'assets/images/banner1.jpg' )); ?>" alt="First slide">
                </div>
                <div class="item">
                    <img class="third-slide" src="<?php echo e(url( 'assets/images/banner2.jpg' )); ?>" alt="Third slide">
                </div>
            </div>
            <div class="banner-search">
                <div class="banner-inner">
                    <p><img src="<?php echo e(url( 'assets/images/eRoam_Logo.png' )); ?>" alt="eroam" class="carousel-logo"></p>
                    <p>Over 500,000 hotels, 1,000 arlines, plus 100,000’s events, activities &amp; restaurants.</p>

                    <form method="post" action="search" id="search-form" class="form-horizontal clearfix">


                        <div class="col-md-10 col-md-offset-1" >
                            <div class="searchbox-new search-box1" style="display:none;">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 padding-right-0 banner-searchbox-new">
                                        <input type="text" name="project" id="project" class="form-control" placeholder="Where do you want to go?">
                                            
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12 padding-left-0">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="searchbox-new search-box2">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="text" value="" name="start_location1" id="start_location1" class="form-control" placeholder="Start Location">
                                    </div>
                                    <div class="col-sm-3">
                                        <input id="travellers1" value="" type="number" name="travellers1" class="form-control" placeholder="No. of Travellers" min="1" max="10" />
                                    </div>
                                    <div class="col-sm-3">
                                        <input id="start_date1" value="" name="start_date1" type="text" class="form-control" placeholder="Start Date" >
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="searchbox-new search-box3" style="display:none;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <input type="text" value="" name="start_location2" id="start_location2" class="form-control" placeholder="Start Location">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" value="" name="end_location2" id="end_location2" class="form-control" placeholder="End Location">
                                    </div>
                                    <div class="col-sm-2">
                                        <input id="travellers2" value="" type="number" name="travellers2" class="form-control" placeholder="No. of Travellers" min="1" max="10" />
                                    </div>
                                    <div class="col-sm-2">
                                        <input id="start_date2" value="" name="start_date2" type="text" class="form-control" placeholder="Start Date" >
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-9 col-xs-12 padding-0">
                                    <div class="checkbox-group">
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-01"><input type="radio" name="option" id="checkbox-01" value="packages" disabled >Tour Packages</label>
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-02"><input type="radio" name="option" id="checkbox-02" value="manual" checked>Tailormade Manual</label>
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-03"><input type="radio" name="option" id="checkbox-03" value="auto">Tailormade Auto</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-12 padding-0" style="display: none;">
                                    <p class="advance-right"><a href="#" class="advance-text">More Options <b class="caret"></b></a></p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
        </div>
    </section>

    <section class="content-wrapper">
        <h1 class="hide"></h1>
        <article class="places-section eroam-trending">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2>TRENDING HOLIDAYS</h2>
                        <div class="places-list">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img1.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img2.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img3.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img4.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img5.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img1.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img3.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 m-t-20">
                                    <div class="place-wrapper">
                                        <div class="place-image">
                                            <img src="<?php echo e(url( 'assets/images/trending-img5.jpg' )); ?>" alt="" class="img-responsive">
                                        </div>
                                        <div class="place-inner">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <p><strong>00 Days</strong></p>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <ul class="rating">
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="place-details">
                                        <p>Name of Trending Itinerary From $ 000.00 Per Person</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
	<section class="inner-page content-wrapper">
		<h1 class="hide"></h1>
		<div class="map-wrapper">
			<div id="map"></div>
			<div class="mapIcons-wrapper">
				<p class="map-icon"><a href="#">
						<svg width="21px" height="24px" viewBox="0 0 21 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
							<desc>Created with Sketch.</desc>
							<defs></defs>
							<g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="(Generic)-Icon---Edit-Itinerary" transform="translate(-16.000000, -17.000000)" fill="#FFFFFF">
									<g id="Icon---Edit-Itinerary-(eRoam)" transform="translate(16.000000, 17.000000)">
										<path d="M16.8519039,12.1743021 L16.8519039,22.3618573 C16.8519039,22.9188258 16.3440134,23.3704013 15.7713079,23.3704013 L1.03704024,23.3704013 C0.464334766,23.3704013 0,22.9188258 0,22.3618573 L0,3.15464079 C0,2.59767236 0.464334766,2.19097699 1.03704024,2.19097699 L11.8155179,2.19097699 C12.3882234,2.19097699 12.8525582,2.64255257 12.8525582,3.199521 C12.8525582,3.75648944 12.3882234,4.20806502 11.8155179,4.20806502 L2.07408048,4.20806502 L2.07408048,21.3533133 L14.7778234,21.3533133 L14.7778234,12.1743021 C14.7778234,11.6173336 15.2421582,11.1657581 15.8148636,11.1657581 C16.3875691,11.1657581 16.8519039,11.6173336 16.8519039,12.1743021 L16.8519039,12.1743021 Z M8.91854604,13.0787139 L7.67306072,13.5592851 L8.17187707,12.3185239 L18.3607974,2.25981011 L19.1295035,3.01747881 L8.91854604,13.0787139 Z M20.7758049,2.48622825 L18.9140584,0.651182407 C18.7678357,0.507212749 18.5684647,0.426024955 18.3602789,0.426024955 L18.3600196,0.426024955 C18.1518338,0.426024955 17.9524628,0.507464885 17.8062401,0.651434543 L6.95153997,11.3674669 C6.87765086,11.4405863 6.8195766,11.5273211 6.78120611,11.6228806 L5.5733135,14.6283418 C5.46053537,14.908717 5.52949855,15.227417 5.74883256,15.4399676 C5.89764783,15.5836851 6.09572252,15.6600823 6.2976861,15.6600823 C6.39413085,15.6600823 6.49135337,15.6426849 6.58442773,15.6068816 L9.64421495,14.4263809 C9.74428933,14.3875519 9.83528962,14.3290564 9.91099355,14.2544241 L20.7758049,3.5489815 C21.0747317,3.25423452 21.0747317,2.7807231 20.7758049,2.48622825 L20.7758049,2.48622825 Z" id="document-edit"></path>
									</g>
								</g>
							</g>
						</svg>
					</a></p>
				<p class="map-icon"><a href="#">
						<svg width="24px" height="25px" viewBox="0 0 24 25" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
							<desc>Created with Sketch.</desc>
							<defs></defs>
							<g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="-(Generic)-Icon---Assistance" transform="translate(-12.000000, -17.000000)">
									<g id="Icon---Assistance-(eRoam)" transform="translate(12.000000, 17.000000)">
										<ellipse id="Oval" fill="#FFFFFF" cx="12" cy="12.0855615" rx="12" ry="12.0855615"></ellipse>
										<text id="?" font-family="HelveticaNeue-Bold, Helvetica Neue" font-size="18" font-weight="bold" fill="#212121">
											<tspan x="7" y="20">?</tspan>
										</text>
									</g>
								</g>
							</g>
						</svg>
					</a></p>
			</div>
		</div>
	</section>
	<a href="#" id="onRegisterModal" data-toggle="modal" data-target="#registerModal" style="display:none;" data-backdrop="static" data-keyboard="false"></a>

	<div class="modal fade" id="registerModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/'"><span aria-hidden="true">&times;</span></button>
					<div class="modal-icon">
						<svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
							<desc>Created with Sketch.</desc>
							<defs></defs>
							<g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="42---EROA007-V4.1-Generic-(B2B-Agent-Screen)-02" transform="translate(-320.000000, -171.000000)" fill="#212121">
									<g id="Group-3" transform="translate(291.000000, 144.000000)">
										<g id="Group-2">
											<path d="M31.8186047,48.7674419 C33.0495814,47.4647442 35.9940465,46.0138605 38.4546047,45.1562791 L41.1141395,47.4181395 C41.5310698,47.7728372 42.1436279,47.7728372 42.5605581,47.4181395 L45.220093,45.1562791 C47.6806512,46.0138605 50.6251163,47.4647442 51.856093,48.7674419 L31.8186047,48.7674419 Z M45.316093,42.8346977 C44.9488372,42.7169302 44.5472558,42.7978605 44.2536744,43.0476279 L41.8372093,45.1024186 L39.4210233,43.0476279 C39.1274419,42.7978605 38.7258605,42.7169302 38.3586047,42.8346977 C36.795814,43.3331163 29,46.016093 29,49.8837209 C29,50.500186 29.499814,51 30.1162791,51 L53.5581395,51 C54.1746047,51 54.6744186,50.500186 54.6744186,49.8837209 C54.6744186,46.016093 46.8786047,43.3331163 45.316093,42.8346977 L45.316093,42.8346977 Z M41.8372093,29.2325581 C44.7495814,29.2325581 47.1188837,31.6166512 47.1188837,34.5474419 C47.1188837,37.4787907 44.7495814,39.8634419 41.8372093,39.8634419 C38.9251163,39.8634419 36.555814,37.4787907 36.555814,34.5474419 C36.555814,31.6166512 38.9251163,29.2325581 41.8372093,29.2325581 L41.8372093,29.2325581 Z M41.8372093,42.096 C45.9805581,42.096 49.3514419,38.7097674 49.3514419,34.5474419 C49.3514419,30.3856744 45.9805581,27 41.8372093,27 C37.6941395,27 34.3232558,30.3856744 34.3232558,34.5474419 C34.3232558,38.7097674 37.6941395,42.096 41.8372093,42.096 L41.8372093,42.096 Z" id="user"></path>
										</g>
									</g>
								</g>
							</g>
						</svg>
					</div>
					<div class="modal-container">
						<h4 class="modal-title" id="myModalLabel">Create New Customer Account</h4>
						<p>Please fill in the details below to registered a new client. If your client has been registered previously, please search and sign them in. <a href="/login">Click Here</a></p>
					</div>
				</div>
				<div class="modal-body">
					<div class="modal-container">
						<form action="register" method="post" class="form-horizontal" id="signup_form">
							<?php echo e(csrf_field()); ?>

							<div class="input-field <?php echo e($errors->register->has('first_name') ? 'has-error' : ''); ?>">
								<input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo e(old('first_name')); ?>">
								<label for="first_name">First Name</label>

							</div>

							<div class="input-field <?php echo e($errors->register->has('last_name') ? 'has-error' : ''); ?>">
								<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo e(old('last_name')); ?>">
								<label for="last_name">Last Name</label>
							</div>

							<div class="input-field <?php echo e($errors->register->has('email') || session()->has('register_error') ? 'has-error' : ''); ?>">
								<input  type="text" class="form-control" id="email" name="email" value="<?php echo e(old('email')); ?>">
								<label for="email">Email Address</label>
							</div>
							<div class="input-field <?php echo e($errors->register->has('password') ? 'has-error' : ''); ?>">
								<input id="password" type="password" class="form-control"  name="password" >
								<label for="pasword">Password</label>
							</div>
							<div class="input-field <?php echo e($errors->register->has('confirmpassword') ? 'has-error' : ''); ?>">
								<input id="confirmpassword" type="password" class="form-control"  name="confirmpassword">
								<label for="confirmpassword">Confirm Password</label>
							</div>

							<div class="input-field select-box">
								<select class="form-control" name="nationality" id="nationality">
									<option value=""> Select Nationality </option>
									<?php if( count( $travelers['nationalities'] ) > 0 ): ?>
										<?php if( count( $travelers['nationalities']['featured'] ) > 0 ): ?>
											<?php foreach( $travelers['nationalities']['featured'] as $featured): ?>
												<option value="<?php echo e($featured['id']); ?>" <?php echo e(isset($travel_pref['nationality']) && $travel_pref['nationality'] == $featured['name'] ? 'selected' : ''); ?>><?php echo e($featured['name']); ?></option>
											<?php endforeach; ?>
										<?php endif; ?>
										<option value="" disabled> --------------------------- </option>
										<?php if( count( $travelers['nationalities']['not_featured'] ) > 0 ): ?>
											<?php foreach( $travelers['nationalities']['not_featured'] as $not_featured): ?>
												<option value="<?php echo e($not_featured['id']); ?>" <?php echo e(isset($travel_pref['nationality']) && $travel_pref['nationality'] == $not_featured['name'] ? 'selected' : ''); ?>><?php echo e($not_featured['name']); ?></option>
											<?php endforeach; ?>
										<?php endif; ?>
									<?php endif; ?>
								</select>
							</div>
							<label id="nationality-error" class="error" for="nationality" style="display: none;">Please select Nationality.</label>

							<div class="input-field select-box">
								<select class="form-control" name="gender" id="gender">
									<option value="">Gender</option>
									<option value="male">Male</option>
									<option value="feMale">Female</option>
								</select>
							</div>
							<label id="gender-error" class="error" for="gender" style="display: none;">Please select Gender.</label>

							<div class="input-field select-box">
								<select class="form-control" name="age_group" id="age_group">
									<option value="">Age Group</option>
									<?php if( count( $travelers['age_groups'] ) > 0 ): ?>
										<?php foreach( $travelers['age_groups'] as $age_group): ?>
											<?php if( empty( $age_group['name'] ) ): ?>
												<?php continue; ?>
											<?php endif; ?>
											<option value="<?php echo e($age_group['id']); ?>" <?php echo e(isset($travel_pref['age_group']) && $travel_pref['age_group'] == $age_group['name'] ? 'selected' : ''); ?>><?php echo e($age_group['name']); ?></option>
										<?php endforeach; ?>
									<?php endif; ?>
								</select>
							</div>
							<label id="age_group-error" class="error" for="age_group" style="display: none;">Please select Age Group.</label>

							<div class="input-field select-box <?php echo e($errors->register->has('currency') ? 'has-error' : ''); ?>">
								<select class="form-control" name="currency" id="currency">
									<option value="" >Select Default Currency</option>
                                    <?php $currencies = session()->get('all_currencies'); ?>
									<?php foreach( $currencies as $currency ): ?>
										<option value="<?php echo e($currency['code']); ?>" <?php echo e((old('currency')) && old('currency') == $currency['code'] ? ' selected' : ''); ?>> <?php echo e('('. $currency['code'] .') '.$currency['name']); ?> </option>
									<?php endforeach; ?>;
								</select>
							</div>
							<label id="currency-error" class="error" for="currency" style="display: none;">Please select Currency.</label>

							<?php if(count($errors->register) > 0 || session()->has('register_error') ): ?>
								<div class="error-msg">
									<p> Something went wrong! </p>
									<?php echo session()->has('register_error') ? '<p>'. array_get( session()->get('register_error'), 'email.0'  ) .'</p>' : ''; ?>

								</div>
							<?php endif; ?>

							<div class="form-group m-t-30">
								<label class="radio-checkbox label_check m-r-10" for="checkbox-03"><input type="checkbox" name = "policy" id="checkbox-03" value="1">By clicking ‘Create Account’, your client agrees to the <span><a href="/terms" target="_blank"> Terms of Use</a></span> and <span><a href="/privacy-policy" target="_blank">Privacy Policy</a></span>.</label>
                                <label for="policy" generated="true" class="error" style="display: none;">Please select checkbox to agree privacy policy and tems of use.</label>
							</div>

							<div class="row m-t-50">
								<div class="col-sm-6">
									<input type="button" name="" class="btn btn-primary btn-block" data-dismiss="modal" value="Cancel" onclick="window.location='/'">
								</div>
								<div class="col-sm-6">
									<input type="submit" name="" class="btn btn-primary btn-block" value="Create Account">
								</div>
							</div>

							<div class="login-or m-t-40">
								<span>or</span>
							</div>

							<div class="m-t-10 text-center">
								<button type="submit" class="fb-login" disabled><i class="fa fa-facebook-official"></i> <span>CONTINUE WITH FACEBOOK</span></button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<button id="fb-login-btn" disabled><i class="fa fa-facebook"></i> Sign Up with Facebook</button>
	

<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>
	<script>
        $(document).ready(function(){
        	jQuery.validator.addMethod("lettersonly", function(value, element) {
			  return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Please enter letters only."); 
            $("#signup_form").validate({
                rules: {
                    first_name: {
                        required: true,
                        lettersonly: true
                    },
                    last_name: {
                        required: true,
                        lettersonly: true
                    },
                    email: {
                        required: true,
                        email:true
                    },
                    password: {
                        required: true
                    },
                    confirmpassword: {
                       equalTo : "#password"
                    },
                    nationality: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    age_group: {
                        required: true
                    },
                    currency: {
                        required: true
                    },
                    policy:{
                        required:true
                    },
                },
                messages: {

                    first_name: {
                        required: "Please enter First Name."
                    },
                    last_name: {
                        required: "Please enter Last Name."
                    },
                    email: {
                        required: "Please enter Email Address."
                    },
                    password: {
                        required: "Please enter Password."
                    },
                    confirmpassword: {
                        equalTo: 'Password and Confirm Password must be same.'
                    },
                    nationality: {
                        required: "Please select Nationality.",
                    },
                    gender: {
                        required: "Please select Gender.",
                    },
                    age_group: {
                        required: "Please select Age Group.",
                    },
                    currency: {
                        required: "Please select Currency.",
                    },
                    policy: {
                        required: "Please select checkbox to agree privacy policy and tems of use.",
                    }
                },
                errorPlacement: function (label, element) {
                    label.insertAfter(element);
                },
                submitHandler: function (form) {

                    form.submit();

                }
            });
        });
	
	</script>
<script>
$(document).ready(function() {
	
	var facebookBtn = $('#fb-login-btn');
	var fbResponse;
	var fields = {
		fields: 'email, picture.width(500).height(500), first_name, last_name'
	};

	// GET FACEBOOK SCRIPT
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/sdk.js', function() {
		FB.init({
			appId: '1731928143731970',
			version: 'v2.7'
		});

		FB.getLoginStatus(function(response) {
			if (response.status == 'connected') {
				FB.api('/me', fields, function(response) {
					fbResponse = response;
				});
			}
			facebookBtn.removeAttr('disabled');
		});
	});

	// ON CLICK FACEBOOK BUTTON
	facebookBtn.on('click', function(e) {
		e.preventDefault();
		FB.login(function(response) {
			var grantedScopes = response.authResponse.grantedScopes.split(',');
			var hasEmail = grantedScopes.some(function(v, k) {
				return v == 'email';
			});
			if (response.status == 'connected') {
				// CHECK IF USER GRANTED PERMISSION TO HIS/HER EMAIL
				if (hasEmail == true) {
					FB.api('/me', fields, function(response) {
						fbResponse = response;
						checkFacebookUser(response);
					});
				} else {
					$('#fb-error-msg').text('You must allow eRoam\'s permission to read your Facebook email address to proceed.').show();
				}
			}
		}, { scope: 'email', return_scopes: true });
	});

	// FACEBOOK USER SIGN UP FOR EROAM
	$('#user-register').delegate('#sign-up-fb-btn', 'click', function(e) {
		e.preventDefault();
		var name = fbResponse.first_name + ' ' + fbResponse.last_name;
		var fname = fbResponse.first_name;
		var lname = fbResponse.last_name;
		var email = fbResponse.email;
		var password = $('#fb-password').val();
		var currency = $('#fb-currency').val();

		// VALIDATION
		if (name == '' || fname == '' || lname == '' || email == '' || password == '' || currency == null) {
			$('#fb-sign-up-error').text('All fields are required.').show();
		} else {
			if (password.length < 6) {
				$('#fb-sign-up-error').text('Password must be at least 6 characters long.').show();
			} else {
				eroam.api('post', 'user/check-email-availability', {email: email}, function(response) {
					if (response) {
						$('#fb-sign-up-error').text('Your Facebook email address ('+email+') is already in use.').show();
					} else {
						// STORE USER
						$('#fb-sign-up-error').hide();
						var data = {
							email: email,
							first_name: fname,
							last_name: lname,
							password: password,
							currency: currency,
							image_url: fbResponse.picture.data.url,
						};
						eroam.api('post', 'user/fb-signup', data, function(response) {
							if (response) {
								eroam.ajax('post', 'login', {username: email, password: password}, function(response) {
									if (response == 'valid') {
										window.location.href = '<?php echo e(url('profile')); ?>';
									}
								}, function() {
									$('#sign-up-fb-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
								});
							}
						}, function() {
							$('#sign-up-fb-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Processing...');
						});
					}
				});
			}
		}
	});
});

function checkFacebookUser(fbUser) {
	if (fbUser.error) {
		window.location.href = '<?php echo e(url('login')); ?>';
	}

	eroam.api('post', 'check-fb-user', {email: fbUser.email}, function(response) {
		if (response) {
			eroam.ajax('post', 'login', {username: response.email}, function(response) {
				if (response == 'valid') {
					window.location.href = '<?php echo e(url('profile')); ?>';
				}
			}, function() {
				$('#fb-login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
			});
		} else {
			var fbEmail = fbUser.email ? fbUser.email : '';
			var currencies = JSON.parse($('#all-currencies').val());
			var currencyHtml = '';

			for (var i = 0; i < currencies.length; i++) {
				currencyHtml += '<option value="'+currencies[i].code+'">'+currencies[i].code+' '+currencies[i].name+'</option>';
			}

			$('#user-register').html(
				'<h4>Sign up for eRoam</h4>' +
				'<div class="form-group">'+
					'<input type="password" id="fb-password" class="form-control" placeholder="Choose a Password">' +
					'<i class="fa fa-lock"></i>' +
				'</div>' +
				'<div class="form-group">'+
					'<select class="form-control" id="fb-currency">' +
						'<option disabled selected value="">Select Currency</option>' +
						currencyHtml +
					'</select>'+
					'<i class="fa fa-dollar"></i>' +
				'</div>' +
				'<p id="fb-sign-up-error" class="error-msg" style="display: none"></p>' +
				'<button id="sign-up-fb-btn"><i class="fa fa-facebook"></i> Sign up as '+ fbUser.first_name +' ' + fbUser.last_name +' <img src="' + fbUser.picture.data.url + '"></button>'
			);
		}
	}, function() {
		$('#fb-login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
	});
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>