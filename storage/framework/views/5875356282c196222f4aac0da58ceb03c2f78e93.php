<?php $__env->startSection('custom-css'); ?>
<style type="text/css">
	#itr {
		padding: 2rem;
	}
	.itr-leg {
		background: #ffffff;
		margin-bottom: 2rem;
	}
	.itr-leg .heading {
		border: 1px solid #B2B2B2;
		border-bottom: 1px solid white;
		position: relative;
		bottom: -1px;
		display: inline-block;
		font-weight: 400;
		margin: 0;
		padding: 2rem;
	}
	.itr-leg .heading i:before {
		font-size: 25px;
		color: #2AA9DF;
	}
	.itr-leg .body {
		border: 1px solid #B2B2B2;
		padding: 2rem;
	}
	.itr-leg .body section {
		margin-bottom: 2rem;
	}
	.itr-leg .body .title {
		font-size: 17px;
		text-transform: uppercase;
	}
	.itr-leg .body .desc {
		margin-left: 8.8rem;
	}
	.itr-leg .body .desc p {
		line-height: 1em;
	}
	.itr-leg .body .desc p i.status-icon {
		margin-left: 1.5rem;
		margin-right: .2rem;
		color: #009688;
		font-weight: 700;
	}
	span.not-available,
	span.not-available i {
		color: #d32f2f !important;
	}
	.itr-leg .body .desc p span.price {
		font-weight: 700;
		font-size: 18px;
		color: #009688;
	}
	.itr-leg .body .title i:before {
		position: relative;
		top: 5px;
		border-radius: 100px;
		padding: 1.5rem;
		color: #ffffff;
		font-size: 25px;
		margin-right: 2.8rem;
	}
	.itr-leg .body .accomodation .title i:before {
		background: #00bcd4;
	}
	.itr-leg .body .activities .title i:before {
		background: #2196f3;
	}
	.itr-leg .body .transport .title i:before {
		background: #f44336;
	}
	.change-btn i {
		font-size: 1.6rem;
		margin-left: 3rem;
		color: #f8f8f8 !important;
		background: #2AA9DF;
		padding: 8px;
		display: inline-block;
		transition: all 100ms ease-in-out 0s;
	}
	.change-btn i:hover {
		text-decoration: none;
		background: #2792BF;
	}
	.old-price {
		text-decoration: line-through;
		color: #ABABAB;
		font-weight: 400;
		margin-left: 1rem;
	}
	#review-summary {
		border: 1px solid #B2B2B2;
	    padding: 5px;
	    margin-top: 11px;
	}
	#review-summary h3 {
		margin-top: 0;
	}
	#total {
		font-size: 16px;
	}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row iternary-box  tabs-content-container">
	<?php /*<div id="itr" class="col-md-8">
		<?php 
			foreach ($itinerary as $key => $leg){
				$hotel = $leg->hotel;
				$hotel =  json_decode(json_encode($hotel), true);
		?>
			<div class="itr-leg">
				<h4 class="heading"><i class="flaticon-map"></i> <?php echo $hotel['city']['name']; ?> - <span class="small-txt">{{ date( 'jS, M Y', strtotime($leg->city->date_from)) }}</h4>
				<div class="body">
					<section class="accomodation">
						<h5 class="title"><i class="flaticon-rest"></i> accomodation</h5>
						<div class="desc">
							<p>
								<span> <?php echo $hotel['name'];?> (Single)</span> <i class="flaticon-approve-circular-button status-icon"></i>
								<span class="price">$240.10 <span class="old-price">$270.98</span></span>
								<a href="#" class="change-btn"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Accomodation"></i></a>
							</p>
						</div>
					</section>
					<section class="activities">
						<h5 class="title"><i class="flaticon-backpacker"></i> activities</h5>
						<div class="desc">
							<p><?php echo $hotel['name'];?> (Single) <i class="flaticon-approve-circular-button status-icon"></i> <span class="price">$240.10</span> <a href="#" class="change-btn"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Activity"></i></a></p>
							<p><?php echo $hotel['name'];?> (Single) <span class="not-available"><i class="flaticon-forbidden status-icon"></i> Not Available</span> <a href="#" class="change-btn"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Activity"></i></a></p>
						</div>
					</section>
					<section class="transport">
						<h5 class="title"><i class="flaticon-plane"></i> onward transport</h5>
						<div class="desc">
							<p>
								<span>Coach Minivan (Greyhound)</span> <i class="flaticon-approve-circular-button status-icon"></i>
								<span class="price">$240.10</span>
								<a href="#" class="change-btn" data-toggle="modal" data-target="#change-itinerary-modal"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Onward Transport"></i></a>
							</p>
							<p>Depart: Sunday, Jun 25 10:00 PM (AEST +10:00)</p>
							<p>Arrive: Monday, Jun 26 10:25 AM (AEST +10:00)</p>
						</div>
					</section>
				</div>
			</div>
		<?php } ?>
	</div> */ ?>

	<div class="col-sm-8 padding-right-0" >
	    <div class="panel-group sortable-list m-t-10" id="accordion1" role="tablist" aria-multiselectable="true">
	      	<?php 
	      		$i=0;
	      		foreach ($itinerary as $key => $leg){
					$hotel = $leg->hotel;
					$hotel =  json_decode(json_encode($hotel), true);
					//dd($hotel);exit;
	  				$i++;
	      	?>
	            <div class="panel panel-default">
	              <div class="panel-heading" role="tab" id="headingDate<?php echo e($i); ?>">
	                <div class="panel-title row">
	                  <div class="col-sm-1 col-xs-2 m-t-10">
	                    <svg width="28px" height="27px" viewBox="0 0 28 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	                      <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
	                      <desc>Created with Sketch.</desc>
	                      <defs></defs>
	                      <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	                          <g id="45---EROA007-V4.1-Generic-(Create-Itinerary)-01" transform="translate(-18.000000, -59.000000)" fill="#212121">
	                              <g id="(Generic)-Icon---Location-Preferences" transform="translate(18.000000, 59.000000)">
	                                  <g id="(eRoam)-Icon---Location-Preferences">
	                                      <path d="M7.53597816,15.6938325 C6.41269472,15.6938325 5.49904215,14.7574227 5.49904215,13.6064237 C5.49904215,12.4554248 6.41269472,11.519015 7.53597816,11.519015 C8.65926159,11.519015 9.57337087,12.4554248 9.57337087,13.6064237 C9.57337087,14.7574227 8.65926159,15.6938325 7.53597816,15.6938325 M7.53597816,9.92609431 C5.53146871,9.92609431 3.90054977,11.5770428 3.90054977,13.6064237 C3.90054977,15.6358046 5.53146871,17.2867531 7.53597816,17.2867531 C9.54071596,17.2867531 11.1718633,15.6358046 11.1718633,13.6064237 C11.1718633,11.5770428 9.54071596,9.92609431 7.53597816,9.92609431 M7.53506473,24.6908759 C6.91781832,24.0081956 5.94250961,22.8703952 4.97108295,21.4975251 C2.91405161,18.5904449 1.82684844,15.9841991 1.82684844,13.9605072 C1.82684844,8.2633127 6.19644154,7.82048075 7.5357498,7.82048075 C12.8329252,7.82048075 13.2446512,12.5200518 13.2446512,13.9605072 C13.2446512,17.9671578 9.15410916,22.8967921 7.53506473,24.6908759 M12.6621149,7.80637203 C11.3504377,6.64171947 9.52975487,6 7.5357498,6 C5.54174473,6 3.72106191,6.64171947 2.40938473,7.80637203 C0.833042887,9.20632173 0,11.3342362 0,13.9605072 C0,19.6809129 6.6061123,26.383923 6.8872186,26.6663251 C7.058714,26.8388156 7.29232224,26.9355287 7.5357498,26.9355287 C7.77917735,26.9355287 8.0127856,26.8388156 8.18428099,26.6663251 C8.4653873,26.383923 15.0714996,19.6809129 15.0714996,13.9605072 C15.0714996,11.3342362 14.2384567,9.20632173 12.6621149,7.80637203" id="location"></path>
	                                      <path d="M19.5359782,9.69383246 C18.4126947,9.69383246 17.4990421,8.75742267 17.4990421,7.60642371 C17.4990421,6.45542476 18.4126947,5.51901497 19.5359782,5.51901497 C20.6592616,5.51901497 21.5733709,6.45542476 21.5733709,7.60642371 C21.5733709,8.75742267 20.6592616,9.69383246 19.5359782,9.69383246 M19.5359782,3.92609431 C17.5314687,3.92609431 15.9005498,5.57704279 15.9005498,7.60642371 C15.9005498,9.63580463 17.5314687,11.2867531 19.5359782,11.2867531 C21.540716,11.2867531 23.1718633,9.63580463 23.1718633,7.60642371 C23.1718633,5.57704279 21.540716,3.92609431 19.5359782,3.92609431 M19.5350647,18.6908759 C18.9178183,18.0081956 17.9425096,16.8703952 16.971083,15.4975251 C14.9140516,12.5904449 13.8268484,9.98419914 13.8268484,7.96050722 C13.8268484,2.2633127 18.1964415,1.82048075 19.5357498,1.82048075 C24.8329252,1.82048075 25.2446512,6.52005182 25.2446512,7.96050722 C25.2446512,11.9671578 21.1541092,16.8967921 19.5350647,18.6908759 M24.6621149,1.80637203 C23.3504377,0.641719466 21.5297549,0 19.5357498,0 C17.5417447,0 15.7210619,0.641719466 14.4093847,1.80637203 C12.8330429,3.20632173 12,5.33423617 12,7.96050722 C12,13.6809129 18.6061123,20.383923 18.8872186,20.6663251 C19.058714,20.8388156 19.2923222,20.9355287 19.5357498,20.9355287 C19.7791774,20.9355287 20.0127856,20.8388156 20.184281,20.6663251 C20.4653873,20.383923 27.0714996,13.6809129 27.0714996,7.96050722 C27.0714996,5.33423617 26.2384567,3.20632173 24.6621149,1.80637203" id="location"></path>
	                                  </g>
	                              </g>
	                          </g>
	                      </g>
	                  </svg>
	                  </div>
	                  <div class="col-sm-11 col-xs-10">
	                    <div role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseDate<?php echo e($i); ?>" aria-expanded="true" aria-controls="collapseDate<?php echo e($i); ?>" class="row collapse-title <?php echo e($i != 1 ? 'collapsed' : ''); ?>">
	                      <div class="col-sm-8"> <?php echo $leg->city->name; ?> - <span class="small-txt"><?php echo e(date( 'jS, M Y', strtotime($leg->city->date_from))); ?></div>
	                      <div class="col-sm-4 right-align"><i class="fa fa-minus-circle"></i></div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	              <div id="collapseDate<?php echo e($i); ?>" class="panel-collapse collapse <?php echo e($i == 1 ? 'in' : ''); ?>" role="tabpanel" aria-labelledby="headingDate<?php echo e($i); ?>">
	                <div class="panel-body no-padding">
	                  	<section class="accomodation">
							<h5 class="title"><i class="flaticon-rest"></i> accomodation</h5>
							<div class="desc">
								<p>
									<span> <?php echo $hotel['name'];?> (Single)</span> <i class="flaticon-approve-circular-button status-icon"></i>
									<span class="price">$240.10 <span class="old-price">$270.98</span></span>
									<a href="#" class="change-btn"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Accomodation"></i></a>
								</p>
							</div>
						</section>
						<section class="activities">
							<h5 class="title"><i class="flaticon-backpacker"></i> activities</h5>
							<div class="desc">
								<p><?php echo $hotel['name'];?> (Single) <i class="flaticon-approve-circular-button status-icon"></i> <span class="price">$240.10</span> <a href="#" class="change-btn"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Activity"></i></a></p>
								<p><?php echo $hotel['name'];?> (Single) <span class="not-available"><i class="flaticon-forbidden status-icon"></i> Not Available</span> <a href="#" class="change-btn"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Activity"></i></a></p>
							</div>
						</section>
						<section class="transport">
							<h5 class="title"><i class="flaticon-plane"></i> onward transport</h5>
							<div class="desc">
								<p>
									<span>Coach Minivan (Greyhound)</span> <i class="flaticon-approve-circular-button status-icon"></i>
									<span class="price">$240.10</span>
									<a href="#" class="change-btn" data-toggle="modal" data-target="#change-itinerary-modal"><i class="fa fa-pencil-square-o" data-toggle="tooltip" data-placement="right" title="Change Onward Transport"></i></a>
								</p>
								<p>Depart: Sunday, Jun 25 10:00 PM (AEST +10:00)</p>
								<p>Arrive: Monday, Jun 26 10:25 AM (AEST +10:00)</p>
							</div>
						</section>
	                </div>
	              </div>
	            </div>
	        <?php } ?>
	    </div>
	</div>

	<div class="col-md-4">
		<div id="review-summary">
			<h3>Summary</h3>
			<p>AUD 243.15 Cost Per Day (Per Person)</p>
			<p>Total 13 Nights AUD 3,161.00 (Per Person)</p>
			<p class="bold-txt" id="total">Total : $1,424.50</p>
			<br>
			<h4><i class="fa fa-warning"></i> The following items need attention:</h4>
			<p>1. <a href="#">Sydney Onward Transport</a> <span class="not-available"><i class="flaticon-forbidden status-icon"></i> Not Available</span></p>
			<p>2. <a href="#">Byron Bay Accomodation</a> <span class="not-available"><i class="flaticon-forbidden status-icon"></i> Not Available</span></p>
			<p>3. <a href="#">Airlie Beach City Tour Activity</a> <span class="not-available"><i class="flaticon-forbidden status-icon"></i> Not Available</span></p>
		</div>
	</div>
</div>

<div id="change-itinerary-modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script type="text/javascript">
	
	var searchSession = JSON.parse($('#search-session').val());

	// REVIEW MODULE
	var review = (function() {

		function init() {

			// var transportObject = {
			// 	fare_source_code : 'ODAxMDE5JlRGJlRGJjRkYzRmMjExLTllYzgtNDlkZC04MjcxLTg0MjgwMDY0MzZmMyZURiY=',
			// 	price : '180.00',
			// 	currency : globalCurrency,
			// 	fare_type : 'Public'
			// };
			var leg = 0;

			// var formatTransport = eroam.formatTransport( searchSession.itinerary[ leg ].transport, leg );
			// validateMystifly( formatTransport, leg );

			//var formattedHotel = eroam.formatHotel( searchSession.itinerary[ leg ].hotel, leg );
			//validateAEHotel( formattedHotel, leg );

			buildItinerary();
		}


		function buildItinerary() {
			var itinerary = searchSession.itinerary;
			for (var i = 0; i < itinerary.length; i++) {
				var leg = itinerary[i];

				// HOTEL
				var hotel = eroam.formatHotel(leg.hotel, leg.city);
				buildHotel(hotel, i);

				// ACTIVITIES
				var activities = eroam.formatActivities(leg.activities);
				buildActivities(activities, i);

				// TRANSPORT
				if (i != itinerary.length - 1) {
					console.log( 'transport: ',  leg.transport );
					var nextLeg = itinerary[ i + 1 ];
					var transport = eroam.formatTransport(leg.transport, leg.city, nextLeg.city);
					buildTransport(transport, i);
				}
			}
		}


		function buildHotel(hotel, index) {
			$('.hotel-row').eq(index).html(
				'<div class="col-md-7">' +
					'<b><i class="fa fa-hotel"></i> Accomodation:</b>' +
					'<br>' +
					'<span>'+ hotel.name +' ('+hotel.room_name+')</span>' +
				'</div>' +
				'<div class="col-md-5 text-right">' +
					'<br>' +
					'<span class="bold-txt">$279.89</span>' +
					'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
					'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
				'</div>'
			);
		}


		function buildActivities(activities, index) {
			var activityList = '';
			if (activities.length > 0) {
				for (var i = 0; i < activities.length; i++) {
					var activity = activities[i];
					activityList += '<li>'+ moment(activity.date_selected).format('dddd, MMM D') +' - '+ activity.name +'</li>';
				}

				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b>' +
						'<ul>' +
							activityList +
						'</ul>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
							'<li>' +
								'<span class="bold-txt">$279.89</span>' +
								'<span class="not-available"><i class="fa fa-times-circle"></i> Unavailable</span>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			} else {
				$('.activity-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-child"></i> Activities:</b><br>' +
						'<span>No activities included.</span>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<ul>' +
							'<li>' +
								'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
							'</li>' +
						'</ul>' +
					'</div>'
				);
			}
		}


		function buildTransport(transport, index) {
			if (transport.provider != 'own_arrangement') {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ transport.name +'</span><br>' +
						'<span><b>Depart:</b> '+ transport.departure +'</span><br>' +
						'<span><b>Arrive:</b> '+ transport.arrival +'</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<span class="old-price">$279.89</span>' +
						'<span class="bold-txt">$280.89</span>' +
						'<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			} else {
				$('.transport-row').eq(index).html(
					'<div class="col-md-7">' +
						'<b><i class="fa fa-hotel"></i> Transport:</b>' +
						'<br>' +
						'<span>'+ searchSession.itinerary[index].city.name +' to '+ searchSession.itinerary[index + 1].city.name +' (Own Arrangement)</span><br>' +
					'</div>' +
					'<div class="col-md-5 text-right">' +
						'<br>' +
						'<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
					'</div>'
				);
			}
		}


		function validateMystifly( t, leg )
		{
			var unvalidated = isNotUndefined( t.availability_checked_at ) ? isExpired( t.availability_checked_at ) : true;
			console.log('Mystifly transport should be validated:', unvalidated);
			if( unvalidated )
			{
				MystiflyController.revalidate(
					t.fare_source_code,
					function( revalidateSuccessRS ) {

						var newPrice = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount;
						var newCurrency = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
						var convertedNewPrice = eroam.convertCurrency( newPrice, newCurrency );
						if( convertedNewPrice != t.price ) {
							// NEW PRICE DOES NOT MATCH THE OLD PRICE
							// NEW PRICE OF FLIGHT IS STORE IN VARIABLE "convertedNewPrice"
							// UPDATE THE PRICE UI WITH THE NEW PRICE & USE "globalCurrency" VARIABLE AS THE CURRENCY
							searchSession.itinerary[ leg ].transport.price = convertedNewPrice;
							searchSession.itinerary[ leg ].transport.old_price = t.price;
						}
						searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();


						console.log('revalidate success', revalidateSuccessRS);

						MystiflyController.flightRules(

							t.fare_source_code,

							function( flightRulesSuccessRS ) {

								console.log('fare-rules success', flightRulesSuccessRS);

								var rules = FareRules.FareRule.RuleDetails.RuleDetail;

								if( t.fare_type != 'Public' ) {
									// THIS WILL SHOW A LINK AS THE FLIGHT RULES
									console.log( 'flight rules is a link', rules );
								}else{
									console.log('flight rules is NOT a link', rules);
									if( Array.isArray( rules ) ) {
										rules.forEach(function( elem, key ) {
											
										});
										console.log('rules is an array', rules);
									}else{
										console.log('rules is not an array', rules);
									}
								}

								searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();
								bookingSummary.update( searchSession );
							},

							function( flightRulesFailureRS ) {
								// MYSTIFLY TRANSPORT NOT AVAILABLE
								console.log('fare-rules fail', flightRulesFailureRS);
							}
						);
					},

					function( revalidateFailureRS ) {
						// MYSTIFLY TRANSPORT NOT AVAILABLE
						console.log('revalidate fail', revalidateFailureRS);
					}

				);
			}
		}


		function createAERequest( searchSession )
		{
			// var rq = {
			// 	Title : '',
			// 	FirstName : '',
			// 	LastName : '',
			// 	Address : null,
			// 	Email : '',
			// 	MobileNumber : '',
			// 	HotelRequests : []
			// };
			// searchSession.itinerary.forEach(function( leg, i ){
			// 	if( i.hotel.provider == 'ae' )
			// 	{
			// 		rq.HotelRequests.push({
			// 			HotelId : i.hotel_id,
			// 			RoomId : i.room_id,
			// 			MealId : i.meal_id,
			// 			ProviderId : i.provider_id,
			// 			GroupIdentifier : i.group_identifier,
			// 			ExpectedBookingPrice : i.price, // e.g. 1155.00
			// 			GuestName : '', // e.g. John Doe
			// 			Occupancy : {
			// 				RoomIndex : '',
			// 				Adults : ''
			// 			},
			// 			FromDate : '', // e.g. 2014-10-01T00:00:00
			// 			ToDate : '' // e.g. 2014-10-01T00:00:00
			// 		});
			// 	}
			// });	
			// return rq;
		}

		
		function validateAEHotel( h, leg )
		{
			var unvalidated = isNotUndefined( h.availability_checked_at ) ? isExpired( h.availability_checked_at ) : true;
			console.log('AE hotel should be validated:', unvalidated);
			if( unvalidated )
			{
				var rq = {
					FromDate: h.checkin,
					ToDate : h.checkout,
					Adults : searchSession.travellers,
					HotelId : h.hotel_id
				};
				AEController.checkHotelAvailability(
					rq,
					function( availabilitySuccessRS )
					{
						if( availabilitySuccessRS )
						{
							console.log('AE hotel:', h, 'AE hotel RQ:', rq, 'availabilitySuccessRS', availabilitySuccessRS);

							availabilitySuccessRS[0].rooms.forEach(function( room ){
								if( room.room_id == h.room_id )
								{
									// AN ERROR OCCURED HERE. CANNOT ACCESS SEARCHSESSION VARIABLE HERE. CONTINUE HERE
									var roomPrice = eroam.convertCurrency( h.price, h.currency );
									console.log('roomPrice', roomPrice);
									console.log('room.price', room.price);
									if( room.price != roomPrice )
									{
										searchSession.itinerary[ leg ].hotel.price = room.price;
										searchSession.itinerary[ leg ].hotel.old_price = h.price;
									}
									searchSession.itinerary[ leg ].hotel.provider_id = room.provider_id;
									searchSession.itinerary[ leg ].hotel.availability_checked_at = moment().format();
									bookingSummary.update( JSON.stringify( searchSession ) );
								}
							});
						}					
						else
						{
							console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'No data returned from validateAEHotel');
							// AE HOTEL NOT AVAILABLE
						}
					},
					function( availabilityFailureRS )
					{
						console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'availabilityFailureRS', availabilityFailureRS);
						// AE HOTEL NOT AVAILABLE
					}
				);
			}
		}


		function isExpired( availability_checked_at )
		{
			var difference = moment().diff( moment( availability_checked_at ), 'seconds' );
			console.log('difference', difference);
			return ( difference > 600 ) ? true : false ;
		}


		return {
			init: init,
		};


	})( MystiflyController, HBController );


	$(document).ready(function() {

		$('[data-toggle="tooltip"]').tooltip();
		review.init();

	}); // END OF DOCUMENT READY


</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>