<?php $__env->startSection( 'custom-css' ); ?>
<style type="text/css">
	.transport-content{
		padding-top: 10px;
	}
	.transport-content select, .transport-content button{
		width: 100%;
   		padding: 11px;
	}
	.no-padding-border-margin{
		padding: 0px;
		border: none;
		margin: 0px;
	}
	/*.transport-list{
		padding: 7px;
		border: 1px solid #ccc;
		margin-bottom: 10px;
		cursor: pointer;
	}
	.transport-list button{
		padding: 11px;
		cursor: pointer;
	}
	.blue-button{
		background: #2AA9DF;
		padding: 11px;
		width: 100%;
		color: #ffffff;
		border-radius: 2px;
	}*/
	.transport-header{
		margin-bottom: 10px;
	}
	.transport-price > small{
		font-size: 60%;
	}
	.selected-hotel{
		background:  rgba(39, 168, 223, 0.44);
		border:1px solid #2AA9DF;
	}
	.modal-content{
		border-radius: 3px;
	}
	/*.blue-button:hover, .blue-button:focus{
		color: #ffffff;
	}
	.icon-style{
		border: 2px solid #918F8F;
		border-radius: 4px;
		padding: 8px 0;
		text-align: center;
		width: 38px;
		font-size: 21px;
		top: -4px;
	}
	.white{
		color: #fff;
	}
	.black-background{
		background: #000000;
	}*/
	.zero-left{
		padding-left: 0px;
		margin-left: -15px;
	}
	.transport-details{
		margin: 0 0 0px;
		font-size: 15px;
	}
	.btn > .caret{
		position: absolute;
		top: 50%;
		right: 9px;
	}

	#flight-options{
		display:none;
	}


	#drop-down, #parts-of-day, #flight-options, #popover-search{
		border-radius: 2px;
		border: 1px solid rgb(169, 169, 169);
		position: relative;
		text-align: left;
	}
	.popover{
		border-radius: 15px;
		border: 1px solid #999;
		background-color: #fff;
		/* background-color: rgba(255, 255, 255, 0.44); */
	}
	.popover-title{
		padding: 4px 6px;
		border-radius: 14px 14px 0 0;
		border-bottom: 1px solid #999;
		background-color: #fff;
		/* background-color: rgba(255, 255, 255, 0.7); */
	}
	.popover-title {
		margin: 0px;
		padding-left: 16px;
		padding-top: 6px;
		padding-bottom: 6px;
	}
	.popover-title > strong{
		padding-left: 20px;
	}
	.popover.bottom {
		top: 44px;
		margin-top: 5px;
		left: -59.25px;
		width: 300px;
	}
	#popover-transport-type-filter.popover.bottom ul{
		list-style: none;
		padding-left: 0px;
		margin-bottom: 0px;
		height: 292px;
		overflow: scroll;
		width: 100%;
	}
	#day-filter.popover.bottom ul{
		list-style: none;
		padding-left: 0px;
		margin-bottom: 0px;
		width: 100%;
	}
	.popover.bottom ul > li {
		margin-bottom: 5px;
		padding-left: 10px;
	}
	.popover.bottom ul > li > span{
		padding-left: 15px;
	}
	.show{
		display: block;
	}
	.popover-content {
		padding: 6px;
		/* background-color: rgba(255, 255, 255, 0.7); */
		background-color: #fff;
		border-radius: 0 0 14px 14px;
	}
	/*.selected-transport{
		background: rgba(39, 168, 223, 0.44);
		border: 1px solid #2AA9DF;
	}*/
	.itinerary-container {
		/*height: 448px;*/
		overflow-y: auto;
		overflow-x: hidden;
	}
	#list-container{
		height: 620px;
		/*		overflow-y: hidden;
				overflow-x: hidden;
		*/		margin-bottom: 20px;

	}
	#popover-flight-options{
		left: -95.25px;
	}
	.no-dot{
		list-style: none;
		padding: 0;
	}
	.gray-button{
		background-color: #e0e0e0;
		background-position: 0 -15px;
		padding: 11px;
		width: 100%;
		border: 1px solid rgb(169, 169, 169);
		border-radius: 2px;
	}
	#transport-loader{
		/*position: absolute;
		top: 50%;
		width: 100%;
		transform: translateY( -50% );
		text-align: center;*/
		width: 100%;
		position: absolute;
		top: 27%;
		transform: translateY( 50% );
		text-align: center;
	}
	#transport-loader span {
		padding: 8px 21px;
		font-family: "HelveticaNeue";
		font-size: 18px;
	}
	#popover-search-container.popover.bottom {
		top: 44px;
		margin-top: 5px;
		left: -530.25px;
		width: 600px !important;
		max-width: 600px !important;
	}
	.ui-state-active, .ui-slider-handle {
		background: #2AA9DF !important;
	}
	.range-field{
		border-top:0;
		border-left: 0; 
		border-right: 0;
		font-weight: bold;
		background: transparent;
		border-bottom: 1px solid #999;
	}
	.range-field:focus{
		outline: none;
	}
	.popover-search-content{
		padding: 17px;
	}
	#suggestion-container > ul {
		list-style: none;
		padding-left: 0;
	}

	.pad10_0 { padding: 10px 0px;}

	.btn-secondary:focus {     
		background-color: #212121;
	    border-color: #212121;
	    color: #fff;
	    outline: none;
	}


	.panel-heading {
		cursor: pointer;
	}

	.textCap{
		text-transform: capitalize;
	}

	.price-sign { margin-top: 3px; }
	.transport-price { line-height: 20px;}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="tabs-container">
	<div class="row">
	  	<div class="col-sm-7">
	    	<div>
	      		<div class="panel-icon">    
			        <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			            <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
			            <desc>Created with Sketch.</desc>
			            <defs>
			                <polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>
			            </defs>
			            <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			                <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-503.000000, -69.000000)">
			                    <g id="placeholder" transform="translate(503.000000, 69.000000)">
			                        <g id="Group">
			                            <mask id="mask-2" fill="white">
			                                <use xlink:href="#path-1"></use>
			                            </mask>
			                            <g id="Clip-2"></g>
			                            <path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>
			                        </g>
			                    </g>
			                </g>
			            </g>
			        </svg>
	      		</div>
	      		<div class="panel-container"> 
	        		<h4 id="city-name">
	        			<?php //echo '<pre>'; print_r($data);?>
	        			<?php echo e($data['city_name']); ?>, <?php echo e($data['country_name']); ?><span class="trans-count"></span>
		        	</h4>
	        		<p class="m-t-5">
			          	<svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			              <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
			              <desc>Created with Sketch.</desc>
			              <defs></defs>
			              <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			                  <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-535.000000, -95.000000)" fill="#212121">
			                      <g id="Page-1" transform="translate(535.000000, 95.000000)">
			                          <path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
			                          <path d="M10.1326667,8.000625 L12,8.000625 L12,6.25 L10.1326667,6.25 L10.1326667,8.000625 Z M10.1326667,10.2503125 L12,10.2503125 L12,8.4996875 L10.1326667,8.4996875 L10.1326667,10.2503125 Z M10.1326667,12.5 L12,12.5 L12,10.749375 L10.1326667,10.749375 L10.1326667,12.5 Z M7.733,12.5 L9.60033333,12.5 L9.60033333,10.749375 L7.733,10.749375 L7.733,12.5 Z M5.33333333,12.5 L7.20066667,12.5 L7.20066667,10.749375 L5.33333333,10.749375 L5.33333333,12.5 Z M2.93366667,12.5 L4.79933333,12.5 L4.79933333,10.749375 L2.93366667,10.749375 L2.93366667,12.5 Z M2.93366667,10.2503125 L4.79933333,10.2503125 L4.79933333,8.4996875 L2.93366667,8.4996875 L2.93366667,10.2503125 Z M2.93366667,8.000625 L4.79933333,8.000625 L4.79933333,6.25 L2.93366667,6.25 L2.93366667,8.000625 Z M5.33333333,8.000625 L7.20066667,8.000625 L7.20066667,6.25 L5.33333333,6.25 L5.33333333,8.000625 Z M5.33333333,10.2503125 L7.20066667,10.2503125 L7.20066667,8.4996875 L5.33333333,8.4996875 L5.33333333,10.2503125 Z M7.733,10.2503125 L9.60033333,10.2503125 L9.60033333,8.4996875 L7.733,8.4996875 L7.733,10.2503125 Z M7.733,8.000625 L9.60033333,8.000625 L9.60033333,6.25 L7.733,6.25 L7.733,8.000625 Z M9.60033333,5.749375 L2.39966667,5.749375 L2.39966667,13.000625 L12.534,13.000625 L12.534,5.749375 L9.60033333,5.749375 Z" id="Fill-3"></path>
			                      </g>
			                  </g>
			              </g>
			          	</svg> 
	          			<?php echo e(convert_date($data['date_from'], 'new_eroam_format')); ?> - <?php echo e(convert_date($data['date_to'], 'new_eroam_format')); ?>

	          		</p>
	      		</div>
	    	</div>
	  	</div>

	  	<div class="col-sm-5">
	    	<div class="input-group search-control activities-content">
	      		<span class="input-group-btn">
	        		<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
	      		</span>
	      		<input type="text" class="form-control" placeholder="">
	    	</div>
	  	</div>
	</div>

	<div class="custom-tabs" data-example-id="togglable-tabs">
	    <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
	      	<!--<li role="presentation" class="active"><a href="#top-picks" id="top-picks-tab" role="tab" data-toggle="tab" aria-controls="top-picks" aria-expanded="true">TOP PICKS</a>
	      	</li> -->
	      
	      	<li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents">PROVIDER <span class="caret"></span></a> 
		        <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="provider-tab">
					<li><a id="searchProvider">All</a></li>
					</ul>
	      	</li>

	      	<li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop2" data-toggle="dropdown" aria-controls="myTabDrop2-contents">TRANSPORT <span class="caret"></span></a> 

					<ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="transport-tab">
						<li><a id="transportProvider">All</a></li>
						<li><a id="transportProvider">Bus</a></li>
						<li><a id="transportProvider">Flight</a></li>
					</ul>

	      	</li>
			<li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="durationAsc" data-toggle="dropdown" aria-controls="myTabDrop4-contents">DURATION <span class="fa fa-caret-down"></span></a>
			</li>
	      	<li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="priceSortAsc" data-toggle="dropdown" aria-controls="myTabDrop3-contents">LOWEST PRICE <span class="fa fa-caret-down"></span></a>
	      	</li>

	    </ul> 
	</div>
</div>

<div class="tabs-content-container" style="position: relative;">
	<div class="transportWrapper">
	<!-- <div class="tab-content" id="myTabContent">  -->
		
		<div id="transport-loader">
			<span><i class="fa fa-circle-o-notch fa-spin"></i> Searching for Alternative Travel Options, please wait...</span>
		</div>
		
		
	  	<!-- <div class="tab-pane fade in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab">  -->
	    	<div class="panel-group tabs-content-wrapper" id="transportList" role="tablist" aria-multiselectable="true">
	      		
	    	</div> 
	  	<!-- </div> -->

	   <!--  <div class="tab-pane fade" role="tabpanel" id="duration1" aria-labelledby="duration1-tab"> 
	      	<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p> 
	    </div> 
	    <div class="tab-pane fade" role="tabpanel" id="stops1" aria-labelledby="stops1-tab"> 
	      	<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p> 
	    </div>  -->
	<!-- </div>  -->
	</div>
</div>

<input type="hidden" id="fl-opt-json" value="<?php echo e(json_encode($data['flight_options'])); ?>">

<div class="col-md-12">
	
	<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="flightRules" id="flightRules">
		<div class="modal-dialog modal-sm modal-size" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="gridSystemModalLabel">Flight Rules & Conditions</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 rules-content">

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-6">
							<button type="button" class="btn blue-button flight-rules-agree">OK</button>
						</div>
						<div class="col-md-6">
							<button type="button" class="btn gray-button" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

</div>

<?php /* <?php echo e(dd( $data )); ?> */ ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>
<script>

	/***************************** START GLOBAL VARIABLE DECLERATIONS *************************/
	var transport_count = 0;
	var selectedTransportId = "<?php echo e($data['selected_transport_id']); ?>";
	var selectedProvider = "<?php echo e($data['selected_provider']); ?>";
	var maxPrice = 0;

	var search_session = JSON.parse( $('#search-session').val() );
	var key = "<?php echo e($data['key']); ?>";
	//console.log('asdfffffffffffffff', JSON.stringify(search_session.itinerary[0].transport));
	// console.log('asdfffffffffffffff', search_session);
	var flightOptions = JSON.parse( $('#fl-opt-json').val() );
	var eroam_data = {
		city_ids: ['<?php echo e($data['city_id']); ?>'],
		date_from: formatDate("<?php echo e($data['date_from']); ?>"),
		date_to: formatDate("<?php echo e($data['date_to']); ?>")
	};
	var flightsExist        = false;
	var availableTransTypes = [];
	var totalTransportCount = 0;
	var activityDates = ["<?php echo join('", "', $data['activity_dates']); ?>"];

	/***************************** END GLOBAL VARIABLE DECLERATIONS *************************/


	/*************************************** START EVENT HANDLERS **************************************/

	// functions to call upon DOM ready
	$(document).ready(function(){
		//console.log(getDayTime('2017-03-01 08:30', '9:30'));
		// function to to make all transport api calls e.g. eroam, mystifly; queueing functionality added;
		buildTransports();

		var changeRangeField = function(){
		
			var id = $(this).attr('id');

			var min = parseInt( eroam.convertCurrency($('#price-from').val(), globalCurrency) ) ? parseInt( eroam.convertCurrency($('#price-from').val(), globalCurrency) ) :  parseInt( eroam.convertCurrency(0, 'AUD') );
			var max = parseInt( eroam.convertCurrency($('#price-to').val(), globalCurrency) ) ? parseInt( eroam.convertCurrency($('#price-to').val(), globalCurrency) ) :  parseInt( eroam.convertCurrency(maxPrice, 'AUD') );
			var value = parseInt( eroam.convertCurrency($(this).val(), globalCurrency) );

			var sliderMin = parseInt( eroam.convertCurrency(0, 'AUD') );
			//var sliderMax = parseInt( eroam.convertCurrency(50000, 'AUD') );
			var sliderMax = parseInt( maxPrice );

			if( !isNaN(value) ){

				if (id == 'price-from') {
					if(value <= max){
						$( '#slider-range' ).slider({
							range: true,
							min: sliderMin,
							max:  sliderMax,
							values: [ value, max ]
						});
					}
				}else{
					if(value >= min){
						$( '#slider-range' ).slider({
							range:true,
							min: sliderMin,
							max: sliderMax,
							values: [ min, value ]
						});
					}
				}
			}
		}
		$('.range-field').bind('keyup', changeRangeField);

		var validateRange = function(){
			var from = parseInt($('#price-from').val());
			var to = parseInt($('#price-to').val());
			var value = $(this).val();
			var id = $(this).attr('id');
			if( !isNaN(value) ){
				if (id == 'price-from') {
					if(from > to){
						$('#error_range').text('Invalid Price Range').fadeIn('slow');
						$('#price-from').val(0);
						$( '#slider-range' ).slider({
							range: true,
							min: 0,
							max:  maxPrice,
							values: [ 0, to ]
						});
						setTimeout(function(){ 
							$('#error_range').fadeOut('slow');
						}, 3000);
					}
				}else{
					if(to < from){
						$('#error_range').text('Invalid Price Range').fadeIn('slow');
						$('#price-to').val(maxPrice);
						$( '#slider-range' ).slider({
							range: true,
							min: 0,
							max:  maxPrice,
							values: [ from, maxPrice ]
						});
						setTimeout(function(){ 
							$('#error_range').fadeOut('slow');
						 }, 3000);
					}
				}
			}else{
				if (id == 'price-from') {
					$('#error_range').text('Invalid Price').fadeIn('slow');
					$('#price-from').val(0);
					$( '#slider-range' ).slider({
						range: true,
						min: 0,
						max:  maxPrice,
						values: [ 0, to ]
					});
					setTimeout(function(){ 
						$('#error_range').fadeOut('slow');
					}, 3000);
				}else{
					$('#error_range').text('Invalid Price').fadeIn('slow');
					$('#price-to').val(maxPrice);
					$( '#slider-range' ).slider({
						range: true,
						min: 0,
						max:  maxPrice,
						values: [ from, maxPrice ]
					});
					setTimeout(function(){ 
						$('#error_range').fadeOut('slow');
					 }, 3000);
					
				}
			}
		}
		$('.range-field').focusout(validateRange);

		var arrowSelect = function(event) {
			var numOfSuggestions = $('.suggestions').length-1;
			var selected = -1;
			var items = $('.suggestions');
			var current = $('.selected-suggestion').index();
	
			if(event.which == $.ui.keyCode.UP) {
				if(current == -1){
					$(items[numOfSuggestions]).addClass('selected-suggestion');
					var value = $(items[numOfSuggestions]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}else{
					var next = current - 1;
					next = next < 0 ? numOfSuggestions : next;
					$(items[current]).removeClass('selected-suggestion');
					$(items[next]).addClass('selected-suggestion');
					var value = $(items[next]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}
			}
			else if(event.which == $.ui.keyCode.DOWN){
				if(current == -1){
					$(items[0]).addClass('selected-suggestion');
					var value = $(items[0]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}else{
					var next = current + 1;
					next = next > numOfSuggestions ? 0 : next;
					$(items[current]).removeClass('selected-suggestion');
					$(items[next]).addClass('selected-suggestion');
					var value = $(items[next]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}
			}
			else{
				$('#suggestion-container').find('ul').html('');
				var priceFrom  = parseFloat($('#price-from').val());
				var priceTo = parseFloat($('#price-to').val());
				var pattern = new RegExp($('.search-field').val().toString(), 'gi');
				$('.transport-list').filter(function() {

					var list = '<li class="suggestions">'+$(this).attr('data-transport-operator')+'</li>'; 
					var transPrice = parseFloat( Math.ceil( $(this).attr('data-price') ).toFixed(2) );

					if( transPrice >= priceFrom && transPrice <= priceTo ){
						if($(this).attr('data-transport-operator').match(pattern) != null){
							if ( !$('ul li.suggestions:contains("'+$(this).attr('data-transport-operator')+'")').length ) {
								$('#suggestion-container').find('ul').append(list);
							}
						}
					}else{
						if( $(this).attr('data-transport-operator').match(pattern) != null){
							if ( !$('ul li.suggestions:contains("'+$(this).attr('data-transport-operator')+'")').length ) {
								$('#suggestion-container').find('ul').append(list);
							}
						}
					}
				});
			}
		}

		/*
		| Added by junfel 
		| function for showing suggestion on keydown and selecting suggestion through arrow down and arrow up
		*/
		$('.search-field').bind('keydown', arrowSelect);
		/*
		| Added by Junfel
		| function for clearing search and display all transports
		*/
		$('#clear-search').click(function(){
			var min = parseInt(eroam.convertCurrency(0, 'AUD'));
		
			var max =  parseInt(maxPrice);
			$( '#slider-range' ).slider({
				range: true,
				min: min,
				max: max,
				values: [ min, max ]
			});
			$('#price-from').val(min);
			$('#price-to').val(max);

			$('.transport-list').show();
			$('.search-field').val('');
			$('#search-form').submit();
		});
		/*
		| Added by Junfel
		| function for selecting suggestion through mouse click
		*/
		$('body').on('click', '.suggestions', function(){
			var value = $(this).text();
			$('.search-field').val(value);
			$('#search-form').submit();
		});
		/*
		| Added by Junfel 
		| Function for searching transports in the selected City
		*/
		$('#search-form').submit(function(){
			/*
			| search pattern
			| check all possible matches and case insensitive
			*/
			var priceFrom  = parseFloat($('#price-from').val());
			var priceTo = parseFloat($('#price-to').val());
	
			$('.transport-list').show();
			$('#suggestion-container').find('ul').html('');
			var pattern = new RegExp($('.search-field').val().toString(), 'gi');
			$('.transport-list').filter(function() {
				//if (price_range) {
				var transPrice = parseFloat($(this).attr('data-price'));
				if( transPrice >= priceFrom && transPrice <= priceTo ){
				
					if($(this).attr('data-transport-operator').match(pattern) == null){
						$(this).hide();
					}else{
						$(this).show();
					}
				}else{
					$(this).hide();
				}
				
			});
			return false;
		});
	});


	// function to show the popover on the buttons clicked (filters)
	$('body').on('click', '.filter-buttons',  function(){
	
		var name = $(this).attr('data-filter-name');
		var isActive = $(this).hasClass('show');
		$('.t-popover').removeClass('show');
		$('.filter-buttons').removeClass('show');
		
		switch( name )
		{
			case 'type':
				if(!isActive){
					$('#drop-down').addClass('show');
					$('#popover-transport-type-filter').addClass('show');
				}else{
					$('#drop-down').removeClass('show');
					$('#popover-transport-type-filter').removeClass('show');
				}
				break;
			case 'day-parts':
				if(!isActive){
					$('#parts-of-day').addClass('show');
					$('#popover-day-filter').addClass('show');
				}else{
					$('#parts-of-day').removeClass('show');
					$('#popover-day-filter').removeClass('show');
				}
				break;
			case 'fl-options':
				if(!isActive){
					$('#flight-options').addClass('show');
					$('#popover-flight-options').addClass('show');
					return false;
				}else{
					$('#flight-options').removeClass('show');
					$('#popover-flight-options').removeClass('show');
				}
				break;
			case 'popover-search':
				if(!isActive){
					$('#popover-search').addClass('show');
					$('#popover-search-container.popover').addClass('show');
					return false;
				}else{

					$('#popover-search').removeClass('show');
					$('#popover-search-container').removeClass('show');
				}
				break;
		}
		
	});

	$('body').click(function(e){
		var target = e.target;
		if ( $(target).hasClass('filter-buttons') === false && $('.popover').has(e.target).length === 0 ) {
			$('.filter-buttons').removeClass('show');
			$('.popover.bottom').removeClass('show');
		}
	});

	// EVENT HANDLER FOR BOOKING TRANSPORTS
	$('body').on('click', '.transportButton', function(){

		$('.flight-rules-agree').data(); // empty the data attributes of the flight modal
		$('.flight-rules-agree').prop( "disabled", true );
		$('.rules-content').html('<div class="text-center"><i class="fa fa-circle-o-notch fa-1x fa-spin" aria-hidden="true" style="color:#2AA9DF;"></i>  Loading.. </div>');

		if(! $(this).find('.select-transport').hasClass('btn-secondary') ) // check if transport is selected
		{
			var travelDuration =  get_hours_min($(this).attr('data-duration'));

			var dataEta = $(this).attr('data-eta');
			travelDuration = formatTime(travelDuration);
			if($(this).attr('data-provider') == 'mystifly'){
				dataEta = moment( dataEta, moment.ISO_8601 );
				dataEta = dataEta.format('hh:mm A');
			}
			var arrivalTime = arrival_am_pm(dataEta);

			departureDate = getDayTime(eroam_data.date_to+' '+arrivalTime, travelDuration);

			var thisData = this;
			departureDate = departureDate.split(' ');
			var displayDate = moment(departureDate[0]).format('Do, MMMM YYYY');
			if( activityDates.indexOf(departureDate[0]) != -1 ){
				
				if( parseFloat(departureDate[1]) < 18 ){
					eroam.confirm( 
						'Confirm', 
						'there\'s an activity booked on this departure date, would you like to proceed and cancel the activity?',
						function(){
							//$('#departing-date').html('<strong> Departing </strong> '+displayDate); 
							cancelActivity(departureDate[0]);
							saveTransport(thisData);
						} 
					);
				}else{
					//$('#departing-date').html('<strong> Departing </strong> '+displayDate); 
					saveTransport(this);
				}
				
			}else{
				//$('#departing-date').html('<strong> Departing </strong> '+displayDate); 
				saveTransport(this);
			}
		}
		else
		{
			$(this).find('.select-transport').attr('data-is-selected', 'false');
		}
	});


	// when user selects a mystifly flight and agrees to the flight rules then this function is triggered.
	$('.flight-rules-agree').click(function(e){
		e.preventDefault();
		var data                         = $(this).data();
		var transport                    = JSON.parse( JSON.stringify( data ) );
		transport.transport_type         = {};
		transport.transport_type.id      = data.transportTypeId;
		transport.transport_type.name    = data.transportTypeName;
		transport.transporttype          = {};
		transport.transporttype.id       = data.transportTypeId;
		transport.transporttype.name     = data.transportTypeName;
		transport.price                  = [];
		transport.price[0]               = {};
		transport.price[0].price         = data.price;
		transport.price[0].currency      = {};
		transport.price[0].currency.code = globalCurrency;

		var theElement = $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]');
		$('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
		theElement.find('.select-transport').addClass('btn-secondary');

		search_session.itinerary[key].transport = convertToSnakeCase( transport );
		bookingSummary.update( JSON.stringify( search_session ) );

		$('.transport-list').removeClass('selected-transport');
		$('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

		$('#flightRules').modal('hide');

	});


	// function to trigger when selecting day filter
	$('body').on('click', '.day', function(){
		$('.day').removeClass('btn-secondary');
		$(this).toggleClass('btn-secondary');
		var value = $(this).attr('data-value');
		$('.transport-list').filter(function() {

			var filter = $(this).attr('data-filter-trans');
			var am_pm = $(this).attr('data-am-pm');

			if( !$(this).hasClass('selected-transport') && filter == '0'){

				if( value == am_pm){
					$(this).show();
					$(this).attr('data-filter-period', 0);
				}else if( value == 'BOTH' ){
					$(this).show();
					$(this).attr('data-filter-period', 0);
				}else{
					$(this).hide();
					$(this).attr('data-filter-period', 1);
				}
			}
			
		});
	});


	// function to trigger when selecting transport types
	$('body').on('click', '.trans-type', function(){
		var transportTypeId = $(this).data('transport-type-id');
		$(this).toggleClass('btn-secondary');
		// if the transport type selected is flight
		if( transportTypeId == 1 )
		{
			$('#flight-options').toggle();
		}

		if( $(this).hasClass('btn-secondary') )
		{
			$(this).attr('data-trans-is-selected', 'true')
		}
		else
		{
			$(this).attr('data-trans-is-selected', 'false');
		}
		//console.log('sadf', to_array($('.trans-type.btn-secondary'), 'data-transport-type-name') );
		var transport_types = to_array($('.trans-type.btn-secondary'), 'data-transport-type-name');
		$('.transport-list').filter(function() {


			var filter = $(this).attr('data-filter-period');

			var transType = $(this).attr('data-transport-type-name');
			if( !$(this).hasClass('selected-transport') && filter == '0'){
				if( transport_types.indexOf(transType) !== -1 ){
					$(this).show();
					$(this).attr('data-filter-trans', 0);
				}else{
					$(this).hide();
					$(this).attr('data-filter-trans', 1);
				}
			}
		});
	});


	// function to trigger when selecting flight options
	$('body').on('click', '.fl-opt', function(){

		var option     = $(this).data('option');
		var isSelected = $(this).data('is-selected');

		flightOptions[ option ] = !isSelected;

		$(this).data('is-selected', !isSelected);

		$(this).toggleClass('btn-secondary');

	});


	// event handler to trigger filter transports
	$('body').on('click', '#transport-filter', function() {
		var filters = to_array($('.trans-type.btn-secondary'), 'data-transport-type-id');
		var day = $('.btn-secondary.day').attr('data-value');

		var  data = { filter:filters, day: day, flight_options: flightOptions, leg: <?php echo e($data['leg']); ?> };
		//comment out by junfel
		/*eroam.ajax('post', 'session/transport-filter', data, function(response){
			window.location.reload();
		});*/
	});

	/*$('body').on('click', '.panel-heading', function() {
		alert($(this).attr('id'));
		//headingPicks
	});*/

	/********************************* END EVENT HANDLERS ****************************/


	/***************************** START FUNCTIONS ****************************/

	// function to queue the calls to APIs e.g. eroam, mystifly
	function buildTransports()
	{
		// These are the functions in the queue to be called.
		var tasks = [
			hideTransType,
			buildEroamTransport, // call eroam 
			buildMystiflyTransport, // call mystifly next
			buildBusBudTransport, // call busbud next
			showTransportType, // show the available transport types
			hidePageLoader, // hides the page loader when all API calls are done
			rangeSlider,
			checkTransportCount,
            getDistinctProvider
		];

		$.each(tasks, function(index, value) {
			$(document).queue('tasks', processTask(value));
		});
		// queue`
		$(document).queue('tasks');

		$(document).dequeue('tasks');

	}

    function processTask(fn){
        return function(next){
            doTask(fn, next);
        }
    }

    function doTask(fn, next){
        fn(next);
    }


	function buildEroamTransport( next )
	{
		var trans_eroam_data = {
			from_city_id : "<?php echo e($data['city_id']); ?>",
			to_city_id : "<?php echo e($data['to_city_id']); ?>",
			date_from: formatDate("<?php echo e($data['date_from']); ?>"),
			date_to: formatDate("<?php echo e($data['date_to']); ?>"),
			day: "<?php echo e($day); ?>",
			transport_types: [<?php echo e(join(', ', $transport_pref)); ?> ]
		};

		var eroamApiCall = eroam.apiDeferred('city/from-and-to-v2', 'POST', trans_eroam_data, 'eroam', true);

		var transport_type_ids = ["<?php echo implode('", "', session()->get('transport_type_ids')); ?>"];
		var day = "<?php echo e($day == 1 ? 'AM' : 'PM'); ?>";
		if ( <?php echo e($day); ?> == 0 )
			var day = 0;
		
			eroam.apiPromiseHandler( eroamApiCall, function( eroamResponse ){
				if( eroamResponse === null ) eroamResponse = [];
				console.log( "eroamResponse :", eroamResponse );
				if( eroamResponse.length > 0 )
				{
					transport_count = eroamResponse.length;
					eroamResponse.forEach(function( eroamTrans, eroamTransIndex ){
						if(day)
						{
							var split_am_pm = etdFormat(eroamTrans.etd);
							var am_pm       = split_am_pm.split(' ');
							if(day == am_pm[1])
							{
								
							}
							else
							{
								return;
							}
						}
						eroamTrans.duration = calculateTransportDuration(eroamTrans.etd, eroamTrans.eta);
						eroamTrans.provider = 'eroam';
						try{
							console.log('eroamTrans',eroamTrans);
							appendTransport( eroamTrans );
							addTransportTypeId( eroamTrans.transport_type.id )
							totalTransportCount++;
							console.log('e',totalTransportCount);
						}catch( e ){
							console.log('Error on eroam append', e.message );
						}				
			
					});
					
				}
				next();
			});
	}

	function buildMystiflyTransport( next )
	{
		try{

			var mystiflyRQ;
			var mystiflyApiCalls            = [];
			var arrayOfOriginIataCodes      = splitString("<?php echo e($data['from_city_iatas']); ?>", ",");
			var arrayOfDestinationIataCodes = splitString("<?php echo e($data['to_city_iatas']); ?>", ",");

		
			if( arrayOfOriginIataCodes.length > 0 && arrayOfDestinationIataCodes.length > 0 )
			{
				arrayOfOriginIataCodes.forEach(function( originIataCode ){
					arrayOfDestinationIataCodes.forEach(function( destinationIataCode ){
						mystiflyRQ = {
							DepartureDate : "<?php echo e(date('Y-m-d', strtotime($data['departure_date']))); ?>",
							OriginLocationCode : originIataCode,
							DestinationLocationCode : destinationIataCode,
							CabinPreference : 'Y',
							Code : ['ADT'],
							Quantity : [search_session.travellers],
							IsRefundable : flightOptions.IsRefundable,
							IsResidentFare : flightOptions.IsResidentFare,
							NearByAirports : flightOptions.NearByAirports,
							provider: 'mystifly'
						}
						console.log('mystiflyRQ', mystiflyRQ);
						mystiflyApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', mystiflyRQ, 'mystifly', true ) );
					});
				});

				eroam.apiArrayOfPromisesHandler( mystiflyApiCalls, function( mystiflyArrayOfResponses ){
					mystiflyArrayOfResponses.forEach( function( mystiflyResponse ){
					/*	console.log('mystiflyRS', mystiflyResponse);*/
						if( mystiflyResponse != null )
						{
							if( Array.isArray( mystiflyResponse.PricedItineraries.PricedItinerary ) )
							{
								if( mystiflyResponse.PricedItineraries.PricedItinerary.length > 0 )
								{
									var k= 1;
									var transport_count_limit = Math.abs(20 - transport_count);
									mystiflyResponse.PricedItineraries.PricedItinerary.forEach(function( mystiflyTrans ){
										if(transport_count_limit >= k){
											mystiflyTrans.provider     = "mystifly";
											mystiflyTrans.fromCityName = "<?php echo e(get_city_by_id( $data['city_id'] )['name']); ?>";
											mystiflyTrans.toCityName   = "<?php echo e(get_city_by_id( $data['to_city_id'] )['name']); ?>";
											mystiflyTrans.fromCityId   = "<?php echo e($data['city_id']); ?>";
											mystiflyTrans.toCityId     = "<?php echo e($data['to_city_id']); ?>";
											k++;
											try{
												console.log( 'mystifly', mystiflyTrans );
												appendTransport( mystiflyTrans );	
												addTransportTypeId(1);
												totalTransportCount++;
											}catch(e){
												console.log('error occured in mystifly data', e.message);
											}
										}
									});
								}
							}
							else
							{
								var mystiflyTrans = mystiflyResponse.PricedItineraries.PricedItinerary;
								if( mystiflyTrans != null )
								{
									totalTransportCount++;
									mystiflyTrans.provider     = 'mystifly';
									mystiflyTrans.fromCityName = "<?php echo e(get_city_by_id( $data['city_id'] )['name']); ?>";
									mystiflyTrans.toCityName   = "<?php echo e(get_city_by_id( $data['to_city_id'] )['name']); ?>";
									mystiflyTrans.fromCityId   = "<?php echo e($data['city_id']); ?>";
									mystiflyTrans.toCityId     = "<?php echo e($data['to_city_id']); ?>";

									try{
										console.log( 'mystifly', mystiflyTrans );
										appendTransport( mystiflyTrans );	
										addTransportTypeId(1);
									}catch(e){
										console.log('error occured in mystifly data', e.message);
									}
								}
							}

						}
					});

					next();
				});
			}
			else
			{
				next();
			}
		}
		catch(e)
		{
			console.log('An error occured during the mystifly call', e.message);
			next();
		}
	}

	 function buildBusBudTransport( next )
    {
        try{

            var busbudRQ;
            var busbudApiCalls            = [];
            var arrayOfOriginBusbudCodes      = splitString("<?php echo e($data['from_city_iatas']); ?>", ",");
            var arrayOfDestinationBusbudCodes = splitString("<?php echo e($data['to_city_iatas']); ?>", ",");
            var arrayOfOriginBusbudCodes      = "<?php echo e($data['from_city_geohash']); ?>";
            var arrayOfDestinationBusbudCodes = "<?php echo e($data['to_city_geohash']); ?>";


            if( arrayOfOriginBusbudCodes != '' && arrayOfDestinationBusbudCodes != '' )
            {
                busbudRQ = {
                            DepartureDate : "<?php echo e(date('Y-m-d', strtotime($data['departure_date']))); ?>",
                            OriginLocationCode : arrayOfOriginBusbudCodes,
                            DestinationLocationCode : arrayOfDestinationBusbudCodes,
                            adult : search_session.travellers,
                            child : 0,
                            provider: 'busbud'
                        }
                console.log('busbudRQ', busbudRQ);
                busbudApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', busbudRQ, 'busbud', true ) );


                eroam.apiArrayOfPromisesHandler( busbudApiCalls, function( busbudArrayOfResponses ){
                    console.log('busbudResponse', busbudArrayOfResponses);
                    if( busbudArrayOfResponses != '' ){
                    busbudArrayOfResponses.forEach( function( busbudResponse ){


                           // var busbudTrans = busbudResponse;
                            busbudResponse.forEach(function( busbudTrans ){
                            busbudTrans.provider     = "busbud";
                            busbudTrans.fromCityName = "<?php echo e(get_city_by_id( $data['city_id'] )['name']); ?>";
                            busbudTrans.toCityName   = "<?php echo e(get_city_by_id( $data['to_city_id'] )['name']); ?>";
                            busbudTrans.fromCityId   = "<?php echo e($data['city_id']); ?>";
                            busbudTrans.toCityId     = "<?php echo e($data['to_city_id']); ?>";
                            busbudTrans.OriginLocationCode     = "<?php echo e($data['from_city_geohash']); ?>";
                            busbudTrans.DestinationLocationCode = "<?php echo e($data['to_city_geohash']); ?>";
                            busbudTrans.DepartureDate     = "<?php echo e(date('Y-m-d', strtotime($data['departure_date']))); ?>";
                                        try{
                                            console.log( 'busbud', busbudTrans );
                                            appendTransport( busbudTrans );
                                            addTransportTypeId(1);
                                            totalTransportCount++;
                                        }catch(e){
                                            console.log('error occured in busbud data', e.message);
                                        }
                            });

                    });
            }
                    next();
                });
            }
            else
            {
                next();
            }
        }
        catch(e)
        {
            console.log('An error occured during the mystifly call', e.message);
            next();
        }
    }//appendTransport

	function addTransportTypeId( transTypeId )
	{
		if( isNotUndefined( transTypeId ) )
		{
			if( $.inArray( transTypeId, availableTransTypes ) == -1 )
			{
				availableTransTypes.push( transTypeId );
			}
		}
	}

	function showTransportType( next )
	{
		console.log('availableTransTypes', availableTransTypes);
		availableTransTypes.forEach(function( transTypeId ){
			$(".li-trans-type[data-trans-type-id='"+transTypeId+"']").data("show-type","yes").show();
			$('.trans-type[data-transport-type-id="'+transTypeId+'"]').addClass('btn-secondary').data('trans-is-selected', 'true');
			if( transTypeId == 1 )
			{
				$('#flight-options').show();
			}
		});
		next();
	}


	/*
	| Functions Created by Junfel
	*/
	/*
	| get each value for selected transport type
	*/
	function to_array( data, attribute ){
		var to_array = [];
		for ( var counter = 0; counter < data.length; counter++ ) {
	    	to_array.push( data[counter].getAttribute( attribute ) );
	  	}
  		return to_array;
	}
 
	/*
	| funtion to display transport icon
	| return font-awesom icon class.
	*/
	function transport_icon(type){
		var icon = '';
		switch(type){
			case 'Private boat':
			case 'Ferry':
			case 'Ferry or Boat':
			case 'Speed boat':
			case 'Slow boat':
			case 'Boat':
			case 'Jet Boat':
			case 'Cruise':
			case 'Ferry Boat':
				icon = 'fa-ship';
			break;
			case 'Minivan':
			case 'Private Minibus':
			case 'Coach or Minivan':
			case 'Coach Minivan':
			case 'Coach':
			case 'Bus':
				icon = 'fa-bus';
			break;
			case 'Flight':
				icon = 'fa-plane';
			break;
			case 'Private Car (Landcruiser)':
			case 'Private Car or Minivan':
			case 'Private Car Minivan':
			case 'Private car':
			case 'Private Car (Deluxe)':
				icon = 'fa-car';
			break;
			case 'Train':
				icon = 'fa-train';
			break;
			case 'Taxi':
				icon = 'fa-taxi';
			break;	
			case 'Coach + Ferry or Boat':
			case 'Train + Ferry or Boat':
			case 'Train + Minivan':
			case 'Train + Coach':
			case 'Minivan + Ferry or Boat':
				icon = 'fa-plus';
			break;	
			case 'Transport Pass':
				icon = 'fa-id-card';
			break;

			default:
				icon = 'fa-car';
			break;
		}
		return icon;
	}

	/*
	| formatting price 
	| return price with currency code
	*/

	function price_format(price, code){
		var new_price = Math.ceil(price).toFixed(2);
		var new_code = code.replace('D', '$');
		return new_code+' '+new_price;
	}
	/*
	| Temporary filter for the season
	| Return true or false
	*/

	function compare_date(from, price_from, price_to){
		var result = false;
		var from = new Date(from);
		var price_from = new Date(price_from);
		var price_to = new Date(price_to);
		if( from >= price_from && from <= price_to ){
			result = true;
		}
		return result;
	}
	function get_arrival_am_pm(arrival){
		var arrival_split = arrival.split("+"); // since there's a possibility that there's a +, we split the + first
		var arrival_split_am_pm = arrival_split[0].split(" ");
		var arrival_hour = parseInt(arrival_split_am_pm[0]);
		if(arrival){

		}
	}
	/*
	| Function for calculating travel duration
	| Created by Aljun.
	*/
	function calculateTransportDuration(departure,arrival){
		if(departure && arrival){
			departure = $.trim(departure);
			arrival = $.trim(arrival);

			var departure_split = departure.split(" ");
			var departure_split_time = departure_split[0].split(":");
			var departure_hour = parseInt(departure_split_time[0]);
			var departure_min = parseInt(departure_split_time[1]);
			//var departure_sec = departure_split_time[2] !== undefined ? parseInt(departure_split_time[2]) : 0;
			var departure_hour_24 = departure_split[1] == "PM" ? departure_hour+12 : departure_hour;
			//console.log('departure_hour_24 : '+departure_hour_24);
			
			var arrival_split = arrival.split("+"); // since there's a possibility that there's a +, we split the + first
			var arrival_split_am_pm = arrival_split[0].split(" ");
			var arrival_split_hour_min = arrival_split_am_pm[0].split(":");
			var arrival_hour = parseInt(arrival_split_hour_min[0]);
			var arrival_min = parseInt(arrival_split_hour_min[1]);
			var formatted_min = arrival_min / 60;
			//var arrival_sec = arrival_split_am_pm[2] !== undefined ? parseInt(arrival_split_am_pm[2]) : 0;
			var plus_value = arrival_split[1] !== undefined ? arrival_split[1] : 0;
			//var arrival_hour_24 = plus_value == 0 ? arrival_hour+12 : arrival_hour;
			var arrival_hour_24 = arrival_split[1] == "PM" ? arrival_hour+12 : arrival_hour;
			//console.log('arrival_hour_24 : '+arrival_hour_24);


			if(plus_value > 0 ){
				var temp_hours = parseInt(24 - departure_hour_24); //3 
				temp_hours     = parseInt(temp_hours) + parseInt(arrival_hour_24 );
				temp_hours     += plus_value > 1 ? (plus_value - 1) * 24 : 0;
				total_hours = temp_hours + formatted_min;

			}else{

				var departure_time = departure_hour_24 + (departure_min / 60);

				if(departure_time > 12){
					arrival_hour_24 = plus_value == 0 ? arrival_hour+24 : arrival_hour;
				}
				if(arrival_hour > 12){
					arrival_hour_24 =  arrival_hour;
				}
				var arrival_time = arrival_hour_24 + (arrival_min / 60);
				var total_hours = arrival_time - departure_time;
			}
			return total_hours ;
		}else{
			return "";
		}
	}

	function get_hours_min1(hour_min,type,provider){
		//alert(type);
		var hours_mins = hour_min.toString().split(".");
		var hours = parseInt(hours_mins[0]);
		var mins = parseFloat('0.'+hours_mins[1]) * 60;

		if(type == 'Detail' && provider == 'busbud'){ //alert('hi');
			a = parseInt(hour_min);
            var hours = Math.trunc(a/60);
            var minutes = a % 60;
			return hours+' Hour(s) and '+Math.ceil(minutes)+' Minute(s)';
		}else if(type == 'Detail'){
            return hours+' Hour(s) and '+Math.ceil(mins)+' Minute(s)';
		} else {
			return hours+' hr(s) '+Math.ceil(mins)+' min(s)';
		}	
	}

	function arrival_am_pm(eta){

		var result;
		var arrival_split = eta.split("+"); 
		var arrival_split_am_pm = arrival_split[0].split(" ");
		var arrival_split_hour_min = arrival_split[0].split(":");
		var arrival_hour = parseInt(arrival_split_hour_min[0]);

		var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


		if(typeof arrival_split_am_pm[1] !== 'undefined'){

			if(arrival_split_am_pm[1] == 'PM'){
				arrival_hour = arrival_hour + 12;
			}
			result = padd_zero(arrival_hour)+':'+arrival_min;//+' '+arrival_split_am_pm[1];
		}else{
			if( arrival_hour < 12 ){
				result = padd_zero(arrival_hour)+':'+arrival_min;//+' AM';
			}else{
				//hour = arrival_hour - 12;
				result = padd_zero(hour)+':'+arrival_min;//+' PM';
			}
		}
		
		return result;
	}

	function etdFormat(etd){
		var time = etd.replace(":00", "");

		//return parseInt(time[0]) > 12 ? moment(etd, ['HH:mm']).format('hh:mm A') : moment(etd, ["hh:mm A"]).format('hh:mm A') ;
		return parseInt(time[0]) > 12 ? moment(etd, ['HH:mm A']).format('hh:mm') : moment(etd, ["hh:mm A"]).format('HH:mm') ;
	}
	function padd_zero(number) {
		if(parseInt(number) == 0){
			number = 12;
		}
	    return (number < 10) ? ("0" + number) : number;
	}



	function appendTransport( transport )
	{
		var html, airline_code,attributes, transportName, transportTypeName, durationString, supplierName, fromCity, toCity, etd, eta, duration, currency, price = 0, priceString, convertedPrice = 0;
		var selected       = '';
		var selected_trans = '';
		var selected_trans2= '';
		var transportClass = '';
		var isSelected     = 'false';
		var departTimeZone = moment().tz(search_session.itinerary[parseInt(key)].city.timezone.name).format('z Z');
		var arriveTimeZone = moment().tz(search_session.itinerary[parseInt(key)+1].city.timezone.name).format('z Z');

		switch( transport.provider )
		{
			case 'eroam':
				transportTypeName = transport.transport_type.name;
				var operator      = transport.operator.name;
				fromCity          = transport.from_city.name;
				toCity            = transport.to_city.name;
				etd               = transport.etd;
				eta               = transport.eta;
				var formattedEtd  = etdFormat(etd);
				var etdAmPm       = formattedEtd.split(' ');
				duration          = transport.duration.toFixed(2);
				priceId           = 0;
				currency          = 'AUD'
				transportClass    = ( transport.transport_type_id == 1 ) ? 'Cabin Class Not Specified' : '';
				transportName     = operator.toUpperCase()+' / Depart - '+fromCity+' '+formattedEtd+' ( '+ departTimeZone +' ) '+'. Arrive - '+toCity+' '+arrival_am_pm(eta)+ '( '+ arriveTimeZone+' )';
				
				var fliteFromTo   = fromCity+ ' to '+toCity;// + ' '+moment( etd, moment.ISO_8601 ).format('ddd Do MMM YYYY');
				var departTime 	  = formattedEtd;//etd.format('hh:mm');
				var arriveTime 	  = arrival_am_pm(eta);//eta.format('hh:mm')+'<br/> '+eta.format('Do MMM');
				var fliteName	  = operator.toUpperCase();
				var fliteDeparture= fromCity;
				var fliteArrival  = toCity;
				var departTimeDetail= formattedEtd;//etd.format('hh:mm - dddd Do MMM YYYY');
				var arriveTimeDetail= arrival_am_pm(eta);//eta.format('hh:mm - dddd Do MMM YYYY');
				var FlightNumber  = transport.id;

				if( isNotUndefined( transport.price[0] ) )
				{
					currency       = transport.price[0].currency.code;
					price          = transport.price[0].price;
					convertedPrice = eroam.convertCurrency( price, currency );
					priceId        = transport.price[0].id;
				}

				if( selectedProvider == transport.provider )
				{
					if( selectedTransportId == transport.id )
					{
						isSelected     = 'true';
						selected       = 'btn-secondary';
						selected_trans = 'selected-transport';
						selected_trans2= 'gray-bg';

						$("#search-icon").removeClass (function (index, className) {
							return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
						});
						$('#search-icon').addClass( transport_icon(transportTypeName) );
					}
				}
				dataAttributes = [
					'data-provider="eroam" ',
					'data-is-selected="'+isSelected+'" ',
					'data-id="'+transport.id+'" ',
					'data-from-city-id="'+transport.from_city.id+'" ',
					'data-to-city-id="'+transport.to_city.id+'" ',
					'data-transport-id="'+transport.id+'" ',
					'data-transport-type-id="'+transport.transport_type.id+'" ',
					'data-transport-type-name="'+transport.transport_type.name+'" ',
					'data-price="'+price+'" ',
					'data-currency="'+currency+'" ',
					'data-duration="'+transport.duration+'" ',
					'data-price-id="'+priceId+'" ',
					'data-etd="'+etd+'" ',
					'data-eta="'+eta+'" ',
					'data-transport-operator="'+transport.operator.name+'" ',
					'data-am-pm="'+etdAmPm[1]+'" ',
					'data-filter-period="0" ',
					'data-filter-trans="0" '
				].join('');
				break;

			// eeeeeeeeeeeeeeeeeeeeeeeeeeeee
			case 'mystifly':
				airline_code			= transport.ValidatingAirlineCode;
				transportTypeName       = 'Flight';
				fromCity                = transport.fromCityName;
				toCity                  = transport.toCityName;
				var t                   = transport.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment;
				var fareSourceCode      = transport.AirItineraryPricingInfo.FareSourceCode;
				//var airlineName         = ( isNotUndefined( t.OperatingAirline.Name ) ) ? t.OperatingAirline.Name : 'N/A';
				var airlineName         = ( isNotUndefined( transport.ValidatingAirlineName ) ) ? transport.ValidatingAirlineName : 'N/A';
				//var airlineCode         = ( isNotUndefined( t.OperatingAirline.Code ) ) ? t.OperatingAirline.Code : 'N/A';
				var airlineCode         = ( isNotUndefined( transport.ValidatingAirlineCode ) ) ? transport.ValidatingAirlineCode : 'N/A';
				etd                     = t.DepartureDateTime;
				eta                     = t.ArrivalDateTime;
				transportClass          = (t.CabinClassText).trim() != '' ? t.CabinClassText : 'Cabin Class Not Specified';

				var cachedDepartureTime = moment( etd, moment.ISO_8601 ).format('HH:mm');

				var departure           = moment( eroam_data.date_to+"T"+cachedDepartureTime+":00", moment.ISO_8601 );
				var departure2 			= moment( etd, moment.ISO_8601 );

				durationInMins          = parseInt(t.JourneyDuration);
				duration                = ( parseFloat(durationInMins) / 60.0 );
				
				var arrival2             = moment( eta, moment.ISO_8601 );
				var arrival             = moment( departure.add( durationInMins, 'minutes' ) );
				
				currency                = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
				price                   = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount;
				convertedPrice          = eroam.convertCurrency( price, currency );
				
				transportName      		= airlineName.toUpperCase()+', Flight # '+t.FlightNumber+' / Depart - '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure2.format('hh:mm A')+' ( '+ departTimeZone +' ). Arrive - '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+arrival2.format('hh:mm A')+' ( '+ arriveTimeZone +' )';
				
				var fliteFromTo      	= '<span class="textCap" >'+airlineName.toLowerCase()+'</span>, Flight #'+t.FlightNumber+'<br>'+(t.DepartureData != null ? t.DepartureData : fromCity)+ ' to '+(t.ArrivalData != null ? t.ArrivalData : toCity);// + ' '+moment( etd, moment.ISO_8601 ).format('ddd Do MMM YYYY');
				var departTime 		 	= departure2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+departure2.format('Do MMM')+'</span>';
				var arriveTime 		 	= arrival2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+arrival2.format('Do MMM')+'</span>';
				var fliteName			= '<span class="textCap" >'+airlineName.toLowerCase()+'</span>, Flight #'+t.FlightNumber;//irlineName.toUpperCase()+', Flight '+t.FlightNumber;
				var fliteDeparture      = (t.DepartureData != null ? t.DepartureData : fromCity);
				var fliteArrival      	=  (t.ArrivalData != null ? t.ArrivalData : toCity);
				var departTimeDetail 	= departure2.format('HH:mm dddd Do MMM YYYY');
				var arriveTimeDetail 	= arrival2.format('HH:mm dddd Do MMM YYYY');
				var FlightNumber 		= t.FlightNumber;


				//console.log('my data ==> '+t.DepartureDateTime+'//'+t.ArrivalDateTime);

				var arrivalItineraryText = arrival.format('HH:mm A, Do, MMMM YYYY');

				var bookingSummaryText = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+'<br/><small>Depart: '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure.format('Do, MMMM YYYY')+' '+departure2.format('hh:mm A')+'</small><br/><small>Arrive: '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+moment(search_session.itinerary[parseInt(key)+1].city.date_from).format('Do, MMMM YYYY')+' '+arrival2.format('hh:mm A')+'</small>';

				if( selectedProvider == transport.provider )
				{
					if( selectedTransportId.trim() == fareSourceCode.trim() )
					{
						isSelected     = 'true';
						selected       = 'btn-secondary';
						selected_trans = 'selected-transport';
						selected_trans2= 'gray-bg';
						$("#search-icon").removeClass (function (index, className) {
							return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
						});
						$('#search-icon').addClass( transport_icon(transportTypeName) );
					}
				}


				dataAttributes = [
					'data-provider="mystifly" ',
					'data-is-selected="'+isSelected+'" ',
					'data-from-city-id="'+transport.fromCityId+'" ',
					'data-to-city-id="'+transport.toCityId+'" ',
					'data-id="'+fareSourceCode+'" ',
					'data-transport-id="'+fareSourceCode+'" ',
					'data-fare-source-code="'+fareSourceCode+'" ',
					'data-fare-type="'+transport.AirItineraryPricingInfo.FareType+'" ',
					'data-airline-code="'+airlineCode+'" ',
					'data-operating-airline="'+airlineName+'" ',
					'data-transport-operator="'+airlineName+'" ',
					'data-flight-number="'+t.FlightNumber+'" ',
					'data-etd="'+etd+'" ',
					'data-eta="'+eta+'" ',
                    'data-durationMin="'+durationInMins+'" ',
					'data-duration="'+duration+'" ',
					'data-arrival-location="'+t.ArrivalAirportLocationCode+'" ',
					'data-departure-location="'+t.DepartureAirportLocationCode+'" ',
					'data-passenger-type-code="ADT" ',
					'data-passenger-type-quantity="'+search_session.travellers+'" ',
					'data-arrival-data="'+(t.ArrivalData != null ? t.ArrivalData : toCity)+'" ',
					'data-departure-data="'+(t.DepartureData != null ? t.DepartureData : fromCity)+'" ',
					'data-transport-type-id="1" ',
					'data-transport-type-name="'+transportTypeName+'" ',
					'data-price="'+price+'" ',
					'data-currency="'+currency+'" ',
					'data-cabin-class="'+transportClass+'" ',
					'data-booking-summary-text="'+bookingSummaryText+'" ',
					'data-arrival-itinerary-text="'+arrivalItineraryText+'" ',
					'data-am-pm="'+ departure2.format('A') +'" ',
					'data-filter-period="0" ',
					'data-filter-trans="0" ',
					'data-is-passport-mandatory="'+transport.IsPassportMandatory+'" '
				].join('');
				break;

				case 'busbud':

                transportTypeName = 'Coach Minivan';
                var operator      = transport.transportTypeName;
                fromCity          = transport.fromCityName;
                toCity            = transport.toCityName;
                duration          = transport.duration;
                priceId           = 0;
                currency          = transport.currency;
                transportClass    = transport.transportClass;
                durationString    = transport.timeInHours;
                var departTime1 	  = transport.departure_time;//etd.format('hh:mm');
                var arriveTime1 	  = transport.arrival_time;//eta.format('hh:mm')+'<br/> '+eta.format('Do MMM');

                //transportName     = operator.toUpperCase()+' / Depart - '+fromCity+' '+departTime+'. Arrive - '+toCity+' '+arriveTime+ '';

                var fliteFromTo   = fromCity+ ' to '+toCity;// + ' '+moment( etd, moment.ISO_8601 ).format('ddd Do MMM YYYY');

               // var fliteFromTo   = transport.fromCityName;
                //var fliteFromTo   = '<span class="textCap" >'+airlineName.toLowerCase()+'</span>, Flight #'+t.FlightNumber+'<br>'+(t.DepartureData != null ? t.DepartureData : fromCity)+ ' to '+(t.ArrivalData != null ? t.ArrivalData : toCity);// + ' '+moment( etd, moment.ISO_8601 ).format('ddd Do MMM YYYY');
                var fliteName	  = operator.toUpperCase();
                var fliteDeparture= fromCity;
                var fliteArrival  = toCity;
                //var departTimeDetail= departTime;//etd.format('hh:mm - dddd Do MMM YYYY');
                //var arriveTimeDetail= arriveTime;//eta.format('hh:mm - dddd Do MMM YYYY');
                var FlightNumber  = transport.transportId;

                etd                     = transport.departure_time1;
                eta                     = transport.arrival_time1;

                var departure2 	  = moment( etd, moment.ISO_8601 );
                var departTime 	  = departure2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+departure2.format('Do MMM')+'</span>';
                var arriveTime2   = moment( eta, moment.ISO_8601 );
                var arriveTime 	  = arriveTime2.format('HH:mm')+'<br/><span style="font-size: 12px;">'+arriveTime2.format('Do MMM')+'</span>';

                var departTimeDetail 	= departure2.format('HH:mm dddd Do MMM YYYY');
                var arriveTimeDetail 	= arriveTime2.format('HH:mm dddd Do MMM YYYY');
                currency                = transport.currency;
                price                   = transport.price;
                convertedPrice          = eroam.convertCurrency( price, currency );

                if( selectedProvider == transport.provider )
                {
                    if( selectedTransportId.trim() == transport.transportId.trim() )
                    {
                        isSelected     = 'true';
                        selected       = 'btn-secondary';
                        selected_trans = 'selected-transport';
                        selected_trans2= 'gray-bg';
                        $("#search-icon").removeClass (function (index, className) {
                            return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                        });
                        $('#search-icon').addClass( transport_icon(transportTypeName) );
                    }
                }
                dataAttributes = [
                    'data-provider="busbud" ',
                    'data-is-selected="'+isSelected+'" ',
                    'data-id="'+transport.transportId+'" ',
                    'data-from-city-id="'+transport.fromCityId+'" ',
                    'data-to-city-id="'+transport.toCityId+'" ',
                    'data-transport-id="'+transport.transportId+'" ',
                    'data-transport-type-id="'+transport.transportId+'" ',
                    'data-transport-type-name="Bus" ',
                    'data-price="'+price+'" ',
                    'data-etd="'+etd+'" ',
                    'data-eta="'+eta+'" ',
                    'data-currency="'+currency+'" ',
                    'data-durationMin="'+transport.duration+'" ',
                    'data-duration="'+transport.duration+'" ',
                    'data-etd="'+departTime1+'" ',
                    'data-eta="'+arriveTime1+'" ',
                    'data-transport-operator="'+transport.transportTypeName+'" ',
                    'data-filter-period="0" ',
                    'data-OriginLocationCode="'+transport.OriginLocationCode+'" ',
                    'data-DestinationLocationCode="'+transport.DestinationLocationCode+'" ',
                    'data-DepartureDate="'+transport.DepartureDate+'" ',
                    'data-transport-type-id="'+transport.transportId+'" ',
                    'data-filter-trans="0" '
                ].join('');
                break;

		}
		
		if( transport.provider == 'busbud'){
            durationString = durationString;
		}else{
            durationString = get_hours_min1( duration,'head',transport.provider);
		}
		
		// var convertedPricePerPerson = Math.ceil( convertedPrice / parseFloat( search_session.travellers ) );
		maxPrice = parseInt( Math.ceil( convertedPrice ) ) > maxPrice ? parseInt( Math.ceil( convertedPrice ) ) : maxPrice;
		priceString = globalCurrency + ' ' + Math.ceil( convertedPrice ).toFixed(2);

		/*html = '<div class="col-md-12 transport-list '+selected_trans+'" '+dataAttributes+'>';
		html += '<div class="row">';
		html += '<div class="col-md-2">';
		html += '<i class="fa icon-style fa-check white select-transport '+selected+'" '+dataAttributes+'></i> '; 
		html += ' <i class="icon-style fa '+transport_icon(transportTypeName)+' fa-2x" ></i>';  
		html += '</div>';
		html += '<div class="col-md-8 zero-left"> <p class="transport-details"> <strong class="blue-txt"> '+priceString+' Per Person </strong><br />';
	 	html += transportName+' </p>';
		html += '</div>';
	 	html += '<div class="col-md-2" style="padding:0;">';
	 	html += '<i class="fa fa-clock-o" style="font-size: 17px;" ></i> '+durationString+'<br/>'+transportClass;
	 	html += '</div>';
		html += '</div>';
		html += '</div>';*/


		html = '<div class="panel panel-default transport-list '+selected_trans+'" '+dataAttributes+'>'+
	        		'<div class="panel-heading '+selected_trans2+'" role="tab" id="headingPicks'+FlightNumber+'" role="button" data-toggle="collapse" data-parent="#transportList" href="#collapsePicks'+FlightNumber+'" aria-expanded="true" aria-controls="collapsePicks'+FlightNumber+'">'+
	          			'<div class="panel-title row text-center">'+
	            			'<div class="col-md-1 col-sm-4 col-xs-6 transport-img transport-border">';
	    if(airline_code == '' || airline_code == undefined){
	    	html+='<i class="fa '+transport_icon(transportTypeName)+' fa-3x pad10_0 "></i>';

	    }else{
	    	html+='<div class="transport-img-center"><img class="img-responsive" src="http://pics.avs.io/100/50/'+airline_code+'.png"/></div>';
	    }

	    html+='</div>'+
				            '<div class="col-md-5 col-sm-4 col-xs-6 transport-border text-left">'+
				              	'<p><i class="fa '+transport_icon(transportTypeName)+' fa-1x"></i><strong>  '+transportTypeName+'</strong></p>'+
				              	'<p style="padding-left:15px;">'+fliteFromTo+'</p>'+
				            '</div>'+
				            '<div class="col-md-1 col-sm-4 col-xs-6 transport-border">'+
				              	'<p><strong>Depart</strong></p>'+
					            '<p class="m-t-10">'+departTime+'</p>'+
				            '</div>'+
				            '<div class="col-md-1 col-sm-4 col-xs-6 transport-border">'+
				              	'<p><strong>Arrive</strong></p>'+
				              	'<p class="m-t-10">'+arriveTime+'</p>'+
				            '</div>'+
				            '<div class="col-md-2 col-sm-4 col-xs-6 transport-border">'+
				              	'<p><strong>Duration</strong></p>'+
				              	'<p class="m-t-10">'+ durationString +'<br/>'+ transportClass +'</p>'+
				            '</div>'+
				            /*'<div class="col-md-1 col-sm-4 col-xs-6 transport-border">'+
				              	'<p><strong>Stops</strong></p>'+
				              	'<p class="m-t-10">-</p>'+
				            '</div>'+
				            '<div class="col-md-1 col-sm-4 col-xs-6 transport-border">'+
				              	'<p><strong>Details</strong></p>'+
				              	'<p class="m-t-10"><a role="button" data-toggle="collapse" data-parent="#transportList" href="#collapsePicks'+FlightNumber+'" aria-expanded="true" aria-controls="collapsePicks'+FlightNumber+'">More</a></p>'+
				            '</div>'+*/
				            '<div class="col-md-2 col-sm-4 col-xs-6 transport-border">'+
				              	'<p><strong>Price</strong></p>'+
				              	'<p class="m-t-10">'+ priceString +'</p>'+
				            '</div>'+
	          			'</div>'+
	        		'</div>'+
	        		'<div id="collapsePicks'+FlightNumber+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPicks'+FlightNumber+'">'+
	          			'<div class="panel-body">';
	          			html +='<div class="loc-icon">';
	          			html+='<i class="fa '+transport_icon(transportTypeName)+' fa-1x"></i>';
	          			//  if(airline_code == '' || airline_code == undefined){
	          			//  	html+='<i class="fa '+transport_icon(transportTypeName)+' fa-1x"></i>';
	          			// }else{
	          			// 	html+='<img class="img-responsive" src="http://pics.avs.io/100/50/'+airline_code+'.png"/>';
	          			// }

	          			html +='</div><div class="location-content">'+
			            		'<h5><strong>'+ transportTypeName+'</strong></h5><p>'+fliteName+'</p>'+  //
					            '<div class="row">'+
					              	'<div class="col-sm-8">'+
					                	'<div class="row">'+
					                  		'<div class="col-md-2"><strong>Departs:</strong></div>'+
					                  		'<div class="col-md-10">'+fliteDeparture+' - '+departTimeDetail+'</div>'+
					                	'</div>'+

					                	'<div class="row">'+
					                		'<div class="col-md-2"><strong>Arrives:</strong></div>'+
					                  		'<div class="col-md-10">'+fliteArrival+' - '+arriveTimeDetail+'</div>'+
					                	'</div>'+

					                	'<div class="row m-t-10">'+
					                  		'<div class="col-md-2"><strong>Duration:</strong></div>'+
					                  		'<div class="col-md-10">'+get_hours_min1( duration,"Detail",transport.provider)+'</div>'+
					                	'</div>'+
					                	'<div class="row">'+
					                  		'<div class="col-md-2"><strong>Class:</strong></div>'+
					                  		'<div class="col-md-10">'+transportClass+'</div>'+
					                	'</div>'+
					                	'<div class="row">'+
						                	'<div class="col-md-2"><strong>Baggage:</strong></div>'+
						                  	'<div class="col-md-10"><i>Coming Soon</i></div>'+
						                '</div>'+
					              	'</div>'+
					              	'<div class="col-sm-4"><div class="transport-right-price">'+
					              		'<div class="row">'+
					                  		'<div class="col-sm-5 price-sign">'+
					                    		'<h5>'+globalCurrency+'</h5>'+
					                  		'</div>'+
					                  		'<div class="col-sm-7">'+
					                    		'<h3 class="transport-price"><strong>'+Math.ceil( convertedPrice ).toFixed(2)+'</strong></h3>'+
					                    		'from, per person'+
					                  		'</div>'+
					                	'</div>'+

					                	'<div class="transportButton m-t-30" '+dataAttributes+'>'+
						                	'<button type="button" name="" class="btn btn-primary btn-block select-transport '+selected+'" '+dataAttributes+'>ADD TO ITINERARY</button>'+
						              	'</div></div>'+

					              	'</div>'+
					            '</div>'+
					            /*'<div class="row m-t-30">'+
					              	'<div class="col-md-4 col-sm-5">'+
					                	'<div class="row">'+
					                  		'<div class="col-sm-5 price-sign">'+
					                    		'<h5>'+globalCurrency+'</h5>'+
					                  		'</div>'+
					                  		'<div class="col-sm-7">'+
					                    		'<h3 class="transport-price"><strong>'+Math.ceil( convertedPrice ).toFixed(2)+'</strong></h3>'+
					                    		'from, per person'+
					                  		'</div>'+
					                	'</div>'+
					              	'</div>'+
					              	'<div class="col-md-8 col-sm-7 m-t-5 transportButton" '+dataAttributes+'>'+
					                	'<button type="button" name="" class="btn btn-primary btn-block select-transport '+selected+'" '+dataAttributes+'>ADD TO ITINERARY</button>'+
					              	'</div>'+
					            '</div>'+*/
					            '<p class="notes m-t-30">All prices include GST, Government taxes and are one way per person in Australian dollars. Payment fees may apply depending on your payment method. Additional Servicing Fee and / or Booking Price Guarantee can include multiple passengers and products. Total price for all passengers will be shown after you select your flight(s). To view the fare rules, <a href="javascript://" >click here</a>.</p>'+
			          		'</div>'+
			          	'</div>'+
	        		'</div>'+
	      		'</div>';

		if( selected_trans != '' )
		{
			$('#transportList').prepend(html);
		}
		else
		{
			$('#transportList').append(html);
		}	
	}


	function formatDate(date, time = false)
	{
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		if(time){
			day += ' '+ d.getHours();
		}
		return [year, month, day].join('-');
	}

	function hideTransType( next )
	{
		$(".li-trans-type").data('show-type', 'no').hide();
		next();
	}

	function hidePageLoader( next )
	{
		$( '#transport-loader' ).fadeOut(300);
		next();
	}

	function checkTransportCount( next )
	{
		$('.trans-count').html( ': Top ' + totalTransportCount + ( (totalTransportCount == 1 || totalTransportCount == 0) ? ' Transport' : ' Transports' )+ ' Found');
		if( totalTransportCount == 0 )
		{
			var html = '<h4 class="text-center">No Transport Found.</h4>';
			$('#transportList').append( html );
		}
		next();
	}
	/*
	| Added by Junfel
	| get departure date and time(24 hour format )
	*/
	function getDayTime(arrivalDate, duration) {
		var hours_mins = duration.split(':');
		var result = new Date(arrivalDate);
		result.setHours(result.getHours() - parseInt(hours_mins[0]));
		result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));

		return formatDate(result, true);
	}
	/*
	| Added by Junfel
	| remove "hr(s)" and "min(s)"
	*/
	function formatTime(time){
		return time.replace(/hr\(s\)|min\(s\)| /gi, function(x){
			return x == 'hr(s)' ? ':' : '';
		});
	}
	/*
	| Added by Junfel
	| for activity cancellation when transport conflicts with activity.
	*/
	function cancelActivity(departureDate){
		$.each(search_session.itinerary, function (key, itinerary) {
			var temp  = itinerary.activities;
	  		if(itinerary.city.id == eroam_data.city_ids[0] && itinerary.activities.length > 0){
	  			$.each(itinerary.activities, function (k, activity) {
	  				if( typeof activity != 'undefined' ){
	  					if( activity.date_selected == departureDate ){
			  				temp.splice(k,1);
			  			}
	  				}
				});
		    }
		});
		bookingSummary.update( JSON.stringify( search_session ) );
	}

	function saveTransport(transportData){
		$( transportData ).find('.select-transport').attr('data-is-selected', 'true');
			var data = $( transportData ).find('.select-transport').data();
			var contentMsg = 'Are you sure you want to book this transport?';

			switch( data.provider )
			{
				case 'eroam':

					var transport                    = JSON.parse( JSON.stringify( data ) );
					transport.transport_type_id      = data.transportTypeId;
					transport.transporttype          = {};
					transport.transporttype.id       = data.transportTypeId;
					transport.transporttype.name     = data.transportTypeName;
					transport.operator               = {};
					transport.operator.name          = data.transportOperator;
					transport.price                  = [];
					transport.price[0]               = {};
					transport.price[0].price         = data.price;
					transport.price[0].id            = data.priceId;
					transport.price[0].currency      = {};
					transport.price[0].currency.code = globalCurrency;
					transport.provider               = 'eroam';
					//console.log(transport);
					
					$('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
					$( transportData ).find('.select-transport').toggleClass('btn-secondary');
					/*
					| Added by junfel
					*/

					$("#search-icon").removeClass (function (index, className) {
						return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
					});
					$('#search-icon').addClass( transport_icon(transport.transporttype.name) );
					/*
					| End Junfel
					*/
					search_session.itinerary[key].transport = convertToSnakeCase( transport );
					bookingSummary.update( JSON.stringify( search_session ) );		

					$('.transport-list').removeClass('selected-transport');
					$('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

					$('.transport-list').find('.panel-heading').removeClass('gray-bg');
					$('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').find('.panel-heading').addClass('gray-bg');

					break;

				// COMMENT OUT GET FLIGHT RULES CODE FOR ANTHONY'S HONGKONG PRESENTATION; 
				// case 'mystifly':
				
				// 	$('.flight-rules-agree').data( data );

				// 	$('.modal-size').removeClass('modal-sm');
				// 	if( (data.fareType).trim() != 'Public' )
				// 	{
				// 		$('.modal-size').addClass('modal-sm');	
				// 	}
				// 	else
				// 	{
				// 		$('.modal-size').addClass('modal-lg');
				// 	}

				// 	$('#flightRules').modal('show');
				// 	mystiflyRQ = { FareSourceCode : data.fareSourceCode };
				// 	var mystiflyApiCall = eroam.apiDeferred('mystifly/fare-rules', 'GET', mystiflyRQ, 'mystifly', true);
				// 	eroam.apiPromiseHandler( mystiflyApiCall, function( mystiflyResponse ){
				// 		console.log("$('.flight-rules-agree').data()", $('.flight-rules-agree').data());
				// 		console.log( 'mystiflyResponse fareRules', mystiflyResponse );
				// 		if( mystiflyResponse ) // check if 
				// 		{		
				// 			if( data.airlineCode == mystiflyResponse.FareRules.FareRule.Airline )
				// 			{
				// 				try
				// 				{
				// 					var ruleDetail = mystiflyResponse.FareRules.FareRule.RuleDetails.RuleDetail;
				// 					if( (data.fareType).trim() != 'Public' && validURL( ruleDetail.Rules ) )
				// 					{
				// 						var html = 'I have read and agree to abide by the <a href="'+ruleDetail.Rules+'" target="_blank"><b>Rules and Conditions</b></a> for this flight.';
				// 						$('.rules-content').html( html );
				// 					}
				// 					else
				// 					{
				// 						if( Array.isArray( ruleDetail ) )
				// 						{
				// 							var htmlRules = '<div style="padding:15px;">';
				// 							ruleDetail.forEach(function(elem, key){
				// 								htmlRules += [
				// 									'<h4>'+elem.Category+'</h4>',
				// 									'<p>'+elem.Rules+'</p>',
				// 									( key == (ruleDetail.length - 1) ? '' : '<hr/>' )

				// 								].join('');
				// 							});
				// 							htmlRules += '</div>';
				// 							$('.rules-content').html( htmlRules );						
				// 						}
				// 						else
				// 						{
				// 							$('.rules-content').html( ruleDetail.Rules );		
				// 						}
										
				// 					}
				// 					$('.flight-rules-agree').prop( "disabled", false );
				// 				}
				// 				catch( e )
				// 				{
				// 					console.log( 'An error occured getting the flight rules', e.message );
				// 					console.log( 'The response causing the error', mystiflyResponse );
				// 					$('#flightRules').modal('hide');
				// 				}
				// 			}
				// 			else
				// 			{
				// 				$('#flightRules').modal('hide');
				// 			}
				// 		}
				// 		else
				// 		{
				// 			$('#flightRules').modal('hide');
				// 		}
				// 	});
				// 	break;


				case 'mystifly':

					var transport                    = JSON.parse( JSON.stringify( data ) );
					transport.transport_type         = {};
					transport.transport_type.id      = data.transportTypeId;
					transport.transport_type.name    = data.transportTypeName;
					transport.transporttype          = {};
					transport.transporttype.id       = data.transportTypeId;
					transport.transporttype.name     = data.transportTypeName;
					transport.price                  = [];
					transport.price[0]               = {};
					transport.price[0].price         = data.price;
					transport.price[0].id            = data.id;
					transport.price[0].currency      = {};
					transport.price[0].currency.code = globalCurrency;

					$('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
					$( transportData ).find('.select-transport').toggleClass('btn-secondary');

					/*
					| Added by junfel
					*/
					$("#search-icon").removeClass (function (index, className) {
						return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
					});
					$('#search-icon').addClass( transport_icon(transport.transporttype.name) );
					/*
					| End Junfel
					*/
					
					search_session.itinerary[key].transport = convertToSnakeCase( transport );
					//console.log(search_session.itinerary[key].transport);
					bookingSummary.update( JSON.stringify( search_session ) );		

					$('.transport-list').removeClass('selected-transport');
					$('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

					$('.transport-list').find('.panel-heading').removeClass('gray-bg');
					$('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').find('.panel-heading').addClass('gray-bg');

					break;

				case 'busbud':

                    var transport                    = JSON.parse( JSON.stringify( data ) );
                    transport.transport_type         = {};
                    transport.transport_type.id      = data.transportTypeId;
                    transport.transport_type.name    = data.transportTypeName;
                    transport.transporttype          = {};
                    transport.transporttype.id       = data.transportTypeId;
                    transport.transporttype.name     = data.transportTypeName;
                    transport.price                  = [];
                    transport.price[0]               = {};
                    transport.price[0].price         = data.price;
                    transport.price[0].id            = data.id;
                    transport.price[0].currency      = {};
                    transport.price[0].currency.code = globalCurrency;

                    $('.select-transport').removeClass('btn-secondary').attr('data-is-selected', 'false');
                    $( transportData ).find('.select-transport').toggleClass('btn-secondary');

					/*
					 | Added by junfel
					 */
                    $("#search-icon").removeClass (function (index, className) {
                        return (className.match (/(^|\s)fa-\S+/g) || []).join(' ');
                    });
                    $('#search-icon').addClass( transport_icon(transport.transporttype.name) );
					/*
					 | End Junfel
					 */

                    search_session.itinerary[key].transport = convertToSnakeCase( transport );
                    //console.log(search_session.itinerary[key].transport);
                    bookingSummary.update( JSON.stringify( search_session ) );

                    $('.transport-list').removeClass('selected-transport');
                    $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').addClass('selected-transport');

                    $('.transport-list').find('.panel-heading').removeClass('gray-bg');
                    $('.transport-list[data-provider="'+data.provider+'"][data-transport-id="'+data.transportId+'"]').find('.panel-heading').addClass('gray-bg');

                    break;	
					
			}
	}
	function rangeSlider(next){
		var min = parseInt( eroam.convertCurrency(0, 'AUD') );
		//var max = parseInt( eroam.convertCurrency(50000, 'AUD') );
		var max = parseInt( maxPrice );
		
		$( '#slider-range' ).slider({
			range: true,
			step: 5,
			min: min,
			max: max,
			values: [ min, max ],
			slide: function( event, ui ) {
				$( '#price-from' ).val(ui.values[ 0 ]);
				$( '#price-to' ).val(ui.values[ 1 ]);
			}
		});
		//$( '#price-from' ).val($( '#slider-range' ).slider( 'values', 0 ));
		//$( '#price-to' ).val($( '#slider-range' ).slider( 'values', 1 ));

		$( '#price-from' ).val(0);
		$( '#price-to' ).val(5000);

		$('.curr').text(globalCurrency);

		next();
	}

	function calculateHeight(){

        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        if(winHeight < oheight){ 
          $(".page-content .tabs-content-container").outerHeight(elemHeight);
        } else{
          $(".page-content .tabs-content-container").outerHeight(winelemHeight);
        }
      }
       

	/***************************** END FUNCTIONS ****************************/
    /**************************** start sorting function ********************/
    $(document).on('click', '#priceSortAsc', function() { //alert(1);
        sortMeBy('data-price', 'div.tabs-content-wrapper', 'div.transport-list', 'asc');
        $('#priceSortAsc span').removeClass('fa-caret-down').addClass('fa-caret-up');
        $('#priceSortAsc').attr('id', 'priceSortDesc');

    });
    $(document).on('click', '#priceSortDesc', function() {
        $('#priceSortDesc span').removeClass('fa-caret-up').addClass('fa-caret-down');
        sortMeBy('data-price', 'div.tabs-content-wrapper', 'div.transport-list', 'desc');
        $('#priceSortDesc').attr('id', 'priceSortAsc');
    });
    $(document).on('click', '#durationAsc', function() { //alert(1);
        sortMeBy('data-durationmin', 'div.tabs-content-wrapper', 'div.transport-list', 'asc');
        $('#durationAsc span').removeClass('fa-caret-down').addClass('fa-caret-up');
        $('#durationAsc').attr('id', 'durationDesc');
    });
    $(document).on('click', '#durationDesc', function() {
        $('#durationDesc span').removeClass('fa-caret-up').addClass('fa-caret-down');
        sortMeBy('data-durationmin', 'div.tabs-content-wrapper', 'div.transport-list', 'desc');
        $('#durationDesc').attr('id', 'durationAsc');
    });

    function sortMeBy(arg, sel, elem, order) {
        var $selector = $(sel),
            $element = $selector.children(elem);

        $element.sort(function(a, b) {
            var an = parseInt(a.getAttribute(arg)),
                bn = parseInt(b.getAttribute(arg));

            if (order == 'asc') {
                if (an > bn)
                    return 1;
                if (an < bn)
                    return -1;
            } else if (order == 'desc') {
                if (an < bn)
                    return 1;
                if (an > bn)
                    return -1;
            }
            return 0;
        });

        $element.detach().appendTo($selector);
    }
    /**************************** end sorting function ********************/
    /**************************** start search function ********************/
    $(document).on('click', '#searchProvider', function() {
        $("#transport-tab").children().removeClass("active");
        var provider = $(this).text(); //alert(provider);
        filterListOperator(provider);
        $("#provider-tab").children().removeClass("active");
        $(this).parent().addClass("active");
    });
    function filterListOperator(value) {
        var list = $("div.tabs-content-wrapper div.transport-list");
        $(list).fadeOut("fast");
        var totalCount = 0;
        if (value == "All") {
            $("div.tabs-content-wrapper").find("div.transport-list").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");
            });
        } else { //alert(1);
            //Notice this *=" <- This means that if the data-category contains multiple options, it will find them
            //Ex: data-category="Cat1, Cat2"
            $("div.tabs-content-wrapper").find("div.transport-list[data-transport-operator*='" + value + "']").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");

            });
        }
        checkTransportCountForSearch(totalCount);
    }
    $(document).on('click', '#transportProvider', function() {
        $("#provider-tab").children().removeClass("active");
        var provider = $(this).text(); //alert(provider);
        filterListTransport(provider);
        $("#transport-tab").children().removeClass("active");
        $(this).parent().addClass("active");
    });
    function filterListTransport(value) {
        var list = $("div.tabs-content-wrapper div.transport-list");
        $(list).fadeOut("fast");
        var totalCount = 0;
        if (value == "All") {
            $("div.tabs-content-wrapper").find("div.transport-list").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");
            });
        } else { //alert(1);
            //Notice this *=" <- This means that if the data-category contains multiple options, it will find them
            //Ex: data-category="Cat1, Cat2"
            $("div.tabs-content-wrapper").find("div.transport-list[data-transport-type-name*='" + value + "']").each(function (i) {
                totalCount ++;
                $(this).delay(200).slideDown("fast");

            });
        }
        checkTransportCountForSearch(totalCount);
    }
    function checkTransportCountForSearch(totalTransportCount)
    {
        $('.trans-count').html( ': Top ' + totalTransportCount + ( (totalTransportCount == 1 || totalTransportCount == 0) ? ' Transport' : ' Transports' )+ ' Found');
        if( totalTransportCount == 0 )
        {
            var html = '<h4 class="text-center">No Transport Found.</h4>';
            $('#transportList').append( html );
        }
        //next();
    }

    /**************************** end search function ********************/

    function getDistinctProvider(){
        var items = {};
        $('div.transport-list').each(function() {
            items[$(this).attr('data-transport-operator')] = true;
        });
        var html = '';
        var result = new Array();
        for(var i in items)
        {
            if(i != 'N/A'){
                result.push(i);
                html += '<li><a id="searchProvider">'+i+'</a></li>';
			}

        }
        $('#provider-tab').append( html );
        //alert(result);

	}
</script> 
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>