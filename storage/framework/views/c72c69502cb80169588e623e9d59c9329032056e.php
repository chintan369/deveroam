<?php $__env->startSection('custom-css'); ?>
<style>
	.flaticon:before {
		font-size: 15rem;
	}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<div class="container" style="min-height: 500px;">
		<div class="row">
			<div class="text-center white-box col-md-6 col-md-offset-3" style="padding-top: 15%;">
				<i class="flaticon flaticon-island"></i>
				<h1> <?php echo e($exception->getMessage()); ?></h1>
				<h4><a href="<?php echo e(url( '/' )); ?>"><i class="fa fa-long-arrow-left"></i> Return to homepage</a></h4>
				<br>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make( 'layouts.static' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>