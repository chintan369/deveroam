<?php echo $__env->make('user.search_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $__env->startSection('content'); ?>
<section>
    <?php echo $__env->make('user.profile_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    <?php echo $__env->make('user.profile_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    <?php if(session()->has('profile_step4_success')): ?>
                        <p class="success-box m-t-30">
                            <?php echo e(session()->get('profile_step4_success')); ?>

                        </p>
                    <?php endif; ?>
                    <?php if(session()->has('profile_step4_error')): ?>
                        <p class="danger-box m-t-30">
                            <?php echo e(session()->get('profile_step4_error')); ?>

                        </p>
                    <?php endif; ?>
                    <div class="m-t-30">
                        <h2 class="profile-title">Your Contact Preferences</h2>
                        <form class="form-horizontal m-t-20" method="post" action="<?php echo e(url('profile_step4')); ?>" id="profile_step4_form">
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Email Address</label>
                                <input type="text" name="email" readonly value="<?php echo e($user->customer->email); ?>" class="form-control" placeholder="kane@eroam.com" />
                            </div>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Contact Phone Number</label>
                                <input type="text" name="contact_no" class="form-control" placeholder="0401070744" value="<?php echo e($user->customer->contact_no); ?>"/>
                                <?php if($errors->has('contact_no')): ?> 
                                    <span class="error active"><?php echo e($errors->first('contact_no')); ?></span>
                                <?php endif; ?>
                            </div>  
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Mobile Number</label>
                                <input type="text" name="mobile_no" class="form-control" placeholder="9878654532" value="<?php echo e($user->customer->mobile_no); ?>"/>
                                <?php if($errors->has('mobile_no')): ?> 
                                    <span class="error active"><?php echo e($errors->first('mobile_no')); ?></span>
                                <?php endif; ?>
                            </div>       
                            <div class="panel-form-group form-group">
                                <label class="label-control">Preferred Contact Method</label>
                                <select class="form-control" name="pref_contact_method">
                                    <option value="" >Select Preferred Contact Method</option>
                                    <option value="phone_call" <?php echo e($user->customer->pref_contact_method == 'phone_call' ? 'selected' : ''); ?>>Phone Call</option>
                                    <option value="sms" <?php echo e($user->customer->pref_contact_method == 'sms' ? 'selected' : ''); ?>>SMS</option>
                                    <option value="email" <?php echo e($user->customer->pref_contact_method == 'email' ? 'selected' : ''); ?>>Email</option>
                                    <option value="mail" <?php echo e($user->customer->pref_contact_method == 'mail' ? 'selected' : ''); ?>>Mail</option>
                                </select>
                                <?php if($errors->has('pref_contact_method')): ?> 
                                    <span class="error active"><?php echo e($errors->first('pref_contact_method')); ?></span>
                                <?php endif; ?>
                            </div>     
                            <h2 class="profile-title">Physical Address</h2><br>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Address Line One (1)</label>
                                <input type="text" name="phy_address_1" value="<?php echo e($user->customer->phy_address_1); ?>" class="form-control phy_address_1" placeholder="Street Address, P.O Box, Company Name" />
                                <?php if($errors->has('phy_address_1')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_address_1')); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Address Line Two (2)</label>
                                <input type="text" name="phy_address_2" value="<?php echo e($user->customer->phy_address_2); ?>" class="form-control phy_address_2" placeholder="Street Address, P.O Box, Company Name" />
                                <?php if($errors->has('phy_address_2')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_address_2')); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="panel-form-group form-group">
                                <label class="label-control">Country</label>
                                <select id="phy_country" name="phy_country" class="form-control phy_country">
                                    <option value="">Select Country</option>
                                    <?php foreach($all_countries as $country): ?>
                                        <option value="<?php echo e($country['id']); ?>" <?php echo e($user->customer->phy_country == $country['id'] ? 'selected' : ''); ?>><?php echo e($country['name']); ?></option>
                                    <?php endforeach; ?>                                       
                                </select>
                                <?php if($errors->has('phy_country')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_country')); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">State</label>
                                <input type="text" name="phy_state" value="<?php echo e($user->customer->phy_state); ?>" class="form-control phy_state" placeholder="State" />
                                <?php if($errors->has('phy_state')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_state')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">City</label>
                                <input type="text" name="phy_city" value="<?php echo e($user->customer->phy_city); ?>" class="form-control phy_city" placeholder="City" />
                                <?php if($errors->has('phy_city')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_city')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Zip</label>
                                <input type="text" name="phy_zip" value="<?php echo e($user->customer->phy_zip); ?>" class="form-control phy_zip" placeholder="Zip Code" />
                                <?php if($errors->has('phy_zip')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_zip')); ?></span>
                                <?php endif; ?>
                            </div>   

                            <div class="m-t-20">
                                <button type="submit" name="" class="btn btn-black btn-block">UPDATE PROFILE</button>
                            </div>
                            <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>">
                            <input type="hidden" name="step" value="1">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
<section>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_leftmenu.js?v='.$version.'')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_step4.js?v='.$version.'')); ?>"></script>
<?php echo $__env->make('layouts.profileLayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>