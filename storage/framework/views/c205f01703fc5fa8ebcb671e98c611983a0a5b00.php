<?php 
    $legkey =  explode('?', $_SERVER['HTTP_REFERER']); 

    if(count($legkey)>1) {
      $legkey =  explode('&', $legkey[1]);
      $legkey =  explode('=', $legkey[0]);
      $legkey =  $legkey[1];
    } else {
      $legkey =  0;
    }

    $preUrl = url()->previous();
    $isMap = 0;
    if(substr($preUrl, -3) == 'map'){
       $isMap = 1;
    }
     
    $preUrl = str_replace(url('').'/', '', $preUrl);

    $countcity =  count(session()->get( 'search' )['itinerary']) - 1;
    $last = count(session()->get('search')['itinerary']) - 1;

    //echo $function = Request::segment(1);
?>

<script type="text/javascript">
  function calculateWidth(){
    var owidth = $('.custom-tabs .nav-tabs').parent().width();
    $(".tabs-container .custom-tabs .nav-tabs>li").css('padding',0);
    var elem = $('.tabs-container .custom-tabs .nav-tabs>li').length;
    var elemWidth = (owidth / elem );
    //alert(owidth+'//'+elem+'=='+elemWidth);
    $(".tabs-container .custom-tabs .nav-tabs>li").width(elemWidth);
  }

  $(document).ready(function(){
    calculateWidth();
  });

  $(window).resize(function(){
    calculateWidth();
  });
</script>

<style type="text/css">
  #reorder-locations li:first-child {
      border: 1px solid transparent !important;
      background: #555555 !important;
      cursor: default !important;
      color:#fff;
  }
  #reorder-locations li:first-child .panel-container,
  #reorder-locations li:hover .panel-container {
    color:#fff;
  }

  #reorder-locations li {
    list-style: none;
    list-style-type: none;
  }
  #reorder-locations li:hover {
    background: #212121;
    border-color: #212121;
    color:#fff;
    cursor:move;
    list-style: none;
    outline: none;
  }

  .btn-blue {
    background-color: #212121;
    color:#fff;
    border-radius: 0px !important;
    outline: none;
    font-family: 'HelveticaNeue';
    border: 1px solid rgba(223,223,223,0.30);
    font-size: 14px;
    border-radius: 0px;
    cursor: default;
  }

  .btn-blue:hover{
    background-color: #212121;
    color:#fff;
  }

  #perPersonCost{
    cursor: default;
  }

  #perPersonCost:hover {
    background-color: #212121 !important;
    color:#fff;
    border: 1px solid rgba(223,223,223,0.30);
    cursor: default;
  }

</style>

<div class="location-inner m-t-20">
	<div class="loc-icon">
       <svg width="24px" height="31px" viewBox="0 0 24 31" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
          <desc>Created with Sketch.</desc>
          <defs>
              <polygon id="path-1" points="0 30.9698922 0 0 23.994 0 23.994 30.9698922 1.54499419e-15 30.9698922"></polygon>
          </defs>
          <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g id="49---EROA007-V4.1-Generic-(Edit-Itinerary)-01" transform="translate(-20.000000, -57.000000)">
                  <g id="(Generic)-Icon---Booking-Summary" transform="translate(20.000000, 57.000000)">
                      <g id="(eRoam)-Icon---Booking-Summary">
                          <g id="Group-3">
                              <mask id="mask-2" fill="white">
                                  <use xlink:href="#path-1"></use>
                              </mask>
                              <g id="Clip-2"></g>
                              <path d="M22.56006,30.0020117 L1.43991,30.0020117 C1.175385,30.0020117 0.96006,29.7849361 0.96006,29.517958 L0.96006,14.517058 L23.03991,14.517058 L23.03991,29.517958 C23.03991,29.7849361 22.824585,30.0020117 22.56006,30.0020117 M1.43991,0.967865366 L22.56006,0.967865366 C22.824585,0.967865366 23.03991,1.18494098 23.03991,1.45161659 L23.03991,13.5491776 L0.96006,13.5491776 L0.96006,1.45161659 C0.96006,1.18494098 1.175385,0.967865366 1.43991,0.967865366 M22.56006,-1.51219512e-05 L1.43991,-1.51219512e-05 C0.64596,-1.51219512e-05 -1.5e-05,0.651287317 -1.5e-05,1.45161659 L-1.5e-05,29.517958 C-1.5e-05,30.3185898 0.64596,30.9698922 1.43991,30.9698922 L22.56006,30.9698922 C23.35401,30.9698922 23.999985,30.3185898 23.999985,29.517958 L23.999985,1.45161659 C23.999985,0.651287317 23.35401,-1.51219512e-05 22.56006,-1.51219512e-05" id="Fill-1" fill="#212121" mask="url(#mask-2)"></path>
                          </g>
                          <g id="Group-38" transform="translate(2.850000, 2.873171)" fill="#212121">
                              <path d="M2.4298425,2.44959732 C2.4298425,1.64919244 3.0758925,0.99796561 3.8701425,0.99796561 C4.6640925,0.99796561 5.3100675,1.64919244 5.3100675,2.44959732 L5.3100675,2.93372659 C5.3100675,3.73413146 4.6640925,4.38535829 3.8701425,4.38535829 C3.0758925,4.38535829 2.4298425,3.73413146 2.4298425,2.93372659 L2.4298425,2.44959732 Z M3.8701425,5.35323878 C5.1934425,5.35323878 6.2701425,4.26778512 6.2701425,2.93372659 L6.2701425,2.44959732 C6.2701425,1.11553878 5.1934425,0.030085122 3.8701425,0.030085122 C2.5467675,0.030085122 1.4701425,1.11553878 1.4701425,2.44959732 L1.4701425,2.93372659 C1.4701425,4.26778512 2.5467675,5.35323878 3.8701425,5.35323878 L3.8701425,5.35323878 Z" id="Fill-4"></path>
                              <path d="M0.98994,7.28862171 C0.98994,6.96198756 1.362315,6.33677049 1.94559,6.33677049 L2.099715,6.33677049 C2.55414,6.94512659 3.186615,7.28862171 3.870165,7.28862171 C4.55334,7.28862171 5.186115,6.94512659 5.64024,6.33677049 L5.794365,6.33677049 C6.37764,6.33677049 6.750015,6.96198756 6.750015,7.28862171 L6.750015,8.2565022 L0.98994,8.2565022 L0.98994,7.28862171 Z M0.91029,9.22438268 L6.831765,9.22438268 C7.31604,9.22438268 7.71009,8.82712902 7.71009,8.33891683 L7.71009,7.28862171 C7.71009,6.86747537 7.51524,6.39612415 7.18914,6.02782902 C6.813315,5.60305341 6.317865,5.36889 5.794365,5.36889 L5.386815,5.36889 C5.220165,5.36889 5.06544,5.45629488 4.97814,5.59927293 C4.69809,6.05792171 4.294365,6.32111927 3.870165,6.32111927 C3.44559,6.32111927 3.04194,6.05792171 2.761815,5.59927293 C2.674515,5.45629488 2.519865,5.36889 2.35314,5.36889 L1.94559,5.36889 C0.86694,5.36889 0.029865,6.40111439 0.029865,7.28862171 L0.029865,8.33687537 C0.029865,8.82622171 0.424815,9.22438268 0.91029,9.22438268 L0.91029,9.22438268 Z" id="Fill-6"></path>
                              <path d="M9.15,1.48211 L14.42985,1.48211 C14.69505,1.48211 14.910075,1.26526122 14.910075,0.997980732 C14.910075,0.730700244 14.69505,0.514229512 14.42985,0.514229512 L9.15,0.514229512 C8.884875,0.514229512 8.67015,0.730700244 8.67015,0.997980732 C8.67015,1.26526122 8.884875,1.48211 9.15,1.48211" id="Fill-8"></path>
                              <path d="M9.15,5.35323122 L10.589925,5.35323122 C10.85505,5.35323122 11.07015,5.13645805 11.07015,4.86910195 C11.07015,4.6021239 10.85505,4.38535073 10.589925,4.38535073 L9.15,4.38535073 C8.884875,4.38535073 8.67015,4.6021239 8.67015,4.86910195 C8.67015,5.13645805 8.884875,5.35323122 9.15,5.35323122" id="Fill-10"></path>
                              <path d="M13.95,4.38536585 L12.510075,4.38536585 C12.24495,4.38536585 12.02985,4.60213902 12.02985,4.86911707 C12.02985,5.13647317 12.24495,5.35324634 12.510075,5.35324634 L13.95,5.35324634 C14.215125,5.35324634 14.42985,5.13647317 14.42985,4.86911707 C14.42985,4.60213902 14.215125,4.38536585 13.95,4.38536585" id="Fill-12"></path>
                              <path d="M17.31006,4.38536585 L15.870135,4.38536585 C15.60501,4.38536585 15.38991,4.60213902 15.38991,4.86911707 C15.38991,5.13647317 15.60501,5.35324634 15.870135,5.35324634 L17.31006,5.35324634 C17.575185,5.35324634 17.78991,5.13647317 17.78991,4.86911707 C17.78991,4.60213902 17.575185,4.38536585 17.31006,4.38536585" id="Fill-14"></path>
                              <path d="M9.15,3.41750049 L17.310075,3.41750049 C17.5752,3.41750049 17.789925,3.20102976 17.789925,2.93374927 C17.789925,2.66646878 17.5752,2.44962 17.310075,2.44962 L9.15,2.44962 C8.884875,2.44962 8.67015,2.66646878 8.67015,2.93374927 C8.67015,3.20102976 8.884875,3.41750049 9.15,3.41750049" id="Fill-16"></path>
                              <path d="M0.51006,14.0634146 L1.470135,14.0634146 C1.73496,14.0634146 1.949985,13.8466415 1.949985,13.5796634 C1.949985,13.3123073 1.73496,13.0955341 1.470135,13.0955341 L0.51006,13.0955341 C0.244935,13.0955341 0.029835,13.3123073 0.029835,13.5796634 C0.029835,13.8466415 0.244935,14.0634146 0.51006,14.0634146" id="Fill-18"></path>
                              <path d="M17.78994,13.0955493 L3.870165,13.0955493 C3.604965,13.0955493 3.38994,13.3123224 3.38994,13.5796029 C3.38994,13.8466566 3.604965,14.0634298 3.870165,14.0634298 L17.78994,14.0634298 C18.055065,14.0634298 18.27009,13.8466566 18.27009,13.5796029 C18.27009,13.3123224 18.055065,13.0955493 17.78994,13.0955493" id="Fill-20"></path>
                              <path d="M0.51006,15.9991454 L1.470135,15.9991454 C1.73496,15.9991454 1.949985,15.7823722 1.949985,15.5150917 C1.949985,15.2477356 1.73496,15.0312649 1.470135,15.0312649 L0.51006,15.0312649 C0.244935,15.0312649 0.029835,15.2477356 0.029835,15.5150917 C0.029835,15.7823722 0.244935,15.9991454 0.51006,15.9991454" id="Fill-22"></path>
                              <path d="M17.78994,15.03128 L3.870165,15.03128 C3.604965,15.03128 3.38994,15.2477507 3.38994,15.5150312 C3.38994,15.7823873 3.604965,15.9991605 3.870165,15.9991605 L17.78994,15.9991605 C18.055065,15.9991605 18.27009,15.7823873 18.27009,15.5150312 C18.27009,15.2477507 18.055065,15.03128 17.78994,15.03128" id="Fill-24"></path>
                              <path d="M12.02988,17.9345737 L0.51003,17.9345737 C0.24498,17.9345737 0.02988,18.1513468 0.02988,18.4186273 C0.02988,18.685681 0.24498,18.9024541 0.51003,18.9024541 L12.02988,18.9024541 C12.295005,18.9024541 12.51003,18.685681 12.51003,18.4186273 C12.51003,18.1513468 12.295005,17.9345737 12.02988,17.9345737" id="Fill-26"></path>
                              <path d="M10.11006,21.8057327 L0.51006,21.8057327 C0.244935,21.8057327 0.029835,22.0225059 0.029835,22.2897863 C0.029835,22.5571424 0.244935,22.7736132 0.51006,22.7736132 L10.11006,22.7736132 C10.375185,22.7736132 10.58991,22.5571424 10.58991,22.2897863 C10.58991,22.0225059 10.375185,21.8057327 10.11006,21.8057327" id="Fill-28"></path>
                              <path d="M0.51006,20.8381698 L8.18991,20.8381698 C8.455035,20.8381698 8.670135,20.6213966 8.670135,20.3541161 C8.670135,20.08676 8.455035,19.8702893 8.18991,19.8702893 L0.51006,19.8702893 C0.244935,19.8702893 0.029835,20.08676 0.029835,20.3541161 C0.029835,20.6213966 0.244935,20.8381698 0.51006,20.8381698" id="Fill-30"></path>
                              <path d="M5.78994,23.7414634 L0.51009,23.7414634 C0.244965,23.7414634 0.029865,23.9582366 0.029865,24.2252146 C0.029865,24.4925707 0.244965,24.7093439 0.51009,24.7093439 L5.78994,24.7093439 C6.055065,24.7093439 6.270165,24.4925707 6.270165,24.2252146 C6.270165,23.9582366 6.055065,23.7414634 5.78994,23.7414634" id="Fill-32"></path>
                              <path d="M17.78994,23.7414634 L15.87009,23.7414634 C15.604965,23.7414634 15.38994,23.9582366 15.38994,24.2252146 C15.38994,24.4925707 15.604965,24.7093439 15.87009,24.7093439 L17.78994,24.7093439 C18.055065,24.7093439 18.27009,24.4925707 18.27009,24.2252146 C18.27009,23.9582366 18.055065,23.7414634 17.78994,23.7414634" id="Fill-34"></path>
                              <path d="M17.78994,21.8057327 L16.829865,21.8057327 C16.56504,21.8057327 16.350015,22.0225059 16.350015,22.2897863 C16.350015,22.5571424 16.56504,22.7736132 16.829865,22.7736132 L17.78994,22.7736132 C18.055065,22.7736132 18.27009,22.5571424 18.27009,22.2897863 C18.27009,22.0225059 18.055065,21.8057327 17.78994,21.8057327" id="Fill-36"></path>
                          </g>
                      </g>
                  </g>
              </g>
          </g>
       </svg>
	</div>

  <div class="location-content">
    	<h4 class="loc-title" >Booking Summary</h4>
  </div>

  <div>
    	<div class="m-t-10">
        <div class="row">
          <div class="col-sm-2"></div>
          <div class="col-sm-10"></div>
        </div>

        <div class="row">
          <div class="col-sm-2 padding-right-0">
            <span id="perPersonCost" class="btn btn-secondary btn-block" style="padding: 9px 10px 2px 10px;text-transform: none;">
              <p style="font-size: 20px; line-height: 13px;"><?php echo e(str_replace('Nights','',session()->get( 'search' )['total_number_of_days'])); ?></p>
              <p style="font-size:12px; line-height: 0px;">days</p>
            </span>
          </div>
          <div class="col-sm-10 padding-right-0">
            <span id="perPersonCost" class="btn btn-secondary btn-block" style="padding: 9px 10px 2px 10px; text-transform: none;">
              <p style="font-size: 20px; line-height: 13px;"><?php echo e(session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_day']); ?> per day</p>
              <p style="font-size:12px; line-height: 0px;">TOTAL <?php echo e(session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_person']); ?> (per person)</p>
            </span>
          </div>
        </div>

      	<?php /*<p id="perpersonCost" class="btn btn-primary btn-block">{{ session()->get( 'search' )['total_number_of_days'] }} - <strong>{{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_day'] }}</strong> (Per Head)</p>
        <p id="totalCost" class="btn btn-blue btn-block" style="display: none;">Total {{ session()->get( 'search' )['total_number_of_days'] }}-<strong>{{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_person'] }}</strong> (Per Head)</p> */ ?>
        <?php /*<p id="perpersonCost" class="btn btn-primary btn-block">{{ session()->get( 'search' )['total_number_of_days'] }} - <strong>{{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_day'] }}</strong> Cost Per Day (Per Person)</p>
        <p id="totalCost" style="display: none;">Total {{ session()->get( 'search' )['total_number_of_days'] }} - <strong>{{ session()->get( 'search' )['currency'].' '.session()->get( 'search' )['cost_per_person'] }}</strong> (Per Person)</p>*/ ?>
      </div>

      <div class="m-t-10">
          <div class="row">
            <div class="col-xs-5">
              <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                  <desc>Created with Sketch.</desc>
                  <defs></defs>
                  <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="49---EROA007-V4.1-Generic-(Edit-Itinerary)-01" transform="translate(-66.000000, -241.000000)" fill="#212121">
                          <g id="(Generic)-Icon---Calendar" transform="translate(66.000000, 241.000000)">
                              <g id="(eRoam)-Icon---Calendar">
                                  <path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
                                  <path d="M10.1326667,8.000625 L12,8.000625 L12,6.25 L10.1326667,6.25 L10.1326667,8.000625 Z M10.1326667,10.2503125 L12,10.2503125 L12,8.4996875 L10.1326667,8.4996875 L10.1326667,10.2503125 Z M10.1326667,12.5 L12,12.5 L12,10.749375 L10.1326667,10.749375 L10.1326667,12.5 Z M7.733,12.5 L9.60033333,12.5 L9.60033333,10.749375 L7.733,10.749375 L7.733,12.5 Z M5.33333333,12.5 L7.20066667,12.5 L7.20066667,10.749375 L5.33333333,10.749375 L5.33333333,12.5 Z M2.93366667,12.5 L4.79933333,12.5 L4.79933333,10.749375 L2.93366667,10.749375 L2.93366667,12.5 Z M2.93366667,10.2503125 L4.79933333,10.2503125 L4.79933333,8.4996875 L2.93366667,8.4996875 L2.93366667,10.2503125 Z M2.93366667,8.000625 L4.79933333,8.000625 L4.79933333,6.25 L2.93366667,6.25 L2.93366667,8.000625 Z M5.33333333,8.000625 L7.20066667,8.000625 L7.20066667,6.25 L5.33333333,6.25 L5.33333333,8.000625 Z M5.33333333,10.2503125 L7.20066667,10.2503125 L7.20066667,8.4996875 L5.33333333,8.4996875 L5.33333333,10.2503125 Z M7.733,10.2503125 L9.60033333,10.2503125 L9.60033333,8.4996875 L7.733,8.4996875 L7.733,10.2503125 Z M7.733,8.000625 L9.60033333,8.000625 L9.60033333,6.25 L7.733,6.25 L7.733,8.000625 Z M9.60033333,5.749375 L2.39966667,5.749375 L2.39966667,13.000625 L12.534,13.000625 L12.534,5.749375 L9.60033333,5.749375 Z" id="Fill-3"></path>
                              </g>
                          </g>
                      </g>
                  </g>
              </svg> Date:
            </div>
            <div class="col-xs-7">
              <?php //echo '<pre>'; echo count(session()->get( 'search' )['itinerary']); echo '</pre>';?>
            	<?php echo e(date( 'j M Y', strtotime( session()->get( 'search' )['itinerary'][0]['city']['date_from'] ) )); ?> - <?php echo e(date( 'j M Y', strtotime( session()->get( 'search' )['itinerary'][count(session()->get( 'search' )['itinerary']) - 1]['city']['date_to'] ) )); ?>

            </div>
          </div>
          <div class="row">
            <div class="col-xs-5">
              <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                  <desc>Created with Sketch.</desc>
                  <defs>
                      <polygon id="path-1" points="0.0734846939 0.0301886111 8.05731122 0.0301886111 8.05731122 7.93047228 0.0734846939 7.93047228"></polygon>
                      <polygon id="path-3" points="0 5.61288199 0 0.127353462 14.9144388 0.127353462 14.9144388 5.61288199 1.12675642e-15 5.61288199"></polygon>
                  </defs>
                  <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="49---EROA007-V4.1-Generic-(Edit-Itinerary)-01" transform="translate(-66.000000, -260.000000)">
                          <g id="(Generic)-Icon---User" transform="translate(66.000000, 260.000000)">
                              <g id="(eRoam)-Icon---User">
                                  <g id="Group-3" transform="translate(3.367347, 0.121513)">
                                      <mask id="mask-2" fill="white">
                                          <use xlink:href="#path-1"></use>
                                      </mask>
                                      <g id="Clip-2"></g>
                                      <path d="M4.06516837,1.0618806 C5.68180102,1.0618806 7.00440306,2.37258211 7.00440306,3.97439892 C7.00440306,5.57621572 5.68180102,6.88691723 4.06516837,6.88691723 C2.4489949,6.88691723 1.12639286,5.57621572 1.12639286,3.97439892 C1.12639286,2.37258211 2.4489949,1.0618806 4.06516837,1.0618806 M4.06516837,7.93047228 C6.26955612,7.93047228 8.05731122,6.15859802 8.05731122,3.97439892 C8.05731122,1.79019981 6.26955612,0.0301582708 4.06516837,0.0301582708 C1.8612398,0.0301582708 0.0734846939,1.80203253 0.0734846939,3.97439892 C0.0734846939,6.1467653 1.8612398,7.93047228 4.06516837,7.93047228" id="Fill-1" fill="#212121" mask="url(#mask-2)"></path>
                                  </g>
                                  <g id="Group-6" transform="translate(0.000000, 8.616800)">
                                      <mask id="mask-4" fill="white">
                                          <use xlink:href="#path-3"></use>
                                      </mask>
                                      <g id="Clip-5"></g>
                                      <path d="M5.00797959,1.17090852 L9.90593878,1.17090852 C11.9142551,1.17090852 13.5674694,2.65181918 13.8246122,4.56932694 L1.08991837,4.56932694 C1.34706122,2.6636519 3.00027551,1.17090852 5.00797959,1.17090852 M0.526653061,5.61288199 L14.3877245,5.61288199 C14.6819082,5.61288199 14.9145612,5.38244732 14.9145612,5.09087691 C14.9145612,2.36024877 12.6735918,0.127353462 9.90593878,0.127353462 L5.00797959,0.127353462 C2.25287755,0.127353462 -3.06122449e-05,2.34841605 -3.06122449e-05,5.09087691 C-3.06122449e-05,5.38244732 0.232622449,5.61288199 0.526653061,5.61288199" id="Fill-4" fill="#212121" mask="url(#mask-4)"></path>
                                  </g>
                              </g>
                          </g>
                      </g>
                  </g>
              </svg> Travellers:
            </div>
            <div class="col-xs-7">
              <?php echo e(session()->get( 'search' )['travellers']); ?>

            </div>
          </div>
          
          <p class="m-t-20">Click on Segment to Edit
                <?php if(session()->get('map_data')['type'] == 'manual' && count(session()->get('map_data')['cities']) > 2 && session()->get( '_previous' )['url'] == url( 'map' )): ?>
                      &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<a href="javascript://" id="reorder-locations-btn" >Reorder</a>
          <?php endif; ?>
            </p>

          <?php if(session()->get('map_data')['type'] == 'manual' && count(session()->get('map_data')['cities']) > 2 && session()->get( '_previous' )['url'] == url( 'map' )): ?>
            <!--<p><a href="javascript://" id="reorder-locations-btn" class="btn btn-primary btn-block">Reorder Locations</a></p>-->
          <?php endif; ?>
      </div>
  </div>
</div>
<hr/>


<?php //echo '<pre>'; print_r(session()->get( 'search' )['itinerary'][0]['activities']); echo '</pre>'; ?>

<div class="location-inner" id="panel-drag">
  <div class="itinerary-container panel-group sortable-list new-panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    <?php if(session()->get('search')): ?>
			<?php foreach(session()->get('search')['itinerary'] as $key => $leg): ?>
				<?php 
					
					$itineraryIndex = $key == $last ? $key : $key + 1;
					$departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
					$arriveTimezone = get_timezone_abbreviation(session()->get('search')['itinerary'][$itineraryIndex]['city']['timezone']['name']);
        ?>
        <?php //echo '<pre>'; print_r($leg['hotel']); echo '</pre>'; die; ?>
      	<div class="panel panel-default itinerary-leg-container" data-index="<?php echo e($key); ?>">
          <div class="panel-heading <?php echo e($key == $legkey ? 'panel-open' : ''); ?>" role="tab" id="heading-<?php echo e($key); ?>">
            	<div class="panel-title">
                  <div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo e($key); ?>" aria-expanded="true" aria-controls="collapse-<?php echo e($key); ?>" class="collapse-title <?php echo e($key != $legkey ? 'collapsed' : ''); ?>">
                    <i class="icon-unfold"></i>
                    <div class="city-name"><strong><?php echo e(str_limit($leg['city']['name'], 18)); ?>, <?php echo e($leg['city']['country']['code']); ?></strong></div>
                  </div>
                  <div class="row">
                    <div class="col-xs-6 date-control">
                      <div class="panel-form-group">
                        <label class="label-control">Check-in Date:</label>
                        <div class="input-group datepicker">
                          <input id="start_date_<?php echo e($key); ?>" type="text" data-leg="<?php echo e($key); ?>" data-cityId="<?php echo e($leg['city']['id']); ?>" data-fromDate="<?php echo e($leg['city']['date_from']); ?>" data-toDate=" <?php echo e($leg['city']['date_to']); ?>" data-nights="<?php echo e($leg['city']['default_nights']); ?>" placeholder="<?php echo e(date( 'j M Y', strtotime( $leg['city']['date_from'] ) )); ?>" class="form-control start_date" value="<?php echo e(date( 'j M Y', strtotime( $leg['city']['date_from'] ) )); ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-xs-6 stay-control">
                      <div class="panel-form-group">
                       <label class="label-control">Length of Stay:</label>
                       <select class="form-control changeNights" id="changeNights" data-leg="<?php echo e($key); ?>" data-cityId="<?php echo e($leg['city']['id']); ?>" data-fromDate="<?php echo e($leg['city']['date_from']); ?>" data-toDate=" <?php echo e($leg['city']['date_to']); ?>">
                        <?php for($i = 1; $i < 11; $i++): ?>
                          <option value="<?php echo e($i); ?>" <?php echo e(($leg['city']['default_nights'] == $i) ? 'selected' : ''); ?> > <?php echo e($i); ?> <?php echo e(($i == 1) ? 'Night': 'Nights'); ?></option>
                        <?php endfor; ?>
                       </select>
                      </div>
                    </div>
                    <input type="hidden" id="night-data-<?php echo e($key); ?>" value="<?php echo e($leg['city']['default_nights']); ?>">
              	  </div>
              </div>
          </div>

          <div id="collapse-<?php echo e($key); ?>" class="panel-collapse collapse <?php echo e($key == $legkey ? 'in' : ''); ?>" role="tabpanel" aria-labelledby="heading-<?php echo e($key); ?>">
            	<div class="panel-body">
              	
                <div class="panel-inner itinerary-leg-link"  data-type="hotels" data-link="<?php echo e(url( $leg['city']['name'].'/hotels?leg='.$key )); ?>">
                		<div class="panel-icon">    
                      <svg width="20px" height="12px" viewBox="0 0 20 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                          <desc>Created with Sketch.</desc>
                          <defs>
                              <polygon id="path-1" points="9.9917561 0.019123506 0.00292682927 0.019123506 0.00292682927 11.9522869 19.9805854 11.9522869 19.9805854 0.019123506 9.9917561 0.019123506"></polygon>
                          </defs>
                          <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="49---EROA007-V4.1-Generic-(Edit-Itinerary)-01" transform="translate(-28.000000, -462.000000)">
                                  <g id="Group-20" transform="translate(20.000000, 403.000000)">
                                      <g id="Group">
                                          <g id="sleeping-bed-silhouette" transform="translate(8.000000, 59.000000)">
                                              <g id="Group-3">
                                                  <mask id="mask-2" fill="white">
                                                      <use xlink:href="#path-1"></use>
                                                  </mask>
                                                  <g id="Clip-2"></g>
                                                  <path d="M18.3805366,8.81593625 L18.3805366,11.9522869 L19.9805854,11.9522869 L19.9805854,7.24776096 L1.60004878,7.24776096 L1.60004878,0.822358566 C1.60004878,0.618215139 1.52190244,0.433338645 1.36585366,0.267681275 C1.20980488,0.102023904 1.02117073,0.019123506 0.799902439,0.019123506 C0.578878049,0.019123506 0.390243902,0.102023904 0.234195122,0.267681275 C0.0781463415,0.43352988 0,0.618406375 0,0.822358566 L0,11.9522869 L1.60004878,11.9522869 L1.60004878,8.81593625 L18.3805366,8.81593625 Z" id="Fill-1" fill="#565656" mask="url(#mask-2)"></path>
                                              </g>
                                              <path d="M4.11697561,4.03505976 C4.52039024,4.03505976 4.87156098,3.89478884 5.17073171,3.61429482 C5.46990244,3.3338008 5.61946341,2.99598406 5.61946341,2.60079681 C5.61946341,2.20541833 5.46990244,1.86760159 5.17073171,1.5872988 C4.87156098,1.30680478 4.52039024,1.16653386 4.11697561,1.16653386 C3.71380488,1.16653386 3.36907317,1.30680478 3.08287805,1.5872988 C2.79668293,1.86779283 2.65356098,2.20560956 2.65356098,2.60079681 C2.65356098,2.99598406 2.79668293,3.3338008 3.08287805,3.61429482 C3.36907317,3.89478884 3.71380488,4.03505976 4.11697561,4.03505976 L4.11697561,4.03505976 Z" id="Fill-4" fill="#565656"></path>
                                              <path d="M19.9805854,6.06205578 L19.9805854,4.14970518 C19.9805854,3.8438247 19.8698537,3.59526693 19.6488293,3.40403187 C19.4276098,3.21279681 19.1478537,3.09160159 18.8098537,3.04058964 L7.2195122,1.9314741 L7.14136585,1.9314741 C6.93331707,1.9314741 6.75780488,2.00151394 6.61473171,2.14173705 C6.47160976,2.28200797 6.39995122,2.45421514 6.39995122,2.65811952 L6.39995122,4.87658964 L3.43404878,4.87658964 C2.93980488,4.87658964 2.69263415,5.07413546 2.69263415,5.46932271 C2.69263415,5.86450996 2.93980488,6.06205578 3.43404878,6.06205578 L19.9805854,6.06205578 Z" id="Fill-5" fill="#565656"></path>
                                          </g>
                                      </g>
                                  </g>
                              </g>
                          </g>
                      </svg>
                    </div>
                		<div class="panel-container"> 
                      <?php if( $leg['hotel'] ): ?>
                    		<p><strong>Accommodation Summary</strong> <br/> <?php echo e($leg['hotel']['name']); ?></p> 

                        <div class="row m-t-20">
                          <div class="col-md-4 col-sm-5">Check-in: </div>
                          <div class="col-md-8 col-sm-7"><?php echo e(date('j M Y', strtotime( $leg['city']['date_from'] ) )); ?></div>
                        </div>
                        <div class="row">
                          <div class="col-md-4 col-sm-5">Check-out: </div>
                          <div class="col-md-8 col-sm-7"><?php echo e(date('j M Y', strtotime( $leg['city']['date_to'] ) )); ?></div>
                        </div>
                        <div class="row m-t-20">
                          <div class="col-md-4 col-sm-5">Duration: </div>
                          <div class="col-md-8 col-sm-7"><?php echo e($leg['hotel']['nights']); ?> Night(s)</div>
                        </div>
                      <?php else: ?>
                        <p><strong>Accommodation Summary</strong> <br/> Own Arrangement</p> 
											<?php endif; ?>
                	  </div>
              	</div>
              	<hr/>

                <div class="panel-inner itinerary-leg-link" data-type="activities" data-link="<?php echo e(url( $leg['city']['name'].'/activities?leg='.$key )); ?>">
                  <div class="panel-icon">    
                    <i class="fa fa-map-o"></i>
                  </div>
                  <div class="panel-container"> 
                    <p class="itinerary-activities-name" id="itinerary-activity-<?php echo e($key); ?>"><strong>Activity Summary</strong><br/> 
                      <?php if( $leg['activities'] ): ?>
                        <?php foreach( $leg['activities'] as $activity  ): ?>
                            <?php echo e($activity['name']); ?>, <?php echo e(date('j M Y', strtotime($activity['date_selected']))); ?><br>
                        <?php endforeach; ?>
                      <?php else: ?>
                        <p>Own Arrangement</p>
                      <?php endif; ?>
                    </p> 
                  </div>
                </div>
                <hr/>

              	<?php if( $leg['transport'] ): ?>
              	  <?php $duration = $leg['transport']['duration'] ? $leg['transport']['duration'] : '' ;?>
									<div class="panel-inner itinerary-leg-link" data-type="transports" data-link="<?php echo e(url( $leg['city']['name'].'/transports?leg='.$key )); ?>">
										<div class="panel-icon">   
                        <i class="fa <?php echo e(get_transport_icon($leg['transport']['transporttype']['name'])); ?>"></i> 
                    </div>
                    <div class="panel-container"> 
                        <p><strong>Transport Summary</strong> <br/> <?php echo e($leg['transport']['transport_name_text']); ?></p> 
                        <div class="row m-t-20">
                          <div class="col-md-4 col-sm-5"><strong>Depart:</strong></div>
                          <div class="col-md-8 col-sm-7"><?php echo $leg['transport']['departure_text']; ?></div>
                        </div>
                        <div class="row">
                          <div class="col-md-4 col-sm-5"><strong>Arrive:</strong></div>
                          <div class="col-md-8 col-sm-7"><?php echo $leg['transport']['arrival_text']; ?></div>
                        </div>
                    </div>
									</div>
									<hr/>
              	<?php elseif( last( session()->get('search')['itinerary'] ) != $leg ): ?>
              		<div class="panel-inner itinerary-leg-link" data-type="transports" data-link="<?php echo e(url( $leg['city']['name'].'/transports?leg='.$key )); ?>">
              			<div class="panel-icon">    
                        <i class="fa fa-car"></i>
                    </div>
                  	<div class="panel-container"> 
                      <p><strong>Transport Summary</strong> <br/> <?php echo e(str_limit( $leg['city']['name'] . ' to ' . session()->get('search')['itinerary'][$key + 1]['city']['name'] , 50 )); ?> <br>(Self-Drive / Own Arrangement)</p>
                    </div>
                </div>
              	<?php endif; ?>
            	</div>
          </div>
      	</div>
      <?php endforeach; ?>
    <?php endif; ?>
  </div> 

  <div id="reorder-locations-container" style="display:none;">
    
      <span class="reorder-btn">Drag <i style="margin: 0 .5rem" class="fa fa-arrows"></i> locations to reorder.</span>
      <br>
      <div class="row">
         <div class="col-sm-4"></div>
        <div class="col-sm-4">
          <a href="javascript://" class="btn btn-primary btn-block" id="save-order-btn">Save</a>
        </div>  
        <div class="col-sm-4">
          <a href="javascript://" class="btn btn-primary btn-block" id="cancel-reorder-btn">Discard</a>
        </div>
      </div>
      <br>
    
    
      <ul id="reorder-locations">
        <?php if(session()->get('search')): ?>
          <?php foreach(session()->get('search')['itinerary'] as $key => $leg): ?>
            <li class="ui-state-default panel-heading" data-city="<?php echo e($leg['city']['id']); ?>">
              <div class="panel-icon"><i class="fa fa-map-marker" style="font-size:1.5em;"></i></div>
              <div class="panel-container"><span class="bold-txt"><?php echo e($leg['city']['name']); ?>, <?php echo e($leg['city']['country']['code']); ?></span></div>
            </li>
          <?php endforeach; ?>
        <?php endif; ?>
      </ul>
  </div>

  <div class="m-t-40 row">
    <?php
			switch ( session()->get( '_previous' )['url'] ) {
				case url( 'map' ) : 
	  ?>
				<div class="col-sm-6">
            <a href="<?php echo e(url( 'proposed-itinerary' )); ?>" name="" class="btn btn-primary btn-block">VIEW ITINERARY</a>
        </div>
        <div class="col-sm-6">
            <a href="<?php echo e(url( 'review-itinerary' )); ?>" name="" class="btn btn-secondary btn-block">BOOK NOW</a>
        </div>

			<?php 
				break;
				case url( 'proposed-itinerary' ) : 
			?>

				<div class="col-sm-6">
            <?php /*<a  href="{{ url( 'map' ) }}" name="" class="btn btn-primary btn-block">VIEW MAP</a>*/ ?>
            <a href="<?php echo e(url( 'proposed-itinerary' )); ?>" name="" class="btn btn-secondary btn-block">VIEW ITINERARY</a>
        </div>
        <div class="col-sm-6">
            <a href="<?php echo e(url( 'review-itinerary' )); ?>" name="" class="btn btn-primary btn-block">BOOK NOW</a>
        </div>
			<?php 
				break;
				default: 
			?>

			<div class="col-sm-6">
          <a href="<?php echo e(url( 'proposed-itinerary' )); ?>" name="" class="btn btn-primary btn-block">VIEW ITINERARY</a>
      </div>
     <!-- <div class="col-sm-6">
          <a  href="<?php echo e(url( 'map' )); ?>" name="" class="btn btn-secondary btn-block">VIEW MAP</a>
      </div>-->
      <div class="col-sm-6">
          <a href="<?php echo e(url( 'review-itinerary' )); ?>" name="" class="btn btn-primary btn-block">BOOK NOW</a>
      </div>	
		<?php 
					break;
			}
		?>
  </div>
</div>


<div class="modal fade no-hotel-summary1" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header " style="display: none">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <p>Sorry, you can't add additional hotel nights because there's no hotel selected.</p>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-6 pull-right">
            <button type="button" class="btn confirm-cancel-button eroam-confirm-cancel-btn" data-dismiss="modal">Close</button>

            <button type="button" class="btn btn-primary confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<style type="text/css">
.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_disabled { opacity: 0.5;}

</style>

<script type="text/javascript" src="<?php echo e(url( 'assets/js/theme/jquery.slimscroll.js' )); ?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php /*<link rel="stylesheet" type="text/css" href="{{ url( 'assets/datetimepicker/jquery.datetimepicker.css' ) }}">
<script src="{{ url( 'assets/datetimepicker/build/jquery.datetimepicker.full.js' ) }}"></script> */ ?>

<script src="<?php echo e(url( 'assets/js/theme/bootstrap-datepicker.js' )); ?>"></script>

<script type="text/javascript">

  var isMap = "<?php echo e($isMap); ?>";

  $(document).ready(function(){

    var totalLeg = "<?php echo e($last); ?>";

    for (var i = 0; i <= totalLeg; i++) {
        $("#start_date_"+i).datepicker({
            format: 'd M yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: '+1d'
        }).on('changeDate', function (selected) {
            var selectDate  = new Date(selected.date.valueOf());
            var leg         = $(this).attr('data-leg');
            var city_id     = $(this).attr('data-cityId');

            eroam.ajax( 'get', 'latest-search', { }, function( response ){
              if(response){
                search_session = JSON.parse(response);
                var session = search_session.itinerary;

                if(session.length > 0){
                  if(session[leg].city.id == city_id ){
                    if(leg == 0){
                      var from_date   = selectDate.getFullYear() + '-' + ('0' + (selectDate.getMonth()+1)).slice(-2) + '-' +  ('0' + selectDate.getDate()).slice(-2);
                      search_session.travel_date = from_date;
                      
                      
                    } else { 
                      var From_date = new Date($("#start_date_0").attr('data-fromdate'));
                      var To_date = new Date($("#start_date_"+leg).attr('data-fromdate'));
                      var diff_date =  To_date - From_date;
                      //var years = Math.floor(diff_date/31536000000);
                      //var months = Math.floor((diff_date % 31536000000)/2628000000);
                      var legDays = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
                      var from_date = deductDays(selectDate, legDays);
                      
                      search_session.travel_date = from_date;
                    }

                    if( search_session.itinerary[leg].activities != null ){
                      var act = sortByDate(search_session.itinerary[leg].activities);
                      var From_date = new Date($("#start_date_"+leg).attr('data-fromdate'));

                      if(From_date > selectDate){
                          var diff_date1 =  From_date - selectDate;
                          var deduct = Math.floor(((diff_date1 % 31536000000) % 2628000000)/86400000);
                          var value = $("#start_date_"+leg).attr('data-nights')
                          var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct) );
                          activityCancellation(activityDates, leg, act, value ); 
                      } else {
                        bookingSummary.update( JSON.stringify( search_session ) );
                      }
                    }else{
                      bookingSummary.update( JSON.stringify( search_session ) );
                    }
                    
                    //bookingSummary.update( JSON.stringify( search_session ) );
                  }
                }
              }
            });
        });
    };

    $('.no-hotel-cancel-btn').one('click',function(e){
      e.preventDefault();
      $('#eroam-confirm').modal('hide');
    });
  });

	$(function () {
		//$('[data-toggle="tooltip"]').tooltip();

    $('.helpBox').find('.row').slimScroll({
        height: '1170px',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
    });

    $('.calendarList12').slimScroll({
      height: '680px', //535px',
      color: '#212121',
      opacity: '0.7',
      size: '5px',
      allowPageScroll: true
    });

    $('#hotel-list, #activity-list, #transportList, .iternary-box, .payment-page').slimScroll({
      height: '99%',
      color: '#212121',
      opacity: '0.7',
      size: '5px',
      allowPageScroll: true
    });

    if(isMap == 1){
      $('.booking-summary').slimScroll({
        height: '1200px',
        color: '#212121',
        opacity: '0.7',
        size: '5px',
        allowPageScroll: true
      });
    }

    $( '#edit-map-btn' ).click( function( event ) {
      //event.preventDefault();
      $( '#edit-map-container' ).show();
      $( '#edit-map-overlay' ).fadeIn( 300 );
    });

    $('body').on('click', '#reorder-locations-btn', function(e) {
      e.preventDefault();
      $('.itinerary-container, #reorder-locations-btn').hide();
      $('#reorder-locations-container').show();
      $('#reorder-locations').sortable({
        items: 'li:not(li:first-child)',
        placeholder: 'ui-state-highlight',
        opacity: 0.7,
        scrollSensitivity: 50,
        forcePlaceholderSize: true,
          over: function(e, ui){ 
            ui.placeholder.height(ui.item.height()-30);
          }
      });
    });

    $('.changeNights').change(function(){
      var leg       = $(this).attr('data-leg');
      var value     = $(this).val();
      var city_id   = $(this).attr('data-cityId');
      var from_date = $(this).attr('data-fromDate');
      var to_date   = $(this).attr('data-toDate');

      eroam.ajax( 'get', 'latest-search', { }, function( response ){

        if(response){
          search_session = JSON.parse(response);
          var session = search_session.itinerary;

          if(session.length > 0){
            if(session[leg].city.id == city_id ){

              var nights = search_session.itinerary[leg].city.default_nights;
      
              if( (search_session.itinerary[leg].hotel != null) && typeof search_session.itinerary[leg].hotel.nights != 'undefined' ){
                
                nights = search_session.itinerary[leg].hotel.nights;
                search_session.itinerary[leg].hotel.nights = value;
              
                var days_to_add = parseInt(value) - parseInt(nights);

                if( days_to_add > 0 ){
                  search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) + days_to_add;
                  search_session.itinerary[leg].city.days_to_add = days_to_add;
                  search_session.itinerary[leg].city.add_after_date = from_date;
                  bookingSummary.update( JSON.stringify( search_session ) );
                }else{
                  
                  var deduct = Math.abs(days_to_add);
                  var val = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

                  search_session.itinerary[leg].city.days_to_deduct = deduct;
                  search_session.itinerary[leg].city.deduct_after_date = from_date;
                  search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);

                  if( search_session.itinerary[leg].activities != null ){
                    
                    var act = sortByDate(search_session.itinerary[leg].activities);
                    var departureDate = getDeparture(leg, from_date, to_date);
                    departureDate = departureDate.split(' ');


                    var activityDates = getActivityToBeCancelledByDates(  search_session.itinerary[leg].city.date_to, parseInt(deduct) );
                    alert(departureDate+'==//=='+activityDates);
                    activityCancellation(activityDates, leg, act, value );
                  }else{
                    bookingSummary.update( JSON.stringify( search_session ) );
                  }
                  return;
                }
              }else{
                $('.no-hotel-summary').modal();
              }
            }

          }
        }
      });
    });
  })

  function activityCancellation( activityDates, itineraryIndex, act, value ){
    
    if( activityDates.length > 0 ){

      eroam.confirm( null, 'Decreasing the number of nights can cause schedule conflicts for the activities on this city. Would you like to cancel some activities for this city?', function(e){
        
        if( act.length > 0){
          for( index = act.length-1; index >= 0; index-- ){
            var actDuration = parseInt(act[index].duration);
            var multiDayRemoved = false;
            if(actDuration > 1){
              actDuration = actDuration - 1;
              var activityDateSelected = act[index].date_selected;
              for( var counter = 1;counter <= actDuration; counter ++ ){
                var multiDayActivityDate = addDays( activityDateSelected, counter );

                if( activityDates.indexOf( multiDayActivityDate ) > -1 && !multiDayRemoved ){
                  act.splice(index, 1);
                  multiDayRemoved = true;
                }
              }
            }else{
              if( activityDates.indexOf( act[index].date_selected ) > -1 ){
                act.splice(index, 1);
              }
            }
            
          }
        }
        
        search_session.itinerary[itineraryIndex].activities = act;
        bookingSummary.update( JSON.stringify( search_session ) )
      });

    }else{
      bookingSummary.update( JSON.stringify( search_session ) );
    }
  }

  function getActivityToBeCancelledByDates( dateTo, numberOfDays ){
    var dateArray = [];
    if( numberOfDays ){
      dateArray.push(dateTo);
      for( var count = 1;count <= numberOfDays; count ++ ){
        dateArray.push( deductDays( dateTo, count) )
      }
    }
    return dateArray;
  }

  function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + parseInt(days) );
    return formatDate(result);
  }

  function deductDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() - parseInt(days) );
    return formatDate(result);
  }

  function formatTime(time){
    return time.replace(/hr\(s\)|min\(s\)|hours|minutes /gi, function(x){
      return x == 'hr(s)' || 'hours' ? ':' : '';
    });
  }

  function get_hours_min(hour_min){
    var hours_mins = hour_min.toString().split(".");
    var hours = parseInt(hours_mins[0]);
    var mins = parseFloat('0.'+hours_mins[1]) * 60;
    return hours+' hr(s) '+Math.ceil(mins)+' min(s)';
  }

  function removePlus(time){
    return time.replace( /\+/g, '' );
  }

  function padd_zero(number) {
    if(parseInt(number) == 0){
      number = 12;
    }
    return (number < 10) ? ("0" + number) : number;
  }

  function arrival_am_pm(eta){
    var result;
    var arrival_split = eta.split("+"); 
    var arrival_split_am_pm = arrival_split[0].split(" ");
    var arrival_split_hour_min = arrival_split[0].split(":");
    var arrival_hour = parseInt(arrival_split_hour_min[0]);
    var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


    if(typeof arrival_split_am_pm[1] !== 'undefined'){

      result = padd_zero(arrival_hour)+':'+arrival_min+' '+arrival_split_am_pm[1];
    }else{
      if( arrival_hour < 12 ){
        result = padd_zero(arrival_hour)+':'+arrival_min+' AM';
      }else{
        hour = arrival_hour - 12;
        result = padd_zero(hour)+':'+arrival_min+' PM';
      }
    }
    
    return result;
  }

  function getDayTime(arrivalDate, duration) {
    var hours_mins = duration.split(':');
    var result = new Date(arrivalDate);
    result.setHours(result.getHours() - parseInt(hours_mins[0]));
    result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));
    return formatDate(result, true);
  }

  function getDeparture(itineraryIndex,from_date, to_date){
    var search_session     = JSON.parse( $('#search-session').val() );
    var newDateTo = addDays(from_date, parseInt(search_session.itinerary[itineraryIndex].city.default_nights)); 
    var transport = search_session.itinerary[itineraryIndex].transport;

    var departureDate = to_date+' 24';
    if(transport != null){
      var eta = transport.eta;
      var  travelDuration =  transport.duration;
      if(transport.provider == 'mystifly'){
      
        travelDuration = formatTime(get_hours_min(travelDuration));
        eta = moment( eta, moment.ISO_8601 );
        eta = eta.format('hh:mm A');

      }else{
        travelDuration = removePlus(travelDuration);
        travelDuration = formatTime(travelDuration);
      }

      var arrivalTime = arrival_am_pm(eta);
      departureDate = getDayTime(newDateTo+' '+arrivalTime, travelDuration);
    }
    return departureDate;
  }
</script>
<script>
  function leftpanel(){
    var w = $( document ).width();
      if(w > 991){
        $('.arrow-btn-new').click(function(){
          if($('.arrow-btn-new').hasClass('open')){
            $('.page-sidebar').css('display', 'none');
            $(this).removeClass('open');
            $('.page-content').css('margin-left', '0px');
          } else{
            $('.page-sidebar').css('display', 'block');
            $(this).addClass('open');
            $('.page-content').css('margin-left', '354px');
          }
        });
      } else{
        $('.arrow-btn-new').click(function(){
          if($('.arrow-btn-new').hasClass('open')){
            $('.page-sidebar').css('display', 'none');
            $(this).removeClass('open');
            $('.page-content').css('margin-left', '0px');
            $('.left-strip').css('left', '0px');
          } else{
            $('.page-sidebar').css('display', 'block');
            $(this).addClass('open');
            $('.page-content').css('margin-left', '0px');
            $('.left-strip').css('left', '300px');
          }
      });
    }
  }

  function calculateHeight(){
      var winHeight = $(window).height();

      var oheight = $('.page-sidebar').outerHeight();// -22
      var oheight1 = $('.page-sidebar').outerHeight();
      //alert(oheight);
      var elem = $('.page-content .tabs-container').outerHeight();
      var elemHeight = oheight - elem;
      $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);

      var elemHeight1 = oheight1 - elem;
      var winelemHeight = winHeight - elem;

      if(winHeight < oheight){
          $(".page-content .tabs-content-wrapper").outerHeight(elemHeight);
          $(".page-content .tabs-content-wrapper1").outerHeight(elemHeight);
          $(".page-content .tabs-content-wrapper2").outerHeight(oheight1);
      } else{
          $(".page-content .tabs-content-wrapper").outerHeight(winelemHeight);
          $(".page-content .tabs-content-wrapper1").outerHeight(winelemHeight);
          $(".page-content .tabs-content-wrapper2").outerHeight(winelemHeight);
      }
     // $(".page-content .tabs-content-wrapper1").outerHeight(elemHeight1);
     // $(".page-content .tabs-content-wrapper2").outerHeight(oheight1);

      /*---- hotel content ------*/
      //var hotelContentHeight = oheight - elem;
      //alert(hotelContentHeight);
      $(".page-content .hotel-container").outerHeight(elemHeight);
      var hotelTop = $('.page-content .hotel-top-content').outerHeight();
      var hotelTabs = $('.page-content .custom-tabs').outerHeight() + 120;
      //alert(hotelTop);
      //alert(hotelTabs);
      var hotelHeight = (elemHeight - hotelTop) - hotelTabs;
      var winHotelHeight = (winelemHeight - hotelTop) - hotelTabs;
      //alert(hotelHeight);
      if(winHeight < oheight1){
          $(".page-content .hotel-container").outerHeight(elemHeight1);
          $(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
      } else{
          $(".page-content .hotel-container").outerHeight(winelemHeight);
          $(".page-content .hotel-bottom-content").outerHeight(winHotelHeight);
      }
      //$(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
  }



  $(document).ready(function(){
      leftpanel();
      calculateHeight();

      $('.panel-heading .collapse-title').click(function() {
        $('.panel-heading').removeClass('panel-open');
        
        //If the panel was open and would be closed by this click, do not active it
        if(!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('panel-open');
      });

  });

  $(window).resize(function(){
      leftpanel();
      calculateHeight();
  });
  
	function displayPrice(display){
	  if(display == 'perperson'){
            //$( "#perpersontotal" ).removeClass( "btn-secondary" ).addClass( "btn-primary" );
            //$( "#perperson" ).removeClass( "btn-primary" ).addClass( "btn-secondary" );
            $( "#perpersontotal" ).removeClass( "active" );
            $( "#perperson" ).addClass( "active" );
            $('#perpersonCost').show();
            $('#totalCost').hide();

		}else{
            //$( "#perperson" ).removeClass( "btn-secondary" ).addClass( "btn-primary" );
            //$( "#perpersontotal" ).removeClass( "btn-primary" ).addClass( "btn-secondary" );
            $( "#perpersontotal" ).addClass( "active" );
            $( "#perperson" ).removeClass( "active" );
            $('#perpersonCost').hide();
            $('#totalCost').show();
		}
	}
</script>