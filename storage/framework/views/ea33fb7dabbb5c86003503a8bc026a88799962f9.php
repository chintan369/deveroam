<?php $__env->startSection('custom-css'); ?>
<style>
	.wrapper {
		background-image: url( <?php echo e(url( 'assets/img/bg1.jpg' )); ?> ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>




    <div id="about-us">
		<div class="top-section-image">
			<img src="<?php echo e(asset('assets/images/bg-image.jpg')); ?>" alt="" class="img-responsive">
		</div>

		<section class="content-wrapper">
			<article class="tankyou-page">
				<div class="container-fluid">
					<div class="col-md-10 col-md-offset-1">
						<div class="m-t-80">
							<h1 class="text-center">Thank You for Booking Your trip with us !</h1>
							<div class="m-t-50">

                              <div class="row m-t-80">
								  <div class="col-sm-4 col-sm-offset-4">
									  <a href="/" name="" class="btn btn-primary btn-block">Return to Homepage</a>
								  </div>
							  </div>


							</div>
						</div>
					</div>
				</div>
			</article>
		</section>
    </div>
	

<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.static', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>