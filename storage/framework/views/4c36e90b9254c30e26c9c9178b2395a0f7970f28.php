<?php $__env->startSection('custom-css'); ?>
<style>
	.wrapper {
		background-image: url( <?php echo e(url( 'assets/img/bg1.jpg' )); ?> ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

	#affilliates p{
		line-height: 1.3em;
		text-align: justify;
	}
	.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>




	<div class="top-section-image">
		<img src="<?php echo e(asset('assets/images/bg-image.jpg')); ?>" alt="" class="img-responsive">
	</div>

	<section class="content-wrapper">
		<h1 class="hide"></h1>
		<article class="about-section">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<h2>LICENSEES &amp; AFFILLIATES</h2>
						<div class="m-t-30">
							<p>eRoam is changing the way the travel industry use technology to better service their clients.</p>
							<p>eRoam benefits both the trade and consumer with huge savings through better efficiency.</p>
							<!-- <p><strong>Have a look at our video on how it works:</strong></p> -->
						</div>
					</div>
				</div>
			</div>
		</article>
		<!-- <article class="video-section">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/n5mtBmwy7aA" allowfullscreen="">
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</article> -->
		<article class="">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<h5>AFFILIATES (Earn upto 15% commission):</h5>
						<div class="m-t-20">
							<p>This option is best suited to smaller travel agents, tour desks &amp; tour operators. Once you register as an affiliate, you can start earning commissions immediately from our 100,000's of contracted hotels, activities and transport providers. Once registered you will be given a log-in and can track your commission revenue through your own dashboard. You can also opt-in for a WHITELABEL account which enables you to embed the eRoam platform onto your website so your clients can use the tools and you earn the commissions.</p>
							<p>If you want to become an affiliate now or want more information <a href="#">register here</a></p>
						</div>
						<div class="m-t-30">
							<h5>LICENSEE</h5>
							<div class="m-t-20">
								<p>This is for larger travel agency groups, Inbound Tour Operators and DMC's that want to manage their own contracted product on the eRoam platform. This will enable your consultants to build itineraries for clients in seconds. Licensee accounts can choose to only use their own contracts, or to also access the eroam contracts on a wholesale basis.</p>
								<p>If you want a live demo or more information <a href="#">register here</a>.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</section>



<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.static', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>