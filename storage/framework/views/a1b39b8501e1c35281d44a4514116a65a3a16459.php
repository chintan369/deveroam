<?php $__env->startSection('content'); ?>  
<script src="<?php echo e(url( 'assets/js/theme/jquery.min.js' )); ?>"></script>  
    <!-- <div class="profile_page"> -->
    <?php echo $__env->make('pages.search_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
    <!-- </div> -->

    <?php echo $__env->make('user.profile_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    <?php echo $__env->make('user.profile_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    <?php if(session()->has('profile_step1_success')): ?>
                        <p class="success-box m-t-30">
                            <?php echo e(session()->get('profile_step1_success')); ?>

                        </p>
                    <?php endif; ?>
                    <?php if(session()->has('profile_step1_error')): ?>
                        <p class="danger-box m-t-30">
                            <?php echo e(session()->get('profile_step1_error')); ?>

                        </p>
                    <?php endif; ?>
                    <div class="m-t-30 profile-prefrence">
                        <h2 class="profile-title">Your Profile</h2>
                        <form class="form-horizontal m-t-20" method="post" action="<?php echo e(url('profile_step1')); ?>" id="profile_step1_form">
                            
                            <div class="form-group">
                                <div class="panel-form-group form-group">
                                    <label class="label-control">Title *</label>
                                    <select class="form-control" name="title">
                                        <option value="">Select Title</option>
                                        <option value="mr" <?php echo e($user->customer->title == 'mr' ? 'selected' : ''); ?>>Mr</option>
                                        <option value="mrs" <?php echo e($user->customer->title == 'mrs' ? 'selected' : ''); ?> >Mrs</option>
                                        <option value="ms" <?php echo e($user->customer->title == 'ms' ? 'selected' : ''); ?> >Ms</option>
                                    </select>
                                </div>
                                <label for="title" generated="true" class="error"></label>
                            </div>
                            <!--total /working hours /9 *1.5 -->
                            <div class="form-group">                                
                                <div class="panel-form-group input-control">
                                    <label class="label-control">First Name *</label>
                                    <input type="text" name="first_name" value="<?php echo e($user->customer->first_name); ?>" class="form-control" placeholder="" />                                    
                                </div>
                                <?php if($errors->has('first_name')): ?> 
                                    <span class="error active"><?php echo e($errors->first('first_name')); ?></span>
                                <?php endif; ?>
                                <label for="first_name" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">                                
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Last Name *</label>
                                    <input type="text" name="last_name" value="<?php echo e($user->customer->last_name); ?>" class="form-control" placeholder="" />                                    
                                </div>
                                <?php if($errors->has('last_name')): ?> 
                                    <span class="error active"><?php echo e($errors->first('first_name')); ?></span>
                                <?php endif; ?>
                                <label for="last_name" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Email Address</label>
                                    <input type="text" name="email" value="<?php echo e($user->customer->email); ?>" class="form-control" placeholder="" />                                
                                </div>
                                <label for="email" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Contact Number *</label>
                                    <input type="text" name="contact_no" class="form-control" placeholder="" value="<?php echo e($user->customer->contact_no); ?>"/>
                                    
                                </div>
                                <?php if($errors->has('contact_no')): ?> 
                                    <span class="error active"><?php echo e($errors->first('contact_no')); ?></span>
                                <?php endif; ?>
                                <label for="contact_no" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Enter Existing Password</label>
                                    <input type="password" name="old_password" class="form-control" placeholder="" />                                                                  
                                </div>
                                <?php if($errors->has('old_password')): ?> 
                                    <span class="error active"><?php echo e($errors->first('old_password')); ?></span>
                                <?php endif; ?>
                                <label for="old_password" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">New Password</label>
                                    <input type="password" name="new_password" id="new_password" class="form-control" placeholder="" />                                    
                                </div>
                                <?php if($errors->has('new_password')): ?> 
                                    <span class="error active"><?php echo e($errors->first('new_password')); ?></span>
                                <?php endif; ?>
                                <label for="new_password" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group input-control ">
                                    <label class="label-control">Confirm New Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="" />                                    
                                </div>
                                <?php if($errors->has('confirm_password')): ?> 
                                <span class="error active"><?php echo e($errors->first('confirm_password')); ?></span>
                                <?php endif; ?>
                                <label for="confirm_password" generated="true" class="error"></label>
                            </div>
                            <p><em>Must be at least 8 characters, and ideally contain some or all of: CAPS and lower case, numbers, random characters like #!*.</em></p>
                            
                            <div class="form-group">
                                <div class="panel-form-group">
                                    <label class="label-control">Gender *</label>
                                    <select class="form-control" name="pref_gender">
                                        <option value="">Select Gender</option>
                                        <option value="male" <?php echo e($user->customer->pref_gender == 'male' ? 'selected' : ''); ?>>Male</option>
                                        <option value="female" <?php echo e($user->customer->pref_gender == 'female' ? 'selected' : ''); ?>>Female</option>
                                        <option value="other" <?php echo e($user->customer->pref_gender == 'other' ? 'selected' : ''); ?>>Other</option>
                                    </select>                                    
                                </div>
                                <?php if($errors->has('pref_gender')): ?> 
                                    <span class="error active"><?php echo e($errors->first('pref_gender')); ?></span>
                                <?php endif; ?>
                                <label for="pref_gender" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group ">
                                    <label class="label-control">Age Group *</label>
                                    <select id="age-group" name="pref_age_group_id" class="form-control">
                                        <option value="">Select Age Group</option>
                                        <?php foreach($travel_preferences['age_groups'] as $age_group): ?>
                                            <option value="<?php echo e($age_group['id']); ?>" <?php echo e($user->customer->pref_age_group_id == $age_group['id'] ? 'selected' : ''); ?>><?php echo e($age_group['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>                                    
                                </div>
                                <?php if($errors->has('pref_age_group_id')): ?> 
                                    <span class="error active"><?php echo e($errors->first('pref_age_group_id')); ?></span>
                                <?php endif; ?>
                                <label for="age-group" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group ">
                                    <label class="label-control">Nationality *</label>
                                    <select id="nationality" name="pref_nationality_id" class="form-control">
                                        <option value="">Select Nationality</option>
                                        <?php foreach($travel_preferences['nationalities']['featured'] as $nationality): ?>
                                            <option value="<?php echo e($nationality['id']); ?>" <?php echo e($user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : ''); ?>><?php echo e($nationality['name']); ?></option>
                                        <?php endforeach; ?>
                                            <option disabled> ======================================== </option>
                                        <?php foreach($travel_preferences['nationalities']['not_featured'] as $nationality): ?>
                                            <option value="<?php echo e($nationality['id']); ?>" <?php echo e($user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : ''); ?>><?php echo e($nationality['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>                                    
                                </div>
                                <?php if($errors->has('pref_nationality_id')): ?> 
                                    <span class="error active"><?php echo e($errors->first('pref_nationality_id')); ?></span>
                                <?php endif; ?>
                                <label for="nationality" generated="true" class="error"></label>
                            </div>

                            <div class="form-group">
                                <div class="panel-form-group ">
                                    <label class="label-control">Default Currency *</label>
                                    <select class="form-control" id="select-currency" name="currency">
                                        <option value="">Select Default Currency</option>
                                        <?php foreach(session()->get('all_currencies') as $currency): ?>
                                            <option value="<?php echo e($currency['code']); ?>" <?php echo e($currency['code'] == $user->customer->currency ? 'selected' : ''); ?>>(<?php echo e($currency['code']); ?>) <?php echo e($currency['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>                                    
                                </div>
                                <?php if($errors->has('currency')): ?> 
                                    <span class="error active"><?php echo e($errors->first('currency')); ?></span>
                                <?php endif; ?>
                                <label for="select-currency" generated="true" class="error"></label>
                            </div>
                            <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>">
                            <input type="hidden" name="step" value="1">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="csrf_token">
                            <input type="hidden" id="registered_user_id" value="<?php echo e($user->customer->email); ?>">
                            <div class="m-t-20">
                                <button type="submit" name="" class="btn btn-black btn-block">UPDATE PROFILE</button>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection( 'custom-js' ); ?>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_leftmenu.js?v='.$version.'')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_step1.js?v='.$version.'')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.profileLayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>