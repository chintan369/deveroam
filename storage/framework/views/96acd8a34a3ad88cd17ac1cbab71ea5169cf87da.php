<?php 
  //echo '<pre>';print_r($travel_pref);exit;
      // $travellers['transport_types'] = [];
      // $travellers['transport_types'][0]['id'] =1;
      //  $travellers['transport_types'][0]['name'] = 'Air (Flights)';
      //  $travellers['transport_types'][1]['id'] = 2;
      //  $travellers['transport_types'][1]['name'] = 'Land (All other modes)';
      $travel_preferences['categories'] = [];
      $travel_preferences['categories'][0]['id'] =9;
      $travel_preferences['categories'][0]['name'] = '5 Star (Luxury)';
      $travel_preferences['categories'][1]['id'] =3;
      $travel_preferences['categories'][1]['name'] = '4 Star (Deluxe)';
      $travel_preferences['categories'][2]['id'] = 2;
      $travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
      $travel_preferences['categories'][3]['id'] = 5;
      $travel_preferences['categories'][3]['name'] = '2 Star (Backpacker / Guesthouse)';
      $travel_preferences['categories'][4]['id'] = 1;
      $travel_preferences['categories'][4]['name'] = 'Camping';
                          
?>


<?php $__env->startSection('custom-css'); ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
  <style type="text/css">
    label.error{
      font-size: 12px !important;
    }


    #tours-loader {
      width: 100%;
      /*position: absolute;
      top: 10%;
      transform: translateY( -50% );*/
      text-align: center;
      margin: 10px 0px -20px 0px;
    }

    #tours-loader span{ 
      padding: 8px 21px;
      font-size: 18px;
      font-family: "HelveticaNeue";
    }
    .padding-left-5{
      padding-left: 5px;
    }
  </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
 
  <?php 
    $i=0;
    $allCountry = array();
    foreach($countries as $country){
      foreach ($country['countries'] as $country_data){
          $allCountry[$i]['name'] = $country_data['name'];
          $allCountry[$i]['id'] = $country_data['id'];
          $allCountry[$i]['region'] = $country['id'];
          $allCountry[$i]['regionName'] = $country['name'];
          $i++;
      } 
    }
    usort($allCountry, 'sort_by_name');

    //echo '<pre>'; print_r($allCountry); die;
  ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script type="text/javascript">
      $( function() {
          var projects = [
              <?php for($i=0;$i<count($cities);$i++) { ?>{
                  value: "<?php echo $cities[$i]['id'];?>",
                  label: "<?php echo $cities[$i]['name'];?>",
                  desc: "<?php echo $cities[$i]['country_name'];?>",
                  cid: "<?php echo $cities[$i]['country_id'];?>"
              },
              <?php } ?>
          ];

          var countries = [
              <?php for($i=0;$i<count($allCountry);$i++) { ?>{
                  value : "<?php echo $allCountry[$i]['id'];?>",
                  label : "<?php echo $allCountry[$i]['name'];?>",
                  rid   : "<?php echo $allCountry[$i]['region'];?>",
                  rname : "<?php echo $allCountry[$i]['regionName'];?>"
              },
              <?php } ?>
          ];

          $( "#project" ).autocomplete({
              minLength: 2,
              source: countries,
              focus: function( event, ui ) {
                  $( "#project" ).val( ui.item.label );
                  return false;
              },
              select: function( event, ui ) {
                  var displayVal = ui.item.label;
                  $( "#project" ).val( displayVal );

                  $('#countryId').val(ui.item.value);
                  $('#countryRegion').val(ui.item.rid);
                  $('#countryRegionName').val(ui.item.rname);

                  $('#starting_country').val('');
                  $('#starting_city').val('');

                  $('#destination_country').val('');
                  $('#destination_city').val('');
                 
                  return false;
              }
          }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
              .append( "<div>" + item.label + "</div>" )
              .appendTo( ul );
          };

          $( "#start_location" ).autocomplete({
              minLength: 2,
              source: projects,
              focus: function( event, ui ) {
                  $( "#start_location" ).val( ui.item.label );
                  return false;
              },
              select: function( event, ui ) {
                  var displayVal = ui.item.label +', ' +ui.item.desc;
                  $( "#start_location" ).val( displayVal );

                  var option = $("#search-form").find("input[name=option]:checked").val();

                  if(option == "packages" || option == "manual"){
                    $('#starting_country').val(ui.item.cid);
                    $('#starting_city').val(ui.item.value);

                   // $('#destination_country').val('');
                   // $('#destination_city').val('');
                  } else {

                      $('#starting_country').val(ui.item.cid);
                      $('#starting_city').val(ui.item.value);

                      //$( "#destination_city" ).val( ui.item.value );
                      //$( "#destination_country" ).val( ui.item.cid );
                  }

                  return false;
              }
          }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
              .append( "<div>" + item.label +", " + item.desc + "</div>" )
              .appendTo( ul );
          };

          $( "#end_location" ).autocomplete({
              minLength: 2,
              source: projects,
              focus: function( event, ui ) {
                  $( "#end_location" ).val( ui.item.label );
                  return false;
              },
              select: function( event, ui ) {
                  var displayVal = ui.item.label +', ' +ui.item.desc;
                  $( "#end_location" ).val( displayVal );
                  $( "#destination_city" ).val( ui.item.value );
                  $( "#destination_country" ).val( ui.item.cid );

                  return false;
              }
          }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
              .append( "<div>" + item.label +", " + item.desc + "</div>" )
              .appendTo( ul );
          };
      } );
  </script>

  <section class="banner-container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo e(url( 'assets/images/banner1.jpg' )); ?>" alt="First slide">
        </div>
        <div class="item">
          <img class="third-slide" src="<?php echo e(url( 'assets/images/banner2.jpg' )); ?>" alt="Third slide">
        </div>
      </div>
      <div class="banner-search">
        <div class="banner-inner">
          <p><img src="<?php echo e(url( 'assets/images/eRoam_Logo.png' )); ?>" alt="eroam" class="carousel-logo"></p>
          <p>Over 500,000 hotels, 1,000 airlines, plus 100,000’s events, activities &amp; restaurants.</p>

          <form method="post" action="tours" id="search-form" class="form-horizontal clearfix">
              <?php echo e(csrf_field()); ?>


              <input type="hidden" name="country" id="starting_country" value="<?php echo e($from_country_id); ?>">
              <input type="hidden" name="city" id="starting_city" value="<?php echo e($from_city_id); ?>">

              <input type="hidden" name="to_country" id="destination_country" value="">
              <input type="hidden" name="to_city" id="destination_city"  value="">

              <input type="hidden" name="auto_populate" id="auto-populate" value="1">

              <input type="hidden" name="countryId" id="countryId" value="">
              <input type="hidden" name="countryRegion" id="countryRegion" value="">
              <input type="hidden" name="countryRegionName" id="countryRegionName" value="">
              <input type="hidden" id="search_option_id" name="option" value="packages">

              <div id="changePreferences">
                <?php 
                  /*
                  | Added by Rekha
                  | Accomodation multi select options
                  */
                  $accommodation_options = '';
                  $room_type_options = '';
                  $transport_type_options  = '';
                  $nationality = '';
                  $gender = '';
                  $age_group = '';
                  $interestoption ='';
                  
                  if ( count( $travellers['categories'] ) > 0 ) {
                    foreach( $travellers['categories'] as $category ){
                      if( empty( $category['name'] ) ){
                        continue;
                      }
                      if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
                        $accommodation_options .= '<input type="hidden" name="hotel_category_id[]"  value="'.$category['id'].'" >';
                      }
                    }
                  }

                  if( count( $travellers['room_types'] ) > 0 ){
                    foreach( $travellers['room_types'] as $room_type){
                      if( empty( $room_type['name'] ) ){
                        continue;
                      }
                      if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
                        $room_type_options .= '<input type="hidden" name="room_type_id[]" value="'.$room_type['id'].'" >';
                      }
                    }
                  }

                  if( count( $travellers['transport_types'] ) > 0 ){
                    foreach( $travellers['transport_types'] as $transport_type){
                      if( empty( $transport_type['name'] ) ){
                          continue;
                      }
                      if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
                        $transport_type_options .= '<input type="hidden" name="transport_types[]" value="'.$transport_type['id'].'" >';
                      }
                    }
                  }
              

                  if( count( $travellers['nationalities'] ) > 0 ){
                    foreach( $travellers['nationalities']['featured'] as $featured ){
                      if( empty( $featured['name'] ) ){
                        continue;
                      }

                      if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) {
                        $nationality .= '<input type="hidden" name="nationality" value="'.$featured['id'].'" > ';
                      }
                    }
                    
                    foreach( $travellers['nationalities']['not_featured'] as $not_featured ){
                      if( empty( $not_featured['name'] ) ){
                        continue;
                      }
                      if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) ){
                        $nationality .= '<input type="hidden" name="nationality" value="'.$not_featured['id'].'" >';
                      }
                    }
                  }

                  $gender_array = ['male', 'female', 'other'];
                  foreach($gender_array as $gen){
                    if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) {
                      $gender .= '<input type="hidden" name="gender" value="'.$gen.'">';
                    }
                  }
                
                  if( count( $travellers['age_groups'] ) > 0 ){
                    foreach( $travellers['age_groups'] as $age){
                      if( empty( $age['name'] ) ){
                        continue;
                      }
                      if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) {
                        $age_group .= '<input type="hidden" name="age_group" value="'.$age['id'].'" >'; 
                      }
                    }
                  }

                  if( count($labels) > 0 ){
                    foreach ($labels as $i => $label){
                      if( empty( $age['name'] ) ){
                        continue;
                      }

                      $sequence = '';
                      if( in_array($label['id'], $interest_ids ) ){ 
                        $sequence = array_search($label['id'], $interest_ids); 
                        $sequence += 1;
                        $interestoption .='<input type="hidden" name="interests['.$sequence.']" value="'.$label['id'].'">';
                      }      
                    }
                  }
                  echo $accommodation_options.$room_type_options.$transport_type_options.$nationality.$gender.$age_group.$interestoption;
                ?>
              </div>

             <!--  <div class="col-md-10 col-md-offset-1" > -->
                <!-- <div class="searchbox-new search-box1" >
                  <div class="row">

                    <div class="col-sm-10 project" style="display:none;">
                      <input type="text" value="" name="project" id="project" class="form-control" placeholder="Where do you want to go?">
                    </div>
                    <div class="col-sm-4 startLocation" >
                      <input type="text" value="" name="start_location" id="start_location" class="form-control" placeholder="Start Location">
                    </div>
                    <div class="col-sm-3 endLocation" style="display:none;">
                      <input type="text" value="" name="end_location" id="end_location" class="form-control" placeholder="End Location">
                    </div>
                    <div class="col-sm-3 travellers">
                      <input id="travellers" value="" type="number" name="travellers" class="form-control" placeholder="No. of Travellers" min="1" max="10" />
                    </div>
                    <div class="col-sm-3 startDate">
                      <input id="start_date" value="" name="start_date" type="text" class="form-control" placeholder="Start Date" >
                    </div>
                    <div class="col-sm-2">
                      <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                    </div>
                  </div>
                </div> -->

                <!-- <div class="row">
                  <div class="col-sm-9 col-xs-12">
                    <div class="checkbox-group">
                      <label class="radio-checkbox label_radio m-r-10" for="checkbox-01"><input type="radio" name="option" id="checkbox-01" value="packages">Tour Packages</label>
                      <label class="radio-checkbox label_radio m-r-10" for="checkbox-02"><input type="radio" name="option" id="checkbox-02" value="manual" checked>Tailormade Manual</label>
                      <label class="radio-checkbox label_radio m-r-10" for="checkbox-03"><input type="radio" name="option" id="checkbox-03" value="auto">Tailormade Auto</label>
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <p class="advance-right"><a href="#" class="advance-text" data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="fa fa-th-large" style="font-size:20px;"></i> Manage Profile </a></p>
                  </div>
                </div> -->
                <!-- <div class="text-left profile_info">
                  <div id="profile-preferences-data">
                    
                          <span id="_accommodation"><strong>Accommodation:</strong> <?php echo e(isset($travel_pref['accommodation_name']) && !empty($travel_pref['accommodation_name']) ? '  '.join(', ', $travel_pref['accommodation_name']) : 'All'); ?> </span> 
                        
                          <span id="_transport"> <strong>Transport:</strong> <?php echo e(isset($travel_pref['transport_name']) && !empty($travel_pref['transport_name']) ? ' '.join(', ', $travel_pref['transport_name']) : 'All'); ?> </span>  
                        
                          <span id="_nationality"> <strong>Nationality:</strong> <?php echo e(isset($travel_pref['nationality']) && !empty($travel_pref['nationality']) ? ' '.$travel_pref['nationality'] : 'All'); ?> </span>
                  </div>
                  <div>
                    
                          <span id="_age"><strong>Age:</strong><?php echo e(isset($travel_pref['age_group']) && !empty($travel_pref['age_group']) ? ' '.$travel_pref['age_group'] : 'All'); ?></span>
                       
                    
                  </div>
                  <div>
                    
                          <span id="_interests"> <strong>Interests:</strong><?php echo e(isset($travel_pref['interestLists']) && !empty($travel_pref['interestLists']) ? '  '.$travel_pref['interestLists'] : 'All'); ?></span>
                        
                    
                  </div>  
               </div>  -->
             <!--  </div> -->

              <div class="col-md-10 col-md-offset-1">
                
                    <div class="row m-t-10" id="tour_package_id">
                      <div class="col-sm-3 padding-right-0">
                        <div class="dropdown packages search_dropdown_menu">
                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <span id="packages-option" class="packages-option" data-value="packages">Tour Packages</span> <span class="fa fa-angle-down"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="packages-option">
                            <li><a href="javascript://" value="packages">Tour Packages</a></li>
                              <li><a href="javascript://" value="manual">Tailormade Manual</a></li>
                              <li><a href="javascript://" value="auto">Tailormade Auto</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-sm-7 padding-0">
                        <input type="text" value="" name="project" id="project" class="form-control ui-autocomplete-input" placeholder="Where do you want to go?" autocomplete="off">
                      </div>
                      <div class="col-sm-2 padding-left-0">
                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                      </div>
                    </div>
                    <div class="m-t-10" id="tailormade_manual_package_id" style="display:none;">
                      <div class="row m-t-10">

                        <div class="col-sm-10">
                          <div class="row">
                            <div class="col-sm-3 padding-right-0">
                              <div class="dropdown packages search_dropdown_menu">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <span id="packages-option" class="packages-option" data-value="manual">Tailormade Manual</span> <span class="fa fa-angle-down"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="packages-option">
                                 <li><a href="javascript://" value="packages">Tour Packages</a></li>
                                  <li><a href="javascript://" value="manual">Tailormade Manual</a></li>
                                  <li><a href="javascript://" value="auto">Tailormade Auto</a></li>
                                </ul>
                              </div>
                            </div>  
                            <div class="col-sm-6 startLocation padding-left-5">
                              <input type="text" value="" name="start_location" id="start_location" class="form-control ui-autocomplete-input" placeholder="Start Location" autocomplete="off">
                            </div>
                            <div class="col-sm-3 endLocation">
                              <input type="text" value="" name="end_location" id="end_location" class="form-control ui-autocomplete-input" placeholder="End Location" autocomplete="off">
                            </div>
                            <div class="col-sm-1 travellers">
                              <input id="travellers" value="" type="number" name="travellers" class="form-control" placeholder="No. of Travellers" min="1" max="10">
                            </div>
                            <div class="col-sm-2 startDate">
                              <input id="start_date"  value="" name="start_date" type="text" class="form-control start_date" placeholder="Start Date">
                            </div>
                          </div>
                        </div>    
                        <div class="col-sm-2">
                          <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-12">
                            <p class="advance-right"><a href="#" class="advance-text" data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="fa fa-th-large" style="font-size:20px;"></i> Manage Profile </a></p>
                          </div>
                      </div>  
                    </div>
                    <!-- <div class="m-t-10" id="tailormade_auto_package_id">
                      <div class="row">
                        <div class="col-sm-3">
                          <div class="dropdown packages search_dropdown_menu">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <span id="packages-option" class="packages-option" data-value="auto">Tailormade Auto</span> <span class="fa fa-angle-down"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="packages-option">
                              <li><a href="javascript://" value="packages">Tour Packages</a></li>
                              <li><a href="javascript://" value="manual">Tailormade Manual</a></li>
                              <li><a href="javascript://" value="auto">Tailormade Auto</a></li>
                            </ul>
                          </div>
                        </div>
                        <div class="col-sm-9">
                          <p class="advance-right"><a href="#" class="advance-text" data-toggle="modal" data-target="#preferencesModal" id="openModel"><i class="fa fa-th-large" style="font-size:20px;"></i> Manage Profile </a></p>
                        </div>
                      </div> -->
                      <!-- <div class="row m-t-10">  
                        <div class="col-sm-3 startLocation">
                          <input type="text" value="" name="start_location" id="start_location" class="form-control ui-autocomplete-input" placeholder="Start Location" autocomplete="off">
                        </div>
                        <div class="col-sm-3 endLocation">
                          <input type="text" value="" name="end_location" id="end_location" class="form-control ui-autocomplete-input" placeholder="End Location" autocomplete="off">
                        </div>
                        <div class="col-sm-2 travellers">
                          <input id="travellers" value="" type="number" name="travellers" class="form-control" placeholder="No. of Travellers" min="1" max="10">
                        </div>
                        <div class="col-sm-2 startDate">
                          <input id="start_date" value="" name="start_date" type="text" class="form-control start_date" placeholder="Start Date">
                        </div>
                        <div class="col-sm-2">
                          <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                        </div>
                      </div> -->
                    <!-- </div> -->

                    <div class="text-left profile_info" style="display:none;">
                  <div id="profile-preferences-data">
                    
                          <span id="_accommodation"><strong>Accommodation:</strong> <?php echo e(isset($travel_pref['accommodation_name']) && !empty($travel_pref['accommodation_name']) ? '  '.join(', ', $travel_pref['accommodation_name']) : 'All'); ?> </span> 
                        
                          <span id="_transport"> <strong>Transport:</strong> <?php echo e(isset($travel_pref['transport_name']) && !empty($travel_pref['transport_name']) ? ' '.join(', ', $travel_pref['transport_name']) : 'All'); ?> </span>  
                        
                          <span id="_nationality"> <strong>Nationality:</strong> <?php echo e(isset($travel_pref['nationality']) && !empty($travel_pref['nationality']) ? ' '.$travel_pref['nationality'] : 'All'); ?> </span>
                  </div>
                  <div>
                    
                          <span id="_age"><strong>Age:</strong> <?php echo e(isset($travel_pref['age_group']) && !empty($travel_pref['age_group']) ? ' '.$travel_pref['age_group'] : 'All'); ?></span>
                  </div>
                  <div>
                    
                          <span id="_interests"> <strong>Interests:</strong> <?php echo e(isset($travel_pref['interestLists']) && !empty($travel_pref['interestLists']) ? '  '.$travel_pref['interestLists'] : 'All'); ?></span>
                  </div>  
               </div> 
                  </div>
          </form>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
    </div>
  </section>

  <section class="content-wrapper">
    <h1 class="hide"></h1>
    <article class="places-section eroam-trending">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <!-- <h2>TRENDING HOLIDAYS</h2> -->
            <h2>Top Tours</h2>
            <div class="places-list">

              <div id="tours-loader">
                <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
              </div>
              <div class="row" id="tourList"></div>
            </div>
          </div>
        </div>
      </div>
    </article>
  </section>

  <?php 
      /*
      | Added by Rekha
      | Accomodation multi select options
      */
      $accommodation_name = ''; 
      $accommodation_options = '';
      $room_type_name = '';
      $room_type_options = '';
      $transport_type_name = '';
      $transport_type_options  = '';
      $nationality = '';
      $nationality_name = '';
      $age_group = '';
      $age_group_name = '';
      $gender = '';
      $gender_name = '';

      

      if ( count( $travellers['categories'] ) > 0 ) {
        foreach( $travellers['categories'] as $category ){
          if( empty( $category['name'] ) ){
            continue;
          }
          
          if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
            $accommodation_name .= '<span title="'.$category['name'].'" class="drop-selected">'.$category['name'].'</span>';
            //$accommodation_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="hotel_category" name="hotel_category_id[]" id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" checked> '.$category['name'] .' </label></li>';
            $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" selected="selected">'.$category['name'].'</option>';
          }else{
            //$accommodation_options .= '<li><label ><input type="radio" class="hotel_category" name="hotel_category_id[]" id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'"> '.$category['name'] .' </label></li>';
            $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'" >'.$category['name'].'</option>';
          }
        }
      }

      if( count( $travellers['room_types'] ) > 0 ){
        foreach( $travellers['room_types'] as $room_type){
          if( empty( $room_type['name'] ) ){
            continue;
          }
          if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
            $room_type_name .= '<span title="'.$room_type['name'].'" class="drop-selected">'.$room_type['name'].'</span>';
            //$room_type_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="room_type_options" name="room_type_id[]" value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" checked> '.$room_type['name'] .' </label></li>';
            $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" selected="selected"> '.$room_type['name'].'</option>';
          }else{
            //$room_type_options .= '<li><label ><input type="radio" class="room_type_options" name="room_type_id[]" value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'"> '.$room_type['name'] .' </label></li>';
            $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'" > '.$room_type['name'].'</option>';
          }
    
        }
      }

      if( count( $travellers['transport_types'] ) > 0 ){
        foreach( $travellers['transport_types'] as $transport_type){
          if( empty( $transport_type['name'] ) ){
              continue;
          }
          if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
            $transport_type_name .= '<span title="'.$transport_type['name'].'" class="drop-selected">'.$transport_type['name'].'</span>';
            //$transport_type_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="transport_type_options" name="transport_types[]" value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" checked> '.$transport_type['name'] .' </label></li>';
            $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" selected="selected">'.$transport_type['name'].'</option>';
          }else{
            //$transport_type_options .= '<li><label ><input type="radio" class="transport_type_options" name="transport_types[]" value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'"> '.$transport_type['name'] .' </label></li>';
            $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'">'.$transport_type['name'].'</option>';
          }
        }
      }

      if( count( $travellers['nationalities'] ) > 0 ){
        foreach( $travellers['nationalities']['featured'] as $featured ){
          if( empty( $featured['name'] ) ){
            continue;
          }
          if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) {
            $nationality_name .= '<span title="'.$featured['name'].'" class="drop-selected">'.$featured['name'].'</span>';
            //$nationality .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="nationality" name="nationality" value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" checked> '.$featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" selected="selected">'.$featured['name'] .'</option>';
          }else{
            //$nationality .= '<li><label ><input type="radio" class="nationality" name="nationality" value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'"> '.$featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'" >'.$featured['name'] .'</option>';
          }
        }

        foreach( $travellers['nationalities']['not_featured'] as $not_featured ){
          if( empty( $not_featured['name'] ) ){
            continue;
          }
          if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) ){
            $nationality_name .= '<span title="'.$not_featured['name'].'" class="drop-selected">'.$not_featured['name'].'</span>';
            //$nationality .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="nationality" name="nationality" value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" checked> '.$not_featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" selected="selected"> '.$not_featured['name'] .'</option>';
          }else{
            //$nationality .= '<li><label ><input type="radio" class="nationality" name="nationality" value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .'</option>';
          }
        }
      }

      $gender_array = ['male', 'female', 'other'];
      foreach($gender_array as $gen){
        if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) {
          $gender_name .= '<span title="'.$gen.'" class="drop-selected">'.ucfirst($gen).'</span>';
          //$gender .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="gender" name="gender" value="'.$gen.'" data-checked="1" data-name="'.$gen.'" checked> '.ucfirst($gen) .' </label></li>';
          $gender .= '<option value="'.$gen.'" data-checked="1" data-name="'.$gen.'" selected="selected"> '.ucfirst($gen) .'</option>';
        }else{
          //$gender .= '<li><label ><input type="radio" class="gender" name="gender" value="'.$gen.'" data-checked="0" data-name="'.$gen.'"> '.ucfirst($gen) .' </label></li>';
          $gender .= '<option value="'.$gen.'" data-checked="0" data-name="'.$gen.'" > '.ucfirst($gen) .'</option>';
        }
      }
    
      if( count( $travellers['age_groups'] ) > 0 ){
        foreach( $travellers['age_groups'] as $age){
          if( empty( $age['name'] ) ){
            continue;
          }
          if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) {
            $age_group_name .= '<span title="'.$age['name'].'" class="drop-selected">'.$age['name'].'</span>';
            //$age_group .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="age_group" name="age_group" value="'.$age['id'].'" data-checked="1" data-name="'.$age['name'].'" checked> '.$age['name'] .' </label></li>';
            $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" selected="selected" data-checked="1">'.$age['name'].'</option>';
          }else{
            //$age_group .= '<li><label ><input type="radio" class="age_group" name="age_group" value="'.$age['id'].'" data-checked="0" data-name="'.$age['name'].'"> '.$age['name'] .' </label></li>';
            $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" data-checked="0">'.$age['name'].'</option>';
          }
        }
      }
  ?>

  <!-- Modal -->
  <div class="modal fade" id="preferencesModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-large" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="modal-icon">
            <svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                <desc>Created with Sketch.</desc>
                <defs></defs>
                <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="42---EROA007-V4.1-Generic-(B2B-Agent-Screen)-02" transform="translate(-320.000000, -171.000000)" fill="#212121">
                        <g id="Group-3" transform="translate(291.000000, 144.000000)">
                            <g id="Group-2">
                                <path d="M31.8186047,48.7674419 C33.0495814,47.4647442 35.9940465,46.0138605 38.4546047,45.1562791 L41.1141395,47.4181395 C41.5310698,47.7728372 42.1436279,47.7728372 42.5605581,47.4181395 L45.220093,45.1562791 C47.6806512,46.0138605 50.6251163,47.4647442 51.856093,48.7674419 L31.8186047,48.7674419 Z M45.316093,42.8346977 C44.9488372,42.7169302 44.5472558,42.7978605 44.2536744,43.0476279 L41.8372093,45.1024186 L39.4210233,43.0476279 C39.1274419,42.7978605 38.7258605,42.7169302 38.3586047,42.8346977 C36.795814,43.3331163 29,46.016093 29,49.8837209 C29,50.500186 29.499814,51 30.1162791,51 L53.5581395,51 C54.1746047,51 54.6744186,50.500186 54.6744186,49.8837209 C54.6744186,46.016093 46.8786047,43.3331163 45.316093,42.8346977 L45.316093,42.8346977 Z M41.8372093,29.2325581 C44.7495814,29.2325581 47.1188837,31.6166512 47.1188837,34.5474419 C47.1188837,37.4787907 44.7495814,39.8634419 41.8372093,39.8634419 C38.9251163,39.8634419 36.555814,37.4787907 36.555814,34.5474419 C36.555814,31.6166512 38.9251163,29.2325581 41.8372093,29.2325581 L41.8372093,29.2325581 Z M41.8372093,42.096 C45.9805581,42.096 49.3514419,38.7097674 49.3514419,34.5474419 C49.3514419,30.3856744 45.9805581,27 41.8372093,27 C37.6941395,27 34.3232558,30.3856744 34.3232558,34.5474419 C34.3232558,38.7097674 37.6941395,42.096 41.8372093,42.096 L41.8372093,42.096 Z" id="user"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
          </div>
          <div class="modal-container">
            <h4 class="modal-title" id="myModalLabel">Personal Profile Preferences</h4>
            <p>Please fill in the details below to personalise your travel preferences.</p>
          </div>
        </div>
        <div class="modal-body">
          <div class="modal-container">
            <form class="form-horizontal">
             <div class="row">
               <div class="col-sm-6">
                 <div class="select-box">
                  <select class="form-control age_group" name="age_group" id="dropdown-age">
                    <option value="">Select Age Group</option>
                    <?php echo $age_group; ?>

                  </select>
                </div>
               </div>
               <div class="col-sm-6">
                 <div class="select-box">
                  <select class="form-control transport_type_options" name="transport_types[]" id="dropdown-transport">
                    <option value="">Select Transport Type</option>
                    <?php echo $transport_type_options; ?>

                  </select>
                </div>
               </div>
             </div>

             <div class="row">
               <div class="col-sm-6">
                 <div class="select-box">
                  <select class="form-control gender" name="gender" id="dropdown-gender">
                    <option value="">Select Gender</option>
                    <?php echo $gender; ?>

                  </select>
                </div>
               </div>
               <div class="col-sm-6">
                 <div class="select-box">
                  <select class="form-control hotel_category" name="hotel_category_id[]" id="dropdown-accommodation">
                    <option value="">Select Accommodation Type</option>
                    <?php echo $accommodation_options; ?>

                  </select>
                </div>
               </div>
             </div>

             <div class="row">
               <div class="col-sm-6">
                 <div class="select-box">
                  <select class="form-control nationality" name="nationality" id="dropdown-nationality">
                    <option value="">Select Nationality</option>
                    <?php echo $nationality; ?>

                  </select>
                </div>
               </div>
               <div class="col-sm-6" style="display:none;">
                 <div class="select-box">
                  <select class="form-control room_type_options" name="room_type_id[]" id="dropdown-room">
                    <option>Select Room Type</option>
                    <?php echo $room_type_options; ?>

                  </select>
                </div>
               </div>
             </div>

              <div class="m-t-20" style="display:none;">
               <h4 class="modal-title" id="myModalLabel">Holiday Preferences</h4>
               <div class="row">
                 <div class="col-sm-6">
                   <label>Price Range Per Day</label>
                   <div class="layout-slider">
                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                    <input id="Slider3" type="slider" name="price2" value="100;10000" />
                    </span>
                  </div>
                 </div>
                 <div class="col-sm-6">
                   <label>Number of Total Days</label>
                  <div class="layout-slider">
                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                    <input id="Slider4" type="slider" name="price3" value="1;100" />
                    </span>
                  </div>
                 </div>
               </div>
              </div>
            
            <div class="m-t-20">
              <h4 class="modal-title" id="myModalLabel">Activity Preferences</h4>
              <p>Please drag your preferred activities into the boxes below. <?php /*Highest priority begins on the left.*/ ?></p> 

              <div class="m-t-10">
                <ul class="drop-list">

                  <?php 
                      $imgArr = array(
                        15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                        5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                        13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                        29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                        11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                        20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                         //'Expeditions','Astronomy','Food','Short-Breaks'
                        );

                      //echo '<pre>'; print_r($labels); echo '</pre>';
                      $ij = 0;
                      if( count($interest_ids) > 0 ){
                        foreach ($interest_ids as $j => $interest_id){
                          $key = array_search($interest_id, array_column($labels, 'id'));
                          $label = $labels[$key];
                          $ij++;
                  ?>
                            <li ondrop="drop(event)" ondragover="allowDrop(event)" class="interest-button-active" id="interest-<?php echo e($label['id']); ?>" data-sequence="<?php echo e($ij); ?>" data-name="<?php echo e($label['name']); ?>" >
                              <img src="<?php echo url( 'assets/images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_<?php echo e($label['id']); ?>" class="img-responsive" alt="<?php echo e($j); ?> - <?php echo e($label['name']); ?>" data-value="<?php echo e($label['id']); ?>" data-name="<?php echo e($label['name']); ?>" title="<?php echo e($label['name']); ?>">
                              <input type="hidden" id="inputValue_<?php echo e($label['id']); ?>" class="input-interest" name="interests[<?php echo e($j + 1); ?>]" value="<?php echo e($label['id']); ?>">
                            </li>
                  <?php  } } ?>

                  <?php 
                    for($k = $ij; $k < 8; $k++){
                      echo '<li ondrop="drop(event)" ondragover="allowDrop(event)" class="blankInterest" data-sequence="'.($k + 1).'"></li>';
                    }
                  ?>
                </ul>
              </div>
              <div class="m-t-10">

                <ul class="drag-list interest-lists first">
                  <?php if( count($labels) > 0 ): ?>
                    <?php foreach($labels as $i => $label): ?>

                      <?php if( empty($label['name']) ): ?>
                        <?php continue; ?>
                      <?php endif; ?>
                      <li ondrop="drop(event)" ondragover="allowDrop(event)" data-value="<?php echo e($label['id']); ?>" class="<?php echo e(in_array($label['id'], $interest_ids ) ? ' interestMove' : ''); ?>" id="interest-<?php echo e($label['id']); ?>">
                        <?php
                          $namedis = 'block';
                          if(!in_array($label['id'], $interest_ids )){
                            $namedis = 'none';
                        ?>
                          <img src="<?php echo url( 'assets/images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_<?php echo e($label['id']); ?>" class="img-responsive" alt="<?php echo e($i); ?> - <?php echo e($label['name']); ?>" data-value="<?php echo e($label['id']); ?>"  data-name="<?php echo e($label['name']); ?>" title="<?php echo e($label['name']); ?>">
                          <input type="hidden" id="inputValue_<?php echo e($label['id']); ?>" class="input-interest" name="interests[]" value="">
                        <?php } ?>
                          <span style="display:<?php echo e($namedis); ?>;" id="name_<?php echo e($label['id']); ?>"><?php echo e($label['name']); ?></span>
                          
                      </li>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </ul>

              </div>

              <div class="m-t-30 text-right">
                <a href="#" data-dismiss="modal" class="m-r-10">Close</a>
                <a href="javascript://" id="save_travel_preferences">Save</a>
              </div>
            </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection( 'custom-js' ); ?>

  <script type="text/javascript">
      var countryName   = '<?php echo e($countryName); ?>';
      var default_currency   = '<?php echo e($default_currency); ?>';
      var siteUrl = $( '#site-url' ).val();
      var activitySequence = [<?php echo e(join(', ', $interest_ids)); ?>];

      function getRole() {
        return $(".packages-option").attr('data-value');
      }

      $(document).ready(function(){
          $('#tailormade_manual_package_id').hide();
          $('#tailormade_auto_package_id').hide();
          $('#packages-option li a').click(function () {
             var value = $(this).attr('value');
             var clickedValue = $(this).text();
             if (value == 'packages') {
                  $('#search_option_id').val('packages');
                  $('#tour_package_id').show();
                  $('#tailormade_manual_package_id').hide();
                  $('#tailormade_auto_package_id').hide();

                  $('#search-form').attr('action', 'tours');
                  $('.project').show();
                  $('#destination_country').val('');
                  $('#destination_city').val('');

                  var  data = { fromTourHome: 'Yes'};
                  eroam.ajax('post', 'session/set_tourHome', data, function(response){ });
              }
              if (value == 'manual') {
                  $('#search-form').attr('action', 'search');
                  $('#search_option_id').val('manual');
                  $('#tour_package_id').hide();
                  $('#tailormade_manual_package_id').show();
                  // $('#tailormade_auto_package_id').hide();

                  $('#search-form').attr('action', 'search');
                  $('.project').hide();
                  if($('.startLocation').hasClass('col-sm-3')){
                      $('.startLocation').addClass('col-sm-6').removeClass('col-sm-3').show();
                  }
                  
                  $('.endLocation').hide();
                  // $('.travellers').addClass('col-sm-3').removeClass('col-sm-2').show();
                  // $('.startDate').addClass('col-sm-3').removeClass('col-sm-2').show();
              }
              if (value == 'auto') {
                  $('#search-form').attr('action', 'search');
                  $('#search_option_id').val('auto');
                  $('#tour_package_id').hide();
                  $('#tailormade_manual_package_id').show();

                  $('#search-form').attr('action', 'search');
                  $('.project').hide();

                  $('.startLocation').addClass('col-sm-3').removeClass('col-sm-6').show();
                  $('.endLocation').show();
                  // $('.travellers').addClass('col-sm-2').removeClass('col-sm-3').show();
                  // $('.startDate').addClass('col-sm-2').removeClass('col-sm-3').show();
              }
                
              $('.packages-option').html(clickedValue);
              $('.packages-option').attr('data-value',value);

          });  
          tourList();

          var tomorrow = new Date();
          tomorrow.setDate(tomorrow.getDate()+1);
          var nextWeek  = new Date();
          nextWeek.setDate(nextWeek.getDate()+7);
          $(".start_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: tomorrow,
            todayHighlight: true
          }).on('changeDate', function(e) {
              $(".start_date").val($("#start_date2").datepicker('getFormattedDate'));
          });

          $('.radio-checkbox').click(function(){
              var option = $("input[name='option']:checked"). val();
              //$('#starting_country').val( "<?php echo e($from_country_id); ?>");
              //$('#starting_city').val( "<?php echo e($from_city_id); ?>" );

              if(option == "packages"){

                $('#search-form').attr('action', 'tours');
                $('.project').show();
                $('.startLocation').hide();
                $('.endLocation').hide();
                $('.travellers').hide();
                $('.startDate').hide();

                $('#destination_country').val('');
                $('#destination_city').val('');

                var  data = { fromTourHome: 'Yes'};
                eroam.ajax('post', 'session/set_tourHome', data, function(response){ });

              } else if(option == "manual"){

                $('#search-form').attr('action', 'search');
                $('.project').hide();
                $('.startLocation').addClass('col-sm-4').removeClass('col-sm-3').show();
                $('.endLocation').hide();
                $('.travellers').addClass('col-sm-3').removeClass('col-sm-2').show();
                $('.startDate').addClass('col-sm-3').removeClass('col-sm-2').show();

                //$('#destination_country').val('');
               // $('#destination_city').val('');

              } else if(option == "auto"){


                $('#search-form').attr('action', 'search');
                $('.project').hide();
                $('.startLocation').addClass('col-sm-3').removeClass('col-sm-4').show();
                $('.endLocation').show();
                $('.travellers').addClass('col-sm-2').removeClass('col-sm-3').show();
                $('.startDate').addClass('col-sm-2').removeClass('col-sm-3').show();


              } else {  }
          });
          var sform = $("#search-form");       
          sform.validate({
              ignore: [],
              rules: {
                  project:{
                      required: function(element) {
                        return (getRole() == 'packages');
                      }
                  },
                  start_location: {
                      required: function(element) {
                        return (getRole() == 'manual' || getRole() == 'auto');
                      }
                  },
                  end_location:{
                      required: function(element) {
                        return (getRole() == 'auto');
                      }
                  },
                  travellers:{
                      required: function(element) {
                        return (getRole() == 'manual' || getRole() == 'auto');
                      }
                  },
                  start_date: {
                      required: function(element) {
                        return (getRole() == 'manual' || getRole() == 'auto');
                      }
                  }, 
              },
              errorPlacement: function (label, element) {
                  label.insertAfter(element);
              },
              submitHandler: function (form) {
                if($('#search-form').attr('action') == 'tours'){
                  var  data = { fromTourHome: 'Yes'};
                  eroam.ajax('post', 'session/set_tourHome', data, function(response){
                    form.submit();
                  });
                } else {
                  form.submit();
                }
                
              }
          })

          $( "#preferencesModal").on('shown.bs.modal', function(){
              $("#Slider3").slider({ 
                from: 100, to: 10000, step: 1 , smooth: true, round: 0, dimension: "", skin: "round", limits: false 
              });

              $("#Slider4").slider({ 
                from: 1, to: 100, step: 1 , smooth: true, round: 0, dimension: "&nbsp;Days", skin: "round", limits: false
              });
          }); 

          /*
          | Temporary saving for the travel pref
          */
          $('body').on('click', '#save_travel_preferences', function() {

            var btn = $(this).button('loading');

            if($('.nationality_id').val()){
              var nationality_dom = $(".nationality option:selected").text();
            }else{
              var nationality_dom = $(".nationality option:selected").val();
            }
            
            if($('.age_group').val()){
              var age_group_dom = $(".age_group option:selected").text();
            }else{
              var age_group_dom = $(".age_group option:selected").val();
            }
            var age_group_id = $('.age_group').val();
            var nationality_id = $('.nationality').val();
            var gender = $('.gender').val();
           
            var nationality = ( isNotUndefined( nationality_dom ) ) ? nationality_dom : [] ;
            var age_group = ( isNotUndefined( age_group_dom ) ) ? age_group_dom : [] ;
            var interestLists = [];
            var accommodations = [];
            var accommodationIds = [];
            var interestListIds = [];
            var roomTypeIds = [];
            var roomTypes = [];
            var transportTypeIds = [];
            var transportTypes = [];
              
            $('.hotel_category option:selected').each(function(){
              if($(this).val()){
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                accommodationIds.push(id);
                accommodations.push(name);
              }
            });

            $('.room_type_options option:selected').each(function(){
              var id = parseInt($(this).val());
              var name = $(this).attr('data-name');
              roomTypeIds.push(id);
              roomTypes.push(name);
            });
            
            $('.transport_type_options option:selected').each(function(){
              if($(this).val()){
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                transportTypeIds.push(id);
                transportTypes.push(name);
              }
            });
            
            $('.interest-button-active').each(function(){
                interestLists.push($(this).attr('data-name'));
            });

            var  data = { 
              travel_preference: [{
                accommodation:accommodationIds, 
                accommodation_name: accommodations,
                room_name: roomTypes,
                room: roomTypeIds,
                transport_name: transportTypes,
                transport: transportTypeIds,
                age_group: age_group,
                nationality: nationality,
                gender: gender,
                interestLists: interestLists.join(', '),
                interestListIds: activitySequence
                //interestListIds: interestListIds
              }]
            };
            

            var interestText = interestLists.length > 0 ? interestLists.join(', ') : 'All';
            var accommodation = accommodations.length > 0 ? accommodations.join(', ') : 'All';
            var transport = transportTypes.length > 0 ? transportTypes.join(', ') : 'All';
            var nationalityText =  nationality !=''  ? nationality : 'All'; 
          
            var ageGroupText =  age_group !='' ? age_group : 'All'; 

            $('#_accommodation').html(' <strong> Accommodation: </strong> '+accommodation);
            $('#_transport').html(' <strong> Transport: </strong> '+transport);
            $('#_nationality').html(' <strong> Nationality: </strong> '+nationalityText);
            $('#_age').html(' <strong> Age: </strong> '+ageGroupText);
            $('#_interests').html(' <strong> Interests: </strong> '+ interestText);
            

            <?php if(session()->has('user_auth')): ?>
              var post = {
                hotel_categories:accommodationIds, 
                hotel_room_types: roomTypeIds,
                transport_types: transportTypeIds,
                age_group: age_group_id,
                nationality: nationality_id,
                gender: gender,
                interests: activitySequence,
                _token: $('meta[name="csrf-token"]').attr('content'),
                user_id: "<?php echo e(session()->get('user_auth')['user_id']); ?>"
              };
              eroam.ajax('post', 'save/travel-preferences', post, function(response){
                //console.log(post);
              });
            <?php endif; ?>;

            eroam.ajax('post', 'session/travel-preferences', data, function(response){
              setTimeout(function() {
                btn.button('reset');

                eroam.ajax('get', 'session/updatePreferences', '', function(responsedata){
                  $("#changePreferences").html(responsedata);
                });

                $('#preferencesModal').modal('hide');
              }, 3000);
            });
          });
      });

      function tourList(next){
        tour_data = {
          countryName: countryName,
          provider: 'getTours'
        };

        // CACHING HOTEL BEDS
        var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'getTours', true);

        eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
         console.log('tourResponse', tourResponse);
          if( tourResponse.length > 0 ){
            $.each( tourResponse, function( key, value ) {
              //console.log('value : '+ key + value);
              appendTours( value );
            })

          }
        })
      }

      function getStars(count, half = false){
        var stars = '';
        if( parseInt( count ) ){
          for( star = 1; star <= count; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
          }
          var emptyStars = 5 - parseInt( count );
          if(half){
            stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
            emptyStars = emptyStars - 1;
          }
          for( empty = 1; empty <= emptyStars; empty ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }else{
          for( star = 1; star <= 5; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }
         var stars = '';
        return stars;
      }

      function appendTours(tour){
        console.log(tour);
        $("#tours-loader").hide();
        var price = parseFloat(tour.price).toFixed(2);
        var star;
        var rating = tour.rating;

        if(rating % 1 === 0){
          stars = getStars( rating);
        }else {
          stars = getStars( rating, true );
        }

        var imgurl = 'http://www.adventuretravel.com.au'+tour.folder_path+'245x169/'+tour.thumb;
        var imgurl2 = 'http://dev.cms.eroam.com/'+tour.thumb;

        $("#overlay").hide();
          if(parseInt(tour.no_of_days) == 1) {
              var day = 'Day';
          }else{
              var day = 'Days';
          }
        var html = '<div class="col-md-3 col-sm-4 m-t-20"><a href="/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'">'+
                  '<div class="place-wrapper">'+
                    '<div class="place-image">'+
                      '<img src="'+imgurl+'" alt="" class="img-responsive" id="tourImage_'+tour.tour_id+'">'+
                    '</div>'+
                    '<div class="place-inner">'+
                      '<div class="row">'+
                        '<div class="col-xs-6">'+

                            '<p><strong>' + parseInt(tour.no_of_days) + day +'</strong></p>' +
                        '</div>'+
                        '<div class="col-xs-6 text-right">'+
                          '<ul class="rating">'+stars+'</ul>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                  '<div class="place-details">'+
                      '<p> '+tour.tour_title+' - From '+default_currency+' '+price+' Per Person</p>'+
                  '</div>'+
                '</a></div>';
        $('#tourList').append(html);

        imageUrl = checkImageUrl(tour.tour_id, imgurl, imgurl2);
      }

      function checkImageUrl(id, url, url2){
        eroam.ajax('post', 'existsImage', {url : url}, function(response){
          if(response == 200){
            //var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
            //$("#tourImage_"+id).attr('src', image);
          } else if(response == 400){
            //var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
            //$("#tourImage_"+id).attr('src', image);
          } else {
            eroam.ajax('post', 'existsImage', {url : url2}, function(response){
              if(response == 200){
                $("#tourImage_"+id).attr('src', url2);
              } else if(response == 400){
                $("#tourImage_"+id).attr('src', url2);
              } else {
                  var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
                  $("#tourImage_"+id).attr('src', image);
              }
            });  
          }
        });  
      }

      function allowDrop(ev) {
          ev.preventDefault();
      }

      function drag(ev) {
          ev.dataTransfer.setData("text", ev.target.id);
          var data = ev.target.id;

          if($("#"+data).parent().parent().prop('className') == 'drop-list'){
            var interest = data.replace('drag_', 'interest-');
            $(".interestMove").css('opacity','0.5');
            $("#"+interest+".interestMove").css('opacity','');
          }
      }

      function drop(ev) { 
          ev.preventDefault();
          var data = ev.dataTransfer.getData("text");
          var name = data.replace('drag', 'name');
          var inputValue = data.replace('drag', 'inputValue');
          var interest = data.replace('drag_', 'interest-');
          var x = document.getElementById(name);
          var count = 0;
          var text = '';
          var data_value = document.getElementById(data).getAttribute("data-value");
          var data_name = document.getElementById(data).getAttribute("data-name");
          data_value = parseInt(data_value);
          var parentClass = $("#"+data).parent().parent().prop('className');
          var ClassName = $("#"+data).parent().prop('className');

          if(ClassName != "interest-button-active"){
            if($("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
                if (x.style.display === 'none') {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';
                }
                
                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                if( hidden_interest_field.val() ){
                  var interestIndex = activitySequence.indexOf(data_value);
                  activitySequence.splice(interestIndex, 1);
                  hidden_interest_field.val('');
                  hidden_interest_field.attr('name', 'interests[]');
                } else {
                  activitySequence.push(data_value);
                  var interestIndex = activitySequence.indexOf(data_value);
                  hidden_interest_field.val(data_value);
                  hidden_interest_field.attr('name', 'interests['+(interestIndex + 1)+']');
                } 

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));
                var parentDiv = $("#"+data).parent();
                parentDiv.attr('id', interest); 
                parentDiv.attr('data-name', data_name); 

                $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
            }

          } else {
            if(ev.target.getAttribute("class") == 'blankInterest' && $("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
      
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);
                activitySequence.push(data_value);

                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                hidden_interest_field.attr('name', 'interests['+ev.target.getAttribute("data-sequence")+']');  

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));
                $("#"+interest).removeClass('interest-button-active').addClass('blankInterest').removeAttr('data-name');
                $("#"+interest).removeAttr('id');
                $("#"+data).parent().attr('id',interest).attr('data-name',data_name);
                $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
            } else {
              if(parseInt(ev.target.getAttribute("data-value")) == data_value){
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);

                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                hidden_interest_field.val('');
                hidden_interest_field.attr('name', 'interests[]');  
                $(".interestMove").css('opacity','');

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));

                $("#"+interest).removeClass('interest-button-active').addClass('blankInterest');
                $("#"+interest).removeAttr('id');
                x.style.display = 'none';
              } 
            }
          } 
      }
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.home2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>