<?php echo $__env->make( 'layouts.header' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php 
    $i=$j=$k=0;
    $allCountryRegion = array();
    $regionList = '';
    $countryList = '';
    $RegionId = array();
    $allCountry = array();
    $countryDropdown = '';
    if(isset($country_id_remove))
    {
    	$country_id_remove  = $country_id_remove;
    }
    else
    {
    	$country_id_remove  = [];
    }
    
    $cityIds = isset($cityIds) ? $cityIds : [];	

    $availableTourRegions = array();
    if(!empty($tourRegions)){
    	foreach ($tourRegions as $key => $value) {
    		array_push($availableTourRegions, $value['region_id']);
    	}
    }
    $availableTourRegions = array_unique($availableTourRegions);
    $availableTourCounts = array();
    if(!empty($totalTourByRegion)){
    	foreach ($totalTourByRegion as $key => $value) {
    		$availableTourCounts[$value['id']] = $value['tourCount'];
    	}
    }
    
   	if($countryIds){
    	foreach ($countryIds as $key =>$countryId) { 

    		$k++;    		
    		$countryRegion 		= $tourCountries['countryRegions'][$key];
    		$countryName 		= $tourCountries['countryNames'][$key];
    		$countryRegionName  = $tourCountries['countryRegionNames'][$key];
    		$label_partial 		= '';
    		if($k <= 10){
				$check_checked = 'checked';
				if (in_array($countryId, $country_id_remove))
				{
					$label_partial = 'label_partial';
				}
				if(isset($country_tour_data[$countryId]['cities']))
	    		{
	    			$open_c = "open";	    			
	    		}
	    		else
	    		{
	    			$open_c = '';
	    		}
	    		if(isset($country_tour_data[$countryId]['cities']))
	    		{
	    		$countryList .= '<p class="regionCountry_'.$countryRegion.'">
						    		<label class="radio-checkbox label_check '.$label_partial.'" for="checkbox-'.$countryId.'" id="label-'.$countryId.'">
						    			<input class="countryList" data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" data-name="'.$countryName.'"  type="checkbox" id="checkbox-'.$countryId.'" name="region[]" value="'.$countryId.'" >'.$countryName.' 
						    		</label>
					    		[<span class="count'.$countryName.'" id="count'.$countryName.'">'.$tourCount[$countryName].'</span>]';
					    		
					    			$countryList .='<a href="javascript://" class="moreCountry morecity'.$countryId.' '.$open_c.'" data-id="'.$countryId.'"><i class="fa fa-plus"></i></a></p>';
					    		

	    		/*$cities 		= get_cities_by_country_id($countryId);
	    		$cityTourCount 	= cityTourCount($countryId);*/
	    		
		    		$cities 		= $country_tour_data[$countryId]['cities'];
		    		$countryList .= '<div class="moreCountry-block morecountry'.$countryId.'">';
		    		foreach ($cities as $city) {
		    			$check_checked = '';
	    				if (in_array($city['city_id'], $cityIds))
	    				{
	    					$check_checked = "checked";
	    				}

	    				$countryList .= '<p><label class="radio-checkbox label_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$countryId.'" data-country-id="'.$countryId.'" data-name="'.$countryName.'" data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" id="checkbox-'.$city['city_id'].'" value="'.$city['city_id'].'" name="city[]">'.$city['city_name'].'('.$city['city_count'].')</label></p>';	
		    		}
		    		$countryList .= '</div>';
		    	}
					
			} else {
				$countryList .= '<p class="regionCountry_'.$countryRegion.'" style="display:none;">
		    		<label class="radio-checkbox label_check" for="checkbox-'.$countryId.'">
		    			<input class="countryList" checked data-regionName ="'.$countryRegionName.'" data-region="'.$countryRegion.'" data-name="'.$countryName.'"  type="checkbox" id="checkbox-'.$countryId.'" name="region[]" value="'.$countryId.'" >'.$countryName.' 
		    		</label>
	    		[<span class="count'.$countryName.'" id="count'.$countryName.'">'.$country_tour_data[$countryId]['country_count'].'</span>]<a href="javascript://" class="moreCountry morecity'.$countryId.'" data-id="'.$countryId.'"><i class="fa fa-plus"></i></a></p>';

				$cities 		= $country_tour_data[$countryId]['cities'];
				$countryList .= '<div class="moreCountry-block morecountry'.$countryId.'">';

	    		foreach ($cities as $city) {
	    			$check_checked = '';
    				if (in_array($city['city_id'], $cityIds))
    				{
    					$check_checked = "checked";
    				}
    				$countryList .= '<p><label class="radio-checkbox label_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$countryId.'" data-country-id="'.$countryId.'" id="checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]"  data-name="'.$countryName.'"  data-regionName ="'.$countryRegionName.'" '.$check_checked.' data-region="'.$countryRegion.'" value="'.$city['city_id'].'">'.$city['name'].'('.$city['city_count'].')</label></p>';	
	    		}
	    		
	    		$countryList .= '</div>';
			}
    	}
    }
    $region_count = array();//print_r($countries);exit;
    foreach($countries as $country){
    	$i++;
    	$selectRegionId = 0;
    	$totalCount = 0;
    	$allCountry = array();
    	$total_country_region = 0;
      	foreach ($country['countries'] as $country_data){

      		//if( count($country_data['city']) > 0  ){
      		if( count($tourCount[$country_data['name']]) > 0  ){
		      	$j++;
		      	$allCountry[$j]['name'] = $country_data['name'];
		        $allCountry[$j]['id'] = $country_data['id'];
		        $allCountry[$j]['region'] = $country_data['region_id'];
		        $allCountry[$j]['regionName'] = $country['name'];

		        $totalCount += $tourCount[$country_data['name']];

		        if($countryName == $country_data['name']){
	          		$selectRegionId = $country['id'];
		          	$RegionId[] = $country['id'];
		        }
		        if(isset($tourCountries['countryNames']) && !empty($tourCountries['countryNames'])){ 	
			        if(in_array($country_data['name'], $tourCountries['countryNames'])){
			        	$selectRegionId = $country['id'];
			        	$RegionId[] = $country['id'];
			        }
		    	}
      		} 
      	} 

      	$selectRegion = '';
      	if($selectRegionId == $country['id']){
          	$selectRegion = 'checked';	
        }
        if(in_array($country_data['region_id'], $availableTourRegions)){
        	if(array_key_exists($country_data['region_id'], $availableTourCounts)){
        		$total_country_region = $availableTourCounts[$country_data['region_id']];
        	}else{
        		$total_country_region = 0;
        	}
        	
	      	$regionList .= '<p>
						      	<label class="radio-checkbox label_check" for="checkbox-'.$i.'">
							      	<input class="regionList" id="regionList" '.$selectRegion.' type="checkbox" id="checkbox-'.$i.'" value="'.$country['id'].'" name="country[]" >'.$country['name'].' 
						      	</label>
						      	[<span>'.$total_country_region.'</span>]</p>';
		}
    	usort($allCountry, 'sort_by_name');
    	$allCountryRegion[$country['id'].'_'.$country['name']] = $allCountry;
    }
	$RegionId = array_unique($RegionId);
	//dd($country_id_remove);

	foreach ($allCountryRegion as $name => $allcountry) {
		foreach ($allcountry as $country) { 
			if(!in_array($country['id'], $countryIds)){
				if(isset($country_tour_data[$country['id']]))
			    {
			    	$k++;
	        		//if($k <= 10){

	        			$label_partial = '';
	        			$check_checked = 'checked';
	        			$open = '';
						if (in_array($country['id'], $country_id_remove))
						{
							$label_partial = 'label_partial';
							$open 			= "open";
						}
	        			
	        			$cities 		= [];
	        			
	        			if(isset($country_tour_data[$country['id']]))
	        			{
	        				$cities 		= $country_tour_data[$country['id']]['cities'];
	        			}
	        			

			      		$countryList .= '<p style="display:none;" class="regionCountry_'.$country['region'].'">
			        			<label class="radio-checkbox label_check '.$label_partial.'" for="checkbox-'.$country['id'].'" id="label-'.$country['id'].'">
			        				<input class="countryList" data-regionName ="'.$country['regionName'].'" data-region="'.$country['region'].'" data-name="'.$country['name'].'" type="checkbox" id="checkbox-'.$country['id'].'" name="region[]" value="'.$country['id'].'" >'.$country['name'].'
			        			</label>
			        		[<span class="count'.$country['name'].'" id="count'.$country['name'].'">'.$tourCount[$country['name']].'</span>]';
			        		if(!empty($cities))
			        		{
			        			$countryList .=' <a href="javascript://" class="moreCountry morecity'.$country['id'].' '.$open.'" data-id="'.$country['id'].'"><i class="fa fa-plus"></i></a></p>';
			        		}

			        	$countryList 	.= '<div class="moreCountry-block test morecountry'.$country['id'].'">';
			        	
			        	if(!empty($cities))
			        	{
				        	foreach ($cities as $city) {
				    			$check_checked = '';
			    				if (in_array($city['city_id'], $cityIds))
			    				{
			    					$check_checked = "checked";
			    				}			    				

			    				$countryList .= '<p><label class="radio-checkbox label_check" for="checkbox-'.$city['city_id'].'"><input type="checkbox" class="cityList cityList'.$country['id'].'" data-country-id="'.$country['id'].'" id="checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]" data-name="'.$country['name'].'"  data-regionName ="'.$country['regionName'].'" '.$check_checked.' data-region="'.$country['region'].'" value="'.$city['city_id'].'">'.$city['city_name'].'('.$city['city_count'].')</label></p>';
				    		}
				    	}
			        	
			    		$countryList .='</div>';

			    		//echo $countryList;exit;
			        	/*-----------------------------------or------------------------

			        	//dd($cities,$cityTourCount);
			        	$countryList 	.= '<div class="moreCountry-block test morecountry'.$country['id'].'">';
			        	foreach ($cities as $key =>$city) 
			    		{
			    			if(@$cityTourCount[$city['name']] > 0)
	    					{
			    				$countryList .= '<p><label class="radio-checkbox label_check" for="checkbox-07"><input type="checkbox" id="checkbox-07" value="1" >'.$city['name'].'('.$cityTourCount[$city['name']].')</label></p>';			
		   					}
			    		}
			    		$countryList .='</div>';*/
			        //}
		        }
  			}

		}
  	}

  	//echo $countryList; die;
  	
?>

	<section class="page-container">
      	<h1 class="hide"></h1>


		    <div class="page-sidebar-wrapper">
				<div class="page-sidebar top-margin">


		          <div class="location-inner m-t-20">
		            <div class="loc-icon">  
		              <svg width="25px" height="25px" viewBox="0 0 25 25" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
		                  <!-- Generator: Sketch 46.2 (44496) - https://www.bohemiancoding.com/sketch -->
		                  <desc>Created with Sketch.</desc>
		                  <defs></defs>
		                  <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		                      <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-21.000000, -59.000000)" stroke="#212121">
		                          <g id="Group-4" transform="translate(22.000000, 60.000000)">
		                              <circle id="Oval-2" stroke-width="2" cx="7.66666667" cy="7.66666667" r="7.66666667"></circle>
		                              <path d="M13.0333333,13.0333333 L22.3602359,22.3602359" id="Line" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"></path>
		                          </g>
		                      </g>
		                  </g>
		              </svg>
		            </div>

		            <div class="location-content">
		              <h4 class="loc-title">Search Region</h4>
		              <div class="m-t-20 black-box" id="regionBox">
		              	<?php echo $regionList; ?>
		              </div>
		            </div>
		          </div>
		          <hr/>

		         <form method="post" action="tours">
		         	<?php echo e(csrf_field()); ?>

		         <div class="location-inner m-t-20">
		            <div class="loc-icon"></div>
		            <div class="location-content">
		              <h4 class="loc-title">Search Country</h4>
		              <div class="m-t-20 black-box" id="countryBox">
		              	<?php echo $countryList; ?>
		              </div>
		              <p class="m-t-20"><a href="#" data-toggle="modal" data-target="#locationModal" id="openModel">View More Locations</a></p>
		            </div>
		            <input type="hidden" name="countryNames[]" id="country_names">
		            <input type="hidden" name="countryRegion[]" id="region_id">
		            <input type="hidden" name="countryRegionName[]" id="region_name">
		            <input type="hidden" name="removeCountry[]" id="remove_country" value="">
		            <input type="hidden" name="from_page"  value="1">
		            <input type="hidden" name="departure_date" value="<?php echo e(@$dep_date); ?>"> 
		            <input type="hidden" name="tour_type" value="<?php echo e(@$tour_type); ?>"> 
		            <center><input type="submit" class="btn btn-black btn-block" value="Search"></center>
		          </div>
		          </form>
		         <!-- <hr/>

		           <div class="location-inner">
		            <div class="loc-icon">
		              <svg width="28px" height="27px" viewBox="0 0 28 27" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
		                  
		                  <desc>Created with Sketch.</desc>
		                  <defs></defs>
		                  <g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		                      <g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-20.000000, -995.000000)" fill="#212121">
		                          <g id="(Generic)-Icon---Location-Preferences" transform="translate(20.000000, 995.000000)">
		                              <g id="(eRoam)-Icon---Location-Preferences">
		                                  <path d="M7.53597816,15.6938325 C6.41269472,15.6938325 5.49904215,14.7574227 5.49904215,13.6064237 C5.49904215,12.4554248 6.41269472,11.519015 7.53597816,11.519015 C8.65926159,11.519015 9.57337087,12.4554248 9.57337087,13.6064237 C9.57337087,14.7574227 8.65926159,15.6938325 7.53597816,15.6938325 M7.53597816,9.92609431 C5.53146871,9.92609431 3.90054977,11.5770428 3.90054977,13.6064237 C3.90054977,15.6358046 5.53146871,17.2867531 7.53597816,17.2867531 C9.54071596,17.2867531 11.1718633,15.6358046 11.1718633,13.6064237 C11.1718633,11.5770428 9.54071596,9.92609431 7.53597816,9.92609431 M7.53506473,24.6908759 C6.91781832,24.0081956 5.94250961,22.8703952 4.97108295,21.4975251 C2.91405161,18.5904449 1.82684844,15.9841991 1.82684844,13.9605072 C1.82684844,8.2633127 6.19644154,7.82048075 7.5357498,7.82048075 C12.8329252,7.82048075 13.2446512,12.5200518 13.2446512,13.9605072 C13.2446512,17.9671578 9.15410916,22.8967921 7.53506473,24.6908759 M12.6621149,7.80637203 C11.3504377,6.64171947 9.52975487,6 7.5357498,6 C5.54174473,6 3.72106191,6.64171947 2.40938473,7.80637203 C0.833042887,9.20632173 0,11.3342362 0,13.9605072 C0,19.6809129 6.6061123,26.383923 6.8872186,26.6663251 C7.058714,26.8388156 7.29232224,26.9355287 7.5357498,26.9355287 C7.77917735,26.9355287 8.0127856,26.8388156 8.18428099,26.6663251 C8.4653873,26.383923 15.0714996,19.6809129 15.0714996,13.9605072 C15.0714996,11.3342362 14.2384567,9.20632173 12.6621149,7.80637203" id="location"></path>
		                                  <path d="M19.5359782,9.69383246 C18.4126947,9.69383246 17.4990421,8.75742267 17.4990421,7.60642371 C17.4990421,6.45542476 18.4126947,5.51901497 19.5359782,5.51901497 C20.6592616,5.51901497 21.5733709,6.45542476 21.5733709,7.60642371 C21.5733709,8.75742267 20.6592616,9.69383246 19.5359782,9.69383246 M19.5359782,3.92609431 C17.5314687,3.92609431 15.9005498,5.57704279 15.9005498,7.60642371 C15.9005498,9.63580463 17.5314687,11.2867531 19.5359782,11.2867531 C21.540716,11.2867531 23.1718633,9.63580463 23.1718633,7.60642371 C23.1718633,5.57704279 21.540716,3.92609431 19.5359782,3.92609431 M19.5350647,18.6908759 C18.9178183,18.0081956 17.9425096,16.8703952 16.971083,15.4975251 C14.9140516,12.5904449 13.8268484,9.98419914 13.8268484,7.96050722 C13.8268484,2.2633127 18.1964415,1.82048075 19.5357498,1.82048075 C24.8329252,1.82048075 25.2446512,6.52005182 25.2446512,7.96050722 C25.2446512,11.9671578 21.1541092,16.8967921 19.5350647,18.6908759 M24.6621149,1.80637203 C23.3504377,0.641719466 21.5297549,0 19.5357498,0 C17.5417447,0 15.7210619,0.641719466 14.4093847,1.80637203 C12.8330429,3.20632173 12,5.33423617 12,7.96050722 C12,13.6809129 18.6061123,20.383923 18.8872186,20.6663251 C19.058714,20.8388156 19.2923222,20.9355287 19.5357498,20.9355287 C19.7791774,20.9355287 20.0127856,20.8388156 20.184281,20.6663251 C20.4653873,20.383923 27.0714996,13.6809129 27.0714996,7.96050722 C27.0714996,5.33423617 26.2384567,3.20632173 24.6621149,1.80637203" id="location"></path>
		                              </g>
		                          </g>
		                      </g>
		                  </g>
		              </svg>
		            </div>

		            <div class="location-content">
		              <h4 class="loc-title">Tours Preferences</h4>
		              <label>Price Range Per Day</label>
		              <div class="layout-slider">
		                <span style="display: inline-block; width: 100%; padding: 0 5px;">
		                <input id="Slider1" type="slider" name="price" value="10;300" />
		                </span>
		              </div>

		              <label>Number of Total Days</label>
		              <div class="layout-slider">
		                <span style="display: inline-block; width: 100%; padding: 0 5px;">
		                <input id="Slider2" type="slider" name="price1" value="1;500" />
		                </span>
		              </div>
		            </div>
		          </div>
		          <hr/>-->

		          <?php /*<div class="location-inner">
		            <div class="loc-icon">
		              <svg width="20px" height="24px" viewBox="0 0 20 24" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
		                  <!-- Generator: Sketch 43.2 (39069) - https://www.bohemiancoding.com/sketch -->
		                  <desc>Created with Sketch.</desc>
		                  <defs></defs>
		                  <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		                      <g id="45---EROA007-V4.1-Generic-(Create-Itinerary)-01" transform="translate(-22.000000, -780.000000)" fill="#212121">
		                          <g id="(Generic)-Icon---PAX-Preferences" transform="translate(22.000000, 780.000000)">
		                              <g id="(eRoam)-Icon---PAX-Preferences">
		                                  <path d="M13.4646063,13.9881818 C13.6438724,14.1654545 13.7459162,14.4109091 13.7459162,14.6645455 C13.7459162,14.9154545 13.6438724,15.1609091 13.4646063,15.3381818 C13.2853401,15.5154545 13.0343675,15.6163636 12.780637,15.6163636 C12.5269065,15.6163636 12.2786918,15.5154545 12.0994257,15.3381818 C11.9174016,15.1609091 11.8153578,14.9154545 11.8153578,14.6645455 C11.8153578,14.4109091 11.9174016,14.1654545 12.0994257,13.9881818 C12.4579579,13.6336364 13.106074,13.6336364 13.4646063,13.9881818 L13.4646063,13.9881818 Z M5.28841544,6.13636364 C5.28841544,5.60918182 5.72030894,5.18181818 6.25369466,5.18181818 L12.8184208,5.18181818 C13.3515307,5.18181818 13.7837,5.60918182 13.7837,6.13636364 C13.7837,6.66354545 13.3515307,7.09090909 12.8184208,7.09090909 L6.25369466,7.09090909 C5.72030894,7.09090909 5.28841544,6.66354545 5.28841544,6.13636364 L5.28841544,6.13636364 Z M5.28841544,10.2272727 C5.28841544,9.70009091 5.72030894,9.27272727 6.25369466,9.27272727 L12.8184208,9.27272727 C13.3515307,9.27272727 13.7837,9.70009091 13.7837,10.2272727 C13.7837,10.7544545 13.3515307,11.1818182 12.8184208,11.1818182 L6.25369466,11.1818182 C5.72030894,11.1818182 5.28841544,10.7544545 5.28841544,10.2272727 L5.28841544,10.2272727 Z M5.13507394,14.5909091 C5.13507394,14.0637273 5.56724324,13.6363636 6.10035316,13.6363636 L9.3605148,13.6363636 C9.89362472,13.6363636 10.325794,14.0637273 10.325794,14.5909091 C10.325794,15.1180909 9.89362472,15.5454545 9.3605148,15.5454545 L6.10035316,15.5454545 C5.56724324,15.5454545 5.13507394,15.1180909 5.13507394,14.5909091 L5.13507394,14.5909091 Z M2.52385574,21.8181818 L16.8651471,21.8181818 L16.8651471,2.18181818 L2.52385574,2.18181818 L2.52385574,21.8181818 Z M17.9683233,0 L1.42067948,0 C0.811450392,0 0.317503223,0.488454545 0.317503223,1.09090909 L0.317503223,22.9090909 C0.317503223,23.5115455 0.811450392,24 1.42067948,24 L17.9683233,24 C18.5775524,24 19.0714996,23.5115455 19.0714996,22.9090909 L19.0714996,1.09090909 C19.0714996,0.488454545 18.5775524,0 17.9683233,0 L17.9683233,0 Z" id="document"></path>
		                              </g>
		                          </g>
		                      </g>
		                  </g>
		              </svg>
		            </div>
		            <div class="location-content black-box">
		              <h4 class="loc-title">Traveller Details</h4>
		              <div class="m-t-20">
		                <div class="input-field">
		                  <input id="field4" type="text">
		                  <label for="field4">Text Field</label>
		                </div>
		                <div class="input-field">
		                  <input id="field5" type="text">
		                  <label for="field5">Text Field</label>
		                </div>
		              </div>
		              <button type="submit" name="" class="btn btn-primary btn-block">Update Travel Preferences</button>
		            </div>
		          </div>  */ ?>
		        </div>
		      </div>



		    <div class="page-content-wrapper">
				<div class="page-content top-margin">
		      	<div class="left-strip">
		        	<a href="javascript://" class="arrow-btn-new open left_side_arrow_btn"><i class="fa fa-angle-left"></i></a>
		      	</div>
					<div class="tabs-main-container">
				<?php echo $__env->yieldContent( 'content' ); ?>
					</div>
				</div>
			</div>

	</section>

	<!-- Modal -->
    <div class="modal fade" id="locationModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-container">
              <h4 class="modal-title" id="myModalLabel">Select a Country</h4>
            </div>
          </div>
          <div class="modal-body">
            <div class="modal-container">
              <form class="form-horizontal" method="post" action="tours" id="country-model">
				<?php
					$str = '';
					$j=0;
					foreach ($allCountryRegion as $name => $allcountry) {
						$idName = explode('_', $name); 

						$display = 'style="display:none;"';
						//if($idName[0] == $RegionId) { $display = '';}
						if(in_array($idName[0], $RegionId)) { $display = '';}

						$str .= '<div class="m-t-20 modelRegionCountry black-box" id="modelRegionCountry_'.$idName[0].'" '. $display.'>
									<label>'.$idName[1].'</label>
									<hr/>
									<div class="row">
										<div class="col-sm-6">';
						$count = round(count($allcountry)/2);
						$i=0;

						foreach ($allcountry as $country) {
							if(isset($country_tour_data[$country['id']]))
			        		{
								$modelSelect = '';
								$open = '';
								 if(isset($tourCountries['countryNames']) && !empty($tourCountries['countryNames'])){
							        if(!empty($countryIds) && in_array($country['id'], $countryIds)){
							        	$modelSelect = 'checked';
							        	$open = "open";        
							        } else if($countryName == $country['name']){
							          	$modelSelect = 'checked';
							          	$open = "open";
							        } else {
							        	$modelSelect = '';
							        	$open = '';
							        }
						    	}
						    	$label_partial = '';

						    	if (in_array($country['id'], $country_id_remove))
								{
									$label_partial = 'label_partial';
									$open = "open";
								}

			        			
			        			$cities 		= $country_tour_data[$country['id']]['cities'];			        			

						    	//$cityTourCount = cityTourCount($country['id']);
								$str .= '<p class="modelCountryList">
										<label class="radio-checkbox label_check '.$label_partial.'" for="checkbox-model-'.$country['id'].'" id="model-label-'.$country['id'].'">
											<input class="countryListModel" '.$modelSelect.' data-regionName ="'.$idName[1].'" data-region="'.$country['region'].'" data-name="'.$country['name'].'" type="checkbox" id="checkbox-model-'.$country['id'].'" name="region[]" value="'.$country['id'].'" >'.$country['name'].' 
										</label>
									[<span class="count'.$country['name'].'">'.$tourCount[$country['name']].'</span>]';
									
									if(!empty($cities))
			        				{
			        					
										$str .= '<a href="javascript://" class="modelmoreCountry modelmorecity'.$country['id'].' '.$open.'" data-id="'.$country['id'].'"><i class="fa fa-plus"></i></a></p>';
									}
								
					    		$str .= '<div class="moreCountry-block modelmorecountry'.$country['id'].'">';

					    		if(!empty($cities))
					    		{
						    		foreach ($cities as $city) 
						    		{
						    			if($city['city_count'] != 0)
						    			{
						    				$check_checked = '';
						    				if (in_array($city['city_id'], $cityIds))
						    				{
						    					$check_checked = "checked";
						    				}		
						    				$str .= '<p><label class="radio-checkbox label_check" for="model-checkbox-'.$city['city_id'].'"><input type="checkbox" class="modelcityList modelcityList'.$country['id'].'" data-country-id="'.$country['id'].'" id="model-checkbox-'.$city['city_id'].'" city="'.$city['city_id'].'" name="city[]" data-name="'.$country['name'].'"  data-regionName ="'.$country['regionName'].'" '.$check_checked.' data-region="'.$country['region'].'" value="'.$city['city_id'].'">'.$city['city_name'].'('.$city['city_count'].')</label></p>';
						    				//$str .= '<p><label class="radio-checkbox label_check" for="checkbox-model-city-'.$value['id'].'"><input type="checkbox" class="cityList cityList'.$country['id'].'" data-name="'.$value['name'].'" data-country-id="'.$country['id'].'" data-regionName ="'.$idName[1].'" data-region="'.$country['region'].'" id="checkbox-model-city-'.$value['id'].'" value="'.$value['id'].'" name="city[]">'.$value['name'].'('.$cityTourCount[$value['name']].')</label></p>';		
						    			}
						    		}
						    	}
					    		$str .= '</div>';
								$i++;
								$j++;

								if($i%$count == 0) {$str .= '</div><div class="col-sm-6">';}
							}
						}
						$str .= '</div></div></div>';
	              	}
	              	echo $str;
              	?>

	            <div class="m-t-10">
	                <div class="m-t-30 text-right">
		                <?php echo e(csrf_field()); ?>

		                <input type="hidden" name="countryNames[]" id="model_country_names">
			            <input type="hidden" name="countryRegion[]" id="model_region_id">
			            <input type="hidden" name="countryRegionName[]" id="model_region_name">
			            <input type="hidden" name="removeCountry[]" id="model_remove_country">
			            <input type="hidden" name="tour_type" value="<?php echo e(@$tour_type); ?>"> 
		                <a href="#" data-dismiss="modal" class="m-r-10">DECLINE</a>
		                <!-- <a href="javascript://" id="saveCountry">ACCEPT</a> -->
		                <input type="submit" value="ACCEPT" class="btnlink">
	                </div>
	            </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/theme/jquery.slimscroll.js' )); ?>"></script>


    <script type="text/javascript">
    	var countryName   		= '<?php echo e($countryName); ?>';
      	var default_currency   	= '<?php echo e($default_currency); ?>';

      	calculateHeight();
        function calculateHeightOld(){
            var winHeight = $(window).height();

            var oheight = $('.page-sidebar').outerHeight()-10;// -22
            var oheight1 = $('.page-sidebar').outerHeight();
            var elem = $('.page-content .tabs-container').outerHeight();
            var elemHeight = oheight - elem;
            $(".page-content .tour-tabs-container").outerHeight(elemHeight);

            var elemHeight1 = (oheight1 - elem)+20;
            var winelemHeight = winHeight - elem;

            if(winHeight < oheight){
                $(".page-content .tour-tabs-container").outerHeight(elemHeight1);

            } else{
                $(".page-content .tour-tabs-container").outerHeight(winelemHeight);

            }
            

            $(".page-content .hotel-container").outerHeight(elemHeight);
            var tourTop = $('.tour-top-content').outerHeight(); // .page-content
            var tourTabs = $('.tour-tabs').outerHeight();
            var tourHeight = (elemHeight - tourTop) - tourTabs;
            var winTourHeight = (winelemHeight - tourTop) - tourTabs;
            
            if(winHeight < oheight1){
                //$(".tabs-content-container-new").outerHeight(elemHeight);
                $(".tour-bottom-content").outerHeight(tourHeight);
            } else{
                //$(".tabs-content-container-new").outerHeight(winelemHeight);
                $(".tour-bottom-content").outerHeight(winTourHeight);
            }
            //$(".page-content .hotel-bottom-content").outerHeight(hotelHeight);
        }
        function calculateHeight(){ //alert(1);
            var winHeight = $(window).height();
            var oheight = $('.page-sidebar').outerHeight();
            //alert(oheight);
            var elem = $('.page-content .tabs-container').outerHeight();
            //alert(elem);
            var elemHeight = oheight - elem;
            //alert(elemHeight);
            var winelemHeight = winHeight - elem;
            if(winHeight < oheight){
                $(".page-content .tour-tabs-container").outerHeight(elemHeight);

            } else{
                $(".page-content .tour-tabs-container").outerHeight(winelemHeight);

            }

			/*---- tour content ------*/
            var tourTop = $('.page-content .tour-top-content').outerHeight();
            //alert(tourTop);
            var tourTabs = $('.page-content .tour-tabs').outerHeight() + 15;
            //alert(tourTabs);
            var tourHeight = (elemHeight - tourTop) - tourTabs;
            //alert(tourHeight);
            var winTourHeight = (winelemHeight - tourTop) - tourTabs;
            if(winHeight < oheight){
                //$(".tabs-content-container-new").outerHeight(elemHeight);
                //$(".tour-bottom-content").outerHeight(tourHeight);
            } else{
                //$(".tabs-content-container-new").outerHeight(winelemHeight);
                //$(".tour-bottom-content").outerHeight(winTourHeight);
            }
        }

    	$(document).ready(function(){
    		//calculateWidth();
    		//countryTourCount();
          	leftpanel();
            calculateHeight();
            var removeC = [];
            var str = '<?php echo @$country_id_remove_push;?>';
            var res = str.split(",");
            res.forEach(function(element) {
			  removeC.push(element);
			});
            	     
            $("#remove_country").val(removeC);
            $('.moreCountry').click(function(){
            	var country_id = $(this).attr('data-id');
            	var morecountry_class = "morecountry"+country_id;
            	var morecity = "morecity"+country_id;
            	$('.'+morecountry_class).slideToggle();
	            $('.'+morecity).toggleClass('open');
	            calculateHeight();
	        });

            $(".modelmoreCountry").click(function(){
            	var country_id = $(this).attr('data-id');
            	var morecountry_class = "modelmorecountry"+country_id;
            	var morecity = "modelmorecity"+country_id;
            	$('.'+morecountry_class).slideToggle();
	            $('.'+morecity).toggleClass('open');
	            calculateHeight();
	        });


	    	$("#Slider1").slider({ 
	          from: 100, to: 10000, step: 1 , smooth: true, round: 0, dimension: "", skin: "round", limits: false 
	        });

	        $("#Slider2").slider({ 
	          from: 1, to: 100, step: 1 , smooth: true, round: 0, dimension: "&nbsp;Days", skin: "round", limits: false
	        });

	        $('#scroll-box').slimScroll({
	            height: '99%',
	            color: '#212121',
	            opacity: '0.7',
	            size: '5px',
	            allowPageScroll: true
	        });

	        $('.tourListGridBox, .gridTourList').slimScroll({
	          height: '100%',
	          color: '#212121',
	          opacity: '0.7',
	          size: '5px',
	          allowPageScroll: true
	        });

	        $('.tourDetailDiv,.bookScroll,.tourViewItineraryDiv').slimScroll({
                height: '600px',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });

            $('.tourItineraryDiv, .tourmakeAnEnquiry').slimScroll({
                height: '94%',
                color: '#212121',
                opacity: '0.7',
                size: '5px',
                allowPageScroll: true
            });

            $(".countryList").click(function(){
            	var val_id = $(this).val();
            	var country_checkall = "morecountry"+val_id;
            	$("#label-"+val_id).removeClass('label_partial');
            	if (this.checked) 
            	{
		            $("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = true;			            
			        });
			    }
			    else
			    {
			    	$("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = false;			            
			        });
			    }
		    });            

	    	var regionIds = [];
	    	var i = 0;
	    	var countryListCount = 0;
	    	$('.countryList:checkbox:checked').each(function(){
	    		if($(this).parent('p').css('display') != 'none'){
	    			countryListCount++;
	    		}
	    	});

	    	var cityListCount = 0;
	    	$('.cityList:checkbox:checked').each(function(){
	    		if($(this).parent('p').css('display') != 'none'){
	    			cityListCount++;
	    		}
	    	});	    	

	    	//console.log('countryListCount = '+countryListCount);

	      	$('#regionList:checkbox:checked').each(function(){
	          	var id = parseInt($(this).val());
	          	regionIds.push(id);	          

	          	$('.regionCountry_'+id).each(function(){
	            	i++; 
	            	if(i >= countryListCount && i <=10)
	            	{
	            		//console.log(i);
	              		$(this).show();
	            	}
	          });
	      	});

	        $('#regionList').click(function(){
	        	
	        	var regionId = parseInt($(this).val());
	            $("#countryBox").find('p').hide();
	            $(".modelRegionCountry").hide();

	            if (!$(this).is(':checked')) {
	                var id = parseInt($(this).val());

	                $('.regionCountry_'+id+' :input').each(function(){ 
	                    var conId = $(this).val();
	                    if ($(this).is(':checked')) {
	                      $("#checkbox-model-"+conId).attr('checked', false);
	                      $(this).attr('checked', false);
	                    }
	                });
	        	}

		        var i=0;
		        $('#regionList:checkbox:checked').each(function(){
	              var id = parseInt($(this).val());
	              regionIds.push(id);

	              $("#modelRegionCountry_"+id).show();
	              
	              $('.regionCountry_'+id).each(function(){
	                i++; //console.log('onclick = '+i);
	                if(i >= 1 && i <=10){
	                  $(this).show();
	                }
	              });

	            }); 

	            if(i == 0) { $(".modelRegionCountry").show(); }
	        });

	        $('.countryList').click(function(){

	            var id = parseInt($(this).val());
	            if ($(this).is(':checked')) {
	              $("#checkbox-model-"+id).attr('checked', true);   
	            } else{
	              $("#checkbox-model-"+id).attr('checked', false); 
	            }

	            var countryIds = [];
	            var countryNames = [];
	            var countryRegions = [];
	            var countryRegionNames = [];
	            var countryIdRemove = [];
	            var cityID = [];
	            var cityIDCount = [];

	            $('.countryList:checked').each(function(){
	              var id = parseInt($(this).val());
	              var name = $(this).attr('data-name');
	              var region = $(this).attr('data-region');
	              var regionName = $(this).attr('data-regionName');

	              countryIds.push(id);
	              countryNames.push(name);
	              countryRegions.push(region);
	              countryRegionNames.push(regionName);	
	            });	            

	            var  data = { 
	              tourCountryData: [{
	                countryIds:countryIds, 
	                countryNames: countryNames,
	                countryRegions: countryRegions,
	                countryRegionNames: countryRegionNames,
	                regionIds: regionIds,
	                search_type : 'country',
	                cityIds:cityID, 
	                countryIdRemove : countryIdRemove
	              }]
	            };
	            $("#country_names").val(countryNames);
	            $("#region_id").val(countryRegions);
	            $("#region_name").val(countryRegionNames);
	            $("#remove_country").val(countryIdRemove);

            	/*eroam.ajax('post', 'session/set_tourCountry', data, function(response){
	                location.reload();
	            });*/
	          

	        });

			$('.cityList').click(function(){

	            var id = parseInt($(this).val());
	            var country_id_chk = $(this).attr('data-country-id'); 

	            var cityListId = "cityList"+country_id_chk;
	            var numberOfChildCheckBoxes = $('.'+cityListId).length;
	            var countryIdRemove = [];

	            var rCountry = $("#remove_country").val();

	            countryIdRemove.push(rCountry);
              	if(jQuery.inArray( country_id_chk, rCountry ))
              	{
              		countryIdRemove.push(country_id_chk);
              	}
              	
              	$("#label-"+country_id_chk).addClass('label_partial');              	
              	$("#model-label-"+country_id_chk).addClass('label_partial');

				//$('.'+cityListId).change(function() {
				  	var checkedChildCheckBox = $('.'+cityListId+':checked').length;
				  	if (checkedChildCheckBox == numberOfChildCheckBoxes)
				  	{
				  		$("#checkbox-"+country_id_chk).prop('checked', true);
				    	$("#label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#label-"+country_id_chk).removeClass('label_partial');
				    	$("#label-"+country_id_chk).removeClass('c_on');

				    	$("#checkbox-model-"+country_id_chk).prop('checked', true);
				    	$("#model-label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#model-label-"+country_id_chk).removeClass('label_partial');
				    	$("#model-label-"+country_id_chk).removeClass('c_on');

				   	}
				   	else if(checkedChildCheckBox == 0)
				   	{
				   		$("#label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-"+country_id_chk).attr('checked', false); 

				   		$("#model-label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-model-"+country_id_chk).attr('checked', false); 

				   	}
				  	else
				  	{
				    	$("#checkbox-"+country_id_chk).prop('checked', false);
				    	$("#checkbox-model-"+country_id_chk).prop('checked', false);
				    }
				//});

	            if ($(this).is(':checked')) {
	              $("#checkbox-model-city-"+id).attr('checked', true);   
	            } else{
	              $("#checkbox-model-city-"+id).attr('checked', false); 
	            }

	            var cityIds = [];
	            var cityNames = [];
	            var cityRegions = [];
	            var cityRegionNames = [];
	            
	            $('.cityList:checked').each(function(){
	              	var id = parseInt($(this).val());
	              	var name = $(this).attr('data-name');
	              	var region = $(this).attr('data-region');
	              	var regionName = $(this).attr('data-regionName');

	              	cityIds.push(id);
	              	cityNames.push(name);
	              	cityRegions.push(region);
	              	cityRegionNames.push(regionName);	              	
	            });

	            var countryID = [];
	            $('.countryList:checkbox:checked').each(function(){
		              var id = $(this).val();
		              countryID.push(id);
		              totalTourCount = totalTourCount + parseInt($("#count"+id).text());
		        });

	            var  data = { 
	              	tourCityData: [{
		                cityIds:cityIds, 
		                countryIds:countryID, 
		                countryNames: cityNames,
		                countryRegions: cityRegions,
		                countryRegionNames: cityRegionNames,
		                regionIds: regionIds,
		                search_type : 'city',
		                countryIdRemove : countryIdRemove
	              	}]
	            };

	            $("#region_id").val(cityRegions);
	            $("#region_name").val(cityRegionNames);

	            //$(this).parent().find('.accordion-body').length == 1; 
	            $("#remove_country").val(countryIdRemove);

	            /*if (cityIds.length === 0) {
	            	window.location.href = "/";
	            }
	            else
	            {
		            eroam.ajax('post', 'session/set_tourCity', data, function(response){
		                location.reload();
		            });
		        }*/

	        });

			$('.modelcityList').click(function(){

	            var id = parseInt($(this).val());
	            var country_id_chk = $(this).attr('data-country-id'); 

	            var cityListId = "modelcityList"+country_id_chk;
	            var numberOfChildCheckBoxes = $('.'+cityListId).length;
	            var countryIdRemove = [];
	            var rCountry = $("#remove_country").val();

              	if(jQuery.inArray( country_id_chk, rCountry ))
              	{
              		countryIdRemove.push(country_id_chk);
              	}
              	$("#label-"+country_id_chk).addClass('label_partial');
              	$("#model-label-"+country_id_chk).addClass('label_partial');

				//$('.'+cityListId).change(function() {
					
					var checkedChildCheckBox = $('.'+cityListId+':checked').length;

				  	if (checkedChildCheckBox == numberOfChildCheckBoxes)
				  	{
				  		$("#checkbox-model-"+country_id_chk).prop('checked', true);
				    	$("#model-label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#model-label-"+country_id_chk).removeClass('label_partial');
				    	$("#model-label-"+country_id_chk).removeClass('c_on');

				    	$("#checkbox-"+country_id_chk).prop('checked', true);
				    	$("#label-"+country_id_chk).attr("class", "radio-checkbox label_check c_on");
				    	$("#label-"+country_id_chk).removeClass('label_partial');
				    	$("#label-"+country_id_chk).removeClass('c_on');
				   	}
				   	else if(checkedChildCheckBox == 0)
				   	{
				   		$("#model-label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-model-"+country_id_chk).attr('checked', false); 

				   		$("#label-"+country_id_chk).removeClass('label_partial');
				   		$("#checkbox-"+country_id_chk).attr('checked', false); 
				   	}
				  	else
				  	{
				    	$("#checkbox-model-"+country_id_chk).prop('checked', false);
				    	$("#checkbox-"+country_id_chk).prop('checked', false);
				    }
				//});

	            if ($(this).is(':checked')) {
	              $("#checkbox-model-city-"+id).attr('checked', true);   
	            } else{
	              $("#checkbox-model-city-"+id).attr('checked', false); 
	            }

	            var cityIds = [];
	            var cityNames = [];
	            var cityRegions = [];
	            var cityRegionNames = [];
	            
	            $('.cityList:checked').each(function(){
	              	var id = parseInt($(this).val());
	              	var name = $(this).attr('data-name');
	              	var region = $(this).attr('data-region');
	              	var regionName = $(this).attr('data-regionName');

	              	cityIds.push(id);
	              	cityNames.push(name);
	              	cityRegions.push(region);
	              	cityRegionNames.push(regionName);
	              	
	            });

	            var countryID = [];
	            $('.countryList:checkbox:checked').each(function(){
		              var id = $(this).val();
		              countryID.push(id);
		              totalTourCount = totalTourCount + parseInt($("#count"+id).text());
		        });

	            var  data = { 
	              	tourCityData: [{
		                cityIds:cityIds, 
		                countryIds:countryID, 
		                countryNames: cityNames,
		                countryRegions: cityRegions,
		                countryRegionNames: cityRegionNames,
		                regionIds: regionIds,
		                search_type : 'city',
		                countryIdRemove : countryIdRemove
	              	}]
	            };

	            $("#model_region_id").val(cityRegions);
	            $("#model_region_name").val(cityRegionNames);
	            $("#model_remove_country").val(countryIdRemove);

	        });

	        $('.countryListModel').click(function(){
	            var id = parseInt($(this).val());
	            if ($(this).is(':checked')) { 
	              $("#checkbox-model-"+id).attr('checked', true);
	            } else{ 
	              $("#checkbox-model-"+id).attr('checked', false);
	            }

	            var val_id = $(this).val();
            	var country_checkall = "modelmorecountry"+val_id;
            	$("#model-label-"+val_id).removeClass('label_partial');
            	if (this.checked) 
            	{
		            $("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = true;			            
			        });
			    }
			    else
			    {
			    	$("."+country_checkall).find('input[type=checkbox]').each(function () {
			            	this.checked = false;			            
			        });
			    }

			    var countryIds = [];
	            var countryNames = [];
	            var countryRegions = [];
	            var countryRegionNames = [];
	            var countryIdRemove = [];
	            var cityID = [];
	            var cityIDCount = [];

	            $('.countryListModel:checked').each(function(){
	              var id = parseInt($(this).val());
	              var name = $(this).attr('data-name');
	              var region = $(this).attr('data-region');
	              var regionName = $(this).attr('data-regionName');

	              countryIds.push(id);
	              countryNames.push(name);
	              countryRegions.push(region);
	              countryRegionNames.push(regionName);	
	            });	            

	            var  data = { 
	              tourCountryData: [{
	                countryIds:countryIds, 
	                countryNames: countryNames,
	                countryRegions: countryRegions,
	                countryRegionNames: countryRegionNames,
	                regionIds: regionIds,
	                search_type : 'country',
	                cityIds:cityID, 
	                countryIdRemove : countryIdRemove
	              }]
	            };
	            $("#model_country_names").val(countryNames);
	            $("#model_region_id").val(countryRegions);
	            $("#model_region_name").val(countryRegionNames);
	            $("#model_remove_country").val(countryIdRemove);
	        });

	        $('body').on('click', '#saveCountry', function() {

	            $(this).text('LOADING...');

	            $.ajax({
				    type : 'POST',
				    url : 'set_session_tours',
				    data : $('#country-model').serialize(),				    
        			dataType: 'json',
				    success: function(response) {
				    	console.log(response);
				    	if(response.msg == "success")
				    	{
                        	location.reload();
                        }
                    }
				});
	            /*var countryIds = [];
	            var countryNames = [];
	            var countryRegions = [];
	            var countryRegionNames = [];*/



	            /*$('.countryListModel:checked').each(function(){
	              var id = parseInt($(this).val());
	              var name = $(this).attr('data-name');
	              var region = $(this).attr('data-region');
	              var regionName = $(this).attr('data-regionName');

	              countryIds.push(id);
	              countryNames.push(name);
	              countryRegions.push(region);
	              countryRegionNames.push(regionName);
	            });

	            var  data = { 
	              tourCountryData: [{
	                countryIds:countryIds, 
	                countryNames: countryNames,
	                countryRegions: countryRegions,
	                countryRegionNames: countryRegionNames,
	                regionIds: regionIds,
	              }]
	            };

	            eroam.ajax('post', 'session/set_tourCountry', data, function(response){
	              setTimeout(function() {
	                $('#locationModal').modal('hide');
	                location.reload();
	              }, 3000);
	            });*/
	        }); 



			var z=0;
	        $('#regionList:checkbox:checked').each(function(){
                z++; 
            });
            if(z == 0) { $(".modelRegionCountry").show(); }

    	});

		/*function countryTourCount(){
	          var countryNames = [];
	          var countryIds = [];
	          var countryRegions = [];

	          $('.countryList').each(function(){ 
	              var name = $(this).attr('data-name');
	              countryNames.push(name);

	              var id = parseInt($(this).val());
	              countryIds.push(id);

	              var region = $(this).attr('data-region');
	              countryRegions.push(region);
	          });

	          countryRegions = jQuery.unique(countryRegions);
	          //console.log(countryRegions);
	          
	          var vars = {};
	          $.each( countryRegions, function( key1, value1 ) {
	            vars['Region_' + value1] = 0;
	          })

	          var data = {
	            countries: countryNames.join('# '),
	            provider: 'tourCountryCount'
	          };

	          // CACHING Tour Count
	          var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', data, 'tourCountryCount', true);
	          eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
	           //console.log('tourCountResponse', tourResponse);
	            if( tourResponse){
	              var allRegion =[];
	              $.each( tourResponse, function( key, value ) {
	                //$(".count"+key).text(value);

	                regionKey = $("#countryBox").find("[data-name='" + key + "']"). attr('data-region');
	                vars['Region_' + regionKey] += value;
	                $(".count_"+regionKey).text(vars['Region_'+regionKey]);
	              })
	            }
	          })
	    }*/

	    function leftpanelOld(){
	        var w = $( document ).width();
	          if(w > 991){
	            $('.arrow-btn').click(function(){
	              if($('.arrow-btn').hasClass('open')){
	                $('.location-wrapper').css('display', 'none');
	                $(this).removeClass('open');
	                $('.tabs-wrapper').css('margin-left', '0px');
	              } else{
	                $('.location-wrapper').css('display', 'block');
	                $(this).addClass('open');
	                $('.tabs-wrapper').css('margin-left', '350px');
	              }
	           });
	          } else{
	            $('.arrow-btn').click(function(){
	              if($('.arrow-btn').hasClass('open')){
	                $('.location-wrapper').css('display', 'none');
	                $(this).removeClass('open');
	                $('.tabs-wrapper').css('margin-left', '0px');
	                $('.create-strip').removeClass('hideShow-panel'); 
	              } else{
	                $('.location-wrapper').css('display', 'block');
	                $(this).addClass('open');
	                $('.tabs-wrapper').css('margin-left', '0px');
	                $('.create-strip').addClass('hideShow-panel');
	              }
	          });
	        }
	    }

        function leftpanel(){
            var w = $( document ).width();
            if(w > 991){
                $('.arrow-btn-new').click(function(){
                    if($('.arrow-btn-new').hasClass('open')){
                        $('.page-sidebar').css('display', 'none');
                        $(this).removeClass('open');
                        $('.page-content').css('margin-left', '0px');
                    } else{
                        $('.page-sidebar').css('display', 'block');
                        $(this).addClass('open');
                        $('.page-content').css('margin-left', '354px');
                    }
                });
            } else{
                $('.arrow-btn-new').click(function(){
                    if($('.arrow-btn-new').hasClass('open')){
                         $('.page-sidebar').css('display', 'block');
                        $(this).removeClass('open');
                        $('.page-content').css('margin-left', '0px');
                        $('.left-strip').css('left', '300px');
                    } else{
                        $('.page-sidebar').css('display', 'none');
                        $(this).addClass('open');
                        $('.page-content').css('margin-left', '0px');
                        $('.left-strip').css('left', '0px');
                    }
                });
            }
        }

	    function calculateWidth(){
	        var owidth = $('.custom-tabs .nav-tabs').parent().width();
	        $(".tabs-container .custom-tabs .nav-tabs>li").css('padding',0);
	        var elem = $('.tabs-container .custom-tabs .nav-tabs>li').length;
	        var elemWidth = (owidth / elem );
	        //alert(owidth+'//'+elem+'=='+elemWidth);
	        $(".tabs-container .custom-tabs .nav-tabs>li").width(elemWidth);
	    }

	    $(window).resize(function(){
	        calculateWidth();
	        leftpanel();
            calculateHeight();
	    });
    </script>

<?php echo $__env->make( 'layouts.footer' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>