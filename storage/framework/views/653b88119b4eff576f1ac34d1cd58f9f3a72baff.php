<?php $currentPath= Route::getFacadeRoot()->current()->uri();?>
<div class="page-sidebar-wrapper">
    <nav class="pushy pushy-left" style="visibility: visible;">
        <h2 class="profile-title">My Account</h2>
        <div class="pushy-height">
            <ul>
                <li <?php if($currentPath == 'profile/step1'){?> class="active" <?php }?>><a href="<?php echo e(url('profile/step1')); ?>"><i class="icon-account"></i><span>Profile</span></a></li>
                <li <?php if($currentPath == 'profile/step2'){?> class="active" <?php }?>><a href="<?php echo e(url('profile/step2')); ?>"><i class="icon-address"></i><span>Addresses</span></a></li>
                <li <?php if($currentPath == 'profile/step3'){?> class="active" <?php }?>><a href="<?php echo e(url('profile/step3')); ?>"><i class="icon-fingerprint"></i><span>Personal Preferences</span></a></li>
                <li <?php if($currentPath == 'profile/step4'){?> class="active" <?php }?>><a href="<?php echo e(url('profile/step4')); ?>"><i class="icon-email"></i><span>Contact Preferences</span></a></li>
                <li <?php if($currentPath == 'manage/trips'){?> class="active" <?php }?>><a href="<?php echo e(url('manage/trips')); ?>"><i class="icon-manage-trip"></i><span>Manage Trips</span></a></li>
                <li><a href="#"><i class="icon-favorite-trip"></i><span>Saved Trips</span></a></li>
            </ul>
        </div>
    </nav>
</div>