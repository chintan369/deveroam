<?php $__env->startSection( 'custom-css' ); ?>
<style type="text/css">
	
	.activities-content{
		padding-top: 0px;
	}
	.activities-content select{
		width: 100%;
		padding: 11px;
	}
	.no-padding-border-margin{
		padding: 0px;
		border: none;
		margin: 0px;
	}
	/*.activity-list{
		padding: 10px;
		margin-bottom: 10px

	}
	.activity-list button{
		padding: 11px;
	}
	.blue-button{
		background: #2AA9DF;
		padding: 11px;
		width: 100%;
		color: #ffffff;
		border: 1px solid #2AA9DF;
		border-radius: 3px;
	}
	.blue-button:active, .blue-button:hover, .blue-button:focus{
		background:#7AC8EA!important;
		color:white!important;
	}
	.activity-header{
		margin-bottom: 10px;
	}
	.activity-price > small{
		font-size: 60%;
	}
	.selected-activity{
		/*background:  rgba(39, 168, 223, 0.44);
		margin:0;
		border: 2px solid #2AA9DF!important;
		border-radius:3px;
	}
	.activity{
		padding:11px;
		border-radius:3px;		
		border: 1px solid #969797;
		margin:1px;
	}
	.thumb {
		width: 100%;
		height: 200px;
		background-size: contain !important;
		background-position: center !important;
		background-repeat: no-repeat !important;
		/* background-color: #2B2B2B !important; *//*
		background-color: transparent;
	}*/
	.text-field-icon{
		padding: 7px 9px 9px 9px;
		border: solid thin;
		height: 100%;
		width: 45px;
		cursor: pointer;
	}

	.hide{
		display: none;
	}
	.show{
		display: block;
	}
	.modal-content{
		border-radius: 2px;
	}

	.modal-cancel{
		transition: all 0.4s;
		background: #ffffff;
		padding: 11px;
		color: #2AA9DF;
		border: 2px solid #2AA9DF;
		border-radius: 3px;
	}

	.gray-button{
		background-color: #e0e0e0;
		background-position: 0 -15px;
		padding: 11px;
		border: 1px solid rgb(169, 169, 169);
		border-radius: 2px;
	}
	#auto-sort-loader{
		display: none;
	}
	#activity-loader, #auto-sort-loader {
		position: absolute;
		top: 10%;
		width: 100%;
		transform: translateY( -50% );
		text-align: center;
	}

	#activity-loader span, #auto-sort-loader span {	
		padding: 8px 21px;
		font-size: 18px;
		font-family: "HelveticaNeue";
	}
	.btn > .caret{
		position: absolute;
		top: 50%;
		right: 9px;
	}
	 #sort-ad, #search-toggler{
		border-radius: 2px;
		border: 1px solid rgb(169, 169, 169);
		position: relative;
		text-align: left;
		width: 100%;
		padding: 11px;
	}
	.popover{
		border-radius: 15px;
		border: 1px solid #999;
		background-color: #fff;
		/* background-color: rgba(255, 255, 255, 0.44); */
	}
	.popover-title{
		padding: 4px 6px;
		border-radius: 14px 14px 0 0;
		border-bottom: 1px solid #999;
		background-color: #fff;
		/* background-color: rgba(255, 255, 255, 0.7); */
	}
	.popover-title {
		margin: 0px;
		padding-left: 16px;
		padding-top: 6px;
		padding-bottom: 6px;
	}
	.popover-title > strong{
		padding-left: 20px;
	}
	.popover.bottom {
		top: 44px;
		margin-top: 5px;
		left: -20px;
		width: 255px;
	}

	#search.popover.bottom {
	    left: -360px;
	    width: 600px !important;
	    max-width: 600px !important;
	}
	.popover.bottom ul{
		list-style: none;
		padding-left: 0px;
	}
	.icon-style{
		border: 2px solid #918F8F;
		border-radius: 4px;
		padding: 8px 0;
		text-align: center;
		width: 38px;
		font-size: 21px;
		top: -4px;
	}
	.white{
		color: #fff;
	}
	.black-background{
		background: #000000;
	}
	.popover-content {
		padding: 6px;
		/*  background-color: rgba(255, 255, 255, 0.7); */
		border-radius: 0 0 14px 14px;
		background-color: #fff;
	}
	.popover.bottom ul > li {
		margin-bottom: 5px;
		padding-left: 10px;
		cursor: pointer;
	}
	.popover.bottom ul > li > span{
		padding-left: 15px;
	}
	.ui-state-active, .ui-slider-handle {
		background: #2AA9DF !important;
	}
	.range-field{
		border-top:0;
		border-left: 0; 
		border-right: 0;
		font-weight: bold;
		background: transparent;
		border-bottom: 1px solid #999;
	}
	.range-field:focus{
		outline: none;
	}
	.btn-container{
		position: relative;
	}
	.datetimepick{
		position: absolute;
		top: 20px;
		left: -45px;
		z-index: -1;
	}
	.half-width{
		width: 50%;
	}
	.left-radius{
		border-radius: 2px 0 0 2px !important;
	}
	.right-radius{
		border-radius: 0 2px 2px 0 !important;
	}
	
	.default-border{
		border: 1px solid #2AA9DF !important;
	}
	.full-width{
		width: 100%;
	}
	.selected-button{
		background: #2AA9DF;
		padding: 11px;
		color: #fff;
		font-weight: bold;
	}
	.btn-group > .not:hover{
		/*background: #2AA9DF;*/
		padding: 11px;
		color: #fff;
		font-weight: bold;
		border: 1px solid #2AA9DF !important;
	}
	.selected-button.focus{
		outline: none !important;
		color: #fff;
	}

	/* VIEW MORE STYLES */
	#view-more-loader {
		text-align: center;
		font-size: 18px;
		padding-top: 250px;
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		display: none;
		font-family: "HelveticaNeue";
	}
	#act-date{
		margin-top: 10px;
	}

	/*.tabs-content-container{
		height:90%;
	}*/ 
	.activity-wrapper, #top-picks, .hotelList-wrapper, .activityScroll{
		height:100%;
	}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="tabs-container">
    <div class="row">
        <div class="col-sm-7">
            <div class="panel-inner">
                <div class="panel-icon">    
                    <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                        <desc>Created with Sketch.</desc>
                        <defs>
                            <polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>
                        </defs>
                        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-503.000000, -69.000000)">
                                <g id="placeholder" transform="translate(503.000000, 69.000000)">
                                    <g id="Group">
                                        <mask id="mask-2" fill="white">
                                            <use xlink:href="#path-1"></use>
                                        </mask>
                                        <g id="Clip-2"></g>
                                        <path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="panel-container activity-header"> 
                    <h4 id="city-name">
                      <?php echo e($city->name); ?>, <?php echo e($city->country_name); ?><span class="act-count"></span>
                    </h4>
                    <p class="m-t-5">
                      	<svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          	<!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                          	<desc>Created with Sketch.</desc>
                          	<defs></defs>
                          	<g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="51---EROA007-V4.1-Generic-(Edit-Transport)-01" transform="translate(-535.000000, -95.000000)" fill="#212121">
                                  <g id="Page-1" transform="translate(535.000000, 95.000000)">
                                      <path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
                                      <path d="M10.1326667,8.000625 L12,8.000625 L12,6.25 L10.1326667,6.25 L10.1326667,8.000625 Z M10.1326667,10.2503125 L12,10.2503125 L12,8.4996875 L10.1326667,8.4996875 L10.1326667,10.2503125 Z M10.1326667,12.5 L12,12.5 L12,10.749375 L10.1326667,10.749375 L10.1326667,12.5 Z M7.733,12.5 L9.60033333,12.5 L9.60033333,10.749375 L7.733,10.749375 L7.733,12.5 Z M5.33333333,12.5 L7.20066667,12.5 L7.20066667,10.749375 L5.33333333,10.749375 L5.33333333,12.5 Z M2.93366667,12.5 L4.79933333,12.5 L4.79933333,10.749375 L2.93366667,10.749375 L2.93366667,12.5 Z M2.93366667,10.2503125 L4.79933333,10.2503125 L4.79933333,8.4996875 L2.93366667,8.4996875 L2.93366667,10.2503125 Z M2.93366667,8.000625 L4.79933333,8.000625 L4.79933333,6.25 L2.93366667,6.25 L2.93366667,8.000625 Z M5.33333333,8.000625 L7.20066667,8.000625 L7.20066667,6.25 L5.33333333,6.25 L5.33333333,8.000625 Z M5.33333333,10.2503125 L7.20066667,10.2503125 L7.20066667,8.4996875 L5.33333333,8.4996875 L5.33333333,10.2503125 Z M7.733,10.2503125 L9.60033333,10.2503125 L9.60033333,8.4996875 L7.733,8.4996875 L7.733,10.2503125 Z M7.733,8.000625 L9.60033333,8.000625 L9.60033333,6.25 L7.733,6.25 L7.733,8.000625 Z M9.60033333,5.749375 L2.39966667,5.749375 L2.39966667,13.000625 L12.534,13.000625 L12.534,5.749375 L9.60033333,5.749375 Z" id="Fill-3"></path>
                                  </g>
                              </g>
                         	</g>
                      	</svg> 
                      	<?php echo e(convert_date($date_from, 'new_eroam_format')); ?> - <?php echo e(convert_date($date_to, 'new_eroam_format')); ?>

                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-5 m-t-10">
            <div class="input-group search-control activities-content">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
                <input type="text" class="form-control" placeholder="">
            </div>
        </div>
    </div>

	<!--<div class="custom-tabs" data-example-id="togglable-tabs">
        <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
            <li role="presentation" class="active"><a href="#top-picks" id="top-picks-tab" role="tab" data-toggle="tab" aria-controls="top-picks" aria-expanded="true">TOP PICKS</a></li> 

            <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop3" data-toggle="dropdown" aria-controls="myTabDrop3-contents">LOWEST PRICE <span class="caret"></span></a> 
                <ul class="dropdown-menu" aria-labelledby="myTabDrop3" id="myTabDrop3-contents"> 
                  	
                </ul> 
            </li>
                  
            <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop1" data-toggle="dropdown" aria-controls="myTabDrop1-contents">RATING <span class="caret"></span></a> 
                <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents"> 
                   
                </ul> 
            </li>

            <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop2" data-toggle="dropdown" aria-controls="myTabDrop2-contents">TYPE <span class="caret"></span></a> 
                <ul class="dropdown-menu" aria-labelledby="myTabDrop2" id="myTabDrop2-contents"> 
                  	<li><a href="#activities" role="tab" id="activities-tab" data-toggle="tab" aria-controls="activities">Activities</a></li>
                  	<li><a href="#restaurants" role="tab" id="restaurants-tab" data-toggle="tab" aria-controls="restaurants">Restaurants</a></li>
                  	<li><a href="#landmarks" role="tab" id="landmarks-tab" data-toggle="tab" aria-controls="landmarks">Landmarks</a></li>
                  	<li><a href="#events" role="tab" id="events-tab" data-toggle="tab" aria-controls="events">Events</a></li>
                </ul> 
            </li> 

            <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop4" data-toggle="dropdown" aria-controls="myTabDrop4-contents">DISTANCE FROM CITY <span class="caret"></span></a> 
                <ul class="dropdown-menu" aria-labelledby="myTabDrop4" id="myTabDrop4-contents"> 
                    <li><a href="#duration1" role="tab" id="duration1-tab" data-toggle="tab" aria-controls="duration1">Duration</a></li>
                </ul> 
            </li>

          	<!-- <li role="presentation" class="dropdown"> <a href="#" class="dropdown-toggle" id="myTabDrop5" data-toggle="dropdown" aria-controls="myTabDrop5-contents">STOPS <span class="caret"></span></a> 
            	<ul class="dropdown-menu" aria-labelledby="myTabDrop5" id="myTabDrop5-contents"> 
              		<li><a href="#stops1" role="tab" id="stops1-tab" data-toggle="tab" aria-controls="stops1">duration1</a></li>
            	</ul> 
          	</li>
        </ul> 
    </div>-->
</div>

<div class="tabs-content-container"  style="position: relative;">
  <div class="tab-content activity-wrapper" id="myTabContent"> 
    <div class="tab-pane fade in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab"> 
      <div class="row hotelList-wrapper tabs-content-wrapper" id="activities-container">



      	<!-- Calendar Data Start -->

      	<?php 
      		$i=0;
      		$actArr = array();
      		
      		foreach ($selected_dates as $name => $date) {
      			$sdate = explode(' ', $date);
  				$sdate = date('Y',strtotime($sdate[2])).'-'.date('m',strtotime($sdate[1])).'-'.str_replace(array('th','rd','nd','st',','), '', $sdate[0]);
  				$sdate2 = date('j M Y',strtotime($sdate));
  				$sdate = date('Y-m-d',strtotime($sdate));
  				//$actArr[$sdate] = session()->get('search')['itinerary'][$leg]['activities'][$i]['name'];
  				$i++;
  			}
        ?>
        <?php /*<div class="col-sm-6 padding-right-0" >
          <p>Click on a segment below to edit, add or remove activities. Or click ‘Add Personal Event’ to add your own.</p>
          <button type="submit" name="" class="btn btn-secondary btn-block m-t-10 m-b-10 " disabled>ADD PERSONAL EVENT</button>
          <div class="panel-group sortable-list calendarList" id="accordion1" role="tablist" aria-multiselectable="true" style="margin-bottom:0px;">
          	<?php 
          		$i=0;
          		$actArr = array();
          		
          		foreach ($selected_dates as $name => $date) {
          			$sdate = explode(' ', $date);
      				$sdate = date('Y',strtotime($sdate[2])).'-'.date('m',strtotime($sdate[1])).'-'.str_replace(array('th','rd','nd','st',','), '', $sdate[0]);
      				$sdate2 = date('j M Y',strtotime($sdate));
      				$sdate = date('Y-m-d',strtotime($sdate));
      				$actArr[$sdate] = session()->get('search')['itinerary'][$leg]['activities'][$i]['name'];
      				$i++;

      				

          	?>
	            <div class="panel panel-default">
	              <div class="panel-heading" role="tab" id="headingDate{{ $i }}">
	                <div class="panel-title row">
	                  <div class="col-sm-1 col-xs-2 m-t-10">
	                    <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	                        <!-- Generator: Sketch 46.2 (44496) - http://www.bohemiancoding.com/sketch -->
	                        <desc>Created with Sketch.</desc>
	                        <defs></defs>
	                        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	                            <g id="55---EROA007-V4.1-Generic-(Edit-Activities)-01" transform="translate(-503.000000, -329.000000)" fill="#212121">
	                                <g id="Group-34" transform="translate(492.000000, 316.000000)">
	                                    <g id="Page-1" transform="translate(11.000000, 13.000000)">
	                                        <path d="M0.534,14.499375 L14.3996667,14.499375 L14.3996667,4.250625 L0.534,4.250625 L0.534,14.499375 Z M0.534,1.5003125 L2.13266667,1.5003125 L2.13266667,2.2496875 C2.13266667,2.3878125 2.25266667,2.5 2.39966667,2.5 L4.267,2.5 C4.414,2.5 4.534,2.3878125 4.534,2.2496875 L4.534,1.5003125 L10.3996667,1.5003125 L10.3996667,2.2496875 C10.3996667,2.3878125 10.5196667,2.5 10.6666667,2.5 L12.534,2.5 C12.681,2.5 12.7993333,2.3878125 12.7993333,2.2496875 L12.7993333,1.5003125 L14.3996667,1.5003125 L14.3996667,3.75 L0.534,3.75 L0.534,1.5003125 Z M2.66666667,1.999375 L4,1.999375 L4,0.500625 L2.66666667,0.500625 L2.66666667,1.999375 Z M10.9336667,1.999375 L12.267,1.999375 L12.267,0.500625 L10.9336667,0.500625 L10.9336667,1.999375 Z M14.6666667,0.9996875 L12.7993333,0.9996875 L12.7993333,0.2503125 C12.7993333,0.1121875 12.681,0 12.534,0 L10.6666667,0 C10.5196667,0 10.3996667,0.1121875 10.3996667,0.2503125 L10.3996667,0.9996875 L4.534,0.9996875 L4.534,0.2503125 C4.534,0.1121875 4.414,0 4.267,0 L2.39966667,0 C2.25266667,0 2.13266667,0.1121875 2.13266667,0.2503125 L2.13266667,0.9996875 L0.267,0.9996875 C0.119666667,0.9996875 0,1.1121875 0,1.25 L0,14.7496875 C0,14.8878125 0.119666667,15 0.267,15 L14.6666667,15 C14.8136667,15 14.9336667,14.8878125 14.9336667,14.7496875 L14.9336667,1.25 C14.9336667,1.1121875 14.8136667,0.9996875 14.6666667,0.9996875 L14.6666667,0.9996875 Z" id="Fill-1"></path>
	                                        <path d="M9.733,7.25125 L11.6003333,7.25125 L11.6003333,5.500625 L9.733,5.500625 L9.733,7.25125 Z M9.733,9.5009375 L11.6003333,9.5009375 L11.6003333,7.7503125 L9.733,7.7503125 L9.733,9.5009375 Z M9.733,11.750625 L11.6003333,11.750625 L11.6003333,10 L9.733,10 L9.733,11.750625 Z M7.33333333,11.750625 L9.20066667,11.750625 L9.20066667,10 L7.33333333,10 L7.33333333,11.750625 Z M4.93366667,11.750625 L6.801,11.750625 L6.801,10 L4.93366667,10 L4.93366667,11.750625 Z M2.534,11.750625 L4.39966667,11.750625 L4.39966667,10 L2.534,10 L2.534,11.750625 Z M2.534,9.5009375 L4.39966667,9.5009375 L4.39966667,7.7503125 L2.534,7.7503125 L2.534,9.5009375 Z M2.534,7.25125 L4.39966667,7.25125 L4.39966667,5.500625 L2.534,5.500625 L2.534,7.25125 Z M4.93366667,7.25125 L6.801,7.25125 L6.801,5.500625 L4.93366667,5.500625 L4.93366667,7.25125 Z M4.93366667,9.5009375 L6.801,9.5009375 L6.801,7.7503125 L4.93366667,7.7503125 L4.93366667,9.5009375 Z M7.33333333,9.5009375 L9.20066667,9.5009375 L9.20066667,7.7503125 L7.33333333,7.7503125 L7.33333333,9.5009375 Z M7.33333333,7.25125 L9.20066667,7.25125 L9.20066667,5.500625 L7.33333333,5.500625 L7.33333333,7.25125 Z M9.20066667,5 L2,5 L2,12.25125 L12.1343333,12.25125 L12.1343333,5 L9.20066667,5 Z" id="Fill-3"></path>
	                                    </g>
	                                </g>
	                            </g>
	                        </g>
	                    </svg>
	                  </div>
	                  <div class="col-sm-11 col-xs-10">
	                    <div role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseDate{{ $i }}" aria-expanded="true" aria-controls="collapseDate{{ $i }}" class="row collapse-title {{ $i != 1 ? 'collapsed' : '' }}">
	                      <div class="col-sm-8"> {{$sdate2}}</div>
	                      <div class="col-sm-4 right-align">1 Activity <i class="fa fa-minus-circle"></i></div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	              <div id="collapseDate{{ $i }}" class="panel-collapse collapse {{ $i == 1 ? 'in' : '' }}" role="tabpanel" aria-labelledby="headingDate{{ $i }}">
	                <div class="panel-body no-padding">
	                  <div id='calendar{{ $i }}'></div>
	                </div>
	              </div>
	            </div>
            <?php } ?>
          </div>
        </div>*/ ?>
        <!-- Calendar Data End -->

        <!-- Activity Lists Start -->
        <div class="col-sm-12 activityScroll">
        	<div id="activity-loader">
				<span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Activities...</span>
			</div>
			<div id="auto-sort-loader">
				<span><i class="fa fa-circle-o-notch fa-spin"></i> Auto-sorting activities based on your interests...</span>
			</div>


			<div class="activity-list" id="activity-list"></div>
        </div>
        <!-- Activity Lists End -->

      </div>

      	<!-- Activity Details Start -->
        <div id="view-more-details"></div>

		<div id="view-more-loader">
			<i class="fa fa-circle-o-notch fa-spin"></i> Loading Activity Data ...
		</div>
        <!-- Activity Details End -->
    </div>

    
    <div class="tab-pane fade" role="tabpanel" id="activities" aria-labelledby="activities-tab"> 
      	<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p> 
    </div> 
    <div class="tab-pane fade" role="tabpanel" id="restaurants" aria-labelledby="restaurants-tab"> 
      	<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p> 
    </div> 
    <div class="tab-pane fade" role="tabpanel" id="landmarks" aria-labelledby="landmarks-tab"> 
      	<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p> 
    </div> 
    <div class="tab-pane fade" role="tabpanel" id="events" aria-labelledby="events-tab"> 
      	<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p> 
    </div>
    
    <div class="tab-pane fade" role="tabpanel" id="duration1" aria-labelledby="duration1-tab"> 
      	<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p> 
    </div> 

  </div>
</div>

<input type="hidden" id="selected-activities" value="<?php echo e($selected_activities); ?>"> 
<input type="hidden" class="the-city-id" value="<?php echo e(json_encode( get_city_by_id( $city_id ) )); ?>">

<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>

  <?PHP /*<link href="{{ url( 'assets/css/fullcalendar.css' ) }}" rel="stylesheet">
<link href="{{ url( 'assets/css/fullcalendar.print.min.css' ) }}" rel="stylesheet" media='print'>
<script type="text/javascript" src="{{ url( 'assets/js/theme/fullcalendar.js' ) }}"></script>


<link href="http://localhost:8000/assets/css/datepicker.css" rel="stylesheet">
<script src="http://localhost:8000/assets/js/theme/bootstrap-datepicker.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> */ ?>

<link rel="stylesheet" type="text/css" href="<?php echo e(url( 'assets/datetimepicker/jquery.datetimepicker.css' )); ?>">
<script src="<?php echo e(url( 'assets/datetimepicker/build/jquery.datetimepicker.full.js' )); ?>"></script>


<?php /*<link rel="stylesheet" href="{{ url( 'assets/css/jquery-ui.css') }}"> */?>
<script>
/*
| Moved some variables
*/

var index, cityId, dateFrom,selectedActivities,destinationId, totalActivityCount=0;

var search_session = JSON.parse( $('#search-session').val() );
var dates = ['<?php echo session()->has('dates_available_'.$city_id) ? join("', '", session()->get('dates_available_'.$city_id) ) : '0000/00/00'; ?>'];
var cityName = "<?php echo e($city->name.', '.$city->country_name); ?>";
var cityId = <?php echo e($city_id); ?>;
var	dateFrom  = formatDate("<?php echo e($date_from); ?>"); 
var	dateTo = formatDate("<?php echo e($date_to); ?>");
var  postData;
var	destinationId = <?php echo e($destinationId); ?> ;
var selected_dates = { <?php echo join_array_key_value( $selected_dates ); ?> };
var maxPrice = 0;
var travellers = parseFloat(search_session.travellers);
var travel_pref = [ <?php echo e(join(', ', $activity_pref)); ?> ];
var available_to_book = <?php echo e($available_to_book ? 1 : 0); ?>;
var departureDate = "<?php echo e($departure_date); ?>";
var cityData = JSON.parse($('.the-city-id').val());
var leg = <?php echo e($leg); ?>;

console.log('IT', search_session);

$( document ).ready( function(){

	//----------- Calender Start -----------//
		<?php 
      		/*$i=0;
      		foreach ($actArr as $date => $name) {
      			$i++;	
      			if($i != 1){ 	
      	?>
      	$('#collapseDate<?php echo $i;?>').on('shown.bs.collapse', function () {
			$('#calendar<?php echo $i;?>').fullCalendar({
		      defaultDate: '<?php echo $date; ?>',
		      defaultView: 'agendaDay',
		      height: 500, //940,
		      allDayText: '',
		      navLinks: false, // can click day/week names to navigate views
		      selectable: true,
		      selectHelper: true,
		      select: function(start, end) {
		        var title = prompt('Activity  Title:');
		        var eventData;
		        if (title) {
		          eventData = {
		            title: title,
		            start: start,
		            end: end
		          };
		          $('#calendar1').fullCalendar('renderEvent', eventData, true); // stick? = true
		        }
		        $('#calendar1').fullCalendar('unselect');
		      },
		      editable: true,
		      eventLimit: true, // allow "more" link when too many events
		      slotDuration: '01:00:00',
		      events: [
		        {
		          //id: '<?php echo $i; ?>',
		          title: "<?php echo $name; ?>",
		          start: '<?php echo $date; ?>T07:00:00',
		          end: '<?php echo $date; ?>T17:00:00'
		        }
		      ],
		      labelFormat: 'H(:mm)'
		    });
		})
		<?php } else { ?>
			$('#calendar<?php echo $i;?>').fullCalendar({
		      defaultDate: '<?php echo $date; ?>',
		      defaultView: 'agendaDay',
		      height: 500, //940,
		      allDayText: '',
		      navLinks: false, // can click day/week names to navigate views
		      selectable: true,
		      selectHelper: true,
		      select: function(start, end) {
		        var title = prompt('Activity  Title:');
		        var eventData;
		        if (title) {
		          eventData = {
		            title: title,
		            start: start,
		            end: end
		          };
		          $('#calendar1').fullCalendar('renderEvent', eventData, true); // stick? = true
		        }
		        $('#calendar1').fullCalendar('unselect');
		      },
		      editable: true,
		      eventLimit: true, // allow "more" link when too many events
		      slotDuration: '01:00:00', 
		      events: [
		        {
		          //id: '<?php echo $i; ?>',
		          title: "<?php echo $name; ?>",
		          start: '<?php echo $date; ?>T07:00:00',
		          end: '<?php echo $date; ?>T17:00:00'
		        }
		      ]
		    });
		<?php } } */?>
	//----------- Calender End -----------//

	selectedActivities = $('#selected-activities').val() != 0 ? JSON.parse($('#selected-activities').val()) : 0;

	buildActivities();
	$('.filter-buttons').click(function(){
		var id = $(this).attr('id');
		switch(id){
			case 'sort-ad':
				$('#number_of_nights_list').removeClass('show');
				$('#search').removeClass('show');
				$('#sort').toggleClass('show');
			break;
			case 'search-toggler':
				$('#number_of_nights_list').removeClass('show');
				$('#sort').removeClass('show');
				$('#search').toggleClass('show');
				$('#search-field').focus();
			break;
			
		}
	});

	$('body').click(function(e){
		var target = e.target;
		if ( $(target).hasClass('filter-buttons') === false && $('.popover').has(e.target).length === 0 ) {
			$('.filter-buttons').removeClass('show');
			$('.popover.bottom').removeClass('show');
		}
	});

	var changeRangeField = function(){
		
		var id = $(this).attr('id');

		var min = parseInt( eroam.convertCurrency($('#price-from').val(), globalCurrency) ) ? parseInt( eroam.convertCurrency($('#price-from').val(), globalCurrency) ) :  parseInt( eroam.convertCurrency(0, 'AUD') );
		var max = parseInt( eroam.convertCurrency($('#price-to').val(), globalCurrency) ) ? parseInt( eroam.convertCurrency($('#price-to').val(), globalCurrency) ) :  parseInt( eroam.convertCurrency(maxPrice, 'AUD') );
		var value = parseInt( eroam.convertCurrency($(this).val(), globalCurrency) );

		var sliderMin = parseInt( eroam.convertCurrency(0, 'AUD') );
		//var sliderMax = parseInt( eroam.convertCurrency(50000, 'AUD') );
		var sliderMax = parseInt( maxPrice );

		if( !isNaN(value) ){

			if (id == 'price-from') {
				if(value <= max){
					$( '#slider-range' ).slider({
						range: true,
					 	min: sliderMin,
						max:  sliderMax,
					    values: [ value, max ]
				    });
				}
			}else{
				if(value >= min){
					$( '#slider-range' ).slider({
						range:true,
						min: sliderMin,
						max: sliderMax,
						values: [ min, value ]
					});
				}
			}
		}
	}
	$('.btn-group').find('label').click(function(){
		if( $(this).hasClass('not') ){
			//alert($(this).text());
			$('.selected-button').removeClass('selected-button default-border').addClass('not gray-button');
			$(this).removeClass('not gray-button').addClass('selected-button default-border');
		}
	});
	
	$('.range-field').bind('keyup', changeRangeField);
	var validateRange = function(){
		var from = parseInt($('#price-from').val());
		var to = parseInt($('#price-to').val());
		var value = $(this).val();
		var id = $(this).attr('id');
		if( !isNaN(value) ){
			if (id == 'price-from') {
				if(from > to){
					$('#error_range').text('Invalid Price Range').fadeIn('slow');
					$('#price-from').val(0);
					$( '#slider-range' ).slider({
						range: true,
						min: 0,
						max:  maxPrice,
						values: [ 0, to ]
					});
					setTimeout(function(){ 
						$('#error_range').fadeOut('slow');
					}, 3000);
				}
			}else{
				if(to < from){
					$('#error_range').text('Invalid Price Range').fadeIn('slow');
					$('#price-to').val(maxPrice);
					$( '#slider-range' ).slider({
						range: true,
						min: 0,
						max:  maxPrice,
						values: [ from, maxPrice ]
					});
					setTimeout(function(){ 
						$('#error_range').fadeOut('slow');
					 }, 3000);
				}
			}
		}else{
			if (id == 'price-from') {
				$('#error_range').text('Invalid Price').fadeIn('slow');
				$('#price-from').val(0);
				$( '#slider-range' ).slider({
					range: true,
					min: 0,
					max:  maxPrice,
					values: [ 0, to ]
				});
				setTimeout(function(){ 
					$('#error_range').fadeOut('slow');
				}, 3000);
			}else{
				$('#error_range').text('Invalid Price').fadeIn('slow');
				$('#price-to').val(maxPrice);
				$( '#slider-range' ).slider({
					range: true,
					min: 0,
					max:  maxPrice,
					values: [ from, maxPrice ]
				});
				setTimeout(function(){ 
					$('#error_range').fadeOut('slow');
				 }, 3000);
				
			}
		}
	}
	$('.range-field').focusout(validateRange);

	$('.sort-asc-desc').click(function(){
		$('.sort-asc-desc').find('i').removeClass('white black-background');
		$(this).find('i').toggleClass('white black-background');
		var asc_desc = $(this).find('i').attr('data-sort');
		var asc = asc_desc == 'asc' ? true : false;
		activitySort(asc);
	});

	var arrowSelect = function(event) {
			var numOfSuggestions = $('.suggestions').length-1;
			var selected = -1;
			var items = $('.suggestions');
			var current = $('.selected-suggestion').index();
		
			if(event.which == $.ui.keyCode.UP) {
				if(current == -1){
					$(items[numOfSuggestions]).addClass('selected-suggestion');
					var value = $(items[numOfSuggestions]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}else{
					var next = current - 1;
					next = next < 0 ? numOfSuggestions : next;
					$(items[current]).removeClass('selected-suggestion');
					$(items[next]).addClass('selected-suggestion');
					var value = $(items[next]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}
			}
			else if(event.which == $.ui.keyCode.DOWN){
				if(current == -1){
					$(items[0]).addClass('selected-suggestion');
					var value = $(items[0]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}else{
					var next = current + 1;
					next = next > numOfSuggestions ? 0 : next;
					$(items[current]).removeClass('selected-suggestion');
					$(items[next]).addClass('selected-suggestion');
					var value = $(items[next]).text();
					if(value != '' && value != null){
						$('.search-field').val(value);
					}
				}
			}
			else{
				$('#suggestion-container').find('ul').html('');
				var priceFrom  = parseFloat($('#price-from').val());
				var priceTo = parseFloat($('#price-to').val());
				var pattern = new RegExp($('.search-field').val().toString(), 'gi');
				$('.activity-wrapper').filter(function() {

					var list = '<li class="suggestions">'+$(this).find('h4').text()+'</li>'; 
					var actPrice = parseFloat($(this).find('.act-select-btn').attr('data-price'));
					
					if( actPrice >= priceFrom && actPrice <= priceTo ){
						if($(this).find('h4').text().match(pattern) != null){
							$('#suggestion-container').find('ul').append(list);
						}
					}else{
						if($(this).find('h4').text().match(pattern) != null){
							$('#suggestion-container').find('ul').append(list);
						}
					}
				});
			}
		}

		/*
		| Added by junfel 
		| function for showing suggestion on keydown and selecting suggestion through arrow down and arrow up
		*/
		$('.search-field').bind('keydown', arrowSelect);
		/*
		| Added by Junfel
		| function for clearing search and display all activities
		*/
		$('#clear-search').click(function(){
			var min = parseInt(eroam.convertCurrency(0, 'AUD'));
			//var max = parseInt(eroam.convertCurrency(50000, 'AUD'));
			var max =  parseInt(maxPrice);
			$( '#slider-range' ).slider({
				range: true,
				min: min,
				max: max,
				values: [ min, max ]
			});
			$('#price-from').val(min);
			$('#price-to').val(max);

			$('.activity-wrapper').show();
			$('.search-field').val('');
			$('#search-form').submit();
		});
		/*
		| Added by Junfel
		| function for selecting suggestion through mouse click
		*/
		$('body').on('click', '.suggestions', function(){
			var value = $(this).text();
			$('.search-field').val(value);
			$('#search-form').submit();
		});
		/*
		| Added by Junfel 
		| Function for searching activities in the selected City
		*/
		$('#search-form').submit(function(){
			/*
			| search pattern
			| check all possible matches and case insensitive
			*/
			var priceFrom  = parseFloat($('#price-from').val());
			var priceTo = parseFloat($('#price-to').val());
	
			$('.activity-wrapper').show();
			$('#suggestion-container').find('ul').html('');
			var pattern = new RegExp($('.search-field').val().toString(), 'gi');
			$('.activity-wrapper').filter(function() {
				//if (price_range) {
				var actPrice = parseFloat($(this).find('.act-select-btn').attr('data-price'));
				if( actPrice >= priceFrom && actPrice <= priceTo ){
				
					if($(this).find('h4').text().match(pattern) == null){
						$(this).hide();
					}else{
						$(this).show();
					}
				}else{
					$(this).hide();
				}
				
			});
			return false;
		});


	$('body').on('click', '.view-more-btn', showActivityDetails);
	$('body').on('click', '.act-btn-less', function() {
		var provider = $(this).data('provider');
		var code = $(this).data('code');
		$('#view-more-details').html('').hide();
		$('#activities-container').show();
		var activity = $('#activities-container').find('.view-more-btn[data-provider="'+provider+'"][data-code="'+code+'"]').offset().top;
		$('.content-body').scrollTop(activity - 250);
	});
}); // END OF DOCUMENT READY METHOD

function showActivityDetails(event) {
	event.preventDefault();
	var provider = $(this).data('provider');
	var code = $(this).data('code');
	var select = $(this).data('select');
	var cityid = $(this).data('cityid');

	eroam.ajax('get', 'activity/view-more', {provider: provider, code: code, select : select, cityid : cityid, leg :leg}, function(response) {
		$('#view-more-details').html(response).show();

		$(function() {
			calculateHeight();
			$('.activityDesc').slimScroll({
		      height: '100%',
		      color: '#212121',
		      opacity: '0.7',
		      size: '5px',
		      allowPageScroll: true
		    });

	        var jcarousel = $('.jcarousel'); 

	        jcarousel
	            .on('jcarousel:reload jcarousel:create', function () {
	                var carousel = $(this),
	                width = carousel.innerWidth();

	                if (width >= 1280) {
	                    width = width / 5;
	                } else if (width >= 992) {
	                    width = width / 4;
	                }
	                else if (width >= 768) {
	                    width = width / 4
	                }
	                else if (width >= 640) {
	                    width = width / 4;
	                } else {
	                    width = width / 3;
	                }

	                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
	            })
	            .jcarousel({
	                wrap: 'circular'
	            });

	        $('.jcarousel-control-prev')
	            .jcarouselControl({
	                target: '-=1'
	            });

	        $('.jcarousel-control-next')
	            .jcarouselControl({
	                target: '+=1'
	            });

	        $('.jcarousel-pagination')
	            .on('jcarouselpagination:active', 'a', function() {
	                $(this).addClass('active');
	            })
	            .on('jcarouselpagination:inactive', 'a', function() {
	                $(this).removeClass('active');
	            })
	            .on('click', function(e) {
	                e.preventDefault();
	            })
	            .jcarouselPagination({
	                perPage: 1,
	                item: function(page) {
	                    return '<a href="#' + page + '">' + page + '</a>';
	                }
	            });
	    });
	}, function() {
		$('#activities-container').hide();
		$('#view-more-loader').show();
	}, function() {
		$('#view-more-loader').hide();
	});
}

function activitySort(asc = true){
	var activityList = $('.activity-wrapper');
	var sortBy = $('input[name="sort"]:checked').val();
	var OrderedActivities = activityList.sort(function (itemA, itemB) {
		if(sortBy == "alpha"){
			var A = $(itemA).find('h4.activity-title').text();
			var B = $(itemB).find('h4.activity-title').text();
			if(asc){
				return A.localeCompare(B);
			}else{
				return B.localeCompare(A);
			}
		}else{
			var A = parseInt( $(itemA).find('h3.activity-price').attr('data-price') );
			var B = parseInt( $(itemB).find('h3.activity-price').attr('data-price') );
			if(asc){
				return A - B;
			}else{
				return B - A;
			}
		}
	});
	$('#activity-list').html(OrderedActivities);
}

// function to queue the calls to APIs e.g. eroam, viator
function buildActivities()
{
	var tasks = [
		buildEroamActivity, // call eroam 
		buildViatorActivity, // call viator next
		// buildArabianExplorerActivity,
		checkActivityCount, // show the available transport types
		hidePageLoader, // hides the page loader when all API calls are done
		rangeSlider,
		<?php if( count($activity_pref) > 0 ): ?>
		autoSortActivityByPref,
		hideAutoSortLoader
		<?php endif; ?>
	];

	$.each(tasks, function(index, value) {
		$(document).queue('tasks', processTask(value));
	});
	// queue
	$(document).queue('tasks');

	$(document).dequeue('tasks');

}


function processTask(fn){
    return function(next){
        doTask(fn, next);
    }
}

function doTask(fn, next){
    fn(next);
}

function buildEroamActivity( next ){

	eroamRQ = {
		city_ids: [ cityId ],
		date_from: dateFrom,
		date_to: dateTo,
		interests: travel_pref
	};
	var eroamApiCall = eroam.apiDeferred('city/activity', 'POST', eroamRQ, 'activity', true);

	eroam.apiPromiseHandler( eroamApiCall, function( eroamResponse ){
		console.log('eroamResponse :  ', eroamResponse);

		if( eroamResponse.length > 0 )
		{
			eroamResponse.forEach(function( eroamAct, eroamActIndex ){
				
				var duration = parseInt(eroamAct.duration);
				if(duration > 1){
					return;
				}
				eroamAct.provider = 'eroam';
				eroamAct.index = eroamActIndex;
				if( selectedActivities.length > 0 ){
					if(selectedActivities.indexOf('eroam_'+eroamAct.id) > -1){
						eroamAct.selected = true;
						eroamAct.activity_date = selected_dates['eroam_'+eroamAct.id];
					}
				}
				try {
				    appendActivities( eroamAct );
				    totalActivityCount++;
				}
				catch(err) {
				    console.log(err.message);
				}
				
			});
		}

		next();
	});

}

function buildViatorActivity( next ){

	vRQ = {
		destId: destinationId,
		startDate: dateFrom,
		endDate: dateTo,
		currencyCode: 'AUD',
		sortOrder: 'TOP_SELLERS',
		topX: '1-15',
		provider:'viator'
	};

	var viatorApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', vRQ, 'viator_activity', true);
	eroam.apiPromiseHandler( viatorApiCall, function( viatorResponse ){
	
		if( viatorResponse != null )
		{
			if( viatorResponse.length > 0 )
			{
				console.log('viatorResponse', viatorResponse);
				viatorResponse.forEach(function( viatorAct, viatorActIndex ){
					
					viatorAct.provider = 'viator';

					var duration = countViatorActDuration( getViatorDuration(viatorAct.duration) );
					viatorAct.duration = duration;
					//if(viatorAct.duration.includes('days')){
						//return;
					//}
					if( selectedActivities.length > 0 ){
						if(selectedActivities.indexOf('viator_'+viatorAct.code) > -1){
							viatorAct.selected = true;
							viatorAct.activity_date = selected_dates['viator_'+viatorAct.code];
						}
					}
					
					try {
					   	appendActivities( viatorAct );
					   	totalActivityCount++;
					}
					catch(err) {
					    console.log(err.message);
					}
					
				});
			}
		}
		next();
	});
}


function buildArabianExplorerActivity( next )
{
	var aeResponseLimit   = 20;
	var aeResponseCounter = 0;
	var aeApiCallArray    = [];
	var aeCityMapped      = cityData.ae_city_mapped;
	var ae_city           = aeCityMapped ? aeCityMapped.ae_city : null;
	var locationIds       = [];
	console.log("ae_city", ae_city);
	if( ae_city )
 	{
 		ae_region_length = ( ae_city.ae_region ) ? ae_city.ae_region.length : 0;
 		console.log("ae_city.ae_region", ae_city.ae_region);
 		if( ae_region_length > 0 )
 		{
 			console.log("ae_region_length", ae_region_length);
 			var getExcursionLocationRQ = {};
 			// each ae city may contain have multiple LocationIds; loop through each;
 			for(var ctr = 0; ctr < ae_region_length; ctr++ )
	 		{
	 			// console.log( 'LocationId', ae_city.ae_region[ ctr ].LocationId );
				getExcursionLocationRQ = { 
					'FromDate' : dateFrom,
					'ToDate' : dateTo,
					'LocationId' : ae_city.ae_region[ ctr ].LocationId
				};
				locationIds.push( ae_city.ae_region[ ctr ].LocationId );
				console.log("getExcursionLocationRQ", getExcursionLocationRQ);
				aeApiCallArray.push( eroam.apiDeferred('api-ae/get/excursion-location', 'POST', getExcursionLocationRQ, 'ae', true) );
	 		}
 		}
 	}

 	if( aeApiCallArray.length )
 	{
	 	eroam.apiArrayOfPromisesHandler( aeApiCallArray, function( response ){
			
			// START ARABIAN EXPLORERS API RESPONSE
			var excur_availability_rq = [];
			var excursion_ids         = [];
			response.forEach( function( getExcursionLocationRS ){
				console.log("getExcursionSearchLocations response:", getExcursionLocationRS);
				if( getExcursionLocationRS )
				{
					var excursionLocations = [];
					if( Array.isArray( getExcursionLocationRS ) )
					{
						excursionLocations = getExcursionLocationRS;
					}
					else
					{
						excursionLocations.push( getExcursionLocationRS );
					}
					var multi_location_ids = []; // variable to hold the location ids to pass to get the activities
					// loop to check if there are activities in each location
					excursionLocations.forEach( function( excursionLocation ){
						locationIds.forEach(function( locationId ){
							if( excursionLocation.LocationId == locationId )
							{
								multi_location_ids.push( excursionLocation.LocationId );
							}
						});
					});

					var excursionAvailabilityRQ = {
						'FromDate' : dateFrom,
						'ToDate' : dateTo, 
						'MultiLocationIds' : multi_location_ids
					};
					excur_availability_rq.push( eroam.apiDeferred('api-ae/get/excursion-availability', 'POST', excursionAvailabilityRQ, 'ae', true) );
				}
			});


			if( excur_availability_rq.length > 0 )
			{
				eroam.apiArrayOfPromisesHandler( excur_availability_rq, function( excur_availability_rs ){
					console.log("getExcursionAvailabilitySearch response:", excur_availability_rs);
					var excursionPriceRQ = {};
					var excur_price_rq   = [];
					var excursions       = [];
					if( excur_availability_rs )
					{
						excur_availability_rs.forEach( function( excursionAvailabilities ){
							if( excursionAvailabilities )
							{
								
								var excLength = excursionAvailabilities.length;
								for( var excCtr = 0; excCtr < excLength; excCtr++ )
								{
									var excursionAvailability = excursionAvailabilities[ excCtr ];
									// check if excursion already exists in excursion_ids array
									if( excursion_ids.indexOf( excursionAvailability.ExcursionId ) == -1 || aeResponseCounter < aeResponseLimit )
									{
										excursionAvailability.Prices   = [];
										excursionAvailability.Requests = [];
										// filter values
										if( (excursionAvailability.OnRequest == "false" || excursionAvailability.OnRequest === false) &&
											(excursionAvailability.IsAdultsAllowed == "true" || excursionAvailability.IsAdultsAllowed === true) &&
											// (excursionAvailability.IsChildrenAllowed == "true" || excursionAvailability.IsChildrenAllowed === true) && 
											(excursionAvailability.IsInfantsAllowed == "false" || excursionAvailability.IsInfantsAllowed === false) && 
											(excursionAvailability.IsSeniorsAllowed == "false" || excursionAvailability.IsSeniorsAllowed === false)
											)
										{
											// check if excursionAvailability.ExcursionDates has value and store in array
											if(! $.isEmptyObject( excursionAvailability.ExcursionDates ) )
											{
												var excursionDates = [];
												if( Array.isArray( excursionAvailability.ExcursionDates.ExcursionDate ) )
												{
													excursionDates = excursionDates.concat( excursionAvailability.ExcursionDates.ExcursionDate );
												}
												else
												{
													excursionDates.push( excursionAvailability.ExcursionDates.ExcursionDate );	
												}
												excursionDates.forEach( function( excursionDate ){
													// check if excursionAvailability.ExcursionStartTimes has value and store in array
													if(! $.isEmptyObject( excursionAvailability.ExcursionStartTimes ) )
													{
														var excursionStartTimes = [];
														if( Array.isArray( excursionAvailability.ExcursionStartTimes.ExcursionStartTime ) )
														{
															excursionStartTimes = excursionStartTimes.concat( excursionAvailability.ExcursionStartTimes.ExcursionStartTime );
														}
														else
														{
															excursionStartTimes.push( excursionAvailability.ExcursionStartTimes.ExcursionStartTime );
														}
														excursionStartTimes.forEach( function( excursionStartTime ){
															// check if excursionAvailability.Locations has value and store in array
															if(! $.isEmptyObject( excursionAvailability.Locations ) )
															{
																var excursionLocations = [];
																if( Array.isArray( excursionAvailability.Locations.ExcursionSearchLocation ) )
																{
																	excursionLocations = excursionLocations.concat( excursionAvailability.Locations.ExcursionSearchLocation );
																}
																else
																{
																	excursionLocations.push( excursionAvailability.Locations.ExcursionSearchLocation );	
																}
																excursionLocations.forEach( function( excursionLocation ){
																	// create request for getting price 
																	excursionPriceRQ = {
																		ExcursionId : excursionAvailability.ExcursionId,
																		LocationId : excursionLocation.LocationId,
																		TravelDate : excursionDate.ExcursionDate,
																		StartTime : excursionStartTime.StartTime,
																		Adults : travellers
																	};
																	excursionAvailability.Prices.push( excursionPriceRQ );
																});
															}
														});	
													}
												});
											}
										}
										excursion_ids.push( excursionAvailability.ExcursionId );
										// check if prices are available for excursions
										if( excursionAvailability.Prices.length > 0 && aeResponseCounter < aeResponseLimit ){
											excursions.push( cleanExcursionValue( excursionAvailability ) );
											aeResponseCounter++;
										}
										// check if the number of responses have been met
										if( aeResponseCounter >= aeResponseLimit ){
											excCtr = excLength;
										}
									}
								}

								if( excursions )
								{
									excursions.forEach(function( excursion ){
										var excur_price_rq = [];
										// console.log("excursion.Prices", excursion.Prices);
										if( excursion.Prices )
										{
											excursion.Prices.forEach(function( priceRQ ){
												excur_price_rq.push( eroam.apiDeferred('api-ae/get/excursion-price', 'POST', priceRQ, 'ae', true) );
											});
											eroam.apiArrayOfPromisesHandler( excur_price_rq, function( excur_price_rs ){
												if( excur_price_rs )
												{
													excur_price_rs.forEach( function( excursionPrices ){
														excursion.Prices.forEach( function( price, price_key ){
															if( price.LocationId == excursionPrices.LocationId &&
																price.TravelDate == excursionPrices.TravelDate &&
																price.StartTime == excursionPrices.StartTime )
															{
																excursion.Prices[ price_key ].Price = excursionPrices.Amount;
																excursion.Prices[ price_key ].Currency = 'AUD';
															}
														});	
														excursion.provider = 'ae';
														try
														{
															appendActivities( excursion );
															totalActivityCount++; // increment the total number of activities
														}
														catch(e){
															console.log(e.message);
														}
													});
												}
												// console.log("excursions done", excursions);
												next();
											});
										}
										else
										{
											next();
										}
									});
								}
								else
								{
									next();
								}
							}
							else
							{
								next();
							}
						});
					}
					else
					{
						next();
					}

				});
				// END ARABIAN EXPLORERS API RESPONSE
			}
			else
			{
				next();
			}

		});
	}
	else
	{
		next();
	}

}


function cleanExcursionValue( excursion ){
	result = JSON.parse( JSON.stringify( excursion ) );
	delete result.ExcursionStartTimes;
	delete result.ExcursionDates;
	delete result.AdultAgeRange;
	delete result.ChildAgeRange;
	delete result.InfantAgeRange;
	delete result.SeniorAgeRange;
	delete result.ExcursionTypeId;
	delete result.OnRequest;
	return result;
}


function checkActivityCount( next )
{
	$('.act-count').html( ': ' + totalActivityCount + ( (totalActivityCount==1) ? ' Activity' : ' Activities' )+ ' Found');
	if( totalActivityCount == 0 )
	{
		var html = '<h4 class="text-center blue-txt bold-txt">No Activities Found.</h4>';
		$('.activity-list').append( html );
	}
	next();
}


function hidePageLoader( next )
{
	$( '#activity-loader' ).fadeOut(300);
	next();
}
function hideAutoSortLoader( next )
{
	setTimeout(function(){ 
		$( '#auto-sort-loader' ).fadeOut(300);
	}, 1000);
	
	next();
}

// created by miguel on 2017-01-11;
// function to build the html for activity-list based on api call results 
function appendActivities( act )
{	
	var id;
	var name;
	var description;
	var price;
	var currency;
	var imageSrc;
	var dataAttributes;
	var viewMoreBtn; // variable to store "View more" button DOM for
	var index;
	var activityDate;
	var convertedPrice;
	var code;
	var duration;

	switch( act.provider )
	{
		case 'eroam':
			name           = act.name;
			selected       = (act.selected) ? true:false;
			btn_value      = (selected) ? 'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
			class_selected = (selected) ? 'selected-activity' :'';
			description    = act.description.substr(0, 225)+'...';
			price          = parseFloat(act.activity_price[0].price);
			currency       = act.activity_price[0].currency.code;
			
			convertedPrice = eroam.convertCurrency( price, currency );
			imageSrc       = "<?php echo e(config('env.DEV_CMS_URL')); ?>" + (( act.image[0].medium ) ? act.image[0].medium : 'uploads/activities/'+act.image[0].large);
			index          = act.index;
			activityDate   = (selected) ? act.activity_date : '';
			code		   = act.id;
			duration 	   = act.duration;

			var label = [];
			if(act.pivot.length > 0){
				if(selected){
					label.push( parseInt(travel_pref[0]) );
				}else{
					$.each(act.pivot, function(index, value){
						var labelID = parseInt(value.label_id);
						label.push(labelID);
					});
				}
				
			}
		
			var sortedLabels = sortCatPriority(label);

			dataAttributes = [
					'data-provider="'+act.provider+'" ',
					'data-city-id="'+cityId+'" ',
					'data-index="'+index+'" ',
					'data-activity-id="'+act.id+'" ',
					'data-is-selected="'+selected+'" ',
					'data-price="'+convertedPrice+'" ',
					'data-currency="'+globalCurrency+'" ',
					'data-price-id="'+act.activity_price[0].id+'" ',
					'data-name="'+act.name.replace(/"/g, '&quot;')+'"',
					'data-description="'+act.description.replace(/"/g, '&quot;')+'"',
					'data-duration="'+act.duration+'"'
				];

			if(sortedLabels.length > 0){
				var dataLabel = 'data-label="'+sortedLabels[0]+'"';
				dataAttributes.push(dataLabel);
			}
			dataAttributes = dataAttributes.join('');

			viewMoreBtn = '<a href="#"  data-select ="'+selected+'" data-cityId ="'+cityId+'" data-provider="'+act.provider+'" data-code="'+act.id+'" class="view-more-btn"><button class="btn act-btn btn-primary btn-block bold-txt act-view-more">View more</button></a>';
			id = act.id;
			break;

		case 'viator':
			
			name           = act.title;
			selected       = (act.selected) ? true:false;
			btn_value      = (selected) ? 'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
			class_selected = (selected) ? 'selected-activity' :'';
			description    = stripTags(act.shortDescription.substr(0, 225)+'...');
			price          = parseFloat( markUpPrice( act.price, 30 ) );
			currency       = act.currencyCode;
			convertedPrice = eroam.convertCurrency( price, currency );
			imageSrc       = act.thumbnailHiResURL;
			index          = act.code;
			id             = act.code;	
			activityDate   = (selected) ? act.activity_date : '';
			code		   = act.code;
			duration 	   = act.duration;

			//console.log('as', act);
			var sortedLabels = [];

			<?php if( session()->has( 'search_input' ) && count(session()->get( 'search_input' )['interests']) > 0 ): ?>
			if(selected){
				sortedLabels.push( parseInt(travel_pref[0]) );
			}else{
				sortedLabels = sortCatPriority(act.labels);
			}
			<?php endif; ?>

			dataAttributes = [
					'data-provider="'+act.provider+'" ',
					'data-city-id="'+cityId+'" ',
					'data-index="'+index+'" ',
					'data-activity-id="'+act.code+'" ',
					'data-is-selected="'+selected+'" ',
					'data-price="'+convertedPrice+'" ',
					'data-name="'+act.title.replace(/"/g, '&quot;')+'"',
					'data-currency="'+globalCurrency+'"',
					'data-startDate="'+dateFrom+'"',
					'data-endDate="'+dateTo+'"',
					'data-description="'+stripTags(act.shortDescription.replace(/"/g, '&quot;'))+'"',
					'data-images="'+imageSrc+'"',
					'data-duration="'+act.duration+'"'
					
				];
			if(sortedLabels.length > 0){
				var dataLabel = 'data-label="'+sortedLabels[0]+'"';
				dataAttributes.push(dataLabel);
			}
			dataAttributes = dataAttributes.join('');

			viewMoreBtn = '<a href="#"  data-select ="'+selected+'" data-cityId ="'+cityId+'"  data-provider="'+act.provider+'" data-code="'+act.code+'" class="view-more-btn"><button class="btn act-btn btn-primary btn-block bold-txt act-view-more" >View more</button></a>';
			id = act.code;

		    break;

		case 'ae':

			name           = act.ExcursionName;
			selected       = (act.selected) ? true : false;
			btn_value      = (selected) ? 'CANCEL ACTIVITY' : 'BOOK ACTIVITY';
			class_selected = (selected) ? 'selected-activity' : '';
			description    = stripTags(act.Description.substr(0, 225)+'...');
			price          = parseFloat( markUpPrice( parseFloat(act.Prices[0].Price), 30 ) );
			currency       = act.Prices[0].Currency;
			convertedPrice = eroam.convertCurrency( price, currency );
			imageSrc       = act.Images[0];
			index          = act.ExcursionId; 
			id             = act.ExcursionId;	
			activityDate   = (selected) ? act.activity_date : '';
			code		   = act.code;
			duration 	   = act.duration;

			dataAttributes = [
					'data-provider="'+act.provider+'" ',
					'data-city-id="'+cityId+'" ',
					'data-index="'+index+'" ',
					'data-activity-id="'+id+'" ',
					'data-is-selected="'+selected+'" ',
					'data-price="'+convertedPrice+'" ',
					'data-name="'+name+'"',
					'data-currency="'+globalCurrency+'"',
					'data-startDate="'+dateFrom+'"',
					'data-endDate="'+dateTo+'"',
					'data-description="'+stripTags(act.Description.substr(0, 225)+'...')+'"'
				].join('');

			viewMoreBtn = '<a href="#"  data-select ="'+selected+'" data-cityId ="'+cityId+'"  data-provider="'+act.provider+'" data-code="'+act.code+'" class="view-more-btn"><button class="btn act-btn btn-primary btn-block  bold-txt act-view-more">View more</button></a>';
			id = act.code;

			break;

		case 'own_arrangement': 

			break;

		case 'aot':

			break;	

	}
	// var convertedPricePerPerson = Math.ceil( convertedPrice / travellers );
	var priceString = globalCurrency + ' ' + Math.ceil( convertedPrice ).toFixed(2);
	roundedUpConvertedPrice = Math.ceil( convertedPrice ).toFixed(2);

	maxPrice = parseInt( roundedUpConvertedPrice ) > maxPrice ? parseInt( roundedUpConvertedPrice ) : maxPrice;
	
	html = [	
		'<div class="list-wrapper custom-list-wrapper">',
  			'<div class="activity list-box '+class_selected+'" '+dataAttributes+'>',
				'<div class="row">',
                  '<div class="col-sm-7">',
                    '<h4>'+name+'</h4>',
                    '<p>',
                      '<svg width="23px" height="18px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">',
                          '<desc>Created with Sketch.</desc>',
                          '<defs></defs>',
                          '<g id="EROA013-01-(B2C)-Brochure-Site" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">',
                              '<g id="04.02-B2C-Brochure-Site-(Homepage---Activities)" transform="translate(-516.000000, -589.000000)" fill="#212121">',
                                  '<g id="Group" transform="translate(501.000000, 542.000000)">',
                                      '<g id="Group-16">',
                                          '<path d="M19.6358927,62.5289627 L33.1229673,62.5289627 L33.1229673,57.5722498 L19.6358927,57.5722498 L19.6358927,62.5289627 Z M33.5235085,56.8720787 L19.2314118,56.8720787 C19.0120991,56.8720787 18.8321839,57.0318264 18.8321839,57.2221642 L18.8321839,62.8779152 C18.8321839,63.0727849 19.0120991,63.2280008 19.2314118,63.2280008 L33.5235085,63.2280008 C33.7493874,63.2280008 33.9293026,63.0727849 33.9293026,62.8779152 L33.9293026,57.2221642 C33.9293026,57.0318264 33.7493874,56.8720787 33.5235085,56.8720787 L33.5235085,56.8720787 Z M36.4888263,61.9579493 C35.4421663,62.0995697 34.6161322,62.8427934 34.5189518,63.7627593 L18.4316428,63.7627593 C18.3239563,62.7895441 17.4112477,62.0145975 16.2726602,61.9398219 L16.2726602,58.053759 C17.370537,57.9835153 18.2569805,57.2618179 18.4158838,56.3373202 L34.5347108,56.3373202 C34.677855,57.199505 35.4631783,57.8860805 36.4835733,58.023169 L36.4835733,61.9579493 L36.4888263,61.9579493 Z M17.1696098,54.8848617 C17.339019,54.4826598 17.360031,54.048735 17.2103205,53.6284058 L31.8569946,47.8038433 C32.4151257,48.5391362 33.4827977,48.8892218 34.451976,48.6411029 L36.3548749,52.2122021 C35.945141,52.4421936 35.6325875,52.7832154 35.4579253,53.1899491 C35.2622511,53.6374695 35.2622511,54.1201117 35.4474193,54.5619672 L32.9338594,55.6020272 C32.9128474,55.6110909 32.9075944,55.6337502 32.8878956,55.6416809 L31.2253215,55.6416809 L34.2825668,54.4248787 C34.483494,54.3455713 34.5754215,54.141638 34.478241,53.9648958 L31.7388021,48.8269088 C31.6455614,48.6535655 31.4052367,48.5697262 31.2043095,48.6580973 L18.2215228,53.8142117 C18.1295953,53.8538654 18.0468606,53.9252421 18.0114028,54.0090813 C17.9798849,54.0974524 17.9798849,54.1948872 18.0271618,54.2787265 L18.7547022,55.6416809 L18.0219088,55.6416809 C17.7960299,55.6416809 17.6213677,55.8014287 17.6213677,55.9917665 C17.6213677,56.0178246 17.6213677,56.0540794 17.6318737,56.0846694 C17.6161147,56.5933698 17.2575975,57.0227627 16.7388639,57.2266961 L16.097998,55.9611765 C16.5957197,55.7266531 16.9699958,55.3505094 17.1696098,54.8848617 L17.1696098,54.8848617 Z M36.8420905,57.3637846 C36.0121167,57.3637846 35.3397328,56.7927713 35.3187208,56.0892013 C35.3239738,56.0540794 35.3292268,56.0178246 35.3292268,55.9917665 C35.3292268,55.8014287 35.1545646,55.6416809 34.9234327,55.6416809 L34.7080597,55.6416809 L36.1802127,55.0355457 C36.2787064,54.995892 36.3601279,54.9211165 36.3863929,54.8282135 C36.4218506,54.7398424 36.4165976,54.6424076 36.3601279,54.5574354 C36.3246701,54.5007872 36.2892124,54.4600006 36.2629474,54.4430061 C36.0882852,54.1201117 36.0725262,53.7609624 36.2169836,53.4290043 C36.3601279,53.101578 36.6424766,52.8409965 36.981295,52.703908 C37.0942345,52.6767169 37.1914149,52.611005 37.2426317,52.5181021 C37.2991014,52.4206673 37.2991014,52.3141687 37.2478847,52.2167339 L35.0271794,48.0519622 C34.9444447,47.9012781 34.7395777,47.8219707 34.5596625,47.8616244 C34.5242048,47.8706881 34.472988,47.8933473 34.4270243,47.9148736 C33.6666527,48.1981144 32.7749561,47.9103418 32.4518967,47.3347966 C32.4466437,47.2951429 32.4256317,47.2294311 32.4046197,47.1931762 C32.3074392,47.0254977 32.0763073,46.9507222 31.8766933,47.0345614 L16.5287439,53.1378328 C16.3330697,53.2126084 16.287106,53.468658 16.3645877,53.6420013 C16.5392499,53.9603639 16.5536957,54.3183802 16.4105514,54.6469395 C16.2726602,54.9788976 15.9903115,55.2440109 15.5950234,55.3901631 C15.5438066,55.3992268 15.4925899,55.4071576 15.4413732,55.4207531 C15.3336867,55.4479443 15.2404459,55.518188 15.1892292,55.6065591 C15.1432655,55.6949302 15.1380125,55.8014287 15.1892292,55.8943317 L15.9128298,57.3286627 C15.8103963,57.3286627 15.6974568,57.3411253 15.6055294,57.4079701 C15.5227946,57.4702831 15.4715779,57.5677179 15.4715779,57.6696846 L15.4715779,62.3250293 C15.4715779,62.426996 15.5227946,62.519899 15.6055294,62.5856108 C15.6974568,62.6524556 15.8103963,62.6785138 15.9338418,62.670583 C15.9482875,62.6660512 16.087492,62.6343282 16.103251,62.6297964 C16.9489838,62.6297964 17.6371267,63.2234689 17.6371267,63.9451663 C17.6266207,63.9666926 17.6056087,64.0505319 17.6003557,64.0686593 C17.5859099,64.170626 17.6213677,64.2680608 17.6988494,64.3428363 C17.7750179,64.4187448 17.8879574,64.4629304 18.0008968,64.4629304 L34.9759627,64.4629304 C35.2005284,64.4629304 35.3804436,64.3031826 35.3804436,64.1128449 C35.3804436,64.046 35.3594316,63.9802882 35.3187208,63.9315708 C35.3344798,63.2098734 36.0121167,62.6297964 36.771175,62.6263975 C37.0272587,62.6739819 37.2991014,62.5108352 37.2991014,62.2899075 L37.2991014,57.7048064 C37.2885954,57.5144687 37.0679695,57.3637846 36.8420905,57.3637846 L36.8420905,57.3637846 Z" id="Fill-1"></path>',
                                      '</g>',
                                  '</g>',
                              '</g>',
                          '</g>',
                      '</svg>',
                      'Tour Code: '+code+'   |   ',
                      '&nbsp;Location: '+cityName+
                    '</p>',
                  '</div>',
                  '<div class="col-sm-5">',
                    '<div class="row">',
                      '<div class="col-xs-12 m-t-12 text-right border-right">',
                        '<p style="margin-bottom:5px;">From <strong>'+priceString+'</strong> Per Person</p>',
                        '<p>Duration : '+duration+' Day</p>',
                        /*'<ul class="rating">',
			                '<li><a href="#"><i class="fa fa-star"></i></a></li>',
			                '<li><a href="#"><i class="fa fa-star"></i></a></li>',
			                '<li><a href="#"><i class="fa fa-star"></i></a></li>',
			                '<li><a href="#"><i class="fa fa-star"></i></a></li>',
			                '<li><a href="#"><i class="fa fa-star"></i></a></li>',
			            '</ul>',*/
                      '</div>',
                      //'<div class="col-xs-3 text-center">',
                        //'<h3 class="transport-price"><strong>5</strong></h3>HOURS',
                      //'</div>',
                    '</div>',
                  '</div>',
                '</div>',
                '<hr/>',

			  	'<div class="clearfix">',
			  		'<div class="listbox-img" >',
			  			'<img src="'+imageSrc+'" alt="" class="img-responsive" style="height:200px"/>',
			  		'</div>',
			  		'<div class="listbox-details">',
					    '<p class="m-t-10">'+description+'</p>',
					    
					    '<div class="row" style="margin-top: 85px;">',
					      	'<div class="col-sm-6">',
					      		'<input type="text" '+dataAttributes+'  class="datetimepick" id="datepick-'+id+'" value="">',
					      		'<button class="btn btn-primary btn-block act-btn act-select-btn" '+dataAttributes+' style="margin-bottom:0px;" data-button-id="'+id+'">'+btn_value+'</button>',
					      	'</div>',
					      	'<div class="col-sm-6">',
					      		viewMoreBtn,
					      	'</div>',
					    '</div>',
					'</div>',
				'</div>',
			'</div>',  
		'</div>'
	].join('');



	if(class_selected){
		$('.activity-list').prepend( html );
	}else{
		$('.activity-list').append( html );
	}
	
}
/*
| added by junfel
| this function will update the selected activities when date is selected
*/
function updateOnDateSelected(date, i, onAdd = false){
	if(date){
		$(i.context).removeData('mousewheelPageHeight');
		$(i.context).removeData('mousewheelLineHeight');
		
		var data = onAdd ? i.data() : $(i.context).data();
		console.log('checking ', data);
		data.date_selected = date;
		data.extraNights = 0;

		var dateToStart = formatDate( data.date_selected, '/');
		var startCounting = false;
		var countNumOfDates = 0;

		for( count = 0; dates.length > count; count ++ ){
			if( (dateToStart == dates[count]) || startCounting ){
				startCounting = true;
				countNumOfDates += 1;
			}else{
				continue;
			}
		}

		if( data.isSelected ){
			deselectThisActivity( data, $(i.context) );
		}
		else{

			if( parseInt( data.duration ) > 1 && !onAdd){
			/*	var dateToStart = formatDate( data.date_selected, '/');
				var startCounting = false;
				var countNumOfDates = 0;
	
				for( count = 0; dates.length > count; count ++ ){
					if( (dateToStart == dates[count]) || startCounting ){
						startCounting = true;
						countNumOfDates += 1;
					}else{
						continue;
					}
				}*/
		
				if( countNumOfDates >= parseInt( data.duration ) ){
					selectThisActivity( data );
				}else{

					var extraNights = countNumOfDates - parseInt(data.duration);
					extraNights = Math.abs(extraNights);
					data.extraNights = extraNights;
					eroam.confirm( null, 'You need '+extraNights+' more night(s) added to your stay for this city if you select this activity on this date. Would you like to continue?', function(e){
						
						selectThisActivity( data );
					},function(e){
						data = null;
					});
				}
				
			}else{
				var add = false;
				if( countNumOfDates <= 0  ){
					add = true;
				}
				selectThisActivity( data , add);
			}

			/*
			| Display date selected
			*/
			var formattedDate  = moment($(i.context).val()).format('Do, MMMM YYYY');
			//eeeeeeeeeeeeeee
			$(i.context).nextAll('#act-date').text(formattedDate);
		}
	}
}


$('body').on('click', '.act-select-btn', function(e){
	e.preventDefault();
	var data = $(this).data();
	var dataId = $(this).attr('data-button-id');
	var book = $(this).text();
	var viewData = $(this).attr('data-view');
	/*
	| Update by junfel
	| Check if button is for cancel or booking
	| 
	*/

	if(dates[0] == '0000/00/00' && book == 'BOOK ACTIVITY'){ 
		eroam.confirm( null, 'Would you like to add another day?', function(e){
			addNumOfDay(data);
		},function(e){
			data = null;
		});
	}else{
		showDatefield(dataId, data.isSelected, data, viewData);
	}
    //setTimeout(function(){ location.reload(); }, 3000);
});
/*
| function to display date field
*/
function showDatefield(id, selected, data, view){
	if(view == 'view') {
		var input = $('#view_datepick-'+id);
	}else {
		var input = $('#datepick-'+id);
	}

	if(!selected){
		$('#date-field-container-'+id).addClass('show');
		$(input).click();

	}else{
		$('#date-field-container-'+id).removeClass('show');
		
		$(input).nextAll('#act-date').text('');
		deselectThisActivity( data, input );

		if(view == 'view') {
			setTimeout(function(){ parent.location.reload(); }, 2000);
		}else {
			setTimeout(function(){ location.reload(); }, 2000);
		}

        
	}

}
/*
| Function to add number of days if the user want's to stay longer in the selected city
*/
function addNumOfDay(data){

	eroam.ajax( 'get', 'latest-search', { }, function( response ){
		if(response){
			search_session = JSON.parse(response);
			
			var session = search_session.itinerary;
			if(session.length > 0){

				//for(session_counter = 0; session_counter < session.length; session_counter ++){
				if(session[leg].city.id == cityId ){
					var default_nights = search_session.itinerary[leg].city.default_nights;
					
					//search_session.itinerary[leg].city.default_nights = parseInt(default_nights) + parseInt(data.duration);
					search_session.itinerary[leg].city.days_to_add = 1;
					search_session.itinerary[leg].city.add_after_date = dateFrom;
					
					var act;
					var selected_date = dateFrom;

					if( search_session.itinerary[leg].activities != null ){
						if(search_session.itinerary[leg].activities.length > 0 ){
							act = search_session.itinerary[leg].activities;
							act = sortByDate(act);
							var selected_date = act[act.length-1].date_selected;
							if( parseInt( act[act.length-1].duration ) > 1  ){
								var addDuration = parseInt( act[act.length-1].duration ) - 1;
								selected_date = addDays(selected_date, addDuration);
							}
							
						}
					}
					
					/*postData = {
						dates_available:dates, 
						id: cityId,
						_token: $('meta[name="csrf-token"]').attr('content')
					};*/

					//updateAvailableDates(postData);
					var formattedDate  = moment(addDays(selected_date, 1)).format('Do, MMMM YYYY');
					$('#datepick-'+data.activityId).nextAll('#act-date').text(formattedDate);
					updateOnDateSelected(addDays(selected_date, 1), $('#datepick-'+data.activityId), true);
					
				}
			}
		}
	});
	
	
}
/*
| Function for adding days to a given date
*/
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + parseInt(days) );
    
    return formatDate(result, '-');
}
/*
| Function to update the session for dates avaible
*/
/*function updateAvailableDates(data){
	eroam.ajax('post', 'session/dates-available', data, function(response){

	});
}*/
/*
| End Junfel
*/
function selectThisActivity(data, add = false)
{
	if(data){
		var map_data = [];
		var selected_date = data.date_selected;

		switch( data.provider )
		{

			case 'eroam':
				var activityDiv = $('.activity[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');
				var activityBtn = $('.act-select-btn[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');
				activityDiv.toggleClass('selected-activity');

				activityDiv.data('is-selected', true);
				activityBtn.data('is-selected', true);
				activityBtn.html('CANCEL ACTIVITY');
				var dataAttr = activityDiv.data();
			
				/*
				| Added by junfel
				| remove the date selected from the dates array
				*/

				var selectedDates = [];
				
				if( parseInt( data.extraNights ) ){

					var aLength = parseInt( data.extraNights ) ? parseInt( data.extraNights ) + 1 : parseInt(data.duration) ;
					var range = _.range(1, aLength);

					$.each( range, function(key, num){
						var sDate = addDays(selected_date, num).replace(/-/g, '/') ;
						selectedDates.push( sDate );

						var dateIndex = dates.indexOf( selected_date.replace(/-/g, '/') );
					
						if( dateIndex > -1 && ( typeof dates[dateIndex+parseInt(num)] != 'undefined') ){
							selectedDates.push( dates[dateIndex+parseInt(num)] );
						}
					});
				}

				selectedDates.push( selected_date.replace(/-/g, '/') );
		
				for(index = dates.length-1; index >= 0; index--){
					var eachDate = dates[index];
					if( typeof eachDate != 'undefined'){

						if( selectedDates.indexOf(eachDate) > -1 ){
							dates.splice(index, 1);
						}

					}
				}
				
				/*
				| This will update the session for dates available
				*/
				if(dates.length > 0){
					postData = {dates_available:dates, id: cityId};
				}else{
					dates.push('0000/00/00');
					postData = {
						dates_available: ['0000/00/00'], 
						id: cityId,
						_token : $('meta[name="csrf-token"]').attr('content')
					};
				}
				//updateAvailableDates(postData);
		
				//console.log(selected_date);
				var eRoamActdata ={
							"date_from":dateFrom,
							"date_to":dateTo,
							'id':dataAttr.activityId,
							'city_id':dataAttr.cityId,
							'name':	dataAttr.name,
							'city':{'id':dataAttr.cityId},
							'activity_price':[{
								'price':dataAttr.price,
								'currency':{'id':globalCurrency_id,'code':dataAttr.currency}
							}], 
							'price':[{
								'price':dataAttr.price,
								'currency':{'id':globalCurrency_id,'code':dataAttr.currency}
							}], 
							'date_selected':selected_date,
							'currency_id':globalCurrency_id,
							'currency': dataAttr.currency,
							'provider': dataAttr.provider,
							'description': dataAttr.description,
							'duration': dataAttr.duration
						};
		
				var extraNights = parseInt( data.extraNights ) ? parseInt( data.extraNights ) : 0; 

				extraNights =  add ? parseInt(data.duration) : extraNights;
				eRoamActdata.extra_nights = extraNights;
				
				updateSession(search_session,eRoamActdata,true, extraNights);

				break;

			case 'aot':

				break;

			case 'ae':

				break;
			case 'viator':
				var activity_data = data;

				var activityDiv = $('.activity[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');	
				var activityBtn = $('.act-select-btn[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');
				activityDiv.toggleClass('selected-activity');

				activityDiv.data('is-selected', true);
				activityBtn.data('is-selected', true);
				activityBtn.html('CANCEL ACTIVITY');
				
				data ={"date_from":dateFrom,
					   "date_to":dateTo,
					   'id':activity_data.activityId,
					   'city_id':activity_data.cityId,
					   'name':activity_data.name,
				       'city':{'id':activity_data.cityId},
					   'activity_price':[{
							'price':activity_data.price,
							'currency':{'id':1,'code':activity_data.currency}
						}], 
						'price':[{
							'price':activity_data.price,
							'currency':{'id':1,'code':activity_data.currency}
						}], 
						'date_selected':selected_date,
						'currency_id':1,
						'currency': activity_data.currency,
						'provider': activity_data.provider,
						'description': activity_data.description,
						'images':[activity_data.images],
						'duration': activity_data.duration,
						'cancellation_policy':'',
					   };

				console.log('viator-checking-data', data);
				/*
				| Added by junfel
				| remove the date selected from the dates array
				*/
			
				var selectedDates = [];
				
				var aLength = parseInt( data.extraNights ) ? parseInt( data.extraNights ) + 1 : parseInt(data.duration) ;
				var range = _.range(1, aLength);

				$.each( range, function(key, num){
					
					var sDate = addDays(selected_date, num).replace(/-/g, '/') ;
					selectedDates.push( sDate );

					var dateIndex = dates.indexOf( selected_date.replace(/-/g, '/') );
				
					if( dateIndex > -1 && ( typeof dates[dateIndex+parseInt(num)] != 'undefined') ){
						selectedDates.push( dates[dateIndex+parseInt(num)] );
					}
				});
			
				selectedDates.push(selected_date.replace(/-/g, '/'));
				for(index = dates.length-1; index >= 0; index--){
					var eachDate = dates[index];
					if( typeof eachDate != 'undefined'){
		
						if( selectedDates.indexOf(eachDate) > -1 ){
						
							dates.splice(index, 1);
						}

					}
				}
				
				/*
				| This will update the session for dates available
				*/
				if(dates.length > 0){
					postData = {dates_available:dates, id: cityId};
				}else{
					dates.push('0000/00/00');
					postData = {
						dates_available: ['0000/00/00'], 
						id: cityId,
						_token: $('meta[name="csrf-token"]').attr('content')
					};
				}
				//updateAvailableDates(postData);
				/*
				| End junfel
				*/
				console.log(search_session);
				var extraNights = parseInt( activity_data.extraNights ) ? parseInt( activity_data.extraNights ) : 0; 
				extraNights =  add ? parseInt(data.duration) : extraNights;
				data.extra_nights = extraNights;
				updateSessionViator(search_session,data,true, extraNights);
						
			    break;	

		}
	}
	
}


function deselectThisActivity(data, input)
{
	var map_data = [];
	switch( data.provider )
	{

		case 'eroam':
			var activityDiv = $('.activity[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');
			var activityBtn = $('.act-select-btn[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');
			activityDiv.toggleClass('selected-activity');
			activityDiv.data('is-selected', false);
			activityBtn.data('is-selected', false);
			input.data('is-selected', false);
			activityBtn.html('BOOK ACTIVITY');
			map_data.id      = data.activityId;
			map_data.city_id = data.cityId;

			updateSession(search_session,map_data,false)
			break;

		case 'aot':

			break;

		case 'ae':

			break;
		case 'viator':
			var activityDiv = $('.activity[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');	
			var activityBtn = $('.act-select-btn[data-provider="'+data.provider+'"][data-activity-id="'+data.activityId+'"]');
			activityDiv.toggleClass('selected-activity');
			activityDiv.data('is-selected', false);
			activityBtn.data('is-selected', false);
			input.data('is-selected', false);
			activityBtn.html('BOOK ACTIVITY');
			map_data.id = data.activityId;
			map_data.city_id = data.cityId;
			updateSessionViator(search_session,map_data,false)

			break;	

	}
}

/*
| Update by junfel
| Added separator parameter
*/
function formatDate(date, separator='-') {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join( separator );
}

function checkDate(selectedDate, dateAvailable){
		var result = false;
		var selectedDate = new Date(selectedDate);
		var dateAvailable = new Date(dateAvailable);

		if( selectedDate > dateAvailable ){
			result = true;
		}
		return result;
}

function updateSessionViator(search_session,new_activity,isSelected, extraNights = 0){
	var id = parseInt(new_activity.activityId);

	switch(isSelected){
		case true:
			$.each(search_session.itinerary, function (key, itinerary) {
		
				if(itinerary.city.id == new_activity.city_id /*&& itinerary.activities.length > 0*/){
					
					if (itinerary.activities == null ){
						itinerary.activities = [];
					}
					if(extraNights){

						itinerary.city.default_nights =  parseInt(itinerary.city.default_nights) + parseInt(extraNights);
					}
					itinerary.activities.push(new_activity);

					if(itinerary.city.default_nights == 0 || itinerary.city.default_nights == null || itinerary.city.default_nights == ''){
						itinerary.city.default_nights = itinerary.activities.length;
					}
					var act = itinerary.activities;
					act = sortByDate( act );
					itinerary.activities = act;
				}
			});

			break;
		case false:
		
			$.each(search_session.itinerary, function (key, itinerary) {
				
				var temp  = itinerary.activities;

				if(itinerary.city.id == new_activity.city_id && itinerary.activities.length > 0){
					$.each(itinerary.activities, function (k, activity) {
						if( typeof activity != 'undefined' ){

							if( activity.id == new_activity.id ){
								
								if (activity.extra_nights) {
								
									search_session.itinerary[key].city.days_to_deduct = activity.extra_nights;
									search_session.itinerary[key].city.deduct_after_date = dateFrom;
									search_session.itinerary[key].city.default_nights = parseInt( search_session.itinerary[key].city.default_nights ) - parseInt( activity.extra_nights );
								}
								/*
								| Added by junfel
								*/
								/*if(dates[0] == '0000/00/00'){
									dates.splice(0, 1);
								}

								var aLength = parseInt(activity.duration) ;
								var range = _.range(1, aLength);
								var dateSelected = activity.date_selected.replace(/-/g, '/');*/

								var aLength =  parseInt(activity.duration) - parseInt(activity.extra_nights) ;
								if ( aLength ) {
									
									if(dates[0] == '0000/00/00'){
										dates.splice(0, 1);
									}

									//var aLength = parseInt(activity.duration) ;
									var range = _.range(1, aLength);
									var dateSelected = activity.date_selected.replace(/-/g, '/');
									$.each( range, function(key, num){
										dates.push( addDays( dateSelected, num).replace(/-/g, '/') );
									});
									dates.push( dateSelected );
								}

								postData = {
									dates_available:dates, 
									id: cityId,
									_token: $('meta[name="csrf-token"]').attr('content')
								};

								//updateAvailableDates( postData );
								/*
								| End Junfel
								*/
								temp.splice(k,1);
							}
						}
					});
				}
			});
				
			break;	
	}
	search_session._token = $('meta[name="csrf-token"]').attr('content');
	console.log('hi');
    setTimeout(function(){ console.log("Hello"); }, 3000);
	bookingSummary.update(JSON.stringify(search_session));

}

function updateSession(search_session,new_activity,isSelected, extraNights = 0 ){
	
	var id = parseInt(new_activity.id);

	switch(isSelected){
		case true:
			$.each(search_session.itinerary, function (key, itinerary) {
				if(itinerary.city.id == new_activity.city_id /*&& itinerary.activities.length > 0*/){
					if (itinerary.activities == null ){
						itinerary.activities = [];
					}
					
					if(extraNights){
						itinerary.city.default_nights =  parseInt(itinerary.city.default_nights) + parseInt(extraNights);
					}
					itinerary.activities.push(new_activity);
					if(itinerary.city.default_nights == 0 || itinerary.city.default_nights == null || itinerary.city.default_nights == ''){
						itinerary.city.default_nights = itinerary.activities.length;
					}
					var act = itinerary.activities;
					act = sortByDate( act );
					itinerary.activities = act;
				}
			});
			
			break;
		case false:

			$.each(search_session.itinerary, function (key, itinerary) {
				
				var temp  = itinerary.activities;
				if(itinerary.city.id == new_activity.city_id && itinerary.activities.length > 0){
					$.each(itinerary.activities, function (k, activity) {
						if( typeof activity != 'undefined' ){
							if( activity.id == new_activity.id ){
								if (activity.extra_nights) {
								
									search_session.itinerary[key].city.days_to_deduct = activity.extra_nights;
									search_session.itinerary[key].city.deduct_after_date = dateFrom;
									search_session.itinerary[key].city.default_nights = parseInt( search_session.itinerary[key].city.default_nights ) - parseInt( activity.extra_nights );
								}
								/*
								| Added by junfel
								| 
								*/
								/*if(dates[0] == '0000/00/00'){
									dates.splice(0, 1);
								}

								var aLength = parseInt(activity.duration) ;
								var range = _.range(1, aLength);
								var dateSelected = activity.date_selected.replace(/-/g, '/');*/
								
								var aLength =  parseInt(activity.duration) - parseInt(activity.extra_nights) ;
								if ( aLength ) {
									
									if(dates[0] == '0000/00/00'){
										dates.splice(0, 1);
									}

									//var aLength = parseInt(activity.duration) ;
									var range = _.range(1, aLength);
									var dateSelected = activity.date_selected.replace(/-/g, '/');
									$.each( range, function(key, num){
										dates.push( addDays( dateSelected, num).replace(/-/g, '/') );
									});
									dates.push( dateSelected );
								}
								
								

								postData = {
									dates_available:dates, 
									id: cityId,
									_token: $('meta[name="csrf-token"]').attr('content')
								};
								//updateAvailableDates(postData);
								/*
								| End Junfel
								*/
								temp.splice(k,1);
							}
						}
					});
				}
			});
				
			break;	
	}
	search_session._token = $('meta[name="csrf-token"]').attr('content');
	bookingSummary.update(JSON.stringify(search_session));
}
/*
| Added by Junfel
| activity datetime picker
*/
$('body').on('click', '.datetimepick', function(){
	dates.sort();
	var startDate = (dates.length > 0) ? dates[0] : 0;
	if( startDate == '0000/00/00' || startDate == 0 ){
		startDate = dateFrom;
	}

	//$(this).datepicker();

	$(this).datetimepicker({
	//$(this).datepicker({
		onGenerate:function( ct ){
			$(this).css({'background': '#000', 'padding':0, /*'padding-top': '10px',/*'left' :'1029.38px'*/});
			$(this).find('.xdsoft_label, .xdsoft_option ').css({'background': '#000', 'color': '#fff'});
			$(this).find('.xdsoft_calendar').css({'background': '#fff'});
			$(this).find('.xdsoft_date').toggleClass('xdsoft_disabled');
			$(this).find('.xdsoft_datepicker').css({'margin-left': 0,'width': '275px'});
			$(this).find('th').css({'background': '#000', 'color': '#fff', 'border-color': '#000'});
			$(this).find('.xdsoft_date > div').css({'padding': '7px'});
			var datePickerdates = $(this).find('.xdsoft_date');
			datePickerdates.each(function(){
				if(!$(this).hasClass('xdsoft_disabled')){
					//$(this).toggleClass('xdsoft_current');
					$(this).css({'opacity':'1'}).find('div').css({'font-weight':'bold', 'color':'#fff', 'background': '#000', 'opacity':'1'});
				
				}else{

					//$(this).css('background', '#999')
				}
			});
		},
		onSelectDate: function(ct, i){
			updateOnDateSelected(ct.dateFormat('Y-m-d'), i);
		},
		onClose: function(ct, i){
			$(i.context).blur()
		},

		closeOnDateSelect: true,
		timepicker: false,
		format: 'Y-m-d',
		disabledDates: dates,
		startDate: startDate.replace(/-/g, '/'),
		showOtherMonths: true,
		selectOtherMonths: false,
		//minDate: -20, maxDate: "+1M +10D" 
	});

	if($(this).hasClass('focus-triggered')){
		$(this).removeClass('focus-triggered');
		$(this).blur();
	}else{
		$(this).addClass('focus-triggered');
		$(this).focus();
	}
});
/*
| Added by Junfel
| jquery Ui range slider
*/
function rangeSlider(next){
	/*var min = parseInt( eroam.convertCurrency(0, 'AUD') );
	//var max = parseInt( eroam.convertCurrency(50000, 'AUD') );
	var max = parseInt( maxPrice );
	
	$( '#slider-range' ).slider({
		range: true,
		min: min,
		max: max,
		values: [ min, max ],
		slide: function( event, ui ) {
			$( '#price-from' ).val(ui.values[ 0 ]);
			$( '#price-to' ).val(ui.values[ 1 ]);
		}
	});
	$( '#price-from' ).val($( '#slider-range' ).slider( 'values', 0 ));
	$( '#price-to' ).val($( '#slider-range' ).slider( 'values', 1 ));

	$('.curr').text(globalCurrency);

	next();*/
}
/*
| Added by Junfel
| Function for removing html tags from description
*/
function stripTags(str){
	return str.replace(/<(?:.|\n)*?>/gm, '');
}

/*
| Added by junfel
*/
function sortCatPriority(labelIds){
	var labelId = labelIds.sort(function(a,b){
		var itemA = $.inArray(a, travel_pref);
		var itemB = $.inArray(b, travel_pref);
		itemA = itemA == -1 ? 100 : itemA;
		itemB = itemB == -1 ? 100 : itemB;
		if(itemB != 100 && itemA != 100){
			return (itemA < itemB) ? -1 : (itemA > itemB) ? 1 : 0;
		}
	});
	return labelId;
}

/*
| filter labels
*/
function filterLabels(labels){
	var te = [];
	labels.filter(function(element){
		var test = $.inArray(element, travel_pref);
		if(test != -1){
			te.push(element);
		}
	});
	return te;
}

/*
| Added by Junfel
| Function to sort the activties by preference
*/

function autoSortActivityByPref(next){
	$('#auto-sort-loader').fadeIn('slow');
	var activityList = $('.activity-wrapper');

	var sortedActivities = activityList.sort(function(a,b){
		var A = $(a).find('div.activity').attr('data-label');
		var B = $(b).find('div.activity').attr('data-label');

		var itemA = $.inArray(parseInt(A), travel_pref);
		var itemB = $.inArray(parseInt(B), travel_pref);

		itemA = itemA == -1 ? 100 : itemA;
		itemB = itemB == -1 ? 100 : itemB;

		return itemA - itemB;
	});
	$('#activity-list').html(sortedActivities);
	next();
}
function sortByDate(activities){
    var sorted = activities.sort(function (itemA, itemB) {
		var A = itemA.date_selected;
		var B = itemB.date_selected;
	    return A.localeCompare(B);
    });
   return sorted;
}

function countViatorActDuration( duration ){

	var result = 1;
	if(duration){

		var match = duration.match(/hour|day/);
		match = match ? match[0] : '';
		switch( match ){

			case 'hour':
				var hour = duration.split(' ');
				hour = parseInt( hour[0] );
				day = moment.duration(hour, 'hours').days();
				result = parseInt( day ) == 0 ? 1 : day;
			break;

			case 'day':
				var day = duration.split(' ');
				result = parseInt( day[0] );
			break;

			default:
				result = 1;
			break;

		}
	}
	return result;
}

function getViatorDuration(duration){
	var match = duration.match(/[\d\.]+[\s|-]hour|[\d\.]+[\s|-]minute|[\d\.]+[\s|-]day/i);
	return match ? ( match[0].match(/minute/) ? '1 day' : match[0].replace('-', ' ') ) : '1 day';
}

/*function adjustDisabledDates( selected_date, num ){

	if( dates.indexOf( selected_date ) == -1 ){
		num = parseInt(num) + 1;
		selected_date = addDays(selected_date, num).replace(/-/g, '/');
		selectedDates.push( selected_date );

		adjustDisabledDates( selected_date, num );
	}

}*/

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make( 'layouts.search' , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>