<?php $__env->startSection('custom-css'); ?>
<style>
	.wrapper {
		background-image: url( <?php echo e(url( 'assets/img/bg1.jpg' )); ?> ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>




    <div id="about-us">
		<div class="top-section-image">
			<img src="<?php echo e(asset('assets/images/bg-image.jpg')); ?>" alt="" class="img-responsive">
		</div>

		<section class="content-wrapper">
			<h1 class="hide"></h1>
			<article class="about-section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<h2>About Us</h2>
							<div class="m-t-30" style="font-size: 16px; text-align: justify;">
								<p>We are a global travel technology company that specializes in building travel itineraries for the intrepid traveller worldwide. "The eRoam technology uses current travellers' data to advise future travellers (of the same peer group) what is trending best for them. Through our "crowd sourcing" of data our platform offers instant "peer 2 peer" recommendations that are available for instant booking."On the eRoam platform you can research, build and book travel itineraries in under 20 seconds. eRoam offers a range of accommodation, transport and touring options to satisfy any budget from a student or backpacker through to families and luxury travellers.</p>
								<p>For example you could create itineraries along the East Coast of Australia (Sydney to Cairns), New Zealand (Auckland to Christchurch), Thailand (Bangkok to Koh Samui), Indochina (Bangkok to Saigon). We also have a full range of tours and packages for travelers that are after more private touring with the additional creature comforts.</p>
							</div>
						</div>
					</div>
				</div>
			</article>

			<article class="team-section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<h2>The Team</h2>
							<div class="hotels-list">
								<div class="row">
									<div class="col-md-4 col-sm-6 m-t-20">
										<div class="place-wrapper">
											<div class="team-image">
												<img src="<?php echo e(asset('assets/images/ant.png')); ?>" alt="" class="img-responsive">
											</div>
											<div class="place-details">
												<h4>ANTHONY HILL</h4>
												<p>CEO &amp; CO-FOUNDER</p>
												<p class="text-justify">With over 25 years in the travel industry in Europe/Africa/Asia and Australia, founder Anthony Hill brings a wealth of industry experience to eRoam. His vision, from concept, to where we are today, Anthony has driven the development of the system. Anthony has owned and operated online travel businesses since the nineties and has a passion for embracing travel technology.</p>
											</div>
											<p><a href="https://au.linkedin.com/in/anthony-hill-246b7318" target="_blank"><strong>View LinkedIn Profile</strong></a></p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 m-t-20">
										<div class="place-wrapper">
											<div class="team-image">
												<img src="<?php echo e(asset('assets/images/ross.png')); ?>" alt="" class="img-responsive">
											</div>
											<div class="place-details">
												<h4>ROSS GREGORY</h4>
												<p>COO &amp; CO-FOUNDER</p>
												<p class="text-justify">Ross Gregory brings extensive international experience to the team. From creating and managing pubs and hostels in Europe and Australia to creating an international online training portal, his in-depth knowledge of the target market is invaluable. Ross was also a founding director of Eurobus Ltd, the first hop-on- hop-off tour operation in Europe, founded in 1994.</p>
											</div>
											<p><a href="https://au.linkedin.com/in/ross-gregory-b6264413" target="_blank"><strong>View LinkedIn Profile</strong></a></p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 m-t-20">
										<div class="place-wrapper">
											<div class="team-image">
												<img src="<?php echo e(asset('assets/images/martin.png')); ?>" alt="" class="img-responsive">
											</div>
											<div class="place-details">
												<h4>MARTIN COWLEY</h4>
												<p>CHAIR ADVISORY BOARD</p>
												<p class="text-justify">Martin Cowley has 3 decades of experience in executive roles in aviation, travel and technology. He spent 20 years with the Swire Group and Cathay Pacific Airways in senior commercial roles, leading teams in 9 countries around the world and was a member of the founding team at the Oneworld alliance. He subsequently spent 10 years with Sabre as CEO, Sabre Pacific in Australia then SVP EMEA, Sabre Travel Network, based in London. Since 2011 he has built a successful portfolio of board and advisory board roles with a number of multinational companies in the travel and technology sectors. He is currently based in Sydney.</p>
											</div>
											<p><a href="https://au.linkedin.com/in/martin-cowley-63b5423" target="_blank"><strong>View LinkedIn Profile</strong></a></p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 m-t-20">
										<div class="place-wrapper">
											<div class="team-image">
												<img src="<?php echo e(asset('assets/images/gav.png')); ?>" alt="" class="img-responsive">
											</div>
											<div class="place-details">
												<h4>GAVIN RUSSELL</h4>
												<p>TECHNICAL &amp; OPERATIONS ADVISOR</p>
												<p class="text-justify">Gavin has an entrepreneurial-spirit, proven capabilities as an innovative technologist with over 20+ years of executive-level experience. A history of exceeding expectations through identification, qualification, consensus across stakeholders, implementation of enabling technologies and enterprise systems. Gavin's unique blend of innovation, technology and business acumen provides organisations with strategies that ensure global deployment and rapid scale across cloud infrastructure and software applications. Broad expertise in project life cycle management, sales channel development, client/vendor relationship management and financial/operational management.</p>
											</div>
											<p><a href="https://au.linkedin.com/in/gavinrussell" target="_blank"><strong>View LinkedIn Profile</strong></a></p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 m-t-20">
										<div class="place-wrapper">
											<div class="team-image">
												<img src="<?php echo e(asset('assets/images/judd.png')); ?>" alt="" class="img-responsive">
											</div>
											<div class="place-details">
												<h4>JUDD TAYLOR</h4>
												<p>NETWORK &amp; DATABASE SPECIALIST</p>
												<p class="text-justify">Holding a Bachelor of Computer Science degree, Judd brings over 25 years IT industry experience to the eRoam team. Having worked for companies such as ERP Solutions and Oracle, Judd has an extensive knowledge on all of the development aspects of the technology platform.</p>
											</div>
											<p><a href="https://au.linkedin.com/in/juddtaylor" target="_blank"><strong>View LinkedIn Profile</strong></a></p>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 m-t-20">
										<div class="place-wrapper">
											<div class="team-image">
												<img src="<?php echo e(asset('assets/images/kane.png')); ?>" alt="" class="img-responsive">
											</div>
											<div class="place-details">
												<h4>KANE WILLEMS</h4>
												<p>UI / UX SPECIALIST</p>
												<p class="text-justify">Kane has an eye for clean and artful design. He possesses superior UI &amp; UX skills, is able to translate high-level requirements into interaction flows and artefacts, transforming them into beautiful, intuitive, and functional user interfaces. Kane proficiently creates wireframes, storyboards, user flows, process flows and sitemaps to effectively communicate interaction and design ideas.</p>
											</div>
											<p><a href="https://au.linkedin.com/in/kanewillems" target="_blank"><strong>View LinkedIn Profile</strong></a></p>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</article>

			 <article class="video-section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/FskZXKUFvig" allowfullscreen="">
								</iframe>
							</div>
						</div>
					</div>
				</div>
			</article>



		</section>
    </div>
	

<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.static', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>