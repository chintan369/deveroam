<?php echo $__env->make('user.search_banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $__env->startSection('content'); ?>
<section>
    <?php echo $__env->make('user.profile_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    <?php echo $__env->make('user.profile_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    <?php if(session()->has('profile_step2_success')): ?>
                        <p class="success-box m-t-30">
                            <?php echo e(session()->get('profile_step2_success')); ?>

                        </p>
                    <?php endif; ?>
                    <?php if(session()->has('profile_step2_error')): ?>
                        <p class="danger-box m-t-30">
                            <?php echo e(session()->get('profile_step2_error')); ?>

                        </p>
                    <?php endif; ?>
                    <div class="m-t-30">
                        <h2 class="profile-title">Physical Address</h2>
                        <form class="form-horizontal m-t-20" method="post" action="<?php echo e(url('profile_step2')); ?>" id="profile_step2_form">
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Address Line One (1)</label>
                                <input type="text" name="phy_address_1" value="<?php echo e($user->customer->phy_address_1); ?>" class="form-control phy_address_1" placeholder="Street Address, P.O Box, Company Name" />
                                <?php if($errors->has('phy_address_1')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_address_1')); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Address Line Two (2)</label>
                                <input type="text" name="phy_address_2" value="<?php echo e($user->customer->phy_address_2); ?>" class="form-control phy_address_2" placeholder="Street Address, P.O Box, Company Name" />
                                <?php if($errors->has('phy_address_2')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_address_2')); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="panel-form-group form-group">
                                <label class="label-control">Country</label>
                                <select id="phy_country" name="phy_country" class="form-control phy_country">
                                    <option value="">Select Country</option>
                                    <?php foreach($all_countries as $country): ?>
                                        <option value="<?php echo e($country['id']); ?>" <?php echo e($user->customer->phy_country == $country['id'] ? 'selected' : ''); ?>><?php echo e($country['name']); ?></option>
                                    <?php endforeach; ?>                                       
                                </select>
                                <?php if($errors->has('phy_country')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_country')); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">State</label>
                                <input type="text" name="phy_state" value="<?php echo e($user->customer->phy_state); ?>" class="form-control phy_state" placeholder="State" />
                                <?php if($errors->has('phy_state')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_state')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">City</label>
                                <input type="text" name="phy_city" value="<?php echo e($user->customer->phy_city); ?>" class="form-control phy_city" placeholder="City" />
                                <?php if($errors->has('phy_city')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_city')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Zip</label>
                                <input type="text" name="phy_zip" value="<?php echo e($user->customer->phy_zip); ?>" class="form-control phy_zip" placeholder="Zip Code" />
                                <?php if($errors->has('phy_zip')): ?> 
                                    <span class="error active"><?php echo e($errors->first('phy_zip')); ?></span>
                                <?php endif; ?>
                            </div>     

                        <h2 class="profile-title">Billing Address</h2>   
                            <br>
                            <div class="form-group blackcheckbox">
                                <p><label class="radio-checkbox label_check m-r-10" for="bill_checkbox">
                                    <input type="checkbox" id="bill_checkbox" value="9" name="bill_checkbox"> Same as Physical Address
                                </label></p>
                            </div>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Address Line One (1)</label>
                                <input type="text" name="bill_address_1" value="<?php echo e($user->customer->bill_address_1); ?>" class="form-control bill_address_1" placeholder="Street Address, P.O Box, Company Name" />
                                <?php if($errors->has('bill_address_1')): ?> 
                                    <span class="error active"><?php echo e($errors->first('bill_address_1')); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Address Line Two (2)</label>
                                <input type="text" name="bill_address_2" value="<?php echo e($user->customer->bill_address_2); ?>" class="form-control bill_address_2" placeholder="Street Address, P.O Box, Company Name" />
                                <?php if($errors->has('bill_address_2')): ?> 
                                    <span class="error active"><?php echo e($errors->first('bill_address_2')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group form-group">
                                <label class="label-control">Country</label>
                                <select id="bill_country" name="bill_country" class="form-control bill_country">
                                    <option value="">Select Country</option>
                                    <?php foreach($all_countries as $country): ?>
                                        <option value="<?php echo e($country['id']); ?>" <?php echo e($user->customer->bill_country == $country['id'] ? 'selected' : ''); ?>><?php echo e($country['name']); ?></option>
                                    <?php endforeach; ?>                                       
                                </select>
                                <?php if($errors->has('bill_country')): ?> 
                                    <span class="error active"><?php echo e($errors->first('bill_country')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">State</label>
                                <input type="text" name="bill_state" value="<?php echo e($user->customer->bill_state); ?>" class="form-control bill_state" placeholder="State" />
                                <?php if($errors->has('bill_state')): ?> 
                                    <span class="error active"><?php echo e($errors->first('bill_state')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">City</label>
                                <input type="text" name="bill_city" value="<?php echo e($user->customer->bill_city); ?>" class="form-control bill_city" placeholder="City" />
                                <?php if($errors->has('bill_city')): ?> 
                                    <span class="error active"><?php echo e($errors->first('bill_city')); ?></span>
                                <?php endif; ?>
                            </div>

                            <div class="panel-form-group input-control form-group">
                                <label class="label-control">Zip</label>
                                <input type="text" name="bill_zip" value="<?php echo e($user->customer->bill_zip); ?>" class="form-control bill_zip" placeholder="Zip Code" />
                                <?php if($errors->has('bill_zip')): ?> 
                                    <span class="error active"><?php echo e($errors->first('bill_zip')); ?></span>
                                <?php endif; ?>
                            </div>                      
                            <div class="m-t-20">
                                <button type="submit" name="" class="btn btn-black btn-block">UPDATE PROFILE</button>
                            </div>
                            <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_leftmenu.js?v='.$version.'')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url( 'assets/js/eroam_js/profile_step2.js?v='.$version.'')); ?>"></script>
<?php echo $__env->make('layouts.profileLayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>