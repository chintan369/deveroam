<?php $__env->startSection('content'); ?>

  <section class="banner-container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo url( 'assets/images/banner/tour-surf-lifestyle.jpg'); ?>" alt="eRoam">
          <div class="banner-caption">
            <div class="banner-inner">
              <form class="form-horizontal clearfix">
                <div class="col-md-12">
                  <div class="text-center">
                    <h1>LIVE THE AUSTRALIAN SURF LIFESTYLE</h1>
                    <p><strong>30 Day Tour</strong></p>
                    <p class="banner-price">$4000.00 </p>
                    <a href="tourDetail/11372/live_the_australian_surf_lifestyle_1_month_learn_to_surf_academy1" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="first-slide" src="<?php echo url( 'assets/images/banner/tour-surf-lifestyle2.jpg'); ?>" alt="eRoam">
          <div class="banner-caption">
            <div class="banner-inner">
              <form class="form-horizontal clearfix">
                <div class="col-md-12">
                  <div class="text-center">
                    <h1>LIVE THE AUSTRALIAN SURF LIFESTYLE</h1>
                    <p><strong>90 Day Tour</strong></p>
                    <p class="banner-price">$7950.00 </p>
                    <a href="tourDetail/11374/live_the_australian_surf_lifestyle_3_month_learn_to_surf_academy1" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="first-slide" src="<?php echo url( 'assets/images/banner/tour-surf-lifestyle3.jpg'); ?>" alt="eRoam">
          <div class="banner-caption">
            <div class="banner-inner">
              <form class="form-horizontal clearfix">
                <div class="col-md-12">
                  <div class="text-center">
                    <h1>2 DAY GREAT BARRIER REEF SNORKEL</h1>
                    <p><strong>2 Day Tour</strong></p>
                    <p class="banner-price">$500.00 </p>
                    <a href="tourDetail/11378/2_day_great_barrier_reef_snorkel_daintree_rainforest_and_aboriginal_culture_tour" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
    </div>
  </section>
<section class="package-wrapper">
      <div class="package-wrapperInner">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="package-container">
                <p>Search from over 500,000 hotels, 1,000 airlines, plus 100,000’s events, tours &amp; restaurants. Select one of the options below.</p>
                <ul class="nav nav-tabs" id="packageTabs" role="tablist"> 
                  <li class="active">
                    <a href="#tours" id="tours-tab" role="tab" data-toggle="tab" aria-controls="tours" aria-expanded="true"><span class="icon icon-activity"></span> Tours / Packages</a>
                  </li> 
                  <li>
                    <a href="#itinerary" role="tab" id="itinerary-tab" data-toggle="tab" aria-controls="itinerary"><span class="icon icon-location"></span> Create Itinerary</a>
                  </li>
                  <li>
                    <a href="#hotel" role="tab" id="hotel-tab" data-toggle="tab" aria-controls="hotel"><span class="icon icon-hotel"></span> Accommodation</a>
                  </li>
                  <li>
                    <a href="#flights" role="tab" id="flights-tab" data-toggle="tab" aria-controls="flights"><span class="icon icon-plane"></span> Flights</a>
                  </li>
                  <li>
                    <a href="#carHire" role="tab" id="carHire-tab" data-toggle="tab" aria-controls="carHire"><span class="icon icon-car"></span> Car Hire</a>
                  </li>
                </ul> 
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="package-wrapperInner m-t-10">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="tab-content" id="packageTabContent"> 
                <div class="tab-pane fade in active" role="tabpanel" id="tours" aria-labelledby="tours-tab">
                  <div>
                    <label class="radio-checkbox label_radio r_on" for="checkbox-03"><input type="radio" name="option1" id="checkbox-03" value="search-box3" checked>Single Day Tour</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a>
                    <label class="radio-checkbox label_radio" for="checkbox-04"><input type="radio" name="option1" id="checkbox-04" value="search-box4">Multi-Day Tour</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#manualModeModal"><i class="fa fa-question-circle"></i></a>
                  </div>
                  <div class="row m-t-20">
                    <div class="col-md-9 col-sm-8">
                      <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                      <div class="panel-form-group">
                        <label class="label-control">Destination</label>
                        <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-4 date-control">
                      <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                      <div class="panel-form-group">
                        <label class="label-control">Departure Date</label>
                        <div class="input-group datepicker">
                          <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                          <span class="input-group-addon"><i class="icon-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="m-t-10"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                  <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH">
                </div>
                <div class="tab-pane fade" role="tabpanel" id="itinerary" aria-labelledby="itinerary-tab">
                  <div>
                    <label class="radio-checkbox label_radio r_on" for="checkbox-05"><input type="radio" id="checkbox-05" name="option2" value="search-box3" checked>Tailor-Made Auto</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a>
                    <label class="radio-checkbox label_radio" for="checkbox-06"><input type="radio" id="checkbox-06" name="option2" value="search-box4">Manual Itinerary</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#manualModeModal"><i class="fa fa-question-circle"></i></a>
                    <label class="radio-checkbox label_radio" for="checkbox-07"><input type="radio" id="checkbox-07" name="option2" value="search-box5">Package Rates</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#packageRateModal"><i class="fa fa-question-circle"></i></a>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="searchMode-container search-box3 mode-block">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="searchMode-container search-box4">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row m-t-20">
                      <div class="col-sm-3 date-control">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                        <div class="panel-form-group">
                          <label class="label-control">Check-In Date</label>
                          <div class="input-group datepicker">
                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-9">
                        <div class="left-spacing">
                          <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Rooms</label>
                                <select class="form-control">
                                  <option>1 Room</option>
                                  <option>2 Room</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Adults (18+)</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Children (0-17)</label>
                                <select class="form-control">
                                  <option>No Children</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="children-block">
                    <div><span class="smile-icon"><i class="icon-child"></i></span>
                      <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                    </div>
                    <div class="row m-t-10">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group">
                          <label class="label-control">Child 1 Age</label>
                          <select class="form-control">
                            <option>1 Years</option>
                            <option>2 Years</option>
                            <option>3 Years</option>
                            <option>4 Years</option>
                            <option>5 Years</option>
                            <option selected>6 Years</option>
                            <option>7 Years</option>
                            <option>8 Years</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group">
                          <label class="label-control">Child 2 Age</label>
                          <select class="form-control">
                            <option>1 Years</option>
                            <option>2 Years</option>
                            <option>3 Years</option>
                            <option>4 Years</option>
                            <option>5 Years</option>
                            <option>6 Years</option>
                            <option>7 Years</option>
                            <option selected>8 Years</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 1</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 2</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <div class="row">
                      <div class="col-sm-4">
                        <p><span class="icon-hotel m-r-10"></span><strong>Room 3</strong></p>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                                <option>1 Children</option>
                                <option selected>2 Children</option>
                                <option>3 Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="left-spacing">
                          <div><span class="smile-icon"><i class="icon-child"></i></span>
                            <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 1 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 2 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <div class="row">
                      <div class="col-sm-4">
                        <p><span class="icon-hotel m-r-10"></span><strong>Room 4</strong></p>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                                <option>1 Children</option>
                                <option>2 Children</option>
                                <option selected>3 Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="left-spacing">
                          <div><span class="smile-icon"><i class="icon-child"></i></span>
                            <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 1 Age</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 2 Age</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 3 Age</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="m-t-20"><label class="radio-checkbox label_check" for="checkbox-08"><input type="checkbox" id="checkbox-08" value="search-box4">I do not need accommodation in the departing location</label></p>
                  <p class="m-t-10"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                  <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH">
                </div> 
                <div class="tab-pane fade" role="tabpanel" id="hotel" aria-labelledby="hotel-tab">
                  <div>
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="panel-form-group input-control">
                      <label class="label-control">Destination</label>
                      <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                    </div>
                    <div class="row m-t-20">
                      <div class="col-sm-4">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                        <div class="row">
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Check-In Date</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Check-Out Date</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Rooms</label>
                              <select class="form-control">
                                <option>1 Room</option>
                                <option>2 Room</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 1</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 2</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <div class="row">
                      <div class="col-sm-4">
                        <p><span class="icon-hotel m-r-10"></span><strong>Room 3</strong></p>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                                <option>1 Children</option>
                                <option selected>2 Children</option>
                                <option>3 Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="left-spacing">
                          <div><span class="smile-icon"><i class="icon-child"></i></span>
                            <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 1 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 2 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <p class="m-t-20"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p> 
                    <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH" />
                  </div>

                </div>
                <div class="tab-pane fade" role="tabpanel" id="flights" aria-labelledby="flights-tab">
                  <div>
                    <label class="radio-checkbox label_radio r_on m-r-20" for="checkbox-09"><input type="radio" id="checkbox-09" name="option3" value="search-box9" checked>Return</label>
                    <label class="radio-checkbox label_radio m-r-20" for="checkbox-10"><input type="radio" id="checkbox-10" name="option3" value="search-box10">One Way</label>
                    <label class="radio-checkbox label_radio m-r-20" for="checkbox-11"><input type="radio" id="checkbox-11" name="option3" value="search-box11">Multi-City</label>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="searchMode-container search-box9 mode-block">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="searchMode-container search-box10">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="searchMode-container search-box11">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row m-t-20">
                      <div class="col-sm-3 date-control">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Dates</strong></p>
                        <div class="panel-form-group">
                          <label class="label-control">Check-In Date</label>
                          <div class="input-group datepicker">
                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-9">
                        <div class="left-spacing">
                          <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Rooms</label>
                                <select class="form-control">
                                  <option>1 Room</option>
                                  <option>2 Room</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Adults (18+)</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Children (0-17)</label>
                                <select class="form-control">
                                  <option>No Children</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="m-t-20">
                      <div><span class="smile-icon"><i class="icon-child"></i></span>
                        <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                      </div>
                      <div class="row m-t-10">
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 1 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option selected>6 Years</option>
                              <option>7 Years</option>
                              <option>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 2 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 3 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 4 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 5 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 6 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="m-t-30 small-btn"> <!-- searchMode-container search-box11 -->
                      <div class="row">
                        <div class="col-sm-8">
                          <p><span class="icon-location m-r-10"></span><strong>Additional Location</strong></p>
                        </div>
                        <div class="col-sm-4">
                          <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Departing</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Destination</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="row">
                            <div class="col-sm-9 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Check-In Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row m-t-10">
                        <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Departing</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Destination</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="row">
                            <div class="col-sm-9 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Check-In Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row m-t-10">
                        <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Departing</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Destination</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="row">
                            <div class="col-sm-9 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Check-In Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <p class="m-t-20"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p> 
                    <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH" />
                  </div>
                </div>
                <div class="tab-pane fade" role="tabpanel" id="carHire" aria-labelledby="carHire-tab">
                  <div>
                    <label class="radio-checkbox label_check" for="checkbox-12"><input type="checkbox" id="checkbox-12" name="option4" value="search-box12">Drop off at a different location</label>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="without-drop">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Pick-Up Location</label>
                            <input type="text" name="" class="form-control" placeholder="City, Suburb or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="with-drop">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row m-t-20">
                      <div class="col-sm-5">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Pick-Up Date</strong></p>
                        <div class="row">
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Pick-Up Date</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Pick-Up Time</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-7">
                        <div class="left-spacing">
                          <p><span class="icon-manage-trip m-r-10"></span><strong>Drop-Off Date</strong></p>
                          <div class="row">
                            <div class="col-sm-4 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Drop-Off Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-4 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Drop-Off Time</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="content-wrapper">
    <h1 class="hide"></h1>
    <article class="places-section eroam-trending">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
           <!--  <h2>TRENDING HOLIDAYS</h2> -->
            <?php 
              $default_selected_city = session()->get( 'default_selected_city' );
                    $city_name = 'Melbourne';
                    if($default_selected_city == 7){
                      $city_name = 'Sydney';
                    }elseif($default_selected_city == 15){
                      $city_name = 'Brisbane';
                    }elseif($default_selected_city == 30){
                      $city_name = 'Melbourne';
                    }
            ?>
            <h2>TOP TOURS DEPARTING FROM <strong><?php echo $city_name;?></strong></h2>
            <div class="places-list">

              <div id="tours-loader">
                <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
              </div>
              <div class="row" id="tourList"></div>
            </div>
          </div>
        </div>
      </div>
    </article>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>


<script>
var countryName   = '<?php echo e($countryName); ?>';
 var default_currency   = '<?php echo e($default_currency); ?>';
    var siteUrl = $( '#site-url' ).val();
	$(document).ready(function(){
         tourList();
        


		$('#onLoadLoginModal').click();

		var siteUrl = $('#site-url').val();
		
		$('input[class="form-control"]').focus(function(){
			$('.notification').html('');
		});

        $("#auth-form").validate({
            rules: {
                firstname: {
                    required: true
                },
                lastname: {
                    required: true
                },
                emailAddress: {
                    required: true,
                    email:true
                },
                company: {
                    required: true
                },
                phone: {
                    number:true
                },


            },

            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {

                authentication();

            }
        });
		//$('#authenticate-btn').click(function(e){
		function authentication() {
            //e.preventDefault();

            var data = $('#auth-form').serializeArray();
            
            if (validateData(data)) {
              
                $.ajax({
                    url: siteUrl + '/validate-auth-credentials',
                    method: 'POST',
                    data: validateData(data),
                    beforeSend: function () {
                        $('.error-message').hide();
                        $('.notification').css({color: 'rgb(0, 0, 0)'}).html('Checking').show();
                    },
                    success: function (response) {
                        if (response.trim() == 1) {
                            window.location.href = siteUrl;
                        }else {
                            $('input[name="username"]').val('');
                            $('input[name="password"]').val('');
                            $('.notification').hide();
                            $('.error-message').css({color: 'red'}).html('Invalid Username or Password').show();
                        }
                        // else {
                        //     //$('input[name="username"]').val('');
                        //    // $('input[name="password"]').val('');
                        //     $('.notification').hide();
                        //     //$('.error-message').css({color: 'red'}).html('Invalid Username or Password').show();
                        // }

                    },
                    error: function () {
                        $('input[name="username"]').val('');
                        $('input[name="password"]').val('');
                        $('.notification').hide();
                        $('.error-message').css({color: 'red'}).html('An error has occured. Please try again.').show();
                    }
                });
            }
            else {
                $('.notification').hide();
                //$('.error-message').css({color: 'red'}).html('Invalid Username or Password').show();
            }
        }
	});

	function validateData( data ) {
		var result = true;
		var values = {};
		try {
			data.forEach(function( d )
			{
				if( typeof d.value !== 'undefined' )
				{
					if( d.value.trim() == '' )
					{
						result = false;
					}
				}
				else
				{
					result = false;
				}
				values[d.name] = d.value;
			});

			if( result === true )
			{
				result = values;
			}
		} 
		catch( err )
		{
			console.log( 'An error has occured while validating the input fields', err );
			result = false;
		}
		return result;
	}
     function tourList(next){
        tour_data = {
          countryName: countryName,
          provider: 'getTours'
        };

        // CACHING HOTEL BEDS
        var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'getTours', true);

        eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
         console.log('tourResponse', tourResponse);
          if( tourResponse.length > 0 ){
            $.each( tourResponse, function( key, value ) {
              //console.log('value : '+ key + value);
              appendTours( value );
            })

          }
        })
      }
    var cnt1 = 1;
      function getStars(count, half = false){
        var stars = '';
        if( parseInt( count ) ){
          for( star = 1; star <= count; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
          }
          var emptyStars = 5 - parseInt( count );
          if(half){
            stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
            emptyStars = emptyStars - 1;
          }
          for( empty = 1; empty <= emptyStars; empty ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }else{
          for( star = 1; star <= 5; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }
         var stars = '';
        return stars;
      }

      function appendTours(tour){
        console.log(tour);
          $("#tours-loader").hide();
          if(cnt1 == 22){return false;}
          var price = 0;
          var land_price = 0;
          if(!tour.flightPrice  || tour.flightPrice < 1){
            tour.flightPrice = 0;
          }
          if(tour.flightDepart != '' && tour.flightDepart != null){
            <?php 
              $default_selected_city = session()->get( 'default_selected_city' );
              $city_name = 'MEL';
              $city_full_name = 'Melbourne';
              if($default_selected_city == 7){
                $city_name = 'SYD';
                $city_full_name = 'Sydney';
              }elseif($default_selected_city == 15){
                $city_name = 'BNE';
                $city_full_name = 'Brisbane';
              }elseif($default_selected_city == 30){
                $city_name = 'MEL';
                $city_full_name = 'Melbourne';
              }
            ?>
            tour.flightDepart = '<?php echo $city_name;?>';
            var city_full_name = '<?php echo $city_full_name;?>';
          }
          if(tour.price){
            price = parseFloat(parseFloat(tour.price) + parseFloat(tour.flightPrice)).toFixed(2);
            land_price = parseFloat(tour.price).toFixed(2);
          }
          
          var star;
          var rating = tour.rating;

          if(rating % 1 === 0){
              stars = getStars( rating);
          }else {
              stars = getStars( rating, true );
          }

        var imgurl = 'http://www.adventuretravel.com.au'+tour.folder_path+'245x169/'+tour.thumb;
        var imgurl2 = 'http://dev.cms.eroam.com/'+tour.thumb;

        $("#overlay").hide();
          if(parseInt(tour.no_of_days) == 1) {
              var day = 'Day';
          }else{
              var day = 'Days';
          }
          var str1 = tour.short_description;
          if(str1.length > 120) str1 = str1.substring(0,120);
          
          var total_duration = ''; 
          var total_duration1 = ''; 
          if(tour.durationType == 'd'){
            total_duration = 'DAY';
            total_duration1 = 'Day';
            if(Math.ceil(tour.no_of_days) > 1){
              total_duration = 'DAYS';
              total_duration1 = 'Days';
            }
          }else if(tour.durationType == 'h'){
            total_duration = 'HOUR';
            total_duration1 = 'Hour';
            if(Math.ceil(tour.no_of_days) > 1){
              total_duration = 'HOURS';
              total_duration1 = 'Hours';
            }
          } 
          if(tour.start_date){
            tour.start_date = formatDate(new Date(tour.start_date),"start");
          }else{
            tour.start_date = '';
          }
          if(tour.end_date){
            tour.end_date = formatDate(new Date(tour.end_date),"end");
          }else{
            tour.end_date = '';
          }
          if(!tour.discount || tour.discount == '.00'){
            tour.discount = 0;
          }
          if(!tour.saving_per_person || tour.saving_per_person == '.00'){
            tour.saving_per_person = '0.00';
          }
          if(!tour.retailcost){
            tour.retailcost = '0.00';
          }
          
          var html = '<div class="col-md-4 col-sm-6 m-t-20"><div class="flip-container" ontouchstart="this.classList.toggle(\'hover\');"><div class="flipper"><div class="front">'+
              '<div class="front-image"><img src="'+imgurl+'" alt="" class="img-responsive" id="tourImage_'+tour.tour_id+'"/>';
              if(tour.flightPrice != 0){
                html +='<div class="flight-included"><span class="flight-icon"><i class="fa fa-plane"></i></span><span class="flight-text">Flight Included</span></div>';
              }
              html +='</div>'+

              '<div class="journey-details"><h4>'+tour.tour_title+'</h4>' +
              '<p class="hidden-content text-center">'+str1+' </p>'+
              '<div class="row m-t-20"><div class="col-xs-4"><div class="row border-right-black"><div class="col-sm-5 col-xs-6 way-text">TOTAL<br>'+total_duration1+''+
              '</div><div class="col-sm-6 col-xs-6 price-text">'+ tour.no_of_days+'</div></div></div>'+
              '<div class="col-xs-8"><div class="row"><div class="col-sm-4 col-xs-6 way-text">FROM P.P</div><div class="col-sm-7 col-xs-6 price-text">$'+price+'<sup>*</sup>'+
              
              '</div></div></div></div></div></div>'+
              '<div class="back">'+
              '<div class="back-title">'+tour.tour_title+'</div>'+
              '<p class="hidden-content">'+str1+'</p>'+
              '<div class="row"><div class="col-xs-6"><p class="orange-text">Travel Style</p></div>'+
              '<div class="col-xs-6">'+
              '<p>Small Group Tours</p>'+
              '</div>'+
              '</div>'+
              '<div class="row">'+
              '   <div class="col-xs-6">'+
              '   <p class="orange-text">Trip Length</p>'+
              '</div>'+
              '<div class="col-xs-6">'+
              ' <p>' + tour.no_of_days+' '+total_duration1 +'</p>'+
              '</div>'+
              '</div>'+
              ' <div class="row">'+
              '<div class="col-xs-6">'+
              '<p class="orange-text">Tour Dates</p>'+
              '</div>'+
              '<div class="col-xs-6">'+
              ' <p>'+tour.start_date+' - '+tour.end_date+'</p>'+
              '</div>'+
              '</div>'+
              '<div class="row">'+
              '<div class="col-xs-6">'+
              '<p class="orange-text">Cost</p>'+
              '</div>'+
              ' <div class="col-xs-6">'+
              ' <p>$'+price+' p.p twin share</p>'+
              '</div>'+
              '</div>'+
              '<div class="row">'+
              '<div class="col-xs-6">'+
              '<p class="orange-text">Start Finish</p>'+
              '</div>'+
              '<div class="col-xs-6">'+
              '<p>'+tour.departure+' - '+tour.destination+'</p>'+
              '</div>'+
              '</div>'+
              '<div class="row m-t-20">';
              if(tour.discount && tour.discount != '.00'){
              html +='<div class="col-sm-6">'+
              ' <div class="row">'+
              ' <div class="col-sm-6 col-xs-6 way-text">'+
              'eRoam<br/>Discount'+
              '  </div>'+
              '   <div class="col-sm-6 col-xs-6 price-text">'+
              '    '+tour.discount+'%'+
              '    </div>'+
              '   </div>'+
              '   </div>';
              }
              if(tour.saving_per_person && tour.saving_per_person != '.00'){  
              html +='<div class="col-sm-6">'+
                '<div class="row">'+
                '<div class="col-sm-5 col-xs-6 way-text">'+
                'Savings From P.P '+
                '</div>'+
                '<div class="col-sm-6 col-xs-6 price-text"> $'+tour.saving_per_person+'<sup>*</sup>'+
                '</div>'+
                '</div>'+
                '</div>';
              }
             html +='</div><p class="note-text m-t-10">*<em>From price, discount and savings per person is based on the total price per adult in a twin share room, subject to departure dates.</em></p>'+
              '<button type="button" onclick="window.location.href=\'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'" class="btn btn-primary btn-block m-t-20">EXPLORE FURTHER</button>'+
              '</div>'+
              '</div>'+
              '</div>'+
              '</div>';

          var html = '<div class="col-md-4 col-sm-6 m-t-20" onclick="window.location.href=\'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'">'+
              '<div class="flip-container" ontouchstart="this.classList.toggle(\'hover\');">'+
              '<div class="flipper">'+
              '<div class="front">'+
              '<div class="front-image"><img src="'+imgurl+'" alt="" class="img-responsive" id="tourImage_'+tour.tour_id+'"/>';
              if(tour.flightPrice != 0){
                html +='<div class="flight-included"><span class="flight-icon"><i class="fa fa-plane"></i></span><span class="flight-text">Flight Included</span></div>';
              }
              html +='</div>'+
              '<div class="journey-details">'+
          '<h4>'+tour.tour_title+'</h4>'+
          '<div class="m-t-20 border-box">'+
          '<div class="row">'+
          '<div class="col-xs-4">'+
          '<div class="row border-right-gray">'+
          '<div class="col-sm-5 col-xs-6 way-text">TOTAL<br>'+total_duration+'</div><div class="col-sm-6 col-xs-6 price-text">'+ tour.no_of_days+''+
          '</div></div></div>'+
          '<div class="col-xs-8"><div class="row"><div class="col-sm-5 col-xs-6 way-text">eRoam<br>Cost P.P</div>'+
          '<div class="col-sm-6 col-xs-6 price-text">$'+price+'<sup>*</sup></div></div></div></div>';
          if(tour.flightPrice > 0 && (tour.flightDepart != '' && tour.flightDepart != null )){
            html +='<p class="flight-note">FLIGHTS INCLUSIVE DEPARTING '+tour.flightDepart+'</p>';
          }
          html +='</div>'+
          '<p class="hidden-content text-center">'+str1+'</p>'+
          '<div class="row m-t-40"><div class="col-sm-5 red-text"><div class="row border-right-gray"><div class="col-sm-6 col-xs-6 way-text">'+
          'eRoam<br/>Discount</div><div class="col-sm-6 col-xs-6 price-text"> '+tour.discount+'%</div></div></div>'+
          '<div class="col-sm-7 yellow-text"><div class="row"><div class="col-sm-5 col-xs-6 way-text">Savings From P.P</div>'+
          '<div class="col-sm-6 col-xs-6 price-text">$'+tour.saving_per_person+'<sup>*</sup></div></div></div></div></div></div>'+
          '<div class="back"><div class="back-title">'+tour.tour_title+'</div>'+
          '<p class="hidden-content">'+str1+'</p>'+
          '<div class="row m-t-20"><div class="col-sm-6 col-xs-5"><p class="orange-text">Travel Style</p></div>'+
          '<div class="col-sm-6 col-xs-7"><p>Small Group Tours</p></div></div>'+
          '<div class="row"><div class="col-sm-6 col-xs-5"><p class="orange-text">Trip Length</p></div>'+
          '<div class="col-sm-6 col-xs-7"><p>' + tour.no_of_days+' '+total_duration1 +'</p></div></div>'+
          '<div class="row"><div class="col-sm-6 col-xs-5"><p class="orange-text">Tour Dates</p></div>'+
          '<div class="col-sm-6 col-xs-7"><p>'+tour.start_date+' - '+tour.end_date+'</p></div></div>';
          var land_only = '';
          if(tour.flightPrice != 0){
            land_only = '(Land Only)';
            html +='<div class="row"><div class="col-sm-6 col-xs-5"><p class="orange-text">eRoam Cost (Land Only)</p></div>'+
            '<div class="col-sm-6 col-xs-7"><p>$'+land_price+' Per Person</p></div></div>';
            
          }
          html +='<div class="row"><div class="col-sm-6 col-xs-5"><p class="orange-text">Normal Cost '+land_only+'</p></div>'+
          '<div class="col-sm-6 col-xs-7"><p>$'+tour.retailcost+' Per Person </p></div></div>';
           var flightIncl = '';
           var flightCity = '';
           if(tour.flightPrice != 0){
              flightIncl = '(Flight Incl) ';
              flightCity = ' [ex '+city_full_name+']';
            }
          html +='<div class="row"><div class="col-sm-6 col-xs-5"><p class="orange-text">eRoam Cost '+flightIncl+'</p></div>'+
          '<div class="col-sm-6 col-xs-7"><p>$'+price+' Per Person'+flightCity+'</p></div></div>'+
          '<div class="row"><div class="col-sm-6 col-xs-5"><p class="orange-text">Start Finish</p></div>'+
          '<div class="col-sm-6 col-xs-7"><p>'+tour.departure+' - '+tour.destination+'</p></div></div>'+
          '<div class="row m-t-20"><div class="col-sm-5 red-text">'+
          '<div class="row border-right-gray"><div class="col-sm-6 col-xs-6 way-text">eRoam<br/>Discount</div>'+
          '<div class="col-sm-6 col-xs-6 price-text">'+tour.discount+'%</div></div></div>'+
          '<div class="col-sm-7 yellow-text"><div class="row"><div class="col-sm-5 col-xs-6 way-text">Savings From P.P</div>'+
          '<div class="col-sm-6 col-xs-6 price-text">$'+tour.saving_per_person+'<sup>*</sup></div></div></div></div>'+
          '<p class="note-text m-t-30"><em>*From price, discount and savings per person is based on the total price per adult in a twin share room, subject to departure dates</em></p>'+
          '<button type="button" onclick="window.location.href=\'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'" class="btn btn-primary btn-block m-t-20">EXPLORE FURTHER</button>'+
          '</div></div></div></div>';    

          $('#tourList').append(html);
          cnt1 = parseInt(cnt1) + 1;
          //imageUrl = checkImageUrl(tour.tour_id, imgurl);
      }
      // function checkImageUrl(id, url){
      //   eroam.ajax('post', 'existsImage', {url : url}, function(response){
      //     if(response == 200){
      //       //var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
      //       //$("#tourImage_"+id).attr('src', image);
      //     } else if(response == 400){
      //       //var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
      //       //$("#tourImage_"+id).attr('src', image);
      //     } else {
      //         var image = "<?php echo e(url( 'assets/images/no-image.jpg' )); ?>";
      //         $("#tourImage_"+id).attr('src', image);
      //     }
      //   });  
      // }
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.home2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>