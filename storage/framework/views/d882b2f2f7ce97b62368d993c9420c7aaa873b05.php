<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		 <title>:: eRoam ::</title>

	    <link href="<?php echo e(url( 'assets/css/bootstrap.min.css' )); ?>" rel="stylesheet">
	    <link href="<?php echo e(url( 'assets/css/font-awesome.css' )); ?>" rel="stylesheet">
	    <link href="<?php echo e(url( 'assets/css/materialize.css' )); ?>" rel="stylesheet">
		<link href="<?php echo e(url( 'assets/css/datepicker.css' )); ?>" rel="stylesheet">
	    <link href="<?php echo e(url( 'assets/css/main.css' )); ?>" rel="stylesheet">
	    <link href="<?php echo e(url( 'assets/css/media.css' )); ?>" rel="stylesheet">
		<link href="<?php echo e(url( 'assets/css/prettify.css' )); ?>" rel="stylesheet">
		<link href="<?php echo e(url( 'assets/css/jslider.css' )); ?>" rel="stylesheet">

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	    <![endif]-->
		

		<?php echo $__env->yieldContent( 'custom-css' ); ?>
	</head>

	<body class="has-js">
	<div class="loader" style="display:none;"><img src="<?php echo e(url( 'assets/images/loader.gif' )); ?>" alt="Loader" /></div>		
<?php $currentPath= Route::getFacadeRoot()->current()->uri();
	
?>
		<header>
	      <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container-fluid">
	          <div class="row">
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <div class="navbar-header">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                  <span class="sr-only">Toggle navigation</span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="/"><img src="<?php echo e(url( 'assets/images/logo.png' )); ?>" alt="eroam" class="img-responsive"></a>

	              </div>
	            </div>
	            <div class="col-md-10 col-sm-10 col-xs-12 padding-left-0">
	              <div id="navbar" class="navbar-collapse collapse">
	              
	                <ul class="nav navbar-nav navbar-right">
	                  <li><a href="#"><i class="fa fa-heart-o"></i></a></li>	
	                 

						<?php if(session()->has('user_auth')): ?>
							<li <?php if($currentPath == 'profile'): ?>class="active"<?php endif; ?>>
								<a href="#" id="account-dropdown" class="account-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span id="round-profile-picture" style="background-image: url(<?php echo e(session()->get('user_auth')['image_url'] ? session()->get('user_auth')['image_url'] : url('assets/img/default-profile-image.jpg')); ?> )"></span> <?php echo e(session()->get('user_auth')['name']); ?> <i class="fa fa-caret-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo e(url('profile')); ?>"><span class="flaticon-profile"></span> Profile</a></li>
									<li><a href="<?php echo e(url('logout')); ?>"><span class="flaticon-logout"></span> Log Out</a></li>
								</ul>
							</li>
						<?php else: ?>
	                        <li <?php if($currentPath == 'login' || $currentPath == 'register'): ?>class="active"<?php endif; ?>><a href="#" data-toggle="modal" data-target="#loginModal">Sign In</a></li>
             		 		<li><a href="#" data-toggle="modal" data-target="#registerModal">Sign Up</a></li>
						<?php endif; ?>
							  <li <?php if($currentPath == 'about-us'): ?>class="active"<?php endif; ?>><a href="/about-us">About Us</a></li>
	                  			<li <?php if($currentPath == 'contact-us'): ?>class="active"<?php endif; ?>><a href="/contact-us">Contact Us</a></li>
							 <li class="dropdown">
							 	<?php 
										$default_selected_city = session()->get( 'default_selected_city' );
										$city_name = 'Melbourne, AU';
										if($default_selected_city == 7){
											$city_name = 'Sydney, AU';
										}elseif($default_selected_city == 15){
											$city_name = 'Brisbane, AU';
										}elseif($default_selected_city == 30){
											$city_name = 'Melbourne, AU';
										}
								?>
		                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $city_name;?><span class="caret"></span></a>
		                    <ul class="dropdown-menu">
		                       <li><a class="city_drop_down" href="javascript://" data-id="30">Melbourne, AU</a></li>
			                    <li><a class="city_drop_down" href="javascript://" data-id="7">Sydney, AU</a></li>
			                    <li><a class="city_drop_down" href="javascript://" data-id="15">Brisbane, AU</a></li>
		                    </ul>
		                  </li>
		                  <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">$AUD <span class="icon-arrow-bottom header_ic"></span></a>
		                    
		                  </li>
	                </ul>
	              </div>
	            </div>
	         </div>
	       </div>
	      </nav>
	    </header>		