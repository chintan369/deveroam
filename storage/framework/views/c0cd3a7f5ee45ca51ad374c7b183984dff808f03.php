<?php $__env->startSection('custom-css'); ?>
<style>
	.wrapper {
		background-image: url( <?php echo e(url( 'assets/img/bg1.jpg' )); ?> ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

	#sitemap p{
		line-height: 1.3em;
		text-align: justify;
	}
	.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	

	
    <div class="top-section-image">
		<img src="<?php echo e(asset('assets/images/bg-image.jpg')); ?>" alt="" class="img-responsive">
	</div>

	<section class="content-wrapper">
		<h1 class="hide"></h1>
		<article class="about-section">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<h2>eRoam Sitemap</h2>
						<div class="m-t-30 row">
							<div class="col-sm-6">
								<p><strong>Company</strong></p>
								<ul class="simple-listing">
									<li><a href="#">About us</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Terms &amp; Conditions</a></li>
								</ul>
							</div>
							<div class="col-sm-6">
								<p><strong>Contact</strong></p>
								<ul class="simple-listing">
									<li><a href="#">Contact Form</a></li>
								</ul>
								<p class="m-t-20"><strong>Customer</strong></p>
								<ul class="simple-listing">
									<li><a href="#">Register</a></li>
									<li><a href="#">Login</a></li>
									<li><a href="#">Create Itinerary</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</section>



<?php $__env->stopSection(); ?>

<?php $__env->startSection( 'custom-js' ); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.static', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>