@extends( 'layouts.static' )

@section('custom-css')
<style>
	.flaticon:before {
		font-size: 15rem;
	}
</style>
@stop
@section('content')
	<div class="container" style="min-height: 500px;">
		<div class="row">
			<div class="text-center white-box col-md-6 col-md-offset-3" style="padding-top: 15%;">
				<i class="flaticon flaticon-island"></i>
				<h1> {{ $exception->getMessage() }}</h1>
				<h4><a href="{{ url( '/' ) }}"><i class="fa fa-long-arrow-left"></i> Return to homepage</a></h4>
				<br>
			</div>
		</div>
	</div>
@stop