@extends( 'layouts.static' )

@section('custom-css')
<style>
	.flaticon:before {
		font-size: 15rem;
	}
</style>
@stop
@section('content')
	<!--<div class="container" style="min-height: 500px;">
		<div class="row">
			<div class="text-center white-box col-md-6 col-md-offset-3" style="padding-top: 15%;">
				<i class="flaticon flaticon-island"></i>
				<h1> {{ $exception->getMessage() }}</h1>
				<h4><a href="{{ url( '/' ) }}"><i class="fa fa-long-arrow-left"></i> Return to homepage</a></h4>
				<br>
			</div>
		</div>
	</div>-->
	<div class="error-wrapper">
		<div class="container error-container">
			<div class="text-center error-box">
				<div class="error-inner">
					<img src="images/logo-big.png" alt="" class="img-responsive error-logo" />
					<h1 class="error-title"><span>4</span><span class="letter-second">0</span><span>4</span></h1>
					<h2>Oops!</h2>
					<p>we can't seem to find the page you're looking for.</p>
					<p class="m-t-20">Go to <a href="/">Home</a></p>
				</div>
			</div>
		</div>
	</div>
@stop