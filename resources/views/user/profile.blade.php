@extends('layouts.profileLayout')
@section('custom-css')
<style type="text/css">
.error-msg {
    display: none;
    border-radius: 2px;
    padding: 5px 11px;
    text-align: center;
    margin-bottom: 2.5rem;
    color: red;
  }
  #upload-image-btn:disabled{
      border: 1px solid #E0E0E0;
      background-color: #E0E0E0;
      color: #212121;
      outline: none;
  }

</style>
@stop
@section('content')

<section class="inner-page content-wrapper">
      <h1 class="hide"></h1>
      <div class="profile-wrapper clearfix">
        <div class="profile-left-container">
          <button type="button" id="update-profile-picture-btn" class="btn btn-primary btn-block m-t-20 profile-btn text-center" data-toggle="modal" data-target="#profile-picture-modal"><i class="fa fa-pencil-square-o"></i> Update Profile Picture</button>
          
          <hr class="m-t-20">
          <div class="location-inner m-t-20">
            <div class="loc-icon">
              <i class="fa fa-user"></i>
            </div>
            <div class="location-content">
              <h4 class="loc-title">Profile</h4>
              <a class="btn btn-primary btn-block m-t-20" href="{{ url('/') }}">Create new itinerary</a>
              <a class="btn btn-primary btn-block m-t-10" href="{{ url('logout') }}">Log Out</a>
            </div>
          </div>
        </div>
        <div class="profile-container">
          <div class="tabs-container">
            <h4>{{ $user->name }}</h4>
            <div class="custom-tabs m-t-10" data-example-id="togglable-tabs"> 
                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                  <li role="presentation" class="active"><a href="#profile" id="profile-tab" role="tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">Profile</a>
                  </li> 
                  <li role="presentation"><a href="#bookings" id="bookings-tab" role="tab" data-toggle="tab" aria-controls="bookings" aria-expanded="true">Bookings</a>
                  </li>
                  <li role="presentation"><a href="#settings" id="settings-tab" role="tab" data-toggle="tab" aria-controls="settings" aria-expanded="true">Settings</a>
                  </li>
                </ul> 
            </div>
          </div>
          <div class="tabs-content-container">
              <div class="tab-content" id="myTabContent"> 
                <div class="tab-pane fade in active clearfix" role="tabpanel" id="profile" aria-labelledby="profile-tab">
                  <form id="travel-preferences-form" class="form-horizontal" method="post" action="{{ url('profile/update-travel-preferences') }}">
                    @if (session()->has('success'))
                      <div class="col-md-12">
                        <p class="success-box">{{ session()->get('success') }}</p>
                      </div>
                    @endif
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-4"><p class="m-t-10"><strong>Travel Preferences</strong></p></div>
                        <div class="col-md-8">
                          <button type="button" class="btn btn-primary profile-btn" id="save-travel-preferences-btn">Save Travel Preferences</button>
                          
                        </div>
                      </div>
                      <div class="m-t-30 row black-box">
                        <div class="col-md-4">
                          <label class="">Accomodation Type</label>
                        </div>
                        <?php
                         // $travellers['transport_types'] = [];
                          // $travellers['transport_types'][0]['id'] =1;
                          //  $travellers['transport_types'][0]['name'] = 'Air (Flights)';
                          //  $travellers['transport_types'][1]['id'] = 2;
                          //  $travellers['transport_types'][1]['name'] = 'Land (All other modes)';
                          $travel_preferences['categories'] = [];
                          $travel_preferences['categories'][0]['id'] =9;
                          $travel_preferences['categories'][0]['name'] = '5 Star (Luxury)';
                          $travel_preferences['categories'][1]['id'] =3;
                          $travel_preferences['categories'][1]['name'] = '4 Star (Deluxe)';
                          $travel_preferences['categories'][2]['id'] = 2;
                          $travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
                          $travel_preferences['categories'][3]['id'] = 5;
                          $travel_preferences['categories'][3]['name'] = '2 Star (Backpacker / Guesthouse)';
                          $travel_preferences['categories'][4]['id'] = 1;
                          $travel_preferences['categories'][4]['name'] = 'Camping';
                                          ?>  
                        <div class="col-md-8">
                          @foreach ($travel_preferences['categories'] as $category)
                          <p>
                            <label class="radio-checkbox label_check m-r-10" for="checkbox-cat-{{ $category['id'] }}">
                              <input type="checkbox" id="checkbox-cat-{{ $category['id'] }}" value="{{ $category['id'] }}" name="hotel_categories[]" {{ $user->customer->pref_hotel_categories && in_array($category['id'], explode(',', $user->customer->pref_hotel_categories)) ? 'checked' : '' }}>
                                  {{ $category['name'] }}
                            </label>
                          </p>
                          @endforeach
                        </div>
                      </div>
                      <!-- <div class="m-t-20 row black-box">
                        <div class="col-md-4">
                          <label class="">Room Type</label>
                        </div>
                        <div class="col-md-8">
                          @foreach ($travel_preferences['room_types'] as $room_type)
                           <p>
                            <label class="radio-checkbox label_check m-r-10" for="checkbox-room-{{ $room_type['id'] }}">
                              <input type="checkbox" id="checkbox-room-{{ $room_type['id'] }}" value="{{ $room_type['id'] }}" name="hotel_room_types[]" {{ $user->customer->pref_hotel_room_types && in_array($room_type['id'], explode(',', $user->customer->pref_hotel_room_types)) ? 'checked' : '' }}>
                                  {{ $room_type['name'] }}
                            </label>
                          </p>
                          @endforeach
                         
                          
                        </div>
                      </div> -->
                     
                      <div class="m-t-20 row black-box">
                        <div class="col-md-4">
                          <label class="">Transport Type</label>
                        </div>
                        <div class="col-md-8">
                           @foreach ($travel_preferences['transport_types'] as $transport_type)
                          <p>
                            <label class="radio-checkbox label_check m-r-10" for="checkbox-transport-{{ $transport_type['id'] }}">
                              <input type="checkbox" id="checkbox-transport-{{ $transport_type['id'] }}" value="{{ $transport_type['id'] }}" name="transport_types[]" {{ $user->customer->pref_transport_types && in_array($transport_type['id'], explode(',', $user->customer->pref_transport_types)) ? 'checked' : '' }}>
                                  {{ $transport_type['name'] }}
                            </label>
                          </p>
                          @endforeach
                        </div>
                      </div>
                      <div class="m-t-20 row black-box">
                        <div class="col-md-4 select-label-new">
                          <label class="control-label">Nationality</label>
                        </div>
                        <div class="col-md-8">
                         <div class="select-box">
                          <select id="nationality" name="nationality" class="form-control">
                              @foreach ($travel_preferences['nationalities']['featured'] as $nationality)
                                <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                              @endforeach
                              <option disabled> ======================================== </option>
                              @foreach ($travel_preferences['nationalities']['not_featured'] as $nationality)
                                <option value="{{ $nationality['id'] }}" {{ $user->customer->pref_nationality_id == $nationality['id'] ? 'selected' : '' }}>{{ $nationality['name'] }}</option>
                              @endforeach
                            </select>
                        </div>
                        </div>
                      </div>
                      <div class="m-t-20 row black-box">
                        <div class="col-md-4 select-label-new">
                          <label class="control-label">Gender</label>
                        </div>
                        <div class="col-md-8">
                         <div class="select-box">
                          <select id="gender" name="gender" class="form-control">
                              <option value="male" {{ $user->customer->pref_gender == 'male' ? 'selected' : '' }}>Male</option>
                              <option value="female" {{ $user->customer->pref_gender == 'female' ? 'selected' : '' }}>Female</option>
                              <option value="other" {{ $user->customer->pref_gender == 'other' ? 'selected' : '' }}>Other</option>
                            </select>
                        </div>
                        </div>
                      </div>
                      <div class="m-t-20 row black-box">
                        <div class="col-md-4 select-label-new">
                          <label class="control-label">Age Group</label>
                        </div>
                        <div class="col-md-8">
                         <div class="select-box">
                          <select id="age-group" name="age_group" class="form-control">
                              @foreach ($travel_preferences['age_groups'] as $age_group)
                                <option value="{{ $age_group['id'] }}" {{ $user->customer->pref_age_group_id == $age_group['id'] ? 'selected' : '' }}>{{ $age_group['name'] }}</option>
                              @endforeach
                            </select>
                        </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label class="interest-text">Interests:</label>
                      <div class="m-t-20">
                        <ul class="interest-list">
                          @foreach ($labels as $label)
                          <li data-id="{{ $label['id'] }}">
                            <div class="ck-button">
                               <label>
                                  <input type="checkbox" value="{{ $label['id'] }}" class="interest-checkbox" name="interests[]" {{ $user->customer->interests && in_array($label['id'], explode(',', $user->customer->interests)) ? 'checked' : '' }}>
                                  <a href="#" class="{{ $user->customer->interests && in_array($label['id'], explode(',', $user->customer->interests)) ? 'selected' : '' }}">{{ $label['name'] }}</a>
                               </label>
                            </div>
                          </li>
                          @endforeach
                         
                        </ul>
                      </div>
                    </div>
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </form>
                </div>
                <div class="tab-pane fade" role="tabpanel" id="bookings" aria-labelledby="bookings-tab"> 
                  <h4>BOOKING HISTORY 
                  <span class="itinerary-view-style">
                    <a href="#" data-type="grid" title="Grid View">
                      <i class="fa fa-th-large"></i>
                    </a> 
                    <a href="#" data-type="list" title="List View">
                      <i class="fa fa-list"></i>
                    </a>
                  </span>
                </h4>
                  <br>
                  <h4>No bookings yet.</h4>
                </div> 
                <div class="tab-pane fade" role="tabpanel" id="settings" aria-labelledby="settings-tab" id="settings"> 
                   <h4>General Settings</h4>
                  <div class="row m-t-20">
                    <div class="col-md-12">
                       <p class="success-box" style="display: none"></p>
                      </div>
                    <div class="col-md-8">
                      <div class="row">
                        <div class="col-md-4 col-sm-5">Name</div>
                        <div class="col-md-8 col-sm-7">{{ $user->name }}</div>
                      </div>
                      <div class="row m-t-20">
                        <div class="col-md-4 col-sm-5">Email</div>
                        <div class="col-md-8 col-sm-7">{{ $user->customer->email }}</div>
                      </div>
                      <div class="row">
                        <div class="col-md-4 col-sm-5 text-currency">Currency</div>
                        <div class="col-md-8 col-sm-7">
                          <div class="select-box m-t-20">
                            <select class="form-control" id="select-currency">
                              @foreach (session()->get('all_currencies') as $currency)
                                <option value="{{ $currency['code'] }}" {{ $currency['code'] == $user->customer->currency ? 'selected' : '' }}>({{ $currency['code'] }}) {{ $currency['name'] }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row m-t-20">
                        <div class="col-md-4 col-sm-5 m-t-10">Password</div>
                        <div class="col-md-8 col-sm-7">
                          <button type="submit" class="btn btn-primary btn-block profile-btn" data-toggle="modal" data-target="#change-password-modal">Change Password</button>
                        </div>
                      </div>
                    </div> 
                </div> 
              </div> 
            </div>
        </div>
      </div>
    </section>


{{-- MODALS --}}
  <div id="profile-picture-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Update Profile Picture</h4>
        </div>
        <div class="modal-body">
          <form enctype="multipart/form-data" id="image-form">
           <!--  <div class="file-upload">
              <input type="file" name="profile_picture" id="upload-image-input" />
              <input type="hidden" name="user_id" value="{{ $user->id }}">
              <a href="#" id="choose-image-btn" class="profile-btn"><i class="fa fa-image"></i> Choose an image</a>
            </div> -->
            <div class="upload-btn m-t-20" style="width: 32%">
              <i class="fa fa-image"></i> Choose an image
             <input type="file" class="hide_file" name="profile_picture" id="upload-image-input">
             <input type="hidden" name="user_id" value="{{ $user->id }}">
          </div>
          <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <img id="preview-upload-image" class="img-responsive">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <span id="upload-error-msg" class="error-msg">You must select an image file only.</span>
          <button id="upload-image-btn" class="profile-btn btn btn-primary" style="display:none"><i class="fa fa-upload"></i> Upload</button>
          <button type="button" class="btn btn-default btn-primary profile-btn" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div id="change-password-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document" style="width: 450px; margin: 100px auto;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group input-field login-group">
              <input type="password" class="form-control" id="old-password">
              <label for="old-password">Old Password</label>
            </div>
            <div class="form-group input-field login-group">
              <input type="password" class="form-control" id="new-password">
              <label for="new-password">New Password</label>
            </div>
            <div class="form-group input-field login-group">
              <input type="password" class="form-control" id="confirm-new-password">
              <label for="confirm-new-password">Confirm New Password</label>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <span class="error-msg" id="change-password-error"></span>
          <button id="change-password-btn" data-user-id="{{ $user->id }}" class="btn btn-primary profile-btn">Save Changes</button>
          <button type="button" class="btn btn-primary btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


 @stop
 @section('custom-js')
<script type="text/javascript">
  // Profilee Module
  var Profile = (function() {

  function init() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  function selectInterest(event) {
    event.preventDefault();
    var checkbox = $(this).prev('.interest-checkbox');
    var notChecked = checkbox.prop('checked');
    checkbox.trigger('click');
    if (notChecked == false) {
      $(this).addClass('selected');
    } else {
      $(this).removeClass('selected');
    }
  }

  function readFile(input, preview) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        preview.attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  function uploadImage(event) {
    event.preventDefault();
    var uploadBtn = $(this);
    var apiUrl  = $('#api-url').val();
    var form = $('#image-form')[0]; // You need to use standart javascript object here
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($('#upload-image-input').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      $('#upload-error-msg').css('display', 'block');
      return;
    }
    var formData = new FormData(form);

    $.ajax({
      url: apiUrl + 'user/update-profile-picture',
      data: formData,
      type: 'POST',
      headers: {
        'X-Authorization': '7b94b3635849f1da030358d5724c72c1f320ca5c',
      },
      contentType: false,
      processData: false,
      success: function(response) {
        if (response) {
          window.location.reload();
        }
      },
      beforeSend: function() {
        uploadBtn.html('<i class="fa fa-circle-o-notch fa-spin"></i> Uploading...please wait.').prop('disabled', true);
      }
    });
  }

  function changePassword(event) {
    event.preventDefault();
    var id = $(this).data('user-id');
    var oldPassword = $('#old-password');
    var newPassword = $('#new-password');
    var confirmNewPassword = $('#confirm-new-password');

    if (oldPassword.val().trim() == '' || newPassword.val().trim() == '' || confirmNewPassword.val().trim() == '') {
      $('#change-password-error').text('All fields are required.').css('display', 'block');
    } else if (newPassword.val().length < 5) {
      $('#change-password-error').text('Password must be at least 6 characters in length.').css('display', 'block');
    } else if (newPassword.val() != confirmNewPassword.val()) {
      $('#change-password-error').text('New password does not match').css('display', 'block');
    } else {
      $('#change-password-error').css('display', 'none');

      var data = {
        user_id: id,
        old_password: oldPassword.val(),
        new_password: newPassword.val()
      };
      console.log('testing ajax');
      eroam.api('post', 'user/change-password', data, function(response) {
        if (response == 'success') {
          oldPassword.val('');
          newPassword.val('');
          confirmNewPassword.val('');
          $('#change-password-modal').modal('hide');
          $('#settings .success-box').text('Password Successfully Updated!').css('display', 'block');
        } else {
          $('#change-password-error').text('Incorrect password. Please try again.').css('display', 'block');
        }
      });
    }
  }

  function updateCurrency() {
    var selectedCurrency = $('#select-currency').val();
    if (selectedCurrency != '') {
      var customerId = '{{ $user->customer->id }}';
      eroam.api('post', 'user/update-currency', {customer_id: customerId, currency: selectedCurrency}, function() {
        $('#select-currency').effect('highlight', {color: '#A9FDC1'}, 700);
      });
    }
  }

  return {
    init: init,
    selectInterest: selectInterest,
    uploadImage: uploadImage,
    readFile: readFile,
    changePassword: changePassword,
    updateCurrency: updateCurrency,
  };

  })();

  $(document).ready(function() {
    // Select interest
    $('.interest-list li a ').click(Profile.selectInterest);

    // Save Travel Preferences
    $('#save-travel-preferences-btn').click(function(event) {
      event.preventDefault();
      $('#travel-preferences-form').submit();
    });

    // Remove succss box on click
    $('.success-box').click(function() {
      $(this).fadeOut(500);
    });

    $('#choose-image-btn').click(function(event) {
      event.preventDefault();
      $('#upload-image-input').click();
    });

    $('#upload-image-input').change(function() {
      Profile.readFile(this, $('#preview-upload-image'));
      $('#upload-image-btn').html('<i class="fa fa-upload"></i> Upload').prop('disabled', false).show();
      $('#upload-error-msg').css('display', 'none');
    });




    // AJAX UPLOAD
    $('#upload-image-btn').click(Profile.uploadImage);

    // Switch booking view style
    $('.itinerary-view-style a').click(function(event) {
      event.preventDefault();
      var type = $(this).data('type');

      if (type == 'grid') {
        $('.itinerary-grid').fadeIn(300);
        $('.itinerary-list').hide();
      } else {
        $('.itinerary-list').fadeIn(300);
        $('.itinerary-grid').hide();
      }
    });

    $('#change-password-btn').click(Profile.changePassword);

    $('#select-currency').change(Profile.updateCurrency);
  }); // end of document ready function
</script>
@stop