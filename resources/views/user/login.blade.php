@extends('layouts.static')

@section('custom-css')
<style type="text/css">
	.form-group {
		margin-bottom: 20px;
		position: relative;
	}
	.form-group i  {
		position: absolute;
		top: 10px;
		left: 13px;
		font-size: 15px;
		color: #5c5c5c;
	}
	.form-control {
		border-radius: 0;
		padding-left: 35px;
	}


	#fb-login-btn,
	#sign-up-fb-btn {
		outline: 0;
		border: 0;
		background: #3b5998;
		color: #f5f5f5 !important;
		display: block;
		width: 100%;
		padding: 15px;
		margin: 0 auto;
		text-decoration: none !important;
	}
	#fb-login-btn:disabled {
		opacity: 0.6;
	}
	#fb-login-btn img,
	#sign-up-fb-btn img {
		width: 30px;
		height: 30px;
		margin-left: 10px;
	}
	#login-form {
		padding: 5rem;
	}
	.error-msg {
		display: block;
		margin-bottom: 28px;
		color: #FFFFFF;
		background: #FF3D28;
		padding: 7px;
		transition: all 0.3s;
	}

    #login-error-msg {
        margin-bottom: 28px;
        color: red;
        background: #FFFFFF;
        transition: all 0.3s;
    }
    #forgot-password-btn:hover, #forgot-password-btn.active{text-decoration: underline;}
    .wrapper {
		background-image: url( {{ url( 'assets/img/bg1.jpg' ) }} ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

	.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	.text-field{
	    border: 1px solid #000;
	    padding: 11px;
	    width: 100%;
	    background: #fff;
	    border-radius: 2px;
	}
	.blue-button{
		transition: all 0.5s;
		background: #2AA9DF;
		border-color: #2AA9DF;
		color: #fff;
		padding: 11px;
		font-weight: bold;
		border-radius: 2px;
		width: 100%;
	}
	.blue-button:hover{
		background: #BEE5F6;
		color: #2AA9DF;
		border-color: #2AA9DF;
	}
	.has-error .text-field, .has-error .select-style {
	    border-color: #a94442;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	}
	.select-style {
	  
	    border: 1px solid #000;
	    padding: 11px;
	    width: 100%;
	    background: #fff;
	}
	.radius {
	    border-radius: 2px !important;
	}
	.form-control{
		border-radius: 0;
		/* padding-left: 35px; */
	}
	#user-register{
		padding: 5rem;
	}
	.error-msg {
		display: block;
		margin-bottom: 28px;
		color: #FFFFFF;
		background: #FF3D28;
		padding: 7px;
		transition: all 0.3s;
	}

	/* FB LOGIN */
	#fb-login-btn,
	#sign-up-fb-btn {

		outline: 0;
		border: 0;
		background: #3b5998;
		color: #f5f5f5 !important;
		display:none;
		/*display: block;*/
		width: 100%;
		padding: 15px;
		margin: 0 auto;
		text-decoration: none !important;
	}
	#fb-login-btn:disabled {
		opacity: 0.6;
	}
	#fb-login-btn img,
	#sign-up-fb-btn img {
		width: 30px;
		height: 30px;
		margin-left: 10px;
	}
	.form-group {
		margin-bottom: 20px;
		position: relative;
	}
	.form-group i  {
		position: absolute;
		top: 10px;
		left: 13px;
		font-size: 15px;
		color: #5c5c5c;
	}
	.form-control {
		border-radius: 0;
		padding-left: 35px;
	}
</style>
@stop

@section('content')

	
     <div class="login-wrapper">
      <div class="login-container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li class="active"><a href="#signin" aria-controls="signin" role="tab" data-toggle="tab">Sign In</a></li>
          <li ><a href="#signup" aria-controls="signup" role="tab" data-toggle="tab">Sign Up</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="signin">
            <div class="login-inner">
              <div class="modal-icon">
                <svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="42---EROA007-V4.1-Generic-(B2B-Agent-Screen)-02" transform="translate(-320.000000, -171.000000)" fill="#212121">
                            <g id="Group-3" transform="translate(291.000000, 144.000000)">
                                <g id="Group-2">
                                    <path d="M31.8186047,48.7674419 C33.0495814,47.4647442 35.9940465,46.0138605 38.4546047,45.1562791 L41.1141395,47.4181395 C41.5310698,47.7728372 42.1436279,47.7728372 42.5605581,47.4181395 L45.220093,45.1562791 C47.6806512,46.0138605 50.6251163,47.4647442 51.856093,48.7674419 L31.8186047,48.7674419 Z M45.316093,42.8346977 C44.9488372,42.7169302 44.5472558,42.7978605 44.2536744,43.0476279 L41.8372093,45.1024186 L39.4210233,43.0476279 C39.1274419,42.7978605 38.7258605,42.7169302 38.3586047,42.8346977 C36.795814,43.3331163 29,46.016093 29,49.8837209 C29,50.500186 29.499814,51 30.1162791,51 L53.5581395,51 C54.1746047,51 54.6744186,50.500186 54.6744186,49.8837209 C54.6744186,46.016093 46.8786047,43.3331163 45.316093,42.8346977 L45.316093,42.8346977 Z M41.8372093,29.2325581 C44.7495814,29.2325581 47.1188837,31.6166512 47.1188837,34.5474419 C47.1188837,37.4787907 44.7495814,39.8634419 41.8372093,39.8634419 C38.9251163,39.8634419 36.555814,37.4787907 36.555814,34.5474419 C36.555814,31.6166512 38.9251163,29.2325581 41.8372093,29.2325581 L41.8372093,29.2325581 Z M41.8372093,42.096 C45.9805581,42.096 49.3514419,38.7097674 49.3514419,34.5474419 C49.3514419,30.3856744 45.9805581,27 41.8372093,27 C37.6941395,27 34.3232558,30.3856744 34.3232558,34.5474419 C34.3232558,38.7097674 37.6941395,42.096 41.8372093,42.096 L41.8372093,42.096 Z" id="user"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
              </div>
              <div class="modal-container">
                <h4 class="modal-title" id="myModalLabel">Existing Customer Account</h4>
                <p>Please fill in the details below to search for a registered client.</p>
              	@if(session()->has('reset-success'))
                            <p class="success-box">
                                {{ session()->get('reset-success') }}
                            </p>
                        @endif
                        @if(session()->has('register-success'))
                            <p class="success-box">
                                {{ session()->get('register-success') }}
                            </p>
                        @endif
                        <p class="login-error-msg" id="login-error-msg">

                        </p>
              </div>
              <form class="form-horizontal modal-container" id="login-form1">
                <div class="input-field">
                  <!-- <input id="email" type="text">
                  <label for="email">Email Address</label> -->
                  <input id="username" type="text" name="username">
				  <label for="username">Email Address</label>
                </div>

                <div class="input-field">
                  <input id="password" type="password" name="password">
					<label for="password">Password</label>
                </div>

                <div>
                  <a href="/forgot-password" id="forgot-password-btn">Forgot Password?</a>
                </div>

                <div class="row m-t-50">
                  <div class="col-sm-6">
                    <!-- <input type="button" name="" class="btn btn-primary btn-block" value="Cancel"> -->
                  	<input type="button" name="" class="btn btn-primary btn-block" value="Cancel" id="1" onclick="window.location='/'">
                  </div>
                  <div class="col-sm-6">
                    <!-- <input type="button" name="" class="btn btn-primary btn-block" value="Sign in"> -->
					<input type="submit" name="" class="btn btn-primary btn-block" value="Submit" id="login-btn">                 
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="signup">
            <div class="login-inner">
              <div class="modal-icon">
                <svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="42---EROA007-V4.1-Generic-(B2B-Agent-Screen)-02" transform="translate(-320.000000, -171.000000)" fill="#212121">
                            <g id="Group-3" transform="translate(291.000000, 144.000000)">
                                <g id="Group-2">
                                    <path d="M31.8186047,48.7674419 C33.0495814,47.4647442 35.9940465,46.0138605 38.4546047,45.1562791 L41.1141395,47.4181395 C41.5310698,47.7728372 42.1436279,47.7728372 42.5605581,47.4181395 L45.220093,45.1562791 C47.6806512,46.0138605 50.6251163,47.4647442 51.856093,48.7674419 L31.8186047,48.7674419 Z M45.316093,42.8346977 C44.9488372,42.7169302 44.5472558,42.7978605 44.2536744,43.0476279 L41.8372093,45.1024186 L39.4210233,43.0476279 C39.1274419,42.7978605 38.7258605,42.7169302 38.3586047,42.8346977 C36.795814,43.3331163 29,46.016093 29,49.8837209 C29,50.500186 29.499814,51 30.1162791,51 L53.5581395,51 C54.1746047,51 54.6744186,50.500186 54.6744186,49.8837209 C54.6744186,46.016093 46.8786047,43.3331163 45.316093,42.8346977 L45.316093,42.8346977 Z M41.8372093,29.2325581 C44.7495814,29.2325581 47.1188837,31.6166512 47.1188837,34.5474419 C47.1188837,37.4787907 44.7495814,39.8634419 41.8372093,39.8634419 C38.9251163,39.8634419 36.555814,37.4787907 36.555814,34.5474419 C36.555814,31.6166512 38.9251163,29.2325581 41.8372093,29.2325581 L41.8372093,29.2325581 Z M41.8372093,42.096 C45.9805581,42.096 49.3514419,38.7097674 49.3514419,34.5474419 C49.3514419,30.3856744 45.9805581,27 41.8372093,27 C37.6941395,27 34.3232558,30.3856744 34.3232558,34.5474419 C34.3232558,38.7097674 37.6941395,42.096 41.8372093,42.096 L41.8372093,42.096 Z" id="user"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
              </div>
              <div class="modal-container">
                <h4 class="modal-title" id="myModalLabel">Create New Customer Account</h4>
                <p>Please fill in the details below to registered a new client.</p>
              </div>
              <form action="register" method="post" class="form-horizontal modal-container" id="signup_form">
				{{ csrf_field() }}
                
                

                <!-- <div class="input-field">
                  <input id="lname1" type="text">
                  <label for="lname1">Last Name</label>
                </div>

                <div class="input-field">
                  <input id="email2" type="text">
                  <label for="email2">Email Address</label>
                </div>

                <div class="input-field">
                  <input id="pass" type="Password">
                  <label for="pass">Password</label>
                </div>

                <div class="input-field">
                  <input id="confirmPass" type="Password">
                  <label for="confirmPass">Confirm Password</label>
                </div>

                <div class="input-field select-box">
                  <select class="form-control">
                    <option>Select Nationality</option>
                  </select>
                </div>

                <div class="input-field select-box">
                  <select class="form-control">
                    <option>Gender</option>
                  </select>
                </div>

                <div class="input-field select-box">
                  <select class="form-control">
                    <option>Age Group</option>
                  </select>
                </div>

                <div class="input-field select-box">
                  <select class="form-control">
                    <option>Select Default Currency</option>
                  </select>
                </div>
                
                <div class="form-group m-t-30">
                  <label class="radio-checkbox label_check m-r-10" for="checkbox-04"><input type="checkbox" name="policy" id="checkbox-04" value="1">By clicking ‘Create Account’, your client agrees to the <span><a href="/terms" target="_blank"> Terms of Use</a></span> and <span><a href="/privacy-policy" target="_blank">Privacy Policy</a></span>.</label>
                </div>

                <div class="row m-t-50">
                  <div class="col-sm-6">
                    <input type="button" name="" class="btn btn-primary btn-block" value="Cancel">
                  </div>
                  <div class="col-sm-6">
                    <input type="button" name="" class="btn btn-primary btn-block" value="Sign Up">
                  </div>
                </div> -->
                <div class="input-field {{ $errors->register->has('first_name') ? 'has-error' : ''  }}">
								<input type="text" class="form-control" id="first_name" name="first_name" value="{{old('first_name')}}">
								<label for="first_name">First Name</label>

							</div>

							<div class="input-field {{ $errors->register->has('last_name') ? 'has-error' : ''  }}">
								<input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}">
								<label for="last_name">Last Name</label>
							</div>

							<div class="input-field {{ $errors->register->has('email') || session()->has('register_error') ? 'has-error' : ''  }}">
								<input  type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
								<label for="email">Email Address</label>
							</div>
							<div class="input-field {{ $errors->register->has('password') ? 'has-error' : ''  }}">
								<input id="reg_password" type="password" class="form-control"  name="password" >
								<label for="reg_password">Password</label>
							</div>
							<div class="input-field {{ $errors->register->has('confirmpassword') ? 'has-error' : ''  }}">
								<input id="confirmpassword" type="password" class="form-control"  name="confirmpassword">
								<label for="confirmpassword">Confirm Password</label>
							</div>

							<div class="input-field select-box">
								<select class="form-control" name="nationality" id="nationality">
									<option value=""> Select Nationality </option>
									@if( count( $travelers['nationalities'] ) > 0 )
										@if( count( $travelers['nationalities']['featured'] ) > 0 )
											@foreach( $travelers['nationalities']['featured'] as $featured)
												<option value="{{ $featured['id'] }}" {{ isset($travel_pref['nationality']) && $travel_pref['nationality'] == $featured['name'] ? 'selected' : ''  }}>{{ $featured['name'] }}</option>
											@endforeach
										@endif
										<option value="" disabled> --------------------------- </option>
										@if( count( $travelers['nationalities']['not_featured'] ) > 0 )
											@foreach( $travelers['nationalities']['not_featured'] as $not_featured)
												<option value="{{ $not_featured['id'] }}" {{ isset($travel_pref['nationality']) && $travel_pref['nationality'] == $not_featured['name'] ? 'selected' : ''  }}>{{ $not_featured['name'] }}</option>
											@endforeach
										@endif
									@endif
								</select>
							</div>
							<label id="nationality-error" class="error" for="nationality" style="display: none;">Please select Nationality.</label>

							<div class="input-field select-box">
								<select class="form-control" name="gender" id="gender">
									<option value="">Gender</option>
									<option value="male">Male</option>
									<option value="feMale">Female</option>
								</select>
							</div>
							<label id="gender-error" class="error" for="gender" style="display: none;">Please select Gender.</label>

							<div class="input-field select-box">
								<select class="form-control" name="age_group" id="age_group">
									<option value="">Age Group</option>
									@if( count( $travelers['age_groups'] ) > 0 )
										@foreach( $travelers['age_groups'] as $age_group)
											@if( empty( $age_group['name'] ) )
												@continue
											@endif
											<option value="{{ $age_group['id'] }}" {{ isset($travel_pref['age_group']) && $travel_pref['age_group'] == $age_group['name'] ? 'selected' : ''  }}>{{ $age_group['name'] }}</option>
										@endforeach
									@endif
								</select>
							</div>
							<label id="age_group-error" class="error" for="age_group" style="display: none;">Please select Age Group.</label>

							<div class="input-field select-box {{ $errors->register->has('currency') ? 'has-error' : ''  }}">
								<select class="form-control" name="currency" id="currency">
									<option value="" >Select Default Currency</option>
                                    <?php $currencies = session()->get('all_currencies'); ?>
									@foreach( $currencies as $currency )
										<option value="{{ $currency['code'] }}" {{ (old('currency')) && old('currency') == $currency['code'] ? ' selected' : ''  }}> {{ '('. $currency['code'] .') '.$currency['name'] }} </option>
									@endforeach;
								</select>
							</div>
							<label id="currency-error" class="error" for="currency" style="display: none;">Please select Currency.</label>

							@if (count($errors->register) > 0 || session()->has('register_error') )
								<div class="error-msg">
									<p> Something went wrong! </p>
									{!! session()->has('register_error') ? '<p>'. array_get( session()->get('register_error'), 'email.0'  ) .'</p>' : ''  !!}
								</div>
							@endif

							<div class="form-group m-t-30">
								<label class="radio-checkbox label_check m-r-10" for="checkbox-04"><input type="checkbox" name = "policy" id="checkbox-04" value="1">By clicking ‘Create Account’, your client agrees to the <span><a href="/terms" target="_blank"> Terms of Use</a></span> and <span><a href="/privacy-policy" target="_blank">Privacy Policy</a></span>.</label>
                                <label for="policy" generated="true" class="error" style="display: none;">Please select checkbox to agree privacy policy and tems of use.</label>
							</div>

							<div class="row m-t-50">
								<div class="col-sm-6">
									<input type="button" name="" class="btn btn-primary btn-block" data-dismiss="modal" value="Cancel" onclick="window.location='/'">
								</div>
								<div class="col-sm-6">
                                    <!-- <input type="submit" name="" class="btn btn-primary btn-block" value="Create Account"> -->
									<input type="submit" name="" class="btn btn-primary btn-block" value="Sign-Up">
								</div>
							</div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
@stop

@section('custom-js')
<script type="text/javascript" src="{{ url('assets/js/theme/default-load.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
	<?php 
		if(isset($is_register) && $is_register == 1){
			?>
			$('.login-container .nav li').removeClass('active');
			$('.login-container .nav li:nth-child(2)').addClass('active');
			$('.tab-content .tab-pane').removeClass('active');
			$('#signup').addClass('active');
			<?php
		}
	?>
	
	var facebookBtn = $('#fb-login-btn');
	var loginForm = $('#login-form1');
	var fbResponse;
	var fields = {
		fields: 'email, picture.width(500).height(500), first_name, last_name'
	};

	$('.login-group input').keypress(function(e) {
		if (e.which == 13) {
			$('#login-btn').click();
		}
	});
	
    $("#login-form1").validate({
        rules: {
            username: {
                 required: true,
                 email:true
            },
            password: {
                required: true
            },

        },
        messages: {

            username: {
                required: "Please enter Email Address."
            },
            password: {
                required: "Please enter Password."
            },

        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {

            login();

        }
    });


    jQuery.validator.addMethod("lettersonly", function(value, element) {
			  return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Please enter letters only."); 
            $("#signup_form").validate({
                rules: {
                    first_name: {
                        required: true,
                        lettersonly: true
                    },
                    last_name: {
                        required: true,
                        lettersonly: true
                    },
                    email: {
                        required: true,
                        email:true
                    },
                    password: {
                        required: true
                    },
                    confirmpassword: {
                       equalTo : "#reg_password"
                    },
                    nationality: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    age_group: {
                        required: true
                    },
                    currency: {
                        required: true
                    },
                    policy:{
                        required:true
                    },
                },
                messages: {

                    first_name: {
                        required: "Please enter First Name."
                    },
                    last_name: {
                        required: "Please enter Last Name."
                    },
                    email: {
                        required: "Please enter Email Address."
                    },
                    password: {
                        required: "Please enter Password."
                    },
                    confirmpassword: {
                        equalTo: 'Password and Confirm Password must be same.'
                    },
                    nationality: {
                        required: "Please select Nationality.",
                    },
                    gender: {
                        required: "Please select Gender.",
                    },
                    age_group: {
                        required: "Please select Age Group.",
                    },
                    currency: {
                        required: "Please select Currency.",
                    },
                    policy: {
                        required: "Please select checkbox to agree privacy policy and tems of use.",
                    }
                },
                errorPlacement: function (label, element) {
                    label.insertAfter(element);
                },
                submitHandler: function (form) {

                    form.submit();

                }
            });
	// LOGIN USER
	function login(){

		var username = $("#username").val();
		var password = $("#password").val();
		if (username != '' && password != '') {
			eroam.ajax('post', 'login', {username: username, password: password}, function(response) {
				if (response.trim() == 'valid') {
					//$('#login-error-msg').hide();
					$('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');

					window.location.href = '/profile';
					//return false;
				} else {
					$('#login-error-msg').text('Invalid Email Address or Password').show();
					$('#login-btn').html('Log In');
				}
			}, function() {
				$('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
			});
		}

	}
	

	// GET FACEBOOK SCRIPT
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/sdk.js', function() {
		FB.init({
			appId: '1731928143731970',
			version: 'v2.7'
		});

		FB.getLoginStatus(function(response) {
			if (response.status == 'connected') {
				FB.api('/me', fields, function(response) {
					fbResponse = response;
					eroam.api('post', 'check-fb-user', {email: fbResponse.email}, function(response) {
						if (response) {
							facebookBtn.html('<i class="fa fa-facebook"></i> Continue as '+ fbResponse.first_name + '<img src="'+fbResponse.picture.data.url+'">');
						}
					});
				});
			}
			facebookBtn.removeAttr('disabled');
		});
	});

	// ON CLICK FACEBOOK BUTTON
	facebookBtn.on('click', function(e) {
		e.preventDefault();
		FB.login(function(response) {
			var grantedScopes = response.authResponse.grantedScopes.split(',');
			var hasEmail = grantedScopes.some(function(v, k) {
				return v == 'email';
			});
			if (response.status == 'connected') {
				// CHECK IF USER GRANTED PERMISSION TO HIS/HER EMAIL
				if (hasEmail == true) {
					FB.api('/me', fields, function(response) {
						if (response.email) {
							fbResponse = response;
							checkFacebookUser(response);
						} else {
							$('#fb-error-msg').text('Please validate your Facebook email in order to continue.').show();
						}
					});
				} else {
					$('#fb-error-msg').text('You must allow eRoam\'s permission to read your Facebook email address to proceed.').show();
				}
			}
		}, { scope: 'email', return_scopes: true });
	});

	// FACEBOOK USER SIGN UP FOR EROAM
	loginForm.delegate('#sign-up-fb-btn', 'click', function(e) {
		e.preventDefault();
		var name = fbResponse.first_name + ' ' + fbResponse.last_name;
		var fname = fbResponse.first_name;
		var lname = fbResponse.last_name;
		var email = fbResponse.email;
		var password = $('#fb-password').val();
		var currency = $('#fb-currency').val();

		// VALIDATION
		if (name == '' || fname == '' || lname == '' || email == null || password == '' || currency == null) {
			$('#fb-sign-up-error').text('All fields are required.').show();
		} else {
			if (password.length < 6) {
				$('#fb-sign-up-error').text('Password must be at least 6 characters long.').show();
			} else {
				eroam.api('post', 'user/check-email-availability', {email: email}, function(response) {
					if (response) {
						$('#fb-sign-up-error').text('Your Facebook email address ('+email+') is already in use.').show();
					} else {
						// STORE USER
						$('#fb-sign-up-error').hide();
						var data = {
							email: email,
							first_name: fname,
							last_name: lname,
							password: password,
							currency: currency,
							image_url: fbResponse.picture.data.url,
						};
						eroam.api('post', 'user/fb-signup', data, function(response) {
							if (response) {
								eroam.ajax('post', 'login', {username: email, password: password}, function(response) {
									if (response == 'valid') {
										window.location.href = '{{ url('profile') }}';
									}
								}, function() {
									$('#sign-up-fb-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
								});
							}
						}, function() {
							$('#sign-up-fb-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Processing...');
						});
					}
				});
			}
		}
	});
});

function checkFacebookUser(fbUser) {
	if (fbUser.error) {
		window.location.href = '{{ url('login') }}';
	}

	eroam.api('post', 'check-fb-user', {email: fbUser.email}, function(response) {
		if (response) {
			eroam.ajax('post', 'login', {username: response.email}, function(response) {
				if (response == 'valid') {
					window.location.href = '{{ url('profile') }}';
				}
			}, function() {
				$('#fb-login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
			});
		} else {
			var fbEmail = fbUser.email ? fbUser.email : '';
			var currencies = JSON.parse($('#all-currencies').val());
			var currencyHtml = '';

			for (var i = 0; i < currencies.length; i++) {
				currencyHtml += '<option value="'+currencies[i].code+'">'+currencies[i].code+' '+currencies[i].name+'</option>';
			}

			$('#login-form').html(
				'<h4>Sign up for eRoam</h4>' +
				'<div class="form-group">'+
					'<input type="password" id="fb-password" class="form-control" placeholder="Choose a Password">' +
					'<i class="fa fa-lock"></i>' +
				'</div>' +
				'<div class="form-group">'+
					'<select class="form-control" id="fb-currency">' +
						'<option disabled selected value="">Select Currency</option>' +
						currencyHtml +
					'</select>'+
					'<i class="fa fa-dollar"></i>' +
				'</div>' +
				'<p id="fb-sign-up-error" class="error-msg" style="display: none"></p>' +
				'<button id="sign-up-fb-btn"><i class="fa fa-facebook"></i> Sign up as '+ fbUser.first_name +' ' + fbUser.last_name +' <img src="' + fbUser.picture.data.url + '"></button>'
			);
		}
	}, function() {
		$('#fb-login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
	});
}
</script>
@stop
