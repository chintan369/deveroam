@extends('layouts.static')

@section('custom-css')
<style type="text/css">
	#login-box {
			margin-bottom: 150px;
			padding: 25px;
		}
		.form-group {
			margin-bottom: 20px;
			position: relative;
		}
		.form-group i  {
			position: absolute;
			top: 10px;
			left: 13px;
			font-size: 15px;
			color: #5c5c5c;
		}
		.form-control {
			border-radius: 0;
			padding-left: 35px;
		}
		/*#login-btn {
			display: inline-block;
			border: none;
			background: #2AA9DF;
			border: 1px solid transparent;
			color: #f5f5f5;
			width: 100%;
			padding: 15px 45px;
			text-decoration: none;
			transition: all .4s;
		}
		#login-btn:hover {
			color: #2AA9DF;
			border-color: #2AA9DF;
			background: #BEE5F6;
		}
		#login-btn:disabled {
			background: #99B6C2;
		}*/
		.fb-btn,
		#fb-signup-btn {
			background: #3b5998;
			color: #f5f5f5 !important;
			display: block;
			width: 70%;
			padding: 15px;
			margin: 0 auto;
			text-decoration: none !important;
		}
		#send-fp-email-btn {
			margin-top: 20px;
			border-radius: 0;
		}
		#fp-response {
			color: #167e3b;
		}
		#fb-user-login {
			display: none;
		}
		#fb-login-picture {
			border-radius: 100px;
			border: solid 1px #e2e2e2;
			height: 120px;
			width: 120px;
			margin-bottom: 20px;
		}
		#normal-user-login {
			padding: 5rem;
		}
		.header-link{
			font-size:medium;
			text-decoration:none !important;
			font-weight: bold;
			
		}
		#login-box{
			margin-top:100px;
		}

		fieldset{
			border:1px solid gray;
			padding:70px;
			margin-top:30px;


		}
		#error-msg {
			margin-bottom: 28px;
		    color: red;
		    background: #FFFFFF;
		    transition: all 0.3s
		}
		/*.success-box  {
			background: #A9FDC1;
			color: #2F2F2F;
			border: 2px solid #1BCE4E;
			padding: 11px;
			text-align: center;
			cursor: pointer;
		}*/
</style>
@stop

@section('content')
 <div class="login-wrapper">
      <div class="login-container forgot-container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">

          <li class="active"><a href="#signin" aria-controls="signin" role="tab" data-toggle="tab">Forgot Password</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="signin">
            <div class="login-inner">
              <div class="modal-icon">
                <svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="42---EROA007-V4.1-Generic-(B2B-Agent-Screen)-02" transform="translate(-320.000000, -171.000000)" fill="#212121">
                            <g id="Group-3" transform="translate(291.000000, 144.000000)">
                                <g id="Group-2">
                                    <path d="M31.8186047,48.7674419 C33.0495814,47.4647442 35.9940465,46.0138605 38.4546047,45.1562791 L41.1141395,47.4181395 C41.5310698,47.7728372 42.1436279,47.7728372 42.5605581,47.4181395 L45.220093,45.1562791 C47.6806512,46.0138605 50.6251163,47.4647442 51.856093,48.7674419 L31.8186047,48.7674419 Z M45.316093,42.8346977 C44.9488372,42.7169302 44.5472558,42.7978605 44.2536744,43.0476279 L41.8372093,45.1024186 L39.4210233,43.0476279 C39.1274419,42.7978605 38.7258605,42.7169302 38.3586047,42.8346977 C36.795814,43.3331163 29,46.016093 29,49.8837209 C29,50.500186 29.499814,51 30.1162791,51 L53.5581395,51 C54.1746047,51 54.6744186,50.500186 54.6744186,49.8837209 C54.6744186,46.016093 46.8786047,43.3331163 45.316093,42.8346977 L45.316093,42.8346977 Z M41.8372093,29.2325581 C44.7495814,29.2325581 47.1188837,31.6166512 47.1188837,34.5474419 C47.1188837,37.4787907 44.7495814,39.8634419 41.8372093,39.8634419 C38.9251163,39.8634419 36.555814,37.4787907 36.555814,34.5474419 C36.555814,31.6166512 38.9251163,29.2325581 41.8372093,29.2325581 L41.8372093,29.2325581 Z M41.8372093,42.096 C45.9805581,42.096 49.3514419,38.7097674 49.3514419,34.5474419 C49.3514419,30.3856744 45.9805581,27 41.8372093,27 C37.6941395,27 34.3232558,30.3856744 34.3232558,34.5474419 C34.3232558,38.7097674 37.6941395,42.096 41.8372093,42.096 L41.8372093,42.096 Z" id="user"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
              </div>

             

              	<div class="modal-container">
                	<h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                	<p>Please enter your email and we'll help you to reset your password.</p>
              	</div>
               	@if(session()->has('success'))
				 	<p class="success-box">{{ session()->get('success') }}</p>
              	@endif
					<form class="form-horizontal modal-container" id="forgot-form1" method="post">
						<div class="input-field login-group">
							<input id="username" type="text" name="email">
							<label for="username">Email Address</label>
						</div>
						@if(session()->has('error'))
				        <p id="error-msg">{{ session()->get('error') }}</p>
				   		@endif
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						 	<div class="row m-t-50">
			                  <div class="col-sm-6">
			                  	<input type="button" name="" class="btn btn-primary btn-block" data-dismiss="modal" value="Cancel" onclick="window.location='/'">
			                  </div>
			                  <div class="col-sm-6">
			                    <input type="submit" name="" class="btn btn-primary btn-block" value="Continue" id="login-btn">
			                  </div>
                		</div>
					</form>
				
            </div>
          </div>
          
        </div>

      </div>
    </div>
	<!-- <div class="container">
		<div class="row">
			<div class="col-md-4 white-box col-md-offset-4 text-center">
				<div id="normal-user-login">
					<h5>FORGOT PASSWORD</h5>
					@if(session()->has('success'))
						 <p class="success-box">{{ session()->get('success') }}</p>
					@else
						<form method="post" action="forgot-password">
							<div class="form-group">
								<input type="text" name="email" class="form-control" placeholder="Email" autofocus="" required>
								<i class="fa fa-user" aria-hidden="true"></i>
							</div>
							@if(session()->has('error'))
						        <p id="error-msg">{{ session()->get('error') }}</p>
						    @endif
					
							<input type="submit" value="CONTINUE" id="login-btn">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						</form>
					@endif
					
				</div>
			</div>
		</div>
	</div> -->
   <!--  <section class="banner-container">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class="first-slide" src="{{ url( 'assets/images/banner1.jpg' ) }}" alt="First slide">
                </div>
                <div class="item">
                    <img class="third-slide" src="{{ url( 'assets/images/banner2.jpg' ) }}" alt="Third slide">
                </div>
            </div>
            <div class="banner-search">
                <div class="banner-inner">
                    <p><img src="{{ url( 'assets/images/eRoam_Logo.png' ) }}" alt="eroam" class="carousel-logo"></p>
                    <p>Over 500,000 hotels, 1,000 arlines, plus 100,000’s events, activities &amp; restaurants.</p>

                    <form method="post" action="search" id="search-form" class="form-horizontal clearfix">


                        <div class="col-md-10 col-md-offset-1" >
                            <div class="searchbox-new search-box1" style="display:none;">
                                <div class="row">
                                    <div class="col-md-10 col-sm-10 padding-right-0 banner-searchbox-new">
                                        <input type="text" name="project" id="project" class="form-control" placeholder="Where do you want to go?">

                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12 padding-left-0">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="searchbox-new search-box2">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="text" value="" name="start_location1" id="start_location1" class="form-control" placeholder="Start Location">
                                    </div>
                                    <div class="col-sm-3">
                                        <input id="travellers1" value="" type="number" name="travellers1" class="form-control" placeholder="No. of Travellers" min="1" max="10" />
                                    </div>
                                    <div class="col-sm-3">
                                        <input id="start_date1" value="" name="start_date1" type="text" class="form-control" placeholder="Start Date" >
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="searchbox-new search-box3" style="display:none;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <input type="text" value="" name="start_location2" id="start_location2" class="form-control" placeholder="Start Location">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" value="" name="end_location2" id="end_location2" class="form-control" placeholder="End Location">
                                    </div>
                                    <div class="col-sm-2">
                                        <input id="travellers2" value="" type="number" name="travellers2" class="form-control" placeholder="No. of Travellers" min="1" max="10" />
                                    </div>
                                    <div class="col-sm-2">
                                        <input id="start_date2" value="" name="start_date2" type="text" class="form-control" placeholder="Start Date" >
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="submit" name="" class="btn btn-primary btn-block" value="Search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-9 col-xs-12">
                                    <div class="checkbox-group">
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-01"><input type="radio" name="option" id="checkbox-01" value="packages" disabled >Tour Packages</label>
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-02"><input type="radio" name="option" id="checkbox-02" value="manual" checked>Tailormade Manual</label>
                                        <label class="radio-checkbox label_radio m-r-10" for="checkbox-03"><input type="radio" name="option" id="checkbox-03" value="auto">Tailormade Auto</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-12 padding-0" style="display: none;">
                                    <p class="advance-right"><a href="#" class="advance-text">More Options <b class="caret"></b></a></p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
        </div>
    </section> -->

  <!--   <section class="content-wrapper">
        <h1 class="hide"></h1>
        <article class="places-section eroam-trending">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2>Top Tours</h2>
                       <div class="places-list">

                              <div id="tours-loader">
                                <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
                              </div>
                              <div class="row" id="tourList"></div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section> -->
	<!-- <a href="#" id="onForgotModal" data-toggle="modal" data-target="#forgotModal" style="display:none;" data-backdrop="static" data-keyboard="false"></a>
	<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm" role="document" style="width: 450px; margin: 100px auto;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/'"><span aria-hidden="true">&times;</span></button>
					<div class="modal-icon">
						<svg width="26px" height="24px" viewBox="0 0 26 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<desc>Created with Sketch.</desc>
							<defs></defs>
							<g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g id="42---EROA007-V4.1-Generic-(B2B-Agent-Screen)-02" transform="translate(-320.000000, -171.000000)" fill="#212121">
									<g id="Group-3" transform="translate(291.000000, 144.000000)">
										<g id="Group-2">
											<path d="M31.8186047,48.7674419 C33.0495814,47.4647442 35.9940465,46.0138605 38.4546047,45.1562791 L41.1141395,47.4181395 C41.5310698,47.7728372 42.1436279,47.7728372 42.5605581,47.4181395 L45.220093,45.1562791 C47.6806512,46.0138605 50.6251163,47.4647442 51.856093,48.7674419 L31.8186047,48.7674419 Z M45.316093,42.8346977 C44.9488372,42.7169302 44.5472558,42.7978605 44.2536744,43.0476279 L41.8372093,45.1024186 L39.4210233,43.0476279 C39.1274419,42.7978605 38.7258605,42.7169302 38.3586047,42.8346977 C36.795814,43.3331163 29,46.016093 29,49.8837209 C29,50.500186 29.499814,51 30.1162791,51 L53.5581395,51 C54.1746047,51 54.6744186,50.500186 54.6744186,49.8837209 C54.6744186,46.016093 46.8786047,43.3331163 45.316093,42.8346977 L45.316093,42.8346977 Z M41.8372093,29.2325581 C44.7495814,29.2325581 47.1188837,31.6166512 47.1188837,34.5474419 C47.1188837,37.4787907 44.7495814,39.8634419 41.8372093,39.8634419 C38.9251163,39.8634419 36.555814,37.4787907 36.555814,34.5474419 C36.555814,31.6166512 38.9251163,29.2325581 41.8372093,29.2325581 L41.8372093,29.2325581 Z M41.8372093,42.096 C45.9805581,42.096 49.3514419,38.7097674 49.3514419,34.5474419 C49.3514419,30.3856744 45.9805581,27 41.8372093,27 C37.6941395,27 34.3232558,30.3856744 34.3232558,34.5474419 C34.3232558,38.7097674 37.6941395,42.096 41.8372093,42.096 L41.8372093,42.096 Z" id="user"></path>
										</g>
									</g>
								</g>
							</g>
						</svg>
					</div>
					<div class="modal-container">
						<h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
					</div>
				</div>
				<div class="modal-body">
					<div class="modal-container">
						@if(session()->has('success'))
						 <p class="success-box">{{ session()->get('success') }}</p>
						@else
							<form class="form-horizontal" id="forgot-form1" method="post">
								<div class="input-field login-group">
									<input id="username" type="text" name="email">
									<label for="username">Email Address</label>
								</div>
								@if(session()->has('error'))
						        <p id="error-msg">{{ session()->get('error') }}</p>
						   		@endif
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="row m-t-50">
									<div class="col-sm-12">
										<input type="submit" name="" class="btn btn-primary btn-block" value="Continue" id="login-btn">
									</div>
								</div>
							</form>
						@endif	
					</div>
				</div>
			</div>
		</div>
	</div> -->
@stop
@section('custom-js')
<script type="text/javascript" src="{{ url('assets/js/theme/default-load.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
	
	$("#onForgotModal").click();
    $("#forgot-form1").validate({
        rules: {
            email: {
                required: true,
                email:true
            }
        },
        messages: {
            email: {
                required: "Please enter Email Address."
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
        	form.submit();
        }
    });	
});


</script>
@stop
