@extends('layouts.home2')

@section('custom-css')
<style type="text/css">
	#login-box {
			margin-bottom: 150px;
			padding: 25px;
		}
		.form-group {
			margin-bottom: 20px;
			position: relative;
		}
		.form-group i  {
			position: absolute;
			top: 10px;
			left: 13px;
			font-size: 15px;
			color: #5c5c5c;
		}
		.form-control {
			border-radius: 0;
			padding-left: 35px;
		}
		
		.fb-btn,
		#fb-signup-btn {
			background: #3b5998;
			color: #f5f5f5 !important;
			display: block;
			width: 70%;
			padding: 15px;
			margin: 0 auto;
			text-decoration: none !important;
		}
		#send-fp-email-btn {
			margin-top: 20px;
			border-radius: 0;
		}
		#fp-response {
			color: #167e3b;
		}
		#fb-user-login {
			display: none;
		}
		#fb-login-picture {
			border-radius: 100px;
			border: solid 1px #e2e2e2;
			height: 120px;
			width: 120px;
			margin-bottom: 20px;
		}
		#normal-user-login {
			padding: 5rem;
		}
		.header-link{
			font-size:medium;
			text-decoration:none !important;
			font-weight: bold;
			
		}
		#login-box{
			margin-top:100px;
		}

		fieldset{
			border:1px solid gray;
			padding:70px;
			margin-top:30px;


		}
		#error-msg {
			margin-bottom: 28px;
			color: #FFFFFF;
			background: #FF3D28;
			padding: 7px;
			transition: all 0.3s;
		}
		.success-box {
			background: #A9FDC1;
			color: #2F2F2F;
			border: 2px solid #1BCE4E;
			padding: 11px;
			text-align: center;
			cursor: pointer;
		}
</style>
@stop

@section('content')

<section class="banner-container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo url( 'assets/images/banner/tour-surf-lifestyle2.jpg'); ?>" alt="eRoam">
          <div class="banner-caption">
            <div class="banner-inner">
              <form class="form-horizontal clearfix">
                <div class="col-md-12">
                  <div class="text-center">
                    <h1>LIVE THE AUSTRALIAN SURF LIFESTYLE</h1>
                    <p><strong>90 Day Tour</strong></p>
                    <p class="banner-price">$7950.00 </p>
                    <a href="tourDetail/11374/live_the_australian_surf_lifestyle_3_month_learn_to_surf_academy1" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="first-slide" src="<?php echo url( 'assets/images/banner/tour-surf-lifestyle.jpg'); ?>" alt="eRoam">
          <div class="banner-caption">
            <div class="banner-inner">
              <form class="form-horizontal clearfix">
                <div class="col-md-12">
                  <div class="text-center">
                    <h1>LIVE THE AUSTRALIAN SURF LIFESTYLE</h1>
                    <p><strong>30 Day Tour</strong></p>
                    <p class="banner-price">$4000.00 </p>
                    <a href="tourDetail/11372/live_the_australian_surf_lifestyle_1_month_learn_to_surf_academy1" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        
        <div class="item">
          <img class="first-slide" src="<?php echo url( 'assets/images/banner/tour-surf-lifestyle3.jpg'); ?>" alt="eRoam">
          <div class="banner-caption">
            <div class="banner-inner">
              <form class="form-horizontal clearfix">
                <div class="col-md-12">
                  <div class="text-center">
                    <h1>2 DAY GREAT BARRIER REEF SNORKEL</h1>
                    <p><strong>2 Day Tour</strong></p>
                    <p class="banner-price">$500.00 </p>
                    <a href="tourDetail/11378/2_day_great_barrier_reef_snorkel_daintree_rainforest_and_aboriginal_culture_tour" name="" class="btn btn-secondary m-t-80">VIEW OFFER</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
    </div>
  </section>
<section class="package-wrapper">
      <div class="package-wrapperInner">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="package-container">
                <p>Search from over 500,000 hotels, 1,000 airlines, plus 100,000’s events, tours &amp; restaurants. Select one of the options below.</p>
                <ul class="nav nav-tabs" id="packageTabs" role="tablist"> 
                  <li class="active">
                    <a href="#tours" id="tours-tab" role="tab" data-toggle="tab" aria-controls="tours" aria-expanded="true"><span class="icon icon-activity"></span> Tours / Packages</a>
                  </li> 
                  <li>
                    <a href="#itinerary" role="tab" id="itinerary-tab" data-toggle="tab" aria-controls="itinerary"><span class="icon icon-location"></span> Create Itinerary</a>
                  </li>
                  <li>
                    <a href="#hotel" role="tab" id="hotel-tab" data-toggle="tab" aria-controls="hotel"><span class="icon icon-hotel"></span> Accommodation</a>
                  </li>
                  <li>
                    <a href="#flights" role="tab" id="flights-tab" data-toggle="tab" aria-controls="flights"><span class="icon icon-plane"></span> Flights</a>
                  </li>
                  <li>
                    <a href="#carHire" role="tab" id="carHire-tab" data-toggle="tab" aria-controls="carHire"><span class="icon icon-car"></span> Car Hire</a>
                  </li>
                </ul> 
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="package-wrapperInner m-t-10">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="tab-content" id="packageTabContent"> 
                <div class="tab-pane fade in active" role="tabpanel" id="tours" aria-labelledby="tours-tab">
                  <div>
                    <label class="radio-checkbox label_radio r_on" for="checkbox-03"><input type="radio" name="option1" id="checkbox-03" value="search-box3" checked>Single Day Tour</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a>
                    <label class="radio-checkbox label_radio" for="checkbox-04"><input type="radio" name="option1" id="checkbox-04" value="search-box4">Multi-Day Tour</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#manualModeModal"><i class="fa fa-question-circle"></i></a>
                  </div>
                  <div class="row m-t-20">
                    <div class="col-md-9 col-sm-8">
                      <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                      <div class="panel-form-group">
                        <label class="label-control">Destination</label>
                        <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-4 date-control">
                      <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                      <div class="panel-form-group">
                        <label class="label-control">Departure Date</label>
                        <div class="input-group datepicker">
                          <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                          <span class="input-group-addon"><i class="icon-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="m-t-10"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                  <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH">
                </div>
                <div class="tab-pane fade" role="tabpanel" id="itinerary" aria-labelledby="itinerary-tab">
                  <div>
                    <label class="radio-checkbox label_radio r_on" for="checkbox-05"><input type="radio" id="checkbox-05" name="option2" value="search-box3" checked>Tailor-Made Auto</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a>
                    <label class="radio-checkbox label_radio" for="checkbox-06"><input type="radio" id="checkbox-06" name="option2" value="search-box4">Manual Itinerary</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#manualModeModal"><i class="fa fa-question-circle"></i></a>
                    <label class="radio-checkbox label_radio" for="checkbox-07"><input type="radio" id="checkbox-07" name="option2" value="search-box5">Package Rates</label>
                    <a href="#" class="help-icon" data-toggle="modal" data-target="#packageRateModal"><i class="fa fa-question-circle"></i></a>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="searchMode-container search-box3 mode-block">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="searchMode-container search-box4">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row m-t-20">
                      <div class="col-sm-3 date-control">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                        <div class="panel-form-group">
                          <label class="label-control">Check-In Date</label>
                          <div class="input-group datepicker">
                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-9">
                        <div class="left-spacing">
                          <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Rooms</label>
                                <select class="form-control">
                                  <option>1 Room</option>
                                  <option>2 Room</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Adults (18+)</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Children (0-17)</label>
                                <select class="form-control">
                                  <option>No Children</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="children-block">
                    <div><span class="smile-icon"><i class="icon-child"></i></span>
                      <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                    </div>
                    <div class="row m-t-10">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group">
                          <label class="label-control">Child 1 Age</label>
                          <select class="form-control">
                            <option>1 Years</option>
                            <option>2 Years</option>
                            <option>3 Years</option>
                            <option>4 Years</option>
                            <option>5 Years</option>
                            <option selected>6 Years</option>
                            <option>7 Years</option>
                            <option>8 Years</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group">
                          <label class="label-control">Child 2 Age</label>
                          <select class="form-control">
                            <option>1 Years</option>
                            <option>2 Years</option>
                            <option>3 Years</option>
                            <option>4 Years</option>
                            <option>5 Years</option>
                            <option>6 Years</option>
                            <option>7 Years</option>
                            <option selected>8 Years</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 1</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 2</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <div class="row">
                      <div class="col-sm-4">
                        <p><span class="icon-hotel m-r-10"></span><strong>Room 3</strong></p>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                                <option>1 Children</option>
                                <option selected>2 Children</option>
                                <option>3 Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="left-spacing">
                          <div><span class="smile-icon"><i class="icon-child"></i></span>
                            <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 1 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 2 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <div class="row">
                      <div class="col-sm-4">
                        <p><span class="icon-hotel m-r-10"></span><strong>Room 4</strong></p>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                                <option>1 Children</option>
                                <option>2 Children</option>
                                <option selected>3 Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="left-spacing">
                          <div><span class="smile-icon"><i class="icon-child"></i></span>
                            <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 1 Age</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 2 Age</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 3 Age</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="m-t-20"><label class="radio-checkbox label_check" for="checkbox-08"><input type="checkbox" id="checkbox-08" value="search-box4">I do not need accommodation in the departing location</label></p>
                  <p class="m-t-10"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p>
                  <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH">
                </div> 
                <div class="tab-pane fade" role="tabpanel" id="hotel" aria-labelledby="hotel-tab">
                  <div>
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="panel-form-group input-control">
                      <label class="label-control">Destination</label>
                      <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                    </div>
                    <div class="row m-t-20">
                      <div class="col-sm-4">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                        <div class="row">
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Check-In Date</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Check-Out Date</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Rooms</label>
                              <select class="form-control">
                                <option>1 Room</option>
                                <option>2 Room</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 1</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-hotel m-r-10"></span><strong>Room 2</strong></p>
                    <div class="row">
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Adults (18+)</label>
                          <select class="form-control">
                            <option>1 Adult</option>
                            <option>2 Adult</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-3">
                        <div class="panel-form-group form-group">
                          <label class="label-control">Number of Children (0-17)</label>
                          <select class="form-control">
                            <option>No Children</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="m-t-20">
                    <div class="row">
                      <div class="col-sm-4">
                        <p><span class="icon-hotel m-r-10"></span><strong>Room 3</strong></p>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Adults (18+)</label>
                              <select class="form-control">
                                <option>1 Adult</option>
                                <option>2 Adult</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="panel-form-group form-group">
                              <label class="label-control">Number of Children (0-17)</label>
                              <select class="form-control">
                                <option>No Children</option>
                                <option>1 Children</option>
                                <option selected>2 Children</option>
                                <option>3 Children</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="left-spacing">
                          <div><span class="smile-icon"><i class="icon-child"></i></span>
                            <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                          </div>
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 1 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Child 2 Age</label>
                                <select class="form-control">
                                  <option>1 Years</option>
                                  <option>2 Years</option>
                                  <option>3 Years</option>
                                  <option>4 Years</option>
                                  <option>5 Years</option>
                                  <option selected>6 Years</option>
                                  <option>7 Years</option>
                                  <option>8 Years</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <p class="m-t-20"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p> 
                    <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH" />
                  </div>

                </div>
                <div class="tab-pane fade" role="tabpanel" id="flights" aria-labelledby="flights-tab">
                  <div>
                    <label class="radio-checkbox label_radio r_on m-r-20" for="checkbox-09"><input type="radio" id="checkbox-09" name="option3" value="search-box9" checked>Return</label>
                    <label class="radio-checkbox label_radio m-r-20" for="checkbox-10"><input type="radio" id="checkbox-10" name="option3" value="search-box10">One Way</label>
                    <label class="radio-checkbox label_radio m-r-20" for="checkbox-11"><input type="radio" id="checkbox-11" name="option3" value="search-box11">Multi-City</label>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="searchMode-container search-box9 mode-block">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="searchMode-container search-box10">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="searchMode-container search-box11">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row m-t-20">
                      <div class="col-sm-3 date-control">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Dates</strong></p>
                        <div class="panel-form-group">
                          <label class="label-control">Check-In Date</label>
                          <div class="input-group datepicker">
                            <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-9">
                        <div class="left-spacing">
                          <p><span class="icon-travellers m-r-10"></span><strong>Traveller Details</strong></p>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Rooms</label>
                                <select class="form-control">
                                  <option>1 Room</option>
                                  <option>2 Room</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Adults (18+)</label>
                                <select class="form-control">
                                  <option>1 Adult</option>
                                  <option>2 Adult</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="panel-form-group form-group">
                                <label class="label-control">Number of Children (0-17)</label>
                                <select class="form-control">
                                  <option>No Children</option>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="m-t-20">
                      <div><span class="smile-icon"><i class="icon-child"></i></span>
                        <label><strong>Children’s Age Required</strong></label> <a href="#" class="help-icon" data-toggle="modal" data-target="#childrenAgeModal"><i class="fa fa-question-circle"></i></a>
                      </div>
                      <div class="row m-t-10">
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 1 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option selected>6 Years</option>
                              <option>7 Years</option>
                              <option>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 2 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 3 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 4 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 5 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3">
                          <div class="panel-form-group">
                            <label class="label-control">Child 6 Age</label>
                            <select class="form-control">
                              <option>1 Years</option>
                              <option>2 Years</option>
                              <option>3 Years</option>
                              <option>4 Years</option>
                              <option>5 Years</option>
                              <option>6 Years</option>
                              <option>7 Years</option>
                              <option selected>8 Years</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="m-t-30 small-btn"> <!-- searchMode-container search-box11 -->
                      <div class="row">
                        <div class="col-sm-8">
                          <p><span class="icon-location m-r-10"></span><strong>Additional Location</strong></p>
                        </div>
                        <div class="col-sm-4">
                          <p><span class="icon-manage-trip m-r-10"></span><strong>Travel Date</strong></p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Departing</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Destination</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="row">
                            <div class="col-sm-9 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Check-In Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row m-t-10">
                        <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Departing</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Destination</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="row">
                            <div class="col-sm-9 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Check-In Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row m-t-10">
                        <div class="col-sm-8">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Departing</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="panel-form-group input-control">
                                <label class="label-control">Destination</label>
                                <input type="text" name="" class="form-control" placeholder="City or Airport" /> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="row">
                            <div class="col-sm-9 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Check-In Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" name="" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <p class="m-t-20"><a href="#"><i class="icon-fingerprint icon-prefrence"></i><span>Personal Preferences</span></a><a href="#" class="help-icon" data-toggle="modal" data-target="#autoModeModal"><i class="fa fa-question-circle"></i></a></p> 
                    <input type="submit" name="" class="btn btn-primary active m-t-20" value="SEARCH" />
                  </div>
                </div>
                <div class="tab-pane fade" role="tabpanel" id="carHire" aria-labelledby="carHire-tab">
                  <div>
                    <label class="radio-checkbox label_check" for="checkbox-12"><input type="checkbox" id="checkbox-12" name="option4" value="search-box12">Drop off at a different location</label>
                  </div>
                  <div class="m-t-20">
                    <p><span class="icon-location m-r-10"></span><strong>Location</strong></p>
                    <div class="without-drop">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Pick-Up Location</label>
                            <input type="text" name="" class="form-control" placeholder="City, Suburb or Airport" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="with-drop">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Departing</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="panel-form-group input-control">
                            <label class="label-control">Destination</label>
                            <input type="text" name="" class="form-control" placeholder="Region, Country, City or Suburb" /> 
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row m-t-20">
                      <div class="col-sm-5">
                        <p><span class="icon-manage-trip m-r-10"></span><strong>Pick-Up Date</strong></p>
                        <div class="row">
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Pick-Up Date</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6 date-control">
                            <div class="panel-form-group">
                              <label class="label-control">Pick-Up Time</label>
                              <div class="input-group datepicker">
                                <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-7">
                        <div class="left-spacing">
                          <p><span class="icon-manage-trip m-r-10"></span><strong>Drop-Off Date</strong></p>
                          <div class="row">
                            <div class="col-sm-4 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Drop-Off Date</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-4 date-control">
                              <div class="panel-form-group">
                                <label class="label-control">Drop-Off Time</label>
                                <div class="input-group datepicker">
                                  <input id="start_date33" type="text" placeholder="Thu 17 Aug 2018" class="form-control">
                                  <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</section>

</section>

<a href="#" id="onForgotModal" data-toggle="modal" data-target="#forgotModal" style="display:none;" data-backdrop="static" data-keyboard="false"></a>
<div class="modal fade newModal-container" id="forgotModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
		  	<div class="modal-body">		    
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    @if(session()->has('success'))
				<p class="success-box">{{ session()->get('success') }}</p>
			@else
				<form method="post" id="reset-form" action="/reset-password">
					<h2>Reset Password</h2>
					<br>
					<input type="hidden" name="code" value="{{ $response['code'] }}">
					<input type="hidden" name="token" value="{{ $data['token'] }}">
					<div class="panel-form-group input-control form-group">
						<label for="password" class="label-control">New Password</label>
						<input type="password" id="password" name="password" class="form-control">
					</div>
					 <div class="panel-form-group input-control form-group">
					 	<label for="password_confirmation" class="label-control">Confirm Password</label>
						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
					</div>
					@if ( count( $errors->reset ) )
						<p id="error-msg">
							{{ $errors->reset->first() }}
						</p>
					@endif
					@if(session()->has('error'))
						<p id="error-msg">{{ session()->get('error') }}</p>
					@endif
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row m-t-50">
						<div class="col-sm-12">
							<input type="submit" name="" class="btn btn-black btn-block" value="Submit" id="login-btn">
						</div>
					</div>
				</form>
			@endif
		  	</div>
		</div>
	</div>
</div>
@stop
@section('custom-js')
<script type="text/javascript" src="{{ url('assets/js/theme/default-load.js') }}"></script>

<script type="text/javascript">
    $.validator.addMethod("pwcheck", function(value) {
        return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/.test(value) 
    }, "Password must contain Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:");


    $(document).ready(function() {
        $("#onForgotModal").click();
        $("#reset-form").validate({
            rules: {
                password: {
                  required: true,
                  minlength: 8,    
                  pwcheck : true, 
                },
                password_confirmation: {
                    equalTo : "#password"
                },
            },
            messages: {

                email: {
                    required: "Please enter Email Address."
                },
                password_confirmation: {
                    equalTo: 'Password and Confirm Password must be same.'
                },

            },
            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });


</script>
@stop