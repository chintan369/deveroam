
<?php  

/* start variable declarations */

$months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
// $years = range( date( 'Y' ), date( 'Y')+10 );

/* end variable declarations */

?>


@extends( 'layouts.search' )


@section( 'custom-css' )
<style type="text/css">
	.wrapper {
		background-image: url( {{ url( 'assets/img/bg2.jpg' ) }} ) !important;
	}
	.pi-padding{
		padding:25px;
	}
	.display-inline{
		display:inline-block;
	}
	.pi-data-box-padding{
		padding-right:15px;
	}

	.text-heading-padding{
		padding-top:25px;
	}
	i.bsicon:before{
		font-size: 20px;
		margin-left: 0;
		padding: 5px 11px;
		border:1px solid;
		display: inline-block;
		margin-bottom: 1.4rem;
	}

	.default-button{
		background: #2AA9DF;
	    border-color: #2AA9DF;
	    color: #fff;
	    padding: 10px;
	    font-weight: bold;
	    width: 100%;
	}

	.header-padded-top{
		padding-top:15px;
	}

	.booking-details{

	}

	textarea
	{
	  border:3px solid #999999;
	  border-radius: 2px;
	  width:100%;
	  margin:5px 0;
	  padding:3px;
	}

	.colored-final{
		color:#2AA9DF;
	}
	.form-group{
		
	} 
	
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
		-webkit-appearance: none; 
		margin: 0; 
	}

	.confirm-btn,
	.pax-information {
	    color: #fff;
	    text-transform: uppercase;
	    width:100%;
	    border-radius: 2px;
	    font-size: 16px;
	    float: left;
	    display: block;
	    background: #2AA9DF;
	    padding: 10px;
	    text-decoration: none;
	    transition: .23s;
	}
	.confirm-btn:hover, .confirm-btn:focus,
	.pax-information:hover,
	.pax-information:focus {
	    color: white;
		outline: 0;
    	opacity: 0.8;
	}
	.cc-type{
		text-decoration: none;
	    border-radius: 2px;
	    border: 2px solid transparent;
	    transition: all 200ms ease-in-out 0s;
	    display:inline-block;
  		padding:2px;
  		text-align: center;
	}
	.cc-type:hover, .cc-type:focus, .cc-type-active{
		border-color: #2AA9DF!important; 
	}

</style>
@stop

@section('content')
<div class="row">
	<div class="pi-padding">
		<div class="row">
			<div class="col-md-12" >
				<div class="col-md-6">
					<h4 class="bold-txt text-left">PAX DETAILS</h4>

					<p class="text-heading-padding">Click on the icons below to enter each pax details.</p>

					@for ($i = 0; $i < $data['number_traveller']; $i++)
					    <div class="row">
					    	<div class="col-md-2">
					    		<i class="flaticon-profile bsicon"></i>
					    	</div>
					    	<div class="col-md-10">
					    		<a href="#" class="btn full-width pax-information" data-index = "{{$i}}" data-update="0">PAX DETAILS</a>
					    	</div>
							
						</div>
					@endfor
					<h4 class="bold-txt text-left header-padded-top">BOOKING DETAILS</h4>
					<div class="row" style="margin-top:15px">
						<div class="col-md-6">
					    		<p class="bold-txt">From:</p>
				    	</div>
				    	<div class="col-md-6">
				    			<p class="text-left">{{$data['start_city']}}, {{$data['start_city_country_code']}} </p>
				    	</div>
				    	<div class="col-md-6">
					    		<p class=" bold-txt text-left">To:</p>
				    	</div>
				    	<div class="col-md-6">
				    			<p class="text-left">{{$data['end_city']}}, {{$data['end_city_country_code']}} </p>
				    	</div>
						<div class="col-md-6">
					    		<p class="bold-txt">No of Travellers:</p>
				    	</div>
				    	<div class="col-md-6">
				    			<p class="text-left">({{$data['number_traveller']}})</p>
				    	</div>
				    	<div class="col-md-6">
					    		<p class="bold-txt">Travel Date:</p>
				    	</div>
				    	<div class="col-md-6">
				    			<p class="text-left">{{$data['start_date']}}</p>
				    	</div>
				    	<div class="col-md-6">
					    		<p class="bold-txt">Duration:</p>
				    	</div>
				    	<div class="col-md-6">
				    			<p class="text-left">{{$data['total_number_of_days']}}</p>
				    	</div>
					</div>

					<p class="text-heading-padding">General Information</p>
					<p >Request & Special Requirements eg. Dietary, Medical Special Needs.</p>
					<textarea class="full-width" id="" cols="20" rows="6"></textarea>

				</div>
				<div class="col-md-6">
					
					<h4 class="bold-txt text-left">PAYMENT CONFIRMATION</h4>

					<div class="row" style="margin-top:35px">
						<div class="col-md-6">
					    		<p class="bold-txt">Total Per Person:</p>
				    	</div>
				    	<div class="col-md-3">
				    			<p class="text-left">{{ $data['currency'] }}</p>
				    	</div>
				    	<div class="col-md-3">
					    		<p class=" text-left">{{$data['total_per_person']}}</p>
				    	</div>
				    </div>
				    <div class="row" >	
				    	<div class="col-md-6">
					    		<p class="bold-txt">Sub Total Amount:</p>
				    	</div>
				    	<div class="col-md-3">
				    			<p class="text-left">{{ $data['currency'] }}</p>
				    	</div>
				    	<div class="col-md-3">
					    		<p class=" text-left">{{$data['sub_total']}}</p>
				    	</div>
				    </div>
				    <div class="row" >	
						<div class="col-md-6">
					    		<p class="bold-txt">Credit Card Fee's 2.5%:</p>
				    	</div>
				    	<div class="col-md-3">
				    			<p class="text-left">{{ $data['currency'] }}</p>
				    	</div>
				    	<div class="col-md-3">
					    		<p class=" text-left">{{$data['credit_card_price']}}</p>
				    	</div>
					</div>

					 <div class="row " style="margin-top:20px" >	
						<div class="col-md-6">
					    		<p class="bold-txt text-left colored-final">Total Amount Due:</p>
				    	</div>
				    	<div class="col-md-3">
				    			<p class="bold-txt text-left colored-final">{{ $data['currency'] }}</p>
				    	</div>
				    	<div class="col-md-3">
					    		<p class="bold-txt text-left colored-final">{{$data['total_payment']}}</p>
				    	</div>
					</div>
					<div class="col-md-12" style="margin-top:20px">


					<form action="" class="booking-form">
						<div class="row">

							<div class="col-md-12" style="padding:0px; margin-bottom:15px;">
								<label>Credit Card Type:</label>
								<div class="row">
									<div class="col-md-3">
										<a class="cc-type cc-type-active" data-cc-type="visa">
											<img src="{{ url( 'assets/img/VISA-32.png' ) }}" alt="visa"/>
										</a>
									</div>
									<div class="col-md-3">
										<a class="cc-type" data-cc-type="mastercard">
											<img src="{{ url( 'assets/img/MASTERCARD-32.png' ) }}" alt="mastercard"/>
										</a>
									</div>
									<div class="col-md-3">
										<a class="cc-type" data-cc-type="american_express">
											<img src="{{ url( 'assets/img/AMERICAN-EXPRESS-32.png' ) }}" alt="american-express"/>
										</a>
									</div>
									<div class="col-md-3">
										<a class="cc-type" data-cc-type="discover">
											<img src="{{ url( 'assets/img/DISCOVER-32.png' ) }}" alt="discover"/>
										</a>
									</div>
								</div>
							</div>

							<label>Credit card Number:</label>
							<div class="row" style="margin-left: 0px;">
							
								<div class="col-md-12" style="padding-left: 0;">
							 		<div class="form-group">
							 			<input type="number" class="form-control cc-number" required>
							 		</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="row">
									<div class="form-group "> 
										<label>Credit card CVN Number:</label>
										 <input type="number" class="form-control cc-cvn" required>
									</div>	 
								</div>
								
							</div>

							<div class="col-md-12">
								<div class="row">
									<div class="form-group "> 
										<label>Cardholder Name:</label>
										 <input type="text" class="form-control cc-holder-name" pattern="[A-Za-z]{1,}" title="This field accepts only characters(a-z)." required>
									</div>	 
								</div>
							</div>

							<div class="col-md-12">
								<div class="row">
									<div class="form-group "> 
										<label>Cardholder Email:</label>
										 <input type="email" class="form-control cc-holder-email" required>
									</div>	 
								</div>
								
							</div>
							<div class="col-md-12">
								<div class="row">
									<div class="form-group "> 
										<label>Cardholder Address:</label>
										 <input type="text" class="form-control cc-holder-address" required>
									</div>	 
								</div>
							</div>

							<div class="col-md-12">
								<div class="row">
									<div class="form-group "> 
										<label>Cardholder Postal Code:</label>
										 <input type="number" min='1' class="form-control cc-postal-code" required>
									</div>	 
								</div>
							</div>

							<div class="row" style="margin: 0px;">
								<div class="col-md-12" style="padding: 0px;">
									<label for="exampleInputEmail1">Card Expiry Date:</label>
									<div class="row">
									
										<div class="col-md-6">
									 		<div class="form-group ">
												<select class="form-control cc-expiry-month">
													<option disabled value="" required>Month</option>
													@foreach ($months as $month)
														<option>{{ $month }}</option>
													@endforeach
												</select>
									 		</div>
										</div>

										<div class="col-md-6">
									 		<div class="form-group ">
												<select class="form-control cc-expiry-year">
													<option disabled value="" required>Year</option>
													@for ($year = date('Y'); $year < date('Y') + 11; $year++ )
														<option value="{{ substr($year + 1, 2) }}">{{ $year + 1 }}</option>
													@endfor
												</select>
									 		</div>
										</div>

									</div>	
								</div>
							</div>

							<div class="col-md-12">
								<div class="row">
									<div class="form-group">
										<label>
											<input type="checkbox" name="agree_terms" id="agree-terms" required>
											I have read and agree to the <a href="#" data-toggle="modal" data-target="#terms-modal">Cancellation Terms and Conditions.</a>
										</label>
									</div>	 
								</div>
							</div>

							<br/>
							<div class="col-md-12">
								<div class="row">
									<div class="form-group">
										<div style="min-height:20px;">
											<span id="confirm-booking-error" style="color:rgb(218,44,44); text-align:center; display:none;"></span>
										</div>
										<button class="btn full-width confirm-btn">CONFIRM BOOKING</button>
									</div>	 
								</div>
							</div>

						</div>
					</form>

				</div>

			</div>

		</div>

	</div>

	{{-- TERMS --}}
	<div class="modal fade" id="terms-modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title bold-txt blue-txt">Terms &amp; Conditions</h4>
				</div>
				<div class="modal-body">
					<p >eRoam Pty Ltd ACN 609 719 486 VIC32783 is travel technology company and, in the provision of holidays or travel products acts solely as an agent on behalf of the Operator Services.  eRoam Pty Ltd is not liable to you for any act, default or neglect of any kind by any Operational Services.</p>
					<p>Operator Services are all travel, accommodation and other associated services provided to you by a Third Party tour, travel, accommodation or other operator, booked on your behalf via the eRoam.com interface.</p>
					<p> <strong>Disclaimer:</strong> Due to conditions outside of our control such as natural disaster, weather, politics or operational problems (e.g. Flooding, road and park closures), eRoam Pty Ltd and its suppliers have the right to cancel a sector / sectors and or alter the itinerary. In these circumstances, it is not a guarantee that missed or cancelled sectors will be fully refunded. This will be at the suppliers discretion and eRoam Pty Ltd will charge an administration fee. In circumstances where the cancellation / amendment is due to external events outside our reasonable control, refunds will be less any unrecoverable costs and applicable administration fees. We are not responsible for any incidental expenses that may be incurred as a result of amending / cancelling the booking including but not limited to Travel Insurance excess, non-refundable flights or  alternative travel arrangements i.e.: connecting travel services, additional accommodation, tours or upgrading. This will be at the clients expense. eRoam Pty Ltd will endeavour to organise new arrangements for travel during any of these circumstances. ( Amendment fees will apply as per our usual terms and conditions) However, it will be ultimately be the clients own responsibility. eRoam Pty Ltd cannot be held responsible for any forward travel arrangements that are affected by any of the above. Change or cancellation of forward travel booked through eRoam Pty Ltd will be treated under our normal terms and conditions.</p>
					<p>For example you could create itineraries along the East Coast of Australia (Sydney to Cairns), New Zealand (Auckland to Christchurch), Thailand (Bangkok to Koh Samui), Indochina (Bangkok to Saigon). We also have a full range of tours and packages for travelers that are after more private touring with the additional creature comforts.</p>
					<p>In the unlikely hood that eRoam Pty Ltd or its suppliers, are forced to cancel or alter a service due to non-availability, eRoam Pty Ltd or its suppliers, will offer an alternative service of similar inclusions. Any extra costs incurred from the alternative arrangements, such as additional meals etc will be at the clients expense.</p>
					<p>Prices are subject to change. <br/ >Refunds: Refunds are made in AUD only and eRoam Pty Ltd cannot be held responsible for loss due to bank fees or fluctuations in currency exchange rates. Refunds may take up to 30 days to process.</p>
					<p><strong>Cancellation Fees</strong>: In the event of cancellation the following cancellation fees will be payable by the client:</p>
					<ul>
						<li>Outside 55 days - Deposit only</li>
						<li>42 to 55 days prior - 35% of tour cost plus full Greyhound Pass cost if applicable</li>
						<li>28 to 41 days prior - 50% of tour cost plus full Greyhound Pass cost if applicable</li>
						<li>14 to 27 days prior - 75% of tour cost plus full Greyhound Pass cost if applicable</li>
						<li>Less than 14 days prior - 100% of tour cost, if the deposit is greater than the cancellation fee then the deposit will be forfeited. Amendment Fees: If a date change or itinerary change is done at a Clients request, a fee from $50 to $200 per passenger per change will be charged on top of any applicable supplier fees. </li>
						<li>If these changes fall within the cancellation period, then any applicable cancellation fees will also apply
						</li>
					</ul>
					<p><strong>Travel Insurance</strong>: We strongly recommend clients to purchase travel insurance at the same time as booking travel arrangements to suitably cover medical, personal effects, travel delays and cancellation costs.</p>
					<p>All cancellations or disputes should be sent by registered post to:</p>
					<i>Operations</i>
					<br />
					<i>eRoam Pty Ltd</i>
					<br />
					<i>9/80 Balcombe Rd</i>
					<br/ >
					<i>Mentone 3194</i>
					<br />
					<i>AUSTRALIA</i>
					<br />
					<p>Our company name is eRoam Pty Ltd - Level 3, IBM Centre, 60 City Road, Southbank VIC 3006</p>
					<p><strong>Definitions:</strong></p>
					<ul>
						<li><u>Supplier</u> - A supplier is the third party tour operator or service provider that operates the travel arrangements or services for the Client.</li>
						<li><u>Client</u> - The client is the person that has made the booking with eRoam Pty Ltd and is responsible for adhering to the booking conditions and acts as the sole correspondent for the client and the client's party.</li>
					</ul>
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>

	{{-- CONFIRM BOOKING --}}

	{{-- CONFIRM BOOKING --}}
	<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"> Booking Confirmation </h4>
	      </div>
	      <div class="modal-body">
	      	<center>
		     	<h3 class="text-center">Booking request sent</h3>
		    </center>
	      </div>
	      <div class="modal-footer">
	      </div>
	    </div>
	  </div>
	</div>
	{{-- CONFIRM BOOKING --}}


	{{-- START PAX MODAL --}}
	<div class="modal fade pax-modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title pax-details-modal-title"></h4>
			    </div>
			   
			    <div class="modal-body">
					<div class="col-mid-12">
						 <form class="pax-details-form">
							<div class="col-mid-6">
								<div class="row">
									<div class="col-md-6">
							    		<div class="form-group ">
										    <label>First Name:</label>
										    <input type="name" name="firstname" id="firstname" class="form-control" pattern="[A-Za-z]{1,}" title="This field accepts only characters(a-z)." required>
									    </div>
							    	</div>
							    	<div class="col-md-6">
							    		<div class="form-group ">
										    <label>Last Name:</label>
										    <input type="name" name="lastname" id="lastname" class="form-control" pattern="[A-Za-z]{1,}" title="This field accepts only characters(a-z)." required>
									    </div>
							    	</div>
							    	<div class="col-md-6">
							    		<div class="form-group ">
										    <label>Title:</label>
										    <select id="pax-title" name="pax_title" class="form-control" required>
										    	<option disabled="disabled" value="" selected="selected"> - Select Title - </option>
												<option value="Mr">Mr</option>
												<option value="Ms">Ms</option>
												<option value="Mrs">Mrs</option>
												<option value="Miss">Miss</option>
												<option value="Mstr">Mstr</option>
												<option value="Dr">Dr</option>
												<option value="Prof">Prof</option>
												<option value="Rev">Rev</option>
												<option value="Lord">Lord</option>
												<option value="Lady">Lady</option>
												<option value="Sir">Sir</option>
												<option value="Capt">Capt</option>
											</select>
									    </div>
							    	</div>
							    	<div class="col-md-6">
							    		<div class="form-group ">
										    <label>Gender:</label>
										    <select id="pax-gender" name="pax_gender" class="form-control" required>
										    	<option disabled="disabled" value="" selected="selected"> - Select Gender - </option>
												<option value="male">Male</option>
												<option value="female">Female</option>
											</select>
									    </div>
							    	</div>
							    	
							    	<div class='col-sm-6'>
							            <div class="form-group">
							            	<label>Date of birth:</label> 
							                <input type='text' name="dob" class="form-control" id='datetimepicker4' required/>
							            </div>
							        </div>

							    	<div class="col-md-6">
							    		<div class="form-group ">
										    <label for="exampleInputEmail1">Nationality:</label>
										    <select id="nationality" name="nationality" class="form-control" required>
										    	<option disabled="disabled" value="" selected="selected"> - Select Nationality - </option>
												@foreach( $data['featured_nationalities'] as $featured )
													<option value="{{ $featured->name }}">{{ $featured->name }}</option>
												@endforeach
												<option disabled="disabled">---------------</option>
												@foreach( $data['not_featured_nationalities'] as $not_featured )
													<option value="{{ $not_featured->name }}">{{ $not_featured->name }}</option>
												@endforeach
											</select>
									    </div>
							    	</div>

							    	<div class="col-md-6">
							    		<div class="form-group ">
										    <label>Email:</label>
										     <input type="email" name="email" id="email" class="form-control" required>
									    </div>
							    	</div>

							    	<div class="col-md-6">
							    		<div class="form-group ">
										    <label>Contact #:</label>
										     <input type="number" name="contactno" id="contactno" class="form-control" required>
									    </div>
							    	</div>

							    	<div class="col-md-6 col-md-offset-3">
										<input type="hidden" id="hidden-pax-id" value="1">
										<span id="pax-details-error-text" style="color: rgb(218, 44, 44); text-align: center; display: none;">All fields are required.</span>
										<button style="width: 48%; margin-top: 11px;" class="btn btn-primary save-pax-details-btn">Save</button>	
										<button class="btn btn-default" style="width: 48%; margin-top: 11px;" data-dismiss="modal">Cancel</button>	
									</div>
							    
							    </div>
							</div>
						</form>
					</div>
				</div>
				 
		    </div>
		</div>

	</div>
	{{-- END PAX MODAL --}}


	{{-- START SERVICES AVAILABILITY AND PRICES CONFIRMATION MODAL --}}
	<div class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="booking-confirm-modal" id="booking-confirm-modal">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header" style="border-bottom:none!important;padding: 15px 15px 0 15px;">
					<h4 class="modal-title">Confirming Prices and Availabilities</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-offset-1 col-md-10 booking-confirm-modal-body">
							
						</div>
					</div>
				</div>
				<div class="modal-footer" style="background: none;">
					<div class="row">
						<div class="col-md-6">
							<button type="button" class="btn confirm-booking-btn">Book Now</button>
						</div>
						<div class="col-md-6">
							<button type="button" class="btn booking-continue-btn">Continue editing</button>
						</div>	          
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	{{-- END SERVICES AVAILABILITY AND PRICES CONFIRMATION MODAL --}}



</div>


@stop

@section( 'custom-js' )

<script src="https://secure.ewaypayments.com/scripts/eCrypt.js"></script>

<script>

	$(function () {
	 	$( "#datetimepicker4" ).datetimepicker({
	 		format: 'Y-m-d',
			timepicker : false
	 	})
     });

	var search_session = JSON.parse( $('#search-session').val() );

	// START DOCUMENT READY
	$(document).ready(function(){

		testFormat( search_session );

		$('.cc-type').click(function(e){
			var data = $(this).data();
			var thisElement = $(this);
			$('.cc-type').removeClass('cc-type-active');
			thisElement.addClass('cc-type-active');
		});


		$('.date').click(function(){
			$('#datetimepicker1').datetimepicker('show');
		});

		// console.log("search_session", search_session);

		if( search_session.pax_information.length > 0 ) // check if the key "pax_information" exists
		{ 
			// console.log('naa siyay key');
			search_session.pax_information.forEach(function( pax, key ){
				var paxText = pax.firstname + " " + pax.lastname;
				$('.pax-information[data-index="'+key+'"]').html( paxText.toUpperCase() ).data('filled-out', 'true');
			});
		}

		// EVENT HANDLERS
		$('body').on('click', '.pax-information', function(){
			var index      = $(this).data('index');
			var update     = $(this).data('update');
			var modalTitle = ( index == 0 ) ? 'Personal Information (Lead Person)' : 'Personal Information';
			$('.pax-details-form').data('index', index);
			$('.pax-details-modal-title').html( modalTitle );

			// check if pax details already exist
			if( isNotUndefined( search_session.pax_information[ index ] ) )
			{
				var pax = search_session.pax_information[ index ];
				$('input[name="firstname"]').val( pax.firstname );
				$('input[name="lastname"]').val( pax.lastname );
				$('select[name="pax_title"]').val( pax.pax_title );
				$('select[name="pax_gender"]').val( pax.pax_gender );
				$('select[name="nationality"]').val( pax.nationality );
				$('input[name="dob"]').val( pax.dob );
				$('input[name="email"]').val( pax.email );
				$('input[name="contactno"]').val( pax.contactno );
			}
			else
			{
				$('.pax-details-form')[0].reset();
			}
			$('.pax-modal').modal();
		});


		$( ".pax-details-form" ).on( "submit", function( event ) {
			event.preventDefault();
			var index           = $(this).data('index');
			var pax_information = {};
			var form_data       = $(this).serializeArray();

			if( index == 0 ){
				pax_information.lead_person = 'yes';
			}
			$.each(form_data, function (k, v) {
				pax_information[v.name] = v.value;
			});
			$( ".pax-information[data-index='"+index+"']" ).text( ($( "#firstname" ).val(  ) + " " + $( "#lastname" ).val(  )).toUpperCase() );

		  	search_session.pax_information[ index ] = pax_information;
		    bookingSummary.update( JSON.stringify( search_session ) );
		    console.log('stored pax details', search_session );

			$('.pax-modal').modal('hide');
		});


		$( ".booking-form" ).submit(function( e ) {
			e.preventDefault(); 
			$('#confirm-booking-error').html('');
			var pax_information = search_session.pax_information;
			if( pax_information.length != parseInt( search_session.travellers ) )
			{
				$('#confirm-booking-error').html('Fill out all the pax details.').show();
				return false;
			}

			var user_id = "{{ $data['user_id'] }}";

			// added by miguel 2017-03-13; timezone data used for customer email notification
			var tz = jstz.determine(); 
			var data = {
				user_id : user_id,
				timezone : tz.name(),
				search_session : JSON.stringify( search_session )
			};
			console.log('data', data);
			var itineraryRQ = eroam.apiDeferred( 'itinerary/book', 'POST', data , 'itinerary' );

			// var data = {
			// 	allData: formatAllData( search_session ),
			// 	auth: {
			// 		type: 'customer',
			// 		auth: '0'
			// 	}
			// }

			// console.log( data );
			// return false;

			// var itineraryRQ = eroam.apiDeferred( 'itinerary/create', 'POST', data , 'itinerary' );
			eroam.apiPromiseHandler( itineraryRQ, function( itineraryRS ){
				console.log( 'itineraryRS', itineraryRS );
			});
			
			$('#confirm-modal').modal('show');
		}); 

	}); // END DOCUMENT READY


	function testFormat( search_session ){
		var itinerary = [];
		search_session.itinerary.forEach(function( leg, legKey ){
			itinerary.push({
				city : leg.city,
				hotel : eroam.formatHotel( leg.hotel, leg.city ),
				activities : eroam.formatActivities( leg.activities ),
				transport : eroam.formatTransport( leg.transport )
			});
		});
		console.log( 'itinerary', itinerary );
	}


	// function formatAllData( search_session )
	// {
	// 	var s          = search_session;
	// 	var cities     = [];
	// 	var hotels     = [];
	// 	var activities = [];
	// 	var transports = [];
	// 	var tz         = jstz.determine(); 
		
	// 	var subTotalPerPerson = parseFloat( s.cost_per_person.replace(",", "") );
	// 	var subTotalAmount    = parseFloat( subTotalPerPerson * parseInt( s.travellers ) );
	// 	var CCFee             = parseFloat( ( 2.5 * subTotalAmount ) / 100 );
	// 	var totalAmount       = parseFloat( CCFee + subTotalAmount );
	// 	var totalPerPerson    = parseFloat( totalAmount / parseInt( s.travellers ) );

	// 	var lastCityLeg = s.itinerary.length - 1;

	// 	s.itinerary.forEach(function(v, k){
	// 		cities.push( v.city.id );
	// 		hotels.push( formatHotel( v.hotel, v.city ) );
	// 		activities.push( formatActivities( v.activities ) );
	// 		if( k != lastCityLeg ){ transports.push( formatTransport( v.transport ) ); }
	// 	});

	// 	var result = {
	// 		allData : {
	// 			itinerary : {
	// 				cities : cities, 
	// 				hotels : hotels,
	// 				activities : activities,
	// 				transports : transports,
	// 				transportPasses : [],
	// 				travellers : parseInt( s.travellers ),
	// 				travelDate : s.travel_date,
	// 				totalDays : parseInt( splitString( s.total_number_of_days, " " )[0] ),
	// 				totalAmount : totalAmount.toFixed(2),
	// 				totalPerPerson : totalPerPerson.toFixed(2),
	// 				currency : s.currency,
	// 				timezone : tz.name(),
	// 			},
	// 			booking : {
	// 				paxDetails : s.pax_information,
	// 				bookingSummary : {
	// 					totalAmount : totalAmount.toFixed(2),
	// 					totalPerPerson : totalPerPerson.toFixed(2),
	// 					subTotalAmount : subTotalAmount.toFixed(2),
	// 					additionalFees : CCFee.toFixed(2),
	// 					currency : s.currency 
	// 				},
	// 				specialRequirements : '',
	// 				paymentMethod : {
	// 					paymentMethod : {
	// 						paymentMethod: 'credit_card',
	// 						creditCardDetails: getCreditCardDetails(),
	// 						passportAndCreditCard: getPassportAndCreditCard()
	// 					}, 
	// 					passportAndCreditCard : '',
	// 					creditCardDetails : {
	// 						totalAmount : totalAmount.toFixed(2),
	// 						currency : s.currency
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}

	// 	return result;
	// }



	function getCreditCardDetails()
	{
		var ccNumber = '';
		var cvn = $( '.cc-cvn' ).val().replace(" ", "");
		$( '.cc-number' ).filter( function(  ) {
			ccNumber += $( this ).val();
		});

		creditCardDetails = {
			creditCardType: $('.cc-type-active').data('cc-type'),
			cardHolderName: $( '.cc-holder-name' ).val().trim(),
			cardHolderEmail: $( '.cc-holder-email' ).val().replace(" ", ""),
			cardHolderAddress: $( '.cc-holder-address' ).val(),
			cardHolderPostalCode: $( '.cc-postal-code' ).val(),
			cardExpiryDate: $( '.cc-expiry-month').val() + " " + $( '.cc-expiry-year' ).val(),
			ewayEncryptedCardNumber: eCrypt.encryptValue( ccNumber.replace(" ", ""), eroam.getEwayKey() ),
			ewayEncrypedCvnNumber: eCrypt.encryptValue( cvn, eroam.getEwayKey() )
		}

		return creditCardDetails;
	}

	function getPassportAndCreditCard(){
		return {};
	}



</script>
@stop


