@extends('layouts.search')

@section( 'custom-css' )
<style type="text/css">
	.body-container{
		
		margin-top:40px;
		padding:8px 0px 8px 0px;
		
	}
	.border{
		border:1px solid red;
	}

	.small-image img{
	    width: 100%;
	    height: 170px;
	    background-size: contain !important;
	    background-position: center !important;
	    background-repeat: no-repeat !important;
	    background-color: #2B2B2B !important;
	  

	}
	.act-btn{
		color: #fff;
	    text-transform: uppercase;
	    margin: 10px 9px;
	    width: 100%;
	    border-radius: 2px;
	    font-size: 16px;
	    float: left;
	    display: block;
	    background: #2AA9DF;
	    padding: 10px;
	    text-decoration: none;
	    transition: .23s;
	}
	.act-btn-less{
		color: #fff;
	    text-transform: uppercase;
	    margin: 12px 9px;
	    width: 100%;
	    border-radius: 2px;
	    font-size: 16px;
	    float: left;
	    display: block;
	    background: #2AA9DF;
	    padding: 10px;
	    text-decoration: none;
	    transition: .23s;
	}

	.main-body{
		margin-top: 30px;
		margin-left:5px;
	}
	.tab-button{
		color:black;
		width: 33%;
	    border-radius:0px;
	    border:1px solid;
	    background:transparent;
	    text-transform: uppercase;
	    margin-left:-4px;
	    padding:12px;
	}

	.tab-button.focus{
		background-color: #2AA9DF;
		color:white;
		border-radius:2px;
		border:1px solid #2AA9DF;
		outline:0;
		
	}
	.details{
		padding:15px 2px 5px 2px;
	}
	.default{
		background-color: #2AA9DF;
		color:white;
		border:2px solid #2AA9DF;
		outline:0;
	}

	.thumbnail-image{
	    width: 90%;
    	height: 100px;
	    background-size: contain !important;
	    background-position: center !important;
	    background-repeat: no-repeat !important;
	    background-color: transparent !important;

	}
	.thumbnail-image:hover{
		border:3px solid #2AA9DF;
	}
	.responsive{
		margin-top:30px;
		margin-bottom: 20px;
	
	}
	.images{
		margin-right:18px;
		
	}
	.thumb{
		height: 400px;	
	
	}
	.selected{
		border: 3px solid #2AA9DF;
	}

	/* Style the Image Used to Trigger the Modal */
	#myImg {
	    border-radius: 5px;
	    cursor: pointer;
	    transition: 0.3s;
	    
	}

	#myImg:hover {opacity: 0.7;}

	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 50px; /* Location of the box */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.7); /* Black w/ opacity */
	    border:2px solid white;
	}

	/* Modal Content (Image) */
	.modal-content {
	    margin: auto;
	    display: block;
	    max-width: 600px;
	   border:0px;
	}

	/* Caption of Modal Image (Image Text) - Same Width as the Image */
	#caption {
	    margin: auto;
	    display: block;
	    width: 80%;
	    max-width: 700px;
	    text-align: center;
	    color: #ccc;
	    padding: 10px 0;
	    height: 150px;
	}

	/* Add Animation - Zoom in the Modal */
	.modal-content, #caption { 
	    -webkit-animation-name: zoom;
	    -webkit-animation-duration: 0.6s;
	    animation-name: zoom;
	    animation-duration: 0.6s;
	}

	@-webkit-keyframes zoom {
	    from {-webkit-transform:scale(0)} 
	    to {-webkit-transform:scale(1)}
	}

	@keyframes zoom {
	    from {transform:scale(0)} 
	    to {transform:scale(1)}
	}

	/* The Close Button */
	.close {
	    position: absolute !important;
	    top: 30px !important;
	    right: 300px !important;
	    color: #f1f1f1 !important;
	    font-size: 40px !important;
	    font-weight: bold !important;
	    transition: 0.3s !important;
	}
	

	.close:hover,
	.close:focus {
	    color: #bbb;
	    text-decoration: none;
	    cursor: pointer;
	}

	/* 100% Image Width on Smaller Screens */
	@media only screen and (max-width: 700px){
	    .modal-content {
	       
	        width: 100%;
  			max-width: 600px;
	    }
	}

	.map-show {
	    height: 250px;
	    width: 670px;
	    margin-left: -22px;

	}
	#pac-input{
		margin-bottom: 1px;
	}

	.carousel-fade .carousel-inner .item {
	  opacity: 0;
	  -webkit-transition-property: opacity;
	  -moz-transition-property: opacity;
	  -o-transition-property: opacity;
	  transition-property: opacity;
	}
	.carousel-fade .carousel-inner .active {
	  opacity: 1;
	}
	.carousel-fade .carousel-inner .active.left,
	.carousel-fade .carousel-inner .active.right {
	  left: 0;
	  opacity: 0;
	  z-index: 1;
	}
	.carousel-fade .carousel-inner .next.left,
	.carousel-fade .carousel-inner .prev.right {
	  opacity: 1;
	}
	.carousel-fade .carousel-control {
	  z-index: 2;
	}

	.close-btn:before {
		position: absolute;
		right: -40px;
		top: -35px;
		font-size: 3rem;
		color: #ececec;
		z-index: 999;
		cursor: pointer;
		transition: .21s ease-ine;
	}
	.close-btn:hover {
		color: #ffffff;
	}
	.flaticon-left-arrow{
		margin-left: -40px !important;
		
	}
	
	
</style>
@stop

@section('content')

<div class="col-md-12">
	<div class="col-md-12 body-container">
		<div class="col-md-12">
			<div class="col-md-4 small-image">
				<img src="{{$hotel['images'][0]->path}}" alt="">
			</div>
			<div class="col-md-5 ">
					<h4 class="blue-txt bold-txt">{{$hotel['name']}}</h4>
					{{ dd($hotel) }}
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<p> {{$hotel['name']}} </p>
				<div class="row">
					<div class="col-md-4">
						Destination:
					</div>
					<div class="col-md-8">
						{{session()->get('current_city')}}
					</div>
					<div class="col-md-4">
						Address:
					</div>
					<div class="col-md-8">
						{{$hotel['address']}}
					</div>
					<div class="col-md-4">
						Telephone:
					</div>
					<div class="col-md-8">
						{{$hotel['phones'][0]->phoneNumber}}
					</div>
				</div>
			</div>
			<div class="col-md-3 ">	
				<a href="{{ URL::previous() }}"><button class="btn act-btn-less blue-button bold-txt">View Less</button></a>
				
			</div>
	
		</div>

		<div class="col-md-12 main-body">
			<div class="col-md-12">
				<button class="btn bold-txt tab-button button-image default">Images</button>
				<button class="btn bold-txt tab-button button-detail">Detail</button>
				<button class="btn bold-txt tab-button button-location">Location</button>
			</div>

			<div class="col-md-12">
				
				
				<div class="images" >
					

					<div class="col-md-12" style="margin-bottom: 20px;">
					 @foreach ($hotel['images'] as $key => $image)
						
						<div class="col-md-3" id="myImg" style="padding-top:10px" >
							<div  class="thumbnail-image  {{($key == 0) ? 'selected':'' }}" style="background-image: url( {{ $image->path}} )" data-path="{{$image->path}}" data-context = "{{$hotel['name']}}"></div>
						</div>
					 @endforeach  
					</div> 

				</div>
				<div class="details" style="display:none" >
					<h3>DESCRIPTION</h3>
					{!!$hotel['description']!!}	
					<br/>

					
				</div>
				<div class="location" style="display:none">
					<div class="col-md-12 responsive" style="margin-bottom: 20px;">
						<div class="col-md-3"></div>
						<div class="col-md-9 full-right">
							<input id="pac-input"  class="form-control full-width" type="text" readonly value="{{$hotel['name']}}, {{$hotel['address']}}" placeholder="Search Box">	
						</div>
						
						<div class="map-show" id="googleMap"></div>
					</div>	
				</div>
			</div>
		</div>
	</div>
		
	
	
</div>


<!-- The Modal -->
<div id="myModal" class="modal">
  	<div class="modal-content">
		<span class="close-btn flaticon-multiply"></span>
	
		<div id="myCarousel" class="carousel carousel-fade" data-ride="carousel" data-interval="false">
		
		  <!-- Wrapper for slides -->
		  <div class="carousel-inner responsive" role="listbox">
			   @foreach ($hotel['images'] as $key => $image)
				
				 <div class="item {{($key == 0) ? 'active':'' }}" data-path-top="{{ $image->path}}">
			       	<div class="thumb" style="background-image: url( {{ $image->path}} )"></div>
			     </div>  
			  @endforeach  
				
		  </div>
		 
		 
		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left flaticon-left-arrow" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel " role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right flaticon-right-arrow" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		  
		</div>
					


  </div>
  <div id="caption"></div>
</div>



@stop

@section( 'custom-js' )

<script>
	
	$(document).ready(function(){
		$('body').on('click', '.tab-button', function(e){
			e.preventDefault();
			$(".button-image").removeClass("default");
			var myClass = $(this).attr('class');
			myClass = myClass.split(' ');
			//get the last element of an array to check what would be displayed
			// jayson suizo added
			// this should output the ff::
			// button-detail
			// button-itinerary
			// button-image
			// button-location
			// jayson suizo added end
			var last = myClass[myClass.length-1];
			if(last == "button-detail"){
				$('.details').show();
				$('.rooms').hide();
				$('.images').hide();
				$('.location').hide();
			}
			
			if(last == "button-image"){
				$('.details').hide();
				$('.rooms').hide();
				$('.images').show();
				$('.location').hide();
			}
			if(last == "button-location"){
				$('.details').hide();
				$('.rooms').hide();
				$('.images').hide();
				$('.location').show();
				var lat = {{$hotel['latitude']}};
				var lang = {{$hotel['longitude']}};
				var hotel = '{{$hotel['name']}}';
				initialize(lat,lang,hotel);
			}

			$('.tab-button').removeClass('focus');
			$(this).addClass('focus');

		});

		$('body').on('click', '.thumbnail-image', function(e){
			 $('.thumbnail-image').removeClass("selected");			
			 $(this).addClass('selected');
			 $('.item').removeClass('active');
			 var path = $(this).data('path');
			 var caption = $(this).data('context');
			 $("div").find("[data-path-top='" + path + "']").addClass('active');
		     var modal = document.getElementById('myModal');

			// Get the image and insert it inside the modal - use its "alt" text as a caption
			var img = document.getElementById('myImg');
			var modalImg = document.getElementById("img01");
			var captionText = document.getElementById("caption");
		    modal.style.display = "block";
		    modalImg.src = path;
		    captionText.innerHTML = caption;
			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() { 
			    modal.style.display = "none";
			}
		   
		     // Get the modal
			var modalImg = document.getElementById("img01");
			var captionText = document.getElementById("caption");
			
		   //  modal.style.display = "block";
		    modalImg.src = path;
		    captionText.innerHTML = caption+ " images";
	
		});

		$('body').on('click', '.left', function(e){
			 var path = $('div.active').prevAll().data('path-top');
			 $('.thumbnail-image').removeClass('selected');
			 if (path === undefined) {
			    $('.thumbnail-image').last().addClass('selected');	
		     }else{
		     	$("div").find("[data-path='" + path + "']").addClass('selected');
		     } 
		});

		$('body').on('click', '.right', function(e){
			 var path = $('div.active').nextAll().data('path-top');
			 $('.thumbnail-image').removeClass('selected');
			 if (path === undefined) {
			    $('.thumbnail-image').first().addClass('selected');	
		     }else{
		     	$("div").find("[data-path='" + path + "']").addClass('selected');
		     } 
		});

		$('body').on('click','.close-btn',function(e){
			$('.modal').hide();
		});

	
				
	});
	 var map;
      function initialize(lat,lang,name) {
        var mapOptions = {
          zoom: 13,
          center: {lat: lat, lng: lang},
        };
        map = new google.maps.Map(document.getElementById('googleMap'),
            mapOptions);

        var marker = new google.maps.Marker({
          // The below line is equivalent to writing:
          // position: new google.maps.LatLng(-34.397, 150.644)
          position: {lat: lat, lng: lang},
          map: map,
          
        });
        

        // You can use a LatLng literal in place of a google.maps.LatLng object when
        // creating the Marker object. Once the Marker object is instantiated, its
        // position will be available as a google.maps.LatLng object. In this case,
        // we retrieve the marker's position using the
        // google.maps.LatLng.getPosition() method.
        var infowindow = new google.maps.InfoWindow({
          content: '<p>Hotel Name: ' + name + '</p>'
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map, marker);
        });
      }

	
	
	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}

	function getHotelCategory(code){
		var stars;
	
		switch( code ){
			case '5EST':
			case 'HS5':
			case 'APTH5':
				stars = getStars( 5, false );
			break;
			case '4EST':
			case 'HS4':
			case 'APTH4':
				stars = getStars( 4, false );
			break;
			case '3EST':
			case 'HS3':
			case 'APTH3':
				stars = getStars( 3, false );
			break;
			case '2EST':
			case 'HS2':
			case 'APTH2':
				stars = getStars( 2, false );
			break;
			case '1EST':
			case 'HS1':
			case 'APTH1':
				stars = getStars( 1, false );
			break;
			case 'H4_5':
				stars = getStars( 4, true );
			break;
			case 'H3_5':
				stars = getStars( 3, true );
			break;
			case 'H2_5':
				stars = getStars( 2, true );
			break;
			case 'H1_5':
				stars = getStars( 1, true );
			break;
			
		}
			
		return stars;
	}

	function getStars(count, half = false){
		var stars = '';
		if( parseInt( count ) ){
			for( star = 1; star <= count; star ++ ){
				stars += '<i class="fa fa-star"> </i> ';
			}
			if(half){
				stars += '<i class="fa fa-star-half-o"> </i> ';
			}
		}else{
			//for( star = 1; star <= 5; star ++ ){
				stars += '<i class="fa fa-star-o"> <strong>Unranked</strong> </i>   ';
			//}
		}
		return stars;
	}

</script>
	
@stop