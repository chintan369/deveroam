@extends('layouts.search')
<?php
//echo "<pre>";print_r(session()->get('search'));exit;
$total = $totalCost * $travellers;
$gst = $total * 0.025;
$finalTotal = number_format($total + $gst, 2, '.', ',');
$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
?>
@section('custom-css')
	<style type="text/css">
		#itr {
			padding: 2rem;
		}
		.itr-leg {
			background: #ffffff;
			margin-bottom: 2rem;
		}
		.itr-leg .heading {
			border: 1px solid #B2B2B2;
			border-bottom: 1px solid white;
			position: relative;
			bottom: -1px;
			display: inline-block;
			font-weight: 400;
			margin: 0;
			padding: 2rem;
		}
		.itr-leg .heading i:before {
			font-size: 25px;
			color: #2AA9DF;
		}
		.itr-leg .body {
			border: 1px solid #B2B2B2;
			padding: 2rem;
		}
		.itr-leg .body section {
			margin-bottom: 2rem;
		}
		.itr-leg .body .title {
			font-size: 17px;
			text-transform: uppercase;
		}
		.itr-leg .body .desc {
			margin-left: 8.8rem;
		}
		.itr-leg .body .desc p {
			line-height: 1em;
		}
		.itr-leg .body .desc p i.status-icon {
			margin-left: 1.5rem;
			margin-right: .2rem;
			color: #009688;
			font-weight: 700;
		}
		span.not-available,
		span.not-available i {
			color: #d32f2f !important;
		}
		.itr-leg .body .desc p span.price {
			font-weight: 700;
			font-size: 18px;
			color: #009688;
		}
		.itr-leg .body .title i:before {
			position: relative;
			top: 5px;
			border-radius: 100px;
			padding: 1.5rem;
			color: #ffffff;
			font-size: 25px;
			margin-right: 2.8rem;
		}
		.itr-leg .body .accomodation .title i:before {
			background: #00bcd4;
		}
		.itr-leg .body .activities .title i:before {
			background: #2196f3;
		}
		.itr-leg .body .transport .title i:before {
			background: #f44336;
		}
		.change-btn i {
			font-size: 1.6rem;
			margin-left: 3rem;
			color: #f8f8f8 !important;
			background: #2AA9DF;
			padding: 8px;
			display: inline-block;
			transition: all 100ms ease-in-out 0s;
		}
		.change-btn i:hover {
			text-decoration: none;
			background: #2792BF;
		}
		.old-price {
			text-decoration: line-through;
			color: #ABABAB;
			font-weight: 400;
			margin-left: 1rem;
		}
		#review-summary {
			border: 1px solid #B2B2B2;
			padding: 5px;
			margin-top: 11px;
		}
		#review-summary h3 {
			margin-top: 0;
		}
		#total {
			font-size: 16px;
		}
		.white-bg{min-height: initial;}
	</style>
@endsection
@section('content')
    <?php
    $travellers = session()->get('search')['travellers'];
    $total_childs = session()->get('search')['child_total'];
    $rooms = session()->get('search')['rooms'];
    ?>
	<div class="tabs-container">
		<div class="tabs-container-inner">
			<h2 class="topTitle"><i class="icon icon-itinerary"></i><span>Book Itinerary</span></h2>
			<div class="m-t-20 row">
				<div class="col-sm-4">
					<a href="{{ url('/view-itinerary') }}" name="" class="btn btn-primary btn-block m-b-10">VIEW ITINERARY</a>
				</div>
				<div class="col-sm-4">
					<a href="{{ url('/proposed-itinerary-pdf/') }}" target="_blank"  class="btn btn-primary btn-block m-b-10">PRINT / SHARE ITINERARY</a>
				</div>
				<div class="col-sm-4">
					<a href="#" name="" class="btn btn-primary btn-block active m-b-10">SAVE ITINERARY</a>
				</div>
			</div>
		</div>
	</div>
	<div class="tabs-content-container">
		<div class="accomodation-wrapper">
			<div class="paymentTopbox">
				<div class="row">
					<div class="col-sm-7 m-t-5">
						<h4><strong>Multi-City Tailormade Auto</strong></h4>
						<p class="m-t-10">
							<i class="icon icon-calendar"></i>
							{{$startDate}} - {{$endDate}}
						</p>
					</div>
					<div class="col-sm-5">
						<div class="row">
							<div class="col-xs-9 price-right border-right">
								<p></p>
								<ul class="paymentIcons">
									<li><a href="#"><i class="icon icon-hotel"></i></a></li>
									<li><a href="#"><i class="icon icon-activity"></i></a></li>
									<li><a href="#"><i class="icon icon-bus"></i></a></li>
								</ul>
							</div>
							<div class="col-xs-3 text-center">
								<h3 class="transport-price"><strong>{{$totalDays}}</strong></h3>
								<span class="transport-nights">Nights</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="paymentCityBox">
				<div class="table-responsive">
                    <?php
                    $i = 1;
                    $activities = '';
                    $transport = '';
                    $hotel = '';
                    $total_pax = session()->get('search')['travellers'];
                    $itineraryNumber = 0;
                    ?>
					@if ($data['itinerary'])
						@foreach ($data['itinerary'] as $key => $leg)
						<?php
							//echo "<pre>"; print_r($leg); exit();
							if(isset($leg['transport']['provider']) && ( $leg['transport']['provider'] == 'mystifly' || trim($leg['transport']['provider']) == 'busbud') ):
								$transportTotalPrice = $leg['transport']['price'][0]['price'];
								$transportPerPersonPrice = ($leg['transport']['price'][0]['price'] / $travellers);
							else:
								$transportTotalPrice = ($leg['transport']['price'][0]['price'] * $travellers);
								$transportPerPersonPrice = $leg['transport']['price'][0]['price'];
							endif;
						?>
							<table class="table">
								<thead>
								<tr>
									<th width="384px"><h4>{{$leg['city']['name']}}</h4></th>
									<th width="141px" class="text-center">Cost</th>
									<th width="100px"class="text-center">Quantity</th>
									<th width="168px" class="text-right">Total Cost</th>
								</tr>
								</thead>
								<tbody>
								@if(isset($leg['hotel']) && !empty($leg['hotel']))
									<tr>
										<td>
											<div class="cityboxIcon">
												<i class="icon icon-hotel"></i>
											</div>
											<div class="cityboxDetails">
												<strong>{{ $leg['hotel']['name'] }}</strong><br/>
												{{ $leg['hotel']['checkin'] }} - {{ $leg['hotel']['checkout'] }}<br/>
												{{ $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'] }}<br/>
												{{ ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['nonRefundable']) ? "Non-Refundable" : "" }}
											</div>
										</td>
										<td class="text-center">${{$currency}} {{$leg['hotel']['total_price']}}</td>
										<td class="text-center">
											<input type="text" name="" value="1" class="qty-inputControl" />
										</td>
										<td class="text-right">
											<strong>${{$currency}} {{$leg['hotel']['total_price']}}</strong><br/>
											<a href="#"  class="blue-text">Change Dates</a><br/>
											@if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']) && ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'] != ""))
												<a href="#" data-target="#cancellationPolicy{{$i}}" data-toggle="modal" class="blue-text">Cancellation Policy</a><br/>
											@endif
											<a href="/remove-itinerary/{{$itineraryNumber}}/hotel/" class="blue-text">Remove From Itinerary</a>
										</td>
									</tr>
									@if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']) && ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'] != ""))
										<div class="modal fade in" id="cancellationPolicy{{$i}}" tabindex="-1" role="dialog" style="display: none;">
											<div class="modal-dialog modal-md" role="document">
												<div class="modal-content">
													<div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
														<h4 class="modal-title" id="gridSystemModalLabel">Cancellation Policy</h4>
													</div>

													<div class="modal-body">
														<div class="roomType-inner m-t-20">
															<div class="m-t-20">
																<p>{{$leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']}}</p>
															</div>

															<div class="m-t-30 text-right">
																<a href="#" data-dismiss="modal" class="modalLink-blue">CLOSE</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									@endif
                                    <?php $i++;?>
								@endif
								@if(isset($leg['activities']) && !empty($leg['activities']))
                                    <?php $j = 0;?>
									@foreach($leg['activities'] as $activity)
										<?php 
											if (!isset($activity['duration1'])) {
											  $activity['duration1'] = $activity['duration'];
											}
										?>
										<tr>
											<td>
												<div class="cityboxIcon">
													<i class="icon icon-activity"></i>
												</div>
												<div class="cityboxDetails">
													<strong>{{isset($activity['name'])?$activity['name']:''}}</strong><br/>
													{{isset($activity['date_selected'])?date('d M Y', strtotime($activity['date_selected'])):''}}<br/>
													Duration: {{(isset($activity['duration1']))?$activity['duration1'].' (approx.)':'Not Provided'}}
												</div>
											</td>
											<td class="text-center">${{$currency}} {{ ( $activity['price'][0]['price'] ) }}</td>
											<td class="text-center">
												<input type="text" name="" value="{{ $travellers }}" class="qty-inputControl" /></td>
											<td class="text-right">
												<strong>${{$currency}} {{ ( $activity['price'][0]['price'] * $travellers) }}</strong><br/>
												<a href="#" class="blue-text">Change Dates</a><br/>
												<a href="/remove-itinerary/{{$itineraryNumber}}/activities/{{$j}}" class="blue-text">Remove From Itinerary</a>
											</td>
										</tr>
                                        <?php $i++;
                                        $j++;?>
									@endforeach
								@endif
								@if(isset($leg['transport']) && !empty($leg['transport']))
									<tr>
										<td>
											<div class="cityboxIcon">
												<i class="icon icon-bus"></i>
											</div>
											<div class="cityboxDetails">
                                                <?php
                                                $travelDate = explode('<br/>', $leg['transport']['departure_text']);
                                                $travelDate = explode('@', $travelDate[1]);
                                                ?>
												<strong>{{$leg['transport']['transport_name_text']}}</strong><br/><?php echo date('d M Y', strtotime($travelDate[0])); ?><br/>Duration: {{ str_replace('+', '', $leg['transport']['duration']) }}
											</div>
										</td>
										<td class="text-center">
											${{$currency}} {{ $transportPerPersonPrice }} </td>
										<td class="text-center">
											<input type="text" name="" value="{{ $travellers }}" class="qty-inputControl" />
										</td>
										<td class="text-right">
											<strong>${{$currency}} {{ $transportTotalPrice }}</strong><br/>
											<a href="#" class="blue-text">Change Dates</a><br/>
											<a href="/remove-itinerary/{{$itineraryNumber}}/transport/" class="blue-text">Remove From Itinerary</a>
										</td>
									</tr>
                                    <?php $i++;?>
								@endif
								</tbody>
							</table>
                            <?php $itineraryNumber++;?>
						@endforeach
						<table class="table">
							<tbody>
							<tr></tr>
							<tr>
								<td>
									<p class="blue-text">Save Itinerary</p>
									<p class="blue-text">View Saved Itineraries</p>
									<p class="blue-text">Enter Promo Code</p>
									<p class="m-t-20">Contact Us <a href="#" class="blue-text">+61 (0)3 9999 6774</a></p>
								</td>
								<td class="text-right" colspan="2">
									<p>Total Per Person</p>
									<p>Sub Total Amount</p>
									<p>Credit Card Fee 2.5%</p>
									<p class="m-t-20"><strong>Total Amount</strong></p>
								</td>
								<td class="text-right">
									<p>${{$currency}} {{number_format($totalCost, 2, '.', ',')}}</p>
									<p>${{$currency}} {{number_format($total, 2, '.', ',')}}</p>
									<p>${{$currency}} {{number_format($gst, 2, '.', ',')}}</p>
									<p class="m-t-20"><strong>$AUD {{$finalTotal}}</strong></p>
								</td>
							</tr>
							</tbody>
						</table>
					@endif
				</div>
				<a href="{{url('/signin-guest-checkout/')}}" name="" class="btn btn-black btn-block">CHECKOUT</a>
			</div>
		</div>
	</div>
	<!-- </div>
</div> -->
	<!---Start Price Check Modal -->
	@if(!empty($priceCheck))
		<div class="modal fade" id="priceModal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<h2>New Prices</h2>
						<p class="text-center">Sorry selected room prices no longer exisit, Below are the new prices</p>
						<div class="roomType-inner m-t-20">
							@php $i=0; @endphp
							@foreach($priceCheck as $key =>$check)
								@php
									if($i==0){
                                    $from_city = $check['city'];
                                    $leg = $key;
                                    }
                                    $i++;
								@endphp
								<div class="m-t-20">
									<p>City : {{$check['city']}}</p>
									<p>Accomodation : {{$check['hotelName']}}</p>
									<p>New Rate : {{$check['newRate']}}</p>
									<p>Old Rate : {{$check['oldRate']}}</p>
								</div>
							@endforeach
							<div class="m-t-30 text-right">
								<div class="modal-footer" style="background: none;">
									<div class="row">
										<div class="col-md-2"></div>
										<div class="col-md-8 text-center">
											<a href="#" class="btn btn-primary ">Continue</a>
											<a href="{{url($from_city.'/hotels?leg='.$leg)}}" class="btn btn-primary ">No</a>
										</div>
										<div class="col-md-2"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
	<!---Close Modal -->
	<input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
	<input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
@endsection
@section('custom-js')
	<script type="text/javascript" src="{{ url('assets/js/eroam-map.js') }}"></script>
	<script src="{{ url( 'assets/js/theme/bootstrap-datepicker.js' ) }}"></script>
	<script type="text/javascript">
        var searchSession = JSON.parse($('#search-session').val());
        // REVIEW MODULE
        var review = (function() {
            function init() {
                // var transportObject = {
                // 	fare_source_code : 'ODAxMDE5JlRGJlRGJjRkYzRmMjExLTllYzgtNDlkZC04MjcxLTg0MjgwMDY0MzZmMyZURiY=',
                // 	price : '180.00',
                // 	currency : globalCurrency,
                // 	fare_type : 'Public'
                // };
                var leg = 0;
                // var formatTransport = eroam.formatTransport( searchSession.itinerary[ leg ].transport, leg );
                // validateMystifly( formatTransport, leg );
                //var formattedHotel = eroam.formatHotel( searchSession.itinerary[ leg ].hotel, leg );
                //validateAEHotel( formattedHotel, leg );
                buildItinerary();
            }
            function buildItinerary() {
                var itinerary = searchSession.itinerary;
                for (var i = 0; i < itinerary.length; i++) {
                    var leg = itinerary[i];
                    // HOTEL
                    var hotel = eroam.formatHotel(leg.hotel, leg.city);
                    buildHotel(hotel, i);
                    // ACTIVITIES
                    var activities = eroam.formatActivities(leg.activities);
                    buildActivities(activities, i);
                    // TRANSPORT
                    if (i != itinerary.length - 1) {
                        console.log( 'transport: ',  leg.transport );
                        var nextLeg = itinerary[ i + 1 ];
                        var transport = eroam.formatTransport(leg.transport, leg.city, nextLeg.city);
                        buildTransport(transport, i);
                    }
                }
            }
            function buildHotel(hotel, index) {
                $('.hotel-row').eq(index).html(
                    '<div class="col-md-7">' +
                    '<b><i class="fa fa-hotel"></i> Accomodation:</b>' +
                    '<br>' +
                    '<span>'+ hotel.name +' ('+hotel.room_name+')</span>' +
                    '</div>' +
                    '<div class="col-md-5 text-right">' +
                    '<br>' +
                    '<span class="bold-txt">$279.89</span>' +
                    '<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
                    '<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
                    '</div>'
                );
            }
            function buildActivities(activities, index) {
                var activityList = '';
                if (activities.length > 0) {
                    for (var i = 0; i < activities.length; i++) {
                        var activity = activities[i];
                        activityList += '<li>'+ moment(activity.date_selected).format('dddd, MMM D') +' - '+ activity.name +'</li>';
                    }
                    $('.activity-row').eq(index).html(
                        '<div class="col-md-7">' +
                        '<b><i class="fa fa-child"></i> Activities:</b>' +
                        '<ul>' +
                        activityList +
                        '</ul>' +
                        '</div>' +
                        '<div class="col-md-5 text-right">' +
                        '<br>' +
                        '<ul>' +
                        '<li>' +
                        '<span class="bold-txt">$279.89</span>' +
                        '<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
                        '<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
                        '</li>' +
                        '<li>' +
                        '<span class="bold-txt">$279.89</span>' +
                        '<span class="not-available"><i class="fa fa-times-circle"></i> Unavailable</span>' +
                        '<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
                        '</li>' +
                        '</ul>' +
                        '</div>'
                    );
                } else {
                    $('.activity-row').eq(index).html(
                        '<div class="col-md-7">' +
                        '<b><i class="fa fa-child"></i> Activities:</b><br>' +
                        '<span>No activities included.</span>' +
                        '</div>' +
                        '<div class="col-md-5 text-right">' +
                        '<br>' +
                        '<ul>' +
                        '<li>' +
                        '<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
                        '</li>' +
                        '</ul>' +
                        '</div>'
                    );
                }
            }
            function buildTransport(transport, index) {
                if (transport.provider != 'own_arrangement') {
                    $('.transport-row').eq(index).html(
                        '<div class="col-md-7">' +
                        '<b><i class="fa fa-hotel"></i> Transport:</b>' +
                        '<br>' +
                        '<span>'+ transport.name +'</span><br>' +
                        '<span><b>Depart:</b> '+ transport.departure +'</span><br>' +
                        '<span><b>Arrive:</b> '+ transport.arrival +'</span><br>' +
                        '</div>' +
                        '<div class="col-md-5 text-right">' +
                        '<br>' +
                        '<span class="old-price">$279.89</span>' +
                        '<span class="bold-txt">$280.89</span>' +
                        '<span class="available"><i class="fa fa-check-circle"></i> Available</span>' +
                        '<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
                        '</div>'
                    );
                } else {
                    $('.transport-row').eq(index).html(
                        '<div class="col-md-7">' +
                        '<b><i class="fa fa-hotel"></i> Transport:</b>' +
                        '<br>' +
                        '<span>'+ searchSession.itinerary[index].city.name +' to '+ searchSession.itinerary[index + 1].city.name +' (Own Arrangement)</span><br>' +
                        '</div>' +
                        '<div class="col-md-5 text-right">' +
                        '<br>' +
                        '<a href="#" class="change-btn" data-toggle="tooltip" data-placement="right" title="Change/Update"><i class="fa fa-pencil-square-o"></i> Change</a>' +
                        '</div>'
                    );
                }
            }
            function validateMystifly( t, leg )
            {
                var unvalidated = isNotUndefined( t.availability_checked_at ) ? isExpired( t.availability_checked_at ) : true;
                console.log('Mystifly transport should be validated:', unvalidated);
                if( unvalidated )
                {
                    MystiflyController.revalidate(
                        t.fare_source_code,
                        function( revalidateSuccessRS ) {
                            var newPrice = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount;
                            var newCurrency = revalidateSuccessRS.PricedItinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
                            var convertedNewPrice = eroam.convertCurrency( newPrice, newCurrency );
                            if( convertedNewPrice != t.price ) {
                                // NEW PRICE DOES NOT MATCH THE OLD PRICE
                                // NEW PRICE OF FLIGHT IS STORE IN VARIABLE "convertedNewPrice"
                                // UPDATE THE PRICE UI WITH THE NEW PRICE & USE "globalCurrency" VARIABLE AS THE CURRENCY
                                searchSession.itinerary[ leg ].transport.price = convertedNewPrice;
                                searchSession.itinerary[ leg ].transport.old_price = t.price;
                            }
                            searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();
                            console.log('revalidate success', revalidateSuccessRS);
                            MystiflyController.flightRules(
                                t.fare_source_code,
                                function( flightRulesSuccessRS ) {
                                    console.log('fare-rules success', flightRulesSuccessRS);
                                    var rules = FareRules.FareRule.RuleDetails.RuleDetail;
                                    if( t.fare_type != 'Public' ) {
                                        // THIS WILL SHOW A LINK AS THE FLIGHT RULES
                                        console.log( 'flight rules is a link', rules );
                                    }else{
                                        console.log('flight rules is NOT a link', rules);
                                        if( Array.isArray( rules ) ) {
                                            rules.forEach(function( elem, key ) {
                                            });
                                            console.log('rules is an array', rules);
                                        }else{
                                            console.log('rules is not an array', rules);
                                        }
                                    }
                                    searchSession.itinerary[ leg ].transport.availability_checked_at = moment().format();
                                    bookingSummary.update( searchSession );
                                },
                                function( flightRulesFailureRS ) {
                                    // MYSTIFLY TRANSPORT NOT AVAILABLE
                                    console.log('fare-rules fail', flightRulesFailureRS);
                                }
                            );
                        },
                        function( revalidateFailureRS ) {
                            // MYSTIFLY TRANSPORT NOT AVAILABLE
                            console.log('revalidate fail', revalidateFailureRS);
                        }
                    );
                }
            }
            function createAERequest( searchSession )
            {
                // var rq = {
                // 	Title : '',
                // 	FirstName : '',
                // 	LastName : '',
                // 	Address : null,
                // 	Email : '',
                // 	MobileNumber : '',
                // 	HotelRequests : []
                // };
                // searchSession.itinerary.forEach(function( leg, i ){
                // 	if( i.hotel.provider == 'ae' )
                // 	{
                // 		rq.HotelRequests.push({
                // 			HotelId : i.hotel_id,
                // 			RoomId : i.room_id,
                // 			MealId : i.meal_id,
                // 			ProviderId : i.provider_id,
                // 			GroupIdentifier : i.group_identifier,
                // 			ExpectedBookingPrice : i.price, // e.g. 1155.00
                // 			GuestName : '', // e.g. John Doe
                // 			Occupancy : {
                // 				RoomIndex : '',
                // 				Adults : ''
                // 			},
                // 			FromDate : '', // e.g. 2014-10-01T00:00:00
                // 			ToDate : '' // e.g. 2014-10-01T00:00:00
                // 		});
                // 	}
                // });
                // return rq;
            }
            function validateAEHotel( h, leg )
            {
                var unvalidated = isNotUndefined( h.availability_checked_at ) ? isExpired( h.availability_checked_at ) : true;
                console.log('AE hotel should be validated:', unvalidated);
                if( unvalidated )
                {
                    var rq = {
                        FromDate: h.checkin,
                        ToDate : h.checkout,
                        Adults : searchSession.travellers,
                        HotelId : h.hotel_id
                    };
                    AEController.checkHotelAvailability(
                        rq,
                        function( availabilitySuccessRS )
                        {
                            if( availabilitySuccessRS )
                            {
                                console.log('AE hotel:', h, 'AE hotel RQ:', rq, 'availabilitySuccessRS', availabilitySuccessRS);
                                availabilitySuccessRS[0].rooms.forEach(function( room ){
                                    if( room.room_id == h.room_id )
                                    {
                                        // AN ERROR OCCURED HERE. CANNOT ACCESS SEARCHSESSION VARIABLE HERE. CONTINUE HERE
                                        var roomPrice = eroam.convertCurrency( h.price, h.currency );
                                        console.log('roomPrice', roomPrice);
                                        console.log('room.price', room.price);
                                        if( room.price != roomPrice )
                                        {
                                            searchSession.itinerary[ leg ].hotel.price = room.price;
                                            searchSession.itinerary[ leg ].hotel.old_price = h.price;
                                        }
                                        searchSession.itinerary[ leg ].hotel.provider_id = room.provider_id;
                                        searchSession.itinerary[ leg ].hotel.availability_checked_at = moment().format();
                                        bookingSummary.update( JSON.stringify( searchSession ) );
                                    }
                                });
                            }
                            else
                            {
                                console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'No data returned from validateAEHotel');
                                // AE HOTEL NOT AVAILABLE
                            }
                        },
                        function( availabilityFailureRS )
                        {
                            console.log('AE hotel:', h, 'AE hotel RQ:', rq ,'availabilityFailureRS', availabilityFailureRS);
                            // AE HOTEL NOT AVAILABLE
                        }
                    );
                }
            }
            function isExpired( availability_checked_at )
            {
                var difference = moment().diff( moment( availability_checked_at ), 'seconds' );
                console.log('difference', difference);
                return ( difference > 600 ) ? true : false ;
            }
            return {
                init: init,
            };
        })( MystiflyController, HBController );
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
            review.init();
        }); // END OF DOCUMENT READY
	</script>
	<script src="{{ url( 'assets/js/theme/prettify.js' ) }}"></script>
	<script src="{{ url( 'assets/js/theme/jquery.slimscroll.js' ) }}"></script>
	<script type="text/javascript">
        $("#checkin-date, #checkout-date").datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            todayHighlight: true
        });
        function calculateWidth(){
            var owidth = $('.payment-steps').parent().width() - 10;
//alert(owidth);
            var elem = $('.payment-steps li').length;
//alert(owidth);
//alert(elem);
            if (elem >6) {
                var elemWidth = owidth / 6;
            }else{
                var elemWidth = owidth / elem;
            }
            $(".payment-steps li").width(elemWidth);
        }
        $( window ).load( eMap.init );
        $(document).ready(function() {
            calculateWidth();
            calculateHeight();
            $.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z," "]+$/i.test(value);
            }, "Letters only please.");
            $.validator.addMethod("numbersonly", function(value, element) {
                return this.optional(element) || /^[0-9," "]+$/i.test(value);
            }, "Numbers only please.");
            var todayDate = new Date().getDate();
            $(".passenger_dob").datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
            });
            $(".passenger_passport_expiry_date").datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
            });
            $("#signup_form_id").validate({
                ignore: [],
                rules: {
                    passenger_address_one: {
                        required: true
                    },
                    passenger_suburb: {
                        required: true
                    },
                    passenger_state: {
                        required: true
                    },
                    passenger_zip: {
                        required: true,
                        number:true
                    },
                    passenger_title: {
                        required: true
                    },
                    passenger_gender: {
                        required: true
                    },
                    first_name: {
                        required: true,
                        lettersonly:true
                    },
                    last_name: {
                        required: true,
                        lettersonly:true
                    },
                    card_number: {
                        required: true,
                        number:true
                    },
                    year: {
                        required: true
                    },
                    cvv: {
                        required: true,
                        maxlength: 3,
                        number:true
                    },
                    month: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    postalcode: {
                        required: true
                    }
                },
                errorPlacement: function (label, element) {
                    label.insertAfter(element);
                },
                submitHandler: function (form) {
//alert('hi');
                    //	payment();
                    form.submit();
                }
            });
            $('.passenger_zip').each(function () {
                $(this).rules('add', {
                    required: true,
                    number:true
                });
            });
            $('.passenger_state').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_suburb').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_address_one').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_gender').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_title').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_first_name').each(function () {
                $(this).rules('add', {
                    required: true,
                    lettersonly:true
                });
            });
            $('.passenger_last_name').each(function () {
                $(this).rules('add', {
                    required: true,
                    lettersonly:true
                });
            });
            $('.passenger_dob').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_contact_no').each(function () {
                $(this).rules('add', {
                    required: true,
                    numbersonly:true,
                    maxlength: 15,
                });
            });
            $('.passenger_email').each(function () {
                $(this).rules('add', {
                    required: true,
                    email:true
                });
            });
            $('.passenger_country').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_passport_expiry_date').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
            $('.passenger_passport_num').each(function () {
                $(this).rules('add', {
                    required: true
                });
            });
        });
        $(window).resize(function(){
            leftpanel();
            calculateHeight();
            leftStripHeight();
            calculateWidth()
        });
        $('.accomodation-wrapper').slimScroll({
            height: '100%',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
        function calculateHeight(){
            var winHeight = $(window).height();
            var oheight = $('.page-sidebar').outerHeight();
            var elem = $('.page-content .tabs-container').outerHeight();
            var elemHeight = oheight - elem;
            var winelemHeight = winHeight - elem;
            if(winHeight < oheight){
                $(".page-content .tabs-content-container").outerHeight(elemHeight);
            } else{
                $(".page-content .tabs-content-container").outerHeight(winelemHeight);
            }
        }
        function map(){
// When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
            function init() {
// Basic options for a simple Google Map
// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
// How zoomed in you want the map to start at (always required)
                    zoom: 11,
// The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York
// How you would like to style the map.
// This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
                };
// Get the HTML DOM element that will contain your map
// We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');
                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);
                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'eRoam!'
                });
            }
        }
	</script>
@endsection