@extends('layouts.profileLayout')
@section('content')  
<script src="{{ url( 'assets/js/theme/jquery.min.js' ) }}"></script>  
<section>
<div class="profile_page">
  @include('pages.search_banner') 
</div>

<section>
    @include('user.profile_sidebar')
    
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="content-container accomodation-tabs-main-container">
                <div class="content-container-inner">
                    <div class="text-right"><a href="javascript://" class="navbtn menu-btn hidden-lg hidden-md"><i class="icon icon-list"></i></a></div>
                    <h2 class="profile-title">Complete Your Account Setup</h2>
                    @include('user.profile_header')
                    <div class="m-t-40">
                        <hr/>
                    </div>
                    @if(session()->has('profile_step1_success'))
                        <p class="success-box m-t-30">
                            {{ session()->get('profile_step1_success') }}
                        </p>
                    @endif
                    @if(session()->has('profile_step1_error'))
                        <p class="danger-box m-t-30">
                            {{ session()->get('profile_step1_error') }}
                        </p>
                    @endif
                    <div class="m-t-30">
                <h2 class="profile-title">Manage Trips</h2>
                <div class="row m-t-30">
                    @if(count($all_trips) > 0)
                      @foreach($all_trips as $trip)
                      <div class="col-lg-4 col-md-6 col-sm-4 m-t-10">
                          <div class="trips-wrapper">
                            <div class="trip-container">
                              <div class="tripImage"><img src="{{ url('images/trip-image.png')}} " alt="" class="img-responsive"></div>
                              <div class="row">
                                <?php 
                                $from_city_info = get_city_by_id($trip['from_city_id']);
                                $to_city_info   = get_city_by_id($trip['to_city_id']);
                                $from_country   = get_country_by_country_name($from_city_info['country_name']);
                                $to_country     = get_country_by_country_name($to_city_info['country_name']);
                                
                                ?>
                                <div class="col-sm-5">
                                  <h3><strong>{{$from_city_info['airport_codes']}}</strong></h3>
                                  <small>{{$trip['from_city_name']}}, {{$from_country[1]['iso_3_letter_code']}}</small>
                                </div>
                                <div class="col-sm-2">
                                  <strong>-</strong>
                                </div>
                                <div class="col-sm-5">
                                  <h3><strong>{{$to_city_info['airport_codes']}}</strong></h3>
                                  <small>{{$trip['to_city_name']}}, {{$to_country[1]['iso_3_letter_code']}}</small>
                                </div>
                              </div>
                            </div>
                            <div class="tripBooking">
                              <div class="tripTitle"><strong>Multi-City Tailormade Auto</strong></div>
                              <p><small>Booking ID: {{$trip['invoice_no']}}</small></p>
                              <ul class="paymentIcons">
                                <li><a href="#"><i class="icon icon-hotel"></i></a></li>
                                <li><a href="#"><i class="icon icon-activity"></i></a></li>
                                <li><a href="#"><i class="icon icon-plane"></i></a></li>
                                <li><a href="#"><i class="icon icon-bus"></i></a></li>
                              </ul>
                            </div>
                            <div class="traveller-details">
                              <div class="row">
                                <div class="col-sm-4"><strong>Travellers:</strong></div>
                                <div class="col-sm-8">
                                  @foreach($trip['passenger_information'] as $passenger_info)
                                      {{ $passenger_info['first_name']." ".$passenger_info['last_name'] }}<br>
                                  @endforeach
                                  
                                </div>
                              </div>
                              <div class="row m-t-10">
                                <div class="col-sm-4"><strong>Itinerary:</strong></div>
                                <div class="col-sm-8">
                                  <?php $itinery = str_replace(",",' - ',$trip['from_city_to_city']); ?>
                                  {{$itinery}}
                                </div>
                              </div>
                            </div>
                            <div class="m-t-10 travellerPrice-box">
                              <h4><strong>{{$trip['currency'] }}$ {{ $trip['cost_per_day'] }} Per Day</strong></h4>
                              <p>Total Cost <strong>${{ $trip['currency'] }} {{ $trip['total_per_person'] }}</strong><br>(Per Person) Inc. Taxes</p>
                            </div>
                            <div class="barcode-box">
                              <p><center>{{ date( 'j M Y', strtotime($trip['from_date']))}} - {{ date( 'j M Y',strtotime($trip['to_date']))}}</center></p>
                            </div>
                          </div>
                      </div>
                      @endforeach      
                    @else
                      <div class="col-lg-12 col-md-12 col-sm-12 m-t-10">
                        <h3>No Tours Found</h3>
                      </div>
                    @endif              
                </div>
              </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
@stop
@section( 'custom-js' )
<script type="text/javascript" src="{{  url( 'assets/js/eroam_js/profile_leftmenu.js?v='.$version.'') }}"></script>
<script type="text/javascript" src="{{  url( 'assets/js/eroam_js/profile_step1.js?v='.$version.'') }}"></script>
@stop