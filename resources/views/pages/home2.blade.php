<?php 
  //echo '<pre>';print_r($travel_pref);exit;
      // $travellers['transport_types'] = [];
      // $travellers['transport_types'][0]['id'] =1;
      //  $travellers['transport_types'][0]['name'] = 'Air (Flights)';
      //  $travellers['transport_types'][1]['id'] = 2;
      //  $travellers['transport_types'][1]['name'] = 'Land (All other modes)';
      $travel_preferences['categories'] = [];
      $travel_preferences['categories'][0]['id'] =9;
      $travel_preferences['categories'][0]['name'] = '5 Star (Luxury)';
      $travel_preferences['categories'][1]['id'] =3;
      $travel_preferences['categories'][1]['name'] = '4 Star (Deluxe)';
      $travel_preferences['categories'][2]['id'] = 2;
      $travel_preferences['categories'][2]['name'] = '3 Star (Standard)';
      $travel_preferences['categories'][3]['id'] = 5;
      $travel_preferences['categories'][3]['name'] = '2 Star (Backpacker / Guesthouse)';
      $travel_preferences['categories'][4]['id'] = 1;
      $travel_preferences['categories'][4]['name'] = 'Camping';
                 
?>
@extends('layouts.home2')

@section('custom-css')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
  <style type="text/css">
    label.error{
      font-size: 12px !important;
    }


    #tours-loader {
      width: 100%;
      /*position: absolute;
      top: 10%;
      transform: translateY( -50% );*/
      text-align: center;
      margin: 10px 0px -20px 0px;
    }

    #tours-loader span{ 
      padding: 8px 21px;
      font-size: 18px;
      font-family: "HelveticaNeue";
    }
    .padding-left-5{
      padding-left: 5px;
    }
  </style>
@stop

@section('content')
 
  <?php 
    $i=0;
    $allCountry = array();
    foreach($countries as $country){
      foreach ($country['countries'] as $country_data){
          $allCountry[$i]['name'] = $country_data['name'];
          $allCountry[$i]['id'] = $country_data['id'];
          $allCountry[$i]['region'] = $country['id'];
          $allCountry[$i]['regionName'] = $country['name'];
          $i++;
      } 
    }
    usort($allCountry, 'sort_by_name');

    //echo '<pre>'; print_r($allCountry); die;
  ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <script type="text/javascript">
      $( function() {
          
          var projects = [
              <?php for($i=0;$i<count($cities);$i++) { ?>{
                  value: "<?php echo $cities[$i]['id'];?>",
                  label: "<?php echo $cities[$i]['name'];?>",
                  desc: "<?php echo $cities[$i]['country_name'];?>",
                  cid: "<?php echo $cities[$i]['country_id'];?>"
              },
              <?php } ?>
          ];

          var countries = [
              <?php for($i=0;$i<count($allCountry);$i++) { ?>{
                  value : "<?php echo $allCountry[$i]['id'];?>",
                  label : "<?php echo $allCountry[$i]['name'];?>",
                  rid   : "<?php echo $allCountry[$i]['region'];?>",
                  rname : "<?php echo $allCountry[$i]['regionName'];?>"
              },
              <?php } ?>
          ];
         
          $( "#project" ).autocomplete({
              minLength: 2,
              source: countries,
              focus: function( event, ui ) {
                 
                  $( "#project" ).val( ui.item.label );
                  return false;
              },
              select: function( event, ui ) {
                  
                  var displayVal = ui.item.label;
                  $( "#project" ).val( displayVal );

                  $('#countryId').val(ui.item.value);
                  $('#countryRegion').val(ui.item.rid);
                  $('#countryRegionName').val(ui.item.rname);

                  $('#starting_country').val('');
                  $('#starting_city').val('');

                  $('#destination_country').val('');
                  $('#destination_city').val('');
                 
                  return false;
              },
              response: function( event, ui ) {
                if(ui.content.length == 0){
                  project_search_cnt = ui.content.length;
                }else{
                  project_search_cnt = 1;
                }
              }
          }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
              .append( "<div>" + item.label + "</div>" )
              .appendTo( ul );
          };

          $( "#start_location" ).autocomplete({
              minLength: 2,
              source: projects,
              focus: function( event, ui ) {
                  $( "#start_location" ).val( ui.item.label );
                  return false;
              },
              select: function( event, ui ) {
                  var displayVal = ui.item.label +', ' +ui.item.desc;
                  $( "#start_location" ).val( displayVal );
                  //var option = $("#search-form").find("input[name=option]:checked").val();
                  var option = $(".radio_tailor:checked").val();
                  if(option == "packages" || option == "manual"){
                    $('#starting_country').val(ui.item.cid);
                    $('#starting_city').val(ui.item.value);

                   // $('#destination_country').val('');
                   // $('#destination_city').val('');
                  } else {

                      $('#starting_country').val(ui.item.cid);
                      $('#starting_city').val(ui.item.value);

                      //$( "#destination_city" ).val( ui.item.value );
                      //$( "#destination_country" ).val( ui.item.cid );
                  }

                  return false;
              },
              response: function( event, ui ) {
                if(ui.content.length == 0){
                  start_location_search_cnt = ui.content.length;
                }else{
                  start_location_search_cnt = 1;
                }
              }
          }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
              .append( "<div>" + item.label +", " + item.desc + "</div>" )
              .appendTo( ul );
          };

          $( "#end_location" ).autocomplete({
              minLength: 2,
              source: projects,
              focus: function( event, ui ) {
                  $( "#end_location" ).val( ui.item.label );
                  return false;
              },
              select: function( event, ui ) {
                  var displayVal = ui.item.label +', ' +ui.item.desc;
                  $( "#end_location" ).val( displayVal );
                  $( "#destination_city" ).val( ui.item.value );
                  $( "#destination_country" ).val( ui.item.cid );

                  return false;
              },
              response: function( event, ui ) {
                console.log(ui.content.length);
                if(ui.content.length == 0){
                  end_location_search_cnt = ui.content.length;
                }else{
                   end_location_search_cnt = 1;
                }
              }
          }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
              .append( "<div>" + item.label +", " + item.desc + "</div>" )
              .appendTo( ul );
          };
      } );
  </script>

  @include('pages.search_banner')
  <section class="content-wrapper">
    <h1 class="hide"></h1>
    <article class="places-section eroam-trending">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="homeTitle">
              <h3>TOP TOURS</h3>
            </div>
            <?php 
              $default_selected_city = session()->get( 'default_selected_city' );
                    $city_name = 'Melbourne';
                    if($default_selected_city == 7){
                      $city_name = 'Sydney';
                    }elseif($default_selected_city == 15){
                      $city_name = 'Brisbane';
                    }elseif($default_selected_city == 30){
                      $city_name = 'Melbourne';
                    }
            ?>
             <!-- <h2>TOP TOURS DEPARTING FROM <strong><?php echo $city_name;?></strong></h2> -->
            <div class="homeTour-listing m-t-50">

              <div id="tours-loader">
                <span><i class="fa fa-circle-o-notch fa-spin"></i> Loading Tours...</span>
              </div>
              <div class="row" id="tourList"></div>
            </div>
          </div>
        </div>
      </div>
    </article>
  </section>

  <?php 
      /*
      | Added by Rekha
      | Accomodation multi select options
      */
      $accommodation_name = ''; 
      $accommodation_options = '';
      $room_type_name = '';
      $room_type_options = '';
      $transport_type_name = '';
      $transport_type_options  = '';
      $nationality = '';
      $nationality_name = '';
      $age_group = '';
      $age_group_name = '';
      $gender = '';
      $gender_name = '';      

      if ( count( $travellers['categories'] ) > 0 ) {
        foreach( $travellers['categories'] as $category ){
          if( empty( $category['name'] ) ){
            continue;
          }
          
          if (isset($travel_pref['accommodation_name']) && in_array($category['name'], $travel_pref['accommodation_name'])) {
            $accommodation_name .= '<span title="'.$category['name'].'" class="drop-selected">'.$category['name'].'</span>';
            //$accommodation_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="hotel_category" name="hotel_category_id[]" id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" checked> '.$category['name'] .' </label></li>';
            $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="1" data-name="'.$category['name'].'" selected="selected">'.$category['name'].'</option>';
          }else{
            //$accommodation_options .= '<li><label ><input type="radio" class="hotel_category" name="hotel_category_id[]" id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'"> '.$category['name'] .' </label></li>';
            $accommodation_options .= '<option id="accommodation-'.$category['id'].'" value="'.$category['id'].'" data-checked="0" data-name="'.$category['name'].'" >'.$category['name'].'</option>';
          }
        }
      }

      if( count( $travellers['room_types'] ) > 0 ){
        foreach( $travellers['room_types'] as $room_type){
          if( empty( $room_type['name'] ) ){
            continue;
          }
          if (isset($travel_pref['room_name']) && in_array($room_type['name'], $travel_pref['room_name'])) {
            $room_type_name .= '<span title="'.$room_type['name'].'" class="drop-selected">'.$room_type['name'].'</span>';
            //$room_type_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="room_type_options" name="room_type_id[]" value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" checked> '.$room_type['name'] .' </label></li>';
            $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="1" data-name="'.$room_type['name'].'" selected="selected"> '.$room_type['name'].'</option>';
          }else{
            //$room_type_options .= '<li><label ><input type="radio" class="room_type_options" name="room_type_id[]" value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'"> '.$room_type['name'] .' </label></li>';
            $room_type_options .= '<option value="'.$room_type['id'].'" data-checked="0" data-name="'.$room_type['name'].'" > '.$room_type['name'].'</option>';
          }
    
        }
      }

      if( count( $travellers['transport_types'] ) > 0 ){
        foreach( $travellers['transport_types'] as $transport_type){
          if( empty( $transport_type['name'] ) ){
              continue;
          }
          if (isset($travel_pref['transport_name']) && in_array($transport_type['name'], $travel_pref['transport_name'])) {
            $transport_type_name .= '<span title="'.$transport_type['name'].'" class="drop-selected">'.$transport_type['name'].'</span>';
            //$transport_type_options .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="transport_type_options" name="transport_types[]" value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" checked> '.$transport_type['name'] .' </label></li>';
            $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="1" data-name="'.$transport_type['name'].'" selected="selected">'.$transport_type['name'].'</option>';
          }else{
            //$transport_type_options .= '<li><label ><input type="radio" class="transport_type_options" name="transport_types[]" value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'"> '.$transport_type['name'] .' </label></li>';
            $transport_type_options .= '<option value="'.$transport_type['id'].'" data-checked="0" data-name="'.$transport_type['name'].'">'.$transport_type['name'].'</option>';
          }
        }
      }

      if( count( $travellers['nationalities'] ) > 0 ){
        foreach( $travellers['nationalities']['featured'] as $featured ){
          if( empty( $featured['name'] ) ){
            continue;
          }
          if ( isset($travel_pref['nationality']) && ( $featured['name'] == $travel_pref['nationality'] ) ) {
            $nationality_name .= '<span title="'.$featured['name'].'" class="drop-selected">'.$featured['name'].'</span>';
            //$nationality .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="nationality" name="nationality" value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" checked> '.$featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$featured['id'].'" data-checked="1" data-name="'.$featured['name'].'" selected="selected">'.$featured['name'] .'</option>';
          }else{
            //$nationality .= '<li><label ><input type="radio" class="nationality" name="nationality" value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'"> '.$featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$featured['id'].'" data-checked="0" data-name="'.$featured['name'].'" >'.$featured['name'] .'</option>';
          }
        }

        foreach( $travellers['nationalities']['not_featured'] as $not_featured ){
          if( empty( $not_featured['name'] ) ){
            continue;
          }
          if ( isset($travel_pref['nationality']) && ( $not_featured['name'] == $travel_pref['nationality'] ) ){
            $nationality_name .= '<span title="'.$not_featured['name'].'" class="drop-selected">'.$not_featured['name'].'</span>';
            //$nationality .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="nationality" name="nationality" value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" checked> '.$not_featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$not_featured['id'].'" data-checked="1" data-name="'.$not_featured['name'].'" selected="selected"> '.$not_featured['name'] .'</option>';
          }else{
            //$nationality .= '<li><label ><input type="radio" class="nationality" name="nationality" value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .' </label></li>';
            $nationality .= '<option value="'.$not_featured['id'].'" data-checked="0" data-name="'.$not_featured['name'].'"> '.$not_featured['name'] .'</option>';
          }
        }
      }

      $gender_array = ['male', 'female', 'other'];
      foreach($gender_array as $gen){
        if (isset($travel_pref['gender']) && ($gen == $travel_pref['gender']) ) {
          $gender_name .= '<span title="'.$gen.'" class="drop-selected">'.ucfirst($gen).'</span>';
          //$gender .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="gender" name="gender" value="'.$gen.'" data-checked="1" data-name="'.$gen.'" checked> '.ucfirst($gen) .' </label></li>';
          $gender .= '<option value="'.$gen.'" data-checked="1" data-name="'.$gen.'" selected="selected"> '.ucfirst($gen) .'</option>';
        }else{
          //$gender .= '<li><label ><input type="radio" class="gender" name="gender" value="'.$gen.'" data-checked="0" data-name="'.$gen.'"> '.ucfirst($gen) .' </label></li>';
          $gender .= '<option value="'.$gen.'" data-checked="0" data-name="'.$gen.'" > '.ucfirst($gen) .'</option>';
        }
      }
    
      if( count( $travellers['age_groups'] ) > 0 ){
        foreach( $travellers['age_groups'] as $age){
          if( empty( $age['name'] ) ){
            continue;
          }
          if (isset($travel_pref['age_group']) && ($age['name'] == $travel_pref['age_group']) ) {
            $age_group_name .= '<span title="'.$age['name'].'" class="drop-selected">'.$age['name'].'</span>';
            //$age_group .= '<li><label ><i class="fa fa-check"></i><input type="radio" class="age_group" name="age_group" value="'.$age['id'].'" data-checked="1" data-name="'.$age['name'].'" checked> '.$age['name'] .' </label></li>';
            $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" selected="selected" data-checked="1">'.$age['name'].'</option>';
          }else{
            //$age_group .= '<li><label ><input type="radio" class="age_group" name="age_group" value="'.$age['id'].'" data-checked="0" data-name="'.$age['name'].'"> '.$age['name'] .' </label></li>';
            $age_group .= '<option value="'.$age['id'].'" data-name="'.$age['name'].'" data-checked="0">'.$age['name'].'</option>';
          }
        }
      }
  ?>

  <!-- Modal -->
  <!-- <div class="modal fade" id="preferencesModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-large" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
         <h4 class="modal-title" id="myModalLabel">Personal Profile Preferences</h4>
          <p>Please fill in the details below to personalise your travel preferences.</p>
        </div>
        <div class="modal-body">
            <form class="form-horizontal">
             <div class="row">
               <div class="col-sm-6">
                 <div class="panel-form-group">
                   <label class="label-control">Select Age Group</label>
                  <select class="form-control age_group" name="age_group" id="dropdown-ages">
                    <option value="">Select Age Group</option>
                    {!! $age_group !!}
                  </select>
                </div>
               
               </div>
               <div class="col-sm-6">
                 <div class="panel-form-group">
                   <label class="label-control">Select Transport Type</label>
                  <select class="form-control transport_type_options" name="transport_types[]" id="dropdown-transports">
                    <option value="">Select Transport Type</option>
                    {!! $transport_type_options !!}
                  </select>
                </div>
               </div>
             </div>

             <div class="row">
               <div class="col-sm-6">
                   <div class="panel-form-group">
                   <label class="label-control">Select Gender</label>
                  <select class="form-control gender" name="gender" id="dropdown-genders">
                    <option value="">Select Gender</option>
                    {!! $gender !!}
                  </select>
                </div>
               </div>
               <div class="col-sm-6">
                  <div class="panel-form-group">
                   <label class="label-control">Select Accommodation Type</label>
                  <select class="form-control hotel_category" name="hotel_category_id[]" id="dropdown-accommodations">
                    <option value="">Select Accommodation Type</option>
                    {!! $accommodation_options !!}
                  </select>
                </div>
               </div>
             </div>

             <div class="row">
               <div class="col-sm-6">
                  <div class="panel-form-group">
                   <label class="label-control">Select Nationality</label>
                  <select class="form-control nationality" name="nationality" id="dropdown-nationalitys">
                    <option value="">Select Nationality</option>
                    {!! $nationality !!}
                  </select>
                </div>
               </div>
               <div class="col-sm-6" style="display:none;">
                  <div class="panel-form-group">
                   <label class="label-control">Select Room Type</label>
                  <select class="form-control room_type_options" name="room_type_id[]" id="dropdown-rooms">
                    <option>Select Room Type</option>
                    {!! $room_type_options !!}
                  </select>
                </div>
               </div>
             </div>

              <div class="m-t-20" style="display:none;">
               <h4 class="modal-title" id="myModalLabel">Holiday Preferences</h4>
               <div class="row">
                 <div class="col-sm-6">
                   <label>Price Range Per Day</label>
                   <div class="layout-slider">
                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                    <input id="Slider5" type="slider" name="price2" value="100;10000" />
                    </span>
                  </div>
                 </div>
                 <div class="col-sm-6">
                   <label>Number of Total Days</label>
                  <div class="layout-slider">
                    <span style="display: inline-block; width: 100%; padding: 0 5px;">
                    <input id="Slider6" type="slider" name="price3" value="1;100" />
                    </span>
                  </div>
                 </div>
               </div>
              </div>
            
            <div class="m-t-20">
              <h4 class="modal-title" id="myModalLabel">Activity Preferences</h4>
              <p>Please drag your preferred activities into the boxes below. <?php /*Highest priority begins on the left.*/ ?></p> 

              <div class="m-t-10">
                <ul class="drop-list">

                  <?php 
                      $imgArr = array(
                        15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
                        5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
                        13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
                        29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
                        11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
                        20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
                         //'Expeditions','Astronomy','Food','Short-Breaks'
                        );

                      //echo '<pre>'; print_r($labels); echo '</pre>';
                      $ij = 0;
                      if( count($interest_ids) > 0 ){
                        foreach ($interest_ids as $j => $interest_id){
                          $key = array_search($interest_id, array_column($labels, 'id'));
                          $label = $labels[$key];
                          $ij++;
                  ?>
                            <li ondrop="drop(event)" ondragover="allowDrop(event)" class="interest-button-active" id="interest-{{ $label['id'] }}" data-sequence="{{ $ij }}" data-name="{{ $label['name'] }}" >
                              <img src="<?php echo url( 'assets/images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $j }} - {{ $label['name'] }}" data-value="{{$label['id']}}" data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                              <input type="hidden" id="inputValue_{{ $label['id'] }}" class="input-interest" name="interests[{{$j + 1}}]" value="{{ $label['id'] }}">
                            </li>
                  <?php  } } ?>

                  <?php 
                    for($k = $ij; $k < 8; $k++){
                      echo '<li ondrop="drop(event)" ondragover="allowDrop(event)" class="blankInterest" data-sequence="'.($k + 1).'"></li>';
                    }
                  ?>
                </ul>
                <div class="clearfix"></div>
              </div>

              <hr/>
              <div class="m-t-10">

                <ul class="drag-list interest-lists first">
                  @if( count($labels) > 0 )
                    @foreach ($labels as $i => $label)

                      @if( empty($label['name']) )
                        @continue
                      @endif
                      <li ondrop="drop(event)" ondragover="allowDrop(event)" data-value="{{$label['id']}}" class="{{ in_array($label['id'], $interest_ids ) ? ' interestMove' : ''  }}" id="interest-{{ $label['id'] }}">
                        <?php
                          $namedis = 'block';
                          if(!in_array($label['id'], $interest_ids )){
                            $namedis = 'none';
                        ?>
                          <img src="<?php echo url( 'assets/images/'.$imgArr[$label['id']].'.jpg'); ?>" draggable="true" ondragstart="drag(event)" id="drag_{{ $label['id'] }}" class="img-responsive" alt="{{ $i}} - {{ $label['name'] }}" data-value="{{$label['id']}}"  data-name="{{ $label['name'] }}" title="{{ $label['name'] }}">
                          <input type="hidden" id="inputValue_{{ $label['id'] }}" class="input-interest" name="interests[]" value="">
                        <?php } ?>
                          <span style="display:{{ $namedis }};" id="name_{{ $label['id'] }}">{{ $label['name'] }}</span>
                          
                      </li>
                    @endforeach
                  @endif
                </ul>
                <div class="clearfix"></div>
              </div>

              <div class="m-t-30 text-right">
                <a href="#" data-dismiss="modal" class="m-r-10">Close</a>
                <a href="javascript://" id="save_travel_preferences">Save</a>
              </div>
            </div>

            </form>
        </div>
      </div>
    </div>
  </div> -->

  <!-- Modal -->
    <div class="modal fade" id="autoModeModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tailor-Made Auto</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-4 popup-map">
                <img src="{{ url( 'assets/images/auto-map.jpg' ) }}" alt="" class="img-responsive" />
              </div>
              <div class="col-sm-8">
                <p>Tailor-Made Auto will automatically generate a multi-city itinerary between your two selected locations. The itinerary will include multiple ‘trending’ cities, recommended tours, transport and hotels. Once this itinerary has been created, you have the ability to edit any segment using the interactive map, or booking summary panel.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="manualModeModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Manual Itinerary</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-4 popup-map">
                 <img src="{{ url( 'assets/images/auto-map.jpg' ) }}" alt="" class="img-responsive" />
              </div>
              <div class="col-sm-8">
                
                <p>Manual Itinerary allows the user to plot a multi-city itinerary by clicking on an interactive map. As you add each new location ‘trending’ data will recommended the best tours, transport and hotels to suit ythe travellers profile. Once this itinerary has been created, you have the ability to edit any segment using the interactive map, or booking summary panel.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@stop
@section( 'custom-js' )

  <script type="text/javascript">
      var countryName   = '{{ $countryName }}';
      var default_currency   = '{{ $default_currency }}';
      var siteUrl = $( '#site-url' ).val();
      var activitySequence = [{{ join(', ', $interest_ids) }}];
      var project_search_cnt = 1;
      var end_location_search_cnt = 1;
      var start_location_search_cnt = 1;
      function getRole() {
        return $(".packages-option").attr('data-value');
      }
  </script>
  <script type="text/javascript">
      $(document).ready(function(){
          $('#itinerary-tab').click();
          $('#packages-option li a').click(function () {
          $('#project').removeClass('error');
          $('#start_date').removeClass('error');
          $('#end_location').removeClass('error');
          $('#start_location').removeClass('error');

             //$('.error').remove();
             var value = $(this).attr('value');
             var clickedValue = $(this).text();
           
            document.getElementById('packages-option').innerHTML = clickedValue;
            if(value == 'packages') {
               $('#search_btn').attr('disabled',false);
               $('#search_option_id').val('packages');
               $('#search-form').attr('action', 'tours');
               $('#destination_country').val(''); 
               $('#itinerary').hide();
               $('#comming_soon').hide();
               $('#project').show();
               $('#end_location').hide();
                var  data = { fromTourHome: 'Yes'};
                eroam.ajax('post', 'session/set_tourHome', data, function(response){ });
            }else if(value == 'itinerary'){
              $('#search_btn').attr('disabled',false);
              $('#search-form').attr('action', 'search');
                var radio_value = $("input[name='option1']:checked").val();
                $('#search_option_id').val(radio_value);
              $('#itinerary').show();
              $('#comming_soon').hide();
               $('#project').hide();
               $('#end_location').show();
            }else{
               $('#search_btn').attr('disabled',true);
               $('#itinerary').hide();
               $('#comming_soon').show();
            }

              $('.packages-option').html(clickedValue);
              $('.packages-option').attr('data-value',value);

          });
          /*$("input[type=radio]").change(function(){
              $('#start_location').hide();
              alert( $("input[type=radio][name=option1]").val() );
          });*/
          tourList();

          var tomorrow = new Date();
          tomorrow.setDate(tomorrow.getDate()+1);
          var nextWeek  = new Date();
          nextWeek.setDate(nextWeek.getDate()+7);
          $(".start_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: tomorrow,
            todayHighlight: true
          }).on('changeDate', function(e) {
              $(".start_date").val($("#start_date2").datepicker('getFormattedDate'));
          });

          
          var sform = $("#search-form");
          sform.validate({
              ignore: [],
              rules: {
                  project:{
                      required: function(element) {

                        return (getRole() == 'packages');
                      }
                  },
                  end_location:{
                      required: function(element) {

                          return (getRole() == 'itinerary');
                      }
                  },
                  start_location: {
                      required: function(element) {
                          //alert($("input[class='radio_tailor']:checked").val()+123);
                          if($("input[class='radio_tailor']:checked").val() != undefined) {
                              if($("input[class='radio_tailor']:checked").val() == 'auto') {
                                  return (getRole() == 'itinerary');
                              }else{
                                  return (getRole() == 'packages');
                              }
                          }else{
                              return (getRole() == 'itinerary');
                          }
                      }
                  },

                  travellers:{
                      required: function(element) {
                        return (getRole() == 'itinerary');
                      }
                  },
                  start_date: {
                      required: function(element) {
                        return (getRole() == 'itinerary');
                      }
                  },
              },
              errorPlacement: function (label, element) {
                  label.insertAfter(element);
              },
              submitHandler: function (form) {
                  if(project_search_cnt == 0){
                    $('#project').val('');
                    if($('#project_div').find('label').length < 0){
                      $('#project_div').append(' <label for="project" generated="true" class="error active">Sorry, No tours found for this country.</label>');
                    }
                    $('#project_div').find('label').show();
                    $('#project_div').find('label').text('Sorry, No tours found for this country');
                    return false;
                  }
                  if(end_location_search_cnt == 0){
                   
                    $('#end_location').val('');
                    if($('#end_location_div').find('label').length < 0){
                      $('#end_location_div').append('<label for="end_location" generated="true" class="error active">Sorry, No data found for this City.</label>');
                    }
                    $('#end_location_div').find('label').show();
                    $('#end_location_div').find('label').text('Sorry, No data found for this City');
                    return false;
                  }
                  if(start_location_search_cnt == 0){
                    $('#start_location').val('');
                    if($('#start_location_div').find('label').length < 0){
                      $('#start_location_div').append('<label for="start_location" generated="true" class="error active" >Sorry, No data found for this City.</label>');
                    }
                    $('#start_location_div').find('label').show();
                    $('#start_location_div').find('label').text('Sorry, No data found for this City');
                    return false;
                  }
                  
                if($('#search-form').attr('action') == 'tours'){ //alert(1);
                  var  data = { fromTourHome: 'Yes'};
                  eroam.ajax('post', 'session/set_tourHome', data, function(response){
                    form.submit();
                  });
                } else {//alert(2);
                  form.submit();
                }
                
              }
          })

          $( "#preferencesModal").on('shown.bs.modal', function(){
              $("#Slider5").slider({ 
                from: 100, to: 10000, step: 1 , smooth: true, round: 0, dimension: "", skin: "round", limits: false 
              });

              $("#Slider6").slider({ 
                from: 1, to: 100, step: 1 , smooth: true, round: 0, dimension: "&nbsp;Days", skin: "round", limits: false
              });
          }); 
      });

      function tourList(next){
        tour_data = {
          countryName: countryName,
          provider: 'getTours',
          default_selected_city: '<?php echo session()->get( 'default_selected_city' );?>'
        };

        // CACHING HOTEL BEDS
        var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'getTours', true);

        eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
         //console.log('tourResponse', tourResponse);
          if( tourResponse.length > 0 ){
            $.each( tourResponse, function( key, value ) {
              //console.log('value : '+ key + value);
              appendTours( value );
            })

          }
        })
      }
      var cnt1 = 1;
      function getStars(count, half = false){
        var stars = '';
        if( parseInt( count ) ){
          for( star = 1; star <= count; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
          }
          var emptyStars = 5 - parseInt( count );
          if(half){
            stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
            emptyStars = emptyStars - 1;
          }
          for( empty = 1; empty <= emptyStars; empty ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }else{
          for( star = 1; star <= 5; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }
         var stars = '';
        return stars;
      }

      function appendTours(tour){
          console.log(tour);
          $("#tours-loader").hide();
          if(cnt1 == 22){return false;}
          var price = 0;
          var land_price = 0;
          if(!tour.flightPrice  || tour.flightPrice < 1){
            tour.flightPrice = 0;
          }
          if(tour.flightDepart != '' && tour.flightDepart != null){
            <?php 
              $default_selected_city = session()->get( 'default_selected_city' );
              $city_name = 'MEL';
              $city_full_name = 'Melbourne';
              if($default_selected_city == 7){
                $city_name = 'SYD';
                $city_full_name = 'Sydney';
              }elseif($default_selected_city == 15){
                $city_name = 'BNE';
                $city_full_name = 'Brisbane';
              }elseif($default_selected_city == 30){
                $city_name = 'MEL';
                $city_full_name = 'Melbourne';
              }
            ?>
            tour.flightDepart = '<?php echo $city_name;?>';
            var city_full_name = '<?php echo $city_full_name;?>';
          }
          if(tour.price){
            price = parseFloat(parseFloat(tour.price) + parseFloat(tour.flightPrice)).toFixed(2);
            land_price = parseFloat(tour.price).toFixed(2);
          }
          
          var star;
          var rating = tour.rating;

          if(rating % 1 === 0){
              stars = getStars( rating);
          }else {
              stars = getStars( rating, true );
          }


        var imgurl = 'http://www.adventuretravel.com.au'+tour.folder_path+'245x169/'+tour.thumb;
        var imgurl2 = 'http://dev.cms.eroam.com/'+tour.thumb;

        $("#overlay").hide();
          if(parseInt(tour.no_of_days) == 1) {
              var day = 'Day';
          }else{
              var day = 'Days';
          }
          var str1 = tour.short_description;
          if(str1.length > 120) str1 = str1.substring(0,120);
          
          var total_duration = ''; 
          var total_duration1 = ''; 
          if(tour.durationType == 'd'){
            total_duration = 'DAY';
            total_duration1 = 'Day';
            if(Math.ceil(tour.no_of_days) > 1){
              total_duration = 'DAYS';
              total_duration1 = 'Days';
            }
          }else if(tour.durationType == 'h'){
            total_duration = 'HOUR';
            total_duration1 = 'Hour';
            if(Math.ceil(tour.no_of_days) > 1){
              total_duration = 'HOURS';
              total_duration1 = 'Hours';
            }
          } 
          if(tour.start_date){
            tour.start_date = formatDate(new Date(tour.start_date),"start");
          }else{
            tour.start_date = '';
          }
          if(tour.end_date){
            tour.end_date = formatDate(new Date(tour.end_date),"end");
          }else{
            tour.end_date = '';
          }
          if(!tour.discount || tour.discount == '.00'){
            tour.discount = 0;
          }
          if(!tour.saving_per_person || tour.saving_per_person == '.00'){
            tour.saving_per_person = '0.00';
          }
          if(!tour.retailcost){
            tour.retailcost = '0.00';
          }



          var html =  '<div class="col-md-4 col-sm-6" onclick="window.location.href=\'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'" >'+
                        '<div class="home-flipContainer flip-container" ontouchstart="this.classList.toggle(\'hover\');">'+
                          '<div class="flipper">'+
                            '<div class="front">'+
                              '<div class="front-image">'+
                                '<img src="'+imgurl+'" alt="eRoam" class="img-responsive"/>';
                                if(tour.flightPrice != 0){
                                  html +='<div class="flight-included">'+
                                    '<div class="flight-includedInner">'+
                                      '<span class="flight-icon first"><i class="icon-plane"></i></span>'+
                                      '<span class="flight-icon second"></span>'+
                                      '<span class="flight-icon third"></span>'+
                                    '</div>'+
                                  '</div>';
                                }
                              html +='</div>'+
                              '<div class="journey-details">'+
                                '<div class="row">'+
                                  '<div class="col-sm-8 col-xs-7 m-t-10">'+
                                    '<h4>Tour Name</h4>'+
                                    '<p class="blue-text tour-name-hidden">'+tour.tour_title+'</p>'+
                                  '</div>'+
                                  '<div class="col-sm-4 col-xs-5 background-gray">'+
                                    '<p class="days-text m-t-10">'+ tour.no_of_days+'<br>'+total_duration1+'</p>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="row">'+
                                  '<div class="col-sm-8">'+
                                    '<p>'+str1+'</p>'+
                                  '</div>'+
                                  '<div class="col-sm-4 background-gray price-content text-right">'+
                                    '<p><span class="days-text blue-text">$'+parseInt(price)+'</span><br/><em>Usually $'+tour.retailcost.slice(0, -2)+'</em></p>'+
                                    '<p class="small-text m-t-30">Flights Inclusive*</p>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="journey-inner">'+
                                '<p>OFFER AVAILABLE FOR</p>'+
                                '<div class="row">'+
                                  '<div class="col-sm-3 col-xs-3">'+
                                    '<strong>02</strong><br/>'+
                                    '<span class="text-uppercase">DAYS</span>'+
                                  '</div>'+
                                  '<div class="col-sm-3 col-xs-3">'+
                                    '<strong>10</strong><br/>'+
                                    '<span class="text-uppercase">HOURS</span>'+
                                  '</div>'+
                                  '<div class="col-sm-3 col-xs-3">'+
                                    '<strong>30</strong><br/>'+
                                    '<span class="text-uppercase">MINS</span>'+
                                  '</div>'+
                                  '<div class="col-sm-3 col-xs-3">'+
                                    '<strong>37</strong><br/>'+
                                    '<span class="text-uppercase">SECS</span>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '<div class="back">'+
                              '<div class="back-inner">'+
                                '<div class="back-title">Tour Name</div>'+
                                '<p class="blue-text">'+tour.tour_title+'</p>'+
                                '<p class="m-t-30"><span class="days-text blue-text">$'+price+'</span><br/><span class="note-text"><em>Per Person. Flights inclusive departing MEL / SYD / BNE</em></span></p>'+
                                '<div class="back-content m-t-20">'+
                                  '<div class="row">';
                                    var land_only = '';
                                    if(tour.flightPrice != 0){
                                      land_only = '(Land Only)';
                                      html +='<div class="col-sm-5 col-xs-5">'+
                                        '<p class="blue-text">Land Only</p>'+
                                      '</div>'+
                                      '<div class="col-sm-7 col-xs-7">'+
                                        '<p><strong>$'+ land_price +'</strong> Per Person</p>'+
                                      '</div>';
                                    }
                                  html +='</div>'+
                                  '<div class="row">'+
                                    '<div class="col-sm-5 col-xs-5">'+
                                      '<p class="blue-text">Savings From</p>'+
                                    '</div>'+
                                    '<div class="col-sm-7 col-xs-7">'+
                                      '<p><strong>'+ ( parseInt(price) - parseInt(land_price)) +'</strong> Per Person</p>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="back-title m-t-20"><p>Tour Details</p></div>'+
                                  '<div class="row">'+
                                    '<div class="col-sm-5 col-xs-5">'+
                                      '<p class="blue-text">Travel Style</p>'+
                                    '</div>'+
                                    '<div class="col-sm-7 col-xs-7">'+
                                      '<p>Small Group Tours</p>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="row">'+
                                    '<div class="col-sm-5 col-xs-5">'+
                                      '<p class="blue-text">Travel Style</p>'+
                                    '</div>'+
                                    '<div class="col-sm-7 col-xs-7">'+
                                      '<p>Small Group Tours</p>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="row">'+
                                    '<div class="col-sm-5 col-xs-5">'+
                                      '<p class="blue-text">Tour Length</p>'+
                                    '</div>'+
                                    '<div class="col-sm-7 col-xs-7">'+
                                      '<p>'+ tour.no_of_days+' Days</p>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="row">'+
                                    '<div class="col-sm-5 col-xs-5">'+
                                      '<p class="blue-text">Tour Dates</p>'+
                                    '</div>'+
                                    '<div class="col-sm-7 col-xs-7">'+
                                      '<p>1 Jan 2018 - 31 Dec 2018</p>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="row">'+
                                    '<div class="col-sm-5 col-xs-5">'+
                                      '<p class="blue-text">Start / Finish</p>'+
                                    '</div>'+
                                    '<div class="col-sm-7 col-xs-7">'+
                                      '<p>Kota Kinabalu - Kota Kinabalu</p>'+
                                    '</div>'+
                                  '</div>'+
                                  '<p class="note-text m-t-20">*From price, discount and savings per person is based on the total price per adult in a twin share room, subject to departure dates.</p>'+
                                '</div>'+
                              '</div>'+
                              '<div class="journeyBack-inner">'+
                                      '<button type="button" onclick="window.location.href=\'/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'\'" class="btn btn-primary btn-block m-t-30">VIEW OFFER</button>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>';    

          $('#tourList').append(html);
          cnt1 = parseInt(cnt1) + 1;
          imageUrl = checkImageUrl(tour.tour_id, imgurl, imgurl2);
      }


      function checkImageUrl(id, url, url2){
          eroam.ajax('post', 'existsImage', {url : url}, function(response){
              if(response == 200){
                  //var image = "{{ url( 'assets/images/no-image.jpg' ) }}";
                  //$("#tourImage_"+id).attr('src', image);
              } else if(response == 400){
                  //var image = "{{ url( 'assets/images/no-image.jpg' ) }}";
                  //$("#tourImage_"+id).attr('src', image);
              } else {
                  eroam.ajax('post', 'existsImage', {url : url2}, function(response){
                      if(response == 200){
                          $("#tourImage_"+id).attr('src', url2);
                      } else if(response == 400){
                          $("#tourImage_"+id).attr('src', url2);
                      } else {
                          var image = "{{ url( 'assets/images/no-image.jpg' ) }}";
                          $("#tourImage_"+id).attr('src', image);
                      }
                  });
              }
          });
      }

      function allowDrop(ev) {
          ev.preventDefault();
      }

      function drag(ev) {
          ev.dataTransfer.setData("text", ev.target.id);
          var data = ev.target.id;

          if($("#"+data).parent().parent().prop('className') == 'drop-list'){
            var interest = data.replace('drag_', 'interest-');
            $(".interestMove").css('opacity','0.5');
            $("#"+interest+".interestMove").css('opacity','');
          }
      }

      function drop(ev) { 
          ev.preventDefault();
          var data = ev.dataTransfer.getData("text");
          var name = data.replace('drag', 'name');
          var inputValue = data.replace('drag', 'inputValue');
          var interest = data.replace('drag_', 'interest-');
          var x = document.getElementById(name);
          var count = 0;
          var text = '';
          var data_value = document.getElementById(data).getAttribute("data-value");
          var data_name = document.getElementById(data).getAttribute("data-name");
          data_value = parseInt(data_value);
          var parentClass = $("#"+data).parent().parent().prop('className');
          var ClassName = $("#"+data).parent().prop('className');

          if(ClassName != "interest-button-active"){
            if($("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
                if (x.style.display === 'none') {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';
                }
                
                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                if( hidden_interest_field.val() ){
                  var interestIndex = activitySequence.indexOf(data_value);
                  activitySequence.splice(interestIndex, 1);
                  hidden_interest_field.val('');
                  hidden_interest_field.attr('name', 'interests[]');
                } else {
                  activitySequence.push(data_value);
                  var interestIndex = activitySequence.indexOf(data_value);
                  hidden_interest_field.val(data_value);
                  hidden_interest_field.attr('name', 'interests['+(interestIndex + 1)+']');
                } 

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));
                var parentDiv = $("#"+data).parent();
                parentDiv.attr('id', interest); 
                parentDiv.attr('data-name', data_name); 

                $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
            }

          } else {
            if(ev.target.getAttribute("class") == 'blankInterest' && $("."+ev.target.getAttribute("class")).parent().prop('className') == 'drop-list'){
      
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);
                activitySequence.push(data_value);

                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                hidden_interest_field.attr('name', 'interests['+ev.target.getAttribute("data-sequence")+']');  

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));
                $("#"+interest).removeClass('interest-button-active').addClass('blankInterest').removeAttr('data-name');
                $("#"+interest).removeAttr('id');
                $("#"+data).parent().attr('id',interest).attr('data-name',data_name);
                $("#"+data).parent().removeClass('blankInterest').addClass('interest-button-active');
            } else {
              if(parseInt(ev.target.getAttribute("data-value")) == data_value){
                var interestIndex = activitySequence.indexOf(data_value);
                activitySequence.splice(interestIndex, 1);

                var hidden_interest_field = $("#"+data).parent().find('.input-interest');
                hidden_interest_field.val('');
                hidden_interest_field.attr('name', 'interests[]');  
                $(".interestMove").css('opacity','');

                ev.target.appendChild(document.getElementById(data));
                ev.target.appendChild(document.getElementById(inputValue));

                $("#"+interest).removeClass('interest-button-active').addClass('blankInterest');
                $("#"+interest).removeAttr('id');
                x.style.display = 'none';
              } 
            }
          } 
      }

     

      function setupLabel() {
          if ($('.label_radio input').length) {
              $('.label_radio').each(function(){ 
                  $(this).removeClass('r_on');
              });
              $('.label_radio input:checked').each(function(){ 
                  $(this).parent('label').addClass('r_on');
                  var inputValue = $(this).attr("value");
                  //alert(inputValue);
                  $("." + inputValue).addClass('mode-block');
                  $("." + inputValue).siblings().removeClass('mode-block');
              });
          };

          if ($('.label_check input').length) {
              $('.label_check').each(function(){ 
                  $(this).removeClass('c_on');
              });
              $('.label_check input:checked').each(function(){ 
                  $(this).parent('label').addClass('c_on');
              });                
          };

      };
      $(document).ready(function(){
          $('.label_check, .label_radio').click(function(){
              setupLabel();
          });
           $('.radio_tailor').click(function(){
               var radio_value = $("input[name='option1']:checked").val();
               $('#search_option_id').val(radio_value);
            $('#project').removeClass('error');
             $('#start_date').removeClass('error');
             $('#end_location').removeClass('error');
             $('#start_location').removeClass('error');

             

             //$('.error').remove();

              $('.radio_tailor').each(function(){
                  if(this.checked){
                    if($(this).val() == 'auto'){
                      $('#departure_to_cls').show();
                      $('#start_location').show();
                      $('#departure_date_cls').removeClass('col-sm-6');
                      $('#departure_date_cls').addClass('col-sm-3');
                      
                    }else if($(this).val() == 'manual'){
                      $('#start_location').hide();
                      $('#departure_to_cls').hide();
                      $('#departure_date_cls').removeClass('col-sm-3');
                      $('#departure_date_cls').addClass('col-sm-6');

                    }
                  }
              }); 
          });
          setupLabel(); 
      });
      function formatDate(date,type) {
          var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
          var day = date.getDate();
          var monthIndex = date.getMonth();
          var year = date.getFullYear();
          if(type == 'start'){
              return day + ' ' + monthNames[monthIndex];
          }else{
              return day + ' ' + monthNames[monthIndex] + ' ' + year;
          }

      }
  </script>
@stop