@extends('layouts.static')


@section('custom-css')
<style>
	.wrapper {
		background-image: url( {{ url( 'assets/img/bg1.jpg' ) }} ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	
</style>
@stop

@section('content')




    <div id="about-us">
		<div class="top-section-image">
			<img src="{{asset('assets/images/bg-image.jpg')}}" alt="" class="img-responsive">
		</div>

		<section class="content-wrapper">
			<article class="tankyou-page">
				<div class="container-fluid">
					<div class="col-md-10 col-md-offset-1">
						<div class="m-t-80">
							
									<h1 class="text-center">Thank you for booking your trip with us !</h1>
									
							
							<div class="m-t-50">

                              <div class="row m-t-80">
								  <div class="col-sm-4 col-sm-offset-4">
									  <a href="/" name="" class="btn btn-primary btn-block">Return to Homepage</a>
								  </div>
							  </div>


							</div>
						</div>
					</div>
				</div>
			</article>
		</section>
    </div>
	

@stop

@section( 'custom-js' )

@stop