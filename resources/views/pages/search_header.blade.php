<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>eRoam</title>

	    <link href="{{ url( 'assets/css/bootstrap.min.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/font-awesome.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/materialize.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/main.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/media.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/datepicker.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/prettify.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/jslider.css' ) }}" rel="stylesheet">
		<script src="{{ url( 'assets/js/theme/jquery.min.js' ) }}"></script>
		<script src="{{ url( 'assets/js/jquery-ui.min.js' ) }}"></script>

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	    <![endif]-->
		

		@yield( 'custom-css' )
	</head>

	<body class="has-js">

		<header>
	      <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container-fluid">
	          <div class="row">
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <div class="navbar-header">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                  <span class="sr-only">Toggle navigation</span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="#"><img src="{{ url( 'assets/images/eRoam_Logo.png' ) }}" alt="eroam" class="img-responsive"></a>
	              </div>
	            </div>
	            <div class="col-md-10 col-sm-10 col-xs-12 padding-left-0">
	              <div id="navbar" class="navbar-collapse collapse">
	                <ul class="nav navbar-nav">
	                  <li class="search-text">SEARCH:</li>
	                  <li class="active"><a href="#">Create Your Own Holiday</a></li>
	                  <li><a href="#">Hotel</a></li>
	                  <li><a href="#">Flight</a></li>
	                  <li><a href="#">Activity</a></li>
	                  <li><a href="#">Car Hire</a></li>
	                </ul>
	                <ul class="nav navbar-nav navbar-right">
	                  <li><a href="/">HOME</a></li>
	                  <li><a href="/about-us">ABOUT US</a></li>
	                  <li><a href="/login">LOGIN</a></li>
	                  <li class="dropdown">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">$AUD <span class="caret"></span></a>
	                    <ul class="dropdown-menu">
	                     	<?php $currencies = session()->get('all_currencies'); ?>
                       		@foreach( $currencies as $currency )
                       			<li><a href="#" value="{{ $currency['code'] }}" class="header_currency_option">{{ '('. $currency['code'] .') '.$currency['name'] }}</a></li>
                       		@endforeach;
	                    </ul>
	                  </li>
	                </ul>
	              </div>
	            </div>
	         </div>
	       </div>
	      </nav>
	    </header>		