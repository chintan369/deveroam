@extends( 'layouts.search' )
<?php
$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
?>
@section('custom-css')
<style type="text/css">
  #checkin-date {
    background-color: #fff;
    color: #000;
  }
  #checkout-date {
    background-color: #fff;
    color: #000;
  }
</style>
@stop
@section('content')
<div class="tabs-main-container accomodation-tabs-main-container">
  <div class="tabs-container">
    <div class="tabs-container-inner">
      <h2 class="topTitle"><i class="icon icon-itinerary"></i><span>View Detailed Itinerary</span></h2>
      <div class="row m-t-10">
        <div class="col-sm-3 date-control">
          <div class="panel-form-group form-group">
            <label class="label-control">Check-in Date:</label>
            <div class="input-group datepicker">
              <input id="checkin-date" type="text" placeholder="{{$startDate}}" value="{{ $startDate }}" class="form-control" disabled="disabled">
              <span class="input-group-addon"><i class="icon-calendar"></i></span>
            </div>
          </div>
        </div>
        <div class="col-sm-3 date-control">
          <div class="panel-form-group form-group">
            <label class="label-control">Check-out Date:</label>
            <div class="input-group datepicker">
              <input id="checkout-date" type="text" placeholder="{{ $endDate }}" value="{{ $endDate }}" class="form-control" disabled="disabled">
              <span class="input-group-addon"><i class="icon-calendar"></i></span>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="panel-form-group form-group">
            <label class="label-control">Rooms</label>
            <select class="form-control">
              <option>1</option>
              <option>2</option>
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="panel-form-group form-group">
            <label class="label-control">Adults (18+)</label>
            <select class="form-control">
              <option {{ $travellers==1?'selected':''}}>1</option>
              <option {{ $travellers==2?'selected':''}}>2</option>
              <option {{ $travellers==3?'selected':''}}>3</option>
              <option {{ $travellers==4?'selected':''}}>4</option>
              <option {{ $travellers==5?'selected':''}}>5</option>
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="panel-form-group form-group">
            <label class="label-control">Children (0-17)</label>
            <input class="form-control disabled" data-toggle="tooltip" readonly="" title="" value="0" data-original-title="Not Available in Pilot" type="text">
          </div>
        </div>
      </div>
      <div class="m-t-20 row">
        <div class="col-sm-4">
          <a href="javaScript:void(0)" name="" class="btn btn-primary btn-block active m-b-10">VIEW ITINERARY</a>
        </div>
        <div class="col-sm-4">
          <a href="#" class="btn btn-primary btn-block m-b-10">PRINT / SHARE ITINERARY</a>
        </div>
        <div class="col-sm-4">
          <a href="{{url('/payment-summary/')}}" name="" class="btn btn-primary btn-block m-b-10">BOOK ITINERARY</a>
        </div>
      </div>
    </div>
  </div>
  <div class="tabs-content-container">
    <div class="accomodation-wrapper">
      <div class="m-t-20 payment-steps">
        <ul>
          @foreach (session()->get('search')['itinerary'] as $key => $leg)
          <li>
            <a href="#">
              <span class="stepCircle">{{ $leg['city']['default_nights']}}</span>
              <p><strong>{{ str_limit($leg['city']['name'], 18) }}</strong><br/>{{ $leg['city']['country']['name'] }}</p>
            </a>
          </li>
          @endforeach
        </ul>
      </div>
      <div class="m-t-20">
        <div class="paymentMap">
          <div id="map" style="height: 400px;"></div>
        </div>
      </div>
      <div class="tripCity-list m-t-20">
        @if (session()->get('search'))
          <?php $i=1; ?>
          <?php //echo "<pre>"; print_r(session()->get('search')); exit(); ?>
          @foreach (session()->get('search')['itinerary'] as $key => $leg)
            <div class="tripCity-listBox">
              <div class="tripCity-listInner">
                <div class="tripCity-inner">
                  <div class="border-bottom-gray">
                    <div class="row">
                      <div class="col-sm-7">
                        <h4><strong><strong>{{$leg['city']['name']}}</strong>, {{$leg['city']['country']['name']}}</strong></h4>
                        <p class="tripDate">
                          <i class="icon icon-calendar accomomdation-icon"></i>
                          {{date('d F Y', strtotime($leg['hotel']['checkin']))}}
                        </p>
                      </div>
                      <div class="col-sm-5">
                        <div class="row">
                          <div class="col-xs-9 price-right border-right">
                            <p></p>
                            <ul class="paymentIcons">
                              <li><a href="#"><i class="icon icon-hotel"></i></a></li>
                              <li><a href="#"><i class="icon icon-activity"></i></a></li>
                              <li><a href="#"><i class="icon icon-bus"></i></a></li>
                            </ul>
                          </div>
                          <div class="col-xs-3 text-center">
                            <h3 class="transport-price"><strong>{{$leg['hotel']['nights']}}</strong></h3>
                            <span class="transport-nights">Nights</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tripCity-inner">
                  <div class="clearfix">
                    <div class="accomodation-listBoxImg">
                      <img src="https://cms.eroam.com/{{$leg['city']['image'][0]['small'] }}" alt="" class="img-responsive">
                    </div>
                    <div class="accomodation-listBoxDetails">
                      <p>{!! substr($leg['city']['description'], 0, 500) !!}</p>
                      <div class="m-t-20">
                        <a href="javascript://" class="trip-view" data-id="trip{{$i}}"><i class="fa fa-angle-down"></i> Click to view detailed itinerary</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tripDetails-wrapper" id="trip{{$i}}">
                @if(isset($leg['hotel']) && !empty($leg['hotel']))
                  <div class="tripDetails-maincontainer">
                    <div class="tripDetails-container">
                      <div class="tripDetails-top">
                        <div class="row">
                          <div class="col-sm-8">
                            <span class="m-r-10"><i class="icon icon-hotel"></i></span> <strong>ACCOMMODATION</strong>
                          </div>
                          <div class="col-sm-4 tripIcons">
                            <a href="#"><i class="fa fa-pencil"></i></a>
                            <a href="#"><i class="fa fa-ban"></i></a>
                          </div>
                        </div>
                      </div>
                      <div class="tripDetails-bottom">
                        <div class="table-responsive">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Booking#</th>
                                <th>Location</th>
                                <th>Check In</th>
                                <th>Check out</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr class="tourTripName" data-id="tourTrip{{$i}}">
                                <td>{{date('d M Y', strtotime($leg['hotel']['checkin']))}}</td>
                                <td>{{$leg['hotel']['name']}}</td>
                                <td>Pending</td>
                                <td>{{$leg['hotel']['address1']}}</td>
                                <td>{{date('d M Y', strtotime($leg['hotel']['checkin']))}}</td>
                                <td>{{date('d M Y', strtotime($leg['hotel']['checkout']))}}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="tripHotel-mainWrapper">
                      <div class="tripHotel-wrapper" id="tourTrip{{$i}}">
                        <div class="accomodation-list" data-id="accomodation{{$i}}">
                          <div class="accomodation-listBox m-t-20">
                            <div class="row">
                              <div class="col-sm-7">
                                <h4><a href="#">{{$leg['hotel']['name']}}</a></h4>
                                <p>
                                  <i class="icon icon-location accomomdation-icon"></i>
                                  Location: {{$leg['city']['country']['name']}}, {{$leg['hotel']['city']}} / {{$leg['hotel']['address1']}}
                                </p>
                              </div>
                              <div class="col-sm-5">
                                <div class="row">
                                  <div class="col-xs-9 price-right border-right">
                                    <?php
                                      $singleRate = '@nightlyRateTotal';
                                      $singleRate = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];

                                      $singleRate = number_format(($singleRate * $eroamPercentage) / 100 + $singleRate, 2);
                                      $subTotal = $singleRate;
                                      $taxes = 0;

                                      $totalNights = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['NightlyRatesPerRoom']['@size'];

                                      if (isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])):

                                        $taxes = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
                                        $taxesPerDay = $taxes / $totalNights;

                                      endif;

                                      $ratePerDay = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@averageBaseRate'];

                                      $ratePerDay = number_format(($ratePerDay * $eroamPercentage) / 100 + $ratePerDay + $taxesPerDay, 2);

                                      $selectedRate = $subTotal + $taxes;
                                    ?>
                                    <p>From <strong>${{$currency}} {{$selectedRate}}</strong> Per Person<br/>
                                      <span class="blue-text">eRoam Discount 25%</strong></span><br/>
                                      <span class="blue-text">PACKAGE RATE</strong></span>
                                    </p>
                                  </div>
                                  <div class="col-xs-3 text-center">
                                    <h3 class="transport-price"><strong>{{$leg['hotel']['nights']}}</strong></h3>
                                    <span class="transport-nights">Nights</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <hr/>
                            <div class="clearfix">
                              <div class="accomodation-listBoxImg">
                                <img src="https://cms.eroam.com/{{$leg['city']['image'][0]['small']}}" alt="" class="img-responsive" />
                              </div>
                              <div class="accomodation-listBoxDetails">
                                <ul class="rating">
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                                <div class="hotelFacility">
                                  {!! $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RoomType']['descriptionLong'] !!}
                                  <!-- <ul>
                                    <li>VIP Room Upgrades &amp; More</li>
                                    
                                    @if (strpos($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RoomType']['descriptionLong'], 'Non-Smoking') !== false)
                                    <li>Smoke-free property</li>
                                    @endif
                                    <li>Popular property highlights</li>
                                    <li>Babysitting or childcare (surcharge)</li>
                                    @if (strpos($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RoomType']['descriptionLong'], 'Free WiFi') !== false)
                                    <li>Free WiFi</li>
                                    @endif
                                    <li>24-hour front desk</li>
                                    <li>Restaurant</li>
                                  </ul> -->
                                </div>
                                <div class="accomodation-desc m-t-20">
                                  {!! $leg['hotel']['shortDescription'] !!}
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tripHotel-wrapper" id="tourTrip2">
                        <div class="accomodation-list" data-id="accomodation1">
                          <div class="accomodation-listBox m-t-20">
                            <div class="row">
                              <div class="col-sm-7">
                                <h4><a href="#">Hotel Name</a></h4>
                                <p>
                                  <i class="icon icon-location accomomdation-icon"></i>
                                  Location: Country, City / Suburb
                                </p>
                              </div>
                              <div class="col-sm-5">
                                <div class="row">
                                  <div class="col-xs-9 price-right border-right">
                                    <p>From <strong>$AUD 2799.00</strong> Per Person<br/>
                                      <span class="blue-text">eRoam Discount 25%</strong></span><br/>
                                      <span class="blue-text">PACKAGE RATE</strong></span>
                                    </p>
                                  </div>
                                  <div class="col-xs-3 text-center">
                                    <h3 class="transport-price"><strong>00</strong></h3>
                                    <span class="transport-nights">Nights</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <hr/>
                            <div class="clearfix">
                              <div class="accomodation-listBoxImg">
                                <img src="images/accomodation-default-image.jpg" alt="" class="img-responsive" />
                              </div>
                              <div class="accomodation-listBoxDetails">
                                <ul class="rating">
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                                <div class="hotelFacility">
                                  <ul>
                                    <li>VIP Room Upgrades &amp; More</li>
                                    <li>Smoke-free property</li>
                                    <li>Popular property highlights</li>
                                    <li>Babysitting or childcare (surcharge)</li>
                                    <li>Free WiFi</li>
                                    <li>24-hour front desk</li>
                                    <li>Restaurant</li>
                                  </ul>
                                </div>
                                <div class="accomodation-desc m-t-20">
                                  <p>Along with a restaurant, this hotel has an outdoor pool and a health club. Free WiFi in public areas and free self parking are also provided. Additionally, a bar/lounge, a coffee shop/café, and a sauna are onsite.</p>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                          <div class="accomodation-listBox m-t-20">
                            <div class="row">
                              <div class="col-sm-7">
                                <h4><a href="#">Hotel Name</a></h4>
                                <p>
                                  <i class="icon icon-location accomomdation-icon"></i>
                                  Location: Country, City / Suburb
                                </p>
                              </div>
                              <div class="col-sm-5">
                                <div class="row">
                                  <div class="col-xs-9 price-right border-right">
                                    <p>From <strong>$AUD 2799.00</strong> Per Person<br/>
                                      <span class="blue-text">eRoam Discount 25%</strong></span><br/>
                                      <span class="blue-text">PACKAGE RATE</strong></span>
                                    </p>
                                  </div>
                                  <div class="col-xs-3 text-center">
                                    <h3 class="transport-price"><strong>00</strong></h3>
                                    <span class="transport-nights">Nights</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <hr/>
                            <div class="clearfix">
                              <div class="accomodation-listBoxImg">
                                <img src="images/accomodation-default-image.jpg" alt="" class="img-responsive" />
                              </div>
                              <div class="accomodation-listBoxDetails">
                                <ul class="rating">
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                  <li><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                                <div class="hotelFacility">
                                  <ul>
                                    <li>VIP Room Upgrades &amp; More</li>
                                    <li>Smoke-free property</li>
                                    <li>Popular property highlights</li>
                                    <li>Babysitting or childcare (surcharge)</li>
                                    <li>Free WiFi</li>
                                    <li>24-hour front desk</li>
                                    <li>Restaurant</li>
                                  </ul>
                                </div>
                                <div class="accomodation-desc m-t-20">
                                  <p>Along with a restaurant, this hotel has an outdoor pool and a health club. Free WiFi in public areas and free self parking are also provided. Additionally, a bar/lounge, a coffee shop/café, and a sauna are onsite.</p>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
                @if(isset($leg['activities']) && !empty($leg['activities']))
                  <div class="tripDetails-maincontainer">
                    <div class="tripDetails-container m-t-20">
                      <div class="tripDetails-top">
                        <div class="row">
                          <div class="col-sm-8">
                            <span class="m-r-10"><i class="icon icon-activity"></i></span> <strong>TOURS / ACTIVITIES</strong>
                          </div>
                          <div class="col-sm-4 tripIcons">
                            <a href="#"><i class="fa fa-pencil"></i></a>
                            <a href="#"><i class="fa fa-ban"></i></a>
                          </div>
                        </div>
                      </div>
                      <div class="tripDetails-bottom">
                        <div class="table-responsive">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Booking#</th>
                                <th>Location</th>
                                <th>Start Time</th>
                                <th>Duration</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($leg['activities'] as $activity)
                                <tr>
                                  <td>{{date('d M Y', strtotime($activity['date_selected']))}}</td>
                                  <td>{{$activity['name']}}</td>
                                  <td>Pending</td>
                                  <td>{{ $activity['location'] != ''?$activity['location']:'' }}</td>
                                  <td>Not Available</td>
                                  <td>{{$activity['duration1']}}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
                @if(isset($leg['transport']) && !empty($leg['transport']))
                  <div class="tripDetails-maincontainer">
                    <div class="tripDetails-container m-t-20">
                      <div class="tripDetails-top">
                        <div class="row">
                          <div class="col-sm-8">
                            <span class="m-r-10"><i class="icon icon-bus"></i></span> <strong>TRANSPORT</strong>
                          </div>
                          <div class="col-sm-4 tripIcons">
                            <a href="#"><i class="fa fa-pencil"></i></a>
                            <a href="#"><i class="fa fa-ban"></i></a>
                          </div>
                        </div>
                      </div>
                      <div class="tripDetails-bottom">
                        <div class="table-responsive">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Booking#</th>
                                <th>Departure Time</th>
                                <th>Arrival / Time</th>
                                <th>Duration</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <?php
                                  $travelDate = explode('<br/>', $leg['transport']['departure_text']);
                                  $travelDate = explode('@', $travelDate[1]);
                                  $departure_time = explode('<br/>', $leg['transport']['departure_text']);
                                  $arrival_time = explode('<br/>', $leg['transport']['arrival_text']);
                                ?>
                                <td><?php echo date('d M Y', strtotime($travelDate[0])); ?></td>
                                <td>{{ $leg['transport']['supplier']['name'] }}</td>
                                <td>Pending</td>
                                <td>{{$departure_time[1]}}</td>
                                <td>{{$arrival_time[1]}}</td>
                                <td>{{ str_replace('+', '', $leg['transport']['duration'])}}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
              </div>
            </div>
            <?php $i++; ?>
          @endforeach
        @endif

      </div>
      
    </div>
  </div>
</div>
<input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
  <input type="hidden" id="all-cities" value="{{ json_encode(Cache::get('cities')) }}">
@stop
@section( 'custom-js' )
<script type="text/javascript" src="{{ url('assets/js/eroam-map.js') }}"></script>
<script src="{{ url('/assets/js/jquery.scrollbar.js') }}"></script>
<script type="text/javascript" src="{{ url('/assets/js/jquery.slimscroll.js') }}"></script>
    
<script>
$('body').on('click', '.btn-email-friend', function(){
$('.pax-modal').modal();
});
jQuery.validator.addMethod("lettersonly", function(value, element) {
return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
}, "Letters only please.");
$("#myForm").validate({
rules: {
emails: {
required: true,
email:true
},
name: {
required: true,
lettersonly: true
}
},
messages: {
email: {
required: "Please enter Email."
}
},
errorPlacement: function (label, element) {
label.insertAfter(element);
},
submitHandler: function (form) {
emailFriend();
}
});
function emailFriend(){
//$('body').on('click', '.send-email', function(){
// e.preventDefault();
if($('#emails').val() && $('#name').val()){
var email = $('#emails').val();
var name = $('#name').val();
// $('#name').css("border-color", "");
// $('#emails').css("border-color", "");
// $('#rror-sending').hide();
if (!(isValidEmailAddress(email))) {
// $('#error-email').show();
// $('#emails').css("border-color", "red");
return false;
}
$.ajax({
url: 'send/itinerary',
type: 'GET',
data: {name:name,emails: email},
success:function(response){
console.log(response);
$( "#name" ).val("");
$( "#emails" ).val("");
$( ".success-box" ).show();
$( ".send-email" ).text('Send');
$( "#name" ).focus();
$(".send-email").prop("disabled",false);
$( ".discard" ).prop("disabled",false);
}, beforeSend: function(){
$( ".send-email" ).text('Sending ');
$( ".send-email" ).append( '<i class="fa fa-refresh fa-spin center" aria-hidden="true"></i>' );
$( ".send-email" ).prop("disabled",true);
$( ".discard" ).prop("disabled",true);
},
error: function (jqXHR, exception) {
$('#error-sending').show();
},
});
}else{
// $('#name').css("border-color", "");
// $('#emails').css("border-color", "");
// if(!($('#name').val())){
//  $('#name').css("border-color", "red");
// }
// if(!($('#emails').val())){
//  $('#emails').css("border-color", "red");
// }
// $('#pax-details-error-text').show();
}
}
function isValidEmailAddress(email) {
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
return regex.test(email);
}
</script>

<script type="text/javascript">
      $("#start_date1, #start_date2, #start_date3, #start_date4, #checkin-date, #checkout-date").datepicker({
        format: 'dd M yyyy',
        autoclose: true,
        todayHighlight: true
      });

      function tripHideShow(){  
        $('.trip-view').click(function(){
            var href_value = $(this).attr('data-id');
            $('#' + href_value).toggle();
            $('#' + href_value).toggleClass("open");
            $(this).toggleClass("active");
            $('#' + href_value).parent('.tripCity-listBox').toggleClass('open');
        });
      }

      function tripHotelHideShow(){  
        $('.tourTripName').click(function(){
            var href_value = $(this).attr('data-id');
            $('#' + href_value).show();
            $('#' + href_value).addClass("open");
            $('#' + href_value).siblings('.tripHotel-wrapper').hide();
            $('#' + href_value).siblings('.tripHotel-wrapper').removeClass('open');
            $('.tourTripName').removeClass('active');
            $(this).addClass("active");
        });
      }

      $('.accomodation-wrapper').slimScroll({
          height: '100%',
          color: '#212121',
          opacity: '0.7',
          size: '5px',
          allowPageScroll: true
      });

      jQuery(document).ready(function(){
        calculateWidth();
        jQuery('.scrollbar-outer').scrollbar();
      });

      $(function() {
        //$('.search-box').eq(0).show();
        $('.package-select').click(function(){
            var href_value = $(this).attr('data-id');
            //alert(href_value);
            $('#' + href_value).show();
            $('#' + href_value).addClass("open-block");
            $('#' + href_value).siblings().hide();
        });
      });

      function setupLabel() {
          if ($('.label_radio input').length) {
              $('.label_radio').each(function(){ 
                  $(this).removeClass('r_on');
              });
              $('.label_radio input:checked').each(function(){ 
                  $(this).parent('label').addClass('r_on');
                  var inputValue = $(this).attr("value");
                  //alert(inputValue);
                  $("." + inputValue).addClass('mode-block');
                  $("." + inputValue).siblings().removeClass('mode-block');
              });
          };

          if ($('.label_check input').length) {
              $('.label_check').each(function(){ 
                  $(this).removeClass('c_on');
              });
              $('.label_check input:checked').each(function(){ 
                  $(this).parent('label').addClass('c_on');
              });                
          };

      };
      $( window ).load( eMap.init );
      $(document).ready(function(){

          $('.label_check, .label_radio').click(function(){
              setupLabel();
          });
          setupLabel(); 
          //map();
          leftpanel();
          calculateHeight();
          leftStripHeight();
          //calculateWidth();
          tripHotelHideShow();
          tripHideShow();
      });

      function calculateWidth(){
        var owidth = $('.payment-steps').parent().width();
        //alert(owidth);
        var elem = $('.payment-steps li').length;
        //alert(owidth);
        //alert(elem);
        if (elem >6) {
          //var elemWidth = owidth / 6;
          var elemWidth = 200;
          var fullWidth = elemWidth * elem;
          $(".payment-steps ul").width(fullWidth);
          $('.payment-steps').addClass('scrollbar-outer');
        }else{
          var elemWidth = (owidth / elem)-1;
        }
        $(".payment-steps li").width(elemWidth);
      }

      function leftpanel(){
        var w = $( document ).width();
          if(w > 991){
            $('.arrow-btn-new').click(function(){
              if($('.arrow-btn-new').hasClass('open')){
                $('.page-sidebar').css('display', 'none');
                $(this).removeClass('open');
                $('.page-content').css('margin-left', '0px');
              } else{
                $('.page-sidebar').css('display', 'block');
                $(this).addClass('open');
                $('.page-content').css('margin-left', '354px');
              }
          });
          } else{
            $('.arrow-btn-new').click(function(){
              if($('.arrow-btn-new').hasClass('open')){
                $('.page-sidebar').css('display', 'none');
                $(this).removeClass('open');
                $('.page-content').css('margin-left', '0px');
                $('.left-strip').css('left', '0px');
               
              } else{
                $('.page-sidebar').css('display', 'block');
                $(this).addClass('open');
                $('.page-content').css('margin-left', '0px');
                $('.left-strip').css('left', '300px');
              }
          });
          $('.pageSidebar-inner').slimScroll({
              height: '100%',
              color: '#212121',
              opacity: '0.7',
              size: '5px',
              allowPageScroll: true
          });
        }
      }

      function calculateHeight(){
        var winHeight = $(window).height();
        var oheight = $('.page-sidebar').outerHeight();
        var elem = $('.page-content .tabs-container').outerHeight();
        var elemHeight = oheight - elem;
        var winelemHeight = winHeight - elem;
        if(winHeight < oheight){ 
          $(".page-content .tabs-content-container").outerHeight(elemHeight);
        } else{
          $(".page-content .tabs-content-container").outerHeight(winelemHeight);
        }
      }

      $(window).resize(function(){
        leftpanel();
        calculateHeight();
        leftStripHeight();
        calculateWidth()
      });

      function leftStripHeight(){
        var stripHeight = $('.tabs-main-container').outerHeight();
        var lefticons = $('.leftStrip-icons').outerHeight();
        var arrowHeight = stripHeight - lefticons;
        $(".arrow-btn-new").outerHeight(arrowHeight);
      }

      function map(){
        // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'eRoam!'
                });
            }
      }

    </script>@stop