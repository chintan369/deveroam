@extends('layouts.search')
<?php
$total = $totalCost * $travellers;
$gst = $total * 0.025;
$finalTotal = number_format($total + $gst, 2, '.', ',');
$eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
?>
@section('custom-css')
	<style type="text/css">
		#itr {
			padding: 2rem;
		}
		.itr-leg {
			background: #ffffff;
			margin-bottom: 2rem;
		}
		.itr-leg .heading {
			border: 1px solid #B2B2B2;
			border-bottom: 1px solid white;
			position: relative;
			bottom: -1px;
			display: inline-block;
			font-weight: 400;
			margin: 0;
			padding: 2rem;
		}
		.itr-leg .heading i:before {
			font-size: 25px;
			color: #2AA9DF;
		}
		.itr-leg .body {
			border: 1px solid #B2B2B2;
			padding: 2rem;
		}
		.itr-leg .body section {
			margin-bottom: 2rem;
		}
		.itr-leg .body .title {
			font-size: 17px;
			text-transform: uppercase;
		}
		.itr-leg .body .desc {
			margin-left: 8.8rem;
		}
		.itr-leg .body .desc p {
			line-height: 1em;
		}
		.itr-leg .body .desc p i.status-icon {
			margin-left: 1.5rem;
			margin-right: .2rem;
			color: #009688;
			font-weight: 700;
		}
		span.not-available,
		span.not-available i {
			color: #d32f2f !important;
		}
		.itr-leg .body .desc p span.price {
			font-weight: 700;
			font-size: 18px;
			color: #009688;
		}
		.itr-leg .body .title i:before {
			position: relative;
			top: 5px;
			border-radius: 100px;
			padding: 1.5rem;
			color: #ffffff;
			font-size: 25px;
			margin-right: 2.8rem;
		}
		.itr-leg .body .accomodation .title i:before {
			background: #00bcd4;
		}
		.itr-leg .body .activities .title i:before {
			background: #2196f3;
		}
		.itr-leg .body .transport .title i:before {
			background: #f44336;
		}
		.change-btn i {
			font-size: 1.6rem;
			margin-left: 3rem;
			color: #f8f8f8 !important;
			background: #2AA9DF;
			padding: 8px;
			display: inline-block;
			transition: all 100ms ease-in-out 0s;
		}
		.change-btn i:hover {
			text-decoration: none;
			background: #2792BF;
		}
		.old-price {
			text-decoration: line-through;
			color: #ABABAB;
			font-weight: 400;
			margin-left: 1rem;
		}
		#review-summary {
			border: 1px solid #B2B2B2;
			padding: 5px;
			margin-top: 11px;
		}
		#review-summary h3 {
			margin-top: 0;
		}
		#total {
			font-size: 16px;
		}
		.white-bg{min-height: initial;}
	</style>
@endsection
@section('content')
	@if(session()->has('register_confirm'))
		<p class="success-box m-t-30">
			{{ session()->get('register_confirm') }}
		</p>
	@endif
	<div class="tabs-main-container accomodation-tabs-main-container">
		<div class="tabs-container">
			<div class="tabs-container-inner">
				<h2 class="topTitle"><i class="icon icon-itinerary"></i><span>Book Itinerary</span></h2>
				<div class="m-t-20 row">
					<div class="col-sm-4">
						<a href="{{ url('/view-itinerary') }}" name="" class="btn btn-primary btn-block m-b-10">VIEW ITINERARY</a>
					</div>
					<div class="col-sm-4">
						<a href="{{ url('/proposed-itinerary-pdf/') }}" target="_blank"  class="btn btn-primary btn-block m-b-10">PRINT / SHARE ITINERARY</a>
					</div>
					<div class="col-sm-4">
						<a href="#" name="" class="btn btn-primary btn-block active m-b-10">SAVE ITINERARY</a>
					</div>
				</div>
			</div>
		</div>
		<div class="tabs-content-container">
			<div class="accomodation-wrapper">
				<div class="paymentTopbox">
					<div class="row">
						<div class="col-sm-7 m-t-5">
							<h4><strong>Multi-City Tailormade Auto</strong></h4>
							<p class="m-t-10">
								<i class="icon icon-calendar"></i>
								31 January 2018 - 11 February 2018
							</p>
						</div>
						<div class="col-sm-5">
							<div class="row">
								<div class="col-xs-9 price-right border-right">
									<p></p>
									<ul class="paymentIcons">
										<li><a href="#"><i class="icon icon-hotel"></i></a></li>
										<li><a href="#"><i class="icon icon-activity"></i></a></li>
										<li><a href="#"><i class="icon icon-bus"></i></a></li>
									</ul>
								</div>
								<div class="col-xs-3 text-center">
									<h3 class="transport-price"><strong>11</strong></h3>
									<span class="transport-nights">Days</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="paymentCityBox">
					<h4 class="border-bottom-gray">Returning Customers</h4>
					<div class="row m-t-20">
						<div class="col-sm-8">
							Sign in with your social network
						</div>
						<div class="col-sm-4 text-right">
							<a href="#"><i class="fa fa-lock"></i> Secure</a>
						</div>
					</div>
					<div class="row m-t-20">
						<div class="col-xs-6">
							<a href="{{ url('redirect/google') }}" class="google-login"><i class="fa fa-google-plus"></i> <span>GOOGLE</span></a>
						</div>
						<div class="col-xs-6">
							<button type="" class="fb-login"  data-toggle="tooltip" title="Not Available in Pilot"><i class="fa fa-facebook"></i> <span>FACEBOOK</span></button>
						</div>
					</div>
					<div class="login-or m-t-30">
						<span>or</span>
					</div>
					<form class="form-horizontal" id="login_form2">
						{{ csrf_field() }}
						<p class="error" id="login-error-message2"></p>
						<div class="panel-form-group input-control form-group">
							<label class="label-control">Email Address</label>
							<input type="text" name="username_login" id="username_login2" class="form-control" placeholder="kane@eroam.com">
						</div>
						<div class="panel-form-group input-control form-group">
							<label class="label-control">Password</label>
							<input type="password" style="display:none">
							<input type="password" name="password_login" id="password_login2" class="form-control" placeholder="....">
						</div>
						<div class="form-group">
							<label class="radio-checkbox label_check" for="checkbox-login"><input type="checkbox" id="checkbox-login2" value="4">Keep Me Signed In.</label>
						</div>
						<div class="form-group">
							<input type="submit" id="submitLoginForm2" name="" class="btn btn-black btn-block" value="CHECKOUT">
						</div>
						<div class="text-right">
							<a href="#" data-toggle="tooltip" title="Not Available in Pilot">Recover my password</a>
						</div>
					</form>
				</div>
				<div class="paymentCityBox">
					<h4 class="border-bottom-gray">Guest Checkout</h4>
					<div class="m-t-20">
						<p>Proceed to checkout, and you can create an eRoam account at the end.</p>
						<a href="{{ url('/review-itinerary/') }}" name="" class="btn btn-primary btn-block m-t-40">CONTINUE AS GUEST</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection