@extends('layouts.home')


@section('custom-css')
<style>
	.wrapper {
		background-image: url( {{ url( 'assets/img/bg1.jpg' ) }} ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}
	.main-content{
		padding: 10px 0 265px 0;
	}
	.full-width{
		width: 100%;
	}
	.half-width{
		width: 50%;
	}
	.table-display{
		display: table;
	}
	.no-radius{
		border-radius: 0px !important;
	}
	
	.default-border{
		border: 1px solid #2AA9DF !important;
	}
	.white-button{
		background: #fff;
	    color: #2AA9DF;
	    padding: 11px;
	    font-weight: bold;
	}

	.default-button{
		background: #BEE5F6;
	    border-color: #2AA9DF;
	    color: #2AA9DF;
	    padding: 11px;
	    font-weight: bold;
	}
	.blue-button{
		background: #2AA9DF;
	    border-color: #2AA9DF;
	    color: #fff;
	    padding: 11px;
	    font-weight: bold;
	}
	.manual.default-button:hover,
	.auto.default-button:hover, 
	.manual.default-button.focus, 
	.auto.default-button.focus,
	.blue-button:hover,
	.blue-button.focus{
		background: #BEE5F6;
		color: #2AA9DF;
		border-color: #2AA9DF;

	}
	.auto.white-button:hover, .manual.white-button:hover{
		background: #BEE5F6;
		color: #2AA9DF;
	}
	.select-style{
		text-align-last: center;
		border: 1px solid #000;	
		padding: 11px;
		width: 100%;
		background: #fff;
	}
	.select-padding{
		padding: 12px 0 12px 0;
	}
	.col-reduced-padding{
		padding-right: 2px ;
		padding-left: 2px ;
	}
	.col-reduced-padding:last-child{
		padding-right:0 ;
	}
	.col-reduced-padding:first-child{
		padding-left:0 ;
	}

	.last{
		padding-right:0 ;
	}
	.first{
		padding-left:0 ;
	}
	#homepage form .multi-city,
	#homepage form #search{
		padding-top: 20px;
	}
	#homepage #homepage-desc{
		padding-top: 10px;
		line-height: 1.3em;
	}

	.interest-lists > li {
		display: inline-block;
		margin-bottom: 5px;
	}
	#traveler-option-toggle-box{
		display: none;
	}
	.hide{
		display: none;
	}
	.wrapper{
		background-attachment: fixed;
	}
	#profile-preferences-toggler, #check-box{
		cursor: pointer;
	}
	#profile-preferences-toggler > i, #check-box > i{
		vertical-align: middle;
	}
	#profile-preferences-toggler > p, #check-box  > p{
		padding-left: 12px;
		display: inline-block;
	}
	#traveler-option-toggle-box{
		margin-top: 20px;
	}
	.interest-button-active, .interest-lists > li:hover{
		background:  rgba(39, 168, 223, 0.44);
		color: #2AA9DF;
	}
	#profile-preferences-data{
		line-height: 1.7em;
	}
	#profile-preferences-data > span {
		padding-right: 15px;
	}
	#traveler-option-toggle-box > div > select{
		margin-bottom: 7px;
	}
	.country-button{
		background: #fff;
	    color: #333;
	  
	}

	.btn.focus{
		outline: none !important;
	}
	.country-city-container  > ul{
		list-style: none;
	}
	.country-city-container  > ul > li{
		display: inline-block;
		margin-bottom: 5px;
		margin-right: 5px;
		color: #2AA9DF;
	}
	.country-city-container > ul > li:hover{
		background:  rgba(39, 168, 223, 0.44);
		color: #2AA9DF;
	}
	.modal-content{
		border-radius: 3px;
	}
	.modal-footer{
		background: #008ed4;
	}
	#all-countries-container{
		display: none;
	}
	.destination-container{
		display: none;
	}
	.col-md-2-4 {
	   	width: 20%;
	   	float: left;
	}

	button{
		overflow: hidden;
	}
	.icon-style{
		border: 2px solid #918F8F;
	    border-radius: 4px;
	    padding: 6px 0;
	    text-align: center;
	    width: 37px;
	    font-size: 21px;
	}
	.check-box{
		color: #fff;
	}
	.black-background{
		background: #000;
	}
	#no-time-box {
		display: none;
	}
</style>
@stop

@section('content')
<div class="row">
	 <div class="white-box top-margin col-md-6 col-md-offset-3  top-bottom-paddings " id="homepage">
        <div class="col-xs-8 col-xs-offset-2" >

			<form action="" method="POST">
			{{ csrf_field() }}
			  <div class="form-group">
			    <label for="exampleInputEmail1">Username</label>
			    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Username">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			  </div>

			  <button type="submit" class="btn blue-button full-width no-radius search">LOGIN</button>
			</form>
		</div>
 	</div>
</div>

@stop

@section( 'custom-js' )
<script>
	



</script>
@stop