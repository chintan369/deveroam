<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>:: eRoam ::</title>
		<style type="text/css">
		body{
		/*font-family: Arial;*/
		color: #212121;
		font-size: 14px;
		page-break-inside: avoid;
		}
		p{
		margin: 0px;
		font-size: 13px;
		}
		/*.payment-steps ul{margin:0; padding: 0;}
		.payment-steps ul li{list-style: none; float: left; width: 25%; text-align: center;}
		.stepCircle{width: 30px; height: 23px; background-color: #ffffff; border: solid 1px #249DD0; border-radius: 50%; margin: 0 auto 10px auto; position: relative; z-index: 1; display: inline-block; padding: 3.5px 0; color: #249DD0;}
		.payment-steps ul li::before{content: ""; width: 100%; height: 2px; border-bottom: dotted 1px #249DD0; display: block; position: relative; top: 15px;}
		.payment-steps ul li:first-child::before{left: 50%; width: 49.5%;}
		.payment-steps ul li:last-child::before{width: 49.5%;}
		.text-center{text-align: center;}*/
		</style>
	</head>
	<body>
		<table style="width: 100%;">
			<tr>
				<td>
					<img src="{{public_path('assets/img/logo/logo-black.png')}}" alt="" class="logo" style="width: 100px;">
				</td>
				<td style="text-align: right;">
					<p style="margin-top: 10px;">{{$today}}</p>
				</td>
			</tr>
		</table>
		<div style="border-top: solid 1px #212121; padding-top: 30px; margin-top: 10px;">
			<ul style="margin:0; padding: 0;">
				<?php $i = 1; ?>
				@foreach ( $search['itinerary'] as $key => $leg )
				<li style="list-style: none; float: left; width: 16%; text-align: center;">
					<div style="width: 100%; height: 2px; border-bottom: dotted 1px #249DD0; display: block; left: {{ ($i == 1)?'50%':'25%'  }}; position: relative; top: 16px; display: block;"></div>
					<span class="stepCircle" style="width: 30px; height: 18px; background-color: #ffffff; border: solid 1px #249DD0; border-radius: 50%; margin: 0 auto 10px auto; position: relative; z-index: 1; display: inline-block; padding: 6px 0; color: #249DD0; font-weight: bold;">{{$leg['hotel']['nights']}}</span>
					<p><strong>{{ $leg['city']['name'] }}</strong><br/>{{ $leg['city']['country']['name'] }}</p>
				</li>
				<?php $i++; ?>
				@endforeach
				<div style="clear: both; display: block; height: 0;"></div>
			</ul>
		</div>
		@foreach ( $search['itinerary'] as $key => $leg )
		<?php
		//echo "<pre>"; print_r($leg); exit();
			$last = count($search['itinerary']) - 1;
			$itineraryIndex = $key == $last ? $key : $key + 1;
			$departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
			$arriveTimezone = get_timezone_abbreviation($search['itinerary'][$itineraryIndex]['city']['timezone']['name']);
			?>
		<table style="width: 100%; border: solid 1px #DEDEDE; padding: 0 8px; margin-top: 15px;" cellpadding="0">
			<tbody>
			<tr>
				<td style="width: 75%;">
					<h4 style="font-size: 16px; line-height: 22px; margin: 5px 0 0 0;"><strong>{{ $leg['city']['name'] }}</strong>, {{ $leg['city']['country']['name'] }}</h4>
					<p>{{ date('d F Y', strtotime($leg['city']['date_from']))}}</p>
				</td>
				<td style="width: 25%;">
					<table>
						<tr>
							<td style="text-align: right; border-right: solid 1px #DEDEDE; padding-right: 15px;">
								<table>
									<tr>
										<td>
											<img src="{{ public_path('/assets/images/ic_local_hotel.png') }}" alt="" style="margin-right: 8px;"/>
										</td>
										<td>
											<img src="{{ public_path('/assets/images/ic_local_activity.png') }}" alt="" style="margin-right: 8px;"/>
										</td>
										<td>
											<img src="{{ public_path('/assets/images/ic_directions_bus.png') }}" alt="" style="margin-right: 8px;"/>
										</td>
									</tr>
								</table>
							</td>
							<td style="padding-left: 15px; text-align: center;">
								<h3 style="font-size: 30px; margin:0px;"><strong>{{$leg['hotel']['nights']}}</strong></h3>
								<span style="font-size: 16px;">Nights</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table style="width: 100%; border-top: solid 1px #DEDEDE;" cellpadding="6">
						<tr>
							<td style="width: 150px; vertical-align: top;">
								@if(!empty($leg['city']['image']))
								<img style="width:100%;" src="https://cms.eroam.com/{{$leg['city']['image'][0]['small'] }}" alt="" class="img-responsive">
								@else
								<img style="width:100%;" src="{{ public_path('/assets/images/no-image1.jpg') }}" alt="" class="img-responsive">
								@endif
							</td>
							<td style="vertical-align: top;">
								{!! $leg['city']['description'] !!}
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
		@if(isset($leg['hotel']) && !empty($leg['hotel']))
		<table style="border: solid 1px #DEDEDE; border-collapse: collapse; width: 100%;">
			<tr style="background-color: #D8D8D8;">
				<td colspan="2" style="vertical-align: middle; padding: 5px 10px">
					<span>
						<img src="{{ public_path('/assets/images/ic_local_hotel.png') }}" alt="" style="margin-right: 8px; margin-top: 2px;"/>
					</span>
					<strong style="font-size: 14px;">ACCOMMODATION</strong>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table style="border-collapse: collapse; text-align: left; width: 100%;">
						<thead style="background-color: #F3F3F3;">
							<tr>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Date</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Name</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Location</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Check In</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Check out</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ date('d F Y', strtotime($leg['city']['date_from']))}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ $leg['hotel']['name'] }}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{$leg['hotel']['address1']}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;"> {{date('d M Y', strtotime($leg['hotel']['checkin']))}} </td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{date('d M Y', strtotime($leg['hotel']['checkout']))}}</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		@endif
		@if(isset($leg['activities']) && !empty($leg['activities']))
		<table style="border: solid 1px #DEDEDE; border-collapse: collapse; width: 100%;">
			<tr style="background-color: #D8D8D8;">
				<td style="vertical-align: middle; padding: 5px 10px" colspan="2">
					<span class="m-r-10">
						<img src="{{ public_path('/assets/images/ic_local_activity.png') }}" alt="" style="margin-right: 8px; margin-top: 2px;"/>
					</span>
				
				<strong style="font-size: 14px;">TOURS / ACTIVITIES</strong></td>
			</tr>
			<tr>
				<td colspan="2">
					<table style="border-collapse: collapse; width: 100%; text-align: left;">
						<thead style="background-color: #F3F3F3;">
							<tr>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Date</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Name</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Location</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Duration</th>
							</tr>
						</thead>
						<tbody>
							@foreach($leg['activities'] as $activity)
							<?php
                              if (!isset($activity['duration1'])) {
                                $activity['duration1'] = $activity['duration'];
                              }
                            ?>
							<tr>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{isset($activity['date_selected'])?date('d M Y', strtotime($activity['date_selected'])):''}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{isset($activity['name'])?$activity['name']:''}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ ( isset($activity['location']) && $activity['location'] != '')?$activity['location']:'-' }}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;"> {{(isset($activity['duration1']))?$activity['duration1'].' (approx.)':'Not Provided'}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		@endif
		@if(isset($leg['transport']) && !empty($leg['transport']))
		<table style="border: solid 1px #DEDEDE; border-collapse: collapse; width: 100%;">
			<tr style="background-color: #D8D8D8;">
				<td style="vertical-align: middle; padding: 5px 10px" colspan="2">
					<span class="m-r-10">
						<img src="{{ public_path('/assets/images/ic_directions_bus.png') }}" alt="" style="margin-right: 8px; margin-top: 2px;"/>
					</span>				
				<strong style="font-size: 14px;">TRANSPORT</strong></td>
			</tr>
			<tr>
				<td colspan="2">
					<table style="border-collapse: collapse; width: 100%; text-align: left;">
						<thead style="background-color: #F3F3F3;">
							<tr>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Date</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Name</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Departure Time</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Arrival / Time</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Duration</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$travelDate = explode('<br/>', $leg['transport']['departure_text']);
								$travelDate = explode('@', $travelDate[1]);
								$departure_time = explode('<br/>', $leg['transport']['departure_text']);
								$arrival_time = explode('<br/>', $leg['transport']['arrival_text']);
							?>
							<tr>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ date('d M Y', strtotime($travelDate[0])) }}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ $leg['transport']['transport_name_text'] }}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{$departure_time[1]}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{$arrival_time[1]}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;"> {{ str_replace('+', '', $leg['transport']['duration'])}}<</td>
				
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		@endif
		@endforeach
	</body>
</html>