@extends('layouts.search')
<?php
$str  = json_encode($return_response);
//echo "<pre>";print_r($return_response);
  ?>
  @section('custom-css')
  <style type="text/css">
  #itr {
    padding: 2rem;
  }
  .itr-leg {
    background: #ffffff;
    margin-bottom: 2rem;
  }
  .itr-leg .heading {
    border: 1px solid #B2B2B2;
    border-bottom: 1px solid white;
    position: relative;
    bottom: -1px;
    display: inline-block;
    font-weight: 400;
    margin: 0;
    padding: 2rem;
  }
  .itr-leg .heading i:before {
    font-size: 25px;
    color: #2AA9DF;
  }
  .itr-leg .body {
    border: 1px solid #B2B2B2;
    padding: 2rem;
  }
  .itr-leg .body section {
    margin-bottom: 2rem;
  }
  .itr-leg .body .title {
    font-size: 17px;
    text-transform: uppercase;
  }
  .itr-leg .body .desc {
    margin-left: 8.8rem;
  }
  .itr-leg .body .desc p {
    line-height: 1em;
  }
  .itr-leg .body .desc p i.status-icon {
    margin-left: 1.5rem;
    margin-right: .2rem;
    color: #009688;
    font-weight: 700;
  }
  span.not-available,
  span.not-available i {
    color: #d32f2f !important;
  }
  .itr-leg .body .desc p span.price {
    font-weight: 700;
    font-size: 18px;
    color: #009688;
  }
  .itr-leg .body .title i:before {
    position: relative;
    top: 5px;
    border-radius: 100px;
    padding: 1.5rem;
    color: #ffffff;
    font-size: 25px;
    margin-right: 2.8rem;
  }
  .itr-leg .body .accomodation .title i:before {
    background: #00bcd4;
  }
  .itr-leg .body .activities .title i:before {
    background: #2196f3;
  }
  .itr-leg .body .transport .title i:before {
    background: #f44336;
  }
  .change-btn i {
    font-size: 1.6rem;
    margin-left: 3rem;
    color: #f8f8f8 !important;
    background: #2AA9DF;
    padding: 8px;
    display: inline-block;
    transition: all 100ms ease-in-out 0s;
  }
  .change-btn i:hover {
    text-decoration: none;
    background: #2792BF;
  }
  .old-price {
    text-decoration: line-through;
    color: #ABABAB;
    font-weight: 400;
    margin-left: 1rem;
  }
  #review-summary {
    border: 1px solid #B2B2B2;
  padding: 5px;
  margin-top: 11px;
  }
  #review-summary h3 {
    margin-top: 0;
  }
  #total {
    font-size: 16px;
  }
  .white-bg{min-height: initial;}
</style>
  @stop
  @section('content')
  <div class="tabs-main-container accomodation-tabs-main-container">
    <div class="tabs-content-container">
      <div class="accomodation-wrapper">
        <?php
          if(isset($return_response) && !empty($return_response)){
              $name = $return_response['passenger_info'][0]['passenger_first_name'].' '.$return_response['passenger_info'][0]['passenger_last_name'];
              $email = $return_response['passenger_info'][0]['passenger_email'];
              $contact = $return_response['passenger_info'][0]['passenger_contact_no'];
              //$gender = $return_response['passenger_info'][0]['passenger_gender'];
              $dob = date('d-m-Y',strtotime($return_response['passenger_info'][0]['passenger_dob']));
              //$country = $return_response['passenger_info'][0]['passenger_country_name'];
              
                $startDate = date('j M Y', strtotime($return_response['from_date']));
                $endDate = date('j M Y', strtotime($return_response['to_date']));
        ?>
            <div class="paymentTopbox">
              <div class="row">
                <div class="col-sm-7 m-t-5">
                  <h4><strong>Multi-City Tailormade Auto</strong></h4>
                  <p class="m-t-10">
                    <i class="icon icon-calendar"></i>
                    {{$startDate}} - {{$endDate}}
                  </p>
                </div>
                <div class="col-sm-5">
                  <div class="row">
                    <div class="col-xs-9 price-right border-right">
                      <p></p>
                      <ul class="paymentIcons">
                        <li><a href="#"><i class="icon icon-hotel"></i></a></li>
                        <li><a href="#"><i class="icon icon-activity"></i></a></li>
                        <li><a href="#"><i class="icon icon-bus"></i></a></li>
                      </ul>
                    </div>
                    <div class="col-xs-3 text-center">
                      <h3 class="transport-price"><strong>{{$return_response['total_days']}}</strong></h3>
                      <!--<span class="transport-nights">Days</span>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-t-30 text-center">
              <h4><strong>Booking Complete</strong></h4>
              <p class="m-t-10">Confirmation email to: {{$return_response['billing_info']['passenger_email']}}<br/>Your eRoam booking reference: {{$return_response['invoiceNumber']}}</p>
            </div>
        <?php
          }
        ?>
        <div class="row m-t-40">
          <div class="col-sm-6">
            <a href="#" name="" class="btn btn-primary btn-block m-b-10">PRINT RECEIPT</a>
          </div>
          <div class="col-sm-6">
            <a href="{{ url('/proposed-itinerary-pdf/') }}" target="_blank"  name="" class="btn btn-primary btn-block m-b-10">PRINT / SHARE ITINERARY</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  @stop
  @section( 'custom-js' )
  @stop