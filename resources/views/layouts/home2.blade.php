@include( 'layouts.header' )
<section class="content-wrapper">

	@yield( 'content' )

</section>
@include( 'layouts.footer' )