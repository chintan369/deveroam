<footer>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-4 col-lg-offset-4 col-md-8 col-md-offset-2">
            <div class="row">
              <div class="col-sm-3 col-xs-12 footer-logo">
                <a href="/"><img src="{{ url( 'assets/images/footer-logo.png')}}" alt="eroam" class="img-responsive"></a>
              </div>
              <div class="col-sm-9 col-xs-12 sociallinks-container">
                <ul class="social-links">
                   <li><a href="https://www.facebook.com/eroam.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/EroamOfficial" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.linkedin.com/company/7934068?trk=prof-exp-company-name" target="_blank"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="https://au.pinterest.com/EroamOfficial/" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
              <li><a href="https://plus.google.com/115818520227253101465?hl=en" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.youtube.com/channel/UCdPYvmFa1Ivt4DYGv42OxSw" target="_blank"><i class="fa fa-youtube"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="footer-menu">
              <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/terms">Terms &amp; Conditions</a></li>
                <li><a href="/privacy-policy">Privacy Policy</a></li>
                <li><a href="https://eroamsupport.atlassian.net/servicedesk/customer/portal/1/group/2" target="_blank">Feedback</a></li>
                <!--<li><a href="/contact-us">Contact Us</a></li>-->
              </ul>
              <p class="copyright-content">Powered by eRoam © Copyright 2016-2018. All Rights Reserved. Patent pending AU2016902466</p>
            </div>
          </div>
        </div>
      </div>
    </footer>

        <!-- START ALERT -->
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="eroamConfirm" id="eroam-confirm">
          <div class="modal-dialog eroam-confirm-modal-size" role="document" >
            <div class="modal-content">
              <div class="modal-header eroam-confirm-header">
                <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title eroam-confirm-title" id="gridSystemModalLabel"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12 eroam-confirm-content-close" style="display:none;">
                    <p>
                      <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 eroam-confirm-content">

                  </div>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-primary confirm-ok-button">OK</button>
                  <button type="button" class="btn btn-primary confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<!-- START ALERT -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="eroamConfirm" id="eroam-confirm">
  <div class="modal-dialog eroam-confirm-modal-size" role="document" >
    <div class="modal-content">
      <div class="modal-header eroam-confirm-header">
        <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title eroam-confirm-title" id="gridSystemModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 eroam-confirm-content-close" style="display:none; margin-left: 10px;">
            <p>
              <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 eroam-confirm-content">

          </div>
        </div>
      </div>
      <div class="modal-footer">

        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 text-center">
            <button type="button" class="btn btn-primary confirm-ok-button">OK</button>
            <button type="button" class="btn btn-primary confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
          </div>
          <div class="col-md-2"></div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- START ALERT -->
<div class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="sessionExpire" id="session-expire">
  <div class="modal-dialog modal-sm" role="document" >
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
        <h4 class="modal-title" id="gridSystemModalLabel">Session Notification</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="">
            Your eRoam session is about to time-out. Would you like to continue?
          </div>
        </div>
      </div>
      <div class="modal-footer" style="background: none;">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 text-center">
            <!-- <button type="button" class="btn btn-primary session-home-btn">Home</button>
            <button type="button" class="btn btn-primary session-continue-btn"> Continue<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button> -->
            <button type="button" class="btn btn-primary session-home-btn">Yes</button>
            <button type="button" class="btn btn-primary session-continue-btn"> No</button>
          </div>
          <div class="col-md-2"></div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- login modal start -->
<div class="modal fade newModal-container" id="loginModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @if(session()->has('register_confirm'))
            <p class="success-box m-t-30">
                {{ session()->get('register_confirm') }}
            </p>
        @elseif(session()->has('reset-success'))
            <p class="success-box m-t-30">
                {{ session()->get('reset-success') }}
            </p> 
        @elseif(session()->has('reset-success-done'))
            <p class="danger-box m-t-30">
                {{ session()->get('reset-success-done') }}
            </p>     
        @endif
        <form class="form-horizontal" id="login_form">     
            {{ csrf_field() }}       
            <h2>Sign In</h2>
            <p class="text-center">with your social network</p>
            <div class="row m-t-20">
              <div class="col-xs-6">
                <a href="{{ url('redirect/google') }}" class="google-login"><i class="fa fa-google-plus"></i> <span>GOOGLE</span></a>
              </div>
              <div class="col-xs-6">
                <button type="" class="fb-login"  data-toggle="tooltip" title="Not Available in Pilot"><i class="fa fa-facebook"></i> <span>FACEBOOK</span></button>
              </div>
            </div>
            <div class="login-or m-t-30">
              <span>or</span>
            </div>
            <p class="error" id="login-error-message"></p>
            <div class="panel-form-group input-control form-group">
              <label class="label-control">Email Address</label>
              <input type="text" name="username_login" id="username_login" class="form-control" placeholder=""> 
            </div>
            <div class="panel-form-group input-control form-group">
              <label class="label-control">Password</label>
              <input type="password" style="display:none">                
              <input type="password" name="password_login" id="password_login" class="form-control" placeholder=""> 
            </div>
            <div class="form-group">
              <label class="radio-checkbox label_check" for="checkbox-login"><input type="checkbox" id="checkbox-login" value="4">Keep Me Signed In.</label>
            </div>
            <div class="form-group">
              <input type="submit" id="submitLoginForm" class="btn btn-black btn-block" value="SIGN IN">
            </div>
            <div class="row">
              <div class="col-sm-6">
                Not a member? <a href="javascript:void();" id="sign_up_button">Sign Up</a>
              </div>
              <div class="col-sm-6 text-right">
                <a href="javascript:void()" id="forgot_password_button">Recover my password</a>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- login modal end -->

 <!-- Signup Modal -->
<div class="modal fade newModal-container" id="registerModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         @if(session()->has('register_confirm_fail'))
            <p class="danger-box m-t-30">
                {{ session()->get('register_confirm_fail') }}
            </p>
        @else
            <p class="success-box m-t-30 register_message" style="display:none"></p>
        @endif
        <form method="post" class="form-horizontal" id="signup_form_model">
            {{ csrf_field() }}
            <h2>Sign Up</h2>
            <p class="text-center">with your social network</p>
            <div class="row m-t-20">
              <div class="col-xs-6">
                <a href="{{ url('redirect/google') }}" class="google-login"><i class="fa fa-google-plus"></i> <span>GOOGLE</span></a>
              </div>
              <div class="col-xs-6">
                <button type="" class="fb-login"  data-toggle="tooltip" title="Not Available in Pilot"><i class="fa fa-facebook"></i> <span>FACEBOOK</span></button>
              </div>
            </div>
            <div class="login-or m-t-30">
              <span>or</span>
            </div>
            <div class="panel-form-group input-control form-group ">
                <label class="label-control">Email Address</label>
                <input type="text" name="reg_email" class="form-control" id="reg_email" placeholder="">
                <span class="text-danger">
                    <strong id="email-error"></strong>
                </span> 
            </div>
            <div class="panel-form-group input-control form-group">
                <label class="label-control">Password</label>
                <input type="password" style="display:none" id="pwd_r">
                <input type="password" name="reg_pass" id="reg_pass" class="form-control" placeholder=""> 
                
                <span class="text-danger">
                    <strong id="password-error"></strong>
                </span>
            </div>
            <div class="form-group">
              <label class="radio-checkbox label_check" for="checkbox-reg"><input type="checkbox" id="checkbox-reg" value="4">Show Password</label>
            </div>
            <div class="form-group">
              <input type="submit" id="submitSignupForm" class="btn btn-black btn-block" value="SIGN UP">
            </div>
            <div class="row">
              <div class="col-sm-6">
                Already registered? <a href="javascript:void()" id="sign_in_button">Sign in</a>
              </div>
              <div class="col-sm-6 text-right">
                by signing up you accept our <a href="{{url('terms')}}" target="_blank">Terms of service</a>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- signup modal end -->

<!-- forgot password modal start -->
<div class="modal fade newModal-container" id="forgotModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @if(session()->has('forgot_confirm'))
            <p class="success-box m-t-30">
                {{ session()->get('forgot_confirm') }}
            </p>      
        @else
            <p class="success-box m-t-30 forgot_message" style="display:none"></p>
        @endif
        <form class="form-horizontal" id="forgot_form" >     
            {{ csrf_field() }}       
            <h2>Recover Password</h2>
            <p class="text-center">Please enter your email and we'll help you to reset your password.</p>
            <div class="row"></div>
            <p class="error" id="login-error-message"></p>
            <div class="panel-form-group input-control form-group">
              <label class="label-control">Email Address</label>
              <input type="text" name="user_email" id="user_email" class="form-control" placeholder=""> 
            </div>      
            <div class="row">     
               <div class="form-group col-sm-6">
                  <input type="button" data-dismiss="modal" class="btn btn-black btn-block" value="Cancel" onclick="window.location='/'">
                </div>    
                <div class="form-group col-sm-6">
                  <input type="submit" id="submitForgotForm" class="btn btn-black btn-block" value="Continue">
                </div>    
            </div>    
            <div></div>    
          </form>
      </div>
    </div>
  </div>
</div>
<!-- forgot password modal end -->

<!-- Modal for Request demo-->
  <!--   <a href="#" id="onLoadLoginModal" data-toggle="modal" data-target="#loadLoginModal" style="display:none;">LOGIN</a>
    <div class="modal fade newModal-container" id="loadLoginModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-sm" role="document" style="width: 450px; margin: 100px auto;">
        <div class="modal-content">
          <div class="modal-header">

            <div class="" style="padding-bottom: 25px;">
              <h4 class="modal-title" id="myModalLabel">Request A Demo</h4>
            </div>
          </div>
          <div class="modal-body">
            <div class="">
              <form id="auth-form" method="post" class="form-horizontal">
                 {{ csrf_field() }}
                
               
                   <div class="panel-form-group input-control form-group">
                     <label class="label-control">Enter Firstname</label>
                     <input type="text" name="firstname" id="firstname" class="form-control">
                   </div>
                   <div class="panel-form-group input-control form-group">
                     <label class="label-control">Enter Lastname</label>
                     <input type="text" name="lastname" id="lastname" class="form-control">
                   </div>
                   <div class="panel-form-group input-control form-group">
                     <label class="label-control">Enter Email</label>
                     <input type="text" name="emailAddress" id="emailAddress" class="form-control">
                   </div>
                   <div class="panel-form-group input-control form-group">
                     <label class="label-control">Enter Phone Number</label>
                     <input type="text" name="phone" id="phone" class="form-control">
                   </div>
                   <div class="panel-form-group input-control form-group">
                     <label class="label-control">Enter Company Name</label>
                     <input type="text" name="company" id="company" class="form-control">
                   </div>

                 <div class="notification text-center row m-t-25"></div>
                 <div class="input-field">
                    <span class="error-message"></span>
                 </div>

                    <div class="input-field">
                    <button type="submit" name="" class="btn btn-primary btn-block" id="authenticate-btn">Request a Demo</button>
                 </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div> -->

    <!-- Modal for Autentication-->
    <a href="#" id="onLoadLoginModal" data-toggle="modal" data-target="#loadLoginModal" style="display:none;">LOGIN</a>
    <div class="modal fade" id="loadLoginModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-sm" role="document" style="width: 450px; margin: 100px auto;">
        <div class="modal-content">
          <div class="modal-header">

            <div class="" style="padding-bottom: 25px;">
              <h4 class="modal-title" id="myModalLabel">Authentication</h4>
            </div>
          </div>
          <div class="modal-body">
            <div class="">
              <form id="auth-form" method="post" class="form-horizontal">
                 {{ csrf_field() }}
                <div class="input-field">
                  <input type="text" name="username" id="username" >
                  <label for="username">Enter Username</label>
                </div>

                <div class="input-field">
                  <input type="password" name="password" id="password">
                  <label for="password">Enter Password</label>

                </div>


                 <div class="notification text-center row m-t-25"></div>
                 <div class="input-field">
                    <span class="error-message"></span>
                 </div>

                    <div class="input-field">
                    <button type="submit" name="" class="btn btn-primary btn-block" id="authenticate-btn">LOGIN</button>
                 </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
     <!-- Modal Closed for Authentication-->
<!--City  Modal -->
    <div class="modal fade" id="cityModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content modal-city">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Select Departure City</h4>
          </div>
          <div class="modal-body">
            <p class="m-t-10">Select your departure city to see the tour flight inclusive prices. </p>
            <form class="form-horizontal m-t-20">
              <div class="panel-form-group">
               <label class="label-control">Select Departure Location</label>
               <select class="form-control" name="default_selected_city" id="default_selected_city">
                 <?php $default_selected_city = session()->get( 'default_selected_city' );?>
                 <option value="30" <?php if($default_selected_city == '30'){ echo 'selected';}?>>Melbourne, AU</option>
                 <option value="7" <?php if($default_selected_city == '7'){ echo 'selected';}?>>Sydney, AU</option>
                 <option value="15" <?php if($default_selected_city == '15'){ echo 'selected';}?>>Brisbane, AU</option>
               </select>
              </div>
            </form>
            <div class="m-t-40 text-right">
              <a href="javascript://" id="decline_city" data-dismiss="modal" class="m-r-10 modal-link">DECLINE</a>
              <a href="javascript://" class="modal-link" id="accept_city">ACCEPT</a>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- Close City Modal -->  


	<input type="hidden" id="site-url" value={{ url('/') }}>
	<input type="hidden" id="api-url" value={{ config( 'env.API_URL' ) }}>
	<input type="hidden" id="cms-url" value={{ config( 'env.CMS_URL' ) }}>
	<input type="hidden" id="search-session" value="{{ json_encode( session()->get( 'search' ) ) }}">
	<input type="hidden" id="search-input" value="{{ json_encode( session()->get( 'search_input' ) ) }}">
	<input type="hidden" id="itinerary-page" value="{{ request()->segment(2) }}">
	<input type="hidden" id="itinerary-leg" value="{{ request()->input('leg') }}">
	<input type="hidden" id="currency-layer" value='{{ json_encode(session()->get('currency_layer')) }}'>
	<input type="hidden" id="all-currencies" value='{{ json_encode(session()->get('all_currencies')) }}'>
	<input type="hidden" id="apis" value="{{ Cache::get('apis') }}">

@if(session()->has('register_confirm') || session()->has('reset-success') || session()->has('reset-success-done'))
<script type="text/javascript">
  $(document).ready(function(){        
      $("#loginModal").modal('show');
  });
</script>
@endif
@if(session()->has('register_confirm_fail'))
<script type="text/javascript">
  $(document).ready(function(){
      $('#pwd_r').attr('type', 'password');
      $("#registerModal").modal('show');
  });
</script>
@endif
    <script src="{{ url( 'assets/js/theme/jquery.min.js' ) }}"></script>

    <script src="{{ url( 'assets/jquery-ui/jquery-ui.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/materialize.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/jquery.validate.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/bootstrap.min.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/jquery.jcarousel.min.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/main.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/bootstrap-datepicker.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/prettify.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.slimscroll.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/tmpl.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.dependClass-0.1.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/draggable-0.1.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.slider.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/pushy.min.js' ) }}"></script>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>

    <script type="text/javascript" src="{{ url('assets/js/markerlabel.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/infobox.js') }}"></script>

  <script src="{{ url( 'assets/js/moment.js' ) }}"></script>
  <script src="{{ url( 'assets/js/moment-timezone.js' ) }}"></script>
  <script src="{{ url( 'assets/js/jstz.min.js' ) }}"></script> {{-- library to determine user timezone --}}

  <script src="{{ url( 'assets/js/slick.min.js' ) }}"></script>
  <script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>

 
  <script type="text/javascript">
    $(".register_message").hide();
    var globalCurrency = "{{ ( session()->has('currency') ) ? session()->get('currency') : 'AUD' }}";
    var globalCurrency_id = "{{ ( session()->has('currency_id') ) ? session()->get('currency_id') : 1 }}"
    var listOfCurrencies = JSON.parse($('#currency-layer').val());
    @if( session()->has('search_input') )
    <?php $input = session()->get( 'search_input' ); ?>
    var travel_pref = [{{ isset( $input['interests'] ) ? join(', ',  $input['interests']) : '' }}];
    @else
    var travel_pref = [];
    @endif
  </script>

  <script src="{{ url( 'assets/js/eroam.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-aot.js' ) }}"></script>
  <script src="{{ url( 'assets/js/booking-summary.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-mystifly.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-hb.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-ae.js' ) }}"></script>
  <script src="http://malsup.github.io/jquery.blockUI.js"></script>
  <script>
        
    $('#sign_in_button').click(function(){
        $('#registerModal').modal('hide');
        $('#forgotModal').modal('hide');
        $("#loginModal").modal('show');        
    });

    $("#sign_up_button").click(function(){ 
        $("#loginModal").modal('hide');
        $('#forgotModal').modal('hide');
        $('#registerModal').modal('show');        
    });

    $("#forgot_password_button").click(function(){
        $("#loginModal").modal('hide');
        $('#registerModal').modal('hide');
        $('#forgotModal').modal('show');
    });
    /* $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);*/
    $( document ).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $(".loginModal").click(function(){ 
            $('#registerModal').modal('hide');
        });
        
        $(".registerModal").click(function(){ 
            $('#loginModal').modal('hide');
        });
     <?php 
        $currentPath= Route::getFacadeRoot()->current()->uri();
        $showCityModal = session()->get( 'showCityModal' );
        /* if($currentPath == '/' && $showCityModal == 1){
          ?>
           // $('#cityModal').modal('show');
          <?php
        }*/
    ?>

    $('#account-dropdown').dropdown();

    getAllCurrencies();

    $('#accept_city').click(function(){
       var siteUrl = $('#site-url').val();
       $('.loader').show();
      eroam.ajax('post', 'session/city', { default_selected_city:$('#default_selected_city option:selected').val(), showCityModal:2 }, function(response){
         window.location.reload();
        });
    });
    $('#decline_city').click(function(){
       var siteUrl = $('#site-url').val();
       $('.loader').show();
        eroam.ajax('post', 'session/city', { default_selected_city:30, showCityModal:2 }, function(response){
         window.location.reload();
        });
    });
    $('.city_drop_down').click(function(){
      $('.loader').show();
       var siteUrl = $('#site-url').val();
       eroam.ajax('post', 'session/city', { default_selected_city:$(this).attr('data-id'), showCityModal:2 }, function(response){
         window.location.reload();
        });
    });
    $('#cityModal').on('hidden.bs.modal', function () {
      $('.loader').show();
        var siteUrl = $('#site-url').val();
        eroam.ajax('post', 'session/city', { default_selected_city:30, showCityModal:2 }, function(response){
         window.location.reload();
        });
    });
    
    /*document.onload = function() {
        inactivityTime();
    };

    document.onmousemove = function() {
        inactivityTime();

    };
    document.onmousedown = function() {
        inactivityTime();
    };
    document.onkeypress = function() {
        inactivityTime();
    };
    document.ontouchstart = function() {
        inactivityTime();
    };

    var inactivityTime = function() {
      if( '{{ isset($page_will_expire) ? 1 : 0 }}' == 1 )
      {
        var t;
        var timer
        document.onload = resetTimer;
        window.onload = resetTimer;
        document.onmousemove = resetTimer;
        document.onkeypress = resetTimer;

        function showInactiveNotification() {
          if(! ($("#session-expire").data('bs.modal') || {isShown: false}).isShown ){
            var counter = 60;
            
            //$('.session-home-btn').html('Home('+counter+')');
            $('#session-expire').modal('show');
            timer = setInterval(function(){
              //$('.session-home-btn').html('Home('+counter+')');
              // if( counter == 0 ){
              //   //$('.session-home-btn').html('Home');
              //   $('.session-home-btn').prop('disabled', true);
              //   $('.session-continue-btn').prop('disabled', true);
              //   clearInterval(timer);
              //   window.location = '{{ url("") }}';
              // }
              counter--;
            }, 1000);
          }
        }

        function resetTimer() {
          clearTimeout(t);
          clearInterval(timer);
          t = setTimeout(showInactiveNotification, 600000);
        }

        $('.session-home-btn').click(function(){
          window.location = '{{ url("") }}';
        });

        $('.session-continue-btn').click(function(){
          clearTimeout(t);
          clearInterval(timer);
          $('#session-expire').modal('hide');
        });
        $('.session-continue-btn').click(function(){
          window.location = '{{ url("app/login") }}';
          //clearTimeout(t);
          // clearInterval(timer);
          // $('#session-expire').modal('hide');
        });
      }
    };*/

    $('body').on('change', '.selected-currency', function() {
      var currency      = $(this).val();
      var id            = $(this).find(':selected').attr('data-value');
      var searchSession = $('#search-session').val();



      eroam.ajax('post', 'session/currency', { currency:currency, id:id }, function(response){
        eroam.ajax( 'post', 'update-booking-summary', { search: searchSession }, function( response ) {
          window.location.reload();
        }, function() {
          $( '.booking-summary' ).html( '<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>' );
        } );
      });
    });

    $('body').on('click', '.itinerary-leg-collapse', function(){
      var processing = $(this).attr('data-processing');

      if( processing == 10 ){
        var key = $(this).attr('data-key');
        //window.itinerayGlobalLeg = key;

        $(this).attr('data-processing', 1);
        eroam.ajax( 'get', 'latest-search', { }, function( response ){
          window.searchSession = JSON.parse(response);
          setDefault(key);

        });
      }

    });

  });

  // getCurrencyLayer();
  function getAllCurrencies() // UPDATED BY RGR
  {
    var allCurrencies = JSON.parse($('#all-currencies').val());
    var html = '';
    $.each(allCurrencies, function(key, value) {
      html += '<option '+( (globalCurrency == value.code) ? 'selected' : '' )+' value="'+value.code+'" data-value="'+value.id+'">('+value.code+') '+value.name+'</option>';
    });

    $('.selected-currency').html( html );
  }



  function getCurrencyLayer()
  {
    eroam.apiWithError('get', 'get_currencies', {}, function(response){
      listOfCurrencies = response;
      eroam.ajax('post', 'session/currency-layer', { currency_layer : response }, function(response){ console.log('Stored CurrencyLayer API data.'); });
    }, null, null, function(){
      getCurrencyLayer();
    });
  }

  function dateFormat(date, separator='-') {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join( separator );
  }
  function setDefault(legKey){
    var tasks = [
      setHotel,
      setActivity,
      setTransport,
      updateAll
        ];
        var key = legKey;

        $.each(tasks, function(index, value) {
            $(document).queue('tasks', processTask(key, value));
        });

        $(document).queue('tasks');
        $(document).dequeue('tasks');
  }
  function processTask(key, fn){
      return function(next){
          doTask(fn, next, key);
      }
  }

  function doTask(fn, next, key){
      fn(key, next);
  }
  function setHotel(key, next){
    var response = [];
    var leg = searchSession.itinerary[key];
    if( leg.hotel == null ){
      showLoader(key, 'hotel');
      var iatas = splitString(leg.city.airport_codes, ',');
      if( iatas.length > 0 ) {

        iatas.forEach(function(iata, iataKey){

          hb_data = {
            city_ids: leg.city.id,
            check_in: leg.city.date_from,
            check_out: leg.city.date_to,
            room:'1',
            adult: searchSession.travellers,
            code: iata,
            child:'0',
            provider: 'hb'
          };

          var hbApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', hb_data, 'hb', true);
          eroam.apiPromiseHandler( hbApiCall, function( hbResponse ){
            response = hbResponse.array;
          });
        });
      }
      else{
        next();
      }

      if(response.length > 0 ){
        var hotelLength = response.length;
        var index = 0;

        while( index < hotelLength ){

          value = response[index];
          var roomValue = value.rooms[0];

          if( roomValue.length > 0 && typeof roomValue.rates[0].net != 'undefined' ){

            roomPrice = parseInt(eroam.convertCurrency(roomValue.rates[0].net, 'AUD'));
            lowest = lowest == 0 ? roomPrice : lowest;
            lowest = roomPrice < lowest ? roomPrice : lowest;

            var hbData = {
              hotel_name: value.name,
              name: value.name,
              price: lowest,
              price_id: value.rateKey,
              currency: 'AUD',
              room_type: roomValue.name,
              rate_key: value.rateKey,
              room_id: roomValue.id,
              hotel_id: value.code,
              id: value.code,
              provider: 'hb',
              location: value.address,
              description: value.description,
              images: value.images,
              cancellation_policy: ''
            };
            index = hotelLength;
          }
          index ++;
        }
        next();
      }else{
        next();
      }
    }else{
      next();
    }

  }
  function setActivity(key, next){

    var leg = searchSession.itinerary[key];

    if( leg.activities == null || leg.activities.length == 0 ){
      showLoader(key, 'activities');
      var destinationReq = {
        destinationName: leg.city.name,
        country: leg.city.country.name
      };
      //console.log('test', destinationReq);

      var destinationApiCall = eroam.apiDeferred('service/location', 'POST', destinationReq, 'destination', true);
      eroam.apiPromiseHandler( destinationApiCall, function( destinationResponse ){

        if( destinationResponse != null ){

          if( Object.keys(destinationResponse).length > 0){
            vRQ = {
              destId: destinationResponse.destinationId,
              startDate: dateFormat(leg.city.date_from),
              endDate: dateFormat(leg.city.date_to),
              currencyCode: 'AUD',
              sortOrder: 'TOP_SELLERS',
              topX: '1-15',
              provider:'viator'
            };

            var viatorApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', vRQ, 'viator_activity', true);
            eroam.apiPromiseHandler( viatorApiCall, function( viatorResponse ){

              if( viatorResponse != null )
              {
                if( viatorResponse.length > 0 )
                {
                  var viatorAct = viatorResponse[0];

                  data ={
                    'date_from':formatDate(leg.city.date_from),
                    'date_to':formatDate(leg.city.date_to),
                    'id':viatorAct.code,
                    'city_id':leg.city.id,
                    'name': viatorAct.title,
                    'city':{
                      'id':leg.city.id
                    },
                    'activity_price':[{
                      'price':viatorAct.price,
                      'currency':{'id':1,'code':'AUD'}
                    }],
                    'price':[{
                      'price':viatorAct.price,
                      'currency':{'id':1,'code':'AUD'}
                    }],
                    'currency_id':1,
                    'currency': 'AUD',
                    'provider': 'viator',
                    'description': viatorAct.shortDescription,
                    /*to be updated static duration*/
                    'duration': 1
                  };

                  searchSession.itinerary[key].activities = [data];
                  next();
                }else{
                  next();
                }
              }else{
                next();
              }
            });
          }else{
            next();
          }
        }else{
          next();
        }
      });
    }else{
      next();
    }
  }
  function setTransport(key, next){
    var leg = searchSession.itinerary[key];
    key = parseInt(key);
    if( key != (searchSession.itinerary.length - 1) ){
      if( leg.transport == null || !leg.transport ){
        showLoader(key, 'transport');
        var mystiflyRQ;
        var mystiflyApiCalls            = [];
        var originIatas                 = leg.city.airport_codes;
        var destinationIatas            = searchSession.itinerary[ key + 1 ].city.airport_codes;
        var arrayOfOriginIataCodes      = splitString( originIatas, "," );
        var arrayOfDestinationIataCodes = splitString( destinationIatas, "," );

        if( arrayOfOriginIataCodes.length > 0 && arrayOfDestinationIataCodes.length > 0 ){
          arrayOfOriginIataCodes.forEach(function( originIataCode ){
            arrayOfDestinationIataCodes.forEach(function( destinationIataCode ){
              mystiflyRQ = {
                DepartureDate : leg.city.date_to,
                OriginLocationCode : originIataCode,
                DestinationLocationCode : destinationIataCode,
                CabinPreference : 'Y',
                Code : ['ADT'],
                Quantity : [searchSession.travellers],
                IsRefundable : false,
                IsResidentFare : false,
                NearByAirports : false,
                provider: 'mystifly'
              };

              try{
                console.log('mystiflyRQ Leg '+key, mystiflyRQ);
                mystiflyApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', mystiflyRQ, 'mystifly', true ) );
              }catch(e){
                console.log('An error has occured while sending the request to the mystifly API for leg '+key, e.message);
              }
            });
          });

          try{
            eroam.apiArrayOfPromisesHandler( mystiflyApiCalls, function( mystiflyArrayOfResponses ){
              mystiflyArrayOfResponses.forEach( function( mystiflyResponse ){

                if( mystiflyResponse != null ){
                  try{
                    var transport;
                    if( Array.isArray( mystiflyResponse.PricedItineraries.PricedItinerary ) ){
                      if( mystiflyResponse.PricedItineraries.PricedItinerary.length > 0 ){
                        transport = mystiflyResponse.PricedItineraries.PricedItinerary[0];
                      }
                    }else{
                      transport = mystiflyResponse.PricedItineraries.PricedItinerary;
                    }

                    var fromCity           = leg.city.name;
                    var toCity             = searchSession.itinerary[ key + 1 ].city.name;
                    var t                  = transport.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment;
                    var fareSourceCode     = transport.AirItineraryPricingInfo.FareSourceCode;
                    var airlineName        = ( isNotUndefined( t.OperatingAirline.Name ) ) ? t.OperatingAirline.Name : 'N/A';
                    var airlineCode        = ( isNotUndefined( t.OperatingAirline.Code ) ) ? t.OperatingAirline.Code : 'N/A';
                    var etd                = t.DepartureDateTime;
                    var eta                = t.ArrivalDateTime;
                    var transportClass     = (t.CabinClassText).trim() != '' ? t.CabinClassText : 'Cabin Class Not Specified';
                    var departure          = moment( etd, moment.ISO_8601 );
                    var arrival            = moment( eta, moment.ISO_8601 );
                    var difference         = moment.duration( arrival.diff( departure ) );
                    var duration           = difference.asHours();
                    var currency           = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
                    var price              = eroam.convertCurrency( transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount, currency );

                    var transportName      = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+' / Depart - '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure.format('hh:mm A')+'. Arrive - '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+arrival.format('hh:mm A');

                    var bookingSummaryText = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+'<br/><small>Depart: '+t.DepartureData+' '+departure.format('hh:mm A')+'</small><br/><small>Arrive: '+t.ArrivalData+' '+arrival.format('hh:mm A')+'</small>';

                    var data = {
                      airline_code: airlineCode,
                      arrival_data: t.ArrivalData,
                      arrival_location: t.ArrivalAirportLocationCode,
                      booking_summary_text: bookingSummaryText,
                      currency: globalCurrency,
                      departure_data: t.DepartureData,
                      departure_location: t.DepartureAirportLocationCode,
                      duration: duration,
                      eta: eta,
                      etd: etd,
                      fare_source_code: fareSourceCode,
                      fare_type: transport.AirItineraryPricingInfo.FareType,
                      flight_number: t.FlightNumber,
                      from_city_id: leg.city.id,
                      id: fareSourceCode,
                      is_selected: true,
                      operating_airline: airlineName,
                      passenger_type_code: "ADT",
                      passenger_type_quantity: searchSession.travellers,
                      price: {
                        0:{
                          price:price,
                          currency:{
                            code:currency
                          }
                        }
                      },
                      provider: "mystifly",
                      to_city_id: searchSession.itinerary[ key + 1 ].city.id,
                      transport_id: fareSourceCode,
                      transport_type: {
                        id: 1,
                        name: "Flight"
                      },
                      transport_type_id: 1,
                      transport_type_name: "Flight",
                      transporttype: {
                        id: 1,
                        name: "Flight"
                      }
                    };
                    searchSession.itinerary[ key ].transport = data;
                    console.log('mystifly successful call Leg '+key, data);

                  }catch(e){
                    console.log('An error has occured while formatting the response of mystifly API call for leg'+key, e.message);
                  }
                  next();
                }else{
                  next();
                }
              });
            });

          }catch(e){
            console.log('An error has occured while processing the response of mystifly API call for leg' +key, e.message);
          }
          next();
        }else{
          next();
        }
      }else{
        next();
      }
    }else{
      next();
    }
  }

  function updateAll(key, next){
    var position = $('.itinerary-leg-container[data-index="'+key+'"]').position().top;

    searchSession.itinerary[key].city.processed = 1;
    bookingSummary.tempUpdate( JSON.stringify( searchSession ), key, position );
    next();
  }

    function showLoader(key, legType){
        $('.itinerary-'+legType+'-name').eq(key).html(
          '<div class="la-ball-pulse" style="color: #2AA9DF">' +
            '<div></div>' +
            '<div></div>' +
            '<div></div>' +
          '</div>'
        );
        $('.itinerary-'+legType+'-name').eq(key).closest('.itinerary-leg-link').attr('data-loading', 'yes').css('cursor', 'wait');
    }
    
    $.validator.addMethod("verifyEmail",
        function(value, element) {
            var result = false;
            $.ajax({
                type:"POST",
                async: false,
                url: "/email/validate", // script to validate in server side
                data: {"reg_email": value,"_token":"{{ csrf_token() }}"},
                success: function(data) {
                    var final_result = '';
                    if(data.success == 1)
                    {
                        if(data.social == 1)
                        {
                            final_result = false;
                        }
                        else
                        {
                            final_result = true;    
                        } 
                    }
                    else
                    {
                        final_result = false;
                    }
                    result = final_result;
                }
            });
            // return true if username is exist in database
            return result;
            alert("RESULT "+result);
        },
        
    );

    $.validator.addMethod("checkEmail",
        function(value, element) {
            var result = false;
            $.ajax({
                type:"POST",
                async: false,
                url: "/email/validate", // script to validate in server side
                data: {"reg_email": value,"_token":"{{ csrf_token() }}"},
                success: function(data) {
                    var final_result = '';
                    if(data.success == 1)
                    {
                        if(data.social == 1)
                        {
                            final_result = true;
                        }
                        else
                        {
                            final_result = false;    
                        } 
                    }
                    else
                    {
                        final_result = true;
                    }
                    result = final_result;
                }
            });
            // return true if username is exist in database
            return result;
            alert("RESULT "+result);
        },
        
    );

    $.validator.addMethod("pwcheck", function(value) {
        return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&_])[A-Za-z\d$@$!%*#?&_]{8,}$/.test(value)
    }, "Password must contain Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:");


    $("#signup_form_model").validate({
        rules: {           
            reg_email: {
                required: true,
                email:true,
                verifyEmail : true
            },
            reg_pass: {
                required: true,
                minlength: 8,    
                pwcheck : true, 
            }
        },        
        messages: {
            reg_email: {
                verifyEmail: "Email already exists"
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            
            var registerForm = $("#signup_form_model");
            var formData = registerForm.serialize();
            //$( '#name-error' ).html( "" );
            $( '#email-error' ).html( "" );
            $( '#password-error' ).html( "" );

            $.ajax({
                url:'/signup',
                type:'POST',
                data:formData,
                beforeSend: function() {
                    $('input#submitSignupForm').val('Please Wait..');
                    $('input#submitSignupForm').prop('type', 'button');
                },
                success:function(data) { 
                    $('input#submitSignupForm').val('Submit');
                    $('input#submitSignupForm').prop('type', 'submit');
                    //$.unblockUI();
                    if(data.errors) {                    
                        if(data.errors.email){
                            $( '#email-error' ).html( data.errors.email );
                        }
                        if(data.errors.password){
                            $( '#password-error' ).html( data.errors.password );
                        }                    
                    }
                    if(data.success) {
                        $(".register_message").show();
                        $("#reg_email").val('');
                        $("#reg_pass").val('');
                        $(".register_message").html('Thank you for Registration. Please confirm your mail from your email Id.');                        
                    }
                },
            });
        }
    });
   
    $("#login_form").validate({
        rules: {           
            username_login: {
                required: true,
                email:true
            },
            password_login: {
                required: true,
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            //$("submitLoginForm").value('Loading..');
            $('input.submitLoginForm').val('Loading');
            var username = $("#username_login").val();
            var password = $("#password_login").val();
            if (username != '' && password != '') {
                eroam.ajax('post', 'login', {username: username, password: password}, function(response) {
                    $('#login-btn').html('Loading...');
                    if (response.trim() == 'valid') {
                        $('#login-error-msg').hide();
                        $('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');                        
                        window.location.href = '/profile/step1';
                    } 
                    else if(response.trim() == 'confirm_first')
                    {
                        $('#login-error-message').text('Please Confirm your mail first.').show();
                        $('#login-btn').html('Log In');
                    }
                    else {
                        $('#login-error-message').text('Invalid Email Address or Password').show();
                        $('#login-btn').html('Log In');
                    }
                }, function() {
                    $('#login-btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
                });
            }
        }
    });

    $("#forgot_form").validate({
        rules: {           
            user_email: {
                required: true,
                email:true,
                checkEmail : true
            },
        },        
        messages: {
            user_email: {
                checkEmail: "We Could Not Find Your Email Address."
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            
            var forgotForm = $("#forgot_form");
            var formData = forgotForm.serialize();
            console.log(formData);

            $.ajax({
                url:'/forgot-password',
                type:'POST',
                data:formData,
                beforeSend: function() {
                    $('input#submitForgotForm').val('Please Wait..');
                    $('input#submitForgotForm').prop('type', 'button');
                },
                success:function(data) { 
                    $('input#submitForgotForm').val('Continue');
                    $('input#submitForgotForm').prop('type', 'submit');
                    //$.unblockUI();
                    if(data.errors) 
                    {                    
                        if(data.errors.email){
                            $( '#email-error' ).html( data.errors.email );
                        }
                        if(data.errors.password){
                            $( '#password-error' ).html( data.errors.password );
                        }                    
                    }
                    if(data.success) {
                        $(".forgot_message").show();
                        $("#user_email").val('');
                        $(".forgot_message").html('Check your mail for recover password link.');
                    }
                },
            });
        }
    });

    $('#checkbox-reg').click(function ()
    {
        if($('#checkbox-reg').is(':checked') == true)
        {
            $("#reg_pass").prop("type", "text");           
        }
        else
        {
            $("#reg_pass").prop("type", "password");            
        } 
    });

    /*$('#checkbox-login').click(function ()
    {
        if($('#checkbox-login').is(':checked') == true)
        {
            $("#password_login").prop("type", "text");           
        }
        else
        {
            $("#password_login").prop("type", "password");            
        } 
    });*/
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
  </script>
	@yield( 'custom-js' )
</body>
</html>