<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>eRoam</title>

	    <link href="{{ url( 'assets/css/bootstrap.min.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/font-awesome.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/materialize.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/datepicker.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/main.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/media.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/prettify.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/jslider.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/jcarousel.connected-carousels.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  		<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
		<script src="{{ url( 'assets/js/theme/jquery.min.js' ) }}"></script>
		<script>
window.domLoadEventFired = true;
	// function checkLoaded() {
 //  		return document.readyState === "complete"
	// }
	if(document.readyState == "complete" || document.readyState == "interactive"){
	}else{
			//-------------------------------------------process before map is loaded
		function itinerary_messages(){
			if($('#map-loader:visible').length == 0){
				
				stop_itenary_message_interval();
				return false;
			}
			if($('#map-loader').find($('#itenary_message_id').length)){
				var itenary_message_id  = $('#itenary_message_id').text();
				if(itenary_message_id == ''){
					$('#itenary_message_id').text('Finding current trending cities...')
				}
				if(itenary_message_id == 'Finding current trending cities...'){
					$('#itenary_message_id').text('Plotting best route...')
				}
				if(itenary_message_id == 'Plotting best route...'){
					$('#itenary_message_id').text('Finding available trending hotels...')
				}
				if(itenary_message_id == 'Finding available trending hotels...'){
					$('#itenary_message_id').text('Finding available trending transport...')
				}
				if(itenary_message_id == 'Finding available trending transport...'){
					$('#itenary_message_id').text('Finding available trending activities...')
				}
				if(itenary_message_id == 'Finding available trending activities...'){
					$('#itenary_message_id').text('The best trending package with be displayed in seconds...')
				}
				if(itenary_message_id == 'The best trending package with be displayed in seconds...'){
					$('#itenary_message_id').text('Finding current trending cities...')
					
				}
			}
		}

		var itenary_message_interval = setInterval(itinerary_messages,5000);
			
		function stop_itenary_message_interval() {
	    	clearInterval(itenary_message_interval);
		}
		//-------------------------------End---------------------------
	}
	
</script>
		<?php /*<script src="{{ url( 'assets/js/jquery-ui.min.js' ) }}"></script>*/ ?>

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	    <![endif]-->
		

		@yield( 'custom-css' )
	</head>

	<body class="has-js">
		<div class="loader" style="display:none;"><img src="{{ url( 'assets/images/loader.gif' ) }}" alt="Loader" /></div>		
    <?php $currentPath= Route::getFacadeRoot()->current()->uri();
    ?>

				<header>
	      <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container-fluid">
	          <div class="row">
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <div class="navbar-header">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                  <span class="sr-only">Toggle navigation</span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="/"><img src="{{ url( 'assets/images/logo.png' ) }}" alt="eroam" class="img-responsive"></a>

	              </div>
	            </div>
	            <div class="col-md-10 col-sm-10 col-xs-12 padding-left-0">
	              <div id="navbar" class="navbar-collapse collapse">
	              
	                <ul class="nav navbar-nav navbar-right">
	                  <li><a data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" href="#"><i class="fa fa-heart-o"></i></a></li>	
	                 

						@if (session()->has('user_auth'))
							<li  class="@if($currentPath == 'profile') active @endif dropdown">
			                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="icon-arrow-bottom header_ic"></span></a>
			                    <ul class="dropdown-menu">
			                      <li><a href="{{ url('profile/step1') }}">Profile</a></li>
			                      <li><a href="{{ url('profile/step2') }}">Addresses</a></li>
			                      <li><a href="{{ url('profile/step3') }}">Personal Preferences</a></li>
			                      <li><a href="{{ url('profile/step4') }}">Contact Preferences</a></li>
			                      <li><a data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot">Manage Trips</a></li>		
			                      <li><a href="{{ url('logout') }}">Sign Out</a></li>
			                    </ul>
			                </li> 
							<?php /* <li @if($currentPath == 'profile')class="active"@endif>
								<a href="#" id="account-dropdown" class="account-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span id="round-profile-picture" style="background-image: url({{ session()->get('user_auth')['image_url'] ? session()->get('user_auth')['image_url'] : url('assets/img/default-profile-image.jpg') }} )"></span> {{ session()->get('user_auth')['name'] }} <i class="fa fa-caret-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="{{ url('profile/step1') }}"><span class="flaticon-profile"></span> Profile</a></li>
									<li><a href="{{ url('logout') }}"><span class="flaticon-logout"></span> Log Out</a></li>
								</ul>
							</li> */?>
						@else
	                        <li @if($currentPath == 'login' || $currentPath == 'register')class="active"@endif><a href="#" data-toggle="modal" data-target="#loginModal">Sign In</a></li>
             		 		<li><a href="#" data-toggle="modal" data-target="#registerModal">Sign Up</a></li>
						@endif
						      <li @if($currentPath == 'about-us')class="active"@endif><a href="https://www.corporate.eroam.com/" target="_blank">Corporate</a></li>
							 <!--  <li @if($currentPath == 'about-us')class="active"@endif><a href="/about-us">About Us</a></li>
	                  		 <li @if($currentPath == 'contact-us')class="active"@endif><a href="https://www.corporate.eroam.com/contact-us/" target="_blank">Contact Us</a></li>-->
							 <li class="dropdown">
							 	<?php 
										$default_selected_city = session()->get( 'default_selected_city' );
										$city_name = 'Melbourne, AU ';
										if($default_selected_city == 7){
											$city_name = 'Sydney, AU ';
										}elseif($default_selected_city == 15){
											$city_name = 'Brisbane, AU ';
										}elseif($default_selected_city == 30){
											$city_name = 'Melbourne, AU ';
										}
								?>
		                   <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $city_name;?><span class="icon-arrow-bottom header_ic"></span></a> -->
		                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Your Location</a>
		                    <ul class="dropdown-menu">
		                       <!-- <li><a class="city_drop_down" href="javascript://" data-id="30">Melbourne, AU</a></li>
			                    <li><a class="city_drop_down" href="javascript://" data-id="7">Sydney, AU</a></li>
			                    <li><a class="city_drop_down" href="javascript://" data-id="15">Brisbane, AU</a></li> -->

			                    <li><a class="city_drop_down" href="javascript://" data-id="30"></a></li>
			                    <!-- <li><a class="city_drop_down" href="javascript://" data-id="7">Sydney, AU</a></li>
			                    <li><a class="city_drop_down" href="javascript://" data-id="15">Brisbane, AU</a></li> -->
		                    </ul>
		                  </li>
		                  <li class="dropdown">
		                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">AUD</a>
		                    <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">AUD <span class="icon-arrow-bottom header_ic"></span></a> -->
		                    
		                      <ul class="dropdown-menu">
			                     <!--  <li><a href="javascript://">AUD</a></li>
			                      <li><a href="javascript://">USD</a></li>
			                      <li><a href="javascript://">CAD</a></li>
			                      <li><a href="javascript://">GBP</a></li>
			                      <li><a href="javascript://">EUR</a></li>
			                      <li><a href="javascript://">NZD</a></li>
			                      <li><a href="javascript://">SGD</a></li>
			                      <li><a href="javascript://">THB</a></li>
			                      <li><a href="javascript://">CNY</a></li>
			                      <li><a href="javascript://">JPY</a></li> -->
			                  </ul>    
		                  </li>
	                </ul>
	              </div>
	            </div>
	         </div>
	       </div>
	      </nav>
	    </header>			