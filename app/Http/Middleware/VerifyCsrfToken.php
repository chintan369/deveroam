<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'set-cache-api-data',
        'get_currencies',
        'get-cities-by-country-id',
        'update-booking-summary',
        'save-booking',
        'map/cache-directions',
        'map/get-directions',
        'map/add-cities-auto-map',
    ];
}
