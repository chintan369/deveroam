<?php

namespace App\Http\Middleware;

use Closure;

class InitialAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if (!session()->has('initial_authentication')) {
        //     return redirect('app/login');
        // }
        return $next($request);
    }
}
