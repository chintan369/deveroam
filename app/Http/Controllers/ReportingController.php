<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;

use App\Http\Requests;
use Carbon\Carbon;
use App\Libraries\ApiCache;
use App\Libraries\EroamSession;
use Cache;
use Guzzle;
use Log;
use DateTime;
use App;
use Config;
use File;
use Excel;

class ReportingController extends Controller
{
	private $cache;

	public function __construct() {
		$this->cache = app('App\Http\Controllers\CacheController');
	}

	public function test(){
		$data = array( [1, 2], [3, 4], [5, 6] );
		Excel::create('testing', function($excel) use ( $data ) {
			$excel->sheet( 'cities' , function($sheet) use ( $data ) {
				$sheet->rows( $data );
			});
		})->export('csv');	
	}


	// created by miguel on 2017-04-09 to generate a list of cities with no data from either HB, AE or Eroam(DB)
	// function will loop through each city and check if there is available cache for the aforementioned API providers, if data is found, then it will return true, else false
	public function citiesWithoutHotelsSheet()
	{
		$data = [];
		$travellers = [ 1, 2, 3 ];
		$regions = json_decode( json_encode( http('GET', 'regions-countries-cities-suburbs') ) );

		foreach( $regions as $region )
		{
			if( count( $region->countries ) > 0 )
			{
				foreach( $region->countries as $country )
				{
					if( !empty( $country->city ) )
					{
						foreach( $country->city as $key => $city )
						{
							$hb_pax    = [ 0, 0, 0 ];
							$aot_pax   = [ 0, 0, 0 ];
							$ae_pax    = [ 0, 0, 0 ];
							$eroam_pax = [ 0, 0, 0 ];
							$no_data   = 0;
							$checkin   = ( new DateTime() )->modify('+10 days')->format('Y-m-d');
							$checkout  = ( new DateTime() )->modify('+11 days')->format('Y-m-d');
							foreach( $travellers as $tkey => $traveller )
							{
								// check if city has HB data by checking cache
								if( !empty( $city->hb_destinations_mapped ) )
								{
									// foreach( $city->hb_destinations_mapped as $dest )
									// {
									// 	$rq = [
									// 		'city_ids'  => $city->id,
									// 		'room'      => 1,
									// 		'adult'     => $traveller,
									// 		'code'      => trim( $dest->hb_destination->destination_code ),
									// 		'child'     => 0,
									// 		'check_in'  => $check_in,
									// 		'check_out' => $check_out
									// 	];
									// 	$rs = $this->cache->set( $rq, 'hb' );
									// 	if( !empty( $rs['hotels'] ) ) {
									// 		if( !empty( $rs['hotels']['hotels'] ) ) { $hb_pax[ $tkey ] = 1; }
									// 	}
									// }
								}
								
								// check if city has AOT data by checking cache
								if( $country->code == 'AU' ){
									// $aot_room_type = NULL;
									// switch( $traveller ){
									// 	case 1:
									// 		$aot_room_type = 'single';
									// 		break;
									// 	case 2:
									// 		$aot_room_type = 'double';
									// 		break;
									// 	case  3:
									// 		$aot_room_type = 'triple';
									// 		break;
									// }
									// $rq = [
									// 	'type'      => 'q_supplier',
									// 	'city_id'   => $city->id,
									// 	'room_type' => $aot_room_type
									// ];
									// $rs = $this->cache->set( $rq, 'aot' );
									// if( count( $rs['data'] ) > 0 ){
									// 	if( count( $rs['data']['suppliers'] ) > 0 ){ $aot_pax[ $tkey ] = 1; }
									// }
								}


								// check if country is UAE
								if( $country->code == 'AE' )
								{
									// check if city has eroam data by making CMS call
									// $rq = [
									// 	'FromDate'  => $checkin,
									// 	'ToDate'    => $checkout, 
									// 	'Adults'    => $traveller,
									// 	'city_name' => $city->name
									// ];
									// $rs = $this->cache->set( $rq, 'ae' );
									// $ae_hotels = $rs['data'];
									// if( count( $ae_hotels ) > 0 ) {
									// 	$ae_index = 0;
									// 	while(  $ae_index < count( $ae_hotels ) ) { 
									// 		if( count( $ae_hotels[ $ae_index ]['rooms'] ) > 0 ) { 
									// 			$ae_pax[ $tkey ] = 1; 
									// 			$ae_index        = count( $ae_hotels ); 
									// 		}
									// 		$ae_index++;
									// 	}
									// }
								}

								// check if city has eroam data by making CMS call
								$rq = [
									'city_ids'          => [ $city->id ],
									'date_from'         => $checkin,
									'date_to'           => $checkout,
									'traveller_number'  => $traveller,
									'hotel_category_id' => NULL,
									'room_type_id'      => NULL
								];
								$rs = http( 'POST', 'city/hotel/v2', $rq );
								if( count( $rs ) > 0 ) {
									foreach ( $rs as $key => $eroam_hotel ) {
										if( count( $eroam_hotel['prices'] ) > 0 ) { $eroam_pax[ $tkey ] = 1; }
									}
								}

							}

							if( !in_array(1, $hb_pax) && !in_array(1, $aot_pax) && !in_array(1, $ae_pax) && !in_array(1, $eroam_pax) ){ 
								$no_data = 1;
							}

							// array_push( $data, [ $region->name, $country->name, $city->id, $city->name, $hb_pax[0], $hb_pax[1], $hb_pax[2], $aot_pax[0], $aot_pax[1], $aot_pax[2], $ae_pax[0], $ae_pax[1], $ae_pax[2], $eroam_pax[0], $eroam_pax[1], $eroam_pax[2], $no_data ] );

						} // end city foreach
					} 
				} //  end country foreach
			}
		} // end region foreach


		Excel::create('cities-without-hotels', function($excel) use ( $result ) {
			$excel->sheet( 'cities' , function($sheet) use ( $result ) {
				$sheet->rows( $data );
				$sheet->row(1, ['REGION NAME', 'COUNTRY NAME', 'CITY ID', 'CITY NAME', 'HAS HB FOR 1 PAX', 'HAS HB FOR 2 PAX', 'HAS HB FOR 3 PAX', 'HAS AOT FOR 1 PAX', 'HAS AOT FOR 2 PAX', 'HAS AOT FOR 3 PAX', 'HAS AE FOR 1 PAX', 'HAS AE FOR 2 PAX', 'HAS AE FOR 3 PAX', 'HAS EROAM FOR 1 PAX', 'HAS EROAM FOR 2 PAX', 'HAS EROAM FOR 3 PAX', 'NO DATA AT ALL']);
			});
		})->export('csv');	

	}

}
