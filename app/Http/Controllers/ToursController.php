<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use StdClass;
use App\Libraries\Filter;
use App\Libraries\EroamSession;
use App\Libraries\ApiCache;
use PDF;
use Mail;
use Cache;
use Validator;
use App\Libraries\Map;
use Carbon\Carbon;
use Config;
use Guzzle;
use Session;


use App\Components\FlashMessages;

//use App\Http\Controllers\ExpediaApiController;

class ToursController extends Controller
{
	private $cities;
	private $countries;
	private $tourCount;
	private $headers = [];

	public function __construct() {

        $this->session = new EroamSession;

		$this->cities = Cache::get( 'cities' );
		$this->countries = Cache::get( 'countries' );
		$this->tourCount = countryTourCount();


        //echo '<pre>'; print_r($this->tourCount); echo '</pre>';
		$this->headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];
	}

	public function search_30_3_18() {
		//$countryName = request()->input('project');
		$countryName = request()->has('project') ?  request()->input('project') : '' ;

		//echo '<pre>'; print_r(request()->input()); die;
		$countryIds[]  	 = request()->input('countryId');
		$countryNames[]	 = request()->input('project');
        $regionIds[] 	 = request()->input('countryRegion');
        $RegionNames[] 	 = request()->input('countryRegionName');

        $tourCountry['countryIds'] = $countryIds;
        $tourCountry['countryNames'] = $countryNames;
        $tourCountry['countryRegions'] = $regionIds;
        $tourCountry['countryRegionNames'] = $RegionNames;

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }

        //session()->forget('tourCountryData');
		if(session()->has('tourCountryData') ){
            $tourCountry = session()->get('tourCountryData');
            $tourCountry = reset( $tourCountry );
            $countryIds = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];
            $regionIds = isset($tourCountry['regionIds']) ? $tourCountry['regionIds'] : [];

            //echo '<pre>'; print_r($tourCountry); echo '</pre>';
        }else {
            $tourCountrydata[0] = $tourCountry; 
            $this->session->set_tourCountry_session($tourCountrydata);
        }
       //echo '<pre>'; print_r($tourCountry); echo '</pre>';
        $tourRegions = http('post', 'getTourCountriesAvailable', [], $this->headers);
		$totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);
        return view(
			'pages.tours',
			[
				'cities'       		=> $this->cities,
                'countries'    		=> $this->countries,
                'tourCount'			=> $this->tourCount,
                'countryName'  		=> $countryName,
				'default_currency'  => 'AUD',
				'regionIds'  		=> $regionIds,
				'countryIds'  		=> $countryIds,
				'tourCountries'		=> $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' =>$totalTourByRegion,
                'page_will_expire'  => 1
			]
		);
	}

    public function search_3_4_18() {
        //$countryName = request()->input('project');
        $countryName = request()->has('project') ?  request()->input('project') : '' ;

        //echo '<pre>'; print_r(request()->input()); die;
        
        $regionIds[]     = request()->input('countryRegion');
        $RegionNames[]   = request()->input('countryRegionName');
        $search_type     = request()->input('searchValType');
        $search_value    = request()->input('searchVal');

        if($search_type == 'city')
        {
            $city_info       = get_city_by_id($search_value);
            $countryIds[]    = $city_info['country_id'];
            $countryNames[]  = $city_info['country_name'];  

        }
        else
        {
            $countryIds[]    = request()->input('countryId');
            $countryNames[]  = request()->input('project');
        }

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['countryRegionNames'] = $RegionNames;

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }
        //session()->forget('tourCountryData');
        if(session()->has('tourCountryData') ){
            $tourCountry    = session()->get('tourCountryData');
            $tourCountry    = reset( $tourCountry );
            $countryIds     = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];
            $regionIds      = isset($tourCountry['regionIds']) ? $tourCountry['regionIds'] : [];

            //echo '<pre>'; print_r($tourCountry); echo '</pre>';
        }else {
            $tourCountrydata[0] = $tourCountry; 
            $this->session->set_tourCountry_session($tourCountrydata);
        }
        /*echo '<pre>'; print_r($tourCountry); echo '</pre>';exit;*/
        $tourRegions = http('post', 'getTourCountriesAvailable', [], $this->headers);
        $totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);
        
        return view(
            'pages.tours',
            [
                'cities'            => $this->cities,
                'countries'         => $this->countries,
                'tourCount'         => $this->tourCount,
                'countryName'       => $countryName,
                'default_currency'  => 'AUD',
                'regionIds'         => $regionIds,
                'countryIds'        => $countryIds,
                'tourCountries'     => $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' =>$totalTourByRegion,
                'page_will_expire'  => 1,
                'search_type'       => $search_type,
                'search_value'      => $search_value,

            ]
        );
    }

    public function search_4_4() {
        
        $countryName = request()->has('project') ?  request()->input('project') : '' ;

        //echo '<pre>'; print_r(request()->input()); die;
        
        $regionIds[]     = request()->input('countryRegion');
        $RegionNames[]   = request()->input('countryRegionName');
        $search_type     = request()->input('searchValType');
        $search_value    = request()->input('searchVal');
        $countryIDRemove = [];
        $tourRegions = http('post', 'getTourCountriesAvailable', [], $this->headers);


        if($search_type == 'city')
        {
            $city_info       = get_city_by_id($search_value);
            $cityIds[]       = $city_info['id'];
            $countryIds[]    = $city_info['country_id'];
            $countryNames[]  = $city_info['country_name']; 
            $countryIDRemove = $countryIds;
            if(empty($city_info['country_id']))
            {
                $countryIds[]    = request()->input('countryId');
            }            
        }
        else if($search_type == 'country')         
        {
            $countryIds[]    = request()->input('countryId');
            $countryNames[]  = request()->input('project');
            foreach ($countryIds as $country_id) {
                $city_info       = get_cities_by_country_id($country_id);
            }
            foreach ($city_info as $city) {
                $cityIds[]       = $city['id'];
            }
        }
        else
        {
            $RegionNames   = [];
            $regionIds     = [];
            $countryNames  = [];            
            $cityIds       = array_unique(request()->input('city'));
            $countryIds    = array_unique(request()->input('region'));
            foreach ($countryIds as $country_id) {
                $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
                $data       = $tourRegions[$search_arr];
                array_push($RegionNames,$data['rname']);
                array_push($regionIds,$data['region_id']);
                array_push($countryNames,$data['name']);                
            }
            $search_type   = 'city';
            $countryIDRemove = explode(",",request()->input('removeCountry')[0]);
        }


        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = $cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }        
        //session()->forget('tourCountryData');
        if(session()->has('tourCountryData') ){
            $tourCountry    = session()->get('tourCountryData');            
            $tourCountry    = reset( $tourCountry );
            $countryIds     = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];                        
            $regionIds      = isset($tourCountry['regionIds']) ? $tourCountry['regionIds'] : [];
        }/*else {
            $tourCountrydata[0] = $tourCountry; 
            $this->session->set_tourCountry_session($tourCountrydata);
        }*/

        $totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);

        $cr = array_unique($countryIDRemove);
        
        //dd($cityIds,$cr,$countryIds,$countryName);
        return view(
            'pages.tours',
            [
                'cities'            => $this->cities,
                'countries'         => $this->countries,
                'tourCount'         => $this->tourCount,
                'countryName'       => $countryName,
                'default_currency'  => 'AUD',
                'regionIds'         => $regionIds,
                'countryIds'        => $countryIds,
                'tourCountries'     => $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' => $totalTourByRegion,
                'page_will_expire'  => 1,
                'search_type'       => $search_type,
                'search_value'      => $search_value,
                'cityIds'           => $cityIds,
                'country_id_remove' => $cr,
            ]
        );
    }

    public function search_17_4_18() {
        
        $countryName = request()->has('project') ?  request()->input('project') : '' ;

        //echo '<pre>'; print_r(request()->all()); die;
        
        $regionIds[]     = request()->input('countryRegion');
        $RegionNames[]   = request()->input('countryRegionName');
        $search_type     = request()->input('searchValType');
        $search_value    = request()->input('searchVal');
        $countryIDRemove = [];
        $tourRegions = http('post', 'getTourCountriesAvailable', [], $this->headers);


        if($search_type == 'city')
        {
            $city_info       = get_city_by_id($search_value);
            $cityIds[]       = $city_info['id'];
            $countryIds[]    = $city_info['country_id'];
            $countryNames[]  = $city_info['country_name']; 
            $countryIDRemove = $countryIds;
            if(empty($city_info['country_id']))
            {
                $countryIds[]    = request()->input('countryId');
            }            
        }
        else if($search_type == 'country')         
        {
            $countryIds[]    = request()->input('countryId');
            $countryNames[]  = request()->input('project');
            foreach ($countryIds as $country_id) {
                $city_info       = get_cities_by_country_id($country_id);
            }
            foreach ($city_info as $city) {
                $cityIds[]       = $city['id'];
            }
        }
        else
        {
            $RegionNames   = [];
            $regionIds     = [];
            $countryNames  = [];            
            $cityIds       = array_unique(request()->input('city')); 
            $countries     = request()->input('region');              
            
            if(empty(request()->input('region')))
            {
                foreach ($cityIds as $city) {
                    $city_info       = get_city_by_id($city);
                    $countryIds[]    = $city_info['country_id'];
                }
                $countryIds    = array_unique($countryIds);
            }
            else
            {
                $countryIds    = array_unique($countries);
            }
            
            foreach ($countryIds as $country_id) {
                $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
                $data       = $tourRegions[$search_arr];
                array_push($RegionNames,$data['rname']);
                array_push($regionIds,$data['region_id']);
                array_push($countryNames,$data['name']);                
            }
            $search_type   = 'city';
            $countryIDRemove = explode(",",request()->input('removeCountry')[0]);

        }

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = $cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;        

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }        
        
        if(!empty(request()->input('from_page')))
        {
            if(session()->has('tourCountryData') ){            
                $tourCountry    = session()->get('tourCountryData'); 
                $tourCountry    = reset( $tourCountry );
                $countryIds     = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];       
                $cityIds        = isset($tourCountry['cityIds']) ? $tourCountry['cityIds'] : [];                   
                $regionIds      = isset($tourCountry['countryRegions']) ? $tourCountry['countryRegions'] : [];
                $countryIDRemove = isset($tourCountry['removeCountry']) ? $tourCountry['removeCountry'] : [];
            }else {
                session()->forget('tourCountryData');
                $tourCountrydata[0] = $tourCountry; 
                $this->session->set_tourCountry_session($tourCountrydata);                
            }
        }
        $totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);

        $cr = array_unique($countryIDRemove);
        //dd($cr);
        //dd($tourCountry,$cr);
        return view(
            'pages.tours',
            [
                'cities'            => $this->cities,
                'countries'         => $this->countries,
                'tourCount'         => $this->tourCount,
                'countryName'       => $countryName,
                'default_currency'  => 'AUD',
                'regionIds'         => $regionIds,
                'countryIds'        => $countryIds,
                'tourCountries'     => $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' => $totalTourByRegion,
                'page_will_expire'  => 1,
                'search_type'       => $search_type,
                'search_value'      => $search_value,
                'cityIds'           => $cityIds,
                'country_id_remove' => $cr,
                'country_id_remove_push'=> implode(",",$cr),
            ]
        );
    }

    public function search_23_4() {
        
        $countryName = request()->has('project') ?  request()->input('project') : '' ;

        //echo '<pre>'; print_r(request()->all()); die;
        
        $regionIds[]     = request()->input('countryRegion');
        $RegionNames[]   = request()->input('countryRegionName');
        $search_type     = request()->input('searchValType');
        $search_value    = request()->input('searchVal');
        $countryIDRemove = [];
        $tourRegions = http('post', 'getTourCountriesAvailable', [], $this->headers);


        if($search_type == 'city')
        {
            $city_info       = get_city_by_id($search_value);
            $cityIds[]       = $city_info['id'];
            $countryIds[]    = $city_info['country_id'];
            $countryNames[]  = $city_info['country_name']; 
            $countryIDRemove = $countryIds;
            if(empty($city_info['country_id']))
            {
                $countryIds[]    = request()->input('countryId');
            }            
        }
        else if($search_type == 'country')         
        {
            $countryIds[]    = request()->input('countryId');
            $countryNames[]  = request()->input('project');
            foreach ($countryIds as $country_id) {
                $city_info       = get_cities_by_country_id($country_id);
            }
            foreach ($city_info as $city) {
                $cityIds[]       = $city['id'];
            }
        }
        else
        {
            $RegionNames   = [];
            $regionIds     = [];
            $countryNames  = [];            
            $cityIds       = array_unique(request()->input('city')); 
            $countries     = request()->input('region');              
            
            if(empty(request()->input('region')))
            {
                foreach ($cityIds as $city) {
                    $city_info       = get_city_by_id($city);
                    $countryIds[]    = $city_info['country_id'];
                }
                $countryIds    = array_unique($countryIds);
            }
            else
            {
                $countryIds    = array_unique($countries);
            }
            
            foreach ($countryIds as $country_id) {
                $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
                $data       = $tourRegions[$search_arr];
                array_push($RegionNames,$data['rname']);
                array_push($regionIds,$data['region_id']);
                array_push($countryNames,$data['name']);                
            }
            $search_type   = 'city';
            $countryIDRemove = explode(",",request()->input('removeCountry')[0]);

        }

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = $cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;        

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }        
        //dd(request()->input('from_page'));
        if(!empty(request()->input('from_page')))
        {
            /*if(session()->has('tourCountryData') ){            
                $tourCountry    = session()->get('tourCountryData'); 
                $tourCountry    = reset( $tourCountry );
                $countryIds     = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];       
                $cityIds        = isset($tourCountry['cityIds']) ? $tourCountry['cityIds'] : [];                   
                $regionIds      = isset($tourCountry['countryRegions']) ? $tourCountry['countryRegions'] : [];
                $countryIDRemove = isset($tourCountry['removeCountry']) ? $tourCountry['removeCountry'] : [];
            }else {*/
                session()->forget('tourCountryData');
                $tourCountrydata[0] = $tourCountry; 
                $this->session->set_tourCountry_session($tourCountrydata);                
            //}
        }
        $totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);

        $cr = array_unique($countryIDRemove);
        //dd($cr);
        //dd($tourCountry,$cr);
        //dd($this->tourCount);
        //$tourCount = array_diff($this->tourCount, array('0'));
        //dd($tourCount);
        return view(
            'pages.tours',
            [
                'cities'            => $this->cities,
                'countries'         => $this->countries,
                'tourCount'         => $this->tourCount,
                'countryName'       => $countryName,
                'default_currency'  => 'AUD',
                'regionIds'         => $regionIds,
                'countryIds'        => $countryIds,
                'tourCountries'     => $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' => $totalTourByRegion,
                'page_will_expire'  => 1,
                'search_type'       => $search_type,
                'search_value'      => $search_value,
                'cityIds'           => $cityIds,
                'country_id_remove' => $cr,
                'country_tour_data' => tourCounts(),
                'country_id_remove_push'=> implode(",",$cr),
            ]
        );
    }

    public function search_30_4_18() {
        
        $countryName = request()->has('project') ?  request()->input('project') : '' ;

        //echo '<pre>'; print_r(request()->all()); die;
        
        $regionIds[]     = request()->input('countryRegion');
        $RegionNames[]   = request()->input('countryRegionName');
        $search_type     = request()->input('searchValType');
        $search_value    = request()->input('searchVal');
        $departure_date  = request()->input('departure_date');
        $countryIDRemove = [];

        $cData = [];
        if(!empty($departure_date) && $departure_date!= '1970-01-01')
        {
            $cData = array(
                'date' => date("Y-m-d", strtotime($departure_date))
                );
        }

        $tourRegions     = http('post', 'getTourCountriesAvailable', $cData, $this->headers);
        
        if($search_type == 'city')
        {
            $city_info       = get_city_by_id($search_value);
            $cityIds[]       = $city_info['id'];
            $countryIds[]    = $city_info['country_id'];
            $countryNames[]  = $city_info['country_name']; 
            $countryIDRemove = $countryIds;
            if(empty($city_info['country_id']))
            {
                $countryIds[]    = request()->input('countryId');
            }            
        }
        else if($search_type == 'country')         
        {
            $countryIds[]    = request()->input('countryId');
            $countryNames[]  = request()->input('project');
            foreach ($countryIds as $country_id) {
                $city_info       = get_cities_by_country_id($country_id);
            }
            foreach ($city_info as $city) {
                $cityIds[]       = $city['id'];
            }
        }
        else
        {
            $RegionNames   = [];
            $regionIds     = [];
            $countryNames  = [];            
            $cityIds       = array_unique(request()->input('city')); 
            $countries     = request()->input('region');              
            
            if(empty(request()->input('region')))
            {
                foreach ($cityIds as $city) {
                    $city_info       = get_city_by_id($city);
                    $countryIds[]    = $city_info['country_id'];
                }
                $countryIds    = array_unique($countryIds);
            }
            else
            {
                $countryIds    = array_unique($countries);
            }
            
            foreach ($countryIds as $country_id) {
                $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
                $data       = $tourRegions[$search_arr];
                array_push($RegionNames,$data['rname']);
                array_push($regionIds,$data['region_id']);
                array_push($countryNames,$data['name']);                
            }
            $search_type   = 'city';
            $countryIDRemove = explode(",",request()->input('removeCountry')[0]);

        }

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = $cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;        

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }        
        //dd(request()->input('from_page'));
        if(!empty(request()->input('from_page')))
        {
            /*if(session()->has('tourCountryData') ){            
                $tourCountry    = session()->get('tourCountryData'); 
                $tourCountry    = reset( $tourCountry );
                $countryIds     = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];       
                $cityIds        = isset($tourCountry['cityIds']) ? $tourCountry['cityIds'] : [];                   
                $regionIds      = isset($tourCountry['countryRegions']) ? $tourCountry['countryRegions'] : [];
                $countryIDRemove = isset($tourCountry['removeCountry']) ? $tourCountry['removeCountry'] : [];
            }else {*/
                session()->forget('tourCountryData');
                $tourCountrydata[0] = $tourCountry; 
                $this->session->set_tourCountry_session($tourCountrydata);                
            //}
        }
        $totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);

        $cr = array_unique($countryIDRemove);
        //dd($cr);
        //dd($tourCountry,$cr);
        //dd($this->tourCount);
        //$tourCount = array_diff($this->tourCount, array('0'));
        //dd($tourCount);
        return view(
            'pages.tours',
            [
                'cities'            => $this->cities,
                'countries'         => $this->countries,
                'tourCount'         => $this->tourCount,
                'countryName'       => $countryName,
                'default_currency'  => 'AUD',
                'regionIds'         => $regionIds,
                'countryIds'        => $countryIds,
                'tourCountries'     => $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' => $totalTourByRegion,
                'page_will_expire'  => 1,
                'search_type'       => $search_type,
                'search_value'      => $search_value,
                'cityIds'           => $cityIds,
                'country_id_remove' => $cr,
                'country_tour_data' => tourCounts(date("Y-m-d", strtotime($departure_date))),
                'dep_date'          => date("Y-m-d", strtotime($departure_date)),
                'search_date'       => $departure_date,
                'country_id_remove_push'=> implode(",",$cr),
            ]
        );
    }

    public function search() {
        
        $countryName = request()->has('project') ?  request()->input('project') : '' ;

        //echo '<pre>'; print_r(request()->all()); die;
        
        $regionIds[]     = request()->input('countryRegion');
        $RegionNames[]   = request()->input('countryRegionName');
        $search_type     = request()->input('searchValType');
        $search_value    = request()->input('searchVal');
        $departure_date  = request()->input('departure_date');
        $tourType        = request()->input('tour_type');
        $countryIDRemove = [];

	
        $cType = count($tourType);
        if($tourType == 'single' || $tourType == 'multi')
        {	
            $tour_type = $tourType;
        }
        else if($cType == 1)
        {
	
            $tour_type = $tourType[0];
        }
        else
        {
	
            $tour_type = "both";
            
        }        

        $tourCounts = countryToursCount(date("Y-m-d", strtotime($departure_date)),$tour_type);
        //dd($tourCounts);
        $cData = array(
            'tour_type' => $tour_type
            );
        if(!empty($departure_date) && $departure_date!= '1970-01-01')
        {
            $cData = array(
                'date' => date("Y-m-d", strtotime($departure_date)) ,
                'tour_type' => $tour_type              
                );
        }        
        
        $tourRegions     = http('post', 'getTourCountriesAvailable', $cData, $this->headers);
       
        if($search_type == 'city')
        {
            $city_info       = get_city_by_id($search_value);
            $cityIds[]       = $city_info['id'];
            $countryIds[]    = $city_info['country_id'];
            $countryNames[]  = $city_info['country_name']; 
            $countryIDRemove = $countryIds;

            if(empty($city_info['country_id']))
            {
                $countryIds[]    = request()->input('countryId');
            }            
        }
        else if($search_type == 'country')         
        {
            $countryIds[]    = request()->input('countryId');
            $countryNames[]  = request()->input('project');
            foreach ($countryIds as $country_id) {
                $city_info       = get_cities_by_country_id($country_id);
            }
            foreach ($city_info as $city) {
                $cityIds[]       = $city['id'];
            }
        }
        else
        {
            $RegionNames   = [];
            $regionIds     = [];
            $countryNames  = [];            
            $cityIds       = array_unique(request()->input('city')); 
            $countries     = request()->input('region');              
            
            if(empty(request()->input('region')))
            {
                foreach ($cityIds as $city) {
                    $city_info       = get_city_by_id($city);
                    $countryIds[]    = $city_info['country_id'];
                }
                $countryIds    = array_unique($countryIds);
            }
            else
            {
                $countryIds    = array_unique($countries);
            }
            
            foreach ($countryIds as $country_id) {
                $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
                $data       = $tourRegions[$search_arr];
                array_push($RegionNames,$data['rname']);
                array_push($regionIds,$data['region_id']);
                array_push($countryNames,$data['name']);                
            }
            $search_type   = 'city';
            $countryIDRemove = explode(",",request()->input('removeCountry')[0]);

        }

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = $cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;        

        if(session()->has('fromTourHome')) {
            session()->forget('fromTourHome');
            session()->forget('tourCountryData');
        }        
        //dd(request()->input('from_page'));
        if(!empty(request()->input('from_page')))
        {
            /*if(session()->has('tourCountryData') ){            
                $tourCountry    = session()->get('tourCountryData'); 
                $tourCountry    = reset( $tourCountry );
                $countryIds     = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];       
                $cityIds        = isset($tourCountry['cityIds']) ? $tourCountry['cityIds'] : [];                   
                $regionIds      = isset($tourCountry['countryRegions']) ? $tourCountry['countryRegions'] : [];
                $countryIDRemove = isset($tourCountry['removeCountry']) ? $tourCountry['removeCountry'] : [];
            }else {*/
                session()->forget('tourCountryData');
                $tourCountrydata[0] = $tourCountry; 
                $this->session->set_tourCountry_session($tourCountrydata);                
            //}
        }
        $totalTourByRegion = http('post', 'tourRegionCount', [], $this->headers);

        $cr = array_unique($countryIDRemove);
        
        //dd($tourCountry,$cr);
        //dd($this->tourCount);
        //$tourCount = array_diff($this->tourCount, array('0'));
        //dd(countryToursCount(date("Y-m-d", strtotime($departure_date)),$tour_type));
        return view(
            'pages.tours',
            [
                'cities'            => $this->cities,
                'countries'         => $this->countries,
                'tourCount'         => $tourCounts,
                'countryName'       => $countryName,
                'default_currency'  => 'AUD',
                'regionIds'         => $regionIds,
                'countryIds'        => $countryIds,
                'tourCountries'     => $tourCountry,
                'tourRegions'       => $tourRegions,
                'totalTourByRegion' => $totalTourByRegion,
                'page_will_expire'  => 1,
                'search_type'       => $search_type,
                'search_value'      => $search_value,
                'cityIds'           => $cityIds,
                'country_id_remove' => $cr,
                'remove_country_id' => implode("#",$cr),
                'country_tour_data' => tourCounts(date("Y-m-d", strtotime($departure_date)),$tour_type),
                'dep_date'          => date("Y-m-d", strtotime($departure_date)),
                'search_date'       => $departure_date,
                'country_id_remove_push'=> implode(",",$cr),
                'tour_type'         => $tour_type,
            ]
        );
    }

    public function set_session_tour()
    {
        $RegionNames   = [];
        $regionIds     = [];
        $countryNames  = [];     
        $cityIds       = array_unique(request()->input('modelcity'));
        $countries     = request()->input('modelRegion');
        $tourRegions = http('post', 'getTourCountriesAvailable', [], $this->headers);

        if(empty(request()->input('modelRegion')))
        {
            foreach ($cityIds as $city) {
                $city_info       = get_city_by_id($city);
                $countryIds[]    = $city_info['country_id'];
            }
            $countryIds    = array_unique($countryIds);
        }
        else
        {
            $countryIds    = array_unique($countries);
        }

        foreach ($countryIds as $country_id) {
            $search_arr = array_search($country_id, array_column($tourRegions, 'id'));
            $data       = $tourRegions[$search_arr];
            array_push($RegionNames,$data['rname']);
            array_push($regionIds,$data['region_id']);
            array_push($countryNames,$data['name']);                
        }
        $countryIDRemove = explode(",",request()->input('removeCountry')[0]);

        $tourCountry['countryIds']      = $countryIds;
        $tourCountry['countryNames']    = $countryNames;
        $tourCountry['countryRegions']  = $regionIds;
        $tourCountry['cityIds']         = $cityIds;        
        $tourCountry['countryRegionNames'] = $RegionNames;             
        $tourCountry['removeCountry']   = $countryIDRemove; 

        session()->forget('tourCountryData');
        $tourCountrydata[0] = $tourCountry; 
        $this->session->set_tourCountry_session($tourCountrydata);
        return \Response::json(['msg' => 'success']);
    }

	public function existsImage(){
		$uri = request()->input('url');
	    $ch = curl_init($uri);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    return $code;
	}

	public function tourDetail(Request $request){

		if (!session()->has('initial_authentication')) {
            return redirect('app/login');
        }
        
		$id       = $request->id;
        $url      = $request->url;
        $default_selected_city = session()->get( 'default_selected_city' );
        $response = http('post', 'tourDetail', ['id' => $id, 'url' => $url,'default_selected_city'=>$default_selected_city], $this->headers);

		$data = $response['data'];
		$data['detail']   	= $response['data']['results'][0];
        $tourCountry['countryIds'] = [];
        $tourCountry['countryNames'] = [];
        $tourCountry['countryRegions'] = [];
        $tourCountry['countryRegionNames'] = [];
        $regionIds = '';

		if( session()->has('tourCountryData') ){
            $tourCountry = session()->get('tourCountryData');
            $tourCountry = reset( $tourCountry );
        }

        $countryIds = isset($tourCountry['countryIds']) ? $tourCountry['countryIds'] : [];
        $regionIds = isset($tourCountry['regionIds']) ? $tourCountry['regionIds'] : [];

		return view(
			'pages.tourDetail',
			[
				'cities'       		=> $this->cities,
                'countries'    		=> $this->countries,
                'tourCount'			=> $this->tourCount,
                'countryName'  		=> '',//$countryName,
				'default_currency'  => 'AUD',
				'regionIds'  		=> $regionIds,
				'countryIds'  		=> $countryIds,
				'tourCountries'		=> $tourCountry,
				'url'				=> $url,
				'data'				=> $data,
                'page_will_expire'  => 1
			]
		);	
	}

	public function payment(){
        if (!session()->has('initial_authentication')) {
            return redirect('app/login');
        }

        require base_path().'/vendor/autoload.php';
        require_once app_path() . '\eway-rapid-php-master\include_eway.php';
        //echo "<pre>"; print_r(request()->input()); //die;
        //generate random Invoice Number
        $digits_needed=8;
        $random_number=''; // set up a blank string
        $count=0;
        while ( $count < $digits_needed ) {
            $random_digit = mt_rand(0, 9);

            $random_number .= $random_digit;
            $count++;
        }
        $invoiceNumber = 'EROAM-'.$random_number;
        //Payment configuration start here
        //SANDBOX Credentials
        $apiKey = 'F9802Cyu/LaPEuVH6DayevjOq0xvO2Ppyf/qGM60sSJGOBiTLz2NZvK+D5ZpVV1eaFCxWY';
        $apiPassword = '9NAW7KKQ';
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;
        //Production Credentials
        //$apiKey = 'A1001Af7PV+/+3/402NLEV7Ftr/0dvVvxKqhScRUc6BcWEt/M=';
        //$apiPassword = 'Ra3UVVdX';
        //$apiEndpoint = \Eway\Rapid\Client::MODE_PRODUCTION;
        //$apiEndpoint = 'https://api.ewaypayments.com/encrypt';
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

        $name = request()->input()['first_name'].' '.request()->input()['last_name'];
        $find = array(",",".");
        $totalAmount = str_replace($find,'',request()->input()['totalAmount']);
        $transaction = [
            'Customer' => [
                'CardDetails' => [
                    'Name' => $name,
                    //'Number' => request()->input()['EWAY_CARDNUMBER'],
                    'Number' => request()->input()['card_number'],
                    'ExpiryMonth' => request()->input()['month'],
                    'ExpiryYear' => request()->input()['year'],
                    //'CVN' => request()->input()['EWAY_CARDCVN'],
                    'CVN' => request()->input()['cvv'],
                ]
            ],
            'Payment' => [
                'TotalAmount' => $totalAmount,
            ],
            'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
        ];
        //print_r($transaction);exit;
        $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
        //echo '<pre>';print_r(request()->input());
        //echo '---';
        //echo '<pre>'; print_r($response);exit;
        if ($response->TransactionStatus === true) {
            
            $data['tour_id'] = request()->input()['tour_id'];
            $data['tour_title']   = '';//request()->input()['Title'];
            $data['Title']   = request()->input()['passenger_title'][1];
            $data['departure_date'] = request()->input()['departure_date'];
            $data['return_date'] = request()->input()['return_date'];
            $data['provider'] = request()->input()['provider'];
            $data['code'] = request()->input()['code'];
            $data['price'] = request()->input()['singleAmount'] + request()->input()['twinAmount'];
            $data['singletotal'] = request()->input()['singleAmount'];
            $data['twintotal'] = request()->input()['twinAmount'];
            $data['sub_total'] = request()->input()['totalAmount'];
            $data['grand_total'] = request()->input()['totalAmount'];
            $data['booking_currency'] = request()->input()['currency'];
            $data['FName']   = request()->input()['passenger_first_name'][1];
            $data['SurName'] = request()->input()['passenger_last_name'][1];
            $data['gender'] = request()->input()['passenger_gender'][1];
            $data['DOB'] = date('Y-m-d H:i:s', strtotime(request()->input()['passenger_dob'][1]));
            $data['contact'] =  request()->input()['passenger_contact_no'];
            $data['email'] = request()->input()['passenger_email'];
            $data['address'] = request()->input()['passenger_country'];
            $data['card_holder_name'] = request()->input()['first_name'].' '.request()->input()['last_name'];
            $data['card_holder_address'] = request()->input()['country'];
            $data['eway_authcode'] = $response->AuthorisationCode;
            $data['eway_transaction_no'] = $response->TransactionID;
            $data['eway_txn_number'] = $response->TransactionID;
            $data['credit_card_fee'] = request()->input()['creditCardFee'];
            $data['postal_code'] = request()->input()['postalcode'];
            $data['request_date'] = date('Y-m-d H:i:s');
            $data['status'] = 0; //Pending
            $data['Agent_id'] = 1;
            $data['payment_method'] = 1;
            $data['BookingType'] = 'Direct into account';
            $data['domain_id'] = 1;
            $data['addon_total_price'] = 0;
            $data['eway_result'] = 'Confirmed';
            $data['domain'] =  $_SERVER['SERVER_NAME'];


            
            $result = http('post', 'tourBooking', [$data], $this->headers);
            //echo '<pre>'; print_r($result); die;
            echo 'Payment successful! ID: '.$response->TransactionID;
            $mail_content = [
                'email' => $data['email'],
                'name' => ucfirst($data['FName']).' '.ucfirst($data['SurName']),
                'tour_title' => request()->input()['Title'],
                'booking_id' => $response->TransactionID,
                'total_pax' => request()->input()['NoOfPass'],
                'single_room_total' => $data['singletotal'],
                'twin_room_total' => $data['twintotal'],
                'total_amount' => $data['grand_total']
                //'twin_qty' => request()->input()['NoOfPass'] - request()->input()['NoOfSingleRoom'],
                //'single_qty' => request()->input()['NoOfSingleRoom']
            ];
            $email = $data['email'];
            
            Mail::send(['html' => 'mail.booking'],$mail_content, function($message) use($email,$invoiceNumber){
                $message->to($email)->subject('Your eRoam Booking '.$invoiceNumber.'');
                $message->from('res@eroam.com');
            });
            return redirect('tourOrderSuccess');
        }else{
            session()->put('error', "Your Payment is declined. Please check your credit card details and try again." );
            session()->put('DateRadioIdValue', request()->input()['DateRadioIdValue']);
            return back();
        }
        exit;

    }
    public function orderSuccess(){

        return view('pages.success');
    }

    public function searchHotels(){   
        $api_key       = "y6de92swh5uw6akrufnaumgm";;//"uqudzqrz92yq6e99vf5mqjcd"; //"y6de92swh5uw6akrufnaumgm";
        $shared_secret = "cshzMEXApR";
        $signature     = hash("sha256", $api_key.$shared_secret.time());
        //$client        = new Client();
        $header        = [ 
            "Api-Key"      => $api_key,
            "X-Signature"  => $signature,
            "Content-Type" => "application/json",
            "Accept"       => "application/json",
            "Accept-Encoding" => "gzip"
            ];
        $url           = 'http://api.test.hotelbeds.com/hotel-api/1.0';
        $url_content   = 'https://api.test.hotelbeds.com/hotel-content-api/1.0/';           

        $body = '{"stay":{"checkIn":"2017-11-15","checkOut":"2017-11-18","shiftDays":3},"occupancies":[{"rooms":"1","adults":"2","children":"0",
        "paxes":[{"type":"AD","age":"30"},{"type":"AD","age":"30"}]}],"filter":{"maxHotels":20,"maxRooms":4,"maxRatesPerRoom":3,"minCategory":1,"maxCategory":5,"paymentType":"BOTH","maxRate":800},
        "destination":{"code":"SYD"}}';
        $url = $url.'/hotels';
        $method = 'post';
 

        $body_array                               = [];
        $body_array['stay']['checkIn']            = "2017-11-15";
        $body_array['stay']['checkOut']           = "2017-11-18";
        $body_array['stay']['shiftDays']          = 3;
        $body_array['occupancies'][0]['rooms']    = "1";
        $body_array['occupancies'][0]['adults']   = "2";
        $body_array['occupancies'][0]['children'] = "0";
        $body_array['occupancies'][0]['paxes'][0]['type'] = "AD";
        $body_array['occupancies'][0]['paxes'][0]['age']  = "30";
        $body_array['occupancies'][0]['paxes'][1]['type'] = "AD";
        $body_array['occupancies'][0]['paxes'][1]['age']  = "30";
        //$body_array['occupancies'][0]['paxes'][0]['type'] = "CH";
        //$body_array['occupancies'][0]['paxes'][0]['age']  = "11";
        $body_array['filter']['maxHotels']       = 20;
        $body_array['filter']['maxRooms']        = 4;
        $body_array['filter']['maxRatesPerRoom'] = 3;
        $body_array['filter']['minCategory']     = 1;
        $body_array['filter']['maxCategory']     = 5;
        $body_array['filter']['paymentType']     = "BOTH";
        $body_array['filter']['maxRate']         = 800;
        $body_array['destination']['code']       = "SYD";
        //$body_array['hotels']['included'] = true;
        //$body_array['hotels']['hotel'][0] = $data['hotel_code'];
        //$body_array['rooms']['included'] = true;
        //$body_array['rooms']['room'][0] = $data['room_code'];
        $body = json_encode($body_array);


        $endpoint = "https://api.test.hotelbeds.com/hotel-api/1.0/hotels";

        
        // Example of call to the API
        try
        {   
            // Get cURL resource
            $curl = curl_init();
            // Set some options 
            curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $endpoint,
                CURLOPT_HTTPHEADER => ['Content-Type:application/json' , 'Accept:application/json' ,'Accept-Encoding:gzip' , 'Api-key:'.$api_key.'', 'X-Signature:'.$signature.'']
            ));

            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Check HTTP status code
            if (!curl_errno($curl)) {
                switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                    case 200:  # OK
                        echo "Server JSON Response:";

                        $resp = json_decode($resp);
                        echo '<pre>'; print_r($resp->hotels->hotels);
                        break;
                    default:
                        echo 'Unexpected HTTP code: ', $http_code, "\n";
                        echo $resp;
                }
            }
            // Close request to clear up some resources
            curl_close($curl);
        } catch (Exception $ex) {
            printf("Error while sending request, reason: %s\n",$ex->getMessage());
        }

        die;

        //$response = Guzzle::$method($url, ['headers' => $header, 'body' => $body] )->getBody();
        //echo "<pre>"; print_r($response);
    }

    public function sendTourEnquiry( Request $request ){

        $firstname= $request->input('firstname');
        $lastname = $request->input('lastname');
        $email    = $request->input('email');
        $phone    = $request->input('phone');
        $tourname = $request->input('tourname');
        $dates    = $request->input('dates');

        /*$validate = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'tourname' => 'required',
            'dates' => 'required',
        ]);

        if ($validate->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }else{*/
            $enquiryContent = [
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'phone' => $phone,
                'tourname' => $tourname,
                'dates' => $dates,
            ];

            Mail::send(['html' => 'mail.TourEnquiry'],$enquiryContent, function($message){
                $message->to('rekha@corigami.com.au')->subject('Contact Us');
                $message->from('andy@corigami.com.au');
            });

            return 1;
           //return Redirect::back()->with('message', 'Message successfully sent.');
        //}
    }
}
