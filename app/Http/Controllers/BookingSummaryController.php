<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BookingSummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $search_session =  session()->get( 'search' ) ;
        $input_session  =  session()->get( 'search_input' );

        $data['number_traveller']           = $input_session['travellers'];
        $data['start_date']                 = $input_session['start_date'];
        $data['total_number_of_days']       = $search_session['total_number_of_days'];
        $data['start_city']                 = $search_session['itinerary'][0]['city']['name'];
        $data['start_city_country_code']    = $search_session['itinerary'][0]['city']['country']['code'];
        $data['itinerary_count']            = count($search_session['itinerary']);
        $data['end_city']                   = $search_session['itinerary'][$data['itinerary_count']-1]['city']['name'];
        $data['end_city_country_code']      = $search_session['itinerary'][$data['itinerary_count']-1]['city']['country']['code'];
        $data['total_per_person']           = $search_session['cost_per_person'];
        $float_total_per_person             = floatval( str_replace( ',', '', $data['total_per_person'] ) );
        $float_sub_total                    = $float_total_per_person * $data['number_traveller'];
        $data['sub_total']                  = number_format( $float_sub_total, 2, '.', ',' );
        $data['credit_card_price']          = round( $float_sub_total * ( 2.5 / 100 ), 2 );
        $data['total_payment']              = number_format( ($float_sub_total + $data['credit_card_price']), 2, '.', ',' );
        $data['currency']                   = $search_session['currency'];
        $data['featured_nationalities']     = json_decode(json_encode(http( 'GET', 'featured/nationalities', NULL)), false); 
        $data['not_featured_nationalities'] = json_decode(json_encode(http( 'GET', 'not-featured/nationalities', NULL)), false);
        $data['user_id']                    = session()->has('user_auth') ? session()->get('user_auth')['id'] : 0 ;

        return view( 'pages.booking-summary', ['data' => $data] );
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // 
    }
}
