<?php

namespace App\Exceptions;

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
//echo "hi";exit;
         if ($e instanceof \ErrorException) {
             abort(506);
         }
         if ($e instanceof TokenMismatchException) { //echo "hi";exit;
             abort(506);
         }
         if ($this->isHttpException($e)) {//echo $e->getStatusCode();exit;
             switch ($e->getStatusCode()) {
                 // not authorized
                 case '403':
                     return redirect()->guest('/');
                     break;

                 // not found
                 case '404':
                     //abort(506);
                     return redirect()->guest('/');
                     break;

                 // internal error
                 case '500':
                     //echo "hi";exit;
                     return redirect()->guest('/');
                     break;

                 default:
                     //abort(404);
                     return $this->renderHttpException($e);
                     break;
             }

         }


        return parent::render($request, $e);
    }
    public function render1($request, Exception $e)
    {

        // custom error message
        if ($e instanceof \ErrorException) {
            return redirect()->guest('/pages');
            // echo "hi";exit;
            //return redirect()->guest('/about-us');
        } else if($e instanceof TokenMismatchException){
            abort(404);
        }else{
            return parent::render($request, $e);
        }

    }
}
