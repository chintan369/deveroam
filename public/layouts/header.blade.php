<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>eRoam</title>

	    <link href="{{ url( 'assets/css/bootstrap.min.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/font-awesome.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/materialize.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/datepicker.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/main.css' ) }}" rel="stylesheet">
	    <link href="{{ url( 'assets/css/media.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/prettify.css' ) }}" rel="stylesheet">
		<link href="{{ url( 'assets/css/jslider.css' ) }}" rel="stylesheet">

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	    <![endif]-->
		

		@yield( 'custom-css' )
	</head>

	<body class="has-js">
<?php $currentPath= Route::getFacadeRoot()->current()->uri();?>
		<header>
	      <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container-fluid">
	          <div class="row">
	            <div class="col-md-2 col-sm-2 col-xs-12">
	              <div class="navbar-header">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	                  <span class="sr-only">Toggle navigation</span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="/"><img src="{{ url( 'assets/images/eRoam_Logo.png' ) }}" alt="eroam" class="img-responsive"></a>
	              </div>
	            </div>
	            <div class="col-md-10 col-sm-10 col-xs-12 padding-left-0">
	              <div id="navbar" class="navbar-collapse collapse">
	                <?php /*<ul class="nav navbar-nav">
	                  <li class="search-text">SEARCH:</li>
	                  <li @if($currentPath == '/' || $currentPath == 'home')class="active"@endif><a href="/">Create Your Own Holiday</a></li>
	                  <li><a href="#">Hotel</a></li>
	                  <li><a href="#">Flight</a></li>
	                  <li><a href="#">Activity</a></li>
	                  <li><a href="#">Car Hire</a></li>
	                </ul>*/ ?>
	                <ul class="nav navbar-nav navbar-right">
	                  <li @if($currentPath == '/')class="active"@endif><a href="/">HOME</a></li>
	                  <li @if($currentPath == 'about-us')class="active"@endif><a href="/about-us">ABOUT US</a></li>
						@if (session()->has('user_auth'))
							<li @if($currentPath == 'profile')class="active"@endif>
								<a href="#" id="account-dropdown" class="account-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span id="round-profile-picture" style="background-image: url({{ session()->get('user_auth')['image_url'] ? session()->get('user_auth')['image_url'] : url('assets/img/default-profile-image.jpg') }} )"></span> {{ session()->get('user_auth')['name'] }} <i class="fa fa-caret-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="{{ url('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
									<li><a href="{{ url('logout') }}"><span class="flaticon-logout"></span> Log Out</a></li>
								</ul>
							</li>
						@else
	                       <li @if($currentPath == 'login' || $currentPath == 'register')class="active"@endif><a href="/login" >LOGIN</a></li>
						@endif

							<li>
								<select class="selected-currency form-control top-selectbox">
                                    <?php $currencies = session()->get('all_currencies'); ?>
									@foreach( $currencies as $currency )
									<option value="{{ $currency['code'] }}">{{ '('. $currency['code'] .') '.$currency['name'] }}</option>
										@endforeach;
								</select>
							</li>



	                </ul>
	              </div>
	            </div>
	         </div>
	       </div>
	      </nav>
	    </header>		