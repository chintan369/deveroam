@include( 'layouts.header' )
	<section class="inner-page content-wrapper">
      	<h1 class="hide"></h1>
      	<div class="map-wrapper">
	      	<div class="mapIcons-wrapper">
					@yield( 'content' )
			</div>
      		<div id="map"></div>
		</div>
	</section>
@include( 'layouts.footer' )