@include( 'layouts.search_header' )
	<section class="inner-page content-wrapper">
      	<h1 class="hide"></h1>
      	<div class="map-wrapper transport-map-wrapper">
        	<!-- <div id="map"></div> -->
        
        	<div class="mapIcons-wrapper">
				{{-- @if ( request()->segment( 1 ) == 'map' )
					<form action="{{ url( 'search' ) }}" method="post">
						<div class="white-box row margin-btm">
							<div class="col-xs-12">
								<div class="row content-header no-padding-btm">
									<div class="option-container col-xs-6">
										<a href="#" data-type="manual" class="search-option-btn padding {{ session()->get( 'search_input' )['option'] == 'manual' ? 'active' : '' }}">Multi-City Manual</a>
										<input class="search-option" type="radio" id="option1" name="option" value="manual" {{ session()->get( 'search_input' )['option'] == 'manual' ? 'checked' : '' }}>
									</div>
									<div class="option-container col-xs-6">
										<a href="#" data-type="auto" class="search-option-btn padding {{ session()->get( 'search_input' )['option'] == 'auto' ? 'active' : '' }}">Multi-City Auto</a>
										<input class="search-option" type="radio" id="option2" name="option" value="auto" {{ session()->get( 'search_input' )['option'] == 'auto' ? 'checked' : '' }}>
									</div>
								</div>
								<div class="row content-header">
									<div class="col-md-2">
										<select id="country" name="country" class="select-country">
											<option value="#">Starting Country</option>
											@foreach ( $countries as $country )
												@if ( count( $country['city'] ) > 0 )
													<option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="col-md-2">
										<select id="starting-point" name="city" class="select-city">
											<option value="#">Starting Point</option>
										</select>
									</div>
									<div class="col-md-2">
										<select id="number-of-travellers" name="travellers">
											<option value="">Travellers</option>
											<option value="1" {{ session()->get( 'search_input' )['travellers'] == "1" ? 'selected' : '' }}>1</option>
											<option value="2" {{ session()->get( 'search_input' )['travellers'] == "2" ? 'selected' : '' }}>2</option>
											<option value="3" {{ session()->get( 'search_input' )['travellers'] == "3" ? 'selected' : '' }}>3</option>
											<option value="4" {{ session()->get( 'search_input' )['travellers'] == "4" ? 'selected' : '' }}>4</option>
											<option value="5" {{ session()->get( 'search_input' )['travellers'] == "5" ? 'selected' : '' }}>5</option>
											<option value="6" {{ session()->get( 'search_input' )['travellers'] == "6" ? 'selected' : '' }}>6</option>
											<option value="7" {{ session()->get( 'search_input' )['travellers'] == "7" ? 'selected' : '' }}>7</option>
											<option value="8" {{ session()->get( 'search_input' )['travellers'] == "8" ? 'selected' : '' }}>8</option>
											<option value="9" {{ session()->get( 'search_input' )['travellers'] == "9" ? 'selected' : '' }}>9</option>
											<option value="10" {{ session()->get( 'search_input' )['travellers'] == "10" ? 'selected' : '' }}>10</option>
										</select>
									</div>
									<div class="col-md-2">
										<input type="text" name="start_date" id="date-of-travel" placeholder="Travel Date" value="{{ session()->get( 'search_input' )['start_date'] }}">
									</div>
									<div class="col-md-4">
										<a href="#" class="update-search-btn padding"><i class="fa fa-search"></i> update</a>
									</div>
								</div>
								<div class="row content-header {{ session()->get( 'search_input' )['option'] == 'manual' ? 'hide' : '' }}" id="auto-container">
									<div class="col-md-2">
										<select id="to-country" name="to_country" class="select-country">
											<option value="#">Destination Country</option>
											@foreach ( $countries as $country )
												@if ( count( $country['city'] ) > 0 )
													<option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
												@endif
											@endforeach
										</select>
									</div>
									<div class="col-md-2">
										<select id="to-city" name="to_city" class="select-city">
											<option value="#">Destination</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit" id="submit-btn" class="hide">
					</form>
				@endif --}}

				<div class="location-wrapper">
					<div class="booking-summary" style="">
						<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>
					</div>
				</div>

				<div class="create-strip">
					<?php 
						$function = Request::segment(1);
					    if($function == "map"){ 
					?>
						    @if ( session()->get( 'map_data' )['type'] == 'auto' )
						      	<p class="map-icon">
							        <a href="#helpBoxDiv" id="edit-map-btn">
								        <svg width="21px" height="24px" viewBox="0 0 21 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								            <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
								            <desc>Created with Sketch.</desc>
								            <defs></defs>
								            <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								                <g id="(Generic)-Icon---Edit-Itinerary" transform="translate(-16.000000, -17.000000)" fill="#FFFFFF">
								                    <g id="Icon---Edit-Itinerary-(eRoam)" transform="translate(16.000000, 17.000000)">
								                        <path d="M16.8519039,12.1743021 L16.8519039,22.3618573 C16.8519039,22.9188258 16.3440134,23.3704013 15.7713079,23.3704013 L1.03704024,23.3704013 C0.464334766,23.3704013 0,22.9188258 0,22.3618573 L0,3.15464079 C0,2.59767236 0.464334766,2.19097699 1.03704024,2.19097699 L11.8155179,2.19097699 C12.3882234,2.19097699 12.8525582,2.64255257 12.8525582,3.199521 C12.8525582,3.75648944 12.3882234,4.20806502 11.8155179,4.20806502 L2.07408048,4.20806502 L2.07408048,21.3533133 L14.7778234,21.3533133 L14.7778234,12.1743021 C14.7778234,11.6173336 15.2421582,11.1657581 15.8148636,11.1657581 C16.3875691,11.1657581 16.8519039,11.6173336 16.8519039,12.1743021 L16.8519039,12.1743021 Z M8.91854604,13.0787139 L7.67306072,13.5592851 L8.17187707,12.3185239 L18.3607974,2.25981011 L19.1295035,3.01747881 L8.91854604,13.0787139 Z M20.7758049,2.48622825 L18.9140584,0.651182407 C18.7678357,0.507212749 18.5684647,0.426024955 18.3602789,0.426024955 L18.3600196,0.426024955 C18.1518338,0.426024955 17.9524628,0.507464885 17.8062401,0.651434543 L6.95153997,11.3674669 C6.87765086,11.4405863 6.8195766,11.5273211 6.78120611,11.6228806 L5.5733135,14.6283418 C5.46053537,14.908717 5.52949855,15.227417 5.74883256,15.4399676 C5.89764783,15.5836851 6.09572252,15.6600823 6.2976861,15.6600823 C6.39413085,15.6600823 6.49135337,15.6426849 6.58442773,15.6068816 L9.64421495,14.4263809 C9.74428933,14.3875519 9.83528962,14.3290564 9.91099355,14.2544241 L20.7758049,3.5489815 C21.0747317,3.25423452 21.0747317,2.7807231 20.7758049,2.48622825 L20.7758049,2.48622825 Z" id="document-edit"></path>
								                    </g>
								                </g>
								            </g>
								        </svg>
								        <span class="map-icon-title" >Edit Map</span>
							        </a>
						      	</p>
							@else
							    <p class="map-icon">
							        <a href="#helpBoxDiv" id="edit-map-btn">
								        <svg width="24px" height="25px" viewBox="0 0 24 25" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
								          <!-- Generator: Sketch 43.2 (39069) - http://www.bohemiancoding.com/sketch -->
								          <desc>Created with Sketch.</desc>
								          <defs></defs>
								          <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								              <g id="-(Generic)-Icon---Assistance" transform="translate(-12.000000, -17.000000)">
								                  <g id="Icon---Assistance-(eRoam)" transform="translate(12.000000, 17.000000)">
								                      <ellipse id="Oval" fill="#FFFFFF" cx="12" cy="12.0855615" rx="12" ry="12.0855615"></ellipse>
								                      <text id="?" font-family="HelveticaNeue-Bold, Helvetica Neue" font-size="18" font-weight="bold" fill="#212121">
								                          <tspan x="7" y="20">?</tspan>
								                      </text>
								                  </g>
								              </g>
								          </g>
								      	</svg>
								        <span class="map-icon-title" id="edit-map-btn">Assistance</span>
							        </a>
							    </p>
							@endif
					<?php } else { ?>
					    <p class="map-icon">
					        <a href="/map">
					            <i class="fa fa-globe"></i>
					            <span class="map-icon-title">VIEW MAP</span>
					        </a>
					    </p>
					<?php } ?>  
				</div>
			</div>

			<div>
	          	<div class="tabs-wrapper padding-right-0" style="padding-top: 48px;">
	            	<?php if($function == "map"){?>
			            <div id="map" style="height: 950px;"></div>
			            <div class="mapWrapper">
			            	@yield( 'content' )
			            </div>
					<?php } else { ?>
			            <div id="map1">
			            	@yield( 'content' )
			            </div>
					<?php } ?>
	          	</div>
	        </div>
		</div>
	</section>
@include( 'layouts.search_footer' )