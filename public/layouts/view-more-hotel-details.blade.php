<style type="text/css">
	.body-container{
		
		margin-top:40px;
		padding:8px 0px 8px 0px;
		
	}
	.border{
		border:1px solid red;
	}

	.small-image img{
	    width: 100%;
	    height: 170px;
	    background-size: contain !important;
	    background-position: center !important;
	    background-repeat: no-repeat !important;
	    background-color: #2B2B2B !important;
	  

	}
	.act-btn{
		color: #fff;
	    text-transform: uppercase;
	    margin: 10px 9px;
	    width: 100%;
	    border-radius: 2px;
	    font-size: 16px;
	    float: left;
	    display: block;
	    background: #2AA9DF;
	    padding: 10px;
	    text-decoration: none;
	    transition: .23s;
	}
	.act-btn-less{
		/*color: #fff;
	    text-transform: uppercase;
	    margin: 12px 9px;
	    width: 100%;
	    border-radius: 2px;
	    font-size: 16px;
	    float: left;
	    display: block;
	    background: #2AA9DF;
	    padding: 10px;
	    text-decoration: none;
	    transition: .23s;*/
	}

	.main-body{
		margin-top: 30px;
		margin-left:5px;
	}
	.tab-button{
		color:black;
		width: 25%;
	    border-radius:0px;
	    border:1px solid;
	    background:transparent;
	    text-transform: uppercase;
	    margin-left:-4px;
	    padding:12px;
	}

	.tab-button.focus{
		background-color: #2AA9DF;
		color:white;
		border-radius:2px;
		border:1px solid #2AA9DF;
		outline:0;
		
	}
	.details{
		padding:15px 2px 5px 2px;
	}
	.tab-button.active {
		background-color: #2AA9DF;
		color:white;
		border:2px solid #2AA9DF;
		outline:0;
	}
	/*.tab-content {
		padding: 1.5rem 0;
		display: none;
	}
	.tab-content.active {
		display: block;
	}*/

	.thumbnail-image{
	    width: 90%;
    	height: 100px;
	    background-size: contain !important;
	    background-position: center !important;
	    background-repeat: no-repeat !important;
	    background-color: transparent !important;

	}
	.thumbnail-image:hover{
		border:3px solid #2AA9DF;
	}
	.responsive{
		margin-top:30px;
		margin-bottom: 20px;
	
	}
	.images{
		margin-right:18px;
		
	}
	.thumb{
		height: 400px;
	}
	.selected{
		border: 3px solid #2AA9DF;
	}

	/* Style the Image Used to Trigger the Modal */
	#myImg {
	    border-radius: 5px;
	    cursor: pointer;
	    transition: 0.3s;
	    
	}

	#myImg:hover {opacity: 0.7;}

	/* The Modal (background) */
	#image-modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 50px; /* Location of the box */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.7); /* Black w/ opacity */
	    border:2px solid white;
	}

	/* Modal Content (Image) */
	.image-modal-content {
	    margin: auto;
	    display: block;
	    max-width: 600px;
	   border:0px;
	}

	/* Caption of Modal Image (Image Text) - Same Width as the Image */
	#caption {
	    margin: auto;
	    display: block;
	    width: 80%;
	    max-width: 700px;
	    text-align: center;
	    color: #ccc;
	    padding: 10px 0;
	    height: 150px;
	}

	/* Add Animation - Zoom in the Modal */
	.image-modal-content, #caption { 
	    -webkit-animation-name: zoom;
	    -webkit-animation-duration: 0.6s;
	    animation-name: zoom;
	    animation-duration: 0.6s;
	}

	@-webkit-keyframes zoom {
	    from {-webkit-transform:scale(0)} 
	    to {-webkit-transform:scale(1)}
	}

	@keyframes zoom {
	    from {transform:scale(0)} 
	    to {transform:scale(1)}
	}

	/* The Close Button */
	.close {
	    position: absolute !important;
	    top: 30px !important;
	    right: 300px !important;
	    color: #f1f1f1 !important;
	    font-size: 40px !important;
	    font-weight: bold !important;
	    transition: 0.3s !important;
	}
	

	.close:hover,
	.close:focus {
	    color: #bbb;
	    text-decoration: none;
	    cursor: pointer;
	}

	/* 100% Image Width on Smaller Screens */
	@media only screen and (max-width: 700px){
	    .image-modal-content {
	       
	        width: 100%;
  			max-width: 600px;
	    }
	}

	.map-show {
	    height: 250px;
	    width: 670px;
	    margin-left: -22px;

	}
	#pac-input{
		visibility: hidden;
		margin-bottom: 1px;
	}

	.carousel-fade .carousel-inner .item {
	  opacity: 0;
	  -webkit-transition-property: opacity;
	  -moz-transition-property: opacity;
	  -o-transition-property: opacity;
	  transition-property: opacity;
	}
	.carousel-fade .carousel-inner .active {
	  opacity: 1;
	}
	.carousel-fade .carousel-inner .active.left,
	.carousel-fade .carousel-inner .active.right {
	  left: 0;
	  opacity: 0;
	  z-index: 1;
	}
	.carousel-fade .carousel-inner .next.left,
	.carousel-fade .carousel-inner .prev.right {
	  opacity: 1;
	}
	.carousel-fade .carousel-control {
	  z-index: 2;
	}

	.close-btn:before {
		position: absolute;
		right: -40px;
		top: -35px;
		font-size: 3rem;
		color: #ececec;
		z-index: 999;
		cursor: pointer;
		transition: .21s ease-ine;
	}
	.close-btn:hover {
		color: #ffffff;
	}
	.flaticon-left-arrow{
		margin-left: -40px !important;

	}
	
	#infowindow-content .place-title {
		font-weight: bold;
	}

	#infowindow-content {
		display: none;
	}

	#googleMap #infowindow-content {
		display: inline;
	}
</style>
<div class="hotel-container" id="hotel1">
			<a href="#" class="hotel-close act-btn-less" data-provider="{{ $provider }}" data-code="{{ $code }}">
                    <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="54---EROA007-V4.1-Generic-(Edit-Hotel)-02" transform="translate(-1240.000000, -185.000000)">
                                <g id="Group-31" transform="translate(491.000000, 168.000000)">
                                    <g id="close" transform="translate(756.000000, 24.000000) scale(1, -1) translate(-756.000000, -24.000000) translate(744.000000, 12.000000)">
                                        <polygon id="Shape" fill="#000000" opacity="0.539999962" points="19 17.6 17.6 19 12 13.4 6.4 19 5 17.6 10.6 12 5 6.4 6.4 5 12 10.6 17.6 5 19 6.4 13.4 12"></polygon>
                                        <polygon id="bounds" points="24 0 0 0 0 24 24 24"></polygon>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg></a>

                   <h4>{{ $data['hotel_name'] }}</h4>
                   <div class="panel-icon">
                     
                      <svg width="14px" height="19px" viewBox="0 0 14 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <desc>Created with Sketch.</desc>
                          <defs>
                              <polygon id="path-1" points="0.00287671233 0.0185829268 0.00287671233 18.999908 13.9797671 18.999908 13.9797671 0.0185829268 0.00287671233 0.0185829268"></polygon>
                          </defs>
                          <g id="EROA013-05-(B2B-/-B2C)-eRoam-Itinerary-Builder" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="53---EROA007-V4.1-Generic-(Edit-Hotel)-01" transform="translate(-500.000000, -402.000000)">
                                  <g id="Group-22" transform="translate(490.000000, 168.000000)">
                                      <g id="Group-44">
                                          <g id="Group-41">
                                              <g id="placeholder" transform="translate(10.000000, 234.000000)">
                                                  <g id="Group">
                                                      <mask id="mask-2" fill="white">
                                                          <use xlink:href="#path-1"></use>
                                                      </mask>
                                                      <g id="Clip-2"></g>
                                                      <path d="M6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.5971537 6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,18.9999073 6.98988356,18.9999073 C7.08428767,18.9999073 7.17437671,18.9613512 7.2374726,18.8935073 C7.51277397,18.5971537 13.9797671,11.5880073 13.9797671,6.77465854 C13.9797671,3.04940732 10.8442466,0.0185829268 6.98988356,0.0185829268 L6.98988356,0.0185829268 Z M6.98988356,10.3136171 C4.96774658,10.3136171 3.32845205,8.7291561 3.32845205,6.77465854 C3.32845205,4.82016098 4.96774658,3.23588537 6.98988356,3.23588537 C9.01202055,3.23588537 10.6513151,4.82016098 10.6513151,6.77465854 C10.6513151,8.72934146 9.01202055,10.3136171 6.98988356,10.3136171 Z M6.86028767,18.8935073 C6.89322603,18.9613512 6.94045205,19.0000927 6.98988356,18.9999073 C6.89547945,18.9999073 6.80539041,18.9613512 6.7424863,18.8935073 C6.46718493,18.5971537 0,11.5880073 0,6.77465854 C0,3.04940732 3.13552055,0.0185829268 6.98988356,0.0185829268 C4.9709589,0.0185829268 2.66282877,3.04940732 2.66282877,6.77465854 C2.66282877,11.5880073 6.71606849,18.597339 6.86028767,18.8935073 Z" id="Combined-Shape" fill="#221F20" mask="url(#mask-2)"></path>
                                                  </g>
                                              </g>
                                          </g>
                                      </g>
                                  </g>
                              </g>
                          </g>
                      </svg>
                   </div>
                   <div class="panel-container">
                     <p>{{$data['address']}}</p>
                   </div>
                  
                   
				    <div class="jcarousel-wrapper">
				        <div class="jcarousel">
				        	<ul>
					        	<?php 

					        	function checkExternalFile($url){
								    $ch = curl_init($url);
								    curl_setopt($ch, CURLOPT_NOBODY, true);
								    curl_exec($ch);
								    $retCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
								    curl_close($ch);

								    return $retCode;
								}
							        $i=0;
							        foreach($data['images'] as $key => $image){
							        	$i++;
							        	$file_exists = checkExternalFile($image);
							        	if($file_exists == 200) {
					        	?>

				                	<li><img src="{{ $image }}" alt="Image {{$i}}"></li>
				                <?php 
				            	} 
				            }?>
				            </ul>
				        </div>

				        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
				        <a href="#" class="jcarousel-control-next">&rsaquo;</a>

       					 <p class="jcarousel-pagination"></p>
    				</div>
                    
                    <div class="m-t-30">
                      <h4>{{ $data['hotel_name'] }}</h4>
                      <div class="row m-t-30">
                        <div class="col-md-5 col-sm-6">
							
                          <div id="googleMap" style="height: 330px;"></div>
                          <input id="pac-input" class="form-control full-width" type="text" readonly data-lat="{{ $data['lat'] != '' ? $data['lat'] : '' }}" data-lng="{{ $data['lng'] != '' ? $data['lng'] : '' }}" value="{{ $data['google_map_location'] != '' ? $data['google_map_location'] : '' }}" placeholder="Search Box">	
                          <div id="infowindow-content">
							<img src="" width="16" height="16" id="place-icon">
							<span id="place-name" class="place-title"></span><br>
							<span id="place-address"></span>
						</div>
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <div class="activityDesc">
                              <p><strong>Description</strong></p>
                              <p>{!! $data['hotel_description'] !!}</p>
                            </div>
                           
                        </div>
                      </div>
                    </div>
			
</div>


<!-- <div class="col-md-12">
	<div class="col-md-12 body-container">
		<div class="col-md-12">
			<div class="col-md-4 small-image">
				<img src="{{ $data['primary_photo'] }}">
			</div>
			<div class="col-md-5">
					<h4 class="blue-txt bold-txt">{{ $data['hotel_name'] }}</h4>
					
					{!! get_hotel_category( $data['star_rating'], $data['provider'] ) !!}
					<p>{{ $data['hotel_name'] }}</p>
				
				<div class="row">
					<div class="col-md-4">
						Destination:
					</div>
					<div class="col-md-8">
						{{ $data['city_name'] }}
					</div>
					<div class="col-md-4">
						Address:
					</div>
					<div class="col-md-8">
						{{ $data['address'] }}
					</div>
					<div class="col-md-4">
						Telephone:
					</div>
					<div class="col-md-8">
						{{ $data['telephone'] }}
					</div>
				</div>
			</div>
			<div class="col-md-3 ">	
				<a href="#." class="btn act-btn-less blue-button bold-txt" data-provider="{{ $provider }}" data-code="{{ $code }}">View Less</a>
			</div>

		</div>
		<div class="col-md-12 main-body">
			<div class="col-md-12">
				<button class="btn bold-txt tab-button active" data-target="images">Images</button>
				<button class="btn bold-txt tab-button" data-target="rooms">Rooms</button>
				<button class="btn bold-txt tab-button" data-target="details">Detail</button>
				<button class="btn bold-txt tab-button" data-target="location">Location</button>
			</div>

			<div class="col-md-12">
				<div class="tab-content active" id="images">
					<div class="col-md-12" style="margin-bottom: 20px;">
						 @forelse ($data['images'] as $key => $image)
							<div class="col-md-3" id="myImg" style="padding-top:10px" >
								<div class="thumbnail-image {{ ($key == 0) ? 'selected':'' }}" style="background-image: url({{ $image }})" data-path="{{ $image }}" data-context="{{ $data['hotel_name'] }}"></div>
							</div>
						@empty
							<h4>No images.</h4>
						 @endforelse
					</div> 
				</div>
				<div class="tab-content" id="rooms">
					<div id="view-more-rooms"></div>
				</div>
				<div class="tab-content" id="details">
					<h3>DESCRIPTION</h3>
					{!! $data['hotel_description'] !!}
					<br/>
					<h3>CANCELLATION POLICY</h3>
					@if ($data['cancel_policy'])
						{!! $data['cancel_policy'] !!}
					@endif
				</div>
				<div class="tab-content" id="location">
					<div class="col-md-12" style="margin-bottom: 20px">
						<div class="col-md-6"></div>
						<div class="col-md-6 full-right">
							<input id="pac-input"  class="form-control full-width" type="text" readonly data-lat="{{ $data['lat'] != '' ? $data['lat'] : '' }}" data-lng="{{ $data['lng'] != '' ? $data['lng'] : '' }}" value="{{ $data['google_map_location'] != '' ? $data['google_map_location'] : '' }}" placeholder="Search Box">	
						</div>
						<div class="map-show" id="googleMap"></div>
						<div id="infowindow-content">
							<img src="" width="16" height="16" id="place-icon">
							<span id="place-name" class="place-title"></span><br>
							<span id="place-address"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->

<!-- <div id="image-modal" class="modal">
  <div class="modal-content image-modal-content">
		<span class="close-btn flaticon-multiply"></span>
	
		<div id="myCarousel" class="carousel carousel-fade" data-ride="carousel" data-interval="false">
		
		  <div class="carousel-inner responsive" role="listbox">
			    @foreach ($data['images'] as $key => $image)
							
				 <div class="item {{($key == 0) ? 'active':'' }}" data-path-top="{{ $image}}">
			       	<div class="thumb" style="background-image: url( {{ $image}} )"></div>
			     </div>  
			    @endforeach
				
		  </div>
		 
		 
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left flaticon-left-arrow" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel " role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right flaticon-right-arrow" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		  
		</div>
					


  </div>
  <div id="caption"></div>
</div> -->

<input type="hidden" id="data-attr" value="{{ $data_attr }}">
<script type="text/javascript" src="{{ url( 'assets/js/theme/jcarousel.responsive.js' ) }}"></script>
<script>
	
	
	$(document).ready(function() {
		// INIT
		var map, marker, infowindow, infowindowContent;
		var dataAttr = JSON.parse($('#data-attr').val());
		buildRoomTypes(true, dataAttr['index'], dataAttr['season'], dataAttr['hotel'], dataAttr['provider'], dataAttr['hotelName'],dataAttr['hotelLocation'],dataAttr['rooms']);

		// UPDATED BY RGR
		$('body').on('click', '.tab-button', function(e) {
			e.preventDefault();
			var target = $(this).data('target');

			// TAB BUTTON
			$('.tab-button').removeClass('active');
			$(this).addClass('active');

			// TAB CONTENT
			// $('.tab-content').removeClass('active');
			// $('.tab-content#'+target).addClass('active');

			// MAP
			if (target == 'location') {
				initMap();
			}
		});

		$('body').on('click', '.thumbnail-image', function(e){
			 $('.thumbnail-image').removeClass("selected");			
			 $(this).addClass('selected');
			 $('.item').removeClass('active');
			 var path = $(this).data('path');
			 var caption = $(this).data('context');
			 $("div").find("[data-path-top='" + path + "']").addClass('active');
		     var modal = document.getElementById('image-modal');

			// Get the image and insert it inside the modal - use its "alt" text as a caption
			var img = document.getElementById('myImg');
			// var modalImg = document.getElementById("img01");
			var captionText = document.getElementById("caption");
		    modal.style.display = "block";
		    // modalImg.src = path;
		    captionText.innerHTML = caption;
			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() { 
			    modal.style.display = "none";
			}
		   
		     // Get the modal
			// var modalImg = document.getElementById("img01");
			var captionText = document.getElementById("caption");
			
		   //  modal.style.display = "block";
		    // modalImg.src = path;
		    captionText.innerHTML = caption+ " images";
	
		});

		$('body').on('click', '.left', function(e){
			 var path = $('div.active').prevAll().data('path-top');
			 $('.thumbnail-image').removeClass('selected');
			 if (path === undefined) {
			    $('.thumbnail-image').last().addClass('selected');	
		     }else{
		     	$("div").find("[data-path='" + path + "']").addClass('selected');
		     } 
		});

		$('body').on('click', '.right', function(e){
			 var path = $('div.active').nextAll().data('path-top');
			 $('.thumbnail-image').removeClass('selected');
			 if (path === undefined) {
			    $('.thumbnail-image').first().addClass('selected');	
		     }else{
		     	$("div").find("[data-path='" + path + "']").addClass('selected');
		     } 
		});
		$('body').on('click','.close-btn',function(e){
			$('.modal').hide();
		});
		init_map();
				
	});

	function init_map() {

		map = new google.maps.Map(document.getElementById('googleMap'), { zoom: 13 });
		// INFO WINDOW
		infowindow = new google.maps.InfoWindow();
		infowindowContent = document.getElementById('infowindow-content');
		infowindow.setContent(infowindowContent);

		// MARKER
		marker = new google.maps.Marker({
			map: map,
			anchorPoint: new google.maps.Point(0, -29)
		});

		var searchBox = $('#pac-input');
		var geocoder = new google.maps.Geocoder;

		if (searchBox.data('lat') != '' && searchBox.data('lng')) { // REVERSE GEOCODING
			var latlng = {lat: searchBox.data('lat'), lng: searchBox.data('lng')};
			geocoder.geocode({'location': latlng}, function(results, status) {
				if (status.toLowerCase() == 'ok') {
					var service = new google.maps.places.PlacesService(map);
					service.getDetails({
						placeId: results[0].place_id
					}, function(place, status) {
						if (status === google.maps.places.PlacesServiceStatus.OK) {
							showPlaceOnMap(place);
						}
					});
				}
			});

		} else {
			geocoder.geocode({
				address: searchBox.val(),
				region: 'no',
			}, function(results, status) {
				if (status.toLowerCase() == 'ok') {
					var service = new google.maps.places.PlacesService(map);
					service.getDetails({
						placeId: results[0].place_id
					}, function(place, status) {
						if (status === google.maps.places.PlacesServiceStatus.OK) {
							showPlaceOnMap(place);
						}
					});
				}
			});
		}
	}

	function showPlaceOnMap(place) {
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);
		}
		marker.setPosition(place.geometry.location);
		marker.setVisible(true);

		var address = '';
		if (place.address_components) {
			address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
			].join(' ');
		}

		infowindowContent.children['place-icon'].src = place.icon;
		infowindowContent.children['place-name'].textContent = place.name;
		infowindowContent.children['place-address'].textContent = address;
		infowindow.open(map, marker);
	}

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}


	
</script>