<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
        <div class="row">
          <div class="col-sm-3 col-xs-4 footer-logo">
            <a href="/"><img src="{{ url( 'assets/images/footer-logo.png')}}" alt="eroam" class="img-responsive"></a>
          </div>
          <div class="col-sm-2 col-xs-4 awards-logo">
            <img src="{{ url( 'assets/images/gold-youth-travel-2016.png')}}" alt="" class="img-responsive">
          </div>
          <div class="col-sm-2 col-xs-4 awards-logo">
            <img src="{{ url( 'assets/images/gold-youth-travel-2017.png')}}" alt="" class="img-responsive">
          </div>
          <div class="col-sm-5 col-xs-12 text-right">
            <ul class="social-links">
              <li><a href="https://www.facebook.com/eroam.official/" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/EroamOfficial" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.linkedin.com/company/7934068?trk=prof-exp-company-name" target="_blank"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="https://au.pinterest.com/EroamOfficial/" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
              <li><a href="https://plus.google.com/115818520227253101465?hl=en" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.youtube.com/channel/UCdPYvmFa1Ivt4DYGv42OxSw" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="footer-menu">
          <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/about-us">About</a></li>
            <li><a href="/affilliates">Affiliates</a></li>
            <li><a href="/sitemap">Sitemap</a></li>
            <li><a href="/terms">Terms &amp; Conditions</a></li>
            <li><a href="/privacy-policy">Privacy Policy</a></li>
            <li><a href="/contact-us">Contact Us</a></li>
          </ul>
          <p class="copyright-content">
            Powered by eRoam © Copyright 2016. All Rights Reserved. Patent pending AU2016902466
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>


        <!-- START ALERT -->
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="eroamConfirm" id="eroam-confirm">
          <div class="modal-dialog eroam-confirm-modal-size" role="document" >
            <div class="modal-content">
              <div class="modal-header eroam-confirm-header">
                <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title eroam-confirm-title" id="gridSystemModalLabel"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12 eroam-confirm-content-close" style="display:none;">
                    <p>
                      <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 eroam-confirm-content">

                  </div>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-primary confirm-ok-button">OK</button>
                  <button type="button" class="btn btn-primary confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<!-- START ALERT -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="eroamConfirm" id="eroam-confirm">
  <div class="modal-dialog eroam-confirm-modal-size" role="document" >
    <div class="modal-content">
      <div class="modal-header eroam-confirm-header">
        <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title eroam-confirm-title" id="gridSystemModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 eroam-confirm-content-close" style="display:none; margin-left: 10px;">
            <p>
              <button type="button" class="close eroam-confirm-cancel-btn"><span aria-hidden="true">&times;</span></button>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 eroam-confirm-content">

          </div>
        </div>
      </div>
      <div class="modal-footer">

        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 text-center">
            <button type="button" class="btn btn-primary confirm-ok-button">OK</button>
            <button type="button" class="btn btn-primary confirm-cancel-button eroam-confirm-cancel-btn">Cancel</button>
          </div>
          <div class="col-md-2"></div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- START ALERT -->
<div class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="sessionExpire" id="session-expire">
  <div class="modal-dialog modal-sm" role="document" >
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
        <h4 class="modal-title" id="gridSystemModalLabel">Session Notification</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            Your eRoam session is about to time-out. Would you like to continue or return to HOME page?
          </div>
        </div>
      </div>
      <div class="modal-footer" style="background: none;">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 text-center">
            <button type="button" class="btn btn-primary session-home-btn">Home</button>
            <button type="button" class="btn btn-primary session-continue-btn"> Continue<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button>
          </div>
          <div class="col-md-2"></div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<!-- Modal -->
    <a href="#" id="onLoadLoginModal" data-toggle="modal" data-target="#loadLoginModal" style="display:none;">LOGIN</a>
    <div class="modal fade" id="loadLoginModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-sm" role="document" style="width: 450px; margin: 100px auto;">
        <div class="modal-content">
          <div class="modal-header">

            <div class="" style="padding-bottom: 25px;">
              <h4 class="modal-title" id="myModalLabel">Authentication</h4>
            </div>
          </div>
          <div class="modal-body">
            <div class="">
              <form id="auth-form" method="post" class="form-horizontal">
                 {{ csrf_field() }}
                <div class="input-field">
                  <input type="text" name="username" id="username" >
                  <label for="username">Enter Username</label>
                </div>

                <div class="input-field">
                  <input type="password" name="password" id="password">
                  <label for="password">Enter Password</label>

                </div>


                 <div class="notification text-center row m-t-25"></div>
                 <div class="input-field">
                    <span class="error-message"></span>
                 </div>

                    <div class="input-field">
                    <button type="submit" name="" class="btn btn-primary btn-block" id="authenticate-btn">LOGIN</button>
                 </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

	<input type="hidden" id="site-url" value={{ url('/') }}>
	<input type="hidden" id="api-url" value={{ config( 'env.API_URL' ) }}>
	<input type="hidden" id="cms-url" value={{ config( 'env.CMS_URL' ) }}>
	<input type="hidden" id="search-session" value="{{ json_encode( session()->get( 'search' ) ) }}">
	<input type="hidden" id="search-input" value="{{ json_encode( session()->get( 'search_input' ) ) }}">
	<input type="hidden" id="itinerary-page" value="{{ request()->segment(2) }}">
	<input type="hidden" id="itinerary-leg" value="{{ request()->input('leg') }}">
	<input type="hidden" id="currency-layer" value='{{ json_encode(session()->get('currency_layer')) }}'>
	<input type="hidden" id="all-currencies" value='{{ json_encode(session()->get('all_currencies')) }}'>
	<input type="hidden" id="apis" value="{{ Cache::get('apis') }}">

    <script src="{{ url( 'assets/js/theme/jquery.min.js' ) }}"></script>
<?php /*
	<script src="{{ url( 'assets/js/theme/jquery.validate.js' ) }}" type="text/javascript"></script>
	<script src="{{ url( 'assets/js/theme/bootstrap.min.js' ) }}" type="text/javascript"></script>
	<script src="{{ url( 'assets/js/theme/jquery.jcarousel.min.js' ) }}" type="text/javascript"></script>
	<script src="{{ url( 'assets/js/theme/main.js' ) }}" type="text/javascript"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtM-LpdcD_6qnBYf6fCY-djZH2aBCuItY"></script>
	<script src="{{ url( 'assets/js/theme/map.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/materialize.js' ) }}" type="text/javascript"></script>

    <script src="{{ url( 'assets/js/theme/bootstrap-datepicker.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/prettify.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.slimscroll.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/tmpl.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.dependClass-0.1.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/draggable-0.1.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.slider.js' ) }}"></script>
  <?php /*<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>

  <script type="text/javascript" src="{{ url('assets/js/markerlabel.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/infobox.js') }}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>*/ ?>
    <script src="{{ url( 'assets/jquery-ui/jquery-ui.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/materialize.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/jquery.validate.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/bootstrap.min.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/jquery.jcarousel.min.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/main.js' ) }}" type="text/javascript"></script>
    <script src="{{ url( 'assets/js/theme/bootstrap-datepicker.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/prettify.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.slimscroll.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/tmpl.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.dependClass-0.1.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/draggable-0.1.js' ) }}"></script>
    <script src="{{ url( 'assets/js/theme/jquery.slider.js' ) }}"></script>

    <?php /* <script src="{{ //url( 'assets/js/theme/map.js' ) }}" type="text/javascript"></script> */?>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>

    <script type="text/javascript" src="{{ url('assets/js/markerlabel.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/js/infobox.js') }}"></script>

  <script src="{{ url( 'assets/js/moment.js' ) }}"></script>
  <script src="{{ url( 'assets/js/moment-timezone.js' ) }}"></script>
  <script src="{{ url( 'assets/js/jstz.min.js' ) }}"></script> {{-- library to determine user timezone --}}

  <script src="{{ url( 'assets/js/slick.min.js' ) }}"></script>
  <script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>

  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
  <?php /*<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> */ ?>
  <script type="text/javascript">
    var globalCurrency = "{{ ( session()->has('currency') ) ? session()->get('currency') : 'AUD' }}";
    var globalCurrency_id = "{{ ( session()->has('currency_id') ) ? session()->get('currency_id') : 1 }}"
    var listOfCurrencies = JSON.parse($('#currency-layer').val());
    @if( session()->has('search_input') )
    <?php $input = session()->get( 'search_input' ); ?>
    var travel_pref = [{{ isset( $input['interests'] ) ? join(', ',  $input['interests']) : '' }}];
    @else
    var travel_pref = [];
    @endif
  </script>

  <script src="{{ url( 'assets/js/eroam.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-aot.js' ) }}"></script>
  <script src="{{ url( 'assets/js/booking-summary.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-mystifly.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-hb.js' ) }}"></script>
  <script src="{{ url( 'assets/js/api-ae.js' ) }}"></script>
  <script>

  $( document ).ready(function(){



    $('#account-dropdown').dropdown();

    getAllCurrencies();

    document.onload = function() {
        inactivityTime();
    };

    document.onmousemove = function() {
        inactivityTime();

    };
    document.onmousedown = function() {
        inactivityTime();
    };
    document.onkeypress = function() {
        inactivityTime();
    };
    document.ontouchstart = function() {
        inactivityTime();
    };
    var inactivityTime = function() {
      if( '{{ isset($page_will_expire) ? 1 : 0 }}' == 1 )
      {
        var t;
        var timer
        document.onload = resetTimer;
        window.onload = resetTimer;
        document.onmousemove = resetTimer;
        document.onkeypress = resetTimer;

        function showInactiveNotification() {
          if(! ($("#session-expire").data('bs.modal') || {isShown: false}).isShown ){
            var counter = 60;
            $('.session-home-btn').html('Home('+counter+')');
            $('#session-expire').modal('show');
            timer = setInterval(function(){
              $('.session-home-btn').html('Home('+counter+')');
              if( counter == 0 ){
                $('.session-home-btn').html('Home');
                $('.session-home-btn').prop('disabled', true);
                $('.session-continue-btn').prop('disabled', true);
                clearInterval(timer);
                window.location = '{{ url("") }}';
              }
              counter--;
            }, 1000);
          }
        }

        function resetTimer() {
          clearTimeout(t);
          clearInterval(timer);
          t = setTimeout(showInactiveNotification, 1740000);
        }

        $('.session-home-btn').click(function(){
          window.location = '{{ url("") }}';
        });

        $('.session-continue-btn').click(function(){
          clearTimeout(t);
          clearInterval(timer);
          $('#session-expire').modal('hide');
        });
      }
    };






    $('body').on('change', '.selected-currency', function() {
      var currency      = $(this).val();
      var id            = $(this).find(':selected').attr('data-value');
      var searchSession = $('#search-session').val();



      eroam.ajax('post', 'session/currency', { currency:currency, id:id }, function(response){
        eroam.ajax( 'post', 'update-booking-summary', { search: searchSession }, function( response ) {
          window.location.reload();
        }, function() {
          $( '.booking-summary' ).html( '<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>' );
        } );
      });
    });

    $('body').on('click', '.itinerary-leg-collapse', function(){
      var processing = $(this).attr('data-processing');

      if( processing == 10 ){
        var key = $(this).attr('data-key');
        //window.itinerayGlobalLeg = key;

        $(this).attr('data-processing', 1);
        eroam.ajax( 'get', 'latest-search', { }, function( response ){
          window.searchSession = JSON.parse(response);
          setDefault(key);

        });
      }

    });

  });





  // getCurrencyLayer();
  function getAllCurrencies() // UPDATED BY RGR
  {
    var allCurrencies = JSON.parse($('#all-currencies').val());
    var html = '';
    $.each(allCurrencies, function(key, value) {
      html += '<option '+( (globalCurrency == value.code) ? 'selected' : '' )+' value="'+value.code+'" data-value="'+value.id+'">('+value.code+') '+value.name+'</option>';
    });

    $('.selected-currency').html( html );
  }



  function getCurrencyLayer()
  {
    eroam.apiWithError('get', 'get_currencies', {}, function(response){
      listOfCurrencies = response;
      eroam.ajax('post', 'session/currency-layer', { currency_layer : response }, function(response){ console.log('Stored CurrencyLayer API data.'); });
    }, null, null, function(){
      getCurrencyLayer();
    });
  }





  function dateFormat(date, separator='-') {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join( separator );
  }
  function setDefault(legKey){
    var tasks = [
      setHotel,
      setActivity,
      setTransport,
      updateAll
        ];
        var key = legKey;

        $.each(tasks, function(index, value) {
            $(document).queue('tasks', processTask(key, value));
        });

        $(document).queue('tasks');
        $(document).dequeue('tasks');
  }
  function processTask(key, fn){
      return function(next){
          doTask(fn, next, key);
      }
  }

  function doTask(fn, next, key){
      fn(key, next);
  }
  function setHotel(key, next){
    var response = [];
    var leg = searchSession.itinerary[key];
    if( leg.hotel == null ){
      showLoader(key, 'hotel');
      var iatas = splitString(leg.city.airport_codes, ',');
      if( iatas.length > 0 ) {

        iatas.forEach(function(iata, iataKey){

          hb_data = {
            city_ids: leg.city.id,
            check_in: leg.city.date_from,
            check_out: leg.city.date_to,
            room:'1',
            adult: searchSession.travellers,
            code: iata,
            child:'0',
            provider: 'hb'
          };

          var hbApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', hb_data, 'hb', true);
          eroam.apiPromiseHandler( hbApiCall, function( hbResponse ){
            response = hbResponse.array;
          });
        });
      }
      else{
        next();
      }

      if(response.length > 0 ){
        var hotelLength = response.length;
        var index = 0;

        while( index < hotelLength ){

          value = response[index];
          var roomValue = value.rooms[0];

          if( roomValue.length > 0 && typeof roomValue.rates[0].net != 'undefined' ){

            roomPrice = parseInt(eroam.convertCurrency(roomValue.rates[0].net, 'AUD'));
            lowest = lowest == 0 ? roomPrice : lowest;
            lowest = roomPrice < lowest ? roomPrice : lowest;

            var hbData = {
              hotel_name: value.name,
              name: value.name,
              price: lowest,
              price_id: value.rateKey,
              currency: 'AUD',
              room_type: roomValue.name,
              rate_key: value.rateKey,
              room_id: roomValue.id,
              hotel_id: value.code,
              id: value.code,
              provider: 'hb',
              location: value.address,
              description: value.description,
              images: value.images,
              cancellation_policy: ''
            };
            index = hotelLength;
          }
          index ++;
        }
        next();
      }else{
        next();
      }
    }else{
      next();
    }

  }
  function setActivity(key, next){

    var leg = searchSession.itinerary[key];

    if( leg.activities == null || leg.activities.length == 0 ){
      showLoader(key, 'activities');
      var destinationReq = {
        destinationName: leg.city.name,
        country: leg.city.country.name
      };
      //console.log('test', destinationReq);

      var destinationApiCall = eroam.apiDeferred('service/location', 'POST', destinationReq, 'destination', true);
      eroam.apiPromiseHandler( destinationApiCall, function( destinationResponse ){

        if( destinationResponse != null ){

          if( Object.keys(destinationResponse).length > 0){
            vRQ = {
              destId: destinationResponse.destinationId,
              startDate: dateFormat(leg.city.date_from),
              endDate: dateFormat(leg.city.date_to),
              currencyCode: 'AUD',
              sortOrder: 'TOP_SELLERS',
              topX: '1-15',
              provider:'viator'
            };

            var viatorApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', vRQ, 'viator_activity', true);
            eroam.apiPromiseHandler( viatorApiCall, function( viatorResponse ){

              if( viatorResponse != null )
              {
                if( viatorResponse.length > 0 )
                {
                  var viatorAct = viatorResponse[0];

                  data ={
                    'date_from':formatDate(leg.city.date_from),
                    'date_to':formatDate(leg.city.date_to),
                    'id':viatorAct.code,
                    'city_id':leg.city.id,
                    'name': viatorAct.title,
                    'city':{
                      'id':leg.city.id
                    },
                    'activity_price':[{
                      'price':viatorAct.price,
                      'currency':{'id':1,'code':'AUD'}
                    }],
                    'price':[{
                      'price':viatorAct.price,
                      'currency':{'id':1,'code':'AUD'}
                    }],
                    'currency_id':1,
                    'currency': 'AUD',
                    'provider': 'viator',
                    'description': viatorAct.shortDescription,
                    /*to be updated static duration*/
                    'duration': 1
                  };

                  searchSession.itinerary[key].activities = [data];
                  next();
                }else{
                  next();
                }
              }else{
                next();
              }
            });
          }else{
            next();
          }
        }else{
          next();
        }
      });
    }else{
      next();
    }
  }
  function setTransport(key, next){
    var leg = searchSession.itinerary[key];
    key = parseInt(key);
    if( key != (searchSession.itinerary.length - 1) ){
      if( leg.transport == null || !leg.transport ){
        showLoader(key, 'transport');
        var mystiflyRQ;
        var mystiflyApiCalls            = [];
        var originIatas                 = leg.city.airport_codes;
        var destinationIatas            = searchSession.itinerary[ key + 1 ].city.airport_codes;
        var arrayOfOriginIataCodes      = splitString( originIatas, "," );
        var arrayOfDestinationIataCodes = splitString( destinationIatas, "," );

        if( arrayOfOriginIataCodes.length > 0 && arrayOfDestinationIataCodes.length > 0 ){
          arrayOfOriginIataCodes.forEach(function( originIataCode ){
            arrayOfDestinationIataCodes.forEach(function( destinationIataCode ){
              mystiflyRQ = {
                DepartureDate : leg.city.date_to,
                OriginLocationCode : originIataCode,
                DestinationLocationCode : destinationIataCode,
                CabinPreference : 'Y',
                Code : ['ADT'],
                Quantity : [searchSession.travellers],
                IsRefundable : false,
                IsResidentFare : false,
                NearByAirports : false,
                provider: 'mystifly'
              };

              try{
                console.log('mystiflyRQ Leg '+key, mystiflyRQ);
                mystiflyApiCalls.push( eroam.ajaxDeferred( 'set-cache-api-data', 'POST', mystiflyRQ, 'mystifly', true ) );
              }catch(e){
                console.log('An error has occured while sending the request to the mystifly API for leg '+key, e.message);
              }
            });
          });

          try{
            eroam.apiArrayOfPromisesHandler( mystiflyApiCalls, function( mystiflyArrayOfResponses ){
              mystiflyArrayOfResponses.forEach( function( mystiflyResponse ){

                if( mystiflyResponse != null ){
                  try{
                    var transport;
                    if( Array.isArray( mystiflyResponse.PricedItineraries.PricedItinerary ) ){
                      if( mystiflyResponse.PricedItineraries.PricedItinerary.length > 0 ){
                        transport = mystiflyResponse.PricedItineraries.PricedItinerary[0];
                      }
                    }else{
                      transport = mystiflyResponse.PricedItineraries.PricedItinerary;
                    }

                    var fromCity           = leg.city.name;
                    var toCity             =searchSession.itinerary[ key + 1 ].city.name;
                    var t                  = transport.OriginDestinationOptions.OriginDestinationOption.FlightSegments.FlightSegment;
                    var fareSourceCode     = transport.AirItineraryPricingInfo.FareSourceCode;
                    var airlineName        = ( isNotUndefined( t.OperatingAirline.Name ) ) ? t.OperatingAirline.Name : 'N/A';
                    var airlineCode        = ( isNotUndefined( t.OperatingAirline.Code ) ) ? t.OperatingAirline.Code : 'N/A';
                    var etd                = t.DepartureDateTime;
                    var eta                = t.ArrivalDateTime;
                    var transportClass     = (t.CabinClassText).trim() != '' ? t.CabinClassText : 'Cabin Class Not Specified';
                    var departure          = moment( etd, moment.ISO_8601 );
                    var arrival            = moment( eta, moment.ISO_8601 );
                    var difference         = moment.duration( arrival.diff( departure ) );
                    var duration           = difference.asHours();
                    var currency           = transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.CurrencyCode;
                    var price              = eroam.convertCurrency( transport.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount, currency );

                    var transportName      = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+' / Depart - '+(t.DepartureData != null ? t.DepartureData : fromCity)+' '+departure.format('hh:mm A')+'. Arrive - '+(t.ArrivalData != null ? t.ArrivalData : toCity)+' '+arrival.format('hh:mm A');

                    var bookingSummaryText = airlineName.toUpperCase()+', Flight # '+t.FlightNumber+'<br/><small>Depart: '+t.DepartureData+' '+departure.format('hh:mm A')+'</small><br/><small>Arrive: '+t.ArrivalData+' '+arrival.format('hh:mm A')+'</small>';

                    var data = {
                      airline_code: airlineCode,
                      arrival_data: t.ArrivalData,
                      arrival_location: t.ArrivalAirportLocationCode,
                      booking_summary_text: bookingSummaryText,
                      currency: globalCurrency,
                      departure_data: t.DepartureData,
                      departure_location: t.DepartureAirportLocationCode,
                      duration: duration,
                      eta: eta,
                      etd: etd,
                      fare_source_code: fareSourceCode,
                      fare_type: transport.AirItineraryPricingInfo.FareType,
                      flight_number: t.FlightNumber,
                      from_city_id: leg.city.id,
                      id: fareSourceCode,
                      is_selected: true,
                      operating_airline: airlineName,
                      passenger_type_code: "ADT",
                      passenger_type_quantity: searchSession.travellers,
                      price: {
                        0:{
                          price:price,
                          currency:{
                            code:currency
                          }
                        }
                      },
                      provider: "mystifly",
                      to_city_id: searchSession.itinerary[ key + 1 ].city.id,
                      transport_id: fareSourceCode,
                      transport_type: {
                        id: 1,
                        name: "Flight"
                      },
                      transport_type_id: 1,
                      transport_type_name: "Flight",
                      transporttype: {
                        id: 1,
                        name: "Flight"
                      }
                    };
                    searchSession.itinerary[ key ].transport = data;
                    console.log('mystifly successful call Leg '+key, data);

                  }catch(e){
                    console.log('An error has occured while formatting the response of mystifly API call for leg'+key, e.message);
                  }
                  next();
                }else{
                  next();
                }
              });
            });

          }catch(e){
            console.log('An error has occured while processing the response of mystifly API call for leg' +key, e.message);
          }
          next();
        }else{
          next();
        }
      }else{
        next();
      }
    }else{
      next();
    }
  }

  function updateAll(key, next){
    var position = $('.itinerary-leg-container[data-index="'+key+'"]').position().top;

    searchSession.itinerary[key].city.processed = 1;
    bookingSummary.tempUpdate( JSON.stringify( searchSession ), key, position );
    next();
  }

  function showLoader(key, legType){
    $('.itinerary-'+legType+'-name').eq(key).html(
      '<div class="la-ball-pulse" style="color: #2AA9DF">' +
        '<div></div>' +
        '<div></div>' +
        '<div></div>' +
      '</div>'
    );
    $('.itinerary-'+legType+'-name').eq(key).closest('.itinerary-leg-link').attr('data-loading', 'yes').css('cursor', 'wait');
  }

  </script>


	@yield( 'custom-js' )

</body>
</html>