<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
	public function home() {
		//$response = http( 'get', 'map/get/all/countries' );
		$countries = test_http( 'get', url('countries') );
		$labels = test_http( 'get', url('labels') );
		$travelers = test_http( 'get', url('traveler-options') );
		return view( 
			'pages.home', 
			[
				'countries'=> $countries,
				'labels' => $labels['data'],
				'travelers' => $travelers
			]
		);
	}

	public function countries(){
		$countries = file_get_contents(url('country.txt'));
		$countries = json_decode( $countries );
		return $countries;
	}

	public function travelers(){
		$traveler_options = file_get_contents( url( 'traveler-options.txt' ) );
		$traveler_options = json_decode( $traveler_options, true );
		return $traveler_options;
	}

	public function labels(){
		$labels = file_get_contents( url( 'labels.txt' ) );
		$labels = json_decode( $labels, true );
		return $labels;
	}


}
