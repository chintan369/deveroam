
var countryName   = '{{ $countryName }}';
 var default_currency   = '{{ $default_currency }}';
    var siteUrl = $( '#site-url' ).val();
  $(document).ready(function(){
         tourList();
  });             
    function tourList(next){
        tour_data = {
          countryName: countryName,
          provider: 'getTours'
        };

        // CACHING HOTEL BEDS
        var tourApiCall = eroam.ajaxDeferred('set-cache-api-data', 'POST', tour_data, 'getTours', true);

        eroam.apiPromiseHandler( tourApiCall, function( tourResponse ){
         console.log('tourResponse', tourResponse);
          if( tourResponse.length > 0 ){
            $.each( tourResponse, function( key, value ) {
              //console.log('value : '+ key + value);
              appendTours( value );
            })

          }
        })
      }
    function getStars(count, half = false){
        var stars = '';
        if( parseInt( count ) ){
          for( star = 1; star <= count; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star"> </i></a><li>';
          }
          var emptyStars = 5 - parseInt( count );
          if(half){
            stars += '<li><a href="#"><i class="fa fa-star-half-o"></i></a><li>';
            emptyStars = emptyStars - 1;
          }
          for( empty = 1; empty <= emptyStars; empty ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }else{
          for( star = 1; star <= 5; star ++ ){
            stars += '<li><a href="#"><i class="fa fa-star-o"></i></a><li>';
          }
        }
         var stars = '';
        return stars;
      }  
    function appendTours(tour){

        $("#tours-loader").hide();
        var price = parseFloat(tour.price).toFixed(2);
        var star;
        var rating = tour.rating;

        if(rating % 1 === 0){
          stars = getStars( rating);
        }else {
          stars = getStars( rating, true );
        }

        var imgurl = 'http://www.adventuretravel.com.au'+tour.DiscountPercentage+'245x169/'+tour.OriginalPrice;

        $("#overlay").hide();
          if(parseInt(tour.no_of_days) == 1) {
              var day = 'Day';
          }else{
              var day = 'Days';
          }
        var html = '<div class="col-md-3 col-sm-4 m-t-20"><a href="/tourDetail/'+tour.tour_id+'/'+tour.tour_url+'">'+
                  '<div class="place-wrapper">'+
                    '<div class="place-image">'+
                      '<img src="'+imgurl+'" alt="" class="img-responsive" id="tourImage_'+tour.tour_id+'">'+
                    '</div>'+
                    '<div class="place-inner">'+
                      '<div class="row">'+
                        '<div class="col-xs-6">'+

                            '<p><strong>' + parseInt(tour.no_of_days) + day +'</strong></p>' +
                        '</div>'+
                        '<div class="col-xs-6 text-right">'+
                          '<ul class="rating">'+stars+'</ul>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                  '<div class="place-details">'+
                      '<p> '+tour.tour_title+' - From '+default_currency+' '+price+' Per Person</p>'+
                  '</div>'+
                '</a></div>';
        $('#tourList').append(html);

        imageUrl = checkImageUrl(tour.tour_id, imgurl);
      }
      function checkImageUrl(id, url){
        eroam.ajax('post', 'existsImage', {url : url}, function(response){
          if(response == 200){
            //var image = "{{ url( 'assets/images/no-image.jpg' ) }}";
            //$("#tourImage_"+id).attr('src', image);
          } else if(response == 400){
            //var image = "{{ url( 'assets/images/no-image.jpg' ) }}";
            //$("#tourImage_"+id).attr('src', image);
          } else {
              var image = "{{ url( 'assets/images/no-image.jpg' ) }}";
              $("#tourImage_"+id).attr('src', image);
          }
        });  
      }