/*------------- Jcarousel slider function Starts -------------- */
(function($) {
        $(function() { 
            var jcarousel = $('.jcarousel'); 

            jcarousel
                .on('jcarousel:reload jcarousel:create', function () {
                    var carousel = $(this),
                        width = carousel.innerWidth();

                    if (width >= 1280) {
                        width = width / 5;
                    } else if (width >= 992) {
                        width = width / 4;
                    }
                    else if (width >= 768) {
                        width = width / 3;
                    }
                    else if (width >= 640) {
                        width = width / 2;
                    }

                    carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                })
                .jcarousel({
                    wrap: 'circular'
                });

            $('.jcarousel-control-prev')
                .jcarouselControl({
                    target: '-=1'
                });

            $('.jcarousel-control-next')
                .jcarouselControl({
                    target: '+=1'
                });

            $('.jcarousel-pagination')
                .on('jcarouselpagination:active', 'a', function() {
                    $(this).addClass('active');
                })
                .on('jcarouselpagination:inactive', 'a', function() {
                    $(this).removeClass('active');
                })
                .on('click', function(e) {
                    e.preventDefault();
                })
                .jcarouselPagination({
                    perPage: 1,
                    item: function(page) {
                        return '<a href="#' + page + '">' + page + '</a>';
                    }
                });
        });
    })(jQuery);


/*------------- Set equal height function Starts -------------- */
function setEqualHeight_CommonClass(arr) {
    var x = new Array([]);
    $(arr).each(function(i) {
        $(this).height('auto');
        x[i] = $(this).height();
        //console.log($(this).attr('class')+'//'+$(this).height());
    });
    Max_Value = Array.max(x);
    $(arr).each(function(i) {
        $(this).height(Max_Value);
    });
}

function setEqualHeight_CommonClassMin(arr) {
    var x = new Array([]);
    $(arr).each(function(i) {
        $(this).height('auto');
        x[i] = $(this).height();
        //console.log($(this).attr('class')+'//'+$(this).height());
    });
    Max_Value = Array.min(x);
    $(arr).each(function(i) {
        $(this).height(Max_Value);
    });
}

function setEqualHeight(arr) {
    var x = new Array([]);
    for (i = 0; i < arr.length; i++) {
        x[i] = $(arr[i]).height("auto");
        x[i] = $(arr[i]).height();
    }
    Max_Value = Array.max(x);
    for (i = 0; i < arr.length; i++) {
        if ($(arr[i]).height() != Max_Value) {
            if ($("body").hasClass("ie7")) {
                x[i] = $(arr[i]).attr("style", "height:" + Max_Value + "px");
            } else {
                x[i] = $(arr[i]).height(Max_Value);
            }
        }
    }
}

Array.max = function(array) {
    return Math.max.apply(Math, array);
};

Array.min = function(array) {
    return Math.min.apply(Math, array);
};
/*------------- Set equal height function Ends -------------- */



$(document).ready(function(){
  setEqualHeight_CommonClass(".place-details");
  setEqualHeight_CommonClass(".place-details h4");
  setEqualHeight_CommonClass(".hotel-details-block .hotel-details .panel-container");
  //setEqualHeight_CommonClass(".tour-title");
  setEqualHeight_CommonClass(".hotel-details-block .hotel-details");
  //setEqualHeight_CommonClassMin(".grid-img");
  setEqualHeight_CommonClass(".grid-details h2");
  setEqualHeight_CommonClass(".grid-details");
  //setEqualHeight_CommonClass(".hotel-img");
  setEqualHeight_CommonClass(".tour-wrapper .carousel-inner .thumbnail");
  // setEqualHeight([".listbox-img", ".listbox-details"]);
  setEqualHeight_CommonClass(".hotel-block .hotel-bg");
});

$(window).resize(function(){
  setEqualHeight_CommonClass(".place-details");
  setEqualHeight_CommonClass(".place-details h4");
  setEqualHeight_CommonClass(".hotel-details-block .hotel-details .panel-container");
  //setEqualHeight_CommonClass(".tour-title");
  setEqualHeight_CommonClass(".hotel-details-block .hotel-details");
  //setEqualHeight_CommonClassMin(".grid-img");
  setEqualHeight_CommonClass(".grid-details h2");
  setEqualHeight_CommonClass(".grid-details");
  //setEqualHeight_CommonClass(".hotel-img");
  setEqualHeight_CommonClass(".tour-wrapper .carousel-inner .thumbnail");
  // setEqualHeight([".listbox-img", ".listbox-details"]);
  setEqualHeight_CommonClass(".hotel-block .hotel-bg");
});




$(function() {
    //$('.dashboard-new-box').eq(0).show();
    $('#packages-option li a').click(function(){
        $('.main-box').hide();
        var value = $(this).attr('value');
        var clickedValue = $(this).text();
        $('#' + value).show();
        document.getElementById('packages-option').innerHTML = clickedValue;
        //console.log(href);
    });
});

$(function() {
    //$('.dashboard-new-box').eq(0).show();
    $('.hotel-title a').click(function(){
        $('.hotelList-wrapper').hide();
        var value = $(this).attr('data-id');
        $('#' + value).show();
        //console.log(value);
    });

    $('.hotel-close').click(function(){
      $('.hotel-container').hide();
      $('.hotelList-wrapper').show();
    });
});


$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


    function setupLabel() {
          if ($('.label_check input').length) {
              $('.label_check').each(function(){ 
                  $(this).removeClass('c_on');
              });
              $('.label_check input:checked').each(function(){ 
                  $(this).parent('label').addClass('c_on');
              });                
          };
          if ($('.label_radio input').length) {
              $('.label_radio').each(function(){ 
                  $(this).removeClass('r_on');
              });
              $('.label_radio input:checked').each(function(){ 
                  $(this).parent('label').addClass('r_on');
              });
          };
      };
      $(document).ready(function(){
          $('.label_check, .label_radio').click(function(){
              setupLabel();
          });
          setupLabel(); 
      });
$(document).ready(function(){
    $('#onRegisterModal').click();
    $('#onloginModal').click();
});