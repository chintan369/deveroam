/* Eroam Google Maps Script */

var eMap = (function($) {

	var mapData = JSON.parse($('#map-data').val()),
		searchSession = JSON.parse($('#search-session').val()),
		searchInput = JSON.parse($('#search-input').val()),
		map,
		mapSettings = {
			radius: 400000,
			displayRadius: false
		},
		theme = {
			styles: [
				{'featureType': 'administrative.province', 'elementType': 'labels', 'stylers': [{'visibility': 'off'}]},
				{'featureType': 'administrative.neighborhood', 'elementType': 'labels', 'stylers': [{'visibility': 'off'}]},
				// {'featureType': 'administrative.locality', 'elementType': 'labels', 'stylers': [{'visibility': 'off'}]},
				// {'featureType': 'all', 'elementType': 'labels', 'stylers': [{'visibility': 'off'}]}, // font weight
				// {'featureType': 'all', 'elementType': 'labels', 'stylers': [{'weight': 6}]}, // font weight
				// {'featureType': 'all', 'elementType': 'labels.text.fill', 'stylers': [{'color': '#333333' }]}, // fill color
				// {'featureType': 'all', 'elementType': 'labels.text.stroke', 'stylers': [{'color': '#EAEAEA' },{'weight': '2'}]}, // stroke color and weight
				{'featureType': 'landscape', 'stylers': [{'color': '#EAEAEA'}]}, // landscape
				{'featureType': 'road', 'stylers': [{'color': '#d8d8d8'}]}, // road
				{'featureType': 'water', 'stylers': [{'color': '#FFFFFF'}]}, // water
				{'featureType': 'poi', 'stylers': [{'color': '#EAEAEA'}]} // point of interest
			],
			icon: {
				active: eroam.siteUrl + '/assets/img/eroam-pin-blue.png',
				// inactive: eroam.siteUrl + '/assets/img/eroam-pin-grey.png'
				inactive: {
					path: google.maps.SymbolPath.CIRCLE,
					strokeColor: '#2AA9DF',
					strokeOpacity: 1,
					fillOpacity: 1,
					fillColor: '#2AA9DF',
					scale: 2.5
				},
				optional: {
					path: google.maps.SymbolPath.CIRCLE,
					strokeColor: '#75B8D5',
					strokeOpacity: 0.5,
					fillOpacity: 1,
					fillColor: '#75B8D5',
					scale: 3
				}
			},
			polyline: {
				flightFerry: {
					strokeOpacity: 1,
					strokeColor:'#27A8DF',
					strokeWeight: 3,
					geodesic: true
				},
				noRoute: {
					strokeOpacity: 1,
					strokeColor:'#969696',
					strokeWeight: 3,
					geodesic: true
				}
			},
			route: {
				active: {
					strokeOpacity: 1,
					strokeColor:'#27A8DF',
					strokeWeight: 3,
					geodesic: true,

				},
				alternative: {
					strokeOpacity: 0,
					strokeColor: '#4AB2DF',
					strokeWeight: 3,
					geodesic: true,
					icons: [{
						icon: {
							path: 'M 0,-1 0,1',
							strokeOpacity: 1,
							scale: 3
						},
						offset: '0',
						repeat: '14px'
					}]
				}
			}
		},
		rendererOptions = {
			preserveViewport: true,
			suppressMarkers: true,
			polylineOptions: theme.route.active
		}
		infowindow     = new google.maps.InfoWindow,
		dirService     = new google.maps.DirectionsService;

	function init() {
		map = new google.maps.Map(document.getElementById('map') , {
			zoom: 5,
			styles : theme.styles,
			mapTypeControl: false
		});

		/**
		 * Event listener for the map when clicked
		 */
		google.maps.event.addListener(map, 'click', function(event) {
			infowindow.close();
		});

		// zoom map listener
		google.maps.event.addListener(map, 'zoom_changed', function() {
			if (this.getZoom() > 3) {
				// SHOW CITY lABELS
				$.each(map.allMarkers, function(k, v) {
					// v.label.open(map);
				});
			} else {
				// HIDE CITY lABELS
				$.each(map.allMarkers, function(k, v) {
					// v.label.close();
				});
			}
		});

		// an array for all circles
		map.circles = [];

		map.allMarkers = [];

		// an array for all active markers
		map.activeMarkers = [];

		// an array for all inactive markers
		map.inactiveMarkers = [];

		// an array for direction routes
		map.directionsRoutes = [];

		// check type
		if (mapData.type == 'manual') {
			initManual();
		} else if (mapData.type == 'auto') {
			buildAutoMap();
		}
	}

	function latlng(lat, lng) {
		return new google.maps.LatLng(lat, lng);
	}

	function setCenter(obj) {
		map.setCenter({
			lat: parseFloat(obj.lat),
			lng: parseFloat(obj.lng)
		});
	}

	function addCircle(lat, lng, radius) {
		var circle = new google.maps.Circle({
			map: map,
			center: latlng(lat, lng),
			visible: mapSettings.displayRadius,
			radius: radius ? radius : mapSettings.radius,
			strokeWeight: 0,
			fillColor: '#1d5a84'
		});
		return circle;
	}

	function resetInactiveMarkers() {
		// Hide all grey markers
		$.each(map.inactiveMarkers, function(k, v) {
			v.setVisible(false);
			v.infowindowCity.setMap(null);
			v.label.setVisible(false);
		});
		map.inactiveMarkers = [];
	}

	function degToRad(deg) {
	  return deg * (Math.PI / 180);
	}

	// GET DISTANCE BETWEEN TWO POITNS IN KILOMETERS
	function getDistance(pointA, pointB) {
		var lat = degToRad(pointB.lat() - pointA.lat()),
			lng = degToRad(pointB.lng() - pointA.lng()),
			variable = Math.sin(lat / 2) * Math.sin(lat / 2) +
				Math.cos(degToRad(pointA.lat())) *
				Math.cos(degToRad(pointB.lat())) *
				Math.sin(lng / 2) * Math.sin(lng / 2),
			anotherVariable = 2 * Math.atan2(Math.sqrt(variable), Math.sqrt(1 - variable)), // formula I found on the web
			earthsRadius = 6371; // google it
			distance = earthsRadius * anotherVariable; // distance in kilometers
		return distance;
	}

	function checkIfInCircle(circle, cityLatLng) {
		var distance = getDistance(circle.getCenter(), cityLatLng);
		return (distance <= circle.getRadius() / 1000) ? true : false;
	}

	function getCitiesAlongThePath(routePath, polyline = false) {
		if (mapData.type == 'auto' && mapData.routes.length == 1 && mapData.routes[0].cities.length == 2) {

			// exclude polylines
			if (polyline === true) {
				$('#map-loader').fadeOut();
				return;
			}

			var smallPathIndex = 0,
				bigPathIndex = 0,
				smallCircles = [],
				bigCircles = [],
				smallRadius = 200000, // 200 km
				bigRadius = 400000; // 400 km

			$.each(routePath, function(key, value) {
				var distanceFromCurrentPath = getDistance(routePath[smallPathIndex], value);
				if (distanceFromCurrentPath >= smallRadius / 1000) { // IF GREATER THAN 200KM
					smallPathIndex = key;
					var smallCircle = addCircle(value.lat(), value.lng(), smallRadius);
					smallCircles.push(smallCircle);
				}
				var distanceFromCurrentPath = getDistance(routePath[bigPathIndex], value);
				if (distanceFromCurrentPath >= bigRadius / 1000) { // IF GREATER THAN 400KM
					bigPathIndex = key;
					var bigCircle = addCircle(value.lat(), value.lng(), bigRadius);
					bigCircles.push(bigCircle);
				}
			});
			var cities = JSON.parse($('#all-cities').val());
			var inbetweenCities = [];

			$.each(cities, function(cityKey, cityValue) {
				
				if (cityValue.latlong) {
					var position = latlng(cityValue.latlong.lat, cityValue.latlong.lng);
					if (cityValue.country_name == 'Australia' || cityValue.country_name == 'India') {
						$.each(bigCircles, function(key, value) {
							var firstOrLast = mapData.routes[0].cities.some(function(v, k) {
								return v.id == cityValue.id;
							});
							var found = inbetweenCities.some(function(v, k) {
								return v.id == cityValue.id;
							});

							// check if city is within the path
							if (checkIfInCircle(value, position) && !firstOrLast && !found && cityValue.optional_city != 1) {
								cityValue.lat = cityValue.latlong.lat;
								cityValue.lng = cityValue.latlong.lng;
								cityValue.distanceFromOrigin = getDistance(latlng(mapData.routes[0].cities[0].lat, mapData.routes[0].cities[0].lng), latlng(cityValue.lat, cityValue.lng));
								inbetweenCities.push(cityValue);
							}
						});
					} else {
						$.each(smallCircles, function(key, value) {
							var firstOrLast = mapData.routes[0].cities.some(function(v, k) {
								return v.id == cityValue.id;
							});
							var found = inbetweenCities.some(function(v, k) {
								return v.id == cityValue.id;
							});

							// check if city is within the path
							if (checkIfInCircle(value, position) && !firstOrLast && !found && cityValue.optional_city != 1) {
								cityValue.lat = cityValue.latlong.lat;
								cityValue.lng = cityValue.latlong.lng;
								cityValue.distanceFromOrigin = getDistance(latlng(mapData.routes[0].cities[0].lat, mapData.routes[0].cities[0].lng), latlng(cityValue.lat, cityValue.lng));
								inbetweenCities.push(cityValue);
							}
						});
					}
				}
			});
			// $('#map-loader').fadeOut();
			// return;
			// check if has in-between cities
			if (inbetweenCities.length > 0) {
				var sortedCities = getSortedCitiesByShortestDistance(inbetweenCities, mapData.routes[0].cities[0], $(mapData.routes[0].cities).last()[0]);
				eroam.ajax('post', 'map/add-cities-auto-map', {cities: JSON.stringify(sortedCities)}, function(response) {
					mapData.routes = response.routes;
					searchSession = response.search_session;
					bookingSummary.update(JSON.stringify(searchSession), true, function() {
						$('#current-route-container').html('');
						for (var i = 0; i < mapData.routes[0].cities.length; i++) {
							var city = mapData.routes[0].cities[i];
							var arrow = i < mapData.routes[0].cities.length - 1 ? '<i class="fa fa-long-arrow-right"></i>' : '';
							$('#current-route-container').append(
								'<span>'+ city.name +'</span>' +
								arrow
							);
						}
					});

					$.each(map.directionsRoutes, function(key, value) {
						value.setMap(null);
					});
					$.each(map.activeMarkers, function(key, value) {
						value.setMap(null);
					});
					map.activeMarkers = [];
					map.allMarkers = [];
					map.directionsRoutes = [];
					buildAutoMap();
				});
			} else {
				$('#map-loader').fadeOut();
			}
		}
	}

	function drawCircle(point, radius, dir)
	{ 
		var radians = Math.PI / 180; // degrees to radians 
		var degrees = 180 / Math.PI; // radians to degrees 
		var earthsRadius = 3963; // 3963 is the radius of the earth in miles
		var points = 32;

		// find the raidus in lat/lon 
		var rlat = (radius / earthsRadius) * degrees;
		var rlng = rlat / Math.cos(point.lat() * radians);

		var extp = new Array();
		var start = dir == 1 ? 0 : points + 1;
		var end = dir == 1 ? points + 1 : 0;
		for (var i = start; (dir == 1 ? i < end : i > end); i = i + dir) {
			var theta = Math.PI * (i / (points/2));
			ey = point.lng() + (rlng * Math.cos(theta)); // center a + radius x * cos(theta) 
			ex = point.lat() + (rlat * Math.sin(theta)); // center b + radius y * sin(theta) 
			extp.push(new google.maps.LatLng(ex, ey));
		}
		return extp;
	}

	function drawRoute(directions, alternative, fix = false, origin = null, destination = null) { // draw the actual route
		// rendererOptions.polylineOptions = alternative ? theme.route.alternative : theme.route.active;
		// var dirRenderer = new google.maps.DirectionsRenderer(rendererOptions);
		var routeLines = alternative ? theme.route.alternative : theme.route.active;
		var steps = directions.routes[0].legs[0].steps;
		var overviewPath = directions.routes[0].overview_path;
		// CREATE THE POLYLINE
		var polyline = new google.maps.Polyline(routeLines);
		polyline.setPath(overviewPath);
		if (!alternative) {
			polyline.origin = origin.getPosition();
			polyline.destination = destination.getPosition();
			polyline.from_city_id = origin.cityId;
			polyline.to_city_id = destination.cityId;
		}
		polyline.setMap(map);
		map.directionsRoutes.push(polyline);
		getCitiesAlongThePath(overviewPath);
	}

	function drawPolyline(path, hasRoute, alternative, fix = false, origin = null, destination = null) { // draw the actual polyline
		var line = hasRoute ? theme.polyline.flightFerry : theme.polyline.noRoute;
		if (alternative) {
			line = theme.route.alternative;
		}
		var polyline = new google.maps.Polyline(line);
		polyline.setPath(path);
		if (!alternative) {
			polyline.origin = origin.getPosition();
			polyline.destination = destination.getPosition();
			polyline.from_city_id = origin.cityId;
			polyline.to_city_id = destination.cityId;
		}
		polyline.setMap(map);
		map.directionsRoutes.push(polyline);
		var distance = google.maps.geometry.spherical.computeDistanceBetween(latlng(path[0].lat, path[0].lng), latlng(path[1].lat, path[1].lng));

		getCitiesAlongThePath(null, true);
	}

	/* Set the route plan */
	function setRoute(marker, routeIndex, roundTrip, markerIndex) {
		var prevMarker = roundTrip ? map.activeMarkers[markerIndex - 1] : map.activeMarkers[map.activeMarkers.indexOf(marker) - 1],
			path = [
				{ lat: prevMarker.getPosition().lat(), lng: prevMarker.getPosition().lng() },
				{ lat: marker.getPosition().lat(), lng: marker.getPosition().lng() }
			],
			routeOptions = {
				origin: latlng(path[0].lat, path[0].lng),
				destination: latlng(path[1].lat, path[1].lng),
				travelMode: google.maps.TravelMode.DRIVING,
				optimizeWaypoints: false
			},
			data = {
				from_city_id: prevMarker.cityId,
				to_city_id: marker.cityId
			},
			route = function(fix = false) {
				var itinerary = roundTrip ? searchSession['itinerary'][markerIndex - 1] : searchSession['itinerary'][map.activeMarkers.indexOf(marker) - 1];
				var flight = itinerary['transport'] !== null && itinerary['transport']['transport_type_id'] == 1 ? true : false;

				dirService.route(routeOptions, function(response, status) {
					switch (status) {
						case google.maps.DirectionsStatus.OK:
							if ((flight == true && (mapData.type == 'auto' && mapData.routes[0].cities.length > 2)) || (flight == true && mapData.type == 'manual')) {
								// FLIGHT
								drawPolyline(path, true, false, fix, prevMarker, marker);
							} else {
								// DRIVING ROUTE
								drawRoute(response, false, fix, prevMarker, marker);
							}
							break;
						case 'ZERO_RESULTS':
							if (flight == true) {
								// FLIGHT
								drawPolyline(path, true, false, fix, prevMarker, marker);
							} else {
								// draw polyline for missing routes or own arrangement
								drawPolyline(path, false, false, fix, prevMarker, marker);
							}
							break;
						case 'OVER_QUERY_LIMIT':
							setTimeout(function() {
								route(true);
							}, 100);
							break;
					}

					// if (status === google.maps.DirectionsStatus.OK) {
					// 	if auto map, if only two cities, if flight
					// 	// DRAW GMAP ROUTE
					// 	drawRoute(response, false, fix, prevMarker, marker);
					// }
					// if (status === google.maps.DirectionsStatus.OK && (mapData.routes[0].cities.length == 2 && mapData.type == 'auto') && flight === false) {
						
					// } else if (flight === true) {
					// 	// draw polyline if transport type if light
					// 	drawPolyline(path, true, false, fix, prevMarker, marker);
					// } else if (status == 'ZERO_RESULTS') {
					// 	// draw polyline for missing routes or own arrangement
					// 	drawPolyline(path, false, false, fix, prevMarker, marker);
					// } else if (status == 'OVER_QUERY_LIMIT') {
					// 	setTimeout(function() {
					// 		route(true);
					// 	}, 100);
					// }
				});
				if (mapData.type == 'auto') {
					setTimeout(function() {
						fitMarkersToMap();
					}, 1000);
				}
			};
		route();
	}

	function setAlternativeRoute(routes) {
		$.each(routes.cities, function(key, value) {
			if (key != 0) {
				var path = [
						{ lat: routes.cities[key - 1].lat , lng: routes.cities[key - 1].lng },
						{ lat: value.lat, lng: value.lng }
					],
					routeOptions = {
						origin: latlng(path[0].lat, path[0].lng),
						destination: latlng(path[1].lat, path[1].lng),
						travelMode: google.maps.TravelMode.DRIVING,
						optimizeWaypoints: false
					},
					data = {
						from_city_id: routes.cities[key - 1].id,
						to_city_id: value.id
					},
					route = function(fix = false) {
						dirService.route(routeOptions, function(response, status) {
							if (status === google.maps.DirectionsStatus.OK) {
								// draw actual route
								drawRoute(response, true, fix);
							} else if (status == 'ZERO_RESULTS') {
								// draw polyline for missing routes or own arrangement
								drawPolyline(path, false, true, fix);
							} else if (status == 'OVER_QUERY_LIMIT') {
								setTimeout(function() {
									route(true);
								}, 100);
							}
						});
					};
				route();
			}
		});
	}

	function switchRoute(key) {
		// update map data session in php
		eroam.ajax('post', 'map/switch-route', {key: key}, function() {
			window.location.reload();
		}, function() {
			$('.close-edit-map-btn').click();
			$('#map-loader').show();
		});
	}

	/* Reset / Redraw routes & polylines */
	function resetRoutes() {
		$.each(map.directionsRoutes, function(key, value) {
			value.setMap(null);
		});
		map.directionsRoutes = [];
		var tempArray = [],
			found;

		$.each(map.activeMarkers, function(key, value) {
			if (key != 0) {
				if (value.roundTrip) {
					setRoute(value, null, true, key);
				} else {
					setRoute(value);
				}
			}
			// else if (key == 0 && map.activeMarkers.length == 1) { // if 1 marker is left
			// 	showNearbyCities(value);
			// }
		});
	}

	function addMarker(obj, active, index) {
		var nights = obj.default_nights.toString().length == 1 ? '0' + obj.default_nights : obj.default_nights;
		if (searchSession && searchSession.itinerary[index]) {
			nights = searchSession.itinerary[index].city.default_nights.toString().length== 1 ? '0' + searchSession.itinerary[index].city.default_nights : searchSession.itinerary[index].city.default_nights;
		} else {
			nights = '';
		}
		var icon = active ? theme.icon.active : theme.icon.inactive,
			lat = parseFloat(obj.lat),
			lng = parseFloat(obj.lng),
			marker = new MarkerWithLabel({
				position: latlng(lat, lng),
				map: map,
				labelContent: nights,
				labelAnchor: new google.maps.Point(7, 31),
				labelClass: 'marker-label',
				icon: icon,
				nights: obj.default_nights
			});

		if (parseInt(obj.optional_city) == 1 && !active) {
			marker.setIcon(theme.icon.optional);
		}

		marker.city = obj;
		marker.cityId = obj.id; // city id
		marker.type = active ? 'active' : 'inactive'; // active is green, inactive is grey
		map.allMarkers.push(marker);
		if (active) {
			map.activeMarkers.push(marker);
		} else {
			map.inactiveMarkers.push(marker);
		}

		// city label
		// setInfoBox(marker, obj);

		// Info window label (city names)
		var infowindowCity = new google.maps.InfoWindow({
			disableAutoPan: true
		});
		marker.infowindowCity = infowindowCity;

		if ($('#city-label-' + obj.id).length == 0) {
			infowindowCity.setContent('<div id="city-label-' + obj.id + '"  class="city-label">' + obj.name + '</div>');
			infowindowCity.setZIndex(-999);
		}

		google.maps.event.addListener(infowindowCity, 'domready', function() {
			$('.city-label').parent().parent().parent().addClass('city-label-outer');
			$('.city-label').parent().parent().parent().parent().addClass('city-label-outer-parent');
			$('.city-label').parent().parent().parent().parent().on('mousedown touchstart', function(event) {
				event.preventDefault();
			});
			$('.city-label').parent().parent().parent().prev().find('> :nth-child(2)').css('top', '8px');
			$('.city-label').parent().parent().parent().prev().find('> :nth-child(4)').css('top', '8px');
			$('.city-label').parent().parent().parent().next().remove();
		});

		// add event listener on hover
		marker.addListener('mouseover', function() {
			marker.infowindowCity.open(map, this);
		});
		// add event listener on hover
		marker.addListener('mouseout', function() {
			marker.infowindowCity.close();
		});

		// add event listener on click
		marker.addListener('click', function() {
			var inactiveIndex = map.inactiveMarkers.indexOf(marker);
			var activeIndex = map.activeMarkers.indexOf(marker);

			// create the custom info window html

			if (this.type == 'inactive') {
				// buildInfoWindow(obj, this.type, inactiveIndex);
				// infowindow.open(map, this);
				changeToActiveMarker(inactiveIndex);
			} else if (this.type == 'active' && activeIndex != 0) {
				buildInfoWindow(obj, this.type, activeIndex);
				infowindow.open(map, this);
				$('.info-window').parent().parent().parent().addClass('info-window-outer');
				// remove from active
				// changeToInactiveMarker(this);
			} else if (this.type == 'active' && activeIndex == 0) {
				obj.roundTrip = this.roundTrip;
				buildInfoWindow(obj, 'round-trip', activeIndex);
				infowindow.open(map, this);
				$('.info-window').parent().parent().parent().addClass('info-window-outer');
			}
			eroam.updateProposedItinerary(map.activeMarkers);
		});

		// add event listener on hover
		
		// google.maps.event.addListener(marker, 'mouseover', function() {
		// 	infowindow.open(map, marker);
		// });
		// google.maps.event.addListener(marker, 'mouseout', function() {
		// 	infowindow.close();
		// });
		return marker;
	}

	function setInfoBox(marker, obj) {
		var infoboxOptions = {
			content: '<div>' +  obj.name + '</div>',
			boxStyle: {
				textAlign: 'left',
				whiteSpace: 'nowrap',
				lineHeight: '16px',
				zIndex: '-999',
				margin: '-7px 0 0 35px' // RIGHT = -18px 0 0 24px, LEFT = 8px 0 0 -134px
			},
			disableAutoPan: true,
			pixelOffset: new google.maps.Size(-25, 0),
			position: marker.getPosition(),
			closeBoxURL: "",
			isHidden: false,
			pane: "floatPane",
			enableEventPropagation: true
		};

		var label = new InfoBox(infoboxOptions);
		marker.label = label;
	}

	function updateLabel(text, index) {
		var marker = map.activeMarkers[index];
		$('.marker-label').eq(index).html(text);
		marker.set('labelContent', text);
	}

	function buildInfoWindow(obj, type, index) {
		var controls = '';

		if (type == 'inactive') {
			controls = 
				'<div class="iw-controls">' +
					'<a href="#" data-marker-index="' + index + '" class="add-destination-btn"><i class="icon flaticon-plus"></i> add</a>' +
					'<a href="#" class="cancel-btn"> cancel</a>'
				'</div>';
		} else if (type == 'active') {
			var nights = parseInt(searchSession.itinerary[index].city.default_nights)
				nightsText = nights == 1 ? 'night' : 'nights';;
			controls = 
				'<div class="iw-controls">' +
					'<span data-nights="' + nights + '" data-index="' + index + '" class="number-of-nights text-center bold-txt">'+ nights + ' ' + nightsText + '</span>' +
					'<a href="#" data-marker-index="' + index + '" data-type="add" class="change-nights-btn"><i class="icon fa fa-plus"></i></a>' +
					'<a href="#" data-marker-index="' + index + '" data-type="subtract" class="change-nights-btn"><i class="icon fa fa-minus"></i></a>' +
					'<a href="#" data-marker-index="' + index + '" class="remove-btn"><i class="icon flaticon-remove"></i> remove destination</a>'
				'</div>';
		} else {
			// round-trip
			var nights = parseInt(searchSession.itinerary[index].city.default_nights),
				nightsText = nights == 1 ? 'Night' : 'Nights';
			var roundTrip = '';

			var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

			var date_from = searchSession.itinerary[index].city.date_from.split('-');
			var new_date_from = date_from[2] + ' ' + monthNames[date_from[1]] + ' ' + date_from[0];

			var date_to = searchSession.itinerary[index].city.date_to.split('-');
			var new_date_to = date_to[2] + ' ' + monthNames[date_to[1]] + ' ' + date_to[0];

			// Round trip controls
			if (map.activeMarkers.length > 1) {
				roundTrip = obj.roundTrip == true ? '<a href="#" data-marker-index="' + index + '" class="remove-round-trip-btn"><i class="icon flaticon-map"></i> Remove ' + obj.name + ' (Round-trip)</a>' : '<a href="#" data-marker-index="' + index + '" class="round-trip-btn"><i class="icon flaticon-map"></i> Return to ' + obj.name + ' (Round-trip)</a>';
			}

			controls = 
				'<div class="iw-controls">' +
					'<span data-nights="' + nights + '" data-index="' + index + '" class="number-of-nights text-center bold-txt">'+ nights + ' ' + nightsText + '</span>' +
					'<a href="#" data-marker-index="' + index + '" data-type="add" class="change-nights-btn"><i class="icon fa fa-plus"></i></a>' +
					'<a href="#" data-marker-index="' + index + '" data-type="subtract" class="change-nights-btn"><i class="icon fa fa-minus"></i></a>' +
					roundTrip +
				'</div>';
		}
		var html1 = 
			'<div class="info-window" data-map-type="' + mapData.type + '">' +
				'<div class="iw-title">' +
					'<p>' + obj.name + '3456</p>' +
				'</div>' +
				'<div class="iw-description">' +
					'<p class="bold-txt">Description</p>' +
					'<img class="pull-left" src="' + $('#cms-url').val() +( ( obj.image.length > 0 ) ? obj.image[0].small : '') + '" alt="' + obj.name + '" />' +
					'<p>' + obj.description + '</p>' +
				'</div>' +
				controls +
			'</div>';

		var html =
      		'<div id="content" class="info-window" data-map-type="' + mapData.type + '">' +
              '<div class="modal-image">' +
                '<img src="' + $('#cms-url').val() +( ( obj.image.length > 0 ) ? obj.image[0].small : '') + '" alt="' + obj.name + '"  class="img-responsive" />' +
              '</div>' +
              '<div id="bodyContent" class="padding-20">' +
               ' <h4 class="modal-title iw-title">' + obj.name + '</h4>' +
                '<p class="m-t-10 iw-description">' + obj.description + '</p>' +
                '<div class="row m-t-20">' +
                  '<div class="col-sm-2">' +
                    '<span class="date-label">Date:</span>' +
                  '</div>' +
                  '<div class="col-sm-10 m-t-5">'+ new_date_from +' - '+ new_date_to+' ('+nightsText+')</div>' +
                '</div>' +
                '<div class="m-t-30 row">' +
                  '<div class="col-sm-6"><a href="#">MORE INFO</a></div>' +
                  '<div class="col-sm-6 text-right">' +
                    '<a href="#" href="#" data-marker-index="' + index + '" data-type="add" class="change-nights-btn m-r-10">ADD NIGHT</a>' +
                    '<a href="#" data-marker-index="' + index + '" data-type="subtract" class="change-nights-btn">REMOVE NIGHT</a>' +
                  '</div>' +
                '</div>' +
              '</div>' +
            '</div>';
		infowindow.setContent(html);
	}

	function getSortedCitiesByShortestDistance(inBetween, origin, destination) {
		var sortedCities = [];
		sortedCities.push(origin);

		$.each(inBetween, function(k, v) {
			// if distance from origin to marker is greater than distance from origin to destination, unset
			if (v && v.distanceFromOrigin > getDistance(latlng(origin.lat, origin.lng), latlng(destination.lat, destination.lng))) {
				inBetween.splice(k, 1);
			}
		});
		inBetween = _.sortBy(inBetween, 'distanceFromOrigin');

		_.forEach(inBetween, function (result) {
			sortedCities.push(result);
		});
		sortedCities.push(destination);
		return sortedCities;
	}

	function sortMarkersByShortestDistance(marker, prevMarker, tolerance = 0.1) {

		// if tolerance is >= 2000KM, don't sort
		if (tolerance >= 20) {
			addNewLocation(marker, prevMarker);
			return;
		}
		// DEFAULT TOLERANCE IS 10KM
		var nearbyRoutes = $.grep(map.directionsRoutes, function(v, k) {
			if (google.maps.geometry.poly.isLocationOnEdge(marker.getPosition(), v, tolerance)) {
				return v;
			}
		});

		// CHECK IF INSIDE TOLERANCE
		if (nearbyRoutes.length > 0) {
			// TOTAL DISTANCE OF ALL NEARBY ROUTES
			var nearbyRoutesDistance = 0;
			$.grep(nearbyRoutes, function(v, k) {
				nearbyRoutesDistance += getDistance(v.origin, v.destination);
			});

			var shortestNearbyRoute = null;
			// DISTANCE OF EACH ROUTE
			$.each(nearbyRoutes, function(key, value) {
				var routeDistance = getDistance(value.origin, value.destination);
				var previewRouteDistance = getDistance(value.origin, marker.getPosition()) + getDistance(marker.getPosition(), value.destination);
				var otherRoutesDistance = nearbyRoutesDistance - routeDistance;
				// FINALIZE DISTANCE
				value.distance = previewRouteDistance + otherRoutesDistance;
				// GET THE SHORTEST DISTANCE
				if ((shortestNearbyRoute === null) || (value.distance < shortestNearbyRoute.distance)) {
					shortestNearbyRoute = value;
				}
			});

			// DISTANCE FROM LAST MARKER TO SELECTED LOCATION
			var normalRouteDistance = nearbyRoutesDistance + getDistance(prevMarker.getPosition(), marker.getPosition());

			// COMPARE NORMAL ROUTE TO SHORTEST NEARBY ROUTE
			if (normalRouteDistance < shortestNearbyRoute.distance) {
				// USE NORMAL ROUTE
				addNewLocation(marker, prevMarker);
			} else {
				// USE THE SHORTEST neARBY ROUTE
				// shortestNearbyRoute.
				// map.activeMarkers.splice();
				var index;
				$.grep(map.activeMarkers, function(v, k) {
					if (v.cityId == shortestNearbyRoute.to_city_id) {
						index = k;
					}
				});
				map.activeMarkers.splice(index, 0, marker);
				

				// UPDATE MAP AND ITINERARY
				var data = {
					city_ids: [shortestNearbyRoute.from_city_id, marker.cityId, shortestNearbyRoute.to_city_id],
					date_from: searchSession.travel_date,
					traveller_number: searchSession.travellers,
					auto_populate: searchSession.auto_populate,
					search_input: searchInput
				};

				var cityIds = [];
				$.each(map.activeMarkers, function(k, v) {
					cityIds.push(v.cityId);
				});

				eroam.ajax('post', 'map/update', { city_ids: cityIds }, function() {
					eroam.api('post', 'map/city-default', data, function(response) {
						var itineraryIndex;
						$.grep(searchSession.itinerary, function(v, k) {
							if (v.city.id == shortestNearbyRoute.from_city_id) {
								itineraryIndex = k;
							}
						});
						searchSession.itinerary[itineraryIndex].transport = response.data[0].transport;
						searchSession.itinerary.splice(itineraryIndex + 1, 0, response.data[1]);
						bookingSummary.update(JSON.stringify(searchSession), true, function() {
							eroam.ajax('get', 'latest-search', null, function(response) {
								searchSession = JSON.parse(response);
								resetRoutes();
								$('#map-loader').fadeOut();
							});
						});
					});
				});
			}

		} else {
			// IF OUTSIDE THE TOLERANCE, INCREASE THE TOLERANCE RANGE BY 10KM
			var newTolerance = tolerance + 0.1;
			sortMarkersByShortestDistance(marker, prevMarker, newTolerance);
		}
	}

	function changeToActiveMarker(index) {
		$('#map-loader').show();
		var marker     = map.inactiveMarkers[index],
			prevMarker = $(map.activeMarkers).last()[0],
			nights     = marker.nights.toString().length == 1 ? '0' + marker.nights : marker.nights;

		marker.set('labelContent', nights);
		// remove from inactive array then push to active array
		if (index > -1) {
			map.inactiveMarkers.splice(index, 1);
			marker.setIcon(theme.icon.active);
			marker.type = 'active';

			// CHECK IF LOCATION FALLS IN A ROUTE PATH
			if (map.directionsRoutes.length > 0 && mapData.auto_sort == 'on') {
				sortMarkersByShortestDistance(marker, prevMarker);
			} else {
				addNewLocation(marker, prevMarker);
			}
		}
	}

	function addNewLocation(marker, prevMarker) {
		map.activeMarkers.push(marker);
		var data = {
			city_ids: [prevMarker.cityId, marker.cityId],
			date_from: searchSession.travel_date,
			//traveller_number: searchSession.travellers,
			traveller_number: searchSession.travellers,
			auto_populate: searchSession.auto_populate,
			search_input: searchInput
		};

		var cityIds = [];
		$.each(map.activeMarkers, function(k, v) {
			cityIds.push(v.cityId);
		});

		eroam.ajax('post', 'map/update', { city_ids: cityIds }, function() {
			eroam.api('post', 'map/city-default', data, function(response) {
				$(searchSession.itinerary).last()[0].transport = response.data[0].transport;
				searchSession.itinerary.push(response.data[1]);
				bookingSummary.update(JSON.stringify(searchSession), true, function() {
					eroam.ajax('get', 'latest-search', null, function(response) {
						searchSession = JSON.parse(response);
						setRoute(marker);
						$('#map-loader').fadeOut();
					});
				});
			});
		});
	}

	function changeToInactiveMarker(index) {
		$('#map-loader').show();
		var marker = map.activeMarkers[index];
		// remove from active array then push to inactive array
		if (index > -1 && index != 0) {
			if (!marker.roundTrip) {
				map.inactiveMarkers.push(marker);
				if (parseInt(marker.city.optional_city) == 1) {
					marker.setIcon(theme.icon.optional);
				} else {
					marker.setIcon(theme.icon.inactive);
				}
				marker.type = 'inactive';
			} else {
				delete marker.roundTrip;
			}
			map.activeMarkers.splice(index, 1);
			// reset all routes
			resetRoutes();

			var cityIds = [];
			$.each(map.activeMarkers, function(k, v) {
				cityIds.push(v.cityId);
			});

			eroam.ajax('post', 'map/update', { city_ids: cityIds }, function() {

				var prev = searchSession.itinerary[index - 1],
					next = searchSession.itinerary[index + 1];
				// check if marker is in between
				if (next) {
					var data = {
						from_city_id: prev.city.id,
						to_city_id: next.city.id
					};
					console.log('searchSession', searchSession);
					eroam.api('post', 'map/get-transport-by-city-ids', data, function(response) {
						console.log('transport by ids', response);
						prev.transport = response.data[0] ? response.data[0] : null;
						searchSession.itinerary.splice(index, 1);
						bookingSummary.update(JSON.stringify(searchSession), true);
					});
				} else { // if it is the last marker, remove from array and set the prev transport to null
					prev.transport = null;
					searchSession.itinerary.splice(index, 1);
					bookingSummary.update(JSON.stringify(searchSession), true);
				}

			});
			setTimeout(function() {
				$('#map-loader').fadeOut();
			}, 2000);
		}
	}

	function checkNearbyCities(marker, callback) {
		var circle = addCircle(marker.getPosition().lat(), marker.getPosition().lng());
		if (circle) {
			var bounds = circle.getBounds(),
				northeast = bounds.getNorthEast(),
				southwest = bounds.getSouthWest(),
				data = {
					'nlat' : parseFloat(northeast.lat()),
					'nlng' : parseFloat(northeast.lng()),
					'slat' : parseFloat(southwest.lat()),
					'slng' : parseFloat(southwest.lng()),
					'cityId': marker.cityId
				};

			// getting all cities within the radius by comparing lat & lng
			eroam.api('post', 'map/nearby-cities', data, function(response) {
				var index = map.activeMarkers.indexOf(marker);
				var found = response.data.some(function(v, k) {
					return v.id == map.activeMarkers[index + 1].cityId;
				});
				callback(found);
			});
		}
	}

	function showNearbyCities(marker) { // marker = show nearby cities of this marker(city)
		// display all cities
		var cities = JSON.parse($('#all-cities').val());
		$.each(cities, function(key, value) {
			var found = map.activeMarkers.some(function(v, k) {
				return v.cityId == value.id;
			});
			if (value.latlong && !found) {
				value.lat = value.latlong.lat;
				value.lng = value.latlong.lng;
				addMarker(value);
			}
		});
		return;

		// resetInactiveMarkers();

		// // remove previous radius
		// for (var i = 0; i < map.circles.length; i++) {
		// 	map.circles[i].setMap(null);
		// }

		// var circle = addCircle(marker.getPosition().lat(), marker.getPosition().lng());
		// if (circle) {
		// 	var bounds = circle.getBounds(),
		// 		northeast = bounds.getNorthEast(),
		// 		southwest = bounds.getSouthWest(),
		// 		data = {
		// 			'nlat' : parseFloat(northeast.lat()),
		// 			'nlng' : parseFloat(northeast.lng()),
		// 			'slat' : parseFloat(southwest.lat()),
		// 			'slng' : parseFloat(southwest.lng()),
		// 			'cityId': marker.cityId,
		// 		};

		// 	// getting all cities within the radius by comparing lat & lng
		// 	eroam.api('post', 'map/nearby-cities', data, function(response) {
		// 		$.each(response.data, function(index, city) {
		// 			city.lat = city.latlong.lat;
		// 			city.lng = city.latlong.lng;
		// 			// check if city is present in markers array
		// 			var found = map.activeMarkers.some(function(v, k) {
		// 				return v.cityId == city.id;
		// 			});
		// 			// check if found and not yet active
		// 			if (!found && checkIfInCircle(circle, latlng(city.lat, city.lng))) {
		// 				addMarker(city);
		// 			} else if(!found && city.has_transport != 0) {
		// 				addMarker(city);
		// 			}
		// 		});
		// 	});
		// }
	}

	function fitMarkersToMap() {
		var bounds = new google.maps.LatLngBounds();
		$.each(map.activeMarkers, function(k, v) {
			bounds.extend(v.getPosition());
		});

		google.maps.event.addListener(map, 'zoom_changed', function() {
			var listener = google.maps.event.addListener(map, 'bounds_changed', function(event) {
				if (this.getZoom() > 5 && this.initialZoom === true) {
					this.setZoom(5);
					this.initialZoom = false;
				}

				google.maps.event.removeListener(listener);
			});
		});
		map.initialZoom = true;
		map.fitBounds(bounds);

	}

	function setRoundTrip() {
		$('#map-loader').show();
		map.activeMarkers[0].roundTrip = true;
		map.activeMarkers.push(map.activeMarkers[0]);
		var marker = $(map.activeMarkers).last()[0];

		var data = {
			city_ids: [map.activeMarkers[map.activeMarkers.length - 2].cityId, marker.cityId],
			date_from: searchSession.travel_date,
			traveller_number: searchSession.travellers,
			auto_populate: searchSession.auto_populate
		};

		var cityIds = [];
		$.each(map.activeMarkers, function(k, v) {
			cityIds.push(v.cityId);
		});

		eroam.ajax('post', 'map/update', { city_ids: cityIds }, function() {
			eroam.api('post', 'map/city-default', data, function(response) {
				$(searchSession.itinerary).last()[0].transport = response.data[0].transport;
				response.data[1].activities = null;
				response.data[1].city.default_nights = '1';
				searchSession.itinerary.push(response.data[1]);
				bookingSummary.update(JSON.stringify(searchSession), true, function() {
					eroam.ajax('get', 'latest-search', null, function(response) {
						searchSession = JSON.parse(response);
						setRoute(marker, null, true, map.activeMarkers.length - 1);
						$('#map-loader').fadeOut();
					});
				});
			});
		});
		setTimeout(function() {
			$('#map-loader').fadeOut();
		}, 2000);
	}

	function removeRoundTrip() {
		var roundTripIndex;
		$.grep(map.activeMarkers, function(value, key) {
			if (key != 0 && value.roundTrip) {
				roundTripIndex = key;
			}
		});
		changeToInactiveMarker(roundTripIndex);
	}

	function initManual() {
		if (mapData.cities.length == 1) {
			// set map centered to starting point
			setCenter(mapData.cities[0]);
			// add green marker to it
			var marker = addMarker(mapData.cities[0], true, 0);
			// show nearby cities
			fitMarkersToMap();
			$('#map-loader').fadeOut();
		} else {
			var tempArray = [],
				found;
			$.each(mapData.cities, function(key, value) {
				if (tempArray.indexOf(value.id) > -1) {
					found = true;
				} else {
					tempArray.push(value.id);
					found = false;
				}

				if (key == 0) {
					var marker = addMarker(value, true, key);
				} else {
					if (found) {
						// round trip
						map.activeMarkers[0].roundTrip = true;
						map.activeMarkers.push(map.activeMarkers[0]);
						var marker = $(map.activeMarkers).last()[0];
						setRoute(marker, null, true, key);
					} else {
						var marker = addMarker(value, false, key);
						var index = map.inactiveMarkers.indexOf(marker);
						map.inactiveMarkers.splice(index, 1);
						map.activeMarkers.push(marker);
						marker.setIcon(theme.icon.active);
						marker.type = 'active';
						setRoute(marker);
					}
				}
			});
			setTimeout(function() {
				fitMarkersToMap();
				$('#map-loader').fadeOut();
			}, 2000);
		}
		showNearbyCities();
	}

	function saveSettings(settings) {
		mapSettings = settings; 

		// show nearby citites
		showNearbyCities($(map.activeMarkers).last()[0]);
	}

	function buildAutoMap() {
		$.each(mapData.routes, function(key, value) {
			if (value.default == 'yes') {
				$.each(value.cities, function(cityIndex, city) {
					if (cityIndex == 0) {
						var marker = addMarker(city, true, cityIndex);
					} else {
						var marker = addMarker(city, false, cityIndex);
						var index = map.inactiveMarkers.indexOf(marker);
						map.inactiveMarkers.splice(index, 1);
						map.activeMarkers.push(marker);
						marker.setIcon(theme.icon.active);
						marker.type = 'active';
						setRoute(marker);
					}
				});
			} else {
				// ALTERNATIVE ROUTES
				setAlternativeRoute(value);
			}
		});
		if (mapData.routes.length != 1 || mapData.routes[0].cities.length != 2) {
			setTimeout(function() {
				$('#map-loader').fadeOut();
			}, 2000);
		}
	}

	function switchToManual() {
		var cityIds = [];
		$.each(map.activeMarkers, function(k, v) {
			cityIds.push(v.cityId);
		});

		eroam.ajax('post', 'map/update', { city_ids: cityIds }, function() {
			window.location.reload();
		});

	}

	/*
	| Function Added by Junfel
	*/
	

	function getDeparture(itineraryIndex){
		var fromDate = eMap.searchSession.itinerary[itineraryIndex].city.date_from;
		var toDate = eMap.searchSession.itinerary[itineraryIndex].city.date_to;
		var newDateTo = addDays(fromDate, parseInt(eMap.searchSession.itinerary[itineraryIndex].city.default_nights)); 
		var transport = eMap.searchSession.itinerary[itineraryIndex].transport;

		var departureDate = toDate+' 24';
		if(transport != null){
			var eta = transport.eta;
			var  travelDuration =  transport.duration;
			if(transport.provider == 'mystifly'){
			
				travelDuration = formatTime(get_hours_min(travelDuration));
				eta = moment( eta, moment.ISO_8601 );
				eta = eta.format('hh:mm A');

			}else{
				travelDuration = removePlus(travelDuration);
				travelDuration = formatTime(travelDuration);
			}

			var arrivalTime = arrival_am_pm(eta);
			departureDate = getDayTime(newDateTo+' '+arrivalTime, travelDuration);
		}

		return departureDate;
	}

	function activityCancellation( activityDates, itineraryIndex, act, nights ){
		if( activityDates.length > 0 ){

			eroam.confirm( null, 'Decreasing the number of nights can cause schedule conflicts for the activities on this city. Would you like to cancel some activities for this city?', function(e){
				
				if( act.length > 0){
					for( index = act.length-1; index >= 0; index-- ){
						var actDuration = parseInt(act[index].duration);
						var multiDayRemoved = false;
						if(actDuration > 1){

							actDuration = actDuration - 1;
							var activityDateSelected = act[index].date_selected;
							for( var counter = 1;counter <= actDuration; counter ++ ){
								var multiDayActivityDate = addDays( activityDateSelected, counter );

								if( activityDates.indexOf( multiDayActivityDate ) > -1 && !multiDayRemoved ){
									act.splice(index, 1);
									multiDayRemoved = true;
								}
							}

						}else{
							if( activityDates.indexOf( act[index].date_selected ) > -1 ){
								act.splice(index, 1);
							}
						}
					}
				}
				var nightsText = nights == 1 ? 'night' : 'nights';
				$('.number-of-nights[data-index="' + itineraryIndex + '"]').data('nights', nights).html(nights + ' ' + nightsText);

				eMap.searchSession.itinerary[itineraryIndex].activities = act;
				bookingSummary.update( JSON.stringify( eMap.searchSession ) );
				var nightsTextOnMarker = nights.toString().length == 1 ? '0' + nights : nights;
				eMap.updateMarkerlabel(nightsTextOnMarker, itineraryIndex);
			} );

		}else{
			bookingSummary.update( JSON.stringify( eMap.searchSession ) );
			var nightsTextOnMarker = nights.toString().length == 1 ? '0' + nights : nights;
			eMap.updateMarkerlabel(nightsTextOnMarker, itineraryIndex);
		}
	}

	/*
	| End Functions added by Junfel
	*/


	function saveOrder(e) {
		e.preventDefault();
		var cityIds = [];

		$('#reorder-locations li').each(function(key, value) {
			cityIds.push($(value).data('city'));
		});

		var data = {
			city_ids: cityIds,
			date_from: searchSession.travel_date,
			traveller_number: searchSession.travellers,
			auto_populate: searchSession.auto_populate,
			search_input: searchInput
		};

		eroam.ajax('post', 'map/update', {city_ids: cityIds, update_search: true}, function(response) {
			mapData = response.map_data;
			init();
			searchSession = response.search_session;
			bookingSummary.update(JSON.stringify(searchSession), true);
		}, function() {
			$('.booking-summary').html('<span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> Updating Data...</span>');
			$('#map-loader').show();
		});
	}

	return {
		init: init,
		saveSettings: saveSettings,
		addDestination: changeToActiveMarker,
		removeDestination: changeToInactiveMarker,
		searchSession: searchSession,
		updateMarkerlabel: updateLabel,
		setRoundTrip: setRoundTrip,
		removeRoundTrip: removeRoundTrip,
		switchRoute: switchRoute,
		switchToManual: switchToManual,
		saveOrder: saveOrder,
		/* Added by Junfel */
		getDeparture: getDeparture,
		activityCancellation: activityCancellation

	};

})(jQuery);

$(function() {

	google.maps.event.addListener(infowindow, 'domready', function() {
		var iwOuter = $('.info-window').parent().parent().parent();
		var iwBackground = iwOuter.prev();

		iwBackground.children(':nth-child(2)').css({'display' : 'none'});
		iwBackground.children(':nth-child(4)').css({'display' : 'none'});
		$('.info-window-outer > div:last').remove();

		var iwCloseBtn = iwOuter.next();
		iwCloseBtn.css({
			opacity: '1',
			right: '35px',
			top: '35px',
			border: '0',
			width: 'auto',
			padding: '2px 5px',
			height: 'auto',
			// 'border-radius': '13px', // circular effect
			// 'box-shadow': '0 0 5px #3990B9' // 3D effect to highlight the button
		});

		iwCloseBtn.html('<img src="' + eroam.siteUrl + '/assets/img/icons/cross.png" alt="" />');
		iwCloseBtn.mouseout(function() {
			$(this).css({opacity: '1'});
		});

		// remove arrow
		iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'display: none !important;'});
		iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'display: none !important;'});

		if ($('.info-window').data('map-type') == 'auto') {
			$('.round-trip-btn').hide();
			$('.remove-round-trip-btn').hide();
			$('.remove-btn').hide();
		}
	});


	/* Info WIndow Controls */
	$('body').on('click', '.iw-controls .add-destination-btn', function(event) {
		event.preventDefault();
		var index = $(this).data('marker-index');
		eMap.addDestination(index);
		infowindow.close();
	});

	$('body').on('click', '.iw-controls .remove-btn', function(event) {
		event.preventDefault();
		var index = $(this).data('marker-index');
		eMap.removeDestination(index);
		infowindow.close();
	});

	$('body').on('click', '.iw-controls .cancel-btn', function(event) {
		event.preventDefault();
		infowindow.close();
	});

	$('body').on('click', '.iw-controls .round-trip-btn', function(event) {
		event.preventDefault();
		eMap.setRoundTrip();
		infowindow.close();
	});

	$('body').on('click', '.iw-controls .remove-round-trip-btn', function(event) {
		event.preventDefault();
		eMap.removeRoundTrip();
		infowindow.close();
	});

	$('body').on('click', '.change-nights-btn', function(event) {
		event.preventDefault();
		var type = $(this).data('type'),
			index = $(this).data('marker-index'),
			nights = $('.number-of-nights[data-index="' + index + '"]').data('nights'),
			nightsText = '';
		/*
		| Updated by Junfel
		*/
		eroam.ajax( 'get', 'latest-search', { }, function( response ){
			if(response){
				eMap.searchSession = JSON.parse(response);
				if (type == 'add' && nights + 1 <= 10) {
					nights = nights + 1;
					nightsText = nights == 1 ? 'night' : 'nights';
					$('.number-of-nights[data-index="' + index + '"]').data('nights', nights).html(nights + ' ' + nightsText);
					eMap.searchSession.itinerary[index].city.default_nights = parseInt(eMap.searchSession.itinerary[index].city.default_nights) + 1;
					/*
					| Updated by junfel
					*/
					eMap.searchSession.itinerary[index].city.add_after_date = eMap.searchSession.itinerary[index].city.date_from;
					eMap.searchSession.itinerary[index].city.days_to_add = 1;
					/*
					| End Added by junfel
					*/
					bookingSummary.update(JSON.stringify(eMap.searchSession));
					var nightsTextOnMarker = nights.toString().length == 1 ? '0' + nights : nights;
					eMap.updateMarkerlabel(nightsTextOnMarker, index);
				} else if (type == 'subtract' && nights - 1 >= 1) {
					nights = nights - 1;
					/*nightsText = nights == 1 ? 'night' : 'nights';
					$('.number-of-nights[data-index="' + index + '"]').data('nights', nights).html(nights + ' ' + nightsText);*/
					
					eMap.searchSession.itinerary[index].city.default_nights = parseInt(eMap.searchSession.itinerary[index].city.default_nights) - 1;
					/*
					| Added by Junfel
					*/
					var val = parseInt(eMap.searchSession.itinerary[index].city.default_nights) - 1;
					eMap.searchSession.itinerary[index].city.deduct_nights = 1;

					if( eMap.searchSession.itinerary[index].activities != null ){
						var deduct = 1;
						var act = sortByDate(eMap.searchSession.itinerary[index].activities);
						var departureDate = eMap.getDeparture(index);
						departureDate = departureDate.split(' ');

					/*	if( parseInt(departureDate[1]) < 18 ){
							deduct = parseInt(deduct) - 1;
						}*/
						var activityDates = getActivityToBeCancelledByDates( eMap.searchSession.itinerary[index].city.date_to, parseInt(deduct) );
						
						eMap.activityCancellation(activityDates, index, act, nights );
					}else{
						bookingSummary.update(JSON.stringify(eMap.searchSession));
						var nightsTextOnMarker = nights.toString().length == 1 ? '0' + nights : nights;
						eMap.updateMarkerlabel(nightsTextOnMarker, index);
					}
					/*
					| End Added by Junfel
					*/
				}
			}
			
		});
	});

	$('body').on('click', '#save-order-btn', eMap.saveOrder);
});
function addDays(date, days) {
	var result = new Date(date);
	result.setDate(result.getDate() + parseInt(days) );
	return dateFormatter(result);
}
function dateFormatter(date, time = false) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if(time){
    	day += ' '+ d.getHours();
    }
    return [year, month, day].join( '-' );
}
function formatTime(time){
	return time.replace(/hr\(s\)|min\(s\)|hours|minutes /gi, function(x){
		return x == 'hr(s)' || 'hours' ? ':' : '';
	});
}
function get_hours_min(hour_min){
	var hours_mins = hour_min.toString().split(".");
	var hours = parseInt(hours_mins[0]);
	var mins = parseFloat('0.'+hours_mins[1]) * 60;

	return hours+' hr(s) '+Math.ceil(mins)+' min(s)';
}
function removePlus(time){
	return time.replace( /\+/g, '' );
}
function arrival_am_pm(eta){

	var result;
	var arrival_split = eta.split("+"); 
	var arrival_split_am_pm = arrival_split[0].split(" ");
	var arrival_split_hour_min = arrival_split[0].split(":");
	var arrival_hour = parseInt(arrival_split_hour_min[0]);
	var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


	if(typeof arrival_split_am_pm[1] !== 'undefined'){

		result = padd_zero(arrival_hour)+':'+arrival_min+' '+arrival_split_am_pm[1];
	}else{
		if( arrival_hour < 12 ){
			result = padd_zero(arrival_hour)+':'+arrival_min+' AM';
		}else{
			hour = arrival_hour - 12;
			result = padd_zero(hour)+':'+arrival_min+' PM';
		}
	}
	
	return result;
}

function getDayTime(arrivalDate, duration) {
	var hours_mins = duration.split(':');
	var result = new Date(arrivalDate);
	result.setHours(result.getHours() - parseInt(hours_mins[0]));
	result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));

	return dateFormatter(result, true);
}
function padd_zero(number) {
	if(parseInt(number) == 0){
		number = 12;
	}
    return (number < 10) ? ("0" + number) : number;
}

function getActivityToBeCancelledByDates( dateTo, numberOfDays ){
	var dateArray = [];
	if( numberOfDays ){
		dateArray.push(dateTo);
		for( var count = 1;count <= numberOfDays; count ++ ){
			dateArray.push( deductDays( dateTo, count) )
		}
	}
	return dateArray;
}
function sortByDate(activities){
    var sorted = activities.sort(function (itemA, itemB) {
		var A = itemA.date_selected;
		var B = itemB.date_selected;
	    return A.localeCompare(B);
    });
   return sorted;
}

function deductDays(date, days) {
	var result = new Date(date);
	result.setDate(result.getDate() - parseInt(days) );
	return formatDate(result);
}
