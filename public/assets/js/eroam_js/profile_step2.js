$(document).ready(function(){

    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Please enter letters only."); 
    $("#profile_step2_form").validate({
        rules: {
            phy_address_1: {
                required: true,
            },
            phy_state: {
                required: true
            },
            phy_city: {
                required: true,
            },
            phy_zip: {
                required: true,
            },     
            phy_country: {
                required: true,
            },       
            bill_address_1: {
               required: true,
            },
            bill_state: {
                required: true
            },
            bill_city: {
                required: true
            },
            bill_zip: {
                required: true
            },
            bill_country: {
                required: true
            }
        },
        highlight: function (element) { // hightlight error inputs
            heightSidemenu();
        },        
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    jQuery("#profile_step2_form").submit(function(event) {
        event.preventDefault();
        console.log("ERROR");
        heightSidemenu();
        return false;
    });
});

$("#bill_checkbox").click(function() {
    if ($(this).is(':checked')) 
    {
        $(".bill_address_1").val($(".phy_address_1").val());
        $(".bill_address_2").val($(".phy_address_2").val());
        $(".bill_country").val($(".phy_country").val());
        $(".bill_state").val($(".phy_state").val());
        $(".bill_city").val($(".phy_city").val());
        $(".bill_zip").val($(".phy_zip").val());
        $(".bill_country").val($(".phy_country").val());
    }
    else
    {
        $(".bill_address_1").val('');
        $(".bill_address_2").val('');
        $(".bill_country").val('');
        $(".bill_state").val('');
        $(".bill_city").val('');
        $(".bill_zip").val('');
        $(".bill_country").val('');
    }
});