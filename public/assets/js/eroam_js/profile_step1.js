


$(document).ready(function(){

    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Please enter letters only."); 

    $.validator.addMethod("pwcheck", function(value) {
        if(value == ''){
            return true;
        }
        return /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&].{8,}$/.test(value) 
    }, "Password must contain Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:");

    var csrf_token = $("#csrf_token").val();
    $.validator.addMethod("verifyEmail",
        function(value, element) {
            var reg_user_email = $("#registered_user_id").val();
            var result = false;
            if(value == reg_user_email)
            {
                return true;
            }
            $.ajax({
                type:"POST",
                async: false,
                url: "/email/validate", // script to validate in server side
                data: {"reg_email": value,"_token": csrf_token},
                success: function(data) {
                    heightSidemenu();
                    var final_result = '';
                    if(data.success == 1)
                    {
                        if(data.social == 1)
                        {
                            final_result = false;
                        }
                        else
                        {
                            final_result = true;    
                        }                        
                    }
                    else
                    {
                        final_result = false;
                    }
                    result = final_result;
                }
            });
            // return true if username is exist in database
            return result;
        }
        
    );

    $("#profile_step1_form").validate({
        debug:true,
        rules: {
            title: {
                required: true,
            },
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            email: {
                required: true,
                email:true,
                verifyEmail : true
            },
            contact_no: {
                required: true,
                number: true,
            },       
            new_password: {
                minlength: 8,    
                pwcheck : true,           
            },
            confirm_password: {
               equalTo : "#new_password",
            },
            pref_gender: {
                required: true
            },
            pref_age_group_id: {
                required: true
            },
            pref_nationality_id: {
                required: true
            },
            currency: {
                required: true
            }
        },
        messages: {
            first_name: {
                required: "Please enter First Name."
            },
            last_name: {
                required: "Please enter Last Name."
            },
            email: {
                required: "Please enter Email Address.",
                verifyEmail: "Email already exists"
            },
            confirm_password: {
                equalTo: 'New Password and Confirm Password must be same.'
            },
            pref_nationality_id: {
                required: "Please select Nationality.",
            },
            pref_gender: {
                required: "Please select Gender.",
            },
            currency: {
                required: "Please select Currency.",
            },
            new_password:{
                minlength : "Minimum Length of new password must be 8 characters long",
            },
        },
        highlight: function (element) { // hightlight error inputs
            heightSidemenu();
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    jQuery("#profile_step1_form").submit(function(event) {
        event.preventDefault();
        console.log("ERROR");
        heightSidemenu();
        return false;
    });
});

