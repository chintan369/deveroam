var w = $( document ).width();
function mobileMenu(){

    if(w < 992){
    //document.body.className = document.body.className.replace("pushy-open-left","");
    $('body').removeClass('pushy-open-left');
    } else{
    $('body').addClass('pushy-open-left');
    }
}

function heightSidemenu(){
    if(w > 420){
    var oheight = $('.content-container').outerHeight() - 1;
    $(".page-sidebar-wrapper .pushy").height(oheight);
    }else{
    var oheight = $('.content-container').outerHeight() + 30;
    $(".page-sidebar-wrapper .pushy").height(oheight);
    }
}

$(window).resize(function(){
    heightSidemenu();
    mobileMenu();
});

$(document).ready(function(){

    heightSidemenu();
    mobileMenu();
});