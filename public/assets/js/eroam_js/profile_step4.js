$(document).ready(function(){

    heightSidemenu();
    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Please enter letters only."); 

    var csrf_token = $("#csrf_token").val();
    $.validator.addMethod("verifyEmail",
        function(value, element) {
            var reg_user_email = $("#registered_user_id").val();
            var result = false;
            if(value == reg_user_email)
            {
                return true;
            }
            $.ajax({
                type:"POST",
                async: false,
                url: "/email/validate", // script to validate in server side
                data: {"reg_email": value,"_token": csrf_token},
                success: function(data) {
                    heightSidemenu();
                    var final_result = '';
                    if(data.success == 1)
                    {
                        if(data.social == 1)
                        {
                            final_result = false;
                        }
                        else
                        {
                            final_result = true;    
                        }                        
                    }
                    else
                    {
                        final_result = false;
                    }
                    result = final_result;
                }
            });
            // return true if username is exist in database
            return result;
        },
        
    );

    $("#profile_step4_form").validate({

        rules: {
            email: {
                required:true,
                email:true,
                verifyEmail : true
            }
            /*phy_address_1: {
                required: true,
            },
            phy_address_2: {
                required: true
            },
            phy_state: {
                required: true
            },
            phy_city: {
                required: true,
            },
            phy_zip: {
                required: true,
            },     
            phy_country: {
                required: true,
            },       
            contact_no: {
               required: true,
            },
            mobile_no: {
                required: true
            },
            pref_contact_method: {
                required: true
            },*/
        },    
        messages: {
            email: {
                verifyEmail: "Email already exists"
            }
        }, 
        highlight: function (element) { // hightlight error inputs
            heightSidemenu();
        },   
        errorPlacement: function (label, element) {
            alert('inn');
            heightSidemenu();
            label.insertAfter(element);  
            
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
    jQuery("#profile_step2_form").submit(function(event) {
        event.preventDefault();
        console.log("ERROR");
        heightSidemenu();
        return false;
    });
});

function heightSidemenu(){
    var w = $( document ).width();
    if(w > 420){
      var oheight = $('.content-container').outerHeight() - 1;
      $(".page-sidebar-wrapper .pushy").height(oheight);
    }else{
      var oheight = $('.content-container').outerHeight();
      $(".page-sidebar-wrapper .pushy").height(oheight);
    }
}

$(document).resize(function(){
    heightSidemenu();
    alert('resize');
});
